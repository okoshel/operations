############################################################################################################
#
# Add to crontab. Set to run everyday 02:00AM
#
############################################################################################################
import csv
import json
import psycopg2
import time
import sys
import datetime
from datetime import date, timedelta
from time import gmtime, strftime
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

# Global variables
html_row = ""
message = ''

today = date.today()
yesterday = date.today() - timedelta(days=1)

# Redshift customer_connection
primary_cluster = psycopg2.connect(host="live-eu-west-1-a-proxylogs-1.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com",
                                   database="loggingdata", user="logging_data", password="RacRuJexAreB7amApast", port="5439")
cur_primary = primary_cluster.cursor()

# Redshift customer_connection
secondary_cluster = psycopg2.connect(host="redshift-secondary-live-eu-west-1c.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com",
                                     database="loggingdata", user="logging_data", password="5hefAduchadrEzu", port="5439")
cur_secondary = secondary_cluster.cursor()

query = "select DATE_TRUNC('minute', DATEADD(ms, startutcinms, '1970-01-01')) AS minute, count(*) AS requests, proxyname \
         from proxy_logs WHERE \
         DATEADD(ms, startutcinms, '1970-01-01') >= '2016-02-08 14:00:00' AND DATEADD(ms, startutcinms, '1970-01-01') < '2016-02-08 16:00:00' \
         AND proxyname = '8dd3fea2bffd4cdb.node.lon3.uk.wandera.com' \
         group by minute, proxyname order by minute, proxyname asc;" # %(yesterday, today, sys.argv[1])

# First query
print "Getting row numbers for all customers:", query
cur_primary.execute(query)
result_from_primary = cur_primary.fetchall()

cur_secondary.execute(query)
result_from_secondary = cur_secondary.fetchall()

for x, y in zip(result_from_primary, result_from_secondary):
    date = str(x[0])
    primary = x[1]
    secondary = y[1]
    proxyname = x[2]
    diff = 0
    diff = primary - secondary

    if primary >= secondary:
        percentage = (float(diff) * 100) / float(primary)
    else:
        percentage = (float(diff) * 100) / float(secondary)

    message += str(x[0]) + " " + proxyname + " " + str(primary) + " " + str(secondary) + " " + str(diff) + " " + str(round(percentage,3)) + "\n"
print message
