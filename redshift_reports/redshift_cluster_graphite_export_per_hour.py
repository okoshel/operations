############################################################################################################
#
# Add to crontab. Set to run everyday 02:00AM
#
############################################################################################################

import csv
import json
import psycopg2
import time
import datetime
from datetime import date, timedelta
from time import gmtime, strftime
# import smtplib
# from email.mime.text import MIMEText
import graphitesend


# Global variables
# message = "Hey Ops! \n"
# message += "Here is summary of rows for both production clusters per hour view: \n"
# message += "Date: \t\t Primary: \t Secondary: \t Difference"
my_list = [0,0,0]
today = date.today()
yesterday = date.today() - timedelta(days=1)


# Redshift customer_connection
primary_cluster = psycopg2.connect(host="redshift-secondary-live-eu-west-1c.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com", database="loggingdata", user="logging_data", password="5hefAduchadrEzu", port="5439")
cur_primary = primary_cluster.cursor()

# Redshift customer_connection
secondary_cluster = psycopg2.connect(host="redshift-secondary-live-eu-west-1c.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com", database="loggingdata", user="logging_data", password="5hefAduchadrEzu", port="5439")
cur_secondary = secondary_cluster.cursor()

query = "select DATE_TRUNC('hour', DATEADD(ms, startutcinms, '1970-01-01')) AS hour, count(*) AS requests from proxy_logs WHERE DATEADD(ms, startutcinms, '1970-01-01') >= '%s 00:00' AND DATEADD(ms, startutcinms, '1970-01-01') < '%s 08:00' group by hour order by hour asc;" %(today, today)

print "Query:", query

cur_primary.execute(query)
result_from_primary = cur_primary.fetchall()

cur_secondary.execute(query)
result_from_secondary = cur_secondary.fetchall()

for x, y in zip(result_from_primary, result_from_secondary):
    print "Date:", x[0] ,"Primary: ", x[1], "Secondary: ", y[1], "Diff: ", (x[1] - y[1])
    message += x[0], "\t\t", x[1], "\t ", y[1],"\t", (x[1] - y[1])

# print message
