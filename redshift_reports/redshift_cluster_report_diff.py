#
# You need to set up your ~/.aws/config with region = <region> and ~/.aws/credentials with aws_access_key_id and aws_secret_access_key
# python redshift_cluster_report_diff.py 2ef 1454770847 1454770929
#
import os
import sys, getopt
import subprocess
import json
import re
# import bsdiff4
from datetime import date, timedelta
# from email.mime.text import MIMEText
# import smtplib
import boto3
from boto3.dynamodb.conditions import Key, Attr

# TODO:

# Little helper
def set_default(obj):
    if isinstance(obj, set):
        return list(obj)
    raise TypeError
# Global vars:
PROXY_NAME = sys.argv[1]
START_TIME_IN_MS = int(sys.argv[2])
END_TIME_IN_MS = int(sys.argv[3])
REDSHIFT_PRIMARY_TABLE_NAME = "LambdaRedshiftBatchesProductionPrimary"
REDSHIFT_SECONDARY_TABLE_NAME = "LambdaRedshiftBatchesProductionSecondary"
REGION = "us-west-1"

def find_all_files(tablename, proxyname, starttime, endtime):
    # Get the service resource.
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(tablename)
    function_return = []
    # sorted_list = []
    try:
        response = table.query(
            IndexName='LambdaRedshiftBatchStatus',
            KeyConditionExpression=Key('status').eq('complete') & Key('lastUpdate').between(starttime, endtime),
            ProjectionExpression="entries"
            )
        for i in response[u'Items']:
            # print(json.dumps(i['entries'], default=set_default))
            for x in i['entries']:
                # print x
                if re.search(proxyname, x):
                    function_return.append (x)
        return sorted(function_return)

    except Exception as e:
        print("Exception",e)

def download_and_diff(primary, secondary):

    os.system("rm -rf temp/primary/* temp/secondary/*")

    for x,y in zip(primary, secondary):
        # x = wandera-redshift-data-live-eu-west-1/raw_access_log/year=2016/month=02/day=06/hour=15/minute=00/0bd0fe86-950e-11e5-81bb-027d8c3730c4_com.wandera.us.us-west-1c.node.2ef6179cec564e5d_0.gz

        print "About to download following: \n", x, "\n", y

        filename_full_primary =  "primary_" + x.split("/")[2] + x.split("/")[3] + x.split("/")[4] + x.split("/")[5] + x.split("/")[6] + "_" + x.split("/")[-1]
        filename_full_secondary =  "secondary_" + y.split("/")[2] + y.split("/")[3] + y.split("/")[4] + y.split("/")[5] + y.split("/")[6] + "_" + y.split("/")[-1]

        batch_primary_diff = "primary_diff"
        batch_secondary_diff = "secondary_diff"

        if x.split("_")[-1] != y.split("_")[-1]:
            print "Looks like we have missing file!"
            print "File from primary bucket: ...", x.split("_")[-2]
            print "File from secondary cluster: ....", y.split("_")[-2]
        # print "batch_primary:", batch_primary_diff, "batch_secondary:", batch_secondary_diff

        # print "filename_full_primary:", filename_full_primary, "filename_full_secondary:", filename_full_secondary

        os.system("s3cmd get s3://%s temp/primary/%s --force -q" % (x, filename_full_primary))
        os.system("s3cmd get s3://%s temp/secondary/%s --force -q" % (y, filename_full_secondary))

        os.system("gunzip -f temp/primary/%s" % (filename_full_primary))
        os.system("gunzip -f temp/secondary/%s" %(filename_full_secondary))

        os.system("echo '%s' >> temp/primary/%s" %(filename_full_primary.replace(".gz", ""), batch_primary_diff))
        os.system("echo '%s' >> temp/secondary/%s" %(filename_full_secondary.replace(".gz", ""), batch_secondary_diff))

        os.system("cat temp/primary/%s >> temp/primary/%s" %(filename_full_primary.replace(".gz", ""), batch_primary_diff))
        os.system("cat temp/secondary/%s >> temp/secondary/%s" %(filename_full_secondary.replace(".gz", ""), batch_secondary_diff))

    diff_output = subprocess.check_output("diff -urw temp/primary/%s temp/secondary/%s | grep ^+ | wc -l"
                                          %(batch_primary_diff, batch_secondary_diff), shell=True)
    print "Diff output:", diff_output

primary_files = find_all_files(REDSHIFT_PRIMARY_TABLE_NAME, PROXY_NAME, START_TIME_IN_MS, END_TIME_IN_MS)
secondary_files = find_all_files(REDSHIFT_SECONDARY_TABLE_NAME, PROXY_NAME, START_TIME_IN_MS, END_TIME_IN_MS)

download_and_diff(primary_files, secondary_files)

# print type(items)

# for i in secondary_batches:
#     print i
#     print type(i)
