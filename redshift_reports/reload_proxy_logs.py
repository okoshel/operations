from os.path import expanduser
import yaml
import argparse
import psycopg2
import json
from datetime import datetime, timedelta

def run():
    parser = argparse.ArgumentParser(description='Reload a proxy_log data from S3 stored data. Require env.AWS_SECRET and env.AWS_KEY to be set')
    parser.add_argument('-c', '--cluster', help='Which production redshift to reload', required=True, choices=['primary','secondary','app'])
    parser.add_argument('-s', '--starttime', help='start time in: YYYY-MM-DD-HH', type=str, required=True)
    parser.add_argument('-e', '--endtime', help='end time in: YYYY-MM-DD-HH (default: start time +1 hour)', type=str, required=False)
    parser.add_argument('-d', '--dryrun', help='show query but do not run any', action="store_true")

    args = parser.parse_args()

    try:  # Load config
        with open("config/config.yml", 'r') as ymlfile:
            cfg = yaml.load(ymlfile)
            cfg = cfg['production']
    except Exception as e:
        print "Couldn't load configuration !", e
        raise

    aws_secret = 'XXX'
    aws_key = 'XXX'
    target_cluster_cfg = cfg[args.cluster]
    del cfg[args.cluster]
    # Pick another cluster randomly
    other_cluster = cfg.keys()[0]
    print "--Using '%s' cluster for comparison" % other_cluster
    
    if not args.dryrun:
        target_conn = psycopg2.connect(host=target_cluster_cfg['host'],
                                       database=target_cluster_cfg['db'], user=target_cluster_cfg['user'],
                                       password=target_cluster_cfg['pass'], port=target_cluster_cfg['port'])
        target_cursor = target_conn.cursor()


        compare_conn = psycopg2.connect(host=cfg[other_cluster]['host'],
                                        database=cfg[other_cluster]['db'], user=cfg[other_cluster]['user'],
                                        password=cfg[other_cluster]['pass'], port=cfg[other_cluster]['port'])
        compare_cursor = compare_conn.cursor()
    try:
        with open(expanduser("~")+'/.woopas.json','r') as woopas_file:
            woopas_data = json.load(woopas_file)
            aws_secret = woopas_data["woopas"]["ec2_secret"]
            aws_key = woopas_data["woopas"]["ec2_user_id"]
    except IOError:
        raise Exception("~/.woopas.json file not found")
    except KeyError:
        raise Exception("AWS_credentials not set")
    except Exception as e:
        raise Exception("Issue while reading AWS credentials: %s", e)

    start_time = datetime.strptime(args.starttime, '%Y-%m-%d-%H')
    start_time = start_time.replace(minute=0, second=0)
    end_time = start_time + timedelta(hours=1)
    if args.endtime:
        end_time = datetime.strptime(args.endtime, '%Y-%m-%d-%H')
        end_time = end_time.replace(minute=0, second=0)
    if end_time <= start_time:
        raise Exception('End time must be after start time')
    if end_time - start_time > timedelta(hours=24):
        raise Exception('Time interval bigger than a day')

    start_time_formatted = start_time.strftime("%Y-%m-%d %H:00:00")
    end_time_formatted = end_time.strftime("%Y-%m-%d %H:00:00")
    query_delete = """DELETE
    from proxy_logs
    where
        customerId not in (select * from globalcustomerexclusionlist)
        AND startutcinms >= datediff(ms, '1970-1-1', '%s')
        AND startutcinms < datediff(ms, '1970-1-1', '%s');
    """ % (start_time_formatted, end_time_formatted)

    rolling_start_time = start_time
    queries_copy = []
    while rolling_start_time < end_time:
        query_copy = """COPY
        proxy_logs
        FROM
            's3://%(prefix)s/raw_access_log/year=%(year)s/month=%(month)s/day=%(day)s/hour=%(hour)s'
        CREDENTIALS
            'aws_access_key_id=%(key)s;aws_secret_access_key=%(secret)s'
        JSON
            's3://%(prefix)s/raw_access_log/jsonpaths/proxy_logs.jsonpaths'
            GZIP ACCEPTINVCHARS MAXERROR 100000 STATUPDATE OFF COMPUPDATE OFF TRUNCATECOLUMNS;
        """ % {'prefix': target_cluster_cfg['s3Prefix'],
               'year': rolling_start_time.strftime("%Y"),
               'month': rolling_start_time.strftime("%m"),
               'day': rolling_start_time.strftime("%d"),
               'hour': rolling_start_time.strftime("%H"),
               'key': aws_key,
               'secret': aws_secret
        }
        queries_copy.append(query_copy)
        rolling_start_time = rolling_start_time + timedelta(hours=1)

    query_compare = """SELECT
        DATE_TRUNC('hour', DATEADD(ms, startutcinms, '1970-01-01')) AS days,
        count(*)
    FROM proxy_logs
    WHERE
        customerId not in (select * from globalcustomerexclusionlist)
        AND startutcinms >= datediff(ms, '1970-1-1', '%s')
        AND startutcinms < datediff(ms, '1970-1-1', '%s')
    GROUP BY
        days
    ORDER BY
        days asc;
    """ % (start_time_formatted, end_time_formatted)

    if args.dryrun:
        print query_delete
        print '\n'.join(queries_copy)
        print query_compare
    else:
        print "Deleting proxy_logs"
        target_cursor.execute(query_delete)
        print "Uploading proxy_logs from S3"
        for query_copy in queries_copy:
            target_cursor.execute(query_copy)
        print "Checking count target cluster"
        target_cursor.execute(query_compare)
        print "Checking count compare cluster"
        compare_cursor.execute(query_compare)
        proxy_log_count_target = target_cursor.fetchall()
        proxy_log_count_compare = compare_cursor.fetchall()
        for target, compare in zip(proxy_log_count_target, proxy_log_count_compare):
            print "Time %s: Target '%s' %s rows - Compare '%s' %s rows: %.5f%% difference" % \
              (target[0], \
               args.cluster, target[1], \
               other_cluster, compare[1], \
               100 - float(target[1]*100) / compare[1]
               )

if __name__ == "__main__":
    run()