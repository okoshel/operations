#!/usr/bin/env python
import os
import yaml
import psycopg2
import psycopg2.extras
import csv
HERE_DIR = os.path.dirname(os.path.realpath(__file__))

def getQueryFromFile(name):
    query = ""
    with open(HERE_DIR+"/saved_query/"+name+'.sql', 'r') as queryfile:
        query= queryfile.read()
    return query

def listToSQL(arr):
    return "','".join(arr)

def to_utf8_string(val):
    try:
        if not isinstance(val, basestring):
            return str(val)
        if not isinstance(val, str):
            val = val.encode('utf8')
    except Exception as e:
        print "%s (%s):%s" % (val, type(val), e)
        raise
    return val

def run():
    try:  # Load config
        with open(HERE_DIR+"/config/config.yml", 'r') as ymlfile:
            cfg = yaml.load(ymlfile)
            cfg = cfg['production']
    except Exception as e:
        print "Couldn't load configuration !", e
        raise

    cluster_cfg = cfg['secondary']

    conn = psycopg2.connect(host=cluster_cfg['host'],
                                   database=cluster_cfg['db'],
                                   user=cluster_cfg['user'],
                                   password=cluster_cfg['pass'],
                                   port=cluster_cfg['port'])
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    rows = []
    ############################################################
    ####################   EDIT BELOW       ####################
    ############################################################
    SAVED_MAP = {
        1: 'custom_investigate',
        2: 'extend_block',
        3: 'extend_caps',
        4: 'extend_savings',
        5: 'secure_deviceview',
        6: 'secure_securityeventlogs',
        7: 'secure_threatview',
        8: 'security_appdiscovery',
        9: 'view_businesspersonal',
        10: 'view_categories',
        11: 'view_roaming',
        12: 'view_siteapps',
        13: 'view_tethering',
        14: 'view_usage'
    }

    if False:
        title="OCS-1822"
        query = """SELECT
            date_trunc('minute', DATEADD(ms, startUtcInMs, '1970-01-01')) as eventdate,
            request,
            datatransferred,
            status,
            proxyErrors,
            port
        FROM
            proxy_logs
        WHERE
            guid = '512f78a6-03f8-4102-8681-57b55dd89e97'
            AND startUtcInMs >= DATEDIFF(ms, '1970-01-01', '2017-06-19 00:00')
            AND startUtcInMs <= DATEDIFF(ms, '1970-01-01', '2017-06-19 23:10')
            AND datatransferred = 3746760
        ORDER BY eventdate ASC;
        """
    elif True:
        title= "OCS-2087"
        # query = getQueryFromFile(SAVED_MAP[12]) % { # OR USE THE MAP
        query = getQueryFromFile("view_siteapps") % {
            'customer': listToSQL(["c9a4e935-2071-4ebe-a615-e2e045292a44", "55e5fd7c-c1c8-459e-80a2-3fa8b39b91e6"]),
            'start': "2017-08-11 00:00:00",
            'end': "2017-08-12 00:00:00"
        }

    ############################################################
    ####################   EDIT ABOVE       ####################
    ############################################################

    print query
    cursor.execute(query)
    rows = cursor.fetchall()
    if len(rows) > 0:
        i_file=0
        while os.path.isfile(HERE_DIR+'/reports/' + title + '_'+str(i_file)+'.csv'):
            i_file +=1
        report_filename = HERE_DIR+'/reports/' + title + '_'+str(i_file)+'.csv'
        with open(report_filename, 'wb') as csvfile:
            mwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            headers = rows[0].keys()
            mwriter.writerow(headers)
            for row in rows:
                try:
                    row_data = [to_utf8_string(row.get(header,"Null")) for header in headers]
                    mwriter.writerow(row_data)
                except Exception as e:
                    print "Exception %s, writing: %s" % (e, row)
        print "File %s done" % (report_filename)
    else:
        print "No result returned"

if __name__ == "__main__":
    run()