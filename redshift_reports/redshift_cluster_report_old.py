############################################################################################################
#
# Add to crontab. Set to run everyday 02:00AM
#
############################################################################################################
import csv
import json
import psycopg2
import time
# import sys
import datetime
from datetime import date, timedelta
from time import gmtime, strftime
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

# TODO: add total diff at the bottom (total + and total -)

# Global variables
message = "Redshift - proxy_logs stats!!! \n"
message += "Here is summary of rows for both production clusters per hour view: \n"
message += "Date: \t\t Primary: \t Secondary: \t Difference\n"
html_table_open =  """\
<html>
  <head></head>
  <body>
    <table>
        <tbody>
            <tr>
               <th>Hour</th>
               <th>Primary</th>
               <th>Secondary</th>
               <th>Diff</th>
               <th>Percentage</th>
            </tr>
"""

html_table_close =  """\
         </tbody></table>
  </body>
</html>
"""

html_row = ""

today = date.today()
yesterday = date.today() - timedelta(days=1)


# Redshift customer_connection
primary_cluster = psycopg2.connect(host="live-eu-west-1-a-proxylogs-1.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com", database="loggingdata", user="logging_data", password="RacRuJexAreB7amApast", port="5439")
cur_primary = primary_cluster.cursor()

# Redshift customer_connection
secondary_cluster = psycopg2.connect(host="redshift-secondary-live-eu-west-1c.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com", database="loggingdata", user="logging_data", password="5hefAduchadrEzu", port="5439")
cur_secondary = secondary_cluster.cursor()

query = "select DATE_TRUNC('hour', DATEADD(ms, startutcinms, '1970-01-01')) AS hour, count(*) AS requests from proxy_logs WHERE DATEADD(ms, startutcinms, '1970-01-01') >= '%s' AND DATEADD(ms, startutcinms, '1970-01-01') < '%s' group by hour order by hour asc;"  %(yesterday, today)
query_group_by_proxy = "select DATE_TRUNC('day', DATEADD(ms, startutcinms, '1970-01-01')) AS hour, count(*) AS requests, proxyname from proxy_logs WHERE DATEADD(ms, startutcinms, '1970-01-01') >= '%s' AND DATEADD(ms, startutcinms, '1970-01-01') < '%s' group by hour, proxyname order by hour, proxyname asc;" %(yesterday, today)


# First query
print "Getting row numbers for all customers:", query
cur_primary.execute(query)
result_from_primary = cur_primary.fetchall()

cur_secondary.execute(query)
result_from_secondary = cur_secondary.fetchall()

for x, y in zip(result_from_primary, result_from_secondary):
    date = str(x[0])
    primary = x[1]
    secondary = y[1]
    diff = 0
    diff = primary - secondary

    if primary >= secondary:
        percentage = (float(diff) * 100) / float(primary)
    else:
        percentage = (float(diff) * 100) / float(secondary)

    message += str(x[0]) + " " + str(primary) + " " + str(secondary) + " " + str(diff) + " " + str(round(percentage,2)) + "\n"
    html_row += "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s%%</td></tr>\n" % (str(x[0]), str(primary), str(secondary), str(diff), str(round(percentage,2)))

html_row += "<tr><th>Hour</th><th>Proxy Name</th><th>Primary</th><th>Secondary</th><th>Diff</th><th>Percentage</th></tr>\n"

# Second query
print "Getting row numbers grouped by proxy:", query_group_by_proxy
cur_primary.execute(query_group_by_proxy)
result_from_primary = cur_primary.fetchall()

cur_secondary.execute(query_group_by_proxy)
result_from_secondary = cur_secondary.fetchall()

for x, y in zip(result_from_primary, result_from_secondary):
    date = str(x[0])
    primary = x[1]
    secondary = y[1]
    proxyname = x[2]
    diff = 0
    diff = primary - secondary

    if primary >= secondary:
        percentage = (float(diff) * 100) / float(primary)
    else:
        percentage = (float(diff) * 100) / float(secondary)

    message += str(x[0]) + " " + proxyname + " " + str(primary) + " " + str(secondary) + " " + str(diff) + " " + str(round(percentage,2)) + "\n"
    html_row += "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s%%</td></tr>\n" % (str(x[0]), proxyname, str(primary), str(secondary), str(diff), str(round(percentage,2)))

print message

msg = MIMEMultipart('alternative')

html_message = html_table_open + html_row + html_table_close
print html_message

# Record the MIME types of both parts - text/plain and text/html.
part1 = MIMEText(message, 'plain')
part2 = MIMEText(html_message, 'html')

# Attach parts into message container.
# According to RFC 2046, the last part of a multipart message, in this case
# the HTML message, is best and preferred.
msg.attach(part1)
msg.attach(part2)

msg['Subject'] = 'Redshift cluster report - rows per hour (%s - %s)' % (yesterday, today)
msg['From'] = 'root@bak-001-cops.eu-west-1b.ie.snappli.net'
msg['To'] = 'ops-notify@wandera.com'

s = smtplib.SMTP('localhost')
s.sendmail('root@bak-001-cops.eu-west-1b.ie.snappli.net', 'ops-notify@wandera.com', msg.as_string())
s.quit()
