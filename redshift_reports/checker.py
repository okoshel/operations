#   checker.py --task=clusterReport --starttime=2016-02-15-00-00 --duration=24h or --endtime
#   checker.py --task=countRows --cluster=primary  batchId=<batchId>
#   checker.py --task=showUnloaded --cluster=primary --countRows=true/false --status=error/locked
#   checker.py --task=checkIfLoaded --cluster=primary --rowsToTest=4 --batchId
import os
# import subprocess
import json
import yaml
import boto3
import gzip
import argparse
import psycopg2
# import datetime
from datetime import date, timedelta
from boto3.dynamodb.conditions import Key, Attr

parser = argparse.ArgumentParser()
parser.add_argument('--cluster', help='staging, primary, secondary', required=True)
parser.add_argument('--starttime', help='start time in: YYYY-MM-DD HH-MM ', type=str)
parser.add_argument('--endtime', help='start time in: YYYY-MM-DD HH-MM', type=str)
parser.add_argument('--batchId', help='batchId', type=str)
parser.add_argument('--dryrun', help='dryrun', default='true', required=True)
parser.add_argument('--task', help='clusterReport, countLines', default='help', required=True)

args = parser.parse_args()

try:                                                                                                        # Load config
    with open("config/config.yml", 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
except Exception as e:
    print "Couldn't load configuration !", e

conn = psycopg2.connect(host=cfg[args.cluster]['host'],                                                     # Redshift connection
                        database=cfg[args.cluster]['db'], user=cfg[args.cluster]['user'],
                        password=cfg[args.cluster]['pass'],port=cfg[args.cluster]['port'])

def query_redshift_for_rows_count(cluster, starttime, endtime):
    primary_cluster = psycopg2.connect(host=cfg[args.cluster]['host'],
                                       database=cfg[args.cluster]['db'],
                                       user=cfg[args.cluster]['user'],
                                       password=cfg[args.cluster]['pass'],
                                       port=cfg[args.cluster]['port'])
    cur_primary = primary_cluster.cursor()
    query = "select DATE_TRUNC('hour', DATEADD(ms, startutcinms, '1970-01-01')) \
             AS hour, count(*) AS requests, proxyname from proxy_logs WHERE DATEADD(ms, startutcinms, '1970-01-01') >= '%s' \
             AND DATEADD(ms, startutcinms, '1970-01-01') < '%s' \
             group by hour, proxyname order by hour, proxyname asc;" %(starttime, endtime)
    cur_primary.execute(query)
    rows_count = cur_primary.fetchall()

    return rows_count

def find_files_in_batch(tablename, batchId, s3Prefix):
    dynamodb = boto3.resource('dynamodb') # Get the service resource
    table = dynamodb.Table(tablename)
    function_return = []
    try:
        response = table.query(
            KeyConditionExpression=Key('s3Prefix').eq(s3Prefix) & Key('batchId').eq(batchId),
            )
        for i in response[u'Items']:
            for x in i['entries']:
                function_return.append (x)
        return function_return
    except Exception as e:
        print("Exception",e)

def download_and_count(batch):
    s3 = boto3.resource('s3')                                                                               # boto3 s3 connection
    os.system("rm -rf temp/batch_count/*")
    num_lines = 0
    for entry in batch:
        print "Entry typ: ", type(entry), entry
        print entry.split("/")[7], entry.split("/")[-1]
        s3.meta.client.download_file('%s','%s' 'temp/') %(entry.split("/")[7], entry.split("/")[-1])
        # num_lines += sum(1 for line in open(entry.split("/")[-1]))
        with gzip.open(entry.split("/")[-1] , 'rb') as f:
            file_content = f.read()
            num_lines += sum(1 for line in f)
    return num_lines

def find_unloaded_batches(table, starttime, endtime):   # Find all batches marked as 'error' in specific table and time
    dynamodb = boto3.resource('dynamodb')                                                                   # DynamoDB connections
    table = dynamodb.Table(table)                                                # DynamoDB connections
    function_return = []                                                                                    # List to store files paths
    try:
        response = table.query(
            IndexName='LambdaRedshiftBatchStatus',
            KeyConditionExpression=Key('status').eq('error') & Key('lastUpdate').between(args.starttime, args.endtime),
            ProjectionExpression="entries"
            )
        for i in response[u'Items']:
            for x in i[u'entries']:
                # print x
                function_return.append (x)
        return sorted(function_return)
    except Exception as e:
        print("Exception in find_unloaded_batch: ",e)

def process_unloaded_batch(batch):  # Download all files from a single batch and query those in RS
    cur = conn.cursor()                                                                                     # Redshift connection
    s3 = boto3.resource('s3')                                                                               # boto3 s3 connection
    os.system("rm -rf temp/batch_count/*")
    for entry in batch:
        print "Entry typ: ", type(entry), entry
        print entry.split("/")[7], entry.split("/")[-1]
        s3.meta.client.download_file('%s','%s' 'temp/') %(entry.split("/")[7], entry.split("/")[-1])        # bucket, file, destination
        # with gzip.open(entry.split("/")[-1], 'rb') as f:                                                    # Gunzip and ...
            # file_content = f.read()                                                                         # ...read
        # Select first, last and two random lines ...

def check_if_batch_loaded(batch, cluster):
    for entry in batch:
        print "Entry typ: ", type(entry), entry
        print entry.split("/")[7], entry.split("/")[-1]
        s3.meta.client.download_file('%s','%s' 'temp/') %(entry.split("/")[7], entry.split("/")[-1])
        # num_lines += sum(1 for line in open(entry.split("/")[-1]))
        with gzip.open(entry.split("/")[-1] , 'rb') as f:
            file_content = f.read()
            num_lines += sum(1 for line in f)
            json_object = json.loads(line)
            query = "select count(request) \
            from proxy_logs where customerId = '%s' and guid = '%s' \
            and startutcinms = %s \
            and connectionId = '%s' " % (json_object['customerId'], json_object['guid'], json_object['startUtcInMs'], json_object['connectionId'])
            break

    cur.execute(query)
    result = cur.fetchall()
    functionOutput = 999
    for row in result:
        functionOutput = row[0]
    return functionOutput


# APP ROUTER
if args.task == 'clusterReport': # Report cluster status to stdout
    print "Running report for production clusters:"
    primary = query_redshift_for_rows_count('primary', args.starttime, args.endtime)
    secondary = query_redshift_for_rows_count('secondary', args.starttime, args.endtime)
    output = ''
    for x,y in zip(primary, secondary):
        date = str(x[0])
        primary = x[1]
        secondary = y[1]
        diff = 0
        diff = primary - secondary
        proxyname = x[2]
        if primary >= secondary:
            percentage = (float(diff) * 100) / float(primary)
        else:
            percentage = (float(diff) * 100) / float(secondary)
        # message +1 DATE + PROXYNAME + PRIMARY_COUNT + SECONDARY_COUNT + DIFFERENCE + DIFFERENCE_PERCENTAGE
        output += str(x[0]) + " " + proxyname + " " + str(primary) + " " + str(secondary) + " " + str(diff) + " " + str(round(percentage,3)) + "\n"
        print output

elif args.task == 'countRows': # Count rows in a batch
    files_in_batch = find_files_in_batch( cfg[args.cluster]['dynamoTable'], args.batchId, cfg[args.cluster]['s3prefix'])
    print "Rows in batch:", download_and_count(files_in_batch)

elif args.task == 'showUnloaded': # Show unloaded batches and count rows
    # print "Showing unloaded batches"
    batch_list = find_unloaded_batches(cfg[args.cluster]['dynamoTable'], args.starttime, args.endtime)

elif args.task == 'checkIfLoaded': # Check if batch has been already loaded

else:
    print "Else..."
