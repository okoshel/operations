#
# You need to set up your ~/.aws/config with region = <region> and ~/.aws/credentials with aws_access_key_id and aws_secret_access_key
# python redshift_cluster_report_diff.py 2ef 1454770847 1454770929
#
import os
import sys, getopt
import subprocess
import json
import re
from datetime import date, timedelta
# from email.mime.text import MIMEText
# import smtplib
import boto3
from boto3.dynamodb.conditions import Key, Attr

# TODO:

# Little helper
# def set_default(obj):
#     if isinstance(obj, set):
#         return list(obj)
#     raise TypeError

# Global vars:
BATCH_ID = sys.argv[1]
CLUSTER = sys.argv[2]

if CLUSTER == 'primary':
    TABLE_NAME = 'LambdaRedshiftBatchesProductionPrimary'
    S3PREFIX = 'wandera-redshift-data-live-eu-west-1/raw_access_log/year=*/month=*/day=*/hour=*/minute=*'
elif CLUSTER == 'secondary':
    TABLE_NAME = 'LambdaRedshiftBatchesProductionSecondary'
    S3PREFIX = 'wandera-redshift-data-live-eu-west-secondary/raw_access_log/year=*/month=*/day=*/hour=*/minute=*'
else:
    sys.exit("No cluster name provided!")

REGION = "us-west-1"

def find_all_files(tablename, batchId, s3prefix):
    dynamodb = boto3.resource('dynamodb') # Get the service resource
    table = dynamodb.Table(tablename)
    function_return = []
    try:
        response = table.query(
            KeyConditionExpression=Key('s3Prefix').eq(s3prefix) & Key('batchId').eq(batchId),
            )
        for i in response[u'Items']:
            for x in i['entries']:
                function_return.append (x)
        return function_return
    except Exception as e:
        print("Exception",e)

def download_and_count(batch):
    os.system("rm -rf temp/batch_count/*")
    for i in batch:
        filename_full =  "primary_" + i.split("/")[2] + i.split("/")[3] + i.split("/")[4] + i.split("/")[5] + i.split("/")[6] + "_" + i.split("/")[-1] 
        os.system("s3cmd get s3://%s temp/batch_count/ --force -q" % (i))
        print "downloaded:", filename_full
    count_lines = "gzcat temp/batch_count/* | wc -l"
    output = subprocess.check_output(count_lines, shell=True)
    # print "Total lines:", output
    return output

files_list = find_all_files(TABLE_NAME, BATCH_ID, S3PREFIX)
print download_and_count(files_list)
