#
# Usage:
#
# Report compare cluster diff redshift_cluster_report.py --table=proxy_logs --start=2016-02-01-16-00 --end=2016-02-01-18-00
#
import yaml
import argparse
import psycopg2
# from datetime import date, timedelta

parser = argparse.ArgumentParser()
parser.add_argument('--table', help='proxy_logs or maybe dynamicFirewallLogs', type=str, required=False, default='proxy_logs')
parser.add_argument('--start', help='start time in: YYYY-MM-DD-HH-MM-SS', type=str, required=True)
parser.add_argument('--end', help='start time in: YYYY-MM-DD-HH-MM-SS', type=str, required=True)
parser.add_argument('--env', help='development, qa, staging, integration, production', type=str, required=False, default='staging')
parser.add_argument('--role', help='primary or secondary', type=str, required=False, default='primary')
parser.add_argument('--interval', help='minute, hour, day', type=str, required=False, default='day')
args = parser.parse_args()

try: # Load config
    with open("config/config.yml", 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
except Exception as e:
    print "Couldn't load configuration !", e

def get_rows(cluster, env, start, end, interval, table):

    query = """SELECT DATE_TRUNC('%s', DATEADD(ms, startutcinms, '1970-01-01')) AS %s, COUNT(*) AS requests 
            FROM %s 
            WHERE startutcinms between datediff(ms,'1970-1-1','%s') and datediff(ms,'1970-1-1','%s')
            GROUP BY %s ORDER BY %s ASC; """ %(interval, interval, table, start, end, interval, interval)
    print query
    
    # Redshift connection 
    rs_connection = psycopg2.connect(host=cfg[args.env][args.role]['host'], 
    database=cfg[args.env][args.role]['db'], user=cfg[args.env][args.role]['user'],
    password=cfg[args.env][args.role]['pass'], port=cfg[args.env][args.role]['port'])

    rs_cursor = rs_connection.cursor()
    rs_cursor.execute(query)
    query_result = rs_cursor.fetchall()
    
    return query_result

if args.env == 'production':
    print "Enviroment is prouction. Getting diff"
    primary_rows_count = get_rows('primary', args.env, args.start, args.end, args.interval, cfg[args.env][args.role]['table'])
    secondary_rows_count = get_rows('secondary', args.env, args.start, args.end, args.interval, cfg[args.env][args.role]['table'])
    
else:
    print "Running query for env %s..." %(args.env)
    primary_rows_count = get_rows(args.env, args.start, args.end, args.interval, cfg[args.env][args.role]['table'])
    
    for el in primary_rows_count:
        print "%s - %s" %(el[0], el[1])
