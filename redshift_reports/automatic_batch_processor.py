# Work in progress! 
#
# Usage:
#
# Query for unloaded batches: abp.py --task=batchStatus --cluster=primary --starttime=2016-02-01-16-00 --endtime=2016-02-01-18-00 --countRows=true/false
# Count rows number in batch: abp.py --task=countRows --cluster=primary --batchId=<batchId>
# Was batch already loaded: abp.py --task=isBatchLoaded --cluster=primary --batchId=<batchId>
# Report cluster difference: abp.py --task=clusterReport --output=cli/email --proxyReport=false/true --starttime=2016-02-01-16-00 --endtime=2016-02-01-18-00
# Reload batch: abp.py --task=loadBatch --batchId=<batchId> --dryrun=true/false
#
import os
import subprocess
import json
import yaml
import boto3
import gzip
import argparse
import psycopg2
# import datetime
from datetime import date, timedelta
from boto3.dynamodb.conditions import Key, Attr

parser = argparse.ArgumentParser()
parser.add_argument('--cluster', help='staging, primary, secondary', required=True)
parser.add_argument('--starttime', help='start time in: YYYY-MM-DD-HH-MM-SS', type=int)
parser.add_argument('--endtime', help='start time in: YYYY-MM-DD-HH-MM-SS', type=int)
parser.add_argument('--batchId', help='batchId', type=str)
parser.add_argument('--dryrun', help='dryrun', default='true', required=True)
args = parser.parse_args()

try:                                                                                                        # Load config
    with open("config/config.yml", 'r') as ymlfile:
        cfg = yaml.load(ymlfile)
except Exception as e:
    print "Couldn't load configuration !", e

conn = psycopg2.connect(host=cfg[args.cluster]['host'],                                                     # Redshift connection
                        database=cfg[args.cluster]['db'], user=cfg[args.cluster]['user'],
                        password=cfg[args.cluster]['pass'],port=cfg[args.cluster]['port'])

def find_unloaded_batches():   # Find all batches marked as 'error' in specific table and time
    dynamodb = boto3.resource('dynamodb')                                                                   # DynamoDB connections
    table = dynamodb.Table(cfg[args.cluster]['dynamoTable'])                                                # DynamoDB connections
    function_return = []                                                                                    # List to store files paths
    try:
        response = table.query(
            IndexName='LambdaRedshiftBatchStatus',
            KeyConditionExpression=Key('status').eq('error') & Key('lastUpdate').between(args.starttime, args.endtime),
            ProjectionExpression="entries"
            )
        for i in response[u'Items']:
            for x in i[u'entries']:
                # print x
                function_return.append (x)
        return sorted(function_return)
    except Exception as e:
        print("Exception in find_unloaded_batch: ",e)

def process_unloaded_batch(batch):  # Download all files from a single batch and query those in RS
    cur = conn.cursor()                                                                                     # Redshift connection
    s3 = boto3.resource('s3')                                                                               # boto3 s3 connection

    for entry in batch:
        print "Entry typ: ", type(entry), entry
        print entry.split("/")[7], entry.split("/")[-1]
        s3.meta.client.download_file('%s','%s' 'temp/') %(entry.split("/")[7], entry.split("/")[-1])        # bucket, file, destination
        # with gzip.open(entry.split("/")[-1], 'rb') as f:                                                    # Gunzip and ...
            # file_content = f.read()                                                                         # ...read
        # Select first, last and two random lines ...

def count_rows(batchId):
    dynamodb = boto3.resource('dynamodb')                                                                   # Get the service resource
    table = dynamodb.Table(cfg[args.cluster]['dynamoTable'])
    files_list = []
    try:
        response = table.query( KeyConditionExpression=Key('s3Prefix').eq(cfg[args.cluster]['s3Prefix']) & Key('batchId').eq(batchId))
        for i in response[u'Items']:
            for x in i['entries']:
                files_list.append (x)
    except Exception as e:
        print("Exception in DynamoDB query",e)

    os.system("rm -rf temp/batch_count/*") # clean folder for new download
    for i in files_list:
        os.system("s3cmd get s3://%s temp/batch_count/ --force -q" % (i))
    count_lines = "gzcat temp/batch_count/* | wc -l"
    output = subprocess.check_output(count_lines, shell=True)
    # print "Total lines:", output
    return output

result = find_unloaded_batches()
process_unloaded_batch(result)
