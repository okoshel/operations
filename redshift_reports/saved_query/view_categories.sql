SELECT
    date_trunc('day', DATEADD(ms, startUtcInMs, '1970-01-01')) AS localdatetime,
    category,
    SUM(dataTransferred) AS dataUsed,
    SUM(CASE
        WHEN ruleBlock = true THEN 1
        ELSE 0
    END) AS blocksCount
FROM
    proxy_logs
WHERE
    proxy_logs.customerid IN ('%(customer)s')
    AND startutcinms >= Datediff(ms, '1970-1-1', '%(start)s')
    AND startutcinms < Datediff(ms, '1970-1-1', '%(end)s')
    AND dataTransferred > 0
    AND (ipDisposition IS NULL
            OR ipDisposition = ''
            OR ipDisposition = 'CELLULAR'
            OR (
                ipDisposition = 'UNKNOWN'
                AND activePolicy IN ('LOCAL', 'ROAMING', 'ROAMING-INACTIVE')
            )
    )
    AND dataSource NOT IN ('APPCOUNTERS', 'BLOCKED_HANDSHAKE', 'VPN_ON_DEMAND')
    AND category NOT IN ('-', '')
GROUP BY
    localdatetime,
    category
ORDER BY
    localdatetime ASC,
    dataUsed DESC