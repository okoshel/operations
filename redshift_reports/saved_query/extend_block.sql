SELECT Date_trunc('day', Dateadd(ms, startutcinms + utcoffsetms, '1970-01-01')) AS localdatetime,
       guid,
       user_email,
       phonenumber,
       rule,
       category,
       roaming
FROM   proxy_logs
       LEFT join userdevice
               ON userdevice._id = proxy_logs.guid
WHERE  proxy_logs.customerid IN ('%(customer)s')
       AND startutcinms >= Datediff(ms, '1970-1-1', '%(start)s')
       AND startutcinms < Datediff(ms, '1970-1-1', '%(end)s')
       AND ruleblock = TRUE
       AND ( ipdisposition IS NULL
              OR ipdisposition = ''
              OR ipdisposition = 'CELLULAR'
              OR ( ipdisposition = 'UNKNOWN' AND activepolicy IN( 'LOCAL', 'ROAMING', 'ROAMING-INACTIVE' ))
           )
GROUP  BY localdatetime,
          guid,
          user_email,
          phonenumber,
          rule,
          category,
          roaming;