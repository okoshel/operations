SELECT Date_trunc('second', Dateadd(ms, createdutcms, '1970-01-01')) AS "Timestamp",
       raweventid                                                    AS id, 
       eventtype                                                     AS "Event type",
       userdevice.user_email                                         AS "Email", 
       userdevice._id                                                AS "GUID",
       hardwarespec_platform || hardwarespec_version || ' (' || info_device_devicesystemversion || ')' AS "Device",
       CASE 
         WHEN riskindex >= 0.0 
              AND riskindex < 0.2 THEN 'low' 
         WHEN riskindex >= 0.2 
              AND riskindex < 0.4 THEN 'medium' 
         WHEN riskindex >= 0.4 
              AND riskindex < 0.6 THEN 'high' 
         WHEN riskindex >= 0.6 THEN 'highest' 
       END                                                           AS "Severity",
       CASE 
         WHEN blocked = TRUE THEN 'Blocked'
         ELSE 'Detected' 
       END                                                           AS "Action" 
       , 
       CASE 
         WHEN wifi = TRUE THEN 'wifi' 
         ELSE 'cellular' 
       END                                                           AS "interface"
FROM   secure_event_log 
       LEFT JOIN userdevice
               ON userdevice._id = secure_event_log.deviceid 
WHERE  secure_event_log.customerid IN ('%(customer)s')
       AND createdutcms >= Datediff(ms, '1970-1-1', '%(start)s')
       AND createdutcms < Datediff(ms, '1970-1-1', '%(end)s')
       AND ( ( riskindex >= 0.4 
               AND riskindex < 0.6 ) 
              OR ( riskindex >= 0.6 
                   AND riskindex < 0.8 ) 
              OR ( riskindex >= 0.8 
                   AND riskindex <= 1.0 ) 
              OR ( riskindex >= 0.2 
                   AND riskindex < 0.4 ) 
              OR ( riskindex >= 0.0 
                   AND riskindex < 0.2 ) ) 
ORDER  BY createdutcms ASC;