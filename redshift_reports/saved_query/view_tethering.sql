SELECT localdatetime, 
       user_email      AS user_email,
       ss.guid         AS guid, 
       SUM(counters)   AS appData, 
       SUM(proxy)      AS proxyData, 
       SUM(perdaydata) AS totalData, 
       CASE 
         WHEN nl.eventtype = 'TETHERING_ACTIVE' THEN 'TRUE' 
         ELSE 'FALSE' 
       END             AS wasTethering, 
       Count(*)        AS tetherEvents 
FROM   userservice_reportable_event_log nl 
       join (SELECT pl.customerid, 
                    pl.guid, 
                    Date_trunc('day', Dateadd(ms, pl.startutcinms + pl.utcoffsetms, '1970-01-01')) AS localdatetime,
                    SUM(pl.datatransferred)                                                        AS perDayData,
                    SUM(CASE WHEN pl.datasource = 'APPCOUNTERS' THEN pl.datatransferred
                          ELSE 0 END)                                                              AS counters,
                    SUM(CASE WHEN pl.datasource = 'PROXY' THEN pl.datatransferred
                          ELSE 0 END)                                                              AS proxy,
                    ud.user_email                                                                  AS user_email
             FROM   proxy_logs pl 
             INNER JOIN userdevice ud
                      ON pl.guid = ud._id 
             WHERE  pl.customerid IN ('%(customer)s')
                    AND Dateadd(ms, pl.startutcinms + pl.utcoffsetms, '1970-01-01') >= '%(start)s'
                    AND Dateadd(ms, pl.startutcinms + pl.utcoffsetms, '1970-01-01') < '%(end)s'
             GROUP  BY localdatetime, 
                       pl.customerid, 
                       pl.guid, 
                       user_email
             ORDER  BY localdatetime DESC) AS ss 
         ON nl.customerid = ss.customerid 
            AND nl.deviceguid = ss.guid 
            AND Date_trunc('day', Dateadd(ms, nl.createdutcms + nl.utcoffsetms, '1970-01-01')) = ss.localdatetime
WHERE  nl.customerid IN ('%(customer)s')
       AND nl.eventtype = 'TETHERING_ACTIVE' 
       AND Dateadd(ms, nl.createdutcms + nl.utcoffsetms, '1970-01-01') >= '%(start)s'
       AND Dateadd(ms, nl.createdutcms + nl.utcoffsetms, '1970-01-01') < '%(end)s'
GROUP  BY localdatetime, 
          ss.user_email,
          guid, 
          wastethering 
ORDER  BY localdatetime DESC; 