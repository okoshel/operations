SELECT
    date_trunc('second', DATEADD(ms, startUtcInMsRaw, '1970-01-01')) as eventdate,
    allowanceBreached,
    capBreached,
    capException,
    capExceptionRule,
    carrierName,
    category,
    datatransferred,
    extra,
    guid,
    "group",
    ipDisposition,
    method,
    proxyErrors,
    request,
    roaming,
    rule,
    ruleBlock,
    secureEnabled,
    sourceIp,
    status,
    tariffZoneId,
    userAgent
FROM
    proxy_logs
WHERE
    guid IN ('%(device)s')
    AND startUtcInMs >= DATEDIFF(ms, '1970-01-01', '%(start)s')
    AND startUtcInMs <= DATEDIFF(ms, '1970-01-01', '%(end)s')
ORDER BY eventdate ASC;