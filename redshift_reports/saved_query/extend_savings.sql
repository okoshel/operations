SELECT
      customerName,
      countryCode,
      carrierName,
      localdatetime,
      SUM(perDayData) AS totalData,
      SUM(CASE WHEN isRoamingDay=0 THEN perDayData ELSE 0 END) AS domesticData,
      SUM(CASE WHEN isRoamingDay=0 THEN perDaySavingsData ELSE 0 END) AS domesticDataSaved,
      SUM(CASE WHEN isRoamingDay=1 THEN perDayData ELSE 0 END) AS roamingData,
      SUM(CASE WHEN isRoamingDay=1 THEN perDaySavingsData ELSE 0 END) AS roamingDataSaved,
      SUM(CASE WHEN isRoamingDay=0 THEN 1 ELSE 0 END) AS domesticDevices,
      SUM(CASE WHEN isRoamingDay=0 THEN capSavingsData ELSE 0 END) AS capSavingsDataDomestic,
      SUM(CASE WHEN isRoamingDay=0 THEN compressionSavingsData ELSE 0 END) AS compressionSavingsDataDomestic,
      SUM(CASE WHEN isRoamingDay=0 THEN blockSavingsData ELSE 0 END) AS blockSavingsDataDomestic,
      SUM(CASE WHEN isRoamingDay=1 THEN capSavingsData ELSE 0 END) AS capSavingsDataRoaming,
      SUM(CASE WHEN isRoamingDay=1 THEN compressionSavingsData ELSE 0 END) AS compressionSavingsDataRoaming,
      SUM(CASE WHEN isRoamingDay=1 THEN blockSavingsData ELSE 0 END) AS blockSavingsDataRoaming,
      SUM(isRoamingDay) AS roamingDevices,
      SUM(CASE WHEN isRoamingDay=1 THEN compressibleData ELSE 0 END) as compressibleRoamingData,
      SUM(CASE WHEN isRoamingDay=0 THEN compressibleData ELSE 0 END) as compressibleDomesticData
FROM
    (SELECT
        SUM(CASE WHEN (pl.method='CONNECT' OR pl.datasource='APPCOUNTERS' ) THEN 0 ELSE pl.datatransferred + pl.sizeReduction END) AS compressibleData,
        (CASE WHEN pl.roaming=true THEN 1 ELSE 0 END) AS isRoamingDay,
        UPPER(pl.country) AS countryCode,
        SUM(pl.datatransferred) AS perDayData,
        SUM(pl.sizeReduction) AS perDaySavingsData,
        SUM(pl.sizeReduction) AS compressionSavingsData,
        SUM(pl.blocksavings) AS blockSavingsData,
        SUM(pl.capsavings) AS capSavingsData,
        c.name AS customerName,
        pl.carrierName AS carrierName,
        DATE_TRUNC('day', DATEADD(ms, pl.startutcinms + pl.utcOffsetMs, '1970-01-01')) AS localdatetime
      FROM proxy_logs pl
      INNER JOIN customer c
              ON pl.customerId = c._id
      WHERE
        pl.customerId in ( '%(customer)s' )
        AND DATEADD(ms, pl.startutcinms, '1970-01-01') >= '%(start)s'
        AND DATEADD(ms, pl.startutcinms, '1970-01-01') < '%(end)s'
      GROUP BY UPPER(pl.country),
            pl.guid,
            localdatetime,
            pl.roaming,
            customerName,
            carrierName
    )
GROUP BY
    countryCode,
    customerName,
    carrierName,
    localdatetime
ORDER BY totalData DESC;