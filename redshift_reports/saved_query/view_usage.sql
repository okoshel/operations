SELECT guid,
       user_email,
       phonenumber,
       date_trunc('day', DATEADD(ms, startUtcInMs, '1970-01-01')) as eventdate,
       SUM(datatransferred) AS AlldataUsed,
       SUM(CASE
             WHEN datasource = 'APPCOUNTERS' THEN datatransferred
             ELSE 0
           END)             AS nonWebDataUsed
FROM   proxy_logs
LEFT JOIN userdevice
           ON userdevice._id = proxy_logs.guid
WHERE  proxy_logs.customerid IN ('%(customer)s')
       AND startutcinms >= Datediff(ms, '1970-1-1','%(start)s')
       AND startutcinms < Datediff(ms, '1970-1-1', '%(end)s')
       AND ( 1 = 2
              OR ( ipdisposition IS NULL
                    OR ipdisposition = ''
                    OR ipdisposition = 'CELLULAR'
                    OR ( ipdisposition = 'UNKNOWN' AND activepolicy IN ( 'LOCAL', 'ROAMING', 'ROAMING-INACTIVE' ) )
              )
           )
GROUP  BY guid,
          user_email,
          phonenumber,
          eventdate
ORDER  BY AlldataUsed DESC;