SELECT
    localdatetime,
    SUM(datatransferred)                                                        AS totalData,
    SUM(CASE WHEN rulename IN ( 'ActiveSync' ) THEN datatransferred ELSE 0 END) AS businessData,
    SUM(CASE WHEN rulename IN ( 'iCloud', 'Tumblr', 'Other Video',
                            'Twitter',
                            'Box', 'YouTube - Video', 'Snapchat',
                            'LinkedIn',
                            'Pinterest', 'Adult', 'Instagram',
                            'Dropbox',
                            'Audio', 'Facebook' ) THEN datatransferred
         ELSE 0 END)                                                            AS personalData,
    SUM(CASE
         WHEN rulename IN ( 'Maps/Location Services', 'App Downloads','O/S Updates',
                            'Security','Advertising' ) THEN datatransferred
         ELSE 0 END)                                                            AS otherData
FROM (
        SELECT
            date_trunc('day', DATEADD(ms, startUtcInMs, '1970-01-01')) AS localdatetime,
            ( CASE WHEN rule = '-' THEN site ELSE rule END )   AS ruleName,
            SUM(datatransferred)                               AS dataTransferred
        FROM   proxy_logs
        WHERE  proxy_logs.customerid IN ('%(customer)s')
               AND startutcinms >= Datediff(ms, '1970-1-1', '%(start)s')
               AND startutcinms < Datediff(ms, '1970-1-1', '%(end)s')
               AND userguid IS NOT NULL
               AND userguid != ''
               AND datasource NOT IN ( 'APPCOUNTERS', 'BLOCKED_HANDSHAKE','VPN_ON_DEMAND' )
               AND ( ipdisposition IS NULL
                      OR ipdisposition = ''
                      OR ipdisposition = 'CELLULAR'
                      OR ( ipdisposition = 'UNKNOWN'
                           AND activepolicy IN ( 'LOCAL', 'ROAMING','ROAMING-INACTIVE')
                         )
                  )
        GROUP BY
            localdatetime,
            ( CASE
              WHEN rule = '-' THEN site
              ELSE rule
            END )
    )
GROUP BY
    localdatetime
ORDER BY localdatetime ASC