SELECT     user_email,
           country,
           (timestamp 'epoch' +(Min(startutcinms)/1000) * interval '1 Second ')AS enter_date,
           (timestamp 'epoch' +(max(startutcinms)/1000) * interval '1 Second ')AS exit_date,
           SUM(
           CASE
                      WHEN datasource = 'PROXY' THEN (datatransferred )
                      ELSE 0
           END) AS web_data_used,
           SUM(
           CASE
                      WHEN datasource = 'APPCOUNTERS' THEN (datatransferred )
                      ELSE 0
           END) AS non_web_data_used
FROM       proxy_logs
LEFT JOIN userdevice
           ON         userdevice._id = proxy_logs.guid
WHERE      proxy_logs.customerid IN ('%(customer)s')
           AND        roaming = TRUE
           AND startutcinms >= Datediff(ms, '1970-1-1','%(start)s')
           AND startutcinms < Datediff(ms, '1970-1-1', '%(end)s')
GROUP BY   guid,
           country,
           userdevice.user_email
ORDER BY   userdevice.user_email;