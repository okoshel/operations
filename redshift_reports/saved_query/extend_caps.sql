SELECT localdatetime, 
       guid, 
       totaldata, 
       user_email, 
       phonenumber, 
       datathresholdlimit, 
       datathresholdtype, 
       datathresholdbreached 
FROM   ((SELECT Dateadd(ms, Min( startutcinms ), '1970-01-01')               AS localDateTime,
                SUM(datatransferred)                                         AS totalData,
                guid, 
                user_email, 
                phonenumber, 
                datathresholdsessionid, 
                datathresholdlimit, 
                datathresholdtype, 
                ( CASE 
                    WHEN capbreached THEN 'CAP' 
                    ELSE 'ALLOWANCE' 
                  END )                                                      AS dataThresholdBreached
         FROM   proxy_logs 
                LEFT JOIN userdevice
                        ON userdevice._id = proxy_logs.guid 
         WHERE  proxy_logs.customerid = '%(customer)s'
                AND startutcinms >= Datediff(ms, '1970-1-1','%(start)s')
                AND startutcinms < Datediff(ms, '1970-1-1', '%(end)s')
                AND activepolicy = 'LOCAL' 
                AND datathresholdsessionid IS NOT NULL 
                AND ( capbreached = TRUE OR allowancebreached = TRUE )
         GROUP  BY guid,
                   user_email, 
                   phonenumber, 
                   datathresholdbreached, 
                   datathresholdsessionid, 
                   datathresholdlimit, 
                   datathresholdtype)) 
ORDER  BY localdatetime DESC, 
          guid; 