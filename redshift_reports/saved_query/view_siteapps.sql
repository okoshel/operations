SELECT appname,
       contentcategory,
       SUM(dataused)             AS dataUsed,
       Count(DISTINCT guid)      AS deviceCount,
       SUM(roamingdataused)      AS roamingDataUsed,
       SUM(localdataused)        AS localDataUsed,
       SUM(capexceptiondataused) AS capExceptionDataUsed,
       SUM(blockscount)          AS blocksCount
FROM   (SELECT guid,
               category             AS contentCategory,
               ( CASE
                   WHEN rule IN( 'Productivity', 'Entertainment', 'Finance',
                                 'Other Video',
                                 'Other Cloud Storage', '-', 'Travel',
                                 'Technology',
                                 'Games', 'Lifestyle', 'Content Servers',
                                 'Shopping',
                                 'Audio', 'Advertising', 'News & Sport',
                                 'Other Social'
                                 ,
                                 'Communication', 'Business & Industry' ) THEN site
                   ||( CASE WHEN Coalesce(tld, '') != '' THEN '.' || tld ELSE '' END ) ELSE rule
                 END )              AS appName,
               SUM(datatransferred) AS dataUsed,
               SUM(CASE WHEN roaming = TRUE THEN datatransferred ELSE 0 END) AS roamingDataUsed,
               SUM(CASE WHEN roaming = FALSE THEN datatransferred ELSE 0 END) AS localDataUsed,
               SUM(CASE WHEN capexception = TRUE THEN datatransferred ELSE 0 END) AS capExceptionDataUsed,
               SUM(CASE WHEN ruleblock = TRUE THEN 1 ELSE 0 END) AS blocksCount
        FROM   proxy_logs
        WHERE  customerid IN ('%(customer)s')
            and startUtcInMs >= datediff(ms, '1970-1-1', '%(start)s')
            and startUtcInMs < datediff(ms, '1970-1-1', '%(end)s')
               AND datatransferred > 0
               AND ( 1 = 2
                      OR ( Coalesce(ipdisposition, '') IN( 'CELLULAR', '' )
                            OR ( ipdisposition = 'UNKNOWN'
                                 AND activepolicy IN( 'LOCAL', 'ROAMING',
                                                      'ROAMING-INACTIVE' )
                               ) ) )
               AND datasource NOT IN( 'APPCOUNTERS', 'BLOCKED_HANDSHAKE',
                                      'VPN_ON_DEMAND' )
        GROUP  BY guid,
                  appname,
                  category)
GROUP  BY appname,
          contentcategory
ORDER  BY dataused DESC,
          appname ASC;