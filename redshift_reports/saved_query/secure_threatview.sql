SELECT
    data.localdatetime AS localdatetime,
    CASE
          WHEN SUM(data.lastscore)IS NULL THEN '100'
          ELSE '100' - Round(Least(SUM(data.lastscore * data.weight) /(SUM(data.weight) + Greatest('0' - Count(data.lastscore), 0)),1) * '100')
    END                                 AS score,
    SUM(data.lastpersistentthreatcount) AS persistentthreat
FROM(
    SELECT   localdatetime,
           Round(Least(1, lastscore), 3) AS lastscore,
           lastpersistentthreatcount,
           '2.0'^(dense_rank() over (PARTITION BY localdatetime ORDER BY lastscore) + 1) AS weight
    FROM (
        SELECT   localdatetime,
                 deviceid,
                 first_value(score) over (PARTITION BY deviceid,localdatetime ORDER BY createdutcms DESC ROWS BETWEEN unbounded preceding AND unbounded following)                 AS lastscore,
                 first_value(persistentthreatcount) over (PARTITION BY deviceid,localdatetime ORDER BY createdutcms DESC ROWS BETWEEN unbounded preceding AND unbounded following) AS lastpersistentthreatcount
        FROM (
                SELECT date_trunc('day', convert_timezone('Europe/London', dateadd(ms, createdutcms, '1970-01-01'))) AS localdatetime,
                       createdutcms,
                       deviceid,
                       score,
                       persistentthreatcount
                FROM   risk_score_log
                WHERE  customerid IN ('%(customer)s')
                    AND createdUtcMs >= Datediff(ms, '1970-1-1', '%(start)s')
                    AND createdUtcMs < Datediff(ms, '1970-1-1', '%(end)s')
            )
         )
    GROUP BY deviceid,
           localdatetime,
           lastscore,
           lastpersistentthreatcount) data
GROUP BY localdatetime
ORDER BY localdatetime ASC