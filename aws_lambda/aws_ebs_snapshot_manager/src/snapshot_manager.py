import boto3  
import collections  
import datetime
import logging
import sys
import os
import re
import itertools

#Lambda Function Constants
AWS_REGION = 'eu-west-1'
# Lock to a single tld if required.
ENVIRONMENT = '(co.uk|biz|net)'
# If we want all env captures use .* or lock to ie. dev|stgn|core  
RE_NODE_AFFIX = '.*'
DEF_SNAPSHOT_SETTING = 'no'
DEF_SNAPSHOT_SCHEDULE = 'daily'
DEF_SNAPSHOT_RETENTION = 7


# Lambda Function Logger Settings
FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format=FORMAT)


# Please mention your region name
# below line code is call cross region
ec = boto3.client('ec2', region_name=AWS_REGION)


def get_aws_instance(filters):
    if filters is None:
        reservations = ec.describe_instances().get('Reservations', [])
    else:
        reservations = ec.describe_instances(
            Filters=filters).get('Reservations', [])
    return sum([[i for i in r['Instances']] for r in reservations], [])


def get_instance_tags(tagsreq, instanceobj):
    """Return tag values from the instance object"""
    return {t: tags['Value']
            for t in tagsreq
            for tags in instanceobj['Tags']
            if t == tags['Key']}


def tag_instance_keys(keys, instance):
    """Tag instance with k:v"""
    createtags = []
    for k, v in keys.iteritems():
        createtags.append({'Key': k, 'Value': v})
    ec.create_tags(Resources=[instance['InstanceId']], Tags=createtags)
    logger.info('Updated {0} with tags {1}'.format(instance['InstanceId'], createtags))


def main():
    # This is the call to get the instances we are interested with.
    # Describe any passed filters in the call.
    # Documented at http://boto3.readthedocs.io/en/latest/reference/
    # services/ec2.html#EC2.Client.describe_instances
    instances = get_aws_instance(filters=None)
    snapshottable_instances = []
    logger.info("Total Instances In {0}: {1}"
                .format(AWS_REGION, len(instances)))
    for instance in instances:
        for t in instance['Tags']:
            if t['Key'] == "Name":
                searchObj = re.search(r'(?P<nodename>.*-{0}\..*wandera\.{1})'
                                      .format(RE_NODE_AFFIX, ENVIRONMENT),
                                      t['Value'], re.I)
                if searchObj is not None:
                    snapshottable_instances.append(instance['InstanceId'])
                    nodeaffixes = re.search(r'(?P<nodeaffix>.*)\..*wandera\.(?P<tld>.*)',
                                            searchObj.group('nodename'), re.I)
                    if nodeaffixes is not None:
                        nodeaffix = nodeaffixes.group('nodeaffix')
                        nodetld = nodeaffixes.group('tld')
                    logger.info("NODE MATCHED: {0} {1} {2}".format(
                        searchObj.group('nodename'),
                        nodeaffix, nodetld))
                    # TODO: tidy up!
                    # lets set env k:v. It will overwrite regardless
                    if nodetld == 'biz' and 'stgn' in nodeaffix:
                        tag_instance_keys({'env': 'staging'}, instance)
                    elif nodetld == 'biz' and 'intg' in nodeaffix:
                        tag_instance_keys({'env': 'integration'}, instance)
                    elif nodetld == 'biz':
                        tag_instance_keys({'env': 'stgn/intg'}, instance)
                    elif nodetld == 'co.uk' and 'dev' in nodeaffix:
                        tag_instance_keys({'env': 'development'}, instance)
                    elif nodetld == 'co.uk' and 'qa' in nodeaffix:
                        tag_instance_keys({'env': 'qa'}, instance)
                    elif nodetld == 'co.uk':
                        tag_instance_keys({'env': 'dev/qa'}, instance)
                    elif nodetld == 'net' and 'cops' in nodeaffix:
                        tag_instance_keys({'env': 'operations'}, instance)
                    elif nodetld == 'net':
                        tag_instance_keys({'env': 'operations'}, instance)
                    else:
                        tag_instance_keys({'env': 'unknown'}, instance)

    logging.info("Identified Instances - {0} with length {1}".format(
                 snapshottable_instances, len(snapshottable_instances)))
    identified_instances = get_aws_instance(filters=[
        {'Name': 'instance-id', 'Values': snapshottable_instances}])
    for instance in identified_instances:
        logging.info("Running Tagging Check On Instance {0} and found {1}"
                     .format(instance['InstanceId'],
                             get_instance_tags(("env", "snapshot"), instance)))

if __name__ == '__main__':
    main()