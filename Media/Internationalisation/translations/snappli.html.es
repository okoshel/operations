<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Snappli</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />

<style type="text/css">

html {
    height: 100%;
}
body {
    height: 100%;
    margin: 0;
    background-repeat: no-repeat;
    background-attachment: fixed;

/* IE10 Consumer Preview */ 
background-image: -ms-linear-gradient(top, #F59000 0%, #F06F00 100%);

/* Mozilla Firefox */ 
background-image: -moz-linear-gradient(top, #F59000 0%, #F06F00 100%);

/* Opera */ 
background-image: -o-linear-gradient(top, #F59000 0%, #F06F00 100%);

/* Webkit (Safari/Chrome 10) */ 
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #F59000), color-stop(1, #F06F00));

/* Webkit (Chrome 11+) */ 
background-image: -webkit-linear-gradient(top, #F59000 0%, #F06F00 100%);

/* W3C Markup, IE10 Release Preview */ 
background-image: linear-gradient(to bottom, #F59000 0%, #F06F00 100%);
		}

#mydiv {
	width:320px;
	background-image:url(images/block_bg2.png);
	background-repeat:no-repeat;
        
}

.placeholder{
	text-align:center;
	color:#FFF;
	padding-top:200px;
	font-family:Arial, Helvetica, sans-serif;
	font-size:16px;
	font-style:italic;}

</style>
</head>

<body>

<div id="mydiv">

<div class="placeholder">
<p>Snappli te está protegiendo.<br/>
Este sitio presenta un riesgo de seguridad.<br/>
Se ha bloqueado para su seguridad.</p>
<p style="padding-top:30px;"><img src="images/logo.png" width="96" height="38" alt="Snappli"></p>
</div>


</div>

</body>
</html>
