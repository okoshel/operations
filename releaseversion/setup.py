from setuptools import setup, find_packages
from releaseversion import __version__

setup(
    name='wandera-releaseversion',
    version=__version__,
    description='Finds latest release version of individual services.',
    url='https://bitbucket.org/snappli-ondemand/operations/',
    author='Mark Pashby',
    author_email='mark.pashby@wandera.com',
    license='MIT',
    packages=find_packages(),
    zip_safe=True,
    entry_points={
        'console_scripts': [
            'releaseversion = releaseversion.__main__:main'
        ]
    }
)
