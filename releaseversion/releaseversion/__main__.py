def main():
    from releaseversion import ReleaseVersion
    ReleaseVersion().run()

if __name__ == '__main__':
    main()
