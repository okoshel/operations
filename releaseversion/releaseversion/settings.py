sw_services = ["apnservice_enterprise",
               "enterprise_connectorservice",
               "enterprise_filterservice",
               "enterprise_notificationservice",
               "enterprise_portal_cms",
               "enterprise_redirectionservice",
               "enterprise_ticketservice",
               "logservice_enterprise",
               "portal_enterprise",
               "profileservice2_enterprise",
               "proxy_enterprise",
               "proxyservice_enterprise",
               "reportservice3_enterprise",
               "userservice2_enterprise",
               "videoservice"]

S3_SW_BUCKET = "snappli-software-repo"
