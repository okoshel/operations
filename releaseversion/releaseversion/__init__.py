import logging
import re
import datetime
import os
import sys
from boto.s3.connection import S3Connection
from natsort import natsorted
from releaseversion import settings


__version__ = '0.0.1'


class ReleaseVersion(object):

    """ Release Version Object For Each Release Software"""

    def __init__(self, name, s3conn, blist, date=datetime.datetime.now()):

        self.swname = name
        self.date = date
        self.auth = s3conn
        softname = self.swname
        if softname == 'proxy_enterprise':
            softname = 'rabbit-enterprise'
        if softname == 'enterprise_connectorservice':
            softname = 'emm-connect'
        swregex = re.match('([a-z]*)_*([a-z]*)', softname)
        for sm in swregex.groups():
            if sm == 'enterprise' or sm == '':
                continue
            for dir in blist:
                if dir.name == 'releases/':
                    continue
                dirregex = re.match('releases\W([a-z]*)', dir.name)
                dirmatch = re.match('{}'.format(dirregex.groups()[0]), sm)
                if dirmatch:
                    self.releasedir = dir.name

    def setup_logging(self):
        """Configure the application logger."""
        logging.basicConfig(format='%(levelname)s %(message)s',
                            level=logging.INFO)

    def awss3_auth(self):
        """Perform Auth to S3 based on passed creds"""
        AWSKEY = os.environ.get('AWS_ACCESS_KEY_ID')
        AWSSECKEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
        if AWSKEY is None or AWSSECKEY is None:
            sys.exit("ERROR: You need to set AWS_ACCESS_KEY_ID"
                     "AND AWS_SECRET_ACCESS_KEY env var values")

    def get_latest_version(self, buc):
        """Lookup latest version of service from AWS s3bucket"""

        software_v_list = []
        vder = buc.list(self.releasedir)
        for key in vder:
            vregex = re.match(
                '.*_([0-9]*\.[0-9]*\.[0-9]*\.?[0-9]*)-([0-9]*).*', key.name)
            if vregex is not None:
                software_v_list.append(key.name)
        # TODO: Find standard lib way of doing this more easily.
        slist = natsorted(software_v_list)
        lversionnum = len(slist) - 1
        lvregex = re.search(
            r"_(?P<version>\w+\.\w+\.\w+(\.\w+){,1}(\-\w+){,2})_",
            slist[lversionnum])
        self.latestversion = lvregex.group('version')
        return self.latestversion

    def set_latest_version(self):
        """ set latest sw version to hieradata cfmgt defined file
            :returns: success bool
        """
