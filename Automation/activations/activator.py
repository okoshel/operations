from database_manager import DatabaseManager
from timezone_retriever import TimezoneCalculator

class AccountActivator(object):
    def __init__(self, database_manager):
        self.__db_manager = database_manager
        self.__tz_calc = TimezoneCalculator()

    def prevalidate_customer_activation(self, customer_name,
                                              parent_name,
                                              reseller_name,
                                              admin_name,
                                              admin_email,
                                              city,
                                              country,):
        """ Validates that the details of an activation are likely to work

        Performs some basic checks on the details of an activation to see
        if the activation will likely be a success.

        These checks include:
            1) Customer already exists
            2) Reseller exists
            3) Parent exists, if required
            4) Admin does not already exist
            5) Timezone information can be retrieved from city and country

        Args:
            customer_name: The name of the customer to activate.
            parent_name  : The name of the parent account for this customer, set to None if
                           no parent child relationship.
            reseller_name: The name of the reseller for this customer account.
            admin_name   : The name of the admin to be added to this account.
            admin_email  : The email address the account admin will use to login.
            city         : The city the account will be based in.
            country      : The ISO country code the account city is located in (eg 'gb').

        Returns:
            A dictionary containing two keys:
                "valid" : A boolean indicating if the activation information is considered valid.
                "error" : A string containing the description the reason the data in invalid.
        """
        if self.__db_manager.customer_exists(customer_name):
            # Customer already exists
            return { "valid" : False, "error" : "Customer already exists" }

        if not self.__db_manager.reseller_exists(reseller_name):
            # Reseller doesn't exist
            return { "valid" : False, "error" : "Reseller doesn't exist" }

        if parent_name:
            # Setting up a child account
            if not self.__db_manager.parent_exists(parent_name):
                # Parent doesn't exist
                return { "valid" : False, "error" : "Parent account doesn't exist" }

        if self.__db_manager.admin_exists(admin_email, admin_name):
            # Admin already exists
            return { "valid" : False, "error" : "Admin already exists" }

        try:
            timezone = self.__tz_calc.get_timezone(city, country)
        except TimezoneCalculator.CouldNotGetTimezone:
            return { "valid" : False, "error" : "Could not get timezone information" }

        # No error so valid
        return { "valid" : True, "error" : None }


    def activate_customer(self, customer_name,
                                parent_name,
                                reseller_name,
                                admin_name,
                                admin_email,
                                city,
                                country,
                                label):
        """ Creates a customer account in the DB and adds and admin to RADAR

        Creates a customer account in the DB along with all the required extras.
        Links to reseller and if required to parent accounts.

        Args:
            customer_name: The name of the customer to activate.
            parent_name  : The name of the parent account for this customer, set to None if
                           no parent child relationship.
            reseller_name: The name of the reseller for this customer account.
            admin_name   : The name of the admin to be added to this account.
            admin_email  : The email address the account admin will use to login.
            city         : The city the account will be based in.
            country      : The ISO country code the account city is located in (eg 'gb').
            label        : Any label text to add to this account.

        Returns:
            A dictionary with two keys:
                "activated"  : A boolean indicating the success state of the activation
                "error"      : A string giving a description of any error in activation
        """
        if prevalidate_customer_activation(customer_name,
                                           parent_name,
                                           reseller_name,
                                           admin_name,
                                           admin_email,
                                           city,
                                           country,
                                           label)["valid"]:
            # Activation is valid
            timezone = None
            try:
                # Get the timezone info, this should always succed as we have just done it
                # in the preactivation check
                timezone = self.__tz_calc.get_timezone(city, country)
            except TimezoneCalculator.CouldNotGetTimezone:
                return { "valid" : False, "error" : "Could not get timezone information" }

            # TODO activate customer
            if activated:
                return { "activated" : True, "error" : None }
            else:
                return { "activated" : False, "error" : "Failed to activate customer" }

    def __create_admin(self, admin_name, admin_email, account_name, role_type,
                             read_only, mdm_access):
        """ Creates an admin account in the database

        Args:
            admin_name   : The name of the admin
            admin_email  : The admins email address
            account_name : The name of the account this admin account will belong to
            role_type    : The type of admin to create
            read_only    : Is this a read-only admin account?
            mdm_access   : Is this an MDM access admin?

        Returns:
            A dictionary containing two keys:
                "created" : A boolean to indicated the creatation status
                "erorr"   : A string containing the description of any error
        """
        # TODO check the appropriate account exists, eg customer, reseller, parent
        # Check the customer exists
        if not self.__db_manager.customer_exists(customer_name):
            return { "created" : False, "error" : "Customer does not exist" }
        # Check the admin doesn't already exist
        if self.database_manager.admin_exists(admin_name, admin_email):
            return { "created" : False, "error" : "Admin already exists" }
        # TODO create admin account


    def create_customer_admin(self, admin_name, admin_email, customer_name, read_only = False, mdm_access = False):
        """ Creates a customer admin

        Creates an admin account with the following parameters:
            role_type : ADMIN

        Args:
            admin_name   : The name of the admin
            admin_email  : The admins email address
            customer_name: The name of the customer this admin account will belong to
            read_only    : Create an account with read-only access? Defaults to False
            mdm_access   : Create an account with MDM access? Defaults to False

        Returns:
            A dictionary containing two keys:
                "created" : A boolean to indicated the creatation status
                "erorr"   : A string containing the description of any error
        """
        return self.__create_admin(admin_name,
                                   admin_email,
                                   customer_name,
                                   "ADMIN",
                                   read_only,
                                   mdm_access)

    def create_reseller_admin(self, admin_name, admin_email, reseller_name, read_only = False, mdm_access = False):
        """ Creates a reseller admin

        Creates an admin account with the following parameters:
            role_type : RESELLER_ADMIN

        Args:
            admin_name   : The name of the admin
            admin_email  : The admins email address
            reseller_name: The name of the reseller this admin account will belong to
            read_only    : Create an account with read-only access? Defaults to False
            mdm_access   : Create an account with MDM access? Defaults to False

        Returns:
            A dictionary containing two keys:
                "created" : A boolean to indicated the creatation status
                "erorr"   : A string containing the description of any error
        """
        return self.__create_admin(admin_name,
                                   admin_email,
                                   reseller_name,
                                   "RESELLER_ADMIN",
                                   read_only,
                                   mdm_access)

    def create_parent_admin(self, admin_name, admin_email, parent_name, read_only = False, mdm_access = False):
        """ Creates a parent admin

        Creates an admin account with the following parameters:
            role_type : PARENT_ADMIN

        Args:
            admin_name : The name of the admin
            admin_email: The admins email address
            parent_name: The name of the parent this admin account will belong to
            read_only    : Create an account with read-only access? Defaults to False
            mdm_access   : Create an account with MDM access? Defaults to False

        Returns:
            A dictionary containing two keys:
                "created" : A boolean to indicated the creatation status
                "erorr"   : A string containing the description of any error
        """
        return self.__create_admin(admin_name,
                                   admin_email,
                                   reseller_name,
                                   "PARENT_ADMIN",
                                   read_only,
                                   mdm_access)

    def create_parent_admin_ro(self, admin_name, admin_email, parent_name):
        """ Helper function to create a read only parent admin """
        return self.create_parent_admin(admin_name, admin_email, parent_name, read_only = True)

    def create_reseller_admin_ro(self, admin_name, admin_email, reseller_name):
        """ Helper function to create a read only reseller admin """
        return self.create_parent_admin(admin_name, admin_email, reseller_name, read_only = True)

    def create_customer_admin_ro(self, admin_name, admin_email, customer_name):
        """ Helper function to create a read only customer admin """
        return self.create_parent_admin(admin_name, admin_email, customer_name, read_only = True)
