from pymongo import MongoReplicaSetClient

class DatabaseManager(object):
    """ Management class for database operations

    A wrapper class around mongodb database operations to provide easy to use fuctions
    for common database actions with regards to managing customer accounts

    Attributes:
        uri         : The uri to connect to the database
        replica_set : The name of the replica set to use
        username    : The username to connect with
        password    : The password for the specified username
        db_name     : The name of the db to use
        auth_db_name: The name of the authorisation db to use
    """
    def __init__(self, uri, replica_set, username, password, db_name, auth_db_name):
        """ Inits the DatabaseManager with the connection details and sets up
        some private local variables for the different collections that will be used
        """
        self.__client = MongoReplicaSetClient(uri, replicaSet = replica_set).db_name
        if not self.__client.authenticate(username, password, source=auth_db_name):
            print "Failed to authenticate"
            raise RuntimeError
        self.__reseller_collection = "reseller"
        self.__parent_collection = "parent"
        self.__customer_collection = "customer"
        self.__admin_collection = "admin"

    def reseller_exists(self, reseller_name):
        """ Checks if a reseller exists in the database

        Args:
            reseller_name : The name of reseller to look for
        Returns:
            A boolean indicating if the reseller exists
        """
        resellers = self.__client.self.__reseller_collection
        return resellers.find_one({'name':reseller_name }):

    def parent_exists(self, parent_name):
        """ Checks if a parent exists in the database

        Args:
            parent_name : The name of parent to look for
        Returns:
            A boolean indicating if the parent exists
        """
        parents = self.__client.self.__parent_collection
        return parents.find_one({'name':parent_name})

    def customer_exists(self, customer_name):
        """ Checks if a customer exists in the database

        Args:
            customer_name : The name of customer to look for
        Returns:
            A boolean indicating if the customer exists
        """
        customers = self.__client.self.__customer_collection
        return customers.find_one({'name':customer_name}):

    def admin_exists(self, admin_email, admin_name):
        """ Checks if an admin exists in the database

        Args:
            admin_email: The email address to look for
            admin_name : The name of admin to look for
        Returns:
            A boolean indicating if the admin exists
        """
        # TODO check security details
        admins = self.__client.self.__admin_collection
        return admins.find_one({'_id':admin_email}):
