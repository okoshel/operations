import requests
import sys
from optparse import OptionParser
from bs4 import BeautifulSoup

class TimezoneCalculator(object):
    class CityInfoNotFound(Exception):
        pass

    class MultipleCitiesFound(Exception):
        pass

    class TimezoneNotFound(Exception):
        pass

    class CouldNotGetTimezone(Exception):
        pass

    class NoJodaTimezoneFound(Exception):
        pass

    def __init__(self):
        self.__joda_timezones = self.__get_joda_timezones()


    def __get_joda_timezones(self):
        r = requests.get("http://joda-time.sourceforge.net/timezones.html")
        soup = BeautifulSoup(r.text, 'html.parser')
        table = soup.findAll('table')
        return self.__convert_joda_data_to_dict(table)

    def __convert_joda_data_to_dict(self, joda_table):
        joda_dict = {}
        for item in joda_table:
            rows = item.findAll('tr')
            for r in rows:
                cols = r.findAll('td')
                if cols:
                    # ignore the first as that is the time offset
                    # 2nd is the timezone
                    # 3rd is any aliases
                    timezone = str(cols[1].string)
                    aliases = cols[2].string
                    if aliases:
                        l_o_a = []
                        a = aliases.split(',')
                        for alias in a:
                          l_o_a.append(str(alias).strip())
                        aliases = l_o_a
                    joda_dict[timezone] = aliases
        return joda_dict

    def __get_joda_timezone(self, timezone):
        if timezone in self.__joda_timezones:
            # timezone already in joda format
            return timezone
        else:
            # check if we have an alias
            for tz in self.__joda_timezones:
                aliases = self.__joda_timezones[tz]
                if aliases:
                    if timezone in aliases:
                        return tz
        raise TimezoneCalculator.NoJodaTimezoneFound

    def __filter_city_results(self, results):
        #  u'types': [u'colloquial_area', u'locality', u'political']},
        filtered_results = []
        for result in results:
           if 'colloquial_area' in result["types"]:
               continue
           if 'sublocality' in result["types"]:
               continue
           else:
                filtered_results.append(result)
        return filtered_results

    def __get_city_location(self, city, country):
        address_payload = city + "," + country
        payload = { 'address' : address_payload, 'sensor' : 'false' }
        r = requests.get("http://maps.googleapis.com/maps/api/geocode/json", params=payload)
        if r.status_code == requests.codes.ok:
            city_info = r.json()
            if city_info["status"] == "OK":
                filtered_results = self.__filter_city_results(city_info["results"])
                if len(filtered_results) > 1:
                    import pprint
                    pprint.pprint(filtered_results)

                    raise TimezoneCalculator.MultipleCitiesFound
                else:
                    return filtered_results[0]["geometry"]["location"]
        raise TimezoneCalculator.CityInfoNotFound

    def __get_tz_from_lat_lon(self, lat, lon):
        location_payload = str(lat) + "," + str(lon)
        payload = { "location" : location_payload, "timestamp" : 0, "sensor" : "false" }
        r = requests.get("https://maps.googleapis.com/maps/api/timezone/json", params=payload)
        if r.status_code == requests.codes.ok:
            timezone_info = r.json()
            if timezone_info["status"] == "OK":
                return timezone_info["timeZoneId"]
        raise TimezoneCalculator.TimezoneNotFound


    def get_timezone(self, city, country):
        try:
            location = self.__get_city_location(city, country)
            timezone = self.__get_tz_from_lat_lon(location["lat"], location["lng"])
            return self.__get_joda_timezone(timezone)
        except (TimezoneCalculator.CityInfoNotFound, TimezoneCalculator.MultipleCitiesFound):
            raise TimezoneCalculator.CouldNotGetTimezone


if __name__ == '__main__':
    usage = "usage: %prog city country_code"
    parser = OptionParser(usage=usage)
    (options,args) = parser.parse_args()

    if len(args) != 2:
        print("Please supply 2 arguments, the city and the country code")
        sys.exit(1)

    try:
        tz_calc = TimezoneCalculator()
        tz = tz_calc.get_timezone(args[0], args[1])
        print tz
        sys.exit(0)
    except TimezoneCalculator.CouldNotGetTimezone:
        sys.exit(1)

