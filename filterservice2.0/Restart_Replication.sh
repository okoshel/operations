#!/bin/bash

#Used to restart mysql replication on a FIS node that has fallen
echo 'Downloading mysql files from s3:'
s3cmd get --recursive s3://us-static-secure-wandera-com/tmp/mtyas/mysql/ .

echo 'Unzipping files:'
gzip -d *.gz

echo 'Stopping slave and resetting master'
mysql -e 'STOP SLAVE'
mysql -e 'RESET MASTER'
mysql -e 'RESET SLAVE ALL'

echo 'Dropping old databases'
mysql -e 'drop database security_other;'
mysql -e 'drop database security_domains;'
mysql -e 'drop database security_ips;'
mysql -e 'drop database security_uris;'

mysql -e 'source /tmp/create_tables.sql'

echo 'Loading Data into mysql (This will take a while):'
mysql -e 'SET @@GLOBAL.wait_timeout=28000;'
mysql -e 'RESET MASTER'
echo 'Loading Security IPs'
mysql -uroot  security_ips < security_ips.sql
mysql -e 'RESET MASTER'
echo 'Loading Security Other'
mysql -uroot security_other < security_other.sql
mysql -e 'RESET MASTER'
echo 'Loading Security Domains'
mysql -uroot  security_domains < security_domains.sql
mysql -e 'RESET MASTER'
echo 'Loading Security URIs (This is the big one that can take 10+ minutes)'
mysql -uroot security_uris < security_uris.sql
mysql -e 'SET @@GLOBAL.wait_timeout=300;'

mysql -e "CHANGE MASTER TO MASTER_HOST='52.211.149.154',MASTER_USER='repl', MASTER_PASSWORD='q8RSyr3M6TE23uJhT5K4', MASTER_AUTO_POSITION=1;"

echo 'Configuring Master and enabling Slave node:'
mysql -e "START SLAVE;"
echo "=======================     Slave Status      ============================"
mysql -e "SHOW SLAVE STATUS\G"
echo "=======================     Master Status      ==========================="
mysql -e "SHOW MASTER STATUS\G"

echo 'Replication should have restarted. You may need to rerun mysql -e "SHOW SLAVE STATUS\G" to see the correct IO status'
