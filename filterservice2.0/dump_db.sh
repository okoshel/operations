#!/bin/bash

#Script to dump out each of the databases need to prime a new filter master or slave

mkdir tmp
cd tmp

mysql -e 'stop slave'
mysqldump --opt --add-drop-database security_ips > security_ips.sql
mysqldump --opt --add-drop-database security_other > security_other.sql
mysqldump --opt --add-drop-database security_domains > security_domains.sql
mysqldump --opt --add-drop-database security_uris > security_uris.sql
mysql -e 'start slave'

gzip *.sql

s3cmd put *.sql.gz s3://us-static-secure-wandera-com/tmp/mtyas/mysql/

cd ..
rm -rf tmp
