# Usage:
# python mongo2sql.py --mongodbname=securityDomains --mongocollection=securityDefinition --sqldbname=security_domains > securityDomains.sql
# python mongo2sql.py --mongodbname=securityIPs --mongocollection=securityDefinition --sqldbname=security_ips > securityIPs.sql
# python mongo2sql.py --mongodbname=securityURIs --mongocollection=securityDefinition --sqldbname=security_uris > securityURIs.sql
# python mongo2sql.py --mongodbname=securityOtherCollections --mongocollection=contentCollection --sqldbname=security_other --sqltablename=content_classification > content_classification.sql
# python mongo2sql.py --mongodbname=securityOtherCollections --mongocollection=osCveData --sqldbname=security_other --sqltablename=os_cve_data > os_cve_data.sql
# python mongo2sql.py --mongodbname=securityOtherCollections --mongocollection=osRiskIndex --sqldbname=security_other --sqltablename=os_risk_index > os_risk_index.sql
# python mongo2sql.py --mongodbname=securityOtherCollections --mongocollection=appMetaData --sqldbname=security_other --sqltablename=app_metadata > app_metadata.sql
# python mongo2sql.py --mongodbname=securityOtherCollections --mongocollection=sourceIpDisposition --sqldbname=security_other --sqltablename=source_ip_disposition > source_ip_disposition.sql
# python mongo2sql.py --mongodbname=securityOtherCollections --mongocollection=appUAMetadataLink --sqldbname=security_other --sqltablename=app_ua_metadata_link > app_ua_metadata_link.sql
# python mongo2sql.py --mongodbname=securityOtherCollections --mongocollection=effectiveAppMetadata --sqldbname=security_other --sqltablename=effective_app_metadata > effective_app_metadata.sql
# python mongo2sql.py --mongodbname=securityOtherCollections --mongocollection=fullAppMetadata --sqldbname=security_other --sqltablename=full_app_metadata > full_app_metadata.sql

import json
import argparse
from pymongo import MongoClient

parser = argparse.ArgumentParser()
parser.add_argument('--mongodbname', help='MongoDB collection name', required=True)
parser.add_argument('--mongocollection', help='MongoDB collection name, default = securityDefinition', required=False, default='securityDefinition')
parser.add_argument('--sqldbname', help='MySQL DB name', required=True)
parser.add_argument('--sqltablename', help='MySQL DB name', required=False, default = 'security_definition')
parser.add_argument('--querylimit', help='Limit MongoDB query - find().limit()', type=int, default=20000000, required=False)
parser.add_argument('--batchsize', help='Limit MongoDB query - find().limit()', type=int, default=1000,  required=False)
args = parser.parse_args()

collectionName = args.mongocollection
mongoDatabaseName = args.mongodbname
tableName = args.sqltablename
mysqlDatabaseName = args.sqldbname
queryLimit = args.querylimit
defintion_json = ''
total_docs = 0

# connect to mongoDB
mongoConnectionString = 'mongodb://filterService:qb81Vgq7gI73@52.17.180.138:21000/'+ mongoDatabaseName
client = MongoClient(mongoConnectionString)
db = client[mongoDatabaseName]
collectionName = db[collectionName]

def init_statement(tablename):
    print "USE `%s`;" %(mysqlDatabaseName)
    print "TRUNCATE TABLE `%s`;" %(tablename)
    print "LOCK TABLES `%s` WRITE;" %(tablename)

def print_content_with_cursor_batch_insert(collectionName,tablename):
    definition_json = ''
    count = 0
    batchSize = args.batchsize #1000 by default
    batchCounter = 0
    # If you on pymongo 3 or higher you need
    # cursor = collectionName.find({},no_cursor_timeout=True).limit(queryLimit)
    # For pymongo below 3.0
    cursor = collectionName.find({},timeout=True).limit(queryLimit)
    # total_docs = collectionName.find().count()
    total_docs = cursor.count()


    # Iterate over the cursor list and ...
    for doc in cursor:

        # ... add INSERT INTO statement every batchSize (1000 by default)
        if batchCounter == 0:

            insert_statement = "INSERT INTO `%s` VALUES " %(tablename)
            insert_statement.replace(r'\n', "")
            print insert_statement

        definition_json = ''
        # Remove unwanted branches
        doc.pop(u'_class', None)
        doc[u'id'] = doc[u'_id'].replace("\'","\'\'")
        doc.pop(u'_id', None)
        entry_json = json.dumps(doc,ensure_ascii=False).replace("\'","\'\'")

        # Format
        definition_json = definition_json + "('"+ doc['id'] + "','" + entry_json + "',0,0,0)"

        # End last insert with ';' rather than ','
        if (count >= total_docs -1) or (count >= queryLimit -1) or (batchCounter == batchSize):
            definition_json = definition_json + ";"
        else:
            definition_json = definition_json + ","

        # Clean out each statement:
        definition_json = definition_json.replace("\"","\\\"")
        definition_json = definition_json.replace(r'\n', "")

        print definition_json.encode('utf-8')

        count = count + 1

        if batchCounter >= batchSize:
            batchCounter = 0
        else:
            batchCounter = batchCounter + 1

    print '-- Task completed. Exported %s out of %s records.' %(count,total_docs)
    # ALWAYS CLOSE THE CURSOR!!!
    cursor.close()

    return definition_json

# def main:

init_statement(tableName)
print_content_with_cursor_batch_insert(collectionName,tableName)
