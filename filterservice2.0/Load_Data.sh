#!/bin/bash

#Used to download mysql data dumps from S3 onto mysql_slaves. It then loads that data onto the slaves.
echo 'Downloading mysql files from s3:'
s3cmd get --recursive s3://us-static-secure-wandera-com/tmp/mtyas/mysql/ .

echo 'Unzipping files:'
gzip -d *.gz

echo 'Configuring mysql to load data dumps:'
mysql -e 'source /usr/share/mysql/innodb_memcached_config.sql'
mysql -e 'source /tmp/create_tables.sql'
mysql -e ' INSTALL PLUGIN daemon_memcached soname "libmemcached.so"; '

echo 'Loading Data into mysql (This will take a while):'
mysql -e 'SET @@GLOBAL.wait_timeout=28000;'
mysql -e 'RESET MASTER'
mysql -uroot  security_ips < security_ips.sql
mysql -e 'RESET MASTER'
mysql -uroot security_other < security_other.sql
mysql -e 'RESET MASTER'
mysql -uroot  security_domains < security_domains.sql
mysql -e 'RESET MASTER'
mysql -uroot security_uris < security_uris.sql
mysql -e 'SET @@GLOBAL.wait_timeout=300;'

echo 'Configuring Master and enabling Slave node:'
mysql -e "CHANGE MASTER TO MASTER_HOST='52.211.149.154',MASTER_USER='repl', MASTER_PASSWORD='q8RSyr3M6TE23uJhT5K4', MASTER_AUTO_POSITION=1;"
mysql -e "START SLAVE;"
echo "=======================     Slave Status      ============================"
mysql -e "SHOW SLAVE STATUS\G"
echo "=======================     Master Status      ==========================="
mysql -e "SHOW MASTER STATUS\G"

echo 'All done, at last!'
