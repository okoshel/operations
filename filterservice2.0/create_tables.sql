CREATE DATABASE IF NOT EXISTS security_ips;

USE security_ips;

CREATE TABLE IF NOT EXISTS security_definition (id VARCHAR(64),
                                payload TEXT,
                                flag INT, cas BIGINT UNSIGNED, exp INT,
                                primary key (id))
ENGINE = INNODB;



CREATE DATABASE IF NOT EXISTS security_domains;

USE security_domains;

CREATE TABLE IF NOT EXISTS security_definition (id VARCHAR(64),
                                payload TEXT,
                                flag INT, cas BIGINT UNSIGNED, exp INT,
                                primary key (id))
ENGINE = INNODB;



CREATE DATABASE IF NOT EXISTS security_uris;

USE security_uris;

CREATE TABLE IF NOT EXISTS security_definition (id VARCHAR(64),
                                payload TEXT,
                                flag INT, cas BIGINT UNSIGNED, exp INT,
                                primary key (id))
ENGINE = INNODB;



CREATE DATABASE IF NOT EXISTS security_other;

USE security_other;

CREATE TABLE IF NOT EXISTS data_leak_rule (id VARCHAR(64),
                          payload TEXT,
                          flag INT, cas BIGINT UNSIGNED, exp INT,
                          primary key (id))
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS app_key_extraction_rule (id VARCHAR(64),
                                  payload TEXT,
                                  flag INT, cas BIGINT UNSIGNED, exp INT,
                                  primary key (id))
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS app_ua_metadata_link (id VARCHAR(64),
                               payload TEXT,
                               flag INT, cas BIGINT UNSIGNED, exp INT,
                               primary key (id))
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS app_metadata (id VARCHAR(64),
                         payload TEXT,
                         flag INT, cas BIGINT UNSIGNED, exp INT,
                         primary key (id))
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS content_classification (id VARCHAR(64),
                                   payload TEXT,
                                   flag INT, cas BIGINT UNSIGNED, exp INT,
                                   primary key (id))
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS os_risk_index (id VARCHAR(64),
                         payload TEXT,
                         flag INT, cas BIGINT UNSIGNED, exp INT,
                         primary key (id))
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS os_cve_data (id VARCHAR(64),
                       payload TEXT,
                       flag INT, cas BIGINT UNSIGNED, exp INT,
                       primary key (id))
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS source_ip_disposition (id VARCHAR(64),
                                 payload TEXT,
                                 flag INT, cas BIGINT UNSIGNED, exp INT,
                                 primary key (id))
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS db_state (id VARCHAR(64),
                     payload TEXT,
                     flag INT, cas BIGINT UNSIGNED, exp INT,
                     primary key (id))
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS update_audit_log (id VARCHAR(64),
                            payload TEXT,
                            flag INT, cas BIGINT UNSIGNED, exp INT,
                            primary key (id))
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS effective_app_metadata (id VARCHAR(64),
                                   payload TEXT,
                                   flag INT, cas BIGINT UNSIGNED, exp INT,
                                   primary key (id))
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS full_app_metadata (id VARCHAR(64),
                             payload TEXT,
                             flag INT, cas BIGINT UNSIGNED, exp INT,
                             primary key (id))
ENGINE = INNODB;



USE innodb_memcache;

CREATE TABLE IF NOT EXISTS `containers` (
  `name` varchar(50) not null primary key,
  `db_schema` VARCHAR(250) NOT NULL,
  `db_table` VARCHAR(250) NOT NULL,
  `key_columns` VARCHAR(250) NOT NULL,
  `value_columns` VARCHAR(250),
  `flags` VARCHAR(250) NOT NULL DEFAULT "0",
  `cas_column` VARCHAR(250),
  `expire_time_column` VARCHAR(250),
  `unique_idx_name_on_key` VARCHAR(250) NOT NULL
) ENGINE = InnoDB;

INSERT INTO containers VALUES
                              ("IP", "security_ips", "security_definition", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("DOMAIN", "security_domains", "security_definition", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("URI", "security_uris", "security_definition", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("DATA_LEAK_RULE", "security_other", "data_leak_rule", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("APP_KEY_EXTRACTION_RULE", "security_other", "app_key_extraction_rule", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("APP_UA_METADATA_LINK", "security_other", "app_ua_metadata_link", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("APP_METADATA", "security_other", "app_metadata", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("CONTENT_CLASSIFICATION", "security_other", "content_classification", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("OS_RISK_INDEX", "security_other", "os_risk_index", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("CVE_DATA", "security_other", "os_cve_data", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("SID", "security_other", "source_ip_disposition", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("DB_STATE", "security_other", "db_state", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("UPDATE_AUDIT_LOG", "security_other", "update_audit_log", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("EFFECTIVE_APP_METADATA", "security_other", "effective_app_metadata", "id", "payload", "flag", "cas", "exp", "PRIMARY"),
                              ("FULL_APP_METADATA", "security_other", "full_app_metadata", "id", "payload", "flag", "cas", "exp", "PRIMARY");
