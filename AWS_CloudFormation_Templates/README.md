# AWS CloudFormation Templates

This location is for storing OPS AWS CloudFormation Templates. Please use descriptive names for the template usage. eg. CreateVPCAndSubnets.json.

All Templates should follow the recommended AWS formats. Please also specify Paramters in each template. This Parameters object can then be updated pragmatically in the future and/or when it is run in AWS console those defined values can be entered manually according to required information.

You should test for the format of your templates using aws cli if possible before running the stack eg.

```
aws cloudformation validate-template --template-body file:////home//local//test//sampletemplate.json
```

Documentation: http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-ec2-instance.html