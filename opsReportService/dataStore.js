var
mongo = require('mongodb'),
  Server = mongo.Server,
  Db = mongo.Db,
  mongo = require('mongoskin'),
  config = require('config').db;

console.log("config uri" + config.uri);

function DataStore() {
  var self = this;
  Db.connect(config.uri, function(err, db) {
    if (err) {
      console.log("Error connecting");
    } else {
      console.log("connected");
      self.db = db;
    }
  });
  console.log("opening");
}

DataStore.prototype.getCollection = function(callback) {
  self = this;
  if (!self.db) {
    callback("Can't get collection because we are Not connected to db")
  } else {
    console.log(self.db);
    console.log("getCollection called");
    var collection = new mongo.Collection(self.db, 'proxy_logs');
    console.log(collection);
    callback(null, collection);
  }
}


DataStore.prototype.groupBy = function(fields, startTime, endTime, callback) {
  this.getCollection(function(err, collection) {
    if (err) {
      console.log(err);
      callback(err);
    } else {
      //var userAgents = [];
      console.log(startTime);
      console.log(endTime);
      console.log(fields);
      collection.group(fields, {
        'startUtcInMs': {
          '$gt': startTime
        },
        'endUtcInMs': {
          '$lt': endTime
        }
      }, {
        //collection.group([field], {} , {
        "count": 0,
        //"dataBefore": 0,
        "dataSaved": 0,
        "dataAfter": 0,
      }, "function (obj, prev) { prev.count++; if ( obj.sizeReduction ) prev.dataSaved+=obj.sizeReduction; if (obj.dataTransferred ) {prev.dataAfter += obj.dataTransferred;}  }", callback);
      console.log("done");
    }
  })
}

exports.DataStore = DataStore;
