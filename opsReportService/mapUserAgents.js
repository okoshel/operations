    var ds = require('./dataStore.js');
    var dataStore = new ds.DataStore();

    function map_userAgent(dbResult) {
      var userAgent = dbResult.userAgent;
      var uaRegex = RegExp(".*Mozilla.*|MobileSafari.*");
      if (uaRegex.test(userAgent) || userAgent === "-") {
        return {
          name: "browser",
          "regEx": uaRegex
        };
      }

      if (userAgent !== "-") {
        var name = userAgent;
        var regExMap = {
          "^(.*d) \\(unknown version\\).*|^com\\.apple\\.madrid-lookup.*|^com\\.apple\\.invitation-registration.*|^server-bag.*|^SyncedDefaults.*|^MobileMeNotificationClient.*|^iPhone$|^iphone \\(.*|^iphone\\d_.*|^iOS\\/.*|^Apple-iPhone.*|^iosfx.*|^gamed\\/.*|^MMSDK.*|^Apple-iPad[0-9].*|^(iPad \\(iPad2,1\\))|^(GeoServices).*|^(locationd).*|^(ubd).*|^MobileBackup.*|^Settings.*|^(AssistantServices).*|^(OTACrashCopier).*|^iOS\\d.*|iOSRBMobile": "System Application",
          "^(AppleCoreMedia)\\/.*": "Apple Media Player",
          "^(TED iOS);.*": "TED",
          "^(uk.co.leeus.shipfinder).*": "Ship Finder",
          "^(tv24.UK).*": "tv24.co.uk",
          "^(com.skype).*": "Skype",
          "^(SF Free).*": "Ship Finder Free",
          "^(AdSheet).*": "Advertising",
          "^(inmobi).*": "Advertising",
          "^(argosapp).*": "Argos",
          "^(BBCBeacon).*": "BBC News",
          "^(CTRExperiments).*": "Cut the Rope: Experiments",
          "^(SuperCrateBox).*": "Super Crate Box",
          "^(EmSea).*": "Google+",
          "^(FacebookConnect).*?": "Facebook Connect",
          "^(BarclaysPremierLeague).*": "Barclays Premier League",
          "^(My02).*": "My 02",
          "^(PowerCam).*": "PowerCam",
          "^(RK_Pro).*": "RunKeep Pro",
          "^(SkyGo).*": "Sky Go",
          "^(SkyNews).*": "Sky News",
          "^(SkyPlus).*": "Sky Plus",
          "^(superrugby2012).*": "Super Rugby 2012",
          "^(WebAlbums-2\\.0).*": "WebAlbums",
          "^(London.*2012).*": "London 2012",
          "^(eBayiPhone).*": "eBay",
          "^(NotesPlus).*": "NotesPlus",
          "^(Evernote).*": "Evernote",
          "^(Pinterest).*": "Pinterest",
          "^(Spotify).*": "Spotify",
          "^(Guardian).*": "Guardian",
          "^(EtsyInc).*": "Etsy",
          "^(BBC\\s?iPlayer).*": "BBC iPlayer",
          "^(com.foursquare).*": "foursquare",
          "^(Flixster).*?": "Flixster",
          "^(Messenger/).*?": "Facebook Messenger",          
          "^(Phone.*Edition/).*?": "Metro",          
          ".*(TheTrainLineApp).*": "The Train Line",
          ".*(VoucherCodes).*": "VoucherCodes",
          "^(Web/).*": "PayByPhone",
          "^(iPhone Mail).*": "Mail",
          "^(com.apple.Maps).*": "Apple Maps",
          "^(Chrome).*": "Chrome",
          "^(sky_sports_news_universal).*": "Sky Sports News",
          "^(GifBoom).*": "GifBoom",
          "^(latitude/).*": "Latitude",
          "^(GmailHybrid/).*": "Gmail",
          ".*(citymapper).*": "CityMapper",
          "^(TV Guide Sky).*": "TV Guide Sky",
          "^(iSteamLight).*": "iSteamLight",
          "^(FacebookTouch).*": "Facebook",
          ".*(com.facebook.Facebook).*": "Facebook",
          ".*(BA2Go).*": "BA2Go",
          "^(Audible).*": "Audible",
          ".*(TradeApp).*": "TradeApp",
          ".*(BusMapper).*": "BusMapper",
          "^(Klout).*": "Klout",
          "^(Consumer_iPhone).*": "AutoTrader",
          ".*(Drive).*": "Google Drive",
          ".*(OmniFocus).*": "OmniFocus",
          ".*(ScoreCenter).*": "ScoreCenter",
          "^(googledriveios).*": "Google Drive",
          "^(iStreamLight).*": "iStreamLight",
          ".*(GoogleMobile).*": "Google",
          ".*(Dolphin-HD).*": "Dolphin",
          ".*(Gmail).*": "Gmail",
          ".*(NatWest).*": "NatWest",          
          "^Apple iPhone v.* (.*) v.*": null,
          "^Apple iPad v.* (.*) v.*": null,
          "(.*?)(-iPhone|-iPad).*": null,
          "(.*?),?[\\s/]iOS.*": null,
          "(.*?)[\\s/]\\d": null
        }


        var regExMatched;
        for (i in regExMap) {
          var regExp = new RegExp(i, "i");
          if (regExp.test(name)) {
            var pattern = regExp.source;
            regExMatched = pattern;
            if (regExMap[i] !== null) {
              name = regExMap[i];
              regExMatched = pattern;
            } else {
              name = regExp.exec(name)[1];
              var regExMap = name;
            }
            break;
          }
        }


        name = unescape(name);
        return {
          "name": name,
          "dataSaved": dbResult.dataSaved,
          "dataBefore": dbResult.dataBefore,
          "regEx": regExMatched
        };
      }
    }


    function map_media(dbResult) {
      var userAgent = dbResult.userAgent;
      var mediaPlayerRegEx = new RegExp("^(AppleCoreMedia)\\/.*");

      var name = "non-media";
      if (mediaPlayerRegEx.test(userAgent) && dbResult.type) {
        var majorContentType = dbResult.type.split('/')[0]
        site = dbResult.site;
        name = site + " - " + dbResult.type;
      }
      return {
        "name": name,
        "dataSaved": dbResult.dataSaved,
        "dataBefore": dbResult.dataBefore,
        "regEx": ""
      };
    }


    function generateMapper(field ) { 
        return function (dbResult) {
            return {
                "name": dbResult[field],
        "dataSaved": dbResult.dataSaved,
        "dataBefore": dbResult.dataBefore,
        "regEx": ""
      };
        }
    }

    var mapping = {
    		"media" : ["userAgent", "type", "site"]
    }
    
    exports.map = function(groupByField, orderByField, startTime, endTime, callback) {
      console.log("finding " + groupByField);
      var fieldList = mapping[groupByField];
      if ( ! fieldList ) {
    	  fieldList = [groupByField]
      } 
      dataStore.groupBy(fieldList, startTime, endTime, function(err, groupByResult) {
        mapUserAgents(groupByResult, orderByField, buildMapFunction(groupByField) , function(result) {
          callback(JSON.stringify(result, undefined, 2));
        });
      });
    }

    
    function buildMapFunction(fieldName) { 
    	try { 
    	    f = eval("map_" + fieldName);
    	    return f;
    	} catch (err) { 
    	    return generateMapper(fieldName);
    	}
    }

    function mapUserAgents(array, orderByField, mapFunction, callback) {
      var resultMap = [];
      for (lineCounter in array) {
        var jsonValue = array[lineCounter];
        var count = jsonValue.count;
        var dataSaved = jsonValue.dataSaved;
        var dataAfter = jsonValue.dataAfter;
        var dataBefore = dataAfter + dataSaved;
        var mapResult = mapFunction(jsonValue);
        var result = mapResult.name;
        var regEx = mapResult.regEx;

        if (resultMap[result]) {
          resultMap[result].count += count;
          resultMap[result].dataSaved += dataSaved;
          resultMap[result].dataBefore += dataBefore;
          resultMap[result].dataAfter += dataAfter;
          resultMap[result].percentageSaved = resultMap[result].dataSaved / resultMap[result].dataBefore * 100;
          if (resultMap[result].userAgents) {
            resultMap[result].userAgents.push({
              "userAgent": jsonValue.userAgent,
              "regEx": regEx
            });
          }
        } else {
          resultMap[result] = {
            name: result,
            count: count,
            dataSaved: dataSaved,
            dataBefore: dataBefore,
            dataAfter: dataAfter,
            percentageSaved: dataSaved / dataBefore * 100
          };
          if (regEx) {
            resultMap[result].userAgents = [{
              "userAgent": jsonValue.userAgent,
              "regEx": regEx
            }];
          }

        }
      }
      var sortedOutput = []
      var totals = {
        count: 0,
        dataSaved: 0,
        dataBefore: 0,
        dataAfter: 0
      }
      for (var key in resultMap) {
        var currentResult = resultMap[key]
        sortedOutput.push(currentResult);
        totals.count += currentResult.count;
        totals.dataSaved += currentResult.dataSaved;
        totals.dataBefore += currentResult.dataBefore;
        totals.dataAfter += currentResult.dataAfter;
      }
      totals.percentageSaved = totals.dataSaved / totals.dataBefore * 100

      console.log(totals);

      sortedOutput.sort(function(a, b) {
        return b[orderByField] - a[orderByField]
      })

      var result = {
        totals: totals,
        details: sortedOutput
      }

      callback(result);
    }
