/**
 * Module dependencies.
 */

var express = require('express'),
  routes = require('./routes'),
  mapper = require('./mapUserAgents.js'),
  config = require('config');

var ISODate = require("isodate");

// Read a date string
var date = ISODate("2011-08-20T19:39:52Z");
console.log(date);

// Write current date as ISO 8061 string.
date = new Date();
console.log(date.toISOString());




var app = module.exports = express.createServer();

// Configuration
app.configure(function() {
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function() {
  app.use(express.errorHandler({
    dumpExceptions: true,
    showStack: true
  }));
});

app.configure('production', function() {
  app.use(express.errorHandler());
});

// Routes
app.get('/', routes.index);
app.get('/apps', function(req, res) {
  forwardRequestGeneric(req, res, "userAgent")
});


app.get('/media', function(req, res) {
	forwardRequestGeneric(req, res, "media")
});


app.get('/sites', function(req, res) {
  forwardRequestGeneric(req, res, "site")
});


app.get('/types', function(req, res) {
  forwardRequestGeneric(req, res, "type")
});

app.get('/users', function(req, res) {
	  forwardRequestGeneric(req, res, "guid")
});


forwardRequestGeneric = function(req, res, field) {
  console.log("got request")
  var startTime = ISODate(req.query["startTime"])
  var endTime = ISODate(req.query["endTime"])
  mapper.map(field, req.query["orderBy"], startTime.getTime(), endTime.getTime(), function(result) {
    res.write(result);
    res.end();
  })
}


//forwardRequest = function(req, res, mapperFunction) {
//  console.log("got request")
//  var startTime = ISODate(req.query["startTime"])
//  var endTime = ISODate(req.query["endTime"])
//  mapperFunction(req.query["orderBy"], startTime.getTime(), endTime.getTime(), function(result) {
//    res.write(result);
//    res.end();
//  })
//}

app.listen(3000, function() {
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});
