# Mcollective Cheat Sheet

This is a collection of OPS related cheat sheet items for Mcollective. Please update this file with any other useful commands that you find or produce.

**Find Commands:-**

Find Identity Names using regex...

    mco find --with-identity /node/
    (results:-
    12322424242424.node.whatever.com
    rwrwrwr1212132.node.whatever.com)

Find package names (including -S for specifics, for including environment for example)....

    mco find --with-class package_apache2 -S "environment=staging"
    (results:-
    cms-101-node.blah.com
    prxnode.blah.com
    webnode.blah.com
    5f708anode.blah.com
    6dcaanode.blah.com)

Find the status of running services on remote nodes (including -S for specific facts, like environment and primary_role)...

    mco rpc service status service=td-agent-proxy_access_s3_ireland_1c -S "environment=production and primary_role=proxy"
    (results:-
    12121212122121.node.blah.com)

**Run Commands:-**

Run a specific class/module on remote nodes (again, using -S for fact targetting)

    mco puppet runall 2 --tags telegraf -S "environment=staging"
    (results:-
    eewrwerwrwr.node.blah.com)

   

WIP
### Version
0.0.1

License
----

MIT

**Free Software, Hell Yeah!**
