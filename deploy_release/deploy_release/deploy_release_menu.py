#!/usr/local/bin/python

#####################################################################
# deploy_release.py
# Author: Arturo Noha
# Version: 3.2
#------------------------------------------------------------------
# USAGE: ./deploy_release.3.2.py --rel <release_num> -u <ssh_username> --env <environment> --relservices <release_services_file> --dryrun <True/False> [options]
#	* Available environments:
#	- stgn: staging
#	- intg: integration
#	- prod-proxy: production proxies (via woopas). Requires proxy deployment plan file
#	- prod-core: app and portal in prodution ==> Set stack weight to 0/1. Option to only ser APP or PORTAL + Detaches instances from Load Balancers
#	- prod-services: non app backend services (siem, connector, delta, filterservice...). Requires release services file
#	- core-full: all production services (app, portal, other services). Requires release services file
#	* Options:
#	--allservices: file with the inforamtion of all available services. Default: ./release_services/all_services.info
#	--nodes: File with the list of nodes to deploy to if not all default nodes required
#	--proxy-plan: File with the daily proxy deployment plan per region. Default: ./release_Proxy_nodes/proxy_deployment_plan.csv
#####################################################################

import os
import sys

from pprint import pprint
import argparse
import re
import subprocess
from subprocess import Popen, PIPE

import fabric
from fabric.context_managers import settings
from fabric.api import env
import imp
from datetime import date, datetime
import time
from time import sleep
import platform
from termcolor import colored
import logging
import colorer

import service_config
import libcloud_api
import woopas_amigo
from service_config import *
from fabfile import *
import interaction
import util
from util import *
from release_version_loader import getServicesVersions

import check_all_versions
from create_release_CR import do_cr_prep
import requests
from jira import JIRA
import thread
from git import Repo

env.use_ssh_config = True

today = date.today()
DATE = today.isoformat()
today_regex = re.compile("{},(.*),TODO".format(DATE))
nodes_todo_regex = re.compile("[^,]*,TODO")

current_dir = os.getcwd()
log_file = os.path.join(current_dir,"logs")
WOOPAS_PLAN = os.path.join(current_dir, "release_Proxy_nodes", "THREAD_1_PLAN.csv")
home = os.path.expanduser('~')

env_opt_mapping = {
    1 : "versions", 2 : "stgn", 3 : "core", 4 : "prod-services", 5 : "radar", 6 : "prod-proxy", 7 : "tools", 8 : "prod-endpoint", 9 : "opscenter", 10 : "apache", 11 : "create_cr"
}
versions_env_mapping = {
    1 : "staging", 2 : "production"
}

def clear_screen():
    clear = Popen( "cls" if platform.system() == "Windows" else "clear", shell=True)
    clear.communicate()

def update_node_status(node,TODOLIST):
    temp_file_name = "{}.tmp".format(TODOLIST)
    with open(TODOLIST, "r")  as todo_file, open(temp_file_name,"w") as temp_file:
        for line in todo_file:
            update_regex_start = re.compile("^{},TODO".format(node))
            replace_str_start = "{},DONE".format(node)
            line = update_regex_start.sub(replace_str_start, line)

            update_regex_middle = re.compile(",{},TODO".format(node))
            replace_str_middle = ",{},DONE".format(node)
            line = update_regex_middle.sub(replace_str_middle, line)

            temp_file.write(line)
    os.remove(TODOLIST)
    os.rename(temp_file_name, TODOLIST)

def stopped_instance_options(name = ""):
    options = ['1','2','3']
    option = raw_input("WARNING: {} is stopped. What would you wish todo?\n".format(name) +
                    "\t1. Start and deploy\n" +
                    "\t2. Deploy without starting\n" +
                    "\t3. Skip\n" +
                    "\nYour choice: ")

    while option not in options:
        print "$ Please select a valid option: {}".format(options)

    return option

def deploy(myhost, user, name, dryrun=True, tags=None, force_deploy=False, environ='production'):
    if not dryrun:
        try:
            with settings(host_string = myhost, user = user):
                return deploy_server(name,tags=tags,force_deploy=force_deploy, environ=environ)
        except Exception as e:
            logging.error("Could not deploy on {}".format(myhost))
            logging.info("Exception: {}".format(e.message))
            logging.info("{}".format(e))
            return -1
    else:
        return 0

def deploy_canary(myhost, user, name, dryrun=True, tags=None):
    for server in service_config.canary_servers:
        deploy(myhost, user, name, dryrun, tags)

def deploy_core(user,stack_type,dryrun=True,apache=False,force_weights=False,rl_tasks=None):
    """
    user: ssh username
    stack_type: CORE / RADAR
    dryrun : True/False
    """
    jira = JIRA("https://snappli.atlassian.net", basic_auth=util.get_jira_credentails())
    jira_creds = util.get_jira_credentails()
    stack_type = stack_type.upper()
    stack_type_regex = re.compile(stack_type)

    if apache:
        stack_type_print = "{} APACHE".format(stack_type)
    else:
        stack_type_print = stack_type

    if not dryrun:
        print(colored("\nHave you sent the release notification email to ops-notify@wandera.com and releases@wandera.com?\n", "yellow"))
        if interaction.ask_value("Please enter 'continue' to start deployment ", choices=['continue','exit']) == 'e':
            sys.exit(0)

        post_reponse = slack_post("STARTING {} DEPLOYMENT".format(stack_type_print))
        logging.info("Slack post status: {}".format(post_reponse))

    if rl_tasks is not None:
        print(colored("Please complete before tasks. Will resume when done. You can press enter when completed to continue","yellow"))
        logging.info(colored("List of before tasks:","cyan"))
        before_tickets = rl_tasks['before']
        for bticket in before_tickets:
            logging.info("\t{0} (https://snappli.atlassian.net/browse/{0})".format(bticket.key))
        if not dryrun:
            try:
                wait_tasks_done(before_tickets,'production','before',jira)
            except KeyboardInterrupt:
                if not dryrun:
                    slack_post("{} deployment ABORTED before BEFORE TASKS completed".format(stack_type_print.upper()))
                logging.warning("User aborted during before tasks")
                if interaction.confirm('\n\nWould you like to run another step? '):
                    main(check_access=False)
                else:
                    sys.exit(1)

    lb_provider = "EC2"
    lb_region = "eu-west-1"
    lb_driver = libcloud_api.get_LB_driver(lb_region)


    ec2_drivers = libcloud_api.init_ec2_driver([lb_region])
    ec2_instances = libcloud_api.get_all_instances(ec2_drivers)

    stack_out_weight = 0
    stack_in_weight = 255
    if apache:
        stack_startup_weights = [stack_in_weight]
        stack_removal_weights = [stack_in_weight/2]
    else:
        stack_startup_weights = [50,150,stack_in_weight]
        stack_removal_weights = [stack_out_weight,100]

    if not dryrun:
        logging.info("")
        if interaction.confirm("Would you like to set Stacks to working weight {}? ".format(stack_in_weight),default='n'):
            for stack in sorted(stacks_info.keys()):
                if not interaction.confirm("Will set weight of Stack {} to {}. Proceed? ".format(stack, stack_in_weight)):
                    if not interaction.confirm("Do you wish to continue with the other stacks? "):
                        sys.exit(0)
                    else:
                        continue
                else:
                    stack_update = libcloud_api.update_route53_weight(stack, stack_in_weight, stack_type=stack_type, dryrun=dryrun, force_weights=force_weights)
                    if stack_update == -1:
                        logging.warning("-- User skipped update of all record sets in stack {} to weight {}. No wait performed".format(stack, stack_in_weight))
                    elif stack_update:
                        logging.info( "-- Waiting for Stack {} to settle at weight {}".format(stack, stack_in_weight))
                        sleep(stack_phase_sleep)
                    else:
                        if not interaction.confirm("ERROR: Failed to set weight to {}. Continue? ".format(stack_in_weight)):
                            sys.exit("\n** Deployment aborted by user **\n")
    else:
        logging.info('')
        logging.info("Will set all Stacks to working weight: {}".format(stack_in_weight))

    for stack in sorted(stacks_info.keys()):
        logging.info('')
        logging.info("* Starting deployment to Stack {}".format(stack))

        for my_stack_type in stacks_info[stack].keys():
            if not stack_type_regex.search(my_stack_type.upper()):
                continue

            #if not apache:
            if not dryrun:
                if not interaction.confirm("Will phase out {} on Stack {}. Proceed? ".format(stack_type, stack)):
                    if not interaction.confirm("Do you wish to continue with the other stacks? "):
                        sys.exit(0)
                    else:
                        continue
            else:
                logging.info( "Will phase out {} on Stack {}.".format(stack_type, stack))


            last_weight = stack_in_weight
            for weight in sorted(stack_removal_weights, key=int, reverse=True):
                if weight == stack_in_weight:
                    continue
                stack_update = libcloud_api.update_route53_weight(stack, weight, stack_type=stack_type, dryrun=dryrun, force_weights=force_weights)
                if stack_update == -1:
                    logging.warning("-- User skipped update of all record sets in stack {} to weight {}. No wait performed".format(stack, weight))
                elif stack_update:
                    last_weight = weight
                    if not dryrun:
                        logging.info( "-- Waiting for {} in Stack {} to settle at weight {}".format(stack_type, stack, weight))
                        sleep(stack_phase_sleep)
                    else:
                        logging.info( "-- Will wait for {} in Stack {} to settle at weight {}\n".format(stack_type, stack, weight))
                        sleep(1)
                else:
                    if not interaction.confirm("ERROR: Failed to set weight to {}. Continue? ".format(weight)):
                        sys.exit("\n** Deployment aborted by user **\n")

            if not dryrun:
                if last_weight > stack_out_weight:
                    post_reponse = slack_post("{} Stack {} is now PARTIALLY OUT OF SERVICE (weight = {}) for {} deplpoyment".format(my_stack_type.upper(),stack,last_weight,stack_type_print))
                else:
                    post_reponse = slack_post("{} Stack {} is now OUT OF SERVICE (weight = {})".format(my_stack_type.upper(),stack,last_weight))
                logging.info("Slack post status: {}".format(post_reponse))

            for lb_instance in sorted(stacks_info[stack][my_stack_type].keys()):
                if not dryrun:
                    if not apache:
                        logging.info("\n-- Starting deployment to instances behind {}".format(lb_instance))
                        for node in stacks_info[stack][my_stack_type][lb_instance]["instances"]:
                            logging.info("\t- {} ({})".format(node, stacks_info[stack][my_stack_type][lb_instance]["instances"][node]))

                        if not interaction.confirm("\n-- Would you like to deploy to those instances? "):
                            logging.info("Skipping deployment of {} insntances as per user request".format(lb_instance))
                            continue
                    else:
                        if not interaction.confirm("\n-- Would you like to deploy to {}? ".format(lb_instance)):
                            logging.info("Skipping deployment of {} insntances as per user request".format(lb_instance))
                            continue

                for lb in sorted(stacks_info[stack][my_stack_type][lb_instance]["lbs"]):
                    if not dryrun:
                        logging.info('')
                        logging.info( "* Detaching {} from {}".format(lb_instance, lb))
                        if not libcloud_api.detach_LB_member(lb_instance, lb, lb_driver):
                            logging.info('')
                            logging.error("Failed to detach {} from {}. Please check manually.".format(lb_instance, lb))
                            if not interaction.confirm("Would you wish to continue with deployment? "):
                                sys.exit(1)
                        else:
                            logging.info( "* {} successfully detached from {}\nWaiting for it to fully take effect".format(lb_instance, lb))
                            sleep(30)
                    else:
                        logging.info( "Will detach {} from {}".format(lb_instance, lb))

                for node in sorted(stacks_info[stack][my_stack_type][lb_instance]["instances"].keys()):
                    if not apache or (apache and node == lb_instance):
                        for driver_name in ec2_instances:
                            for ec2_instance in ec2_instances[driver_name]:
                                if ec2_instance.name == node:
                                    node_ip = ec2_instance.public_ip
                        inst_type = stacks_info[stack][my_stack_type][lb_instance]["instances"][node]

                        if not dryrun:
                            if not interaction.confirm("\n-- Would you like to deploy to {} {} ({})? ".format(inst_type,node, node_ip)):
                                logging.info("Skipping deployment of {} as per user request".format(node))
                                continue
                            logging.info('')
                            logging.info("* Starting deployment to {} ({})".format(node, node_ip))
                            deploy_return = deploy(node_ip, user, node, dryrun=dryrun)
                            if deploy_return != 0:
                                logging.error("Deployment to {} ({}) failed".format(node, node_ip))
                                if not interaction.confirm("\nWould you wish to continue deploying the rest of nodes? ".format(node)):
                                    logging.info("Deployment aborted by user")
                                    sys.exit(1)
                            if not interaction.confirm("\n-- Continue with the rest of nodes? "):
                                print
                                logging.info("** Deployment aborted by user **")
                                sys.exit(1)
                        else:
                            logging.info("Will deploy to {} {} ({})".format(stacks_info[stack][my_stack_type][lb_instance]["instances"][node], node, node_ip))

                for lb in sorted(stacks_info[stack][my_stack_type][lb_instance]["lbs"]):
                    if not dryrun:
                        logging.info('')
                        logging.info( "* Re-attaching {} to {}".format(lb_instance, lb))
                        if libcloud_api.attach_LB_member(lb_instance, lb, lb_driver) == False:
                            logging.error("There was a problem reattching {}".format(lb_instance))
                            if not interaction.confirm("\nContinue deployment? "):
                                logging.info("Deployment aborted by user")
                                sys.exit(1)
                        else:
                            logging.info( "* {} successfully re-attached to {}\nWaiting for it to fully take effect".format(lb_instance, lb))
                            sleep(30)
                    else:
                        logging.info( "Will re-attach {} to {}".format(lb_instance, lb))

            logging.info('')
            #if not apache:
            logging.info("- Reactivating Stack {}".format(stack))
            for weight in sorted(stack_startup_weights, key=int):
                stack_update = libcloud_api.update_route53_weight(stack, weight, stack_type=stack_type, dryrun=dryrun, force_weights=force_weights)
                if stack_update == -1:
                    logging.warning("-- User skipped update of all record sets in stack {} to weight {}. No wait performed".format(stack, weight))
                elif stack_update:
                    last_weight = weight
                    if not dryrun:
                        logging.info( "-- Waiting for {} in Stack {} to settle at weight {}".format(stack_type, stack, weight))
                        sleep(stack_phase_sleep)
                    else:
                        logging.info( "-- Will wait for {} in Stack {} to settle at weight {}\n".format(stack_type, stack, weight))
                        sleep(1)
                else:
                    logging.error("Failed to set weight to 1")
                    if not interaction.confirm("\nContinue? "):
                        logging.info("Deployment aborted by user")
                        sys.exit(1)

            if not dryrun:
                post_reponse = slack_post("{} Stack {} is now BACK IN SERVICE (weight = {})".format(my_stack_type.upper(),stack,last_weight))
                logging.info("Slack post status: {}".format(post_reponse))

            logging.info('')
            logging.info("** Deployment to {} Stack {} completed".format(my_stack_type.upper(),stack))
            if not dryrun:
                if not interaction.confirm("Continue? "):
                    sys.exit(1)

    if rl_tasks is not None:
        print(colored("Please complete After tasks. Will resume when done. You can press enter when completed to continue","yellow"))
        logging.info(colored("List of After tasks:","cyan"))
        after_tickets = rl_tasks['after']
        for aticket in after_tickets:
            logging.info("\t{0} (https://snappli.atlassian.net/browse/{0})".format(aticket.key))
        if not dryrun:
            try:
                wait_tasks_done(after_tickets,'production','after',jira)
            except KeyboardInterrupt:
                if not dryrun:
                    slack_post("{} deployment ABORTED before AFTER TASKS completed".format(stack_type_print.upper()))
                logging.warning("User aborted during after tasks")
                if interaction.confirm('\n\nWould you like to run another step? '):
                    main(check_access=False)
                else:
                    sys.exit(1)

    if not dryrun:
        post_reponse = slack_post("{} DEPLOYMENT COMPLETED".format(stack_type_print.upper()))
        logging.info("Slack post status: {}".format(post_reponse))

def deploy_endpoint(endpoint, user,dryrun=True,force_weights=False):
    lb_provider = "EC2"
    lb_region = "eu-west-1"
    lb_driver = libcloud_api.get_LB_driver(lb_region)

    ep_service = endpoint_mapping[endpoint]["service_name"]
    ep_service_regex = re.compile(ep_service)
    server_type = None

    for service in service_config.ALL_SERVICES:
        if ep_service_regex.search(service['name']) or (service.get('alias') and ep_service_regex.search(service['alias'])):
            server_type = service['node']

    if server_type is None:
        print(colored("server type not found for endpoint {}".format(endpoint), "red"))
        return False
    server_type_regex = re.compile(server_type)

    if not dryrun:
        post_reponse = slack_post("STARTING DEPLOYMENT OF {} ({})".format(endpoint,ep_service))
        logging.info("Slack post status: {}".format(post_reponse))

    ec2_drivers = libcloud_api.init_ec2_driver([lb_region])
    ec2_instances = libcloud_api.get_all_instances(ec2_drivers)

    stack_out_weight = 0
    stack_in_weight = 255
    stack_startup_weights = [50,150,stack_in_weight]
    stack_removal_weights = [stack_out_weight,100]

    if not dryrun:
        logging.info('')
        if interaction.confirm("Would you like to set endpoint {} to working weight {} for all Stacks? ".format(endpoint, stack_in_weight),default='n'):
            for stack in sorted(stacks_info.keys()):
                if not interaction.confirm("Will set weight of {} on Stack {} to {}. Proceed? ".format(endpoint, stack, stack_in_weight)):
                    if not interaction.confirm("Do you wish to continue with the other stacks? "):
                        sys.exit(0)
                    else:
                        continue
                else:
                    if libcloud_api.update_endpoint_weight(endpoint, stack, stack_in_weight, force_weights=force_weights):
                        logging.info( "-- Waiting for {} on Stack {} to settle at weight {}".format(endpoint, stack, stack_in_weight))
                        sleep(30)
                    else:
                        if not interaction.confirm("ERROR: Failed to set weight to {} for {} on Stack {}. Continue? ".format(stack_in_weight, endpoint, stack)):
                            sys.exit("\n** Deployment aborted by user **\n")
    else:
        logging.info('')
        logging.info( "Will set all weigth of {} to working weight {} on all stacks".format(endpoint, stack_in_weight))

    for stack in sorted(stacks_info.keys()):
        if not dryrun:
            logging.info('')
            logging.info( "* Starting deployment to {} on Stack {}".format(endpoint, stack))

            if not interaction.confirm("Will phase out {} on Stack {}. Proceed? ".format(endpoint, stack)):
                if not interaction.confirm("Do you wish to continue with the other stacks? "):
                    sys.exit(0)
                else:
                    continue

            last_weight = stack_in_weight
            for weight in sorted(stack_removal_weights, key=int, reverse=True):
                if weight == stack_in_weight:
                    continue
                stack_update = libcloud_api.update_endpoint_weight(endpoint, stack, weight, force_weights=force_weights)
                if stack_update == -1:
                    logging.warning("-- User skipped update of {} in stack {} to weight {}. No wait performed".format(endpoint, stack, weight))
                elif stack_update:
                    last_weight = weight
                    logging.info( "-- Waiting for {} in Stack {} to settle at weight {}".format(endpoint, stack, weight))
                    sleep(stack_phase_sleep)
                else:
                    if not interaction.confirm("ERROR: Failed to set weight of {} to {} on {}. Continue? ".format(endpoint, weight, stack)):
                        sys.exit("\n** Deployment aborted by user **\n")
            if not dryrun:
                post_reponse = slack_post("{} ({}) is OUT OF SERVICE in Stack {} (weight = {})".format(endpoint,ep_service,stack,last_weight))
                logging.info("Slack post status: {}".format(post_reponse))

            for my_stack_type in sorted(stacks_info[stack].keys()):
                for lb_instance in sorted(stacks_info[stack][my_stack_type].keys()):
                    if not interaction.confirm("\n-- Starting deployment to instances behind {} ".format(lb_instance)):
                        logging.info("Skipping deployment of {} insntances as per user request".format(lb_instance))
                        continue

                    for node in stacks_info[stack][my_stack_type][lb_instance]["instances"]:
                        if server_type_regex.match(node) and node != lb_instance:
                            for driver_name in ec2_instances:
                                for ec2_instance in ec2_instances[driver_name]:
                                    if ec2_instance.name == node:
                                        node_ip = ec2_instance.public_ip
                            if not interaction.confirm("\n-- Would you like to deploy to {} ({})? ".format(node, node_ip)):
                                logging.info("Skipping deployment of {} as per user request".format(node))
                                continue
                            logging.info('')
                            logging.info("* Starting deployment to {} ({})".format(node, node_ip))
                            process_name = endpoint_mapping[endpoint]["puppet_tag"]
                            deploy_return = deploy(node_ip, user, node, dryrun=dryrun, tags="environment_properties,{}".format(process_name))
                            if deploy_return != 0:
                                if not interaction.confirm("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes? ".format(node)):
                                    sys.exit(1)
                            if not interaction.confirm("\n-- Continue with the rest of nodes? "):
                                sys.exit("\n** Deployment aborted by user **\n")
                logging.info('')

            logging.info( "- Reactivating {} on Stack {}".format(endpoint, stack))
            for weight in sorted(stack_startup_weights, key=int):
                stack_update = libcloud_api.update_endpoint_weight(endpoint, stack, weight, force_weights=force_weights)
                if stack_update == -1:
                    logging.warning("-- User skipped update of {} in stack {} to weight {}. No wait performed".format(endpoint, stack, weight))
                elif stack_update:
                    last_weight = weight
                    logging.info( "-- Waiting for {} in Stack {} to settle at weight {}".format(endpoint, stack, weight))
                    sleep(stack_phase_sleep)
                else:
                    if not interaction.confirm("ERROR: Failed to set weight of {} to {} on {}. Continue? ".format(endpoint, weight, stack)):
                        sys.exit("\n** Deployment aborted by user **\n")
            if not dryrun:
                post_reponse = slack_post("{} ({}) is BACK IN SERVICE in Stack {} (weight = {})".format(endpoint,ep_service,stack,last_weight))
                logging.info("Slack post status: {}".format(post_reponse))

            logging.info('')
            logging.info( "** Deployment of {} to Stack {} completed".format(endpoint, stack))
            if not interaction.confirm("Continue? "):
                sys.exit(1)
        # DRYRUN
        else:
            logging.info('')
            logging.info( "* Starting deployment to Stack {}".format(stack, dryrun))
            for weight in sorted(stack_removal_weights, key=int, reverse=True):
                if weight == stack_in_weight:
                    continue
                logging.info('')
                logging.info( "Will set {} on stack {} to {} weight".format(endpoint, stack,weight))
                logging.info( "- will wait {} min for {} on Stack {} to settle at weight {}".format(stack_phase_sleep/60, endpoint, stack, weight))
                sleep(1)
            for my_stack_type in sorted(stacks_info[stack].keys()):
                for lb_instance in sorted(stacks_info[stack][my_stack_type].keys()):
                    logging.info('')
                    logging.info("* Starting deployment to {}".format(lb_instance))
                    logging.info('')

                    for node in sorted(stacks_info[stack][my_stack_type][lb_instance]["instances"]):
                        if server_type_regex.match(node):
                            for driver_name in ec2_instances:
                                for ec2_instance in ec2_instances[driver_name]:
                                    if ec2_instance.name == node:
                                        node_ip = ec2_instance.public_ip
                            logging.info("Will deploy to {} ({})".format(node, node_ip))
                            process_name = endpoint_mapping[endpoint]["puppet_tag"]
                            deploy_return = deploy(node_ip, user, node, dryrun=dryrun, tags=process_name)
                            if deploy_return != 0:
                                if not interaction.confirm("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes? ".format(node)):
                                    sys.exit(1)

                for weight in sorted(stack_startup_weights, key=int):
                    logging.info('')
                    logging.info( "Will set weight of {} to {} for Stack {}".format(endpoint, weight, stack))
                    logging.info("Will sleep {} min".format(stack_phase_sleep/60))
                    sleep(1)
            if not dryrun:
                post_reponse = slack_post("{} ({}) is BACK IN SERVICE in Stack {}".format(endpoint,ep_service,stack))
                logging.info("Slack post status: {}".format(post_reponse))
    if not dryrun:
        post_reponse = slack_post("DEPLOYMENT OF {} ({}) COMPLETED".format(endpoint,ep_service))
        logging.info("Slack post status: {}".format(post_reponse))

def display_options(user, options):
    clear_screen()
    print "-------------------------------------------------"
    print "Running deployment with following options:"
    for option in sorted(options.keys()):
        if options[option] is not None:
            print "\t- {}: {}".format(option, options[option])

    return interaction.confirm('\nContinue? ')

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('-o','--option', dest = "option")
    parser.add_argument('-d','--dryrun', dest = "dryrun", choices=['true','false'])
    parser.add_argument('-b','--branch', dest = "branch")
    parser.add_argument('-a','--at-branch', dest = "at_branch")
    parser.add_argument('-t','--ticket', dest = "ticket")

    return parser.parse_args()

def menu(option=None,dryrun=None):
    check_versions = False
    options = {}
    jira = JIRA("https://snappli.atlassian.net", basic_auth=util.get_jira_credentails())
    options["jira"] = jira
    proxy_plan = None

    if option is None:
        clear_screen()
        print("\nSelect an option:\n" +
                "\n\t1. Get latest versions from S3" +
                "\n\t2. Deploy to staging" +
                "\n\t3. Deploy Core to production" +
                "\n\t4. Deploy Services to production" +
                "\n\t5. Deploy Portal to production" +
                "\n\t6. Deploy Proxy to production" +
                "\n\t7. Deploy Internal Tools" +
                "\n\t8. Deploy To a single Core endpoint" +
                "\n\t9. Deploy OpsCenter" +
                "\n\t10. Deploy to Apache Nodes" +
                "\n\t11. Create CR for production" +
                "\n\t0. Cancel")

        option = raw_input("\nChoice: ")
        try:
            option = int(option)
        except:
            option = -1
    while option not in range(0,len(env_opt_mapping)+1):
        option = raw_input("Please enter a valid option (0-8): ")
        try:
            option = int(option)
        except:
            option = -1
    if option == 0:
        sys.exit(0)
    environ = env_opt_mapping[option]

    endpoint = None
    puppetdir = None

    force_weights = False
    if option in [3,8,10]:
        force_choice = interaction.ask_value("Would you like to ask for confirmation for each weight change?", choices=['Ask','Do not ask confirmation'])
        if force_choice == 'd':
            force_weights = True
    options["force_weights"] = force_weights

    if option == 8:
        endpoint = raw_input("\n- Endpoint (i.e. eu.wandera.com)\nReference: {}: ".format(endpoints_url))
        while endpoint not in endpoint_mapping:
            endpoint = raw_input("\nPlease enter a valid endpoint (i.e. eu.wandera.com): ")

    #if option in [5,6,8]:
    puppetdir = raw_input("\n- Puppet directory: (default: ~/git/puppet)")
    if not puppetdir:
        puppetdir = os.path.expanduser("~/git/puppet")
    while not os.path.isdir(puppetdir):
        puppetdir = raw_input("{} is not a directory. Please enter a valid path for your puppet directory: ".format(puppetdir))

    options["puppetdir"] = puppetdir
    options["endpoint"] = endpoint

    check_branch = None
    stgn_puppet_branch = None
    rn_link = None
    ops_ticket = None
    force_deploy = False
    start_stopped_instances = False

    if option != 1:
        release_num = raw_input("\n- Release version number: ")
        rel_regex = re.compile("[0-9][0-9]-[0-9][0-9].*")
        while not rel_regex.search(release_num):
            release_num = raw_input("\nPlease enter a valid version. Release version number: ")
        options["release_num"] = release_num

    if option in [2,11]:
        ops_jira = get_release_ops_ticket(release_num,jira)
        if ops_jira is not None:
            #ops_ticket = ops_jira.key
            ops_ticket = interaction.ask_value("\n- OPS ticket ", default=ops_jira.key)
        else:
            ops_ticket = interaction.ask_value("\n- OPS ticket:")
        rn_link = None
        while True:
            try:
                ops_ticket_jira = jira.issue(ops_ticket)
                for line in ops_ticket_jira.fields.description.split('\n'):
                    if re.search(r'Release notes',line):
                        ticket_rn_link = re.sub(r'.*Release notes\|\[Link\|([^\]]*)\].*',r'\1',line)
                break
            except Exception as e:
                print e
                ops_ticket = raw_input("\nPlease enter a valid OPS ticket (OPS-XXXX): ")

        link_regex = re.compile("https://snappli.atlassian.net/wiki/spaces/DEV/pages/[0-9]{{1,}}/R{}.*\+Release\+Notes".format(release_num), re.IGNORECASE)
        rn_link = interaction.ask_value("\n- RN link ", default=ticket_rn_link)
        while not link_regex.search(rn_link) or requests.get(rn_link).status_code != 200:
            rn_link = interaction.ask_value("\nPlease enter a valid RN URL: ")
            while not link_regex.search(rn_link) or requests.get(rn_link).status_code != 200:
                rn_link = interaction.ask_value("\nPlease enter a valid RN URL: ")

    if option == 2:
        repo = Repo(puppetdir)
        stgn_puppet_branch = interaction.ask_value("\n- Staging Release branch ", default="release/{}".format(release_num))
        while True:
            try:
                repo.heads[stgn_puppet_branch]
                break
            except:
                stgn_puppet_branch = interaction.ask_value("\n- Plese enter a valid Staging Release branch:", default="release/{}".format(release_num))
        check_branch = interaction.ask_value_with_confirm("\n- Branch to use for tests ", default="release/{}".format(release_num))
        '''while True:
            try:
                repo.heads[check_branch]
                break
            except:
                check_branch = interaction.ask_value("\n- Plese enter a valid Branch to use for tests ", default="release/{}".format(release_num))'''
        if interaction.confirm("\n- Would you like to automatically perform full deployment? "):
            force_deploy = True
            if interaction.confirm("\n- Would you like to start stopped instances? ", default='n'):
                start_stopped_instances = True

    options["ops_ticket"] = ops_ticket
    options["rn_link"] = rn_link
    options["stgn_puppet_branch"] = stgn_puppet_branch
    options["check_branch"] = check_branch
    options["force_deploy"] = force_deploy
    options["start_stopped_instances"] = start_stopped_instances

    if option == 1:
        check_versions = True
    elif option == 11:
        dryrun = False
    else:
        if dryrun is None:
            dryrun = raw_input("\n- Dryrun (true/false): ")
            while dryrun.lower() not in [ "true", "false" ]:
                dryrun = raw_input("\n- Please enter a valid option. Dryrun (true/false): ")
            if dryrun.lower() == "true":
                dryrun = True
            else:
                dryrun = False
        options["dryrun"] = dryrun

        if option == 6:
            proxy_plan = raw_input("\n- Proxy plan (default: {}): ".format(WOOPAS_PLAN))
            if proxy_plan == "":
                proxy_plan = WOOPAS_PLAN
            while not os.path.isfile(proxy_plan):
                proxy_plan = raw_input("\n{} is not a directory.  Proxy plan (default: {}): ".format(proxy_plan, WOOPAS_PLAN))
                if proxy_plan == "":
                    proxy_plan = WOOPAS_PLAN

    options["proxy_plan"] = proxy_plan
    options["check_versions"] = check_versions
    options["environ"] = environ

    return options

def health_check(environ,release_num,before,check_branch=None):
    if check_branch is None:
        check_branch = "release/{}".format(release_num)

    if before:
        check_stage = "before"
    else:
        check_stage = "after"

    hc_status = True
    if environ in healthchecks_urls:
        logging.info('')
        logging.info('PERFORMING HEALTHCHECK for environment {}'.format(environ))
        hc_status = run_healthcheck(environ)
    if environ in acceptance_tests:
        logging.info('')
        logging.info('RUNNING ACCEPTANCE TESTS for environment {}'.format(environ))
        at_url = acceptance_tests[environ]['url']
        at_job = acceptance_tests[environ]['job']
        at_params = {'BRANCH' : check_branch}
        at_status = run_jenkins(url=at_url, job=at_job, params=at_params)
        if before and not at_status:
            logging.error('Acceptance Tests FAILED. Please fix and start again')
            sys.exit(1)
    if not before:
        if environ in healthchecks_urls:
            logging.info('')
            logging.info('PERFORMING HEALTHCHECK for environment {} after ATs'.format(environ))
            hc_status =  run_healthcheck(environ)

    if not before and environ == 'stgn':
        now = datetime.now()
        time_now = now.time()
        if time_now >= datetime.strptime(stgn_deploy_cutoff_time, "%I:%M%p").time():
            print(colored("\n\tIt is after {}. Please run a device enrolment test\n".format(stgn_deploy_cutoff_time), "yellow"))

    return hc_status

def main(option=None,dryrun=None,check_access=True):
    try:
        if not os.path.isfile(CONFIG_FILE):
            logging.error("Configuration file {} not found.\n".format(CONFIG_FILE))
            sys.exit(1)

        if check_access:
            failed_access = libcloud_api.check_cloud_access()
            clear_screen()
            if len(failed_access) > 0:
                logging.error("List of Failed accesses")
                for error in failed_access:
                    logging.error(colored("* {}".format(error),"red"))
                    for region in sorted(failed_access[error]):
                        logging.error("\t- {}".format(region))
                if not interaction.confirm("\nWould you wish to continue with deployment? "):
                    sys.exit(0)

        logging.getLogger().setLevel(logging.INFO)

        fab_user = init_ssh_user()
        options = menu(option,dryrun)
        environ = options["environ"]
        puppetdir = options["puppetdir"]
        force_weights = options["force_weights"]
        check_branch = options["check_branch"]
        stgn_puppet_branch = options["stgn_puppet_branch"]
        force_deploy = options["force_deploy"]
        start_stopped_instances = options["start_stopped_instances"]
        ops_ticket = options["ops_ticket"]
        rn_link = options["rn_link"]
        jira = options["jira"]

        jira_creds = util.get_jira_credentails()

        if options["check_versions"]:
            delta_tools_config_path ="{}/hieradata/environments/operations/common.eyaml".format(options["puppetdir"])
            release_service_versions = sorted(getServicesVersions(tools_path=delta_tools_config_path), key=lambda k: k['name'])
            #print json.dumps(release_service_versions,indent=4,sort_keys=True)
            clear_screen()
            for service in release_service_versions:
                print "- {}".format(service['name']),
                if 'alias' in service:
                    print "({}):".format(service['alias'])
                else:
                    print
                print "\t{}".format(service['version'])
        elif environ == 'create_cr':
            release_num = options["release_num"]
            do_cr_prep(release_num, ops_ticket, rn_link, get_jira_credentails())
        else:
            release_num = options["release_num"]
            dryrun = options["dryrun"]
            endpoint = options["endpoint"]

            global log_file
            if not dryrun:
                log_file = os.path.join(current_dir, "logs", "RL_{}_{}.log".format(release_num,environ))
            else:
                log_file = os.path.join(current_dir, "logs", "RL_{}_{}_dryrun.log".format(release_num,environ))

            # Add logging to the log file

            fileh = logging.FileHandler(log_file, 'a')
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            fileh.setFormatter(formatter)

            log = logging.getLogger()  # root logger
            try:
                log.removeHandler(fileh)
            except:
                pass
            log.addHandler(fileh)
            log.propagate = False

            if not display_options(fab_user, options):
                sys.exit(1)

            stgn_hc_status = True
            if environ == "stgn":
                if not dryrun:
                    stgn_hc_status = health_check(environ, release_num, before=True, check_branch=check_branch)
                puppet_master = puppet_masters['stgn']
                puppet_branch = "release/{}".format(release_num)
            else:
                puppet_master = puppet_masters['prod']
                puppet_branch = 'master'

            providers, provider_aliases = libcloud_api.get_providers()

            if "nodes_TODO_file" in options:
                TODOLIST = options["nodes_TODO_file"]
            else:
                TODOLIST = None

            if environ not in [ "prod-core", "core" , "radar" , "prod-proxy", "prod-endpoint" ]:
                # Getting the instances
                logging.info("Getting list of instances")
                all_instances = libcloud_api.get_all_instances()
            else:
                all_instances = None

            # Git Pull on puppet

            if not dryrun:
                logging.info("Running git pull in puppet master ({}:{})".format(puppet_master, puppet_master_dir))
                with settings(host_string = puppet_master, user = fab_user):
                    git_pull(puppet_master_dir)
            else:
                logging.info("Would run git pull in puppet master ({}:{})".format(puppet_master, puppet_master_dir))

            app_done = False

            logging.info("Getting list of RL tasks")
            rl_tasks = util.get_release_tasks(release_num, jira=jira)

            # read through the file of services to deploy and deploy to required instances
            if environ == "prod-proxy":
                if "proxy_plan" in options:
                    PROXY_PLAN = options["proxy_plan"]
                else:
                    PROXY_PLAN = WOOPAS_PLAN
                    if not interaction.confirm("Proxy default deployment plan: {}. Continue?".format(PROXY_PLAN)):
                        PROXY_PLAN = raw_input("Please enter the file with the woopas plan: ")
                    while not os.path.isfile(PROXY_PLAN):
                        PROXY_PLAN = raw_input("\"{}\" not found. Please enter a valid file: ".format(PROXY_PLAN))

                woopas_amigo.main(release_num,PROXY_PLAN,puppetdir=options["puppetdir"],dryrun=dryrun)
            elif environ in ["core","radar","apache"]:
                if environ == "apache":
                    deploy_core(fab_user,stack_type="core",dryrun=dryrun,apache=True,force_weights=force_weights)
                else:
                    deploy_core(fab_user,environ,dryrun,force_weights=force_weights,rl_tasks=rl_tasks)
                logging.info("\nLOG file: {}\n".format(log_file))
                if not dryrun:
                    check_all_versions.main(puppetdir=puppetdir, environment="production", logfile=log_file, all_instances=all_instances)
            elif environ == "prod-endpoint":
                deploy_endpoint(endpoint,fab_user,dryrun,force_weights=force_weights)
                if not dryrun:
                    check_all_versions.main(puppetdir=puppetdir, environment="production", logfile=log_file, all_instances=all_instances)
            elif environ == "canary-servers":
                for canary in canary_servers:
                    canary_regex = re.compile(canary)
                    for driver in all_instances:
                        for instance in all_instances[driver]:
                            if canary_regex.search(instance.name):
                                host = instance.public_ip
                    if deploy(host,fab_user,canary,dryrun=dryrun) != 0:
                        if interaction.confirm("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes?".format(name)):
                            continue
                        else:
                            sys.exit(1)
            else:
                if environ == 'stgn':
                    if not stgn_hc_status:
                        logging.warning('')
                        logging.warning('Healthcheck returned errors. See full status above')
                        logging.warning('')
                        if interaction.ask_value("Continue? ", choices=['continue','exit']) == 'e':
                            logging.warning("User aborted execution at before tasks")
                            sys.exit(0)

                    print(colored("Please complete before tasks. Will resume when done. You can press enter when completed to continue","yellow"))
                    logging.info(colored("List of before tasks:","cyan"))
                    before_tickets = rl_tasks['before']
                    for bticket in before_tickets:
                        logging.info("\t{0} (https://snappli.atlassian.net/browse/{0}) - '{1}'".format(bticket.key, util.get_ticket_status(bticket.key, jira=jira)))
                    if not dryrun:
                        try:
                            wait_tasks_done(before_tickets,'staging','before',jira)
                        except KeyboardInterrupt:
                            if not dryrun:
                                slack_post("{} deployment ABORTED before BEFORE TASKS completed".format(environ.upper()))
                            logging.warning("User aborted during before tasks")
                            if interaction.confirm('\n\nWould you like to run another step? '):
                                main(check_access=False)
                            else:
                                sys.exit(1)

                if environ == "core-full":
                    deploy_core(fab_user,"core",dryrun,force_weights=force_weights)

                if environ in env_service_types:
                    my_service_types = env_service_types[environ]
                else:
                    all_service_types = []
                    for service in service_config.ALL_SERVICES:
                        if not service.get('deprecated') and not service.get('tools') and not service.get('opscenter') and service['node'] not in all_service_types:
                            all_service_types.append(service['node'])
                    my_service_types = all_service_types

                if 'app' in my_service_types:
                    my_service_types.remove('app')
                    my_service_types = ['app'] + sorted(my_service_types)

                if 'web' in my_service_types:
                    my_service_types.remove('web')
                    my_service_types = my_service_types + ['web']

                if 'prx' in my_service_types:
                    my_service_types.remove('prx')
                    my_service_types = my_service_types + ['prx']

                for service_type in my_service_types:
                    if ( service_type == "app" or service_type == "web" ) and ( environ == "core-full" or environ == "prod-services" ):
                        logging.info('')
                        logging.warning("Skipping deployment of Core \"{}\" services".format(service_type))
                        continue

                    if service_type in woopas_service_types and environ in live_environments and environ != "proxy":
                        logging.info('')
                        logging.warning("Skipping {} deployment. Incompatible type: woopas service not run as --env proxy **".format(service_type))
                        logging.info('')
                        sleep(3)
                        continue

                    logging.info("\nDeploying to {} servers".format(service_type))
                    if service_type == "bea" and environ == "stgn":
                        logging.warning("Switching server type bea to app in staging")
                        service_type = "app"

                    if service_type == "app" and app_done:
                        logging.info("deployment already done on app servers. Skipping")
                        continue

                    logging.info("- services running:")
                    for service in service_config.ALL_SERVICES:
                        if not service.get('deprecated') and service['node'] == service_type:
                            logging.info("\t- {}".format(service['name']))
                    if not force_deploy and not interaction.confirm("\nWould you like to deploy to {} servers? ".format(service_type),default='y'):
                        continue

                    instances_TODO = {}

                    if (environ in live_environments and service_type in [ "fis", "dls" ]) or (environ in ['tools','opscenter']):
                        service_regex = re.compile("^{}-[0-9]*-cops".format(service_type), re.VERBOSE)
                    elif service_type == "vid":
                        service_regex = re.compile("^{}-[0-9]*-prxy.*wandera\.{}".format(service_type, env_extensions[environ.split('-')[0]]), re.VERBOSE)
                    elif environ in live_environments:
                        service_regex = re.compile("^{}-[0-9]*-core".format(service_type), re.VERBOSE)
                    elif service_type == "prx" and environ == "stgn":
                        service_regex = re.compile("(^{}-[0-9]*-{}|\.node\..*wandera\.{})".format(service_type, environ, env_extensions['stgn']), re.VERBOSE)
                    else:
                        service_regex = re.compile("^{}-[0-9]*-{}".format(service_type, environ), re.VERBOSE)
                    if TODOLIST is None:
                        for driver in all_instances:
                            for instance in all_instances[driver]:
                                if service_regex.search(instance.name):
                                    name = instance.name
                                    instances_TODO[name] = { "instance" : instance , "provider" : driver }
                    else:
                        with open(TODOLIST, "r") as todo_nodes:
                            for line in todo_nodes:
                                line = line.rstrip()
                                node = line.rsplit(',')[0]
                                node_regex = re.compile(node)
                                if nodes_todo_regex.search(line) and service_regex.search(node):
                                    logging.info( "Looking for {} instance for {}".format(service_type, node))
                                    found = False
                                    for driver in all_instances:
                                        for instance in all_instances[driver]:
                                            #logging.info("{}".format(instance.name))
                                            if node_regex.search(instance.name):
                                                instances_TODO[node] = { "instance" : instance , "provider" : driver }
                                                found = True
                                                break
                                        if found:
                                            break
                                    if not found:
                                        logging.error("{} not found".format(node))

                    if len(instances_TODO) == 0:
                        logging.info('')
                        logging.info("*** No nodes found for service type \"{}\". Skipping... ***".format(service_type))
                        logging.info('')
                        sleep(3)
                    else:
                        logging.info('')
                        logging.info("** Will deploy on:")
                        for name in instances_TODO:
                            #logging.info("\t- {} ({})".format(name, vars(instances_TODO[name])))
                            if name in canary_servers:
                                logging.info("\t- {} on {} ({}) -- CANARY".format(name, instances_TODO[name]["provider"], instances_TODO[name]["instance"].state),"yellow")
                            else:
                                logging.info("\t- {} on {} ({})".format(name, instances_TODO[name]["provider"], instances_TODO[name]["instance"].state))

                        if force_deploy or interaction.confirm('Continue? '):
                            for name in instances_TODO:
                                instance = instances_TODO[name]["instance"]
                                state = instance.state
                                host = instance.public_ip

                                logging.info('')
                                logging.info("* Deploying to {} - {} (currently {})".format(name, host, state))
                                logging.info('')
                                if not dryrun:
                                    if not force_deploy and not interaction.confirm("Proceed with deployment on {} ({})? ".format(name, host)):
                                        continue
                                    if state != "running":
                                        if not force_deploy:
                                            option = stopped_instance_options(name)
                                        else:
                                            if start_stopped_instances:
                                                option = '1'
                                            else:
                                                option = '3'
                                        if option == '3':
                                            logging.info("Skipping deployment of {}".format(name))
                                            continue
                                        else:
                                            if option == '1':
                                                #driver = instances_TODO[name]["driver"]
                                                logging.info("Starting {}".format(name))
                                                #sleep(1)
                                                if not dryrun:
                                                    libcloud_api.start_instance(name)
                                                    logging.info("Waiting for {} to be fully up".format(name))
                                                    sleep(120)
                                            deploy_return = deploy(host,fab_user,name,dryrun=dryrun,force_deploy=force_deploy, environ=environ)
                                            if deploy_return == 3:
                                                continue
                                            elif deploy_return != 0:
                                                slack_post("ERROR: Deployment failed on {}".format(name))
                                                if force_deploy or interaction.confirm("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of {} nodes? ".format(name,service_type)):
                                                    continue
                                                else:
                                                    break
                                            elif service_type == "app":
                                                    app_done = True
                                            if not dryrun:
                                                if force_deploy or interaction.confirm("Would you like to re-stop the instance? "):
                                                    logging.info("Stopping {}".format(name))
                                                    libcloud_api.stop_instance(name)
                                                    sleep(5)
                                            logging.info("Deployment to {} completed".format(name))

                                    else:
                                        if deploy(host,fab_user,name,dryrun=dryrun,force_deploy=force_deploy,environ=environ) != 0:
                                            slack_post("ERROR: Deployment failed on {}".format(name))
                                            if force_deploy or interaction.confirm("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of {} nodes? ".format(name,service_type)):
                                                continue
                                            else:
                                                break
                                        else:
                                            if service_type == "app":
                                                app_done = True

                                            if TODOLIST is not None:
                                                update_node_status(name, TODOLIST)

                                            logging.info("Deployment to {} completed".format(name))
                                            if not force_deploy and not interaction.confirm("\nDeployment of {} completed. Continue with rest of {} nodes? ".format(name,service_type)):
                                                logging.info("Deployment aborted by user")
                                                break
                    if environ == "core-full":
                        deploy_core(fab_user,"radar",dryrun,force_weights=force_weights)
                print(colored("\nLOG file: {}\n".format(log_file),"green"))

                if environ == 'stgn':
                    print(colored("Please complete after tasks. Will resume when done. You can press enter when completed to continue","yellow"))
                    logging.info("List of after tasks:")
                    after_tickets = rl_tasks['after']
                    for aticket in after_tickets:
                        logging.info("\t{0} (https://snappli.atlassian.net/browse/{0}) - '{1}'".format(aticket.key, util.get_ticket_status(aticket.key, jira=jira)))
                    if not dryrun:
                        try:
                            wait_tasks_done(after_tickets,'staging','after',jira)
                        except KeyboardInterrupt:
                            if not dryrun:
                                slack_post("{} deployment ABORTED before AFTER TASKS completed".format(environ.upper()))
                            logging.warning("User aborted during after tasks")
                            if interaction.confirm('\n\nWould you like to run another step? '):
                                main(check_access=False)
                            else:
                                sys.exit(1)

                if not dryrun:
                    health_check(environ, release_num, before=False, check_branch=check_branch)

                if not dryrun:
                    if environ in live_environments:
                        check_all_versions.main(puppetdir=puppetdir, environment="production", logfile=log_file, all_instances=all_instances)
                    elif environ in ['staging','stgn']:
                        check_all_versions.main(puppetdir=puppetdir, environment="staging", logfile=log_file, all_instances=all_instances, ref_branch=stgn_puppet_branch, force_deploy=force_deploy)

                #print (colored("\nPlease run 'python ./check_all_versions.py' to get the versions deployed and running on the platform", "cyan"))

        fabric.state.output.status = False
        fabric.network.disconnect_all()

        if environ == 'stgn' and not dryrun:
            if not interaction.confirm("Would you like to create the CR for Production? "):
                print "\nYou can create it at any step by running OPTION 11\n"
            else:
                #jira_user, jira_pwd = get_jira_credentails()
                # do_cr_prep(args.release,args.ticket,tuple(args.creds),rn_link=args.rn)
                do_cr_prep(release_num, ops_ticket, rn_link, get_jira_credentails())
    except KeyboardInterrupt:
        pass

    if interaction.confirm('\n\nWould you like to run another step? '):
        main(check_access=False)


#############  MAIN  #############

if __name__ == "__main__":
    logging.basicConfig(level='INFO', format='%(levelname)s: %(message)s')
    option = None
    dryrun = None
    options = parse_options(sys.argv[1:])
    if options.option is not None:
        try:
            option = int(options.option)
        except:
            for opt in env_opt_mapping:
                if env_opt_mapping[opt] == options.option:
                    option = opt
    if options.dryrun is not None:
        if options.dryrun == 'true':
            dryrun = True
        else:
            dryrun = False
    main(option,dryrun)
