import logging
import os
import csv
import libcloud_api
import interaction
import re

def rangeDistributor(total, days=5):
    days -= 1
    days_weight = [i/2 for i in range(2, days+2)]
    total_weight = sum(days_weight)
    days_percent = [i*100/total_weight for i in days_weight]
    days_count = [i*total/100 for i in days_percent]

    #Adding canary day
    days_weight.insert(0,0)
    days_percent.insert(0,0)
    days_count.insert(0,1)

    full_count = sum(days_count)
    days_count[-1]+= total-full_count
    assert (total == sum(days_count))

    for i, weight in enumerate(days_weight):
        logging.info("Day %s:", i)
        logging.info("\tNumber of proxys: %s: (%s%% / Weight %s)", days_count[i],days_percent[i],weight)
    return days_count

def makeProxyPlan(days=5):

    if days < 2:
        logging.critical("Impossible to deploy in so short time")
        exit(1)
    #Get the list of regions to upgrade
    providers = {}
    region_plan_path=os.path.dirname(os.path.realpath(__file__))+'/plans/regions.csv'
    logging.info("Checking region plan in %s", region_plan_path)
    with open(region_plan_path, 'r') as region_plan_file_r:
        csv_reader = csv.DictReader(region_plan_file_r, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for row in csv_reader:
            #Convert Boolean
            row['upgrading'] = (row['upgrading'] == 'True')
            if row["upgrading"]:
                provider_name = row["provider"]
                if provider_name not in providers:
                    providers[provider_name] = []
                providers[provider_name].append(row["region"])
                logging.info("Upgrading Provider %s - Region: %s", row["provider"], row["region"])

    if not interaction.confirm("Upgrading regions correct?"):
        logging.info("Please update %s and start again", region_plan_path)
        exit(0)

    #Initialise the libcloud drivers
    drivers = {}
    logging.info("Initialising drivers")
    for provider,regions in providers.items():
        logging.info("Getting Drivers for %s : %s", provider, regions)

        libcloud_init_map = {
            'openstack':libcloud_api.init_openstack_driver,
            'rackspace':libcloud_api.init_rackspace_driver,
            'ec2':libcloud_api.init_ec2_driver,
            'softlayer':libcloud_api.init_softlayer_driver
        }
        prov_drivers = libcloud_init_map[provider.lower()](regions)
        if len(prov_drivers) > 0:
            drivers.update(prov_drivers)

    #Pull all the instances
    logging.info("Getting all instances from %s providers", len(drivers.keys()))
    all_instances = libcloud_api.get_all_instances(drivers)

    #Filter for proxies only
    all_proxies = {}
    logging.info("Filtering instances to get only proxies")
    proxy_name_regex = re.compile("[0-9a-zA-Z]*\.node\.[^.]+\.[^.]+\.wandera.com")
    for driver_name in all_instances:
        for instance in all_instances[driver_name]:
            if proxy_name_regex.match(instance.name):
                provider = driver_name.split('_')[0]
                if provider in all_proxies: # add the instance to the list of instances associated to the provider
                    all_proxies[provider].append(instance)
                else:
                    all_proxies[provider] = [instance]
    sum_proxy = 0
    for provider, proxies in all_proxies.items():
        logging.info("Provider: %s, Number of Proxy: %s", provider, len(proxies))
        sum_proxy += len(proxies)

    logging.info("Releasing to %s proxies in %s days", sum_proxy, days)
    days_count = rangeDistributor(sum_proxy, days)
    order_proxies = []
    if 'RACKSPACE' in all_proxies:
        #Find canary
        canary_instance = next(obj for obj in all_proxies['RACKSPACE'] if obj.name == "e00817d4ed17a9e9.node.eu-west-2a.gb.wandera.com")
        order_proxies.append([canary_instance])
        logging.info("Found canary, listing for first day")
        days_count.pop(0)
    proxies_lists = sorted([proxies for proxies in all_proxies.values()], key=lambda k: len(k))

    logging.info("Picking instances")
    for count in days_count:
        cur_proxies = []
        provider_index = 0
        while count > 0:
            if len(proxies_lists[provider_index]) != 0 :
                cur_proxies.append(proxies_lists[provider_index].pop(0))
                count -=1
            provider_index = (provider_index + 1) % len(proxies_lists)
        cur_proxies = sorted(cur_proxies, key=lambda k: k.name[::-1])
        order_proxies.append(cur_proxies)

    release_plan_path=os.path.dirname(os.path.realpath(__file__))+'/plans/release.csv'
    with open(release_plan_path, 'w') as release_plan_file_w:
        mwriter = csv.writer(release_plan_file_w, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        headers = ['day','proxy','status']
        mwriter.writerow(headers)
        for i, cur_proxies in enumerate(order_proxies):
            for cur_proxie in cur_proxies:
                row = [i, cur_proxie.name,'TODO']
                mwriter.writerow(row)
    logging.info("Plan created at %s", release_plan_path)


if __name__ == "__main__":
    logging.basicConfig(level='INFO', format='%(levelname)s: %(message)s')
    makeProxyPlan()
