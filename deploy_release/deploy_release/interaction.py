import json
import os
import logging
import subprocess
import platform

INTERACTIVE=True
CACHE_DEFAULT=None

def clear_screen():
    clear = subprocess.Popen( "cls" if platform.system() == "Windows" else "clear", shell=True)
    clear.communicate()

def get_cache_default(key, default=None):
    if CACHE_DEFAULT is None:
        load_cache_default()
    return CACHE_DEFAULT.get(key, default)


def set_cache_default(key, value):
    if key is not None:
        CACHE_DEFAULT[key] = value
        save_cache_default()


def load_cache_default():
    global CACHE_DEFAULT
    cache_interact_path = os.path.dirname(os.path.realpath(__file__)) + '/cache/interaction_default.csv'
    if not os.path.isfile(cache_interact_path):
        CACHE_DEFAULT = {}
        save_cache_default()
    else:
        with open(cache_interact_path, 'r') as cache_interact_file_r:
            CACHE_DEFAULT = json.load(cache_interact_file_r)


def save_cache_default():
    cache_interact_path = os.path.dirname(os.path.realpath(__file__)) + '/cache/interaction_default.csv'
    with open(cache_interact_path, 'w') as cache_interact_file_w:
        json.dump(CACHE_DEFAULT, cache_interact_file_w)


def set_interactive(status):
    global INTERACTIVE
    INTERACTIVE = (status == True)


def confirm(message, default='y', key=None, casesensitive=False):
    """Print a message and return the user's confirmation as a boolean."""
    confirm = ask_value(message,default=default, choices=['yes','No'], key=key, casesensitive=casesensitive)

    return confirm == 'y'


def ask_value(message, default=None, choices=None, key=None, casesensitive=False):
    """Print a message and return the user inputed first character in lower"""
    if key is not None:
        default = get_cache_default('value_'+key, default)
    if choices is not None:
        cleaned_choices = []
        for choice in choices:
            if len(choice) > 1:
                choice = choice[0]+'('+choice[1:]+')'
            cleaned_choices.append(choice)
        message += '(Options %s): ' % cleaned_choices
        if not casesensitive:
            choices = [choice[0].lower() for choice in choices]
    if default is not None:
        message += '(Default \'%s\'): ' % default

    value = None
    while value is None:

        if not INTERACTIVE and default is not None:
            logging.info("Not interactive '%s'", message)
            logging.info("Using default value '%s'", default)
            selection = default
        else:
            selection = raw_input(message)
        if selection:
            if choices is None or selection.lower() in choices:
                #TODO in non interactive we might create an infinite loop
                value = selection
            if key is not None:
                set_cache_default('value_'+key, value)
        elif default is not None:
            value = default
    #always return in lower
    if not casesensitive:
        value = value.lower()
    return value

def ask_value_with_confirm(message, default=None, key=None, casesensitive=False):
    accepted = False
    while not accepted:
        value = ask_value(message, default, key, casesensitive=casesensitive)
        accepted = confirm("Confirm value %s" % value,key=key)
    return value
