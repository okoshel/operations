#!/usr/local/bin/python
import libcloud_api
from libcloud_api import *
import sys
import subprocess
from subprocess import PIPE, Popen

'''lb_region = "eu-west-1"

def get_LB_driver(region=None):
    if region is None:
        region = lb_region

    cls = libcloud.loadbalancer.providers.get_driver(libcloud.loadbalancer.types.Provider.ELB)
    driver = cls("AKIAIO247XZFFOOL53QQ","KKY9mPAjqtez5yIWruqTvByrawrKrj3WUE9aWYxC", region=region)
    return driver

def get_LB_list(driver=None, region=None):
    if driver is None:
        driver = get_LB_driver(region)
    return driver.list_balancers()

def get_LB_members(name, lbs=None):
    driver = get_LB_driver(lb_region)
    if lbs is None:
        lbs = get_LB_list(driver, lb_region)

    for lb in lbs:
        if lb.name == name:
            balancer = driver.get_balancer(balancer_id = lb.id)
            return balancer.list_members()'''

if __name__ == "__main__":
    #lbs = get_LB_members(lb_region)
    lb_regex = re.compile("-core|-cops")
    for region in aws_regions:
        print "\n>> {} <<\n".format(region)
        try:
            LBs = get_LB_list(region=region)
            for lb in LBs:
                #if lb_regex.search(lb.name):
                print "{}".format(lb)
                ssl_cmd = "openssl s_client -connect {}:443 -cipher DES-CBC3-SHA".format(lb.ip)
                cmd_open = Popen(ssl_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
                cmd_open.communicate()
                print "\n=========================\n"
        except Exception as e:
            print "WARN: Failed to get LBs for {}\n{}".format(region,e)
    sys.exit(0)
    if len(sys.argv) != 4:
        sys.exit("USAGE: {} stack weight".format(sys.argv[0]))

    update_endpoint_weight(sys.argv[1],sys.argv[2],int(sys.argv[3]))
    #update_route53_weight(sys.argv[1],int(sys.argv[2]))
    sys.exit(0)
    '''for lb in lbs:
        print "{}".format(lb)'''
    '''aws_drivers = init_ec2_driver([lb_region])
    aws_instances = get_all_instances(aws_drivers)'''

    '''print "\nGetting members in elb-002-core\n"
    balancer, members = get_LB_members("elb-002-core")
    for member in members:
        for driver_name in aws_instances:
            for instance in aws_instances[driver_name]:
                if instance.id == member.id:
                    print "{}, {}".format(member.id, instance.name)

    print "------------------"
    detach_LB_member("web-201-core.eu-west-1b.ie.wandera.com","elb-002-core")
    print "------------------"
    attach_LB_member("web-201-core.eu-west-1b.ie.wandera.com","elb-002-core")
    print "------------------"'''

    '''dns_driver = get_DNS_driver()
    print "Route 53 zones:"
    for zone in dns_driver.list_zones():
        print "{}".format(zone)'''

    '''print "------------------"
    r53_records = get_DNS_Zone_records("wandera.com")
    for record in r53_records:
        print "{}".format(record)
    print "------------------"'''

    '''record_name = "ap"
    print "Details for {}".format(record_name)
    records = get_DNS_records(record_name=record_name, myzone="wandera.com")
    if not records:
        print "ERROR: Failed to get details for {}".format(record_name)
    else:
        for record in records:
            print "{}".format(record.__dict__)'''

    '''dt_drivers = init_dt_driver()
    for driver in dt_drivers:
        print "{}: {}\n{}".format(driver, dt_drivers[driver], dir(driver))
        print "\n{}".format(dt_drivers[driver].list_nodes())'''
    if len(sys.argv) != 4:
        sys.exit("ERROR: usage {} <attach/detach> <instance_name> <load_balance_name>".format(sys.argv[0]))

    action = sys.argv[1]
    instance = sys.argv[2]
    lb = sys.argv[3]

    if action == "attach":
        if attach_LB_member(instance, lb) is not None:
            print "{} attached successfully to {}".format(instance, lb)
        else:
            print "ERROR: Failed to attach {} to {}".format(instance, lb)
    elif action == "detach":
        if detach_LB_member(instance, lb):
            print "{} detached successfully from {}".format(instance, lb)
        else:
            print "ERROR: Failed to detach {} from {}".format(instance, lb)
