import libcloud_api
import os
import sys
import re
from fabric.context_managers import settings
import fabfile
from time import sleep

all_instances = libcloud_api.get_all_instances()
ssh_user = libcloud_api.init_ssh_user()

for driver_name in all_instances:
    print "\nRestarting proxies in {}\n".format(driver_name)
    for instance in all_instances[driver_name]:
        proxy_regex = re.compile(".*\.node\..*\.wandera\.com")
        if proxy_regex.search(instance.name):
            proxy = instance.name
            print "Restarting {}".format(proxy)
            try:
                with settings(host_string=proxy, user=ssh_user):
                    fabfile.service_restart("proxy")
                    sleep(15)
                    fabfile.check_proxy()
                    sleep(10)
            except Exception as e:
                print "\nERROR: Failed to restart {}\nException: {}\n".format(proxy, e)
            print "\n===========================================================\n"
