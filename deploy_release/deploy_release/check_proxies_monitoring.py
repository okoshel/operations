from woopas_amigo import check_proxies_monitoring
import logging
import colorer
from datetime import datetime
import os

today = datetime.now().strftime('%Y-%m-%d')
log_file = "proxy_monitoring_status.{}".format(today)

if __name__ == '__main__':
    logging.basicConfig(level="INFO", format='%(asctime)s %(levelname)s %(message)s')

    if os.path.isfile(log_file):
        open(log_file, 'w').close()

    fileh = logging.FileHandler(log_file, 'a')
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fileh.setFormatter(formatter)

    log = logging.getLogger()  # root logger
    try:
        log.removeHandler(fileh)
    except:
        pass
    log.addHandler(fileh)
    log.propagate = False

    check_proxies_monitoring()

    print "\nInfo available at {}\n".format(log_file)
