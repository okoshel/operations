#!/usr/local/bin/python

#####################################################################
# deploy_release.py
# Author: Arturo Noha
# Version: 3.2
#------------------------------------------------------------------
# USAGE: ./deploy_release.3.2.py --rel <release_num> -u <ssh_username> --env <environment> --relservices <release_services_file> --dryrun <True/False> [options]
#	* Available environments:
#	- stgn: staging
#	- intg: integration
#	- prod-proxy: production proxies (via woopas). Requires proxy deployment plan file
#	- prod-core: app and portal in prodution ==> Set stack weight to 0/1. Option to only ser APP or PORTAL + Detaches instances from Load Balancers
#	- prod-services: non app backend services (siem, connector, delta, filterservice...). Requires release services file
#	- core-full: all production services (app, portal, other services). Requires release services file
#	* Options:
#	--allservices: file with the inforamtion of all available services. Default: ./release_services/all_services.info
#	--nodes: File with the list of nodes to deploy to if not all default nodes required
#	--proxy-plan: File with the daily proxy deployment plan per region. Default: ./release_Proxy_nodes/proxy_deployment_plan.csv
#####################################################################

import os
import sys
import logging
import prepare_version_file

from setuptools.command.easy_install import main as install
with open("requirements.txt", "r") as requirements:
    for line in requirements:
        package = line.rstrip()
        try:
            __import__(package)
        except:
            print "Missing package: {}".format(package)
            install([package])

#print "\n{}\n".format(sys.path)
'''sys.path.append(os.path.abspath('./deploy_release'))
print "\n{}\n".format(sys.path)'''

from pprint import pprint
import argparse
import re
import subprocess
from subprocess import Popen, PIPE

from fabric.context_managers import settings
from fabric.api import env
import imp
from datetime import date, datetime
from time import sleep
import platform
from termcolor import colored

#import deploy_release
#from deploy_release import *
import service_config
import libcloud_api
import woopas_amigo
'''from deploy_release import service_config
from deploy_release import libcloud_api
from deploy_release import woopas_amigo'''
from service_config import *
from fabfile import *

#woopas = imp.load_source("woopas_amigo", "/Users/arturonoha/Documents/bitbucket/wandera_woopas/Woopas_PY/woopas_amigo.py")
env.use_ssh_config = True

today = date.today()
DATE = today.isoformat()
today_regex = re.compile("{},(.*),TODO".format(DATE))
nodes_todo_regex = re.compile("[^,]*,TODO")

current_dir = os.getcwd()
log_file = os.path.join(current_dir,"logs")
WOOPAS_PLAN = os.path.join(current_dir, "release_Proxy_nodes", "proxy_deployment_plan_ALL.csv")
update_versions_script = os.path.join(current_dir, "prepare_version_file.sh")

def clear_screen():
    clear = Popen( "cls" if platform.system() == "Windows" else "clear", shell=True)
    clear.communicate()

def update_node_status(node,TODOLIST):
    temp_file_name = "{}.tmp".format(TODOLIST)
    with open(TODOLIST, "r")  as todo_file, open(temp_file_name,"w") as temp_file:
        for line in todo_file:
            update_regex_start = re.compile("^{},TODO".format(node))
            replace_str_start = "{},DONE".format(node)
            line = update_regex_start.sub(replace_str_start, line)

            update_regex_middle = re.compile(",{},TODO".format(node))
            replace_str_middle = ",{},DONE".format(node)
            line = update_regex_middle.sub(replace_str_middle, line)

            temp_file.write(line)
    os.remove(TODOLIST)
    os.rename(temp_file_name, TODOLIST)

def get_aws_instances():
    options = parse_options(sys.argv[1:])
    regions = libcloud_api.get_regions('EC2')
    print_log(log_file, "{}".format(regions))

def get_input(message = None):
    if message is None:
        display_message = "Would you wish to continue? [Y/n]: "
    else:
        display_message = message + " [Y/n]: "

    option = raw_input(display_message)
    while option not in valid_options:
        option = raw_input("Please enter a valid option [Y/n]: ")

    return option

def stopped_instance_options(name = ""):
    options = ['1','2','3']
    option = raw_input("WARNING: {} is stopped. What would you wish todo?\n".format(name) +
                    "\t1. Start and deploy\n" +
                    "\t2. Deploy without starting\n" +
                    "\t3. Skip\n" +
                    "Your choice: ")

    while option not in options:
        print "$ Please select a valid option: {}".format(options)

    return option

def deploy(myhost, user, name, dryrun=True):
    if not dryrun:
        try:
            with settings(host_string = myhost, user = user):
                return deploy_server(name)
        except Exception as e:
            print_log(log_file, "Could not deploy on {}".format(myhost), "ERROR")
            print_log(log_file, "Exception: {}".format(e.message))
            print_log(log_file, "{}".format(e))
            return -1
    else:
        return 0

def deploy_core(user,dryrun=True):
    lb_provider = "EC2"
    lb_region = "eu-west-1"
    lb_driver = libcloud_api.get_LB_driver(lb_region)


    ec2_drivers = libcloud_api.init_ec2_driver([lb_region])
    ec2_instances = libcloud_api.get_all_instances(ec2_drivers)

    stack_out_weight = 0
    stack_in_weight = 255
    stack_startup_weights = [10,50,150,stack_in_weight]
    stack_removal_weights = [stack_out_weight,50,150]

    if dryrun in [ False, "False", "false" ]:
        print "\n"
        print_log(log_file, "Setting Stacks to working weight: {}".format(stack_in_weight))
        for stack in sorted(stacks_info.keys()):
            if get_input("Will set weight of Stack {} to {}. Proceed?".format(stack, stack_in_weight)) != "Y":
                if get_input("Do you wish to continue with the other stacks?") != "Y":
                    sys.exit(0)
                else:
                    continue
            else:
                if libcloud_api.update_route53_weight(stack, stack_in_weight):
                    print_log(log_file, "-- Waiting for Stack {} to settle".format(stack))
                    sleep(15)
                else:
                    if get_input("ERROR: Failed to set weight to {}. Continue?".format(stack_in_weight)) != "Y":
                        sys.exit("\n** Deployment aborted by user **\n")
    else:
        print "\n"
        print_log(log_file, "Will set all Stacks to working weight: {}".format(stack_in_weight))

    for stack in sorted(stacks_info.keys()):
        if dryrun in [ False, "False", "false" ]:
            print "\n"
            print_log(log_file, "* Starting deployment to Stack {}".format(stack))

            if get_input("Will phase out Stack {}. Proceed?".format(stack)) != "Y":
                if get_input("Do you wish to continue with the other stacks?") != "Y":
                    sys.exit(0)
                else:
                    continue

            for weight in sorted(stack_removal_weights, key=int, reverse=True):
                if weight == stack_in_weight:
                    continue
                if libcloud_api.update_route53_weight(stack, weight):
                    print_log(log_file, "-- Waiting for Stack {} to settle".format(stack))
                    sleep(stack_phase_sleep)
                else:
                    if get_input("ERROR: Failed to set weight to {}. Continue?".format(weight)) != "Y":
                        sys.exit("\n** Deployment aborted by user **\n")

            for lb_instance in sorted(stacks_info[stack].keys()):
                if get_input("\n-- Starting deployment to instances behind {}".format(lb_instance)) != "Y":
                    print_log(log_file, "Skipping deployment of {} insntances as per user request".format(lb_instance))
                    continue
                for lb in sorted(stacks_info[stack][lb_instance]["lbs"]):
                    if get_input("\n. Continue? ({} will be detached from {})".format(lb_instance, lb)) == "n":
                        sys.exit("\n ** Deployment aborted by user **")

                    print "\n"
                    print_log(log_file, "* Detaching {} from {}".format(lb_instance, lb))
                    if not libcloud_api.detach_LB_member(lb_instance, lb, lb_driver):
                        print "\n"
                        print_log(log_file, "Failed to detach {} from {}. Please check manually.".format(lb_instance, lb), "ERROR")
                        if get_input("Would you wish to continue with deployment?") != "Y":
                            sys.exit(1)
                    else:
                        print_log(log_file, "* {} successfully detached from {}\nWaiting for it to fully take effect".format(lb_instance, lb))
                        sleep(15)

                for node in stacks_info[stack][lb_instance]["instances"]:
                    for driver_name in ec2_instances:
                        for ec2_instance in ec2_instances[driver_name]:
                            if ec2_instance.name == node:
                                ip_regex = re.compile("^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$")

                                for ind in range(0,len(ec2_instance.public_ips)):
                                    node_ip = ec2_instance.public_ips[ind]
                                    if ip_regex.search(node_ip):
                                        break
                    if get_input("\n-- Would you like to deploy to {} ({})".format(node, node_ip)) != "Y":
                        print_log(log_file, "Skipping deployment of {} as per user request".format(node))
                        continue
                    print "\n"
                    print_log(log_file, "* Starting deployment to {} ({})".format(node, node_ip))
                    deploy_return = deploy(node_ip, user, node, dryrun)
                    if deploy_return != 0:
                        if get_input("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes?".format(node)) != "Y":
                            sys.exit(1)
                    if get_input("\n-- Continue with the rest of nodes?") != "Y":
                        sys.exit("\n** Deployment aborted by user **\n")

                for lb in sorted(stacks_info[stack][lb_instance]["lbs"]):
                    print "\n"
                    print_log(log_file, "* Re-attaching {} to {}".format(lb_instance, lb))
                    if libcloud_api.attach_LB_member(lb_instance, lb, lb_driver) == False:
                        if get_input("ERROR: There was a problem reattching {}. Continue deployment?".format(lb_instance)) != "Y":
                            sys.exit(1)
                    else:
                        print_log(log_file, "* {} successfully re-attached to {}\nWaiting for it to fully take effect".format(lb_instance, lb))
                        sleep(15)
            print "\n"
            print_log(log_file, "- Reactivating Stack {}".format(stack))
            for weight in sorted(stack_startup_weights, key=int):
                if libcloud_api.update_route53_weight(stack, weight):
                    print_log(log_file, "-- Waiting for Stack {} to settle".format(stack))
                    sleep(stack_phase_sleep)
                else:
                    if get_input("ERROR: Failed to set weight to 1. Continue?") != "Y":
                        sys.exit("\n** Deployment aborted by user **\n")

            print "\n"
            print_log(log_file, "** Deployment to Stack {} completed".format(stack))
            if get_input("Continue?") != "Y":
                sys.exit(1)
        # DRYRUN
        else:
            print "\n"
            print_log(log_file, "* Starting deployment to Stack {}".format(stack, dryrun))
            for weight in sorted(stack_removal_weights, key=int, reverse=True):
                if weight == stack_in_weight:
                    continue
                print "\n"
                print_log(log_file, "Will set stack {} to {} weight".format(stack,weight))
                print_log(log_file, "- will wait {} min for Stack {} to settle".format(stack_phase_sleep/60,stack))
                sleep(2)
            for lb_instance in sorted(stacks_info[stack].keys()):
                print "\n"
                print_log(log_file, "* Starting deployment to {}".format(lb_instance))
                print "\n"
                for lb in sorted(stacks_info[stack][lb_instance]["lbs"]):
                    print_log(log_file, "Will detach {} from {}".format(lb_instance, lb))

                for node in sorted(stacks_info[stack][lb_instance]["instances"]):
                    for driver_name in ec2_instances:
                        for ec2_instance in ec2_instances[driver_name]:
                            if ec2_instance.name == node:
                                ip_regex = re.compile("^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$")

                                for ind in range(0,len(ec2_instance.public_ips)):
                                    node_ip = ec2_instance.public_ips[ind]
                                    if ip_regex.search(node_ip):
                                        break
                    print_log(log_file, "Will deploy to {} ({})".format(node, node_ip))
                    deploy_return = deploy(node_ip, user, node, dryrun)
                    if deploy_return != 0:
                        if get_input("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes?".format(node)) != "Y":
                            sys.exit(1)
                for lb in sorted(stacks_info[stack][lb_instance]["lbs"]):
                    print_log(log_file, "Will re-attach {} to {}".format(lb_instance, lb))

            for weight in sorted(stack_startup_weights, key=int):
                print "\n"
                print_log(log_file, "Will set weight to {} for Stack {}".format(weight, stack))
                print_log(log_file, "Will sleep {} min".format(stack_phase_sleep/60))
                sleep(2)

def parse_options(options):
    parser = argparse.ArgumentParser(description='Deploy Release hub', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--update-versions', action = "store_true", dest = "update_versions", default = False)
    parser.add_argument('--puppet', dest = "puppetdir")
    parser.add_argument('--versions', dest = "verions_file")
    parser.add_argument('--rel', dest = "release_num", help = "Relase number. ie.: 17-07")
    parser.add_argument('-u', dest = "fab_user")
    parser.add_argument('--dryrun', dest = "dryrun")
    parser.add_argument('--env', dest = "environ", help = "stgn / intg / prod-core / prod-services / core-full / proxy")
    parser.add_argument('--nodes', dest = "nodes_TODO_file", help = "Optional: file with the list of nodes to deploy to")
    parser.add_argument('--proxy-plan', dest = "proxy_plan", default = WOOPAS_PLAN, help = "Optional: file with the daily breackdown of proxy regions to deploy. Format: day,provider,region,weight,status")
    parser.add_argument('--branch', dest = "branch", default = "master", help = "Optional: puppet branch to set the changes")

    return parser.parse_args()

def display_options(user, env, nodes_todo, proxy_plan, dryrun, relservices=None, allservices=None):
    clear_screen()
    #if clear != '':
    nodes = []
    if nodes_todo is not None:
        with open(nodes_todo, "r") as nodes_list:
            for line in nodes_list:
                if nodes_todo_regex.match(line):
                    nodes.append(line.rstrip().split(',')[0])
        if len(nodes) == 0:
            sys.exit("ERROR: no nodes TODO found for today ({})".format(DATE))


    print "-------------------------------------------------"
    print "Running deployment with following options:"
    print "\t- user: {}".format(user)
    print "\t- environment: {}".format(env)

    if relservices is not None:
        release_services_list = []
        with open(relservices, "r") as rel_services_file:
            for service in rel_services_file:
                release_services_list.append(service.rstrip())
        print "\t- Relsease services file: {}".format(relservices)
        for serv in release_services_list:
            print "\t\t. {}".format(serv)

    if proxy_plan is not None:
        print "\t- Proxy deployment plan file: {}".format(proxy_plan)

    if allservices is not None:
        print "\t- All services info: {}".format(allservices)

    if len(nodes) > 0:
        print "\t- List of target nodes: {}".format(nodes_todo, nodes)
        for node in nodes:
            print "\t\t. {}".format(node)

    print "\t- dryrun: {}".format(dryrun)

    return get_input()

def print_log(log_file_name, msg, log_level="INFO"):
    now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    with open(log_file_name, "a+") as log_file:
        log_file.write("{} : {}\n".format(now, msg))
    log_level_color = { "WARN" : "yellow" , "ERROR" : "red" }
    if log_level in log_level_color:
        print(colored("{}: {} : {}".format(now,log_level,msg) , log_level_color[log_level]))
    else:
        print "{}: {} : {}".format(now, log_level, msg)

def usage():
    sys.exit("USAGE: \n\t{0} --rel <release_num> --update-versions --env <environment> --puppet <puppetdir>\n\t{0} --rel <release_num> -u <ssh_username> --env <environment> --relservices <release_services_file> --dryrun <True/False> [options]".format(sys.argv[0]) +
            "\n\t* Available environments:" +
            "\n\t- stgn: staging" +
            "\n\t- intg: integration" +
            "\n\t- tools: internal tools (geronimo, pass, launch control)" +
            "\n\t- prod: production (for versions update only)" +
            "\n\t- prod-proxy: production proxies (via woopas). Requires proxy deployment plan file" +
            "\n\t- prod-core: app and portal in prodution" +
            "\n\t- prod-services: non app backend services (siem, connector, delta, filterservice...). Requires release services file" +
            "\n\t- core-full: all production services (app, portal, other services). Requires release services file"
            "\n\t* Options:" +
            "\n\t--nodes: File with the list of nodes to deploy to if not all default nodes required" +
            "\n\t--proxy-plan: File with the daily proxy deployment plan per region. Default: {}".format(WOOPAS_PLAN))

def main(args):
    options = parse_options(args)
    #print "\nArguments: {}\n".format(options)
    if not options.release_num:
        print(colored("\nERROR: Missing release number (--rel <rel_num>)\n" , "red"))
        usage()
    release_num = options.release_num

    if not options.environ or (options.update_versions and options.environ not in update_environments) or (not options.update_versions and options.environ not in environments):
        print(colored("\nERROR: Missing environment (--env <{}>)\n".format(environments) , "red"))
        usage()
    environ = options.environ

    if options.update_versions:
        if not options.puppetdir:
            print(colored("\nERROR: Missing puppet directory\n".format(environments) , "red"))
            usage()

        prepare_version_file.doVersion(options.puppetdir, environment=options.environ, branch=options.branch)
    else:

        if not options.fab_user:
            print(colored("\nERROR: Missing ssh user (-u <username>)\n" , "red"))
            usage()
        fab_user = options.fab_user

        if not options.dryrun:
            print(colored("\nERROR: Missing --dryrun option\n" , "red"))
            usage()
        if options.dryrun.lower() == "true":
            dryrun = True
        elif options.dryrun.lower() == "false":
            dryrun = False
        else:
            print(colored("\nERROR: wrong dryrun value {}".format(options.dryrun) , "red"))
            usage()

        global log_file
        if dryrun in [False,"False","false"]:
            log_file = os.path.join(current_dir, "logs", "RL_{}_{}.log".format(release_num,environ))
        else:
            log_file = os.path.join(current_dir, "logs", "RL_{}_{}_dryrun.log".format(release_num,environ))

        if environ == "proxy" and options.proxy_plan:
            if not os.path.isfile(options.proxy_plan):
                sys.exit("ERROR: Proxy plan file \"{}\" not found".format(options.proxy_plan))

        if display_options(fab_user, environ, options.nodes_TODO_file, options.proxy_plan, dryrun) != "Y":
            sys.exit(1)

        providers, provider_aliases = libcloud_api.get_providers()

        if options.nodes_TODO_file:
            TODOLIST = options.nodes_TODO_file
        else:
            TODOLIST = None

        if environ != "prod-core" and environ != "prod-proxy":
            # Getting the instances
            print_log(log_file, "Getting list of instances")
            all_instances = libcloud_api.get_all_instances()
            #print "[DEBUG] number of instances: {}".format(len(all_instances))

        # Git Pull on puppet

        if dryrun in [ False, "False", "false" ]:
            print_log(log_file, "Running git pull in puppet master ({}:{})".format(puppet_master, puppet_master_dir))
            with settings(host_string = puppet_master, user = fab_user):
                git_pull(puppet_master_dir)
        else:
            print_log(log_file, "Would run git pull in puppet master ({}:{})".format(puppet_master, puppet_master_dir))

        app_done = False

        # read through the file of services to deploy and deploy to required instances
        if environ == "prod-proxy":
            if options.proxy_plan:
                PROXY_PLAN = options.proxy_plan
            else:
                PROXY_PLAN = WOOPAS_PLAN
                if get_input("Proxy default deployment plan: {}. Continue?".format(PROXY_PLAN)) != "Y":
                    PROXY_PLAN = raw_input("Please enter the file with the woopas plan: ")
                while not os.path.isfile(PROXY_PLAN):
                    PROXY_PLAN = raw_input("\"{}\" not found. Please enter a valid file: ".format(PROXY_PLAN))

            woopas_amigo.main(release_num,PROXY_PLAN,dryrun=dryrun)
        elif environ == "prod-core":
            deploy_core(fab_user,dryrun)
            print "\nLOG file: {}\n".format(log_file)
        else:
            if environ == "core-full":
                deploy_core(fab_user,dryrun)

            if environ in env_service_types:
                my_service_types = env_service_types[environ]
            else:
                my_service_types = service_types

            for service_type in my_service_types:
                if ( service_type == "app" or service_type == "web" ) and ( environ == "core-full" or environ == "prod-services" ):
                    print "\n"
                    print_log(log_file, "Skipping deployment of Core \"{}\" services".format(service_type),"WARN")
                    continue

                if service_type in woopas_service_types and environ in live_environments and environ != "proxy":
                    print "\n"
                    print_log(log_file, "Skipping {} deployment. Incompatible type: woopas service not run as --env proxy **".format(service_type),"WARN")
                    print "\n"
                    sleep(3)
                    continue

                print "\nDeploying to {} servers".format(service_type)
                if service_type in server_services_mapping:
                    print "- services running:"
                    for server_service in server_services_mapping[service_type]:
                        pkg_name = server_service.split(':')[0]
                        try:
                            pkg_name += " ({})".format(server_service.split(':')[1])
                        except:
                            pass
                        print "\t. {}".format(pkg_name)
                if get_input("\nWould you like to deploy to {} servers?".format(service_type)) != "Y":
                    continue

                instances_TODO = {}
                if service_type == "bea" and environ == "stgn":
                    print_log(log_file, "Switching server type bea to app in staging", "WARN")
                    service_type = "app"

                if service_type == "app" and app_done:
                    print_log(log_file, "deployment already done on app servers. Skipping")
                    continue

                if (environ in live_environments and service_type in [ "fis", "fid" ]) or (environ == 'tools'):
                    service_regex = re.compile("^{}-[0-9]*-cops".format(service_type), re.VERBOSE)
                elif service_type == "vid":
                    service_regex = re.compile("^{}-[0-9]*-prxy.*wandera\.{}".format(service_type, env_extensions[environ.split('-')[0]]), re.VERBOSE)
                elif environ in live_environments:
                    service_regex = re.compile("^{}-[0-9]*-core".format(service_type), re.VERBOSE)
                elif service_type == "prx" and environ == "stgn":
                    service_regex = re.compile("(^{}-[0-9]*-{}|\.node\..*wandera\.{})".format(service_type, environ, env_extensions['stgn']), re.VERBOSE)
                else:
                    service_regex = re.compile("^{}-[0-9]*-{}".format(service_type, environ), re.VERBOSE)
                if TODOLIST is None:
                    for driver in all_instances:
                        for instance in all_instances[driver]:
                            if service_regex.search(instance.name):
                                name = instance.name
                                instances_TODO[name] = { "instance" : instance , "provider" : driver }
                else:
                    with open(TODOLIST, "r") as todo_nodes:
                        for line in todo_nodes:
                            line = line.rstrip()
                            node = line.rsplit(',')[0]
                            node_regex = re.compile(node)
                            if nodes_todo_regex.search(line) and service_regex.search(node):
                                print_log(log_file, "Looking for {} instance for {}".format(service_type, node))
                                found = False
                                for driver in all_instances:
                                    for instance in all_instances[driver]:
                                        #print_log(log_file, "{}".format(instance.name))
                                        if node_regex.search(instance.name):
                                            instances_TODO[node] = { "instance" : instance , "provider" : driver }
                                            found = True
                                            break
                                    if found:
                                        break
                                if not found:
                                    print_log(log_file, "{} not found".format(node), "ERROR")

                if len(instances_TODO) == 0:
                    print "\n"
                    print_log(log_file, "*** No nodes found for service type \"{}\". Skipping... ***".format(service_type))
                    print "\n"
                    sleep(3)
                else:
                    print "\n"
                    print_log(log_file, "** Will deploy on:")
                    for name in instances_TODO:
                        #print_log(log_file, "\t- {} ({})".format(name, vars(instances_TODO[name])))
                        print_log(log_file, "\t- {} on {} ({})".format(name, instances_TODO[name]["provider"], instances_TODO[name]["instance"].state))

                    #cont = get_input()

                    if get_input() == "Y":
                        for name in instances_TODO:
                            instance = instances_TODO[name]["instance"]
                            state = instance.state
                            ip_regex = re.compile("^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$")

                            for ind in range(0,len(instance.public_ips)):
                                host = instance.public_ips[ind]
                                if ip_regex.search(host):
                                    break

                            print "\n"
                            print_log(log_file, "* Deploying to {} - {} (currently {})".format(name, host, state))
                            print "\n"
                            if dryrun in [ False, "False", "false" ]:
                                if get_input("Proceed with deployment on {} ({})".format(name, host)) != "Y":
                                    continue
                                if state != "running":
                                    option = stopped_instance_options(name)
                                    if option == '3':
                                        print_log(log_file, "Skipping deployment of {}".format(name))
                                        continue
                                    else:
                                        if option == '1':
                                            #driver = instances_TODO[name]["driver"]
                                            print_log(log_file, "Starting {}".format(name))
                                            #sleep(2)
                                            if not dryrun:
                                                libcloud_api.start_instance(name)
                                                print_log(log_file, "Waiting for {} to be fully up".format(name))
                                                sleep(120)
                                        deploy_return = deploy(host,fab_user,name,dryrun)
                                        if deploy_return == 3:
                                            continue
                                        elif deploy_return != 0:
                                            if get_input("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes?".format(name)) == "Y":
                                                continue
                                            else:
                                                sys.exit(1)
                                        elif service_type == "app":
                                                app_done = True
                                else:
                                    if deploy(host,fab_user,name,dryrun) != 0:
                                        if get_input("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes?".format(name)) == "Y":
                                            continue
                                        else:
                                            sys.exit(1)
                                    else:
                                        if service_type == "app":
                                            app_done = True

                                        if TODOLIST is not None:
                                            update_node_status(name, TODOLIST)

                                        if get_input("\nDeployment of {} completed. Continue with rest of nodes?".format(name)) != "Y":
                                            sys.exit("BYE")
            print "\nLOG file: {}\n".format(log_file)

            if environ in health_check_url:
                print(colored("\nVerify end points health check @ {}\n".format(health_check_url[environ]), "magenta"))

            if environ == 'stgn':
                now = datetime.now()
                time_now = now.time()
                if time_now >= datetime.strptime(stgn_deploy_cutoff_time, "%I:%M%p").time():
                    print(colored("\nIt is after {}. Please run a device enrolment test\n".format(stgn_deploy_cutoff_time), "yellow"))

#############  MAIN  #############

if __name__ == "__main__":
    logging.basicConfig(level='INFO', format='%(asctime)s %(levelname)s %(message)s')
    #(options, args) = parse_options(sys.argv[1:])
    main(sys.argv[1:])
