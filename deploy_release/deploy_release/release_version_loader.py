####################################################################################
#### release_version_loader.py - Dedicated release loader system for each IT    ####
####################################################################################

import re
import datetime
import os
import sys
from boto.s3.connection import S3Connection
from natsort import natsorted
import logging
import argparse
import service_config
from ruamel.yaml import YAML

yaml=YAML()
yaml.preserve_quotes = True
yaml.explicit_start = True


# SERVICES_FILE="./release_services/all_services.info"

def GetS3Auth(awsacckey, awsseckey):
    """ Peform AWS auth
        :param: awsacckey string
        :param: awsseckey string
        :returns: conn boto s3 session
    """
    conn = S3Connection(awsacckey, awsseckey)
    return conn


class ReleaseVersion(object):

    """ Release Version Object For Each Release Software
        :param: dirlist s3 bucketlist object
        :returns: major.minor.devstage-hotfixversion string
    """

    def __init__(self, name, s3conn, blist, toolsdir, date=datetime.datetime.now()):

        self.swname = name
        self.date = date
        self.auth = s3conn
        self.toolsdir = toolsdir
        softname = self.swname
        if softname == 'proxy_enterprise':
            softname = 'rabbit-enterprise'
        if softname == 'enterprise_connectorservice':
            softname = 'emm-connect'
        swregex = re.match('([a-z]*)_*([a-z]*)', softname)
        for sm in swregex.groups():
            if sm == 'enterprise' or sm == '':
                continue
            for dir in blist:
                if dir.name == 'releases/' or re.search('openscoring-service-model', dir.name) :
                    continue
                dirregex = re.match('releases\W([a-z]*)', dir.name)
                dirmatch = re.match('{}'.format(dirregex.groups()[0]), sm)
                if dirmatch:
                    self.releasedir = dir.name

    def is_tools_service(self):
        for service in service_config.ALL_SERVICES:
            if service['name'] == self.swname and service.get('tools'):
                return True
        return False

    def get_latest_version(self, buc):
        """ lookup latest version of sofware from AWS s3bucket
            :returns: major.minor.devstage-hotfixversion string
        """

        if self.is_tools_service():
            self.latestversion = "Not available in S3. Check Release Notes"
        else:
            software_v_list = []
            vder = buc.list(self.releasedir)
            for key in vder:
                vregex = re.match(
                    '.*_([0-9]*\.[0-9]*\.[0-9]*\.?[0-9]*)-([0-9]*).*', key.name)
                if vregex is not None:
                    software_v_list.append(key.name)
            # TODO: Find standard lib way of doing this more easily.
            slist = natsorted(software_v_list)
            lversionnum = len(slist) - 1
            lvregex = re.search(
                r"_(?P<version>\w+\.\w+\.\w+(\.\w+){,1}(\-\w+){,2})_",
                slist[lversionnum])
            self.latestversion = lvregex.group('version')
        return self.latestversion

    def set_latest_version(self):
        """ set latest sw version to hieradata cfmgt defined file
            :returns: success bool
        """
        pass

def getServicesVersions(tools_path):
    # The ENV vars for AWS
    AWSKEY = os.environ.get('AWS_ACCESS_KEY_ID')
    AWSSECKEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
    if AWSKEY is None or AWSSECKEY is None:
        sys.exit("ERROR: You need to set AWS_ACCESS_KEY_ID"
                 " AND AWS_SECRET_ACCESS_KEY env var values")
    # S3 bucket const for checking our software versions
    S3_SW_BUCKET = "snappli-software-repo"
    # Run The S3 auth
    s3auth = GetS3Auth(AWSKEY, AWSSECKEY)
    wandera_software_bucket = s3auth.get_bucket(S3_SW_BUCKET)
    wandera_release_dirs = wandera_software_bucket.list("releases/", "/")

    versions = []
    for service in service_config.ALL_SERVICES:
       if not service.get('deprecated'):
            service_name = service['name']
            try:
                sw_service = ReleaseVersion(service_name, s3auth, wandera_release_dirs, tools_path)
                service['name'] = sw_service.swname
                service['version']= sw_service.get_latest_version(wandera_software_bucket)
                versions.append(service)
            except Exception as e:
                logging.warning("Could not get version information for service: %s - Exception: %s", service_name, e)
    return versions

def main():
    versions_infos = getServicesVersions()
    for versions_info in versions_infos:
        versions_info['alias'] = versions_info.get('alias', '')
        print "Software {name} ({alias}) ==> {version}".format(**versions_info)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Prints services version pulled from release S3 bucket')
    parser.add_argument('-l', '--level', help='Log Level',default='ERROR', required=False, choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'])
    # parser.add_argument('-f', '--file', help='Service list file', required=False, default="./release_services/all_services.info", type=str)
    args = parser.parse_args()

    logging.basicConfig(level=args.level, format='%(asctime)s %(levelname)s %(message)s')

    main()
