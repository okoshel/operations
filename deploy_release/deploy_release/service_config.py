server_check_function = {
    'app' : 'check_app',
    'dls' : 'check_delta',
    'fis' : 'check_fis',
    'con' : 'check_connector',
    'sie' : 'check_siem',
    'prx' : 'check_proxy',
    'apache' : 'check_apache',
    'web' : 'check_portal',
    'osc' : 'check_osc',
    'bea' : 'check_beacon',
    'vid' : 'check_video',
    'ras' : 'check_ras',
    'imp' : 'check_tools',
    'ais' : 'check_ais',
    'srs' : 'check_srs',
    'drs' : 'check_drs',
    'mms' : 'check_mms'
}

env_service_types = {
    'tools' : ['imp'],
    'opscenter' : ['doc']
}
env_extensions = {
    'stgn' : 'biz',
    'prod' : 'com',
    'tools' : 'net'
}

endpoint_mapping = {
    "ai.wandera.com" : { "service_name" : "app-inventory-service-enterprise" , "puppet_tag" : "appinventoryservice" },
    "ap.wandera.com" : { "service_name" : "appinfoservice-enterprise" , "puppet_tag" : "appinfoservice" },
    "as.wandera.com" : { "service_name" : "accounting-service" , "puppet_tag" : "accountingservice" },
    "ad.wandera.com" : { "service_name" : "adminservice-enterprise" , "puppet_tag" : "adminservice" },
    "ea.wandera.com" : { "service_name" : "apnservice-enterprise" , "puppet_tag" : "apnservice" },
    "eb.wandera.com" : { "service_name" : "beaconservice-enterprise" },
    "ec.wandera.com" : { "service_name" : "n/a" , "puppet_tag" : "n/a" },
    "ed.wandera.com" : { "service_name" : "dataplanservice-enterprise" , "puppet_tag" : "dataplanservice" },
    "em.wandera.com" : { "service_name" : "proxyservice-enterprise" , "puppet_tag" : "proxyservice" },
    "en.wandera.com" : { "service_name" : "notificationservice-enterprise" , "puppet_tag" : "notificationservice" },
    "ms.wandera.com" : { "service_name" : "modelsmonitoringservice-enterprise" , "puppet_tag" : "modelsmonitoringservice" },
    "ep.wandera.com" : { "service_name" : "profileservice2-enterprise" , "puppet_tag" : "profileservice" },
    "er.wandera.com" : { "service_name" : "reportservice3-enterprise" , "puppet_tag" : "reportservice" },
    "es.wandera.com" : { "service_name" : "stateanalyserservice-enterprise" , "puppet_tag" : "stateanalyserservice" },
    "et.wandera.com" : { "service_name" : "ticketservice-enterprise" , "puppet_tag" : "ticketservice" },
    "eu.wandera.com" : { "service_name" : "userservice2-enterprise" , "puppet_tag" : "userservice" },
    "ew.wandera.com" : { "service_name" : "n/a" , "puppet_tag" : "n/a" },
    "pu.wandera.com" : { "service_name" : "publisherservice-enterprise" , "puppet_tag" : "publisherservice" },
    "ws.wandera.com" : { "service_name" : "wakeupservice-enterprise" , "puppet_tag" : "wakeupservice" },
    "os.wandera.com" : { "service_name" : "openscoring-service-enterprise" , "puppet_tag" : "openscoring" },
    "sr.wandera.com" : { "service_name" : "streaming-report-service" , "puppet_tag" : "streamingreportservice" },
    "form.wandera.com" : { "service_name" : "ssl-strip-form" , "puppet_tag" : "n/a" },
    "fjgy37d81dnhtdie4.emc.eu-west-1b.ie.wandera.com" : { "service_name" : "emm-connect" , "puppet_tag" : "connectorservice" },
    "gufast6chaspachu.emc.eu-west-1b.ie.wandera.com" : { "service_name" : "emm-connect" , "puppet_tag" : "connectorservice" },
    "radar.wandera.com" : { "service_name" : "portal" , "puppet_tag" : "apache2" },
    "telekom-radar.wandera.com" : { "service_name" : "portal" , "puppet_tag" : "apache2" },
    "redirect.wandera.com" : { "service_name" : "redirectionservice-enterprise" , "puppet_tag" : "redirectionservice" }
}

puppet_masters = { 'prod' : "puppet.snappli.net",
                'stgn' : "puppet-stgn.wandera.net" }
puppet_master_dir = "/opt/puppet"
tools_node = { "name" : "imp-001-cops.eu-west-1b.ie.wandera.net" , "ip" : "52.51.187.241"}
environments = ['stgn','dev','intg','tools','prod-proxy','prod-core','core','radar','prod-services','core-full']
update_environments = ['stgn','staging','prod','production']
live_environments = ['prod','core','radar','cops','prod-proxy','prod-core','prod-services','core-full','prod-endpoint']
woopas_services = ['proxy-enterprise','rabbit-enterprise','proxy_enterprise','proxy']
woopas_service_types = ['prx']
health_check_url = { 'stgn' : 'http://ci.snappli.net/job/~stgn-health-check/lastBuild/sitemonitor/#',
                    'core' : 'http://ci.snappli.net/view/Healthchecks/job/~prod-health-check/build?delay=0sec',
                    'prod-services' : 'http://ci.snappli.net/view/Healthchecks/job/~prod-health-check/build?delay=0sec',
                    'radar' : 'http://ci.snappli.net/view/Healthchecks/job/~prod-health-check/build?delay=0sec' }
healthchecks_urls = {
    'stgn' : ['http://54.246.241.216:9998/healthcheck?UserService_app-101-stgn','http://54.246.241.216:2498/healthcheck?NotificationService_app-101-stgn','http://54.246.241.216:2998/healthcheck?TicketService_app-101-stgn','http://54.246.241.216:1898/healthcheck?DataPlanService_app-101-stgn','http://54.246.241.216:6898/healthcheck?ReportService_app-101-stgn','http://54.246.241.216:2298/healthcheck?StateAnalyserService_app-101-stgn','http://54.246.241.216:7998/healthcheck?ProfileService_app-101-stgn','http://54.246.241.216:1798/healthcheck?PublisherService_app-101-stgn','http://54.246.241.216:11998/healthcheck?CertificateService_app-101-stgn','http://54.246.241.216:8998/healthcheck?ApnService_app-101-stgn','http://54.246.241.216:1698/healthcheck?BeaconService_app-101-stgn','http://54.246.241.216:2698/healthcheck?AppInfoService_app-101-stgn','http://54.246.241.216:1398/healthcheck?WakeUpService_app-101-stgn','http://54.246.241.216:1998/healthcheck?ProxyService_app-101-stgn','http://52.212.113.90:2398/healthcheck?DeltaloadService','http://52.30.99.50:1358/healthcheck?SiemService','http://52.30.86.119:1598/healthcheck?FilterService-AWS','http://162.13.179.57:1598/healthcheck?FilterService-Rackspace','https://radar.wandera.biz/healthcheck?','http://52.213.100.49:2798/healthcheck?OpenscoringService_osc-101-stgn','http://34.240.136.221:2898/healthcheck?AppInventoryService_ais-101-stgn','http://54.246.241.216:14998/healthcheck?AccountingService_app-101-stgn','http://54.194.47.191:2599/v2/heartbeat?ConnectorService_con-101-stgn','http://34.242.252.31:15998/healthcheck?DeviceRelayService_drs-101-stgn','http://52.30.86.119:2798/healthcheck?OpenscoringService_fis-101-stgn-AWS','http://162.13.179.57:2798/healthcheck?OpenscoringService_fis-101-stgn-Rackspace']
}
app_healthchecks_urls = {
    'production' : [':9998/healthcheck?UserService',':2498/healthcheck?NotificationService',':2998/healthcheck?TicketService',':1898/healthcheck?DataPlanService',':6898/healthcheck?ReportService',':2298/healthcheck?StateAnalyserService',':7998/healthcheck?ProfileService',':1798/healthcheck?PublisherService',':11998/healthcheck?CertificateService',':8998/healthcheck?ApnService',':2698/healthcheck?AppInfoService',':1398/healthcheck?WakeUpService',':1998/healthcheck?ProxyService',':14998/healthcheck?AccountingService'],
    'default' : [':9998/healthcheck?UserService',':2498/healthcheck?NotificationService',':2998/healthcheck?TicketService',':1898/healthcheck?DataPlanService',':6898/healthcheck?ReportService',':2298/healthcheck?StateAnalyserService',':7998/healthcheck?ProfileService',':1798/healthcheck?PublisherService',':11998/healthcheck?CertificateService',':8998/healthcheck?ApnService',':1698/healthcheck?BeaconService',':2698/healthcheck?AppInfoService',':1398/healthcheck?WakeUpService',':1998/healthcheck?ProxyService',':14998/healthcheck?AccountingService']
}
acceptance_tests = {
    'stgn' : {
        'url' : 'http://ci.snappli.net',
        'job' : 'acceptance-tests-stgn-radar'
    }
}
stgn_deploy_cutoff_time = '3:00PM'
stack_phase_sleep = 120
slack_url = 'https://hooks.slack.com/services/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'
endpoints_url = "https://snappli.atlassian.net/wiki/spaces/OPS/pages/17530904/Snappli+Service+Port+Allocation+and+prefix+List"
containers_repo = "571706765506.dkr.ecr.eu-west-1.amazonaws.com"
CANARY_PROXY = "f69de1f33ef87cc9.node.eu-west-2b.gb.wandera.com"
CANARY_EMMC = "con-201-core.eu-west-1c.ie.wandera.com"
canary_servers = [CANARY_EMMC]

stacks_info = {
    "A" : {
        "CORE" : {
            "web-101-core.eu-west-1a.ie.wandera.com" : {
                "lbs" : [ "elb-002-core", "eu-a-core" ],
                "instances" : { "app-101-core.eu-west-1a.ie.wandera.com" : "APP" , "web-101-core.eu-west-1a.ie.wandera.com" : "APACHE" }
            },
            "web-201-core.eu-west-1b.ie.wandera.com" : {
                "lbs" : [ "elb-002-core", "eu-a-core" ],
                "instances" : { "app-201-core.eu-west-1b.ie.wandera.com" : "APP" , "web-201-core.eu-west-1b.ie.wandera.com" : "APACHE" }
            }
        },
        "RADAR" : {
            "web-103-core.eu-west-1a.ie.wandera.com" : {
                "lbs" : [ "elb-003-core" ],
                "instances" : { "web-103-core.eu-west-1a.ie.wandera.com" : "PORTAL" }
            },
            "web-203-core.eu-west-1b.ie.wandera.com" : {
                "lbs" : [ "elb-003-core" ],
                "instances" : { "web-203-core.eu-west-1b.ie.wandera.com" : "PORTAL" }
            }
        }
    },
    "B" : {
        "CORE" : {
            "web-501-core.eu-west-1b.ie.wandera.com" : {
                "lbs" : [ "elb-202-core", "eu-b-core" ],
                "instances" : { "app-501-core.eu-west-1b.ie.wandera.com" : "APP" , "web-501-core.eu-west-1b.ie.wandera.com" : "APACHE" }
            },
            "web-601-core.eu-west-1c.ie.wandera.com" : {
                "lbs" : [ "elb-202-core", "eu-b-core" ],
                "instances" : { "app-601-core.eu-west-1c.ie.wandera.com" : "APP" , "web-601-core.eu-west-1c.ie.wandera.com" : "APACHE" }
            }
        },
        "RADAR" : {
            "web-503-core.eu-west-1b.ie.wandera.com" : {
                "lbs" : [ "elb-203-core" ],
                "instances" : { "web-503-core.eu-west-1b.ie.wandera.com" : "PORTAL" }
            },
            "web-603-core.eu-west-1c.ie.wandera.com" : {
                "lbs" : [ "elb-203-core" ],
                "instances" : { "web-603-core.eu-west-1c.ie.wandera.com" : "PORTAL" }
            }
        }
    },
    "C" : {
        "CORE" : {
            "web-104-core.eu-west-1a.ie.wandera.com" : {
                "lbs" : [ "elb-101-core", "eu-c-core" ],
                "instances" : { "app-104-core.eu-west-1a.ie.wandera.com" : "APP" , "web-104-core.eu-west-1a.ie.wandera.com" : "APACHE" }
            },
            "web-202-core.eu-west-1b.ie.wandera.com" : {
                "lbs" : [ "elb-101-core", "eu-c-core" ],
                "instances" : { "app-202-core.eu-west-1b.ie.wandera.com" : "APP" , "web-202-core.eu-west-1b.ie.wandera.com" : "APACHE" }
            }
        }
    },
    "D" : {
        "CORE" : {
            "web-502-core.eu-west-1b.ie.wandera.com" : {
                "lbs" : [ "elb-201-core", "eu-d-core" ],
                "instances" : { "app-502-core.eu-west-1b.ie.wandera.com" : "APP" , "web-502-core.eu-west-1b.ie.wandera.com" : "APACHE" }
            },
            "web-602-core.eu-west-1c.ie.wandera.com" : {
                "lbs" : [ "elb-201-core", "eu-d-core" ],
                "instances" : { "app-602-core.eu-west-1c.ie.wandera.com" : "APP" , "web-602-core.eu-west-1c.ie.wandera.com" : "APACHE" }
            }
        }
    }
}

''' Format of service entry in ALL_SERVICES
{
    'name' : '<package name in S3 repo>',,
    'node' : 'type of server on which it runs',
    'alias' : '<service name in puppet>', ==> if different from 'name'
    'deprecated': True ==> only required if true
},'''
ALL_SERVICES=[
    {
        'name' : 'aggregationservice-enterprise',
        'node' : 'app',
        'deprecated': True
    },
    {
        'name': 'apnservice-enterprise',
        'alias': 'apn',
        'node' : 'app',
        'port' : '8998'
    },
    {
        'name': 'app-db-manager-service',
        'node': 'app',
        'deprecated': True
    },
    {
        'name': 'appdbmanagerservice-enterprise',
        'alias': 'appdb',
        'node': 'app'
    },
    {
        'name': 'appinfoservice-enterprise',
        'alias': 'appinfo',
        'node': 'app',
        'port': '2698'
    },
    {
        'name': 'beaconservice-enterprise',
        'alias': 'beacon',
        'node': 'bea',
        'port': '1698'
    },
    {
        'name': 'certificateservice',
        'alias': 'certificate',
        'node': 'app'
    },
    {
        'name': 'dataplanservice-enterprise',
        'alias': 'dataplanservice',
        'node': 'app',
        'port': '1898'
    },
    {
        'name': 'deltaloadservice-enterprise',
        'alias': 'deltaload',
        'node': 'dls',
        'port': '2398'
    },
    {
        'name': 'device-relay-service',
        'alias': 'devicerelayservice',
        'node': 'drs',
        'port': '15998'
    },
    {
        'name': 'emm-connect',
        'alias': 'enterprise_connectorservice',
        'node': 'con',
        'port': '2599',
        'healthcheck': 'v2/heartbeat'
    },
    {
        'name': 'filterservice-enterprise',
        'alias': 'filterservice',
        'node': 'fis',
        'port': '1598'
    },
    {
        'name': 'link-replacement-service-enterprise',
        'alias': 'linkreplacementservice',
        'node': 'prx'
    },
    {
        'name': 'logservice-enterprise',
        'node': 'app',
        'deprecated': True
    },
    {
        'name': 'modelsmonitoringservice-enterprise',
        'alias': 'modelsmonitoringservice',
        'node': 'mms',
        'port': '3098'
    },
    {
        'name': 'notificationservice-enterprise',
        'alias': 'enterprise_notificationservice',
        'node': 'app',
        'port': '2498'
    },
    {
        'name': 'portal-enterprise',
        'alias': 'portal',
        'node': 'web',
        'port': '443'
    },
    {
        'name': 'profileservice2-enterprise',
        'alias': 'profileservice2_enterprise',
        'node': 'app',
        'port': '7998'
    },
    {
        'name': 'proxyservice-enterprise',
        'alias': 'proxyservice_enterprise',
        'node': 'app',
        'port': '1998'
    },
    {
        'name': 'publisherservice-enterprise',
        'alias': 'publisher',
        'node': 'app',
        'port': '1798'
    },
    {
        # FOR NON PRODUCTION
        'name': 'rabbit-enterprise',
        'alias': 'proxy',
        'node': 'prx',
        'env': ['staging','stgn']
    },
    {
        # FOR PRODUCTION
        'name': 'rabbit-enterprise',
        'alias': 'proxy_enterprise',
        'node': 'prx',
        'env': ['production','prod']
    },
    {
        'name': 'redirectionservice-enterprise',
        'alias': 'enterprise_redirectionservice',
        'node': 'ras',
        'deprecated' : True
    },
    {
        'name': 'reportservice3-enterprise',
        'alias': 'reportservice3_enterprise',
        'node': 'app',
        'port': '6898'
    },
    {
        'name': 'siemservice-enterprise',
        'alias': 'siem',
        'node': 'sie',
        'port': '1358'
    },
    {
        'name': 'stateanalyserservice-enterprise',
        'alias': 'sae',
        'node': 'app',
        'port': '2298'
    },
    {
        'name': 'threatcms-enterprise',
        'alias': 'enterprise_portal_cms',
        'node': 'app'
    },
    {
        'name': 'ticketservice-enterprise',
        'alias': 'enterprise_ticketservice',
        'node': 'app',
        'port': '2998'
    },
    {
        'name': 'trafficanalysissystem-enterprise',
        'node': 'app'
    },
    {
        'name': 'userservice2-enterprise',
        'alias': 'userservice2_enterprise',
        'node': 'app',
        'port': '9998'
    },
    {
        'name': 'videoservice',
        'node': 'vid',
        'deprecated': True
    },
    {
        'name': 'wakeupservice-enterprise',
        'alias': 'wakeup',
        'node': 'app',
        'port': '1398'
    },
    {
        'name': 'webhookservice',
        'alias': 'webhook',
        'node': 'app',
        'deprecated': True
    },
    {
        'name': 'openscoring-service-enterprise',
        'alias': 'openscoring',
        'node': 'osc',
        'port': '2798'
    },
    {
        'name': 'accounting-service',
        'alias': 'accountingservice',
        'node': 'app',
        'port': '14998'
    },
    {
        'name': 'adminservice-enterprise',
        'alias': 'adminservice',
        'node': 'app'
    },
    {
        'name': 'app-inventory-service-enterprise',
        'alias': 'app_inv',
        'node': 'ais',
        'port': '2898'
    },
    {
        'name': 'streaming-report-service',
        'alias': 'streamingreportservice',
        'node': 'srs',
        'deprecated': True
    },
    {
        'name': 'launch_control',
        'alias': 'launch_control',
        'node': 'imp',
        'tools': True,
        'container' : 'launch-control',
        'image' : 'launch-control-console',
        'env_file' : '/opt/launch_control/.env',
        'from_port' : 5001,
        'to_port' : 8000
    },
    {
        'name': 'geronimo',
        'alias': 'geronimo',
        'node': 'imp',
        'tools': True,
        'container' : 'geronimo-Production',
        'image' : 'geronimo',
        'from_port' : 8081,
        'to_port' : 80,
        'volume' : '/opt/geronimo/config-Production.js:/var/www/config.js'
    },
    {
        'name': 'pass',
        'alias': 'pass',
        'node': 'imp',
        'tools': True,
        'container' : 'pass-Operations',
        'image' : 'pass',
        'env_file' : '/opt/pass/.env',
        'from_port' : 5002,
        'to_port' : 8000,
        'volume' : '/opt/pass/client_secrets.json:/usr/src/app/client_secrets.json'
    },
    {
        'name': 'feature_instances_manager',
        'alias': 'feature_instances_manager',
        'node': 'imp',
        'tools': True,
        'container' : 'feature-instances-manager-Ops',
        'image' : 'feature-instances-manager',
        'env_file' : '/opt/feature-instances-manager/.env',
        'from_port' : 7007,
        'to_port' : 8000
    },
    {
        'name': 'opscenter',
        'alias': 'opscenter',
        'node': 'doc',
        'opscenter': True
    }
]

SG_mapping = {
    "proxy" : {
        "emea" : [{ 'id' : 'sg-3d0e575b' , 'name' : 'prod-tcp-all-proxies-emea-2' }],
        "apac" : [{ 'id' : 'sg-f9490a9c' , 'name' : 'prod-tcp-all-proxies-apac' }],
        "amer" : [{ 'id' : 'sg-a6490ac3' , 'name' : 'prod-tcp-all-proxies-amer' }],
        "stgn" : [{ 'id' : 'sg-a05c57c2' , 'name' : 'Stgn-ProxyServiceSG' }]
    },
    "ops-mongo-cluster" : {
        "emea" : [{ 'id' : 'sg-9ef542fa' , 'name' : 'ops-mongo-cluster' }],
        "apac" : [{ 'id' : 'sg-9ef542fa' , 'name' : 'ops-mongo-cluster' }],
        "amer" : [{ 'id' : 'sg-9ef542fa' , 'name' : 'ops-mongo-cluster' }],
        "stgn" : [{ 'id' : 'sg-9ef542fa' , 'name' : 'ops-mongo-cluster' }]
    },
    "demo" : {
        "emea" : [{ 'id' : 'sg-7d4f8906' , 'name' : 'testauto' }]
    }
}
