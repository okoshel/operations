#!/usr/local/bin/python

import os
import sys

from pprint import pprint
import argparse
import re
import subprocess
from subprocess import Popen, PIPE

import fabric
from fabric.context_managers import settings
from fabric.api import env
import imp
from datetime import date, datetime
from time import sleep
import platform
from termcolor import colored
import logging

#import deploy_release
#from deploy_release import *
import service_config
import libcloud_api
import woopas_amigo
'''from deploy_release import service_config
from deploy_release import libcloud_api
from deploy_release import woopas_amigo'''
from service_config import *
from fabfile import *

#woopas = imp.load_source("woopas_amigo", "/Users/arturonoha/Documents/bitbucket/wandera_woopas/Woopas_PY/woopas_amigo.py")
env.use_ssh_config = True

'''today = date.today()
DATE = today.isoformat()
today_regex = re.compile("{},(.*),TODO".format(DATE))
nodes_todo_regex = re.compile("[^,]*,TODO")

current_dir = os.getcwd()
log_file = os.path.join(current_dir,"logs")
home = os.path.expanduser('~')
CONFIG_FILE = os.path.join(home, ".wandera/woopas.json")

sg_ref_file = "/Users/arturonoha/Documents/Projects/OPS/OPS-3538_Proxies_access_Mongo/sg_reference.csv"'''

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', dest = "filename")
    parser.add_argument('--dryrun', action = "store_true", dest = "dryrun", default = False)

    return parser.parse_args()

def main(args):
    options = parse_options(args)

    dryrun = options.dryrun

    if os.path.isfile(options.filename) is None:
        sys.exit(colored("\nMissing input file\n", "red"))
    if not os.path.isfile(options.filename):
        sys.exit(colored("\nFILE {} not found\n".format(options.filename), "red"))
    input_filename = options.filename

    print "Getting all drivers"
    all_drivers = libcloud_api.init_all_drivers()
    aws_drivers = {}
    for driver_name in all_drivers:
        if re.match("EC2_.*",driver_name):
            aws_drivers[driver_name] = all_drivers[driver_name]

    print "Getting all instances"
    all_instances = libcloud_api.get_all_instances(all_drivers)

    print "Getting all SG"
    all_sg = libcloud_api.get_security_groups()

    with open(input_filename, "r") as input_file:
        for line in input_file:
            line = line.rstrip()
            instance_name = line.split(',')[0]
            instance_type = line.split(',')[1]
            instance_region = line.split(',')[2]

            params = {}
            params['action'] = 'add'
            params['instance_id'] = instance_name
            params['drivers'] = aws_drivers
            params['all_instances'] = all_instances
            params['description'] = instance_name

            for group in SG_mapping[instance_type][instance_region]:
                params['group_id'] = group['id']
                params['group_name'] = group['name']

                if dryrun in [False, "False", "false"]:
                    print(colored("\nInserting {} (type: {} / region: {} / sg: {})".format(instance_name,instance_type,instance_region,params['group_id']),"magenta"))
                    try:
                        libcloud_api.update_security_group_ip(**params)
                    except Exception as e:
                        logging.error(colored(e,"red"))
                else:
                    print "[DRYRUN] Would try to insert {} (type: {} / region: {} / sg: {})".format(instance_name,instance_type,instance_region,params['group_id'])

if __name__ == "__main__":
    main(sys.argv[1:])
