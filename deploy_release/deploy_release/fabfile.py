#####################################################################
# fabfile.py
# Author: Arturo Noha
# Version: 1.0
#-----------------------------------------------------------------
# Available tasks
# * check_app: check the status of the services running in app servers
# * check_connector: check connector service
# * check_delta: check delta service
# * check_fis: check filter service
# * check_portal: check radar
# * check_proxy: check proxy service
# * check_siem: check siem service
#   - check_<service> will
#       - check the package version for the relevant services running on that server
#       - check the relevant processes running and verify if they are running on the installed package version
#       - tail the log for the relevant services
# * check_process: check the package for a given service and verify the status of the service's process
#   - Argument: string to grep when running dpkg in the host
# * service_start:
# * service_stop:
# * service_restart:
# * service_status:
#   - the 4 service management tasks above take 1 argument: the service name, as recognised by the "service" command
# * create_ssh_access: run mco command on puppet master for given host to grant access to "staff"
# * git_pull: run git pull on the given host
# * puppet_agent: run puppet agent on the host.
#   - syntax: puppet_agent(noop=<True/False>, tags=<tags>, force=<True/False>)
#       - noop: True: run puppet with --noop / False: run puppet without --noop. It will ask for confirmation by default
#       - tags: required tags for the puppet run
#       - force: only applicable for noop=False. If set to "force" will run puppet without asking for confirmation
# * deploy_server: delpoy new release on given host
#       - runs puppet noop
#       - runs puppet without noop if noop succeeds
#       - run the required check_<service> function based on the server type
# * tail_rabbit_log: tail rabbit.log on given proxy node
#----------------------------------------------------------------
# PRIVATE FUNCTIONS
# * _get_option: print a given message and question and parse input from user.
#   - valid user options: Y / n (case sensitive)
#   - question must be a yes or no question
# * _run_extra_commands: run a series of commands on the host.
#   - used to run server specific commands on the check_<service> tasks
#   - takes an array of strings (each string = 1 command to run)
#####################################################################

import os
import sys
from sys import stdin
import subprocess
from subprocess import PIPE, Popen
import time
from fabric.api import env, run, cd
from fabric.operations import sudo, local
from fabric.context_managers import hide, show, settings
import re
import json
from termcolor import colored
import interaction
import logging

from wandera_client.client.service.proxy_manager import ProxyManagerClient
from wandera_client.resource import Server
from wandera_client.client.auth import ServiceAuthentication
import libcloud_api
import util

from service_config import *

env.warn_only = True # Allow mangement of errors and exceptions without stopping execution by default
env['sudo_prefix'] += '-H '
valid_options = [ 'y' , 'n' ]

def health_check(port, endpoint='healthcheck',user=None,ssh=False, status_only=False):
    if ssh:
        head = "https"
    else:
        head = "http"
    url = "{}://{}:{}/{}".format(head,env.host_string,port,endpoint)
    print(colored("\n** Running Health Check ({})\n".format(url) , "magenta"))
    if user is not None:
        curl_cmd = "curl --user {}".format(user)
    else:
        curl_cmd = "curl".format(url)
    healthcheck_cmd = "{} {}".format(curl_cmd, url)
    status_open = Popen(healthcheck_cmd, stdout = PIPE, stderr = PIPE, shell=True)
    out, err = status_open.communicate()

    if err:
        url = "{}://localhost:{}/{}".format(head,port,endpoint)
        healthcheck_cmd = "{} {}".format(curl_cmd, url)
        logging.error("Healthcheck from remote failed. Trying {} locally".format(healthcheck_cmd))
        with hide('running', 'stderr', 'stdout'):
            out = sudo(healthcheck_cmd)
    try:
        status_json = json.loads(out)

        if 'checks' in status_json:
            for check in sorted(status_json['checks'].keys()):
                try:
                    if not status_json['checks'][check]['healthy']:
                        print(colored("{} : {}".format(check,json.dumps(status_json['checks'][check],indent=4)),"red"))
                    else:
                        print(colored("{} : {}".format(check,json.dumps(status_json['checks'][check],indent=4)),"green"))
                        #print "{} : {}".format(check,json.dumps(status_json['checks'][check],indent=4))
                except:
                    print "{} : {}".format(check,json.dumps(status_json['checks'][check],indent=4))
        else:
            for check in sorted(status_json.keys()):
                try:
                    if not status_json[check]['healthy']:
                        print(colored("{} : {}".format(check,json.dumps(status_json[check],indent=4)),"red"))
                    else:
                        print "{} : {}".format(check,json.dumps(status_json[check],indent=4))
                except:
                    print "{} : {}".format(check,json.dumps(status_json[check],indent=4))

        #print json.dumps(status_json, indent=4)
        print
        print "\n----------------------------------------\n"
    except:
        print "----\n{}\n----\n* Response is not json ({})".format(out,type(out))

def check_process(process, check_version=None, silent=False):
    return_dict = {'dpkg_version_match' : True}
    dpkg_cmd = "dpkg -l | grep {}".format(process)

    with hide('running', 'stderr', 'stdout'):
        dpkg_response = sudo(dpkg_cmd)

    format_dpkg_response = re.sub(r'[ \t]+', ':', dpkg_response)

    for service_line in format_dpkg_response.splitlines():
        service = service_line.split(':')[1]
        version = service_line.split(':')[2].split('-')[0]

        if check_version is not None:
            version_regex = re.compile(version)
            if not version_regex.match(check_version):
                print(colored("\nERROR!!! {} dpkg version {} and expected version {} DO NOT MATCH\n".format(process,version,check_version),"red"))
                return_dict.update({'dpkg_version_match' : False})
                return return_dict
            else:
                if not silent:
                    print(colored("{} dpkg version {} matches expected {}".format(process, version, check_version), "green"))

        grep_service_cmd = "ps -ef | grep {} | egrep -v \"grep|tail -f\"".format(service)
        if not silent:
            print(colored("\n* Checking Service: {}. Dpkg Version: {}\n".format(service, version),"blue"))
        try:
            with hide('running', 'stderr', 'stdout'):
                grep_service_response = sudo(grep_service_cmd)
        except:
            print(colored("Failed to run the {}.".format(grep_service_cmd),"red"))
            print "Return Code: {}, Stderr: {}".format(grep_service_response.return_code, grep_service_response.stderr)

        if grep_service_response.failed:
            print(colored("\"{}\" Failed. -- Return Code: {}, Stderr: {}".format(grep_service_cmd, grep_service_response.return_code, grep_service_response.stderr),"red"))
            return_dict.update({'running_version' : 'failed'})
            return_dict.update({'dpkg_version' : version})
            return_dict.update({'process_version' : 'N/A'})
        else:
            if not silent:
                print "\n{}\n".format(grep_service_response)
            PID = re.sub(r'[ \t]+',':',grep_service_response).split(':')[1]
            JAR = re.sub(r'.* -jar ','',grep_service_response).split(' ')[0]
            serv_vers_pattern = re.compile("{}.*{}".format(service, version))
            return_dict.update({'dpkg_version' : version})
            return_dict.update({'process_version' : JAR})

            if not serv_vers_pattern.search(grep_service_response):
                if not silent:
                    print(colored("!!! ERROR: {} is not running in the dpkg version.".format(service),"red"))
                    print "PID: {} -- JAR: {} -- DPKG: {}".format(PID, JAR, version)
                return_dict.update({'running_version' : False})
            else:
                if not silent:
                    print(colored("Process running correct version:\n\t- PID: {}\n\t- JAR: {}\n".format(PID, JAR),"green"))
                return_dict.update({'running_version' : True})

        if not silent:
            print "------------------------------------"

    return return_dict

def _run_extra_commands(commands):
    for command in commands:
        with hide('stdout'):
            cmd_response = sudo(command)
            if cmd_response.failed:
                print "\nERROR: {} failed. Status code: {} ({})".format(command, cmd_response.return_code, cmd_response.stderr)
            else:
                print "{}".format(cmd_response)
            print "------------------------------------"

def check_idles_user_config(CONFIG_FILE):
    pmc = init_proxy_manager(CONFIG_FILE)

    proxy_regex = re.compile(".*\.node\..*\.wandera\.com")
    print "Getting all proxies"
    all_instances = libcloud_api.get_all_instances()
    proxies = []
    for driver in all_instances:
        for instance in all_instances[driver]:
            if proxy_regex.search(instance.name):
                proxies.append(instance)

    print "Starting check of /opt/user-conf in idle running proxies"

    for proxy in proxies:
        try:
            pm_proxy = ProxyManagerClient.node_by_name(pmc, proxy.name)
            proxy_pm_state = pm_proxy.state
        except:
            print "Failed to get Proxy Manager Status for {}".format(proxy.name)
            continue

        if proxy_pm_state == 'idle' and proxy.state.lower() == 'running':
            with settings(host_string=proxy.name):
                with hide('running', 'stderr', 'stdout'):
                    if sudo("ls /opt/user-config | grep -v proxyIdentity.json | wc -l") != "0" :
                        if sudo("ls /opt/user-config/customers | wc -l") == "0":
                            print(colored("{}".format(proxy.name),"yellow") + " - /opt/user-config/customers exists but empty")
                        else:
                            conf_count = sudo("find /opt/user-config/*/*/devices/|grep '\.conf'| wc -l")
                            print(colored(" {} has {} devices in /opt/user-config".format(proxy.name, conf_count),"red"))
                            devices = sudo("find /opt/user-config/*/*/devices/|grep '\.conf'")
                            print devices
                    else:
                        print(colored("{}".format(proxy.name),"green") + " has EMPTY config")

def init_proxy_manager(CONFIG_FILE):
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        pm_url = config_json["woopas"]["proxy_manager_url"]
        pm_api_key = config_json["woopas"]["service_api_key"]
        pm_api_secret = config_json["woopas"]["service_api_secret"]
        pm_auth = ServiceAuthentication(pm_api_key, pm_api_secret)
        pmc = ProxyManagerClient(pm_url,auth=pm_auth)
    except Exception as e:
        print "Failed to load ProxyManagerClient\n({})".format(e.message)
        sys.exit(1)

    return pmc

def assumeid(proxyName=None):
    with hide('running', 'stderr', 'stdout'):
        hostname = sudo('hostname')

    assumeid_url = "http://{}:1666/AssumeIdentity".format(hostname)
    if proxyName is not None:
        assumeid_url += "?name={}".format(proxyName)
    else:
        assumeid_url += "?name=cleancache"

    return sudo("curl {}".format(assumeid_url))

def check_proxy(proxy_version=None,linkreplacement_version=None):
    proxy_logs="/opt/proxy/logs/rabbit.log /opt/proxy/logs/access.log"
    tail_lines = 25
    tail_log_cmd = "tail -n {} {} | cut -c 1-255".format(tail_lines, proxy_logs)

    #check_all_status_cmd = "service --status-all | grep proxy"
    check_all_status_cmd = "service proxy status"
    proxy_extra_commnads = [ tail_log_cmd , check_all_status_cmd ]

    check_response = check_process("rabbit | grep enter", check_version=proxy_version, silent=True)
    if not check_response['dpkg_version_match']:
        return { 'version_match' : check_response['dpkg_version_match'] }
    #if not check_response['running_version']

    #proxy_Status_cmd = "curl --user RabbIT:RabbIT5napProxy http://{}:1666/Status | sed -e 's/:\([^\"}}]*\)\([,}}]\)/:\"\\1\"\\2/g'".format(env.host_string)
    print(colored("\n** Running Health Check (http://{}:1666/Status)\n".format(env.host_string) , "magenta"))
    with hide('running', 'stderr', 'stdout'):
        hostname = sudo("hostname")
    proxy_user, proxy_pwd = util.get_proxy_credentails()
    proxy_Status_cmd = "curl --user {}:{} http://{}:1666/Status".format(proxy_user, proxy_pwd, hostname)
    '''status_open = Popen(proxy_Status_cmd, stdout = PIPE, stderr = PIPE, shell=True)
    out, err = status_open.communicate()'''

    with hide('running', 'stderr', 'stdout'):
        out = sudo(proxy_Status_cmd)
    #status_rep = re.sub(r':([^"]*)(,})' , ':\"\g<1>\"\g<2>' , status)
    #status_rep = re.sub(':' , ':"' , status)
    try:
        status_json = json.loads(out)

        print json.dumps(status_json, indent=4)
        print
        print "\n----------------------------------------\n"
    except:
        print "----\n{}\n----\n* Response is not json ({})".format(out,type(out))

    _run_extra_commands(proxy_extra_commnads)

    check_response = check_process("link-replacement",check_version=linkreplacement_version)

    check_response = check_process("rabbit | grep enter", check_version=proxy_version)
    proxy_status = {
        'version_match' : check_response['dpkg_version_match'],
        'version' : proxy_version,
        'metrics' : {}
    }
    for metric in ['proxy_active_ports' , 'proxy_failed_ports']:
        try:
            print(colored("{} : {}".format(metric,status_json["metrics"]["ProxyRegistry"][metric]), "yellow"))
        except:
            field = "com.wandera.proxy.engine.ProxyRegistry.{}.value".format(metric)
            try:
                print(colored("{} : {}".format(metric, status_json["metrics"][field]), "yellow"))
                proxy_status['metrics'].update( { metric : status_json["metrics"][field] })
            except KeyError as ke:
                print(colored("{} not found in Proxy Status".format(metric), "magenta"))
            except Exception as e:
                print(colored("Failed to get {} ({})".format(metric,e),"red"))

    with hide('running', 'stderr', 'stdout'):
        devices = 0
        if sudo("ls /opt/user-config | grep -v proxyIdentity.json | wc -l") != "0" and sudo("ls /opt/user-config/customers | wc -l") != "0":
            devices = sudo("find /opt/user-config/*/*/devices/|grep '\.conf'| wc -l")
            print "Num devices: {}".format(devices)
        proxy_status.update({'devices' : devices})
    return proxy_status

def print_json(mydict,tabs="",level=1):
    for key in sorted(mydict.keys()):
        if type(mydict[key]).__name__ == "dict" and level <= 1:
            print "{}{} : {{".format(tabs, key)
            print_json(mydict[key],"{}\t".format(tabs),level+1)
            print "{}}}".format(tabs)
        else:
            print "{}{} : {}".format(tabs,key,mydict[key])

def tail_rabbit_log():
    rabbit_log = "/opt/proxy/logs/rabbit.log"
    command = "tail -f {}".format(rabbit_log)
    with hide('running'):
        sudo(command)

def check_fis(version=None):
    print(colored("\nChecking FilterService Status\n","cyan"))
    mysql_cmd = "mysql -e 'SHOW SLAVE STATUS\G' | egrep 'Slave_IO_Running|Slave_SQL_Running'"

    tail_lines = 25
    fis_log = "/opt/filterservice/logs/filter-service-access.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, fis_log)

    fis_extra_commnads = [ mysql_cmd , tail_log_cmd ]

    check_response = check_process("filterservice",check_version=version,silent=True)
    if check_response['dpkg_version_match'] and check_response['running_version'] == True:
        _run_extra_commands(fis_extra_commnads)

    check_response = check_process("filterservice",check_version=version,silent=False)
    #health_check_url = ":1598/healthcheck".format(env.host_string)
    health_check(port=1598)

    print(colored("\nChecking OpenScoringService Status\n","cyan"))

    osc_log = "/opt/openscoringservice/logs/openscoring-service-enterprise.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, osc_log)

    osc_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("openscoring",check_version=version,silent=True)
    if check_response['dpkg_version_match'] and check_response['running_version'] == True:
        _run_extra_commands(osc_extra_commnads)

    check_response = check_process("openscoring",check_version=version)
    #health_check_url = ":2798/healthcheck".format(env.host_string)
    health_check(port=2798)

def check_ras(version=None):
    tail_lines = 25
    ras_log = "/opt/redirectionservice/logs/redirection-service.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, ras_log)

    ras_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("redirection",check_version=version)
    if check_response['dpkg_version_match'] and check_response['running_version'] == True:
        _run_extra_commands(ras_extra_commnads)

def check_tools(version=None):
    check_cmd = "for service in `ls /etc/init.d | grep docker- | cut -d'-' -f2-`; do if [[ `docker ps | grep $service` ]]; then docker ps | grep $service; else echo; echo \"$service is not running\"; echo ; fi; done"
    #_run_extra_commands([check_imp])
    sudo(check_cmd)

def check_delta(version=None):
    tail_lines = 25
    delta_log = "/opt/deltaloadservice/logs/deltaload-service.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, delta_log)

    delta_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("delta",check_version=version)
    if check_response['dpkg_version_match'] and check_response['running_version'] == True:
        _run_extra_commands(delta_extra_commnads)

def check_beacon(version=None):
    tail_lines = 25
    beacon_log = "/opt/beaconservice/logs/beacon-service.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, beacon_log)

    beacon_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("beacon",check_version=version)
    if check_response['dpkg_version_match'] and check_response['running_version'] == True:
        _run_extra_commands(beacon_extra_commnads)

    #health_check_url = ":1698/healthcheck".format(env.host_string)
    health_check(port=1698)

def check_srs(version=None):
    tail_lines = 25
    srs_log = "/opt/streamingreportservice/logs/streamingreportservice.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, srs_log)

    srs_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("streaming-report",check_version=version)
    if check_response['dpkg_version_match'] and check_response['running_version'] == True:
        _run_extra_commands(srs_extra_commnads)

    #health_check_url = ":12898/healthcheck".format(env.host_string)
    health_check(port=1698)

def check_drs(version=None):
    tail_lines = 25
    drs_log = "/opt/devicerelayservice/logs/device-relay-service.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, drs_log)

    drs_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("device-relay-service",check_version=version)
    if check_response['dpkg_version_match'] and check_response['running_version'] == True:
        _run_extra_commands(drs_extra_commnads)

    #health_check_url = ":15998/healthcheck".format(env.host_string)
    health_check(port=15998)

def check_ais(version=None):
    tail_lines = 25
    ais_log = "/opt/appinventoryservice/logs/app-inventory-service.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, ais_log)

    ais_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("app-inventory",check_version=version)
    if check_response['dpkg_version_match'] and check_response['running_version'] == True:
        _run_extra_commands(ais_extra_commnads)

    #health_check_url = "healthcheck?AppInventoryService_ais-201-prod"
    #health_check_url = ":2898/healthcheck".format(env.host_string)
    health_check(port=2898)

def check_osc(version=None):
    tail_lines = 25
    delta_log = "/opt/openscoringservice/logs/openscoring-service-enterprise.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, delta_log)

    osc_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("openscoring",check_version=version)
    if check_response['dpkg_version_match'] and check_response['running_version'] == True:
        _run_extra_commands(osc_extra_commnads)

    #health_check_url = ":2798/healthcheck".format(env.host_string)
    health_check(port=2798)

def check_mms(version=None):
    tail_lines = 25
    delta_log = "/opt/modelsmonitoringservice/logs/models-monitoring-service.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, delta_log)

    mms_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("modelsmonitoringservice",check_version=version)
    if check_response['dpkg_version_match'] and check_response['running_version'] == True:
        _run_extra_commands(mms_extra_commnads)

    #health_check_url = ":2798/healthcheck".format(env.host_string)
    health_check(port=3098)

def check_siem(version=None):
    tail_lines = 25
    siem_log = "/opt/siemservice/logs/siem-service.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, siem_log)

    siem_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("siem",check_version=version)
    if check_response['dpkg_version_match'] and check_response['running_version'] == True:
        _run_extra_commands(siem_extra_commnads)

    #health_check_url = ":1358/healthcheck".format(env.host_string)
    health_check(port=1358)

def check_video(version=None):
    tail_lines = 25
    video_log = "/opt/videoservice/logs/video-service.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, video_log)

    video_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("video",check_version=version)
    if check_response['dpkg_version_match'] and check_response['running_version'] == True:
        _run_extra_commands(video_extra_commnads)

def check_connector(version=None):
    tail_lines = 25
    siem_log = "/opt/connectorservice/logs/connector-service.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, siem_log)

    dpkg_cmd = "dpkg -l | grep emm-connect"

    with hide('running', 'stderr', 'stdout'):
        dpkg_response = sudo(dpkg_cmd)

    format_dpkg_response = re.sub(r'[ \t]+', ':', dpkg_response)

    for service_line in format_dpkg_response.splitlines():
        service = service_line.split(':')[1]
        version = service_line.split(':')[2].split('-')[0]

        grep_service_cmd = "ps -ef | grep connectorservice | egrep -v \"grep|tail -f\"".format(service)
        print "\n* Checking Service: {}. Dpkg Version: {}\n".format(service, version)
        try:
            with hide('running', 'stderr', 'stdout'):
                grep_service_response = sudo(grep_service_cmd)
        except:
            print(colored("Failed to run the {}.".format(grep_service_cmd),"red"))
            print "Return Code: {}, Stderr: {}".format(grep_service_response.return_code, grep_service_response.stderr)

        if grep_service_response.return_code != 0:
            print(colored("\"{}\" Failed. -- Return Code: {}, Stderr: {}".format(grep_service_cmd, grep_service_response.return_code, grep_service_response.stderr),"red"))
        else:
            print "\n{}\n".format(grep_service_response)
            PID = re.sub(r'[ \t]+',':',grep_service_response).split(':')[1]
            JAR = re.sub(r'.* -jar ','',grep_service_response).split(' ')[0]
            serv_vers_pattern = re.compile("emm-connect.*{}".format(version))

            if not serv_vers_pattern.search(grep_service_response):
                print(colored("!!! ERROR: {} is not running in the dpkg version.".format(service),"red"))
                print "PID: {} -- JAR: {} -- DPKG: {}".format(PID, JAR, version)
            else:
                print(colored("Process running correct version:\n\t- PID: {}\n\t- JAR: {}\n".format(PID, JAR),"green"))
        print "------------------------------------"

    _run_extra_commands([tail_log_cmd])

    # TO BE FIXED BY LOOPING HEALTHCHECK UNTIL OK
    print "\nWaiting for healthcheck to respond"
    for i in range(1,25):
        print ".",
        time.sleep(5)
    #health_check_url = "http://{}:2599/v2/heartbeat".format(env.host_string)
    health_check(port=2599,endpoint="v2/heartbeat")

def check_apache(version=None):
    tail_lines = 25
    eu_logs = "/var/log/apache2/eu.wandera.com.*.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, eu_logs)
    check_process_cmd = "ps -ef | grep apache | grep -v grep"

    _run_extra_commands([ check_process_cmd , tail_log_cmd ])

def check_portal(version=None):
    tail_lines = 25
    #portal_log_dir = "/var/www-admin/app/logs/"
    portal_conf_dir = "/var/www-admin/app/config"

    with hide('running', 'stdout', 'stderr'):
        hostname = sudo("hostname")
    if re.search('-stgn', hostname):
        environment = "stgn"
    elif re.search('-(prod|core)', hostname):
        environment = "production"
    elif re.search('-intg', hostname):
        environment = "integration"
    else:
        print "ERROR: Unknwn environment"
        sys.exit(1)

    #log_file = "{}.log".format(environment)
    if environment == 'production':
        conf_file = "environment.yml"
    else:
        conf_file = "config_{}.yml".format(environment)

    portal_log = "/var/log/apache2/*radar*.log"
    portal_conf = os.path.join(portal_conf_dir, conf_file)

    tail_log_cmd = "tail -n {} {}".format(tail_lines, portal_log)
    check_config_cmd = "cat {}".format(portal_conf)
    check_process_cmd = "ps -ef | grep apache | grep -v grep"

    dpkg_cmd = "dpkg -l | grep portal"

    with hide('running', 'stderr', 'stdout'):
        dpkg_response = sudo(dpkg_cmd)

    format_dpkg_response = re.sub(r'[ \t]+', ':', dpkg_response)

    for service_line in format_dpkg_response.splitlines():
        service = service_line.split(':')[1]
        version = service_line.split(':')[2]

    print "\n* Service: {}. Dpkg Version: {}\n".format(service, version)

    _run_extra_commands([ check_process_cmd , check_config_cmd , tail_log_cmd ])

    health_check(port=8080)

def check_app(environ,version=None):
    tail_lines = 25

    dpkg_cmd = "dpkg -l | grep enter"

    with hide('running', 'stderr', 'stdout'):
        dpkg_response = sudo(dpkg_cmd)

    format_dpkg_response = re.sub(r'[ \t]+', ':', dpkg_response)

    failed_services = []
    wrong_version_services = {}

    for service_line in format_dpkg_response.splitlines():
        service = service_line.split(':')[1]
        version = service_line.split(':')[2].split('-')[0]

        grep_service_cmd = "ps -ef | grep {} | egrep -v \"grep|tail -f\"".format(service)
        print(colored("\n* Checking Service: {}. Dpkg Version: {}\n".format(service, version),"blue"))
        time.sleep(1)
        try:
            with hide('running', 'stderr', 'stdout'):
                grep_service_response = sudo(grep_service_cmd)
        except:
            print(colored("Failed to run the {}.".format(grep_service_cmd),"red"))
            print "Return Code: {}, Stderr: {}".format(grep_service_response.return_code, grep_service_response.stderr)

        if grep_service_response.return_code != 0:
            print(colored("\"{}\" Failed. -- Return Code: {}, Stderr: {}".format(grep_service_cmd, grep_service_response.return_code, grep_service_response.stderr),"red"))
            failed_services.append(service)
        else:
            print "\n{}\n".format(grep_service_response)
            PID = re.sub(r'[ \t]+',':',grep_service_response).split(':')[1]
            JAR = re.sub(r'.* -jar ','',grep_service_response).split(' ')[0]
            serv_vers_pattern = re.compile("{}.*{}".format(service, version))

            if not serv_vers_pattern.search(grep_service_response):
                print(colored("!!! ERROR: {} is not running in the dpkg version.".format(service),"red"))
                print "PID: {} -- JAR: {} -- DPKG: {}".format(PID, JAR, version)
                wrong_version_services[service] = [version, JAR]
            else:
                print(colored("Process running correct version:\n\t- PID: {}\n\t- JAR: {}\n".format(PID, JAR),"green"))

            service_alias = re.sub(r'-.*', '', service)
            get_log_cmd = "find /opt/{0}/etc/ -maxdepth 1 -type f -name '*-service.yaml' | xargs grep -A25 '^logging' | grep currentLogFilename | cut -d':' -f2 | tr -d ' ' | sed -e 's:\./::g' | xargs -I logfile sh -c 'echo \"==> /opt/{0}/logfile <==\";tail -n {1} /opt/{0}/logfile'".format(service_alias, tail_lines)
            with hide('running', 'stdout', 'stderr'):
                log = sudo(get_log_cmd)

            if log.failed:
                print(colored("ERROR: Failed to get log file for \"{}\"".format(service),"red"))
            else:
                print "{}".format(log)

        print "------------------------------------"

    if len(failed_services) > 0:
        print "------------------------------------"
        print(colored("ERROR: The following services are stopped:","red"))
        for serv in failed_services:
            print "\t- {}".format(serv)
        print "\nAvailable tasks:"
        print "\tfab -H <host> -u <ssh_username> service_start/service_restart/service_stop/service_status:<service_name>"

    if len(wrong_version_services) > 0:
        print "------------------------------------"
        print(colored("WARN: The following services are not running on the same version as the package","yellow"))
        for serv in wrong_version_services:
            print "\t- {}: version: {} --- Running jar: {}".format(serv, wrong_version_services[serv][0], wrong_version_services[serv][1])

    logging.info("Running health checks")
    util.run_healthcheck(environ,app=True,ip=env.host_string)
    '''for service in ALL_SERVICES:
        if service['node'] == 'app' and not service.get('deprecated'):
            if 'port' in service:
                logging.info("HC for {}".format(service['name']))
                port = service['port']
                if 'healthcheck' in service:
                    endpoint = service['healthcheck']
                else:
                    endpoint = 'healthcheck'
                health_check(port=port, endpoint=endpoint, status_only=True)'''


    app_extra_cmd = ". /usr/local/bin/service-status.sh"
    _run_extra_commands([app_extra_cmd])

    return failed_services, wrong_version_services

#########################################################################

def service_restart(service):
    restart_cmd = "service {} restart".format(service)
    return sudo(restart_cmd)

def service_stop(service):
    stop_cmd = "service {} stop".format(service)
    return sudo(stop_cmd)

def service_start(service):
    start_cmd = "service {} start".format(service)
    return sudo(start_cmd)

def service_status(service):
    status_cmd = "service {} status".format(service)
    return sudo(status_cmd)

def app_services_status():
    app_status_cmd = "~/service-status.sh"
    return sudo(app_status_cmd)

def failover_proxy(src, dest, reverse=None):
    failover_cmd = "woopas failover {} {}".format(src, dest)
    if reverse is not None:
        failover_cmd = "{} -r".format(failover_cmd)
    local(failover_cmd)
    print(colored("\nRemember to set up \"active proxy\" role in Scout accordingly\n","red"))

#########################################################################

def git_pull(puppet_folder,branch='master'):
    # git_pull_command = "cd {}; git pull".format(puppet_folder)
    with cd(puppet_folder):
        sudo("git pull")

def puppet_agent(noop = True, tags = None, force = False):
    if noop == "False" or not noop:
        puppet_agent_cmd = "puppet agent -t"
        noop = False
    else:
        puppet_agent_cmd = "puppet agent -t --noop"

    if tags is not None:
        puppet_agent_cmd = "{} --tags {}".format(puppet_agent_cmd, tags)

    if not noop and not force:
        confirm = _get_option('WARNING : Running puppet agent WITHOUT --noop option ("fab -H <host> -u <user> puppet_agent:noop=True" for -noop option).\nWould you wish to proceed?').lower()
    else:
        confirm = 'y'

    if confirm.lower() == 'y':
        with settings(ok_ret_codes=[0, 2]):
            return sudo(puppet_agent_cmd)
    else:
        print "Skipping puppet agent."
        return -1

def proxy_chain():
    puppet_agent(tags = "proxy_chain")
    puppet_agent(noop = False, tags = "proxy_chain")
    check_proxy()

def puppet_error_options(hostname, noop):
    valid_option = ['1','2','3','4']
    print "WARNING: Puppet returned an error on {} ({}).\nWhat would you like to do?".format(hostname, env.host_string)

    if noop:
        next_step = "puppet live run"
    else:
        next_step = "check node status"

    print """\t1. Re-run"
    \t2. Continue deployment on this node ({})
    \t3. Skip this node and continue on the rest of nodes
    \t4. Abort deployment""".format(next_step)
    option = raw_input("Select an option: ")
    while option not in valid_option:
        option = raw_input("Please, select a valid option: ")
    return option

def run_puppet_deploy(hostname, noop, tags=None, force_deploy=False):
    result = puppet_agent(noop = noop, tags = tags, force = force_deploy)
    while True:
        if result == -1:
            return result
        elif result.failed and result.return_code != 2:
            next_action = puppet_error_options(hostname, noop)
            if next_action == '1':
                run_puppet_deploy(hostname, noop, tags = tags)
            elif next_action == '4':
                sys.exit("** Deployment aborted by user after puppet noop = {} failed (Error Code: {}) **".format(noop, result.return_code))
            else:
                return next_action
        else:
            return 0

def deploy_opscenter():
    build_commands = "cd /opt/opscenter/; docker-compose pull; sleep 5; docker-compose  up -d --build; sleep 5; docker-compose exec web python manage.py collectstatic; sleep 5; docker-compose restart; sleep 15; docker ps"

    ecr_login = "eval $(aws ecr get-login --region eu-west-1)"
    try:
        sudo(ecr_login)
    except:
        print(colored("\nERROR: Failed to get aws login\n"))
        return -1

    try:
        depl = sudo(build_commands)
    except Exception as e:
        print(colored("\nERROR : Failed to deploy opscenter. Command:\n{}".format(build_commands), "red"))


def deploy_tools():
    ecr_login = "eval $(aws ecr get-login --region eu-west-1)"
    try:
        sudo(ecr_login)
    except:
        print(colored("\nERROR: Failed to get aws login\n"))
        return -1

    print "Services to be deployed:"
    for service in ALL_SERVICES:
        if service.get('tools'):
            service_name = service['name']

            deploy_cmd = "docker stop {container}; sleep 5; docker rm {container}; sleep 5; docker run -d -p {from_port}:{to_port} --name {container}".format(**service)
            if 'env_file' in service:
                deploy_cmd = "{} --env-file {}".format(deploy_cmd, service['env_file'])
            if 'volume' in service:
                deploy_cmd = "{} -v {}".format(deploy_cmd, service['volume'])

            if interaction.confirm("Would you like to deploy {}? ".format(service_name)):
                version = interaction.ask_value_with_confirm("Version to deploy: ")
                image = "{}/{}:{}".format(containers_repo, service['image'], version)
                print "Trying \"docker pull {}\"".format(image)
                try:
                    with hide('running', 'stderr', 'stdout'):
                        pull_res = sudo("docker pull {}".format(image))
                    if pull_res.succeeded:
                        deploy_cmd = "{} {}".format(deploy_cmd, image)
                        try:
                            #print "\n{}\n".format(deploy_cmd)
                            depl = sudo(deploy_cmd)
                        except:
                            print(colored("\nERROR : Failed to deploy {}. Command:\n{}".format(service_name, tools_services_cmd[service_name]), "red"))
                    else:
                        print(colored("\nERROR: Failed to pull image. Skipping deployment of {}\n".format(service['container']),"red"))
                except Exception as e:
                    print(colored("\nERROR: Failed to pull image. Skipping\nException: {}".format(e), "red"))
    check_tools()
    return 0

def deploy_server(name, tags=None, force_deploy=False, environ='production'):
    print "Starting release deployment on {} ({})".format(name, env.host_string)

    server_type = re.sub(r'-.*', '', name)
    proxy_regex = re.compile("\.node\.")
    #apache_regex = re.compile("web-[0-9]01-core")
    if proxy_regex.search(name):
        server_type = "prx"
    else:
        for stack in stacks_info:
            for stack_type in sorted(stacks_info[stack].keys()):
                for instance in stacks_info[stack][stack_type]:
                    if name in stacks_info[stack][stack_type][instance]['instances'] and stacks_info[stack][stack_type][instance]['instances'][name] == 'APACHE':
                        server_type = "apache"

    is_tools = False
    is_opscenter = False
    for tools_serv in env_service_types['tools']:
        if server_type == tools_serv:
            is_tools = True
    for service in ALL_SERVICES:
        if server_type == service['node'] and service.get('opscenter'):
            is_opscenter = True

    if is_tools:
        return deploy_tools()
    elif is_opscenter:
        return deploy_opscenter()
    else:
        noop = run_puppet_deploy(env.host_string, noop=True, tags=tags)
        if noop == 4:
            sys.exit("** Deployment aborted by user **")
        elif noop == 3:
            return noop

        live = run_puppet_deploy(env.host_string, noop=False, tags=tags, force_deploy=force_deploy)
        #live = 2
        if live == -1:
            print "WARN: Puppet agent not run. Abort deployment on {} ({})".format(name, env.host_string)
            return -1
        elif live == 4:
            sys.exit("** Deployment aborted by user **")
        elif live == 3:
            return live

        print "Server type: {}".format(server_type)
        if server_type in server_check_function:
            check_function = server_check_function[server_type]
            print "running {}".format(check_function)
            time.sleep(15)
            if server_type == 'app':
                eval(check_function+'("'+ environ + '")')
            else:
                eval(check_function+'()')
        else:
            print(colored("\nNot check function found for server type {}".format(server_type),"yellow"))

        if tags is not None:
            msg_text = "{} in {} ({})".format(tags, name, env.host_string)
        else:
            msg_text = "{} ({})".format(name, env.host_string)
        print "Release deployment completed for {}".format(msg_text)
        post_reponse = util.slack_post("deployment completed for {}".format(msg_text))
        print "Slack post status: {}".format(post_reponse)
        return 0

def start_service(service):
    start_cmd = "service {} start".format(service)
    start = sudo(start_cmd)
    if start.succeeded:
        return check_process(service)
    else:
        print(colored("Failed to start {} on {}".format(service, env.host_string),"red"))


def create_ssh_access(server):
    #server = server.replace("\..*",'')
    server = re.sub(r'\..*', '', server)
    create_cmd = 'mco puppet runall 5 --tags="staff" -S "hostname={}"'.format(server)
    with settings(host_string='puppet.snappli.net', ok_ret_codes=[0, 2]):
        sudo(create_cmd)

def refresh_ff(role,environment):
    #server = server.replace("\..*",'')
    runall = { "app" : 2, "portal" : 2, "proxy" : 5, "app_inv" : 2, "siem" : 2 , "mysql_slave" : 5, "mysql_delta_generator" : 2, "connector" : 2}
    if role not in runall:
        sys.exit(colored("\nERROR: Role {} not available. Available roles: {}\n".format(role, runall.keys()),"red"))
    refresh_cmd = 'mco puppet runall {} --tags="refreshfeatureflags" -S "environment={} and role={}"'.format(runall[role], environment, role)
    with settings(host_string='puppet.snappli.net', ok_ret_codes=[0, 2]):
        sudo(refresh_cmd)

def decrypt_eyaml(string):
    cmd = "eyaml decrypt -s {} --pkcs7-public-key=/var/lib/puppet/public_key.pkcs7.pem --pkcs7-private-key=/var/lib/puppet/private_key.pkcs7.pem".format(string)
    with settings(host_string='puppet.snappli.net'):
        sudo(cmd)

def check_ff(ff):
    print ""

def _get_option(message = None):
    if message is None:
        display_message = "Would you wish to continue? [y/n]: "
    else:
        display_message = message + " [y/n]: "

    option = raw_input(display_message)
    while option.lower() not in valid_options:
        option = raw_input("Please enter a valid option [y/n]: ")

    return option.lower()


def _set_user(user):
    env.user = user

def _set_hosts(hosts):
    print "setting hosts to {}".format(hosts)
    env.disable_known_hosts = True
    env.hosts.extend(hosts)
    print env.hosts

def get_hosts_file():
    sudo("cat /etc/hosts")
