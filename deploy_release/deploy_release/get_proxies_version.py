import libcloud_api
import fabfile
import fabric
from fabric.api import env, run
from fabric.operations import sudo, local
from fabric.context_managers import hide, show
from fabric.context_managers import settings
import re
import os
import sys
from datetime import datetime
from time import sleep
from termcolor import colored

if __name__ == "__main__":
    ssh_user = libcloud_api.init_ssh_user()
    autostart = False
    drivers = None
    if libcloud_api._get_input("Would you like to start stopped instances to check version? (will be re-stopped)") == "Y":
        autostart = True
    drivers = libcloud_api.init_all_drivers()
    #drivers = libcloud_api.init_rackspace_driver()
    proxies_list = libcloud_api.get_all_proxies(drivers)

    grep_service_cmd = "ps -ef | grep rabbit | grep enter | grep -v grep"
    today = datetime.now().strftime('%Y-%m-%d')
    log_file = "proxies_versions.{}".format(today)
    print(colored("\nWill print info to {}\n".format(log_file),"magenta"))

    with open(log_file, "w") as proxy_file:
        for proxy_name in sorted(proxies_list.keys()):
            proxy = proxies_list[proxy_name]['instance']
            proxy_driver = proxies_list[proxy_name]['driver_name']
            if proxy.state != "running":
                #print "\n>> {} <<".format(proxy.name)
                if not autostart:
                    print "\n*** {} is {}. Skipping check ***\n".format(proxy.name, proxy.state)
                    proxy_file.write("{} is {}. Skipped\n".format(proxy.name, proxy.state))
                else:
                    region = proxy.name.split('.')[2][0:-1]
                    driver = { proxy_driver : drivers[proxy_driver] }
                    print "will start {} in {}".format(proxy.name, region)
                    libcloud_api.start_instance(proxy.name, driver, silent=True, statuscake=False)
                    print "Waiting for instance to be up"
                    inst, dvr = libcloud_api.get_instance(proxy.name, driver, silent=True)
                    while inst.state != 'running':
                        sleep(15)
                        inst, dvr = libcloud_api.get_instance(proxy.name, mydrivers=driver, silent=True)
                    sleep(60)
            if proxy.state == "running" or autostart:
                try:
                    with settings(host_string=proxy.name, user=ssh_user):
                        with hide('running', 'stderr', 'stdout'):
                            grep_service_response = sudo(grep_service_cmd)
                    if grep_service_response.failed:
                        print(colored("({}) \"{}\" Failed. -- Return Code: {}, Stderr: {}".format(proxy.name, grep_service_cmd, grep_service_response.return_code, grep_service_response.stderr),"red"))
                        return_dict.update({service : 'failed'})
                        proxy_file.write("{} check failed\n".format(proxy.name))
                    else:
                        JAR = re.sub(r'.* -jar ','',grep_service_response).split(' ')[0].split('-')[2]
                        print "-- {} : {}".format(proxy.name, JAR)
                        proxy_file.write("{} : {}\n".format(proxy.name, JAR))
                except:
                    print(colored("({}) Failed to run the {}.".format(proxy.name, grep_service_cmd),"red"))
                    print "Return Code: {}, Stderr: {}".format(grep_service_response.return_code, grep_service_response.stderr)
                    proxy_file.write("{} check failed (Return Code: {}, Stderr: {})\n".format(proxy.name,grep_service_response.return_code, grep_service_response.stderr))

            if proxy.state != "running":
                #print "\n>> {} <<".format(proxy.name)
                if autostart:
                    region = proxy.name.split('.')[2][0:-1]
                    driver = { proxy_driver : drivers[proxy_driver] }
                    libcloud_api.stop_instance(proxy.name, driver, silent=True,statuscake=False,user=ssh_user)

    fabric.state.output.status = False
    fabric.network.disconnect_all()
    print "\n\nInfo available in * {} *\n".format(log_file)
