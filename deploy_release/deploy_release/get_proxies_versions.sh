#!/bin/bash

BASEDIR=`dirname $0`
ENVDIR="${BASEDIR}/deploy_release_env"

if ! pip list 2> /dev/null | grep -qF "virtualenv "; then
    echo "pip Installing virtualenv";
    sudo -H pip install virtualenv;
fi

if [[ ! -f "${ENVDIR}/bin/activate" ]]; then
  echo "Creating virtual environment"
  if [[ -f "/usr/local/bin/python2.7" ]]; then
    virtualenv --python=/usr/local/bin/python2.7 ${ENVDIR}
  else
    virtualenv ${ENVDIR}
  fi
fi

export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python2.7
source $ENVDIR/bin/activate
cd $BASEDIR

if [ ! -f "$ENVDIR/updated" -o $BASEDIR/requirements.pip -nt $ENVDIR/updated ]; then
    #$BASEDIR/deploy_release/bin/pip install -r $BASEDIR/requirements.pip -E $BASEDIR/deploy_release
    $ENVDIR/bin/pip install -r $BASEDIR/requirements.pip
    touch $ENVDIR/updated
    echo "Requirements installed."
fi

exec python ./get_proxies_version.py
