import argparse
import logging
from os.path import expanduser
from shutil import copyfile
import json
import os
import sys
from jira import JIRA
import util
import datetime

def do_cr_prep(rel_num, ticket, rn_link, jira_creds=None):
    if jira_creds is None:
        jira_creds = util.get_jira_credentails()
    #Inputs:
    # - JIRA ticket
    jira = JIRA("https://snappli.atlassian.net", basic_auth=jira_creds)
    OPS_ticket = jira.issue(ticket)

    release_tasks = util.get_release_tasks(rel_num, jira=jira)
    before_tickets = release_tasks['before']
    after_tickets = release_tasks['after']
    ff_tickets = release_tasks['ff']

    before_content = ""
    for bticket in before_tickets:
        before_content += """
* %(TICKET_NUM)s - %(SUMMARY)s
%(DESCRIPTION)s
""" % {
        'TICKET_NUM': bticket.key,
        'DESCRIPTION': bticket.fields.description,
        'SUMMARY': bticket.fields.summary
    }

    after_content = ""
    for aticket in after_tickets:
        after_content += """
* %(TICKET_NUM)s - %(SUMMARY)s
%(DESCRIPTION)s
""" % {
        'TICKET_NUM': aticket.key,
        'DESCRIPTION' : aticket.fields.description,
        'SUMMARY': bticket.fields.summary
    }

    ff_content = ""
    for fticket in ff_tickets:
        ff_content += """
* %(TICKET_NUM)s - %(SUMMARY)s
%(DESCRIPTION)s
""" % {
        'TICKET_NUM': fticket.key,
        'DESCRIPTION' : fticket.fields.description,
        'SUMMARY': bticket.fields.summary
    }

    # Check if release is odd or even to add proxy deployment or not
    today = datetime.datetime.now()
    next_wed = util.next_weekday(today, 2) # get next wednesday, day = 2
    day2 = next_wed + datetime.timedelta(days=1)
    rel_end = int(rel_num[-1:])
    if rel_end % 2 == 0:
        day3 = day2 + datetime.timedelta(days=1)
        day4 = day3 + datetime.timedelta(days=3)
        day5 = day4 + datetime.timedelta(days=1)
        day6 = day5 + datetime.timedelta(days=1)
        proxy_deployment = """
* *DAY 2 - DAY 6: Proxies deployment*
{code}6. Deploy Proxy to production{code}
-- 2 threads with input files
./release_Proxy_nodes/THREAD_1_PLAN.csv
./release_Proxy_nodes/THREAD_2_PLAN.csv"""
        rel_schedule = """
|%(WED)s|Core, Services (emmc only on con-201), Radar, Tools, Canary Proxy|
|%(DAY2)s|con-101-core, sa-east-1,mil,hkg,tor|
|%(DAY3)s|tok,par,ap-southeast-1,eu-central,eu-west-2 12/100,us-east 11/100|
|%(DAY4)s|ap-southeast-2,us-west-1 33/100,eu-west-2 12/100,DT,us-east 11/100|
|%(DAY5)s|us-west-1 33/100,eu-west-2 38/100,us-east 33/100|
|%(DAY6)s|us-west-1 33/100,eu-west-2 38/100,us-east 45/100|""" % {
        'WED': next_wed.strftime("%Y-%m-%d"),
        'DAY2': day2.strftime("%Y-%m-%d"),
        'DAY3': day3.strftime("%Y-%m-%d"),
        'DAY4': day4.strftime("%Y-%m-%d"),
        'DAY5': day5.strftime("%Y-%m-%d"),
        'DAY6': day6.strftime("%Y-%m-%d")
    }
    else:
        proxy_deployment = ""
        rel_schedule = """
|%(WED)s|Core, Services (emmc only on con-201), Radar, Tools, Canary Proxy|
|%(DAY2)s|con-101-core""" % {
        'WED': next_wed.strftime("%Y-%m-%d"),
        'DAY2': day2.strftime("%Y-%m-%d")
    }

    # Create CR linked
    logging.info("Prepare CR change")
    CR_content = """Created by [Jenkins Pipeline|https://ciops.wandera.net/job/create-production-release-cr]
[RN|%(RN_LINK)s]

*Pull Requests:*
* [All taged %(RL_NUM)s|https://bitbucket.org/snappli-ondemand/puppet/pull-requests/?state=OPEN&query=%(RL_NUM)s]
* Other PRs
-- Add any other changes here if applicable

*FEATURE FLAGS*
* [All PRs|https://bitbucket.org/snappli-ondemand/feature-flags-prod/pull-requests/?state=OPEN&query=%(RL_NUM)s]
%(FF_CONTENT)s

*BEFORE TASKS*
%(BEFORE_CONTENT)s

*DEPLOYMENT*
* *DAY 1*
# Merge branches:
-- [Puppet All|https://bitbucket.org/snappli-ondemand/puppet/pull-requests/?state=OPEN&query=%(RL_NUM)s]
-- [FF|https://bitbucket.org/snappli-ondemand/feature-flags-prod/pull-requests/?state=OPEN&query=%(RL_NUM)s]
-- Additional PRs: as per list above
# Deploy *Core*
{code}3. Deploy Core to production{code}
-- app
--- accountingservice (?)
--- adminservice (?)
--- appinfoservice (?)
--- dataplanservice (?)
--- notificationservice (?)
--- profileservice (?)
--- proxyservice (?)
--- reportservice (?)
--- stateanalyserservice (?)
--- userservice (?)
--- wakeupservice (?)
# Deploy additional *services*:
{code}4. Deploy Services to production{code}
-- app-inventory-service (ais) (?)
-- beaconservice (bea) (?)
-- devicerelayservice (drs) (?)
-- emm-service (*only canary con-201-core*) (con) (?)
-- deltaloadservice (dls) (?)
-- filterservice and openscoringservice (fis) (osc) (?)
-- siemservice (sie) (?)
# Deploy *Portal* (?)
{code}5. Deploy Portal to production{code}
# Deploy *Tools*
{code}7. Deploy Internal Tools{code}
-- Feature Instance Manager (X.X.X) (?)
-- Geronimo (X.X.X) (?)
-- Launch Control (X.X.X) (?)
-- Pass (X.X.X) (?)
-> deploy required when prompted
-> enter required versions *X.X.X* when prompted for version
# Deploy to *Canary Proxy* (?)
-- Set all regions to TODO in release_Proxy_nodes/THREAD_1_PLAN.csv and release_Proxy_nodes/THREAD_2_PLAN.csv
{code}6. Deploy Proxy to production{code}

* *DAY 2*
# Deploy *con-101-core* (?)
{code}4. Deploy Services to production{code}
-- only deploy *emm-connect* to *con-101-core*

%(PROXY_PLAN)s

*AFTER TASKS*
%(AFTER_CONTENT)s

*RELEASE SCHEDULE*
%(SCHEDULE)s""" % {
        'TICKET': ticket,
        'RN_LINK': rn_link,
        'RL_NUM': rel_num,
        'AFTER_CONTENT': after_content,
        'BEFORE_CONTENT': before_content,
        'FF_CONTENT': ff_content,
        'PROXY_PLAN': proxy_deployment,
        'SCHEDULE': rel_schedule
    }
    jira_fields = {
        'project': 'CR',
        'summary': 'R%s to Production' % rel_num,
        'description': CR_content,
        'issuetype': {'name': 'Change'},
        "customfield_13401": {
            "id": "11400",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11400",
            "value": "Extensive / Widespread"
        },
        "customfield_13402": {
            "id": "12031",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/12031",
            "value": "Release (Production)"
        },
        "customfield_13403": {
            "id": "11408",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11408",
            "value": "High"
        },
        "customfield_13404": {
            "id": "11414",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11414",
            "value": "New functionality"
        },
        "customfield_13407": {
            "id": "11417",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11417",
            "value": "High"
        },
    }
    new_issue = jira.create_issue(fields=jira_fields)
    jira.create_issue_link("Relates", new_issue, OPS_ticket)

    print "https://snappli.atlassian.net/browse/%s" % new_issue.key
    print "CR_KEY: {}".format(new_issue.key)

def run():
    parser = argparse.ArgumentParser(description='Create CR for production release', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-l', '--level', help='Log Level',default='INFO', required=False, choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'])
    parser.add_argument('-t', '--ticket', help='Jira Ticket', type=str, required=True)
    parser.add_argument('-c', '--creds', help='Jira Credentials: user password', nargs = '+', required=True)
    parser.add_argument('-n', '--rn', help='Link to access file PR', required=True)
    parser.add_argument('-r', '--release', help='Release number', required=True)

    args = parser.parse_args()

    logging.basicConfig(level=args.level, format='%(asctime)s %(levelname)s %(message)s')

    do_cr_prep(args.release,args.ticket,tuple(args.creds),rn_link=args.rn)

if __name__ == "__main__":
    run()
