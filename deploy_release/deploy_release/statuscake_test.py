import statuscake_api
import libcloud_api
import json
import time
import re
from termcolor import colored

sc_key = "gYKLt2nb75TaOLqCTgim"
sc_user = "jimwalk3r"

if __name__ == "__main__":
    sc, sc_details = libcloud_api.get_status_cake_details()
    print "Getting list of proxies"
    proxies = libcloud_api.get_all_proxies()

    print "\nChecking proxies status:\n"
    for proxy_name in sorted(proxies.keys()):
        for test in sc_details:
            if test["WebsiteName"] == proxy_name:
                if proxies[proxy_name]['instance'].state == 'running' and test['Paused']:
                    print(colored("{} is {} but SC Paused: {}, Unpausing".format(proxy_name, proxies[proxy_name]['instance'].state, test['Paused']),"yellow"))
                    libcloud_api.update_statuscake_test(sc,test,0)
                    test = sc.get_details_test(test["TestID"])
                print "{}: instance state: {}, SC Paused: {}".format(proxy_name, proxies[proxy_name]['instance'].state, test['Paused'])
