# Script to get the versions of the packages deployed and the services running on each instance

import libcloud_api
import util
import fabfile
import fabric
import interaction
import service_config
from fabric.api import env, run
from fabric.operations import sudo, local
from fabric.context_managers import hide, show
from fabric.context_managers import settings
import re
import os
import sys
import argparse
from datetime import datetime
from time import sleep
from termcolor import colored
from git import Repo
from ruamel.yaml import YAML
import logging
import colorer

yaml=YAML()
yaml.preserve_quotes = True
yaml.explicit_start = True

ssh_user = util.init_ssh_user()

env_tld = { "production" : "com|net" , "prod" : "com|net" , "staging" : "biz", "stgn" : "biz" }
today = datetime.now().strftime('%Y-%m-%d')
log_file = "all_versions.{}".format(today)

drivers = libcloud_api.init_all_drivers()

env_config = {
    "production" : {
        'delta_tools_config_path' : "hieradata/environments/operations/common.eyaml",
        'filter_config_path' : "hieradata/environments/operations/role/mysql_slave.eyaml",
        'version_file_path' : "hieradata/environments/production/versions.eyaml"
    },
    "staging" : {
        'delta_tools_config_path' : "hieradata/environments/staging/versions.eyaml",
        'filter_config_path' : "hieradata/environments/staging/versions.eyaml",
        'version_file_path' : "hieradata/environments/staging/versions.eyaml"
    }
}

def get_service_puppet_version(service_name,puppetdir,environment,ref_branch='master'):
    repo = Repo(puppetdir)
    if repo.is_dirty():
        if interaction.ask_value("\nYour git repo is dirty. Please clean it and continue. ", choices=['continue','exit']) == 'e':
            sys.exit(1)

    repo.heads[ref_branch].checkout()
    logging.info("Pulling remote %s", repo.remote())
    repo.remote().pull()

    if environment == 'stgn':
        environment = 'staging'
    if environment == 'prod':
        environment = 'production'

    delta_tools_config_path = "{}/{}".format(puppetdir,env_config[environment]['delta_tools_config_path'])
    filter_config_path = "{}/{}".format(puppetdir,env_config[environment]['filter_config_path'])
    version_file_path = "{}/{}".format(puppetdir,env_config[environment]['version_file_path'])

    with open(delta_tools_config_path, 'r') as delta_config_file_r,\
        open(filter_config_path, 'r') as filter_config_file_r,\
        open(version_file_path, 'r') as version_file_file_r:

        configs = [[yaml.load(delta_config_file_r), delta_config_file_r],
                   [yaml.load(filter_config_file_r), filter_config_file_r],
                   [yaml.load(version_file_file_r), version_file_file_r]]

        for service in service_config.ALL_SERVICES:
            #create service control list for services that might have different alias per environment. i.e. proxy (non prod) v proxy_enterprise (prod)
            if service['name'] == service_name and environment in service.get('env',[environment]):
                service_search_key = service.get('alias', service['name'])
                #print "service_search_key = {}".format(service_search_key)
                key = None
                for config in configs:
                    config_keys = config[0].keys()
                    service_version_keys = [s for s in config_keys if service_search_key in s and "version" in s]
                    #print "len(service_version_keys) = {}".format(len(service_version_keys))
                    if len(service_version_keys) > 1:
                        #print service_version_keys
                        #Need to filter harder
                        service_version_keys = [s for s in service_version_keys if "::version" in s]
                        #print "len(service_version_keys) after = {}".format(len(service_version_keys))

                    if len(service_version_keys) > 1:
                        logging.error("TOO MANY Version Config found for %s", release_service['name'])
                    elif len(service_version_keys) == 1:
                        key = service_version_keys[0]
                        break
                if key is not None:
                    return config[0][key]
                else:
                    logging.error("Could not find version for {}".format(service))
                    return False

def check_node_type(node,node_services,instance_ip_map,web_nodes,puppetdir=None,environment="production",autostart=False, ref_branch='master'):
    if node in ['prx']:
        serv_regex = re.compile(".*\.node\..*\.wandera\.({})".format(env_tld[environment]))
    else:
        serv_regex = re.compile("^{}-".format(node))

    if node == 'fis':
        node_services[node].append('openscoring-service-enterprise')

    logging.info(">> Checking " + colored("{}".format(node),"yellow") + " servers")
    if puppetdir is not None:
        logging.info("Instance Name,Running Version,Dpkg version,Puppet Version")
    else:
        logging.info("Instance Name,Running Version,Dpkg version")
    logging.getLogger().setLevel(logging.ERROR)
    #with open(log_file, "a") as proxy_file:
    for instance_name in sorted(instance_ip_map.keys()):
        if instance_name not in web_nodes and serv_regex.search(instance_name):
            instance_info = instance_ip_map[instance_name]
            instance_ip = instance_info['ip']
            instance_state = instance_info['state']
            instance_driver = None

            if instance_state != "running":
                # Start stopped instances or skip if user selected not to start
                if not autostart:
                    #print "\n*** {} is {}. Skipping check ***\n".format(instance_name, instance_state)
                    logging.info("{} is {}. Skipped".format(instance_name, instance_state))
                else:
                    for driver_name in all_instances:
                        for instance in all_instances[driver_name]:
                            if instance_name == instance.name:
                                instance_driver = { driver_name : drivers[driver_name] }
                    libcloud_api.start_instance(instance_name, instance_driver, silent=True, statuscake=False)
                    print "Waiting for instance to be up"
                    inst, dvr = libcloud_api.get_instance(instance_name, mydrivers=instance_driver, silent=True)
                    while inst.state != 'running':
                        sleep(15)
                        inst, dvr = libcloud_api.get_instance(instance_name, mydrivers=instance_driver, silent=True)
                    sleep(60)

            if instance_state == "running" or autostart:
                # check instances running (stopped instances are up at this point if required)
                print(colored("* {}".format(instance_name),"cyan"))
                for service in node_services[node]:
                    logging.getLogger().setLevel(logging.ERROR)
                    if puppetdir is not None:
                        puppet_version = get_service_puppet_version(service,puppetdir,environment,ref_branch)
                    else:
                        puppet_version = None
                    with settings(host_string=instance_ip, user=ssh_user):
                        try:
                            status = fabfile.check_process(service,silent=True)
                            try:
                                jar = status['process_version']
                                running_version = '.'.join(re.sub(r'.*{}-'.format(service),'',jar).split('.')[:3])
                            except:
                                logging.debug("Failed to parse process running version for {} on {}".format(service,instance_name))
                                running_version = 'N/A'
                            try:
                                dpkg_version = status['dpkg_version']
                            except:
                                logging.debug("ERROR: Failed to parse dpkg running version for {} on {}".format(service,instance_name))
                                dpkg_version = 'N/A'

                            service_check_msg = "{}: running_version: {}, dpkg_version: {}".format(service, running_version, dpkg_version)

                            versions_match = True
                            if not re.search(r'{}'.format(dpkg_version),running_version):
                                versions_match = False
                            if puppet_version is not None:
                                service_check_msg += ", puppet_version: {}".format(puppet_version)
                                if versions_match and not re.search(r'{}'.format(dpkg_version),puppet_version):
                                    versions_match = False

                            logging.getLogger().setLevel(logging.INFO)
                            if not versions_match:
                                status = "MISMATCH"
                                logging.warning("{},".format(instance_name) + colored("{}".format(status),"red") + ",{},{},{},{}".format(service, running_version, dpkg_version, puppet_version))
                            else:
                                status = "OK"
                                logging.info("{},".format(instance_name) + colored("{}".format(status),"green") + ",{},{},{},{}".format(service, running_version, dpkg_version, puppet_version))
                        except Exception as e:
                            logging.error("Failed to check {} on {} ({})".format(service,instance_name,e))
                            #proxy_file.write("Failed to check {} on {}\n".format(service,instance_name))

            if instance_state != "running":
                #Restop started instances, if applicable
                if autostart:
                    libcloud_api.stop_instance(instance_name, instance_driver, silent=True,statuscake=False,user=ssh_user)

def main(autostart=False, drivers=None, environment="production",logfile=None, all_instances=None, proxies_only=False, puppetdir=None, ref_branch='master', force_deploy=False):
    if environment not in env_tld:
        sys.exit("Environment not valid")
    env_regex = re.compile(".*\.wandera\.({})".format(env_tld[environment]))

    global log_file
    if logfile is None:
        log_file += ".{}".format(environment)
    else:
        log_file = logfile

    fileh = logging.FileHandler(log_file, 'a')
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fileh.setFormatter(formatter)

    log = logging.getLogger()  # root logger
    try:
        log.removeHandler(fileh)
    except:
        pass
    log.addHandler(fileh)
    log.propagate = False

    logging.info("** CHECK OF VERSIONS **\n")

    #util.clear_screen()
    if not force_deploy and not autostart and interaction.confirm("Would you like to start stopped instances to check version? (will be re-stopped)", default='n'):
        autostart = True

    print(colored("\nWill print info to {}\n".format(log_file),"magenta"))

    node_services = {}
    for service in service_config.ALL_SERVICES:
        if not service.get('deprecated') and environment in service.get('env',[environment]):
            if service['node'] not in node_services:
                node_services[service['node']] = [ service['name'] ]
            elif service['name'] not in node_services[service['node']]:
                node_services[service['node']].append(service['name'])

    instance_ip_map = {}
    if all_instances is None:
        all_instances = libcloud_api.get_all_instances(drivers)
    for driver_name in sorted(all_instances.keys()):
        for instance in sorted(all_instances[driver_name]):
            if env_regex.search(instance.name):
                instance_ip_map[instance.name] = { 'ip' : instance.public_ip, 'state' : instance.state }

    web_nodes = []
    for stack in service_config.stacks_info:
        try:
            web_nodes.extend(service_config.stacks_info[stack]["CORE"].keys())
        except:
            pass

    #with open(log_file, "a") as proxy_file:
    logging.getLogger().setLevel(logging.INFO)
    logging.info("Checking Services versions in {}".format(environment))
    #check_node_type('fis')
    if not proxies_only:
        for node in sorted(node_services.keys()):
            if node != 'prx':
                check_node_type(node,node_services,instance_ip_map,web_nodes,puppetdir=puppetdir,environment=environment,autostart=autostart,ref_branch=ref_branch)
        if interaction.confirm("Would you like to check the proxies versions?"):
            check_node_type('prx',node_services,instance_ip_map,web_nodes,puppetdir=puppetdir,environment=environment,autostart=autostart,ref_branch=ref_branch)
    else:
        check_node_type('prx',node_services,instance_ip_map,web_nodes,puppetdir=puppetdir,environment=environment,autostart=autostart,ref_branch=ref_branch)

    fabric.state.output.status = False
    fabric.network.disconnect_all()
    print(colored("\n\nInfo available in * {} *\n".format(log_file),"cyan"))

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('--env','--environment','-e', dest = "environment")
    parser.add_argument('--proxy','--proxies','-p', action = "store_true", dest = "proxies_only", default = False)
    parser.add_argument('--puppet','--puppetdir','-d', dest="puppetdir")
    parser.add_argument('-b','--branch', dest="ref_branch", default='master')
    return parser.parse_args()


if __name__ == "__main__":
    options = parse_options(sys.argv[1:])
    if options.environment:
        environment = options.environment
    else:
        environment = "production"
    if options.puppetdir is None:
        puppetdir = interaction.ask_value_with_confirm("Enter your puppet directory: ")
    else:
        puppetdir = options.puppetdir
    while not os.path.isdir(puppetdir):
        puppetdir = interaction.ask_value_with_confirm("{} not found. Enter your puppet directory: ".format(puppetdir))

    logging.basicConfig(level='INFO', format='%(levelname)s: %(message)s')

    main(environment=environment, proxies_only=options.proxies_only, puppetdir=puppetdir, ref_branch=options.ref_branch)
