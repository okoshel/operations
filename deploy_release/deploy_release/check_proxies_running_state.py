#!/usr/local/bin/python

import sys
import os
import re
import json
from datetime import date
import logging
from termcolor import colored

import libcloud_api

from wandera_client.client.service.proxy_manager import ProxyManagerClient
from wandera_client.resource import Server
from wandera_client.client.auth import ServiceAuthentication
from wandera_woopas.database import FileDatabase

import smtplib
from email.mime.text import MIMEText
email_to = 'arturo.noha@wandera.com'
email_from = 'ops@wandera.com'

today = date.today()
DATE = today.isoformat()

home = os.path.expanduser('~')
CONFIG_FILE = os.path.join(home, ".wandera/woopas.json")

current_dir = os.getcwd()
log_dir = os.path.join(current_dir, "logs")
log_file = os.path.join(log_dir, "running_idle_proxies.{}.log".format(DATE))

proxy_name_regex = re.compile("[0-9a-zA-Z]*\.node\.")

def init_proxy_manager():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        pm_url = config_json["woopas"]["proxy_manager_url"]
        pm_api_key = config_json["woopas"]["service_api_key"]
        pm_api_secret = config_json["woopas"]["service_api_secret"]
        pm_auth = ServiceAuthentication(pm_api_key, pm_api_secret)
        pmc = ProxyManagerClient(pm_url,auth=pm_auth)
    except Exception as e:
        sys.exit("ERROR: Failed to load ProxyManagerClient\n({})".format(e.message))

    return pmc

def get_proxies_list():
    all_instances = libcloud_api.get_all_instances()
    all_proxies = {}

    for driver_name in all_instances:
        for instance in all_instances[driver_name]:
            if proxy_name_regex.match(instance.name): # if the name of the instance matches the proxy name convention process
                provider = driver_name.split('_')[0]
                if provider in all_proxies: # add the instance to the list of instances associated to the provider
                    all_proxies[provider].append(instance)
                else:
                    all_proxies[provider] = [instance]
    return all_proxies

def get_proxies_to_stop(proxies_list):
    proxies_to_stop = {}

    pmc = init_proxy_manager()

    for provider in proxies_list:
        for proxy in proxies_list[provider]:
            try:
                pm_proxy = ProxyManagerClient.node_by_name(pmc, proxy.name)
                try:
                    if pm_proxy.state == "idle" and proxy.state == "running":
                        proxy.pm_state = pm_proxy.state
                        if provider in proxies_to_stop:
                            proxies_to_stop[provider].append(proxy)
                        else:
                            proxies_to_stop[provider] = [proxy]
                    elif pm_proxy.state == "idle":
                        print "{} is {} - {}".format(proxy.name,pm_proxy.state,proxy.state)
                except Exception as se:
                    print "ERROR: Could not get PM state from {}\n({})".format(vars(pm_proxy), se)
            except Exception as e:
                print "ERROR: Could not get proxy manager info for {}\n({})".format(proxy.name, e)

    return proxies_to_stop

def send_notification(proxies_to_stop):
    if len(proxies_to_stop) == 0:
        msg_text = "No idle proxies running on {}. No notification sent".format(DATE)
    else:
        msg_text = "Idle Proxies running on {}\n".format(DATE)
        for provider in sorted(proxies_to_stop.keys()):
            for proxy in proxies_to_stop[provider]:
                msg_text = "{}\n[{}] Name: {} - State: {} - Public IPs: {} - Proxy Manager State: {}".format(msg_text, provider, proxy.name, proxy.state, proxy.public_ip, proxy.pm_state)

        msg = MIMEText(msg_text)
        msg['Subject'] = 'Idle Proxies Running ({})'.format(DATE)
        msg['From'] = email_from
        msg['To'] = email_to

        s = smtplib.SMTP('localhost')
        s.sendmail(email_from, [email_to], msg.as_string())
        s.quit()

def main():
    to_stop = get_proxies_to_stop(get_proxies_list())
    try:
        send_notification(to_stop)
    except Exception as e:
        logging.error(colored("Failed to send notification.\n{}".format(e),"red"))
        if not os.path.isdir(log_dir):
            os.makedirs(log_dir)
        with open(log_file, "w") as logfile:
            logfile.write(colored("Failed to send notification.\n{}\n".format(e),"red"))
            for provider in to_stop:
                logfile.write("\n>> {} <<\n\n".format(provider))
                print "\n>> {} <<\n\n".format(provider)
                for proxy in to_stop[provider]:
                    logfile.write("{},{},{},{},{}\n".format(proxy.name,provider,proxy.public_ip,proxy.state,proxy.pm_state))
                    print "{} , {} , {} , {}".format(proxy.name,proxy.public_ip,proxy.state,proxy.pm_state)
                '''print "\n>> {} <<\n".format(provider)
                for proxy in to_stop[provider]:
                    print "{} , {} , {}".format(proxy.name,proxy.state,proxy.pm_state)'''
        print(colored("\nLOG: {}\n".format(log_file),"magenta"))

if __name__ == "__main__":
    #send_notification([])
    main()
