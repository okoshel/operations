#!/usr/local/bin/python

import libcloud_api
from libcloud_api import *
from util import init_ssh_user
import os
import sys
from pprint import pprint
import argparse
import subprocess
import platform
import woopas_amigo

proxy_regex = re.compile(".*\.node\..*\.wandera\.com")

def clear_screen():
    clear = subprocess.Popen( "cls" if platform.system() == "Windows" else "clear", shell=True)
    clear.communicate()

def print_json(mydict,tabs=""):
    for key in mydict:
        if type(mydict[key]).__name__ == "dict":
            print "{}{} : {{".format(tabs, key)
            print_json(mydict[key],"{}\t".format(tabs))
            print "{}}}".format(tabs)
        else:
            print "{}{} : {}".format(tabs,key,mydict[key])

def get_aws_instances():
    options = parse_options(sys.argv[1:])
    regions = get_regions('EC2')
    print "{}".format(regions)

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('--providers', dest = "providers_TODO", nargs = '+', help = "For all providers: all. Default: all")
    parser.add_argument('--get-raw-instances', action = "store_true", dest = "instances_to_get", default = False, help = "print raw data for all instances")
    parser.add_argument('--show-instances', action = "store_true", dest = "instances_to_show", default = False, help = "print name, provider and state info for all instances")
    parser.add_argument('--show-instances-csv', action = "store_true", dest = "instances_to_show_csv", default = False, help = "print name, provider and state info for all instances")
    parser.add_argument('--get-instance', nargs = '+', dest = "instance_to_get", help = "print raw data for a given (list of) instance")
    parser.add_argument('--show-instance', nargs = '+', dest = "instance_to_show", help = "print name, provider and state for a given (list of) instance")
    parser.add_argument('--get-regions', action = "store_true", dest = "get_regions", default = False)
    parser.add_argument('--regions', dest = "regions_TODO", nargs = '+', help = "List of regions. For all regions: all. Default all")
    parser.add_argument('--stop-instances', dest = "instances_to_stop", nargs = '+')
    parser.add_argument('--start-instances', dest = "instances_to_start", nargs = '+')

    parser.add_argument('--get-statuscake', dest = "testId")
    parser.add_argument('--insert-statuscake', action = "store_true", dest = "insert_statuscake", default = False)
    parser.add_argument('--pause-statuscake', dest = "proxies_to_pause_sc", nargs = '+')
    parser.add_argument('--start-statuscake', dest = "proxies_to_start_sc", nargs = '+')

    parser.add_argument('--get-proxies', action = "store_true", dest = "get_proxy_list", default = False)
    parser.add_argument('--get-lb-list', action = "store_true", dest = "get_lb_list", default = False)
    parser.add_argument('--get-lbs-members', action = "store_true", dest = "get_lbs_members", default = False)
    parser.add_argument('--list-images', action = "store_true", dest = "list_images", default = False)
    parser.add_argument('--get-sg', action = "store_true", dest = "get_sg", default = False)
    parser.add_argument('--get-sg-rules', action = "store_true", dest = "get_sg_rules", default = False)
    parser.add_argument('--group-name', dest = "group_name")
    parser.add_argument('--set-stack', nargs = '+', dest = "stack_setup", help = "set weight of a stack. Usage: --set-stack <stack> <weight>")
    parser.add_argument('--insert-sg-ip', action = "store_true", dest = "insert_sg_ip", default = False)
    parser.add_argument('--delete-sg-ip', action = "store_true", dest = "delete_sg_ip", default = False)
    parser.add_argument('--create-sg', action = "store_true", dest = "create_sg", default = False)
    parser.add_argument('--delete-sg', action = "store_true", dest = "delete_sg", default = False)
    parser.add_argument('--file', dest = "input_file")
    parser.add_argument('--group-id', dest = "group_id")
    parser.add_argument('--instances', dest = "sg_instances", nargs = '+')
    parser.add_argument('--port', dest = "sg_port")
    parser.add_argument('--protocol', dest = "protocol")
    parser.add_argument('--to-port', dest = "sg_to_port")
    parser.add_argument('--from-port', dest = "sg_from_port")
    parser.add_argument('--description', dest = "description")
    parser.add_argument('--vpc-id', dest = "vpc_id")
    parser.add_argument('--get-route53', dest = "get_route53_zone")
    parser.add_argument('--get-elastic-ips', action = "store_true", dest = "get_EIPs", default = False)
    parser.add_argument('--assumeid', dest = "assume_proxy")
    parser.add_argument('--clear', action = "store_true", dest = "clear_proxy", default = False)
    parser.add_argument('--check_access', action = "store_true", dest = "check_cloud_access", default = False)
    return parser.parse_args()

#############  MAIN  #############

def main(args):

    #(options, args) = parse_options(sys.argv[1:])
    options = parse_options(args)
    if not options.instances_to_show_csv:
        clear_screen()
    if (options.instances_to_show or options.instances_to_show_csv) and options.instances_to_get:
        sys.exit("ERROR: --get-raw-instances and --show-instances are mutually exclusive")
    #print "\nArguments: {}\n".format(options)
    if options.regions_TODO and not options.providers_TODO and not options.insert_sg_ip and not options.delete_sg_ip and not options.create_sg and not options.delete_sg:
        sys.exit("ERROR: You must specify the provider to which the regions belong")

    if options.check_cloud_access == True:
        check_cloud_access()
        sys.exit(0)

    providers, provider_aliases = get_providers()

    providers_TODO = []
    drivers = {}
    if not options.providers_TODO:
        providers_TODO = providers
    else:
        found = False
        for alias in options.providers_TODO:
            for provider_name in provider_aliases:
                if alias == provider_name or alias in provider_aliases[provider_name]:
                    found = True
                    if provider_name not in providers_TODO:
                        providers_TODO.append(provider_name)
            if not found:
                print "ERROR: Provider {} is not suported. Skipping".format(alias)
        #providers_TODO = options.providers_TODO

    for provider in providers_TODO:
        if provider == "RACKSPACE":
            if options.regions_TODO:
                rs_drivers = init_rackspace_driver(options.regions_TODO)
            else:
                rs_drivers = init_rackspace_driver()

            if len(rs_drivers) > 0:
                #print "[DEBUG] adding RS drivers to all drivers"
                drivers.update(rs_drivers)
        elif provider == "EC2":
            #regions_TODO = aws_regions
            if options.regions_TODO:
                regions_TODO = options.regions_TODO
            else:
                regions_TODO = get_regions("EC2")

            aws_drivers = init_ec2_driver(regions_TODO)
            #aws_drivers = init_ec2_driver()
            if len(aws_drivers) > 0:
                #print "[DEBUG] adding AWS drivers to all drivers"
                drivers.update(aws_drivers)
        elif provider == "SOFTLAYER":
            sl_drivers = init_softlayer_driver()
            if len(sl_drivers):
                #print "[DEBUG] adding SL drivers to all drivers"
                drivers.update(sl_drivers)
        elif provider == "OPENSTACK":
            #dt_drivers = { "OPENSTACK" : "N/A" }
            dt_drivers = init_openstack_driver()
            if len(dt_drivers) > 0:
                #print "[DEBUG] adding DT drivers to all drivers"
                drivers.update(dt_drivers)

    if options.instances_to_show:
        display_instance(drivers)

    if options.instances_to_show_csv:
        print "provider_region,instance_name,state,instance_id,public_ip,pivrate_ip"
        display_instance(drivers,csv=True)

    if options.instances_to_get:
        print "INFO: Getting all instances from relevant providers"
        all_instances = get_all_instances(drivers)
        for driver_name in all_instances:
            dr_instances = all_instances[driver_name]
            if len(dr_instances) > 0:
                print "==> {} instances <<==".format(driver_name)
                for instance in dr_instances:
                    try:
                        json.dumps(vars(instance), indent=4)
                    except:
                        print "{"
                        print_json(vars(instance),"\t")
                        print "}"

    if options.instance_to_show:
        all_instances = get_all_instances(drivers)
        for instance in options.instance_to_show:
            try:
                #print "[DEBUG] Instance to show: {} ({} drivers)".format(instance, len(drivers))
                display_instance(drivers, instance, all_instances)
            except Exception as e:
                print "[ERROR] Could not get information to display for {}".format(options.instance_to_show)
                print "{}".format(e.message)

    if options.instance_to_get:
        all_instances = get_all_instances(drivers)
        for driver_name in all_instances:
            dr_instances = all_instances[driver_name]
            for instance in dr_instances:
                if instance.name in options.instance_to_get:
                    try:
                        json.dumps(vars(instance),indent=4)
                    except:
                        print_json(vars(instance))

    if options.instances_to_stop:
        sc, statuscake_tests_details = get_status_cake_details()
        sc_details = { 'sc' : sc, 'statuscake_tests_details' : statuscake_tests_details }
        user = init_ssh_user()
        '''for instance in options.instances_to_stop:
            if proxy_regex.search(instance):
                user = interaction.ask_value_with_confirm("Enter your ssh user for proxy shutdown: ")
                break'''
        for instance in options.instances_to_stop:
            if stop_instance(instance,drivers,user=user,sc_details=sc_details):
                print "{} successfully stopped".format(instance)
            else:
                print "ERROR: could not stop {}".format(instance)

    if options.instances_to_start:
        sc, statuscake_tests_details = get_status_cake_details()
        sc_details = { 'sc' : sc, 'statuscake_tests_details' : statuscake_tests_details }
        user = init_ssh_user()
        for instance in options.instances_to_start:
            if start_instance(instance,drivers,sc_details=sc_details,user=user):
                print "{} successfully started".format(instance)
            else:
                print "ERROR: could not start {}".format(instance)

    if options.get_proxy_list:
        all_instances = get_all_instances(drivers)
        for driver in all_instances:
            for instance in all_instances[driver]:
                if proxy_regex.search(instance.name):
                    print "{}".format(instance.name)

    if options.get_lb_list:
        lb_drivers = get_LB_list()
        for driver in lb_drivers:
            if re.search('-core', driver.name):
                print "{}".format(vars(driver))

    if options.get_lbs_members:
        lb_drivers = get_LB_list()
        for driver in lb_drivers:
            if re.search('-core', driver.name):
                balancer, members = get_LB_members(driver.name)
                print "** Members of {}".format(driver.name)
                for member in members:
                    print "{}".format(vars(member))

    if options.list_images:
        for driver_name in drivers:
            print "\n{}\n".format(driver_name)
            for image in drivers[driver_name].list_images():
                try:
                    print "ID: {}, Name: {}".format(image.id, image.name)
                except:
                    "ERROR reading {}".format(image)

    if options.stack_setup:
        stack_setup = options.stack_setup
        if len(stack_setup) != 2:
            sys.exit("\nUSAGE: {} <stack [A/B]> <weight>\n".format(sys.argv[0]))
        stack = stack_setup[0]
        weight = stack_setup[1]
        if stack.lower() not in ['a','b']:
            sys.exit("\nUSAGE: {} <stack [A/B]> <weight>\n".format(sys.argv[0]))
        update_route53_weight(stack, int(weight))

    if options.get_sg or options.get_sg_rules:
        group_name = options.group_name

        all_SG = get_security_groups(drivers)
        for driver_name in all_SG:
            if group_name is None:
                print "\nSecurity Groups in {}\n".format(driver_name)
            for sg in all_SG[driver_name]:
                print_sg(sg,group_name=group_name,get_rules=options.get_sg_rules)

    if options.insert_sg_ip or options.delete_sg_ip:
        ec2_driver_region_regex = re.compile("EC2_")
        if (options.sg_instances is None or ( options.group_id is None and options.group_name is None )) and options.input_file is None:
            sys.exit("\nMissing arguments: \n\t--instance <name/ip> [--group-id <SG ID> / --groupe-name <SG Group Name>]\n\t--file")
        params = {}
        if options.insert_sg_ip:
            params['action'] = 'add'
        else:
            params['action'] = 'delete'

        if options.input_file is not None:
            all_instances = get_all_instances()
            with open(options.input_file, "r") as input_file:
                for line in input_file:
                    if 'all_instances' in params:
                        params.pop('all_instances')
                    line = line.rstrip()
                    try:
                        params['group_name'] = line.split(',')[0]
                        params['group_id'] = line.split(',')[1]
                        params['regions'] = [ec2_driver_region_regex.sub('',line.split(',')[2])]
                        params['instances'] = line.split(',')[3]
                        try:
                            if 'protocol' in params:
                                params.pop('protocol')
                            params['protocol'] = line.split(',')[4]
                        except:
                            pass
                        try:
                            if 'from_port' in params:
                                params.pop('from_port')
                            if 'to_port' in params:
                                params.pop('to_port')
                            params['from_port'] = line.split(',')[5]
                            params['to_port'] = line.split(',')[6]
                        except:
                            pass
                        try:
                            if 'description' in params:
                                params.pop('description')
                            params['description'] = line.split(',')[7]
                        except:
                            pass

                        #print "update_security_group_ip\n{}".format(json.dumps(params,indent=4))
                        params['all_instances'] = all_instances
                        update_security_group_ip(**params)
                    except Exception as e:
                        logging.error(colored("Failed to {} : {}\n{}".format(params['action'], params, e),"red"))
        else:
            if options.description is not None:
                params['description'] = options.description

            #for instance_id in options.sg_instances
            params['instances'] = options.sg_instances
            if options.group_id is not None:
                params['group_id'] = options.group_id
            if options.group_name is not None:
                params['group_name'] = options.group_name
            if options.protocol is not None:
                params['protocol'] = options.protocol
            if options.sg_to_port is not None and options.sg_from_port is not None:
                params['to_port'] = options.sg_to_port
                params['from_port'] = options.sg_from_port
            elif options.sg_port is not None:
                params['port'] = options.sg_port
            elif options.sg_to_port is not None or options.sg_from_port is not None:
                sys.exit(colored("\nBoth or none of from_port and to_port must be provided\n","red"))

            if options.regions_TODO is not None:
                params['regions'] = options.regions_TODO

            #params['all_instances'] = all_instances

            if update_security_group_ip(**params) == False:
                logging.error("Failed to {} {}".format(params['action'], instance_id))

    if options.create_sg and options.delete_sg:
        logging.error("--create-sg and --delete-sg options are incompatible")
    else:
        params = {}
        if options.regions_TODO is not None:
            params['regions'] = options.regions_TODO
        if options.group_name is not None:
            params['group_name'] = options.group_name
        if options.group_id is not None:
            params['group_id'] = options.group_id
        if options.vpc_id is not None:
            params['vpc_id'] = options.vpc_id

        if options.create_sg:
            if 'group_name' not in params:
                logging.error("Group Name is required for creation")
            elif options.description is None:
                logging.error("Description is required for creation")
            else:
                sg = create_security_group(options.group_name, options.description, **params)
        elif options.delete_sg:
            delete_security_group(**params)

    if options.get_route53_zone is not None:
        all_records = get_route53_record_sets(get_hosted_zone(options.get_route53_zone).id)
        for rec in all_records:
            print rec['Name']
            print json.dumps(rec,indent=4)

    if options.proxies_to_pause_sc is not None:
        update_proxy_statuscake(options.proxies_to_pause_sc,1)

    if options.proxies_to_start_sc is not None:
        update_proxy_statuscake(options.proxies_to_start_sc,0)

    if options.testId is not None:
        testId = options.testId
        try:
            sc = get_status_cake_driver()
            test = sc.get_details_test(testId)
            print "{}".format(json.dumps(test,indent=4))
        except:
            sc, tests = get_status_cake_details()
            for test in tests:
                print "{}".format(json.dumps(test,indent=4))

    if options.insert_statuscake:
        sc = get_status_cake_driver()
        result = sc.insert_test({'WebsiteName':'test_auto' , 'WebsiteURL' : 'http://test.wandera.net' , 'TestType' : 'HTTP'})
        print "{}".format(json.dumps(result,indent=4))

    if options.get_EIPs:
        driver = drivers['EC2_eu-west-2']
        get_elastic_ips(driver)

    if options.assume_proxy is not None:
        pmc = woopas_amigo.init_proxy_manager()
        aid = woopas_amigo.assumeid(options.assume_proxy, pmc, clear=options.clear_proxy)

if __name__ == "__main__":
    main(sys.argv[1:])
