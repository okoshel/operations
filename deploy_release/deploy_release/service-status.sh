#!/bin/bash
hostname
services="accountingservice:accounting-service adminservice appinfoservice apnservice dataplanservice notificationservice profileservice2 publisherservice reportservice3 stateanalyserservice ticketservice userservice2 wakeupservice"

if [[ `hostname` =~ "app-101-core" ]]; then
    services="$services proxyservice"
fi

if [[ `hostname` =~ "app-101-stgn" || `hostname` =~ "app-101-intg" ]]; then
    services="$services beaconservice"
fi

for service_tags in $services; do
    service=`echo ${service_tags} | cut -d':' -f1`
    if [[ "$service" != "$service_tags" ]]; then
        process=`echo ${service_tags} | cut -d':' -f2`
    else
        process=$service
    fi
    service $service status | egrep --color 'running|not running'
    proc_info=`ps aux | grep $process | grep -v grep | grep -v td-agent | tr -s ' '`
    if [[ ! -z  $proc_info ]]; then
        echo -e "\tUser: `echo $proc_info | cut -d ' ' -f 1,2`"
        echo -e "\tStart time: `echo $proc_info | cut -d ' ' -f 9,10`"
        echo -e "\tVersion: `echo $proc_info | sed 's/.*-jar \+\([^ ]\+\) .*/\1/'`"
    fi
done
