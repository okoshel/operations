#!/usr/local/bin/python

############################################################
# woopas_amigo.py
# Author: Arturo Noha
# version: 3.1
# April 2017
############################################################
# v 1.0: conversion to python of originam woopas_amigo.sh + extra features
# v 2.0: Automatic retrieval of daily proxies to do based on regions to do
# v 2.1: canary proxy management
# v 3.0: check idle/active status from proxy manager and start deployment with active proxies
# v 3.1: deploy region proxies in alphabetical order
#        initialise drivers only for required providers and regions
#
# Arguments:
# 1. (MANDATORY) an input file with the deployment plan: regions to do per day:
#   . format: deplpoyment_day,region,TODO
# 2. a dictionary with the providers libcloud drivers:
#   . format: { 'driver_name1' : 'driver1'[, 'driver_name2' : '{driver2}'...]}
#   . if not provided, it will contact the available providers via libcloud to get them
# 3. a dictionary with the list of instances from the providers to use as reference
#   . format: { 'driver_name1' : [array_of_instances_from_driver1][, 'driver_name2' : [array_of_instances_from_driver2]...]}
#   . if not provided, it will get them from the providers via libcloud
#
# Logic:
# 1. if not provided as arguments, get porivders drivers and instances (via libcloud_api)d
# 2. get the proxies from the regions to deplpoy
# 3. check the state of the proxy from the instances information gather initially
#   3.a. if the instance is stopped, try to start it (via libcloud_api)
# 4. open a new terminal window and tail the rabbit.log on the proxy to upgrade
# 5. run woopas
# 6. if woopas successfull, update the file with the list of proxies and set the proxy to DONE
# 7. check the status of the proxy (via fabfile)
#   - check the proxy_enterprise package verion and verify that the service is running on the same version
#   - check all services status
#   - tail rabbit.log
# 8. if any instance was started, stopped them (via fabfile)


import os
import sys
current_dir = os.getcwd()
import json
from datetime import date
import re
from time import sleep
import subprocess
from subprocess import Popen, PIPE
import fabric
from fabric.context_managers import settings
import tempfile
import appscript
import platform
from termcolor import colored
import logging
from git import Repo

import fabfile
import libcloud_api
from util import CONFIG_FILE
import interaction
from util import print_log
import service_config

import wandera_client
from wandera_client.client.service.proxy_manager import ProxyManagerClient
from wandera_client.resource import Server
from wandera_client.client.auth import ServiceAuthentication

import wandera_woopas
from wandera_woopas import cli
from wandera_woopas.database import FileDatabase
from wandera_woopas.context import setup_context
from wandera_woopas.pm import ProxyController

import click
from click.testing import CliRunner

from ruamel.yaml import YAML
yaml=YAML()
yaml.preserve_quotes = True
yaml.explicit_start = True

today = date.today()
DATE = today.isoformat()
operations_dir = current_dir
proxies_release_dir = os.path.join(current_dir,"release_Proxy_nodes")
logdir = os.path.join(current_dir,"logs")
service_config.CANARY_PROXY = "e00817d4ed17a9e9.node.eu-west-2a.gb.wandera.com"

TODOLIST = {}
proxy_name_regex = re.compile("[0-9a-zA-Z]*\.node\.")

home = os.path.expanduser('~')
started_proxies_file_base = os.path.join(current_dir,"release_Proxy_nodes","started_proxies.list")
get_proxies_script = os.path.join(current_dir,"get_proxies_versions.sh")

def init_ssh_user():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        FAB_USERNAME = config_json["woopas"]["ssh_username"]
    except Exception as e:
        print_log(LOGFILE, "Failed to load ssh user from {}\n".format(CONFIG_FILE) , "ERROR")
        sys.exit(1)

    return FAB_USERNAME

def init_proxy_manager():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        pm_url = config_json["woopas"]["proxy_manager_url"]
        pm_api_key = config_json["woopas"]["service_api_key"]
        pm_api_secret = config_json["woopas"]["service_api_secret"]
        pm_auth = ServiceAuthentication(pm_api_key, pm_api_secret)
        proxy_manager = ProxyManagerClient(pm_url,auth=pm_auth)
    except Exception as e:
        print_log(LOGFILE, "Failed to load ProxyManagerClient\n({})".format(e.message) , "ERROR")
        sys.exit(1)

    return proxy_manager

def find_failover(node,proxy_manager,LOGFILE):
    failover = None
    controller = ProxyController(proxy_manager)
    try:
        failovers = controller.find_failovers(node)
        failover = failovers[0]
        print_log(LOGFILE,'Automatically selecting failover {0} for {1}'.format(failover.server.hostname, node.server.hostname))
    except IndexError:
        print_log(LOGFILE,'Failed to find available failover for {}'.format(node.nodeName),"ERROR")
        #raise PlanFailure('There are no failovers currently available')
    return failover

def clear_screen():
    clear = subprocess.Popen( "cls" if platform.system() == "Windows" else "clear", shell=True)
    clear.communicate()

def update_done_status(done_line,FILE_TO_UPDATE,right_state=True):
    temp_file_name = "{}.tmp".format(FILE_TO_UPDATE)
    done_line = done_line.replace(",TODO","")
    #print "DEBUG: updating line: \"{}\"".format(done_line)
    with open(FILE_TO_UPDATE, "r")  as proxy_plan, open(temp_file_name,"w") as temp_file:
        for line in proxy_plan:
            update_regex = re.compile("{},TODO".format(done_line))
            if right_state:
                replace_str = "{},DONE".format(done_line)
            else:
                replace_str = "{},DONE,WRONG PM STATE".format(done_line)
            line = update_regex.sub(replace_str, line)
            #print "DEBUG: Updated line: \"{}\"".format(line)
            temp_file.write(line)
    os.remove(FILE_TO_UPDATE)
    os.rename(temp_file_name, FILE_TO_UPDATE)

@click.pass_obj
def run_woopas(proxy):
    import requests
    from requests.packages.urllib3.exceptions import (InsecureRequestWarning,
                                                      SNIMissingWarning,
                                                      InsecurePlatformWarning)

    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
    requests.packages.urllib3.disable_warnings(SNIMissingWarning)
    requests.packages.urllib3.disable_warnings(InsecurePlatformWarning)

    #DEFAULT_SOURCE = resource_filename(__name__, 'resources/database_default.json')
    #sources = [DEFAULT_SOURCE, os.path.expanduser(kwargs['file'] or CONFIG_FILE)]
    sources = [CONFIG_FILE]
    ctx_kwargs = {'slow': False, 'throttle': 0.0, 'verbose': 0, 'identity_file': None, 'user': None, 'file': None, 'exclude': None, 'rollback': None, 'yes': None}

    try:
        database = FileDatabase.load_conf(sources)
    except (IOError, ValueError):
        abort('Failed to load database; please check your settings file')

    ctx_obj = setup_context(database, **ctx_kwargs)

    kwargs = { 'arg1' : proxy }
    runner = CliRunner()
    #print "runner.invoke(cli.upgrade_proxy\nobj={}\ntarget={}\n)".format(ctx_obj, proxy)
    result = runner.invoke(cli.upgrade_proxy, [proxy])
    print "\nRESULT\n\n{}".format(vars(result))
    sys.exit(0)
    #cli.upgrade_proxy(obj=ctx_obj,target=proxy)

def new_window(active_svr, user):
    active_cmd = "cd {}; ssh {} -l {} 'tail -f /opt/proxy/logs/rabbit.log'".format(operations_dir, active_svr, user)
    try:
        appscript.app('Terminal').do_script(active_cmd)
    except:
        print_log(LOGFILE,"Could not open window to {}\n".format(active_svr), "WARN")
    #os.system("start cmd /K {}".format(active_cmd))
    #Popen('xterm -hold -e "{}}"'.format(active_cmd))
    #appscript.app('Terminal').do_script(active_cmd)

def get_proxy_puppet_version(puppetdir):
    version_file_path ="{}/hieradata/environments/production/versions.eyaml".format(puppetdir)

    proxy_version = None
    with open(version_file_path, "r") as versions_file:
        config = yaml.load(versions_file)
        proxy_version = config["proxy_enterprise::install::proxy_version"]

    return proxy_version

def print_region_status(region,all_region_proxies,LOGFILE,reference=None):
    clear_screen()
    print_log(LOGFILE, "** Current {} region proxies status **".format(region), "cyan")
    for pm_state in sorted(all_region_proxies.keys()):
        print_log(LOGFILE, "{} proxies".format(pm_state.upper()), "magenta")
        for node_name in sorted(all_region_proxies[pm_state].keys()):
            proxy_msg = "{}".format(node_name)
            log_level = "INFO"
            match = True

            if reference is not None:
                if pm_state not in reference or node_name not in reference[pm_state]:
                    match = False

            if 'proxyName' in all_region_proxies[pm_state][node_name]:
                pm_proxy_name = all_region_proxies[pm_state][node_name]['proxyName']
                proxy_msg += " ({})".format(pm_proxy_name)
                if match and reference is not None and ('proxyName' not in reference[pm_state][node_name] or reference[pm_state][node_name]['proxyName'] != pm_proxy_name):
                    match = False

            if not match:
                log_level = "ERROR"
                for ref_pm_state in reference:
                    for ref_node in reference[ref_pm_state]:
                        if ref_node == node_name:
                            proxy_msg += " - STATUS MISMATCH: original state: {}".format(ref_pm_state)
                            if 'proxyName' in reference[ref_pm_state][ref_node]:
                                proxy_msg += ", Original Proxy: {}".format(reference[ref_pm_state][ref_node]['proxyName'])
            elif reference is not None:
                proxy_msg += " - Same as original"

            print_log(LOGFILE, "\t{}".format(proxy_msg), log_level)

def get_daily_failovers(region_proxies_TODO):
    failovers_list = []

    for pm_state in region_proxies_TODO:
        for proxy in region_proxies_TODO[pm_state]:
            if 'failover' in region_proxies_TODO[pm_state][proxy] and region_proxies_TODO[pm_state][proxy]['failover'] not in failovers_list:
                print(colored("DEBUG: Adding failover {} (used for {})".format(region_proxies_TODO[pm_state][proxy]['failover'],proxy), "cyan"))
                failovers_list.append(region_proxies_TODO[pm_state][proxy]['failover'])
    return failovers_list

def get_instances_to_start(failovers_list, region_proxies_TODO, stopped_proxies):
    started_proxies_list = []

    for failover in failovers_list:
        f_hostname = failover.server.hostname
        if f_hostname in stopped_proxies and f_hostname not in started_proxies_list:
            started_proxies_list.append(f_hostname)

    if 'idle' in region_proxies_TODO:
        for proxy in region_proxies_TODO['idle']:
            if proxy in stopped_proxies and proxy not in started_proxies_list:
                started_proxies_list.append(proxy)

    return started_proxies_list

def print_region_daily_plan(region,started_proxies_list,started_proxies_file,region_proxies_TODO,failovers_list,LOGFILE):
    print_log(LOGFILE, "")
    print_log(LOGFILE, "** Daily plan for {} **".format(region), "cyan")

    if len(failovers_list) > 0:
        print_log(LOGFILE, "List of failovers that will be used:", "magenta")
        for failover in failovers_list:
            print_log(LOGFILE, "\t {} ({})".format(failover.server.hostname,failover._raw['supportedProfileTypes']))

    if len(started_proxies_list) > 0:
        print_log(LOGFILE, "")
        print_log(LOGFILE, "Will START the following instances", "magenta")
        for node in sorted(started_proxies_list):
            print_log(LOGFILE, "\t{}".format(node))

    print_log(LOGFILE, "")
    print_log(LOGFILE, "* Will DEPLOY to the following proxies", "magenta")
    for pm_state in sorted(region_proxies_TODO.keys()):
        print_log(LOGFILE, "- {} proxies".format(pm_state.upper()))
        for node_name in sorted(region_proxies_TODO[pm_state].keys()):
            node_info = "{}".format(node_name)
            if 'proxyName' in region_proxies_TODO[pm_state][node_name]:
                node_info += " ({})".format(region_proxies_TODO[pm_state][node_name]['proxyName'])
            if 'failover' in region_proxies_TODO[pm_state][node_name]:
                node_info += " - Failover: {}".format(region_proxies_TODO[pm_state][node_name]['failover'].server.hostname)
            print_log(LOGFILE, "\t {}".format(node_info))

    previously_started = []
    if os.path.isfile(started_proxies_file):
        with open(started_proxies_file, 'r') as started_file:
            for line in started_file:
                node_started = line.rstrip()
                if node_started not in started_proxies_list and node_started not in previously_started:
                    #print(colored("ADDING {} to PREVIOUSLY STARTED".format(node_started),"cyan"))
                    previously_started.append(node_started)

    if len(started_proxies_list) > 0 or len(previously_started) > 0:
        print_log(LOGFILE, "")
        print_log(LOGFILE, "* Will attempt to STOP to the following instances", "magenta")
        if len(started_proxies_list) > 0:
            print_log(LOGFILE, "- Started for this execution")
            for node_name in sorted(started_proxies_list):
                print_log(LOGFILE, "\t {}".format(node_name))
        if len(previously_started) > 0:
            print_log(LOGFILE, "- Started during PREVIOUS executions")
            for node_name in sorted(previously_started):
                print_log(LOGFILE, "\t {}".format(node_name))

def get_region_proxies_details(region,provider_proxies,proxy_manager,LOGFILE,dryrun):
    all_region_proxies = {}
    stopped_proxies = []
    failovers_list = []
    controller = ProxyController(proxy_manager)
    region_regex = re.compile(".*\.node[\.-]{}[a-z0-9]*[\.-].*wandera[\.-]com".format(region))

    for proxy in provider_proxies:
        #print "DEBUG: checking if {} needs deployment".format(proxy.name)

        if region_regex.search(proxy.name): # add all proxies from the region to do to the list of proxies to deploy
            #print "DEBUG: Adding {} to all_region_proxies".format(proxy.name)
            proxy_running_state = proxy.state

            try:
                #pm_proxy = ProxyManagerClient.node_by_name(proxy_manager, proxy.name)
                pm_proxy = proxy_manager.node_by_name(proxy.name)
                proxy_pm_state = pm_proxy.state
            except:
                print_log(LOGFILE, "Failed to get Proxy Manager Status for {}".format(proxy.name), "WARN")
                continue

            if not proxy_pm_state in all_region_proxies:
                all_region_proxies[proxy_pm_state] = {}

            if proxy.name not in all_region_proxies[proxy_pm_state]:
                all_region_proxies[proxy_pm_state][proxy.name] = { 'node' : proxy }

            if 'proxyName' in pm_proxy._raw:
                all_region_proxies[proxy_pm_state][proxy.name]['proxyName'] = pm_proxy._raw['proxyName']

            if proxy_pm_state == 'active':
                failover = find_failover(pm_proxy,proxy_manager,LOGFILE)
                if failover is not None:
                    all_region_proxies[proxy_pm_state][proxy.name]['failover'] = failover
                    if failover not in failovers_list:
                        failovers_list.append(failover)

            if proxy_running_state != "running": # Get list of stopped instances from regions to deploy
                print_log(LOGFILE, "INFO: {} is stopped. Adding to list of stopped instances.".format(proxy.name))
                stopped_proxies.append(proxy.name)

    return all_region_proxies,stopped_proxies

def restop_instances(user,started_proxies_file,started_proxies_list,provider,region,proxy_manager,dryrun,LOGFILE):
    if os.path.isfile(started_proxies_file):
        with open(started_proxies_file,"r") as started_file:
            for line in started_file:
                proxy_stopped = line.rstrip()
                if proxy_stopped not in started_proxies_list:
                    started_proxies_list.append(proxy_stopped)

    print_log(LOGFILE, "Checking instances to stop amongst: {}".format(sorted(started_proxies_list)))
    if len(started_proxies_list) > 0:
        started_proxies_list_ori = started_proxies_list
        for proxy_stopped in sorted(started_proxies_list_ori):
            #print(colored("Checking status of {} to stop".format(proxy_stopped),"cyan"))
            # check that the proxy is idle and remove from to-stop list otherwise
            pm_proxy = proxy_manager.node_by_name(proxy_stopped)
            if pm_proxy.state != 'idle':
                print_log(LOGFILE, "{} is NOT idle (current state: {}). Will no be restopped. Please check deployment status".format(proxy_stopped,pm_proxy.state) , "WARN")
                started_proxies_list.remove(proxy_stopped)
            else:
                print_log(LOGFILE, "{} is idle (current state: {}). Will restop".format(proxy_stopped,pm_proxy.state))
        if not dryrun:
            fabric.state.output.status = False
            fabric.network.disconnect_all()
            print_log(LOGFILE, "Will stop the following instances:", "cyan")
            for node in sorted(started_proxies_list):
                print_log(LOGFILE, "\t{}".format(node))
            if not interaction.confirm("\n>> Would you like to proceed? "):
                print_log(LOGFILE, "User chose not to stop intances. Please make sure to do it manually","WARN")
            '''else:
                libcloud_api.stop_instance(started_proxies_list, provider=provider, regions=region, user=user)'''
        else:
            print_log(LOGFILE, "Would re-stop:")
            for proxy in started_proxies_list:
                print_log(LOGFILE, "{}".format(proxy))

def main(rel_num,proxy_plan,puppetdir,drivers=None,all_instances=None,dryrun=True):
    clear_screen()

    proxy_version = get_proxy_puppet_version(puppetdir)
    if proxy_version is None:
        sys.exit(colored("\nERROR: Could not find proxy version in config\n", "red"))

    if not os.path.isfile(CONFIG_FILE):
        print(colored("\nERROR: Configuration file {} not found.\n".format(CONFIG_FILE), "red"))
        sys.exit(1)

    repo = Repo(puppetdir)
    if repo.is_dirty():
        if interaction.ask_value("\nYour git repo is dirty. Please clean it and continue. ", choices=['continue','exit']) == 'e':
            sys.exit(1)

    repo.heads['production'].checkout()
    logging.info("Pulling remote %s", repo.remote())
    repo.remote().pull()

    FAB_USERNAME = init_ssh_user()

    proxy_manager = init_proxy_manager()

    dt_proxies_file_name = os.path.join(proxies_release_dir,"DT-proxies.plan")
    PROXY_DEPLOY_PLAN = proxy_plan
    if not os.path.isfile(PROXY_DEPLOY_PLAN):
        print(colored("\nERROR: {} not found\n".format(PROXY_DEPLOY_PLAN), "red"))
        sys.exit(1)

    DONELIST = os.path.join(proxies_release_dir,"RL_{}_done_proxies.txt".format(rel_num))
    if dryrun:
        LOGFILE = os.path.join(logdir,"RL_{}_proxies_deployment_dryrun.log".format(rel_num))
    else:
        LOGFILE = os.path.join(logdir,"RL_{}_proxies_deployment_LIVE.log".format(rel_num))

    done_proxies_list = []
    done_proxy_regex = re.compile(",DONE")
    if os.path.isfile(DONELIST):
        with open(DONELIST, "r") as done_proxies_file:
            for line in done_proxies_file:
                line = line.rstrip()
                if done_proxy_regex.search(line):
                    done_proxies_list.append(line.split(',')[1])

    today_regex = re.compile("^[0-9],.*,TODO")
    day_TODO = -1
    with open(PROXY_DEPLOY_PLAN, "r") as proxy_plan_file:
        for line in proxy_plan_file:
            # get day to do: number of first line TODO
            line = line.rstrip()
            if today_regex.search(line):
                day_TODO = line.split(',')[0]
                break

    providers_TODO = {}
    regions_TODO = 0
    if day_TODO < 0:
        print(colored("\nERROR: Could not find a day to deploy.\n", "red"))
        sys.exit(1)
    else:
        if day_TODO == "0":
            print_log(LOGFILE, "CANARY PROXY DEPLOYMENT ({})".format(service_config.CANARY_PROXY),"INFO")
            regions_TODO += 1
        else:
            print_log(LOGFILE, "Executing plan for day {} of proxy deployment.".format(day_TODO),"INFO")
            print_log(LOGFILE, "day,Provider,Region,day_weight,status")
            day_todo_regex = re.compile("^{},.*,TODO".format(day_TODO))
            with open(PROXY_DEPLOY_PLAN, "r") as proxy_plan_file:
                for line in proxy_plan_file:
                    line = line.rstrip()
                    if day_todo_regex.search(line):
                        regions_TODO += 1
                        print_log(LOGFILE, "{}".format(line))
                        provider = line.split(',')[1]
                        if provider not in providers_TODO:
                            providers_TODO[provider] = []
                        region = line.split(',')[2]
                        if region not in providers_TODO[provider]:
                            providers_TODO[provider].append(region)

        if fabfile._get_option("Continue?") != "Y":
            print_log(LOGFILE, "Deployment aborted by user\n" , "WARN")
            sys.exit(1)
        today_regex = re.compile("^{},.*,TODO".format(day_TODO))

    '''try:
        sc, statuscake_tests_details = libcloud_api.get_status_cake_details()
    except Exception as e:
        print_log(LOGFILE, "Failed to get Status Cake Details.", "ERROR")
        sc = None
        statuscake_tests_details = None
        if not interaction.confirm("Would you like to continue deployment? You will need to update Status Cake Manually: "):
            sys.exit(0)'''

    if day_TODO != "0":
        if drivers is None:
            drivers = {}
            for provider in providers_TODO:
                print_log(LOGFILE, "Getting {} {} drivers".format(provider, providers_TODO[provider]))
                init_command = "libcloud_api.init_{}_driver([".format(provider.lower())
                for region in providers_TODO[provider]:
                    init_command = "{}'{}',".format(init_command, region)
                init_command = "{}])".format(init_command)
                init_command.replace( ",]" , "]" )
                prov_drivers = eval(init_command)
                if len(prov_drivers) > 0:
                    drivers.update(prov_drivers)

                #drivers = libcloud_api.init_all_drivers()

        if all_instances is None:
            print_log(LOGFILE, "Getting all instances")
            all_instances = libcloud_api.get_all_instances(drivers)

        all_proxies = {}
        print_log(LOGFILE, "Getting the list of proxies")
        for driver_name in all_instances: # iterate through all the instances
            for instance in all_instances[driver_name]:
                if proxy_name_regex.match(instance.name): # if the name of the instance matches the proxy name convention process
                    provider = driver_name.split('_')[0]
                    if provider in all_proxies: # add the instance to the list of instances associated to the provider
                        all_proxies[provider].append(instance)
                    else:
                        all_proxies[provider] = [instance]

    with open(PROXY_DEPLOY_PLAN, "r") as proxy_plan_file:
        regions_counter = 1
        for plan_line in proxy_plan_file:
            plan_line = plan_line.rstrip()
            # print_log(LOGFILE, "{}".format(plan_line),"DEBUG")
            if today_regex.search(plan_line):
                print_log(LOGFILE, "DEBUG: Processing {}\nRegion {} of {}".format(plan_line, regions_counter, regions_TODO))
                provider = plan_line.split(',')[1]
                region = plan_line.split(',')[2]
                weight = plan_line.split(',')[3]
                region_proxies_TODO = {} # will hold the list of proxies from the regions that need to be deployed. All if weight = 1, half it weight = 0.5

                region_regex = re.compile(".*\.node[\.-]{}[a-z0-9]*[\.-].*wandera[\.-]com".format(region)) # node name parts split by '-' for DT, '.' for AWS, RS, SL
                all_region_proxies = {}
                started_proxies_file = "{}.{}.{}.{}.day{}".format(started_proxies_file_base,provider,region,rel_num,day_TODO)
                if day_TODO == "0":
                    drivers = eval('libcloud_api.init_' + provider.lower() + '_driver([\'' + region + '\'])')
                    canary_instance, canary_driver = libcloud_api.get_instance(service_config.CANARY_PROXY, drivers)
                    pm_proxy = proxy_manager.node_by_name(service_config.CANARY_PROXY)
                    proxy_pm_state = pm_proxy.state

                    proxy_instance = {}
                    proxy_instance[service_config.CANARY_PROXY] = canary_instance

                    all_region_proxies[proxy_pm_state] = proxy_instance
                else:
                    print_log(LOGFILE, "--------------------------------\n* Deploying to proxies in {}\n".format(region))
                    print_log(LOGFILE, "({})\n".format(plan_line))

                    provider_proxies = all_proxies[provider]

                    all_region_proxies, stopped_proxies = get_region_proxies_details(region, provider_proxies, proxy_manager, LOGFILE, dryrun)

                region_proxy_done_regex = re.compile("\.node\.{}.*,DONE".format(region))

                pending_proxies = []
                only_pending = False
                region_proxy_todo_regex = re.compile("\.node\.{}.*,TODO".format(region))
                if os.path.isfile(DONELIST):
                    with open(DONELIST, "r") as done_proxies_file:
                        for line in done_proxies_file:
                            if region_proxy_todo_regex.search(line):
                                print_log(LOGFILE, "DEBUG: Pending {}".format(line), "red")
                                pending_proxies.append(line.split(',')[1])
                if len(pending_proxies) > 0:
                    print_log(LOGFILE, "The following proxies are pending for {}".format(region), "WARN")
                    for proxy_pending in pending_proxies:
                        print_log(LOGFILE, "\t- {}".format(proxy_pending))
                    if fabfile._get_option("\nWould you like to deploy only these proxies?") == "Y":
                        only_pending = True
                        count_TODO = len(pending_proxies)

                        for pm_state in sorted(all_region_proxies.keys()):
                            for proxy_name in sorted(all_region_proxies[pm_state].keys()):
                                if proxy_name in pending_proxies:
                                    #print(colored("Found proxy {} ({})".format(proxy_name,pm_state),"magenta"))
                                    if pm_state not in region_proxies_TODO:
                                        region_proxies_TODO[pm_state] = {}
                                    region_proxies_TODO[pm_state][proxy_name] = all_region_proxies[pm_state][proxy_name]

                if not only_pending:
                    if weight == "1": # if the daily region weight is 1, we'll do all proxies, unless they have already been done (i.e. aborted execution)
                        #region_proxies_TODO = all_region_proxies
                        region_proxies_TODO = {}
                        for pm_state in sorted(all_region_proxies.keys()):
                            for proxy_name in sorted(all_region_proxies[pm_state].keys()):
                                if proxy_name in done_proxies_list:
                                    logging.warning("{} already deployed. Skipping".format(proxy_name))
                                else:
                                    if pm_state not in region_proxies_TODO:
                                        region_proxies_TODO[pm_state] = {}
                                    region_proxies_TODO[pm_state][proxy_name] = all_region_proxies[pm_state][proxy_name]

                        count_TODO = 0
                        for state in region_proxies_TODO:
                            count_TODO += len(region_proxies_TODO[state])
                    else: # if the weight of region is not 1, select only half of the region's proxies to deploy
                        total_region_proxies = 0
                        numer = int(weight.split('/')[0])
                        denumer = int(weight.split('/')[1])
                        for pm_state in all_region_proxies:
                            total_region_proxies = total_region_proxies + len(all_region_proxies[pm_state])
                        print_log(LOGFILE, "DEBUG: weight = {}. Getting required instances. Total number of proxies = {}".format(weight, total_region_proxies))

                        region_plan_regex = re.compile(",{},".format(region))
                        region_done_regex = re.compile(",{},.*,DONE".format(region))
                        region_proxies_done_count = 0
                        region_days_done = 0
                        region_days_count = 0
                        # Get number of proxies already deployed for the region
                        #region_proxies_done_count = len(region_proxies_done)
                        for proxy_done in done_proxies_list:
                            if region_regex.search(proxy_done):
                                region_proxies_done_count += 1

                        # Get total number of deployment days for the region and the number of days already done
                        region_total_weight = 0
                        with open(PROXY_DEPLOY_PLAN, "r") as done_days_file:
                            for line in done_days_file:
                                if region_plan_regex.search(line):
                                    region_days_count += 1
                                    region_total_weight += float(weight.split('/')[0]) / float(weight.split('/')[1])
                                    if region_done_regex.search(line):
                                        region_days_done += 1

                        region_proxies_remaining = total_region_proxies - region_proxies_done_count
                        #region_days_remaining = region_days_count - region_days_done

                        if (total_region_proxies * numer) % denumer == 0: # if number of prixies is multiple of daily denumericient, we'll do division
                            count_TODO = (total_region_proxies * numer) / denumer
                        else:
                            count_TODO = ( (total_region_proxies * numer) // denumer ) + 1 # if number of proxies is odd number, we'll do floor division by daily denumer + 1'''
                        if count_TODO > region_proxies_remaining: #if remaining number if proxies is lower than calculated count do remaining
                            count_TODO = region_proxies_remaining

                        print_log(LOGFILE, "{} of {} proxies deployed in {} ({} remaining)".format(region_proxies_done_count,total_region_proxies,region,region_proxies_remaining))
                        print_log(LOGFILE, "Will deploy to {} proxies\n".format(count_TODO))
                        sleep(3)
                        added_count = 1

                        for pm_state in sorted(all_region_proxies.keys()):
                            print_log(LOGFILE, "Adding {} proxies".format(pm_state),"DEBUG")
                            for proxy_name in sorted(all_region_proxies[pm_state].keys()):
                                if added_count > count_TODO:
                                    break

                                if proxy_name == service_config.CANARY_PROXY:
                                    print_log(LOGFILE, "service_config.CANARY_PROXY proxy ({}) found. Skipping...".format(service_config.CANARY_PROXY),"INFO")
                                    continue

                                if proxy_name not in done_proxies_list:
                                    print_log(LOGFILE, "Adding {} ({}) to the list of proxies to do.".format(proxy_name, pm_state),"DEBUG")

                                    if pm_state not in region_proxies_TODO:
                                        region_proxies_TODO[pm_state] = {}
                                    region_proxies_TODO[pm_state][proxy_name] = all_region_proxies[pm_state][proxy_name]
                                    added_count = added_count + 1
                            if added_count > count_TODO:
                                break

                    print_log(LOGFILE, "adding proxies to do for {} {} to status tracking file ({})".format(provider, region, DONELIST),"INFO")
                    if not dryrun:
                        with open(DONELIST, "a+") as done_proxies_file:
                            for pm_state in sorted(region_proxies_TODO.keys()):
                                for proxy_name in sorted(region_proxies_TODO[pm_state].keys()):
                                    if proxy_name not in pending_proxies:
                                        done_proxies_file.write("{},{},TODO\n".format(DATE, proxy_name))
                                    else:
                                        print_log(LOGFILE, "{} is already pending deployment. Skip adding to file".format(proxy_name), "WARN")
                    else:
                        print_log(LOGFILE, "INFO: Would add the following to {}".format(DONELIST))
                        for pm_state in sorted(region_proxies_TODO.keys()):
                            for proxy_name in sorted(region_proxies_TODO[pm_state].keys()):
                                if proxy_name not in pending_proxies:
                                    print_log(LOGFILE, "{},{},TODO\n".format(DATE, proxy_name))
                                else:
                                    print_log(LOGFILE, "{} is already pending deployment. Will skip adding to file".format(proxy_name), "WARN")

                proxies_counter = 1

                failovers_list = get_daily_failovers(region_proxies_TODO)
                started_proxies_list = get_instances_to_start(failovers_list, region_proxies_TODO, stopped_proxies)

                # print region deployment plan and ask for confirmation
                print_region_status(region,all_region_proxies,LOGFILE)
                print_region_daily_plan(region,started_proxies_list,started_proxies_file,region_proxies_TODO,failovers_list,LOGFILE)

                print
                if not interaction.confirm("Would you like to proceed? "):
                    continue

                if len(started_proxies_list) > 0:
                    for node in sorted(started_proxies_list):
                        if not dryrun:
                            print "libcloud_api.start_instance({},{})".format(node, drivers)
                            #libcloud_api.start_instance(node, drivers)
                    if not dryrun:
                        print_log(LOGFILE, "Waiting for started instance to fully initiate.")
                        print_log(LOGFILE, "List of instances started written to {}\n".format(started_proxies_file))
                        #sleep(120)
                    else:
                        print_log(LOGFILE, "Would wait 2 min for started instance to fully initiate.")

                for pm_state in sorted(region_proxies_TODO.keys()):
                    print_log(LOGFILE, "Deploying to {} proxies\n".format(pm_state))
                    for PROXY in sorted(region_proxies_TODO[pm_state].keys()):
                        #if only_pending and PROXY not in pending_proxies:
                            #continue
                        print_log(LOGFILE, "* Deploying to {}\nProxy {} of {} in {}\nRegion {} of {}".format(PROXY, proxies_counter, count_TODO, region, regions_counter, regions_TODO))

                        print_log(LOGFILE, "Pausing Status cake for {}".format(PROXY))
                        proxy_test_regex = re.compile(PROXY)
                        paused = False # register if it was already paused not to start it after deployment
                        proxy_sc_test = None
                        '''for sc_test in statuscake_tests_details:
                            if proxy_test_regex.search(sc_test["WebsiteName"]):
                                print_log(LOGFILE, "Current status: TestID: {} - WebsiteName: {} - Paused: {}".format(sc_test["TestID"], sc_test["WebsiteName"], sc_test["Paused"]))
                                proxy_sc_test = sc_test
                                if sc_test["Paused"]:
                                    print_log(LOGFILE, "Already paused. Skipping", "WARN")
                                    paused = True
                                else:
                                    if not dryrun:
                                        sc_status = libcloud_api.update_statuscake_test(sc, sc_test, 1)
                                        print_log(LOGFILE, "StatusCake set to Paused = {}".format(sc_status))
                                    else:
                                        print_log(LOGFILE, "Would Pause the Status Cake Test", "WARN")'''

                        print_log(LOGFILE, "Opening servers log tail windows")
                        if not dryrun:
                            new_window(PROXY, FAB_USERNAME)

                        woopas_cmd = "woopas upgrade_proxy {}".format(PROXY)
                        if 'failover' in region_proxies_TODO[pm_state][PROXY]:
                            ''' Add failover state check ?? '''
                            woopas_cmd += " -f {}".format(region_proxies_TODO[pm_state][PROXY]['failover'].server.hostname)

                        if not dryrun:
                            print_log(LOGFILE, "{}".format(woopas_cmd))
                            '''woopas_open = Popen(woopas_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
                            out, err = woopas_open.communicate()'''
                        else:
                            print_log(LOGFILE, "Would run {}".format(woopas_cmd))

                        if not dryrun:
                            print_log(LOGFILE, "*****************************************************************************")
                            print_log(LOGFILE, "Checking status of {}".format(PROXY))
                            print_log(LOGFILE, "*****************************************************************************")
                            try:
                                with settings(host_string = PROXY, user = FAB_USERNAME):
                                    proxy_status = fabfile.check_proxy(proxy_version)
                                    if proxy_status['version_match']:
                                        print_log(LOGFILE, "*****************************************************************************")
                                        print_log(LOGFILE, "{} successfully updated".format(PROXY),"green")
                                        print_log(LOGFILE, "Version: {}".format(proxy_status['version']),"cyan")
                                        print_log(LOGFILE, "Number of devices in /etc/user-config: {}".format(proxy_status['devices']), "cyan")
                                        for metric in proxy_status['metrics']:
                                            print_log(LOGFILE, "{} : {}".format(metric, proxy_status['metrics'][metric]), "cyan")

                                        # check that the proxy is back to it's original state
                                        after_proxy = proxy_manager.node_by_name(PROXY)
                                        current_state = after_proxy.state
                                        expected_state = pm_state
                                        if PROXY in started_proxies_list:
                                            expected_state = 'idle'
                                        if current_state != expected_state:
                                            print_log(LOGFILE, "{} NOT in expected STATE:".format(PROXY), "red")
                                            print_log(LOGFILE, "\tOriginal State: {}".format(expected_state), "red")
                                            print_log(LOGFILE, "\tCurrent State: {}".format(current_state), "red")
                                            print_log(LOGFILE, "Setting to DONE in {} with a wrong state warning".format(DONELIST))
                                            print_log(LOGFILE, "*****************************************************************************")
                                            update_done_status(PROXY,DONELIST,right_state=False)
                                        else:
                                            print_log(LOGFILE, "PM State OK: (original: {} / actual: {})".format(expected_state, current_state), "green")
                                            print_log(LOGFILE, "Setting to DONE in {}".format(DONELIST))
                                            print_log(LOGFILE, "*****************************************************************************")
                                            update_done_status(PROXY,DONELIST)
                                    else:
                                        print_log(LOGFILE, "rabbit version in proxy and release version do not match. Please check deployment status.", "ERROR")
                            except Exception as e:
                                print_log(LOGFILE, "Failed to run check on {}\nError: {}\nPlease check manually".format(PROXY, e.message))
                                if fabfile._get_option() == "n":
                                    print_log(LOGFILE, "Deployment aborted by user", "WARN")
                                    sys.exit(1)
                        else:
                            print_log(LOGFILE, "Would check proxy status and logs on {}".format(PROXY))
                            print_log(LOGFILE, "Would update {} to DONE in {}\n".format(PROXY, DONELIST))

                        if not paused and proxy_sc_test is not None:
                            if not dryrun:
                                #sc_status = libcloud_api.update_statuscake_test(sc, proxy_sc_test, 0)
                                print_log(LOGFILE, "StatusCake set to Paused = {}".format(sc_status))
                            else:
                                print_log(LOGFILE, "Would Restart the Status Cake Test")
                        else:
                            print_log(LOGFILE, "Status cake was originally Paused. Skipping restart", "WARN")

                        print_log(LOGFILE, "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+")
                        print_log(LOGFILE, "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+")
                        proxies_counter += 1

                        sleep(1)

                print_log(LOGFILE, "Completed deployment to {} region {} for day {}".format(provider, region, day_TODO),"DEBUG")
                regions_counter += 1

                all_region_proxies_after, stopped_proxies_after = get_region_proxies_details(region, provider_proxies, proxy_manager, LOGFILE, dryrun)
                print_region_status(region,all_region_proxies_after,LOGFILE,reference=all_region_proxies)

                if not interaction.confirm("Would you like to confirm successfull deployment and stop started instances? "):
                    continue

                if not dryrun:
                    update_done_status(plan_line,PROXY_DEPLOY_PLAN)
                #if len(started_proxies_list) > 0:
                restop_instances(FAB_USERNAME,started_proxies_file,started_proxies_list,provider,region,proxy_manager,dryrun,LOGFILE)

    fabric.state.output.status = False
    fabric.network.disconnect_all()
    print_log(LOGFILE, "** Deployment of day {} completed **".format(day_TODO))
    if day_TODO == "0":
        print
        print_log(LOGFILE, "Make sure to monitor {}".format(service_config.CANARY_PROXY))
        print_log(LOGFILE, "https://metrics.wandera.net/dashboard/db/canary-proxy?refresh=30s&orgId=3", "WARN")
        print
    print_log(LOGFILE, "*** Available Files ***", "yellow", True)
    print_log(LOGFILE, "\tList of updated proxies to date for release {}: {}".format(rel_num, DONELIST), "cyan", True)
    print_log(LOGFILE, "\tLog file: {}".format(LOGFILE), "cyan", True)
    print
    print_log(LOGFILE, "You can get current version of all proxies by running: {}\n".format(get_proxies_script), "blue", True)

if __name__ == "__main__":

    if len(sys.argv) == 4:
        rel_num = sys.argv[1]
        PROXY_DEPLOY_PLAN = sys.argv[2]
        dryrun = sys.argv[3]
    else:
        print_log(LOGFILE, "Missing Argument! Usage: woopas_amigo.py <release_number> <file> <dryrun>", "ERROR")
        sys.exit(1)

    logging.basicConfig(level="ERROR", format='%(asctime)s %(levelname)s %(message)s')

    main(rel_num, PROXY_DEPLOY_PLAN, dryrun=dryrun)
