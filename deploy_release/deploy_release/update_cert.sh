#!/bin/bash

if [[ $# -ne 1 ]]; then
    echo "DESC: Regenerate certificates for puppet slave (with puppet master update)"
    echo "USAGE: $0 <node_ip>"
    exit 1
fi

ip=$1

echo "Node IP: $ip"

echo -n "Continue? [Y/n]: "
read choice

if [[ $choice != "Y" ]]; then
    exit 1
fi

HOSTNAME=`ssh $ip 'hostname'`
echo "Hostname: $HOSTNAME"
ssh $ip<<EOF
sudo su -
puppet agent --configprint ssldir
puppet agent --configprint ssldir | xargs rm -rvf
EOF

echo ""
echo "+++++++++++++++++++++++++"
echo ""

ssh puppet.snappli.net<<EOF
sudo su -
puppet cert clean $HOSTNAME
EOF

ssh $ip<<EOF
sudo su -
puppet agent -t --noop --tags staff
EOF
