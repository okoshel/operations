import argparse
import logging
import colorer
from os.path import expanduser, realpath, isfile
import os
import sys
from git import Repo
from release_version_loader import getServicesVersions
from ruamel.yaml import YAML
import csv
from shutil import copy2
import interaction
import service_config
from termcolor import colored
import util
import time

yaml=YAML()
yaml.preserve_quotes = True
yaml.explicit_start = True

def preparePuppetGit(puppet_path, branch='master', origin_branch='master'):
    repo = Repo(puppet_path)
    if repo.is_dirty():
        logging.critical("Puppet repository is dirty please clean your changes")
        exit(1)

    print
    logging.info("************************************************")
    logging.info("Working on branch from {}".format(origin_branch))
    repo.heads[origin_branch].checkout()
    logging.debug("Pulling remote %s", repo.remote())
    repo.remote().pull()
    try:
        branch = repo.heads[branch]
    except IndexError:
        logging.info("Creating new branch %s from %s", branch, origin_branch)
        branch = repo.create_head(branch)
    logging.info("Checking out to %s", branch)
    branch.checkout()

    return repo

def writeConfigChange(file_path, user_changes):
    with open(file_path, 'r') as config_file_r:
        config = yaml.load(config_file_r)
        for user_change in user_changes:
            if user_change['applying']:
                if config_file_r.name == user_change['config_file']:
                    config[user_change['config_key']] = user_change['new_value']
    with open(file_path, 'w') as config_file_w:
        yaml.dump(config, config_file_w)

def doVersion(puppet_path, environment, branch='master'):
    base_branch = branch
    for origin_branch in ['master']:
        util.clear_screen()
        if not interaction.confirm("Would you like to update on " + colored("{}? ".format(origin_branch),"yellow")):
            continue
        branch = "{}_{}".format(base_branch, origin_branch)
        repo = preparePuppetGit(puppet_path, branch=branch, origin_branch=origin_branch)

        #load all the config files (their might be some saved in roles...)
        delta_tools_config_path ="{}/hieradata/environments/operations/common.eyaml".format(puppet_path)
        filter_config_path ="{}/hieradata/environments/operations/role/mysql_slave.eyaml".format(puppet_path)
        version_file_path ="{}/hieradata/environments/{}/versions.eyaml".format(puppet_path,environment)
        #tools_file_path ="{}/hieradata/environments/operations/common.eyaml".format(puppet_path)
        config_paths = [delta_tools_config_path, filter_config_path, version_file_path]

        #get list of services with their version number from S3
        release_service_versions = getServicesVersions(tools_path=delta_tools_config_path)
        release_service_versions = sorted(release_service_versions, key=lambda k: k['name'])

        logging.info("Making .bak bakup version files")
        for config_path in config_paths:
            copy2(config_path, config_path+".bak")

        with open(delta_tools_config_path, 'r') as delta_config_file_r,\
            open(filter_config_path, 'r') as filter_config_file_r,\
            open(version_file_path, 'r') as version_file_file_r:

            configs = [[yaml.load(delta_config_file_r), delta_config_file_r],
                       [yaml.load(filter_config_file_r), filter_config_file_r],
                       [yaml.load(version_file_file_r), version_file_file_r]]
            config_changes = []
            services_keys = {}
            for release_service in release_service_versions:
                #create service control list for services that might have different alias per environment. i.e. proxy (non prod) v proxy_enterprise (prod)
                service_search_key = release_service.get('alias', release_service['name'])
                key = None
                for config in configs:
                    config_keys = config[0].keys()
                    service_version_keys = [s for s in config_keys if service_search_key in s and "version" in s]
                    if len(service_version_keys) > 1:
                        #Need to filter harder
                        service_version_keys = [s for s in service_version_keys if "::version" in s]

                    if len(service_version_keys) == 1:
                        if release_service['name'] not in services_keys:
                            services_keys[release_service['name']] = []
                        services_keys[release_service['name']].append(service_version_keys)
                        break

            for release_service in release_service_versions:
                #loop to find the config key and the config file associated
                service_search_key = release_service.get('alias', release_service['name'])
                logging.debug("Checking Service: %s with key %s", release_service['name'], service_search_key)
                key = None
                for config in configs:
                    config_keys = config[0].keys()
                    service_version_keys = [s for s in config_keys if service_search_key in s and "version" in s]
                    if len(service_version_keys) > 1:
                        #Need to filter harder
                        service_version_keys = [s for s in service_version_keys if "::version" in s]

                    if len(service_version_keys) > 1:
                        logging.error("TOO MANY Version Config found for %s", release_service['name'])
                    elif len(service_version_keys) == 1:
                        key = service_version_keys[0]
                        break
                if key is not None:
                    logging.debug("Using config key %s: %s", key, config[0][key])
                    current_version = config[0][key]
                    config_change = {
                        'name': release_service['name'],
                        'config_key': key,
                        'config_file': config[1].name,
                        'old_value': current_version,
                        'new_value': release_service['version'],
                        'tools': release_service.get('tools')
                    }
                    config_changes.append(config_change)
                else:
                    logging.warn("Could not find config key '%s' for service '%s'", service_search_key, release_service['name'])
                    if release_service['name'] in services_keys:
                        if release_service['name'] == "rabbit-enterprise":
                            logging.info("Service %s is already set up with key(s): %s", release_service['name'], str(services_keys[release_service['name']]) + colored(". Production and Staging have different service keys: 'proxy' (Stgn) vs 'proxy_enterprise' (Prod)","yellow"))
                        else:
                            logging.info("Service %s is already set up with key(s): %s", release_service['name'], str(services_keys[release_service['name']]))

                    if interaction.confirm("Would you like to add it? ", key='add_config_key'):
                        new_config_file = interaction.ask_value_with_confirm("Version file: ", default=configs[2][1].name, casesensitive=True)
                        new_alias = release_service['name']
                        for service in service_config.ALL_SERVICES:
                            if service['name'] == release_service['name']:
                                new_alias = service['alias']
                        new_alias = interaction.ask_value_with_confirm("Service puppet alias: ", default=new_alias)
                        config_change = {
                            'name': release_service['name'],
                            'config_key': 'service::{}::version'.format(new_alias),
                            'config_file': new_config_file,
                            'old_value': 'N/A',
                            'new_value': release_service['version'],
                            'tools': release_service.get('tools')
                        }
                        config_changes.append(config_change)

        # We now have the list of the config_key, we check with the user what he wants to change
        user_changes = []
        user_change_plan_file_path = os.path.dirname(os.path.realpath(__file__))+'/cache/config_changes.csv'
        if os.path.isfile(user_change_plan_file_path):
            with open(user_change_plan_file_path, 'r') as user_config_change_file:
                csv_user_reader = csv.DictReader(user_config_change_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                csv_user_changes = []
                for row in csv_user_reader:
                    csv_user_changes.append(row)
            if len(csv_user_changes) > 0:
                #Need to update the user config changes with the correct files
                for csv_user_change in csv_user_changes:
                    csv_user_change['applying'] = {'False':False,'True':True}[csv_user_change['applying']]
                    found = False
                    for config_change in config_changes:
                        if csv_user_change['name'] == config_change['name']:
                            csv_user_change['config_file'] = config_change['config_file']
                            user_changes.append(csv_user_change)
                            found=True
                            break
                    if not found:
                        logging.error("Cannot find config change for user change service %s, removing this user change", csv_user_change['name'])


                logging.warn("Found a version file with the following changes:")
                for user_change in user_changes:
                    if user_change['applying']:
                        if user_change['config_key'] == "service::proxy::version" or user_change['config_key'] == "proxy_enterprise::install::proxy_version":
                            logging.warn("%30s: Changing from %15s to %15s (key: %s)",
                                         user_change['name'], user_change['old_value'],
                                         user_change['new_value'], user_change['config_key'] + colored(" ==> Production and Staging Proxy service tags are different !!!","yellow"))
                        else:
                            logging.info("%30s: Changing from %15s to %15s (key: %s)",
                                     user_change['name'], user_change['old_value'],
                                     user_change['new_value'], user_change['config_key'])
                use_saved_change = interaction.confirm("Use this config change file?", key='use_prep_cache')
                if not use_saved_change:
                    # delete the file
                    user_changes = []
                    os.remove(user_change_plan_file_path)

        if len(user_changes) == 0:
            for config_change in config_changes:
                print "Service: %s" % config_change['name']
                print "\tPuppet Current Config: %s" % config_change['old_value']
                print "\tS3 Release Config    : %s" % config_change['new_value']
                if not config_change.get('tools'):
                    print "\t=> Different Version: %s"% (config_change['old_value'] != config_change['new_value'])
                    choice = interaction.ask_value("Switching to new version?",'y',['yes','Other','Skip'], "keep_version")
                else:
                    print(colored("\t=> INTERNAL TOOLS SERVICE <=","yellow"))
                    choice = interaction.ask_value("Deploy a new version?",'y',['yes','Skip'], "keep_tools_version")
                    if choice == 'y':
                        choice = 'o'
                if choice == 'o':
                    new_version = interaction.ask_value_with_confirm("Version to use, Current %s? " % config_change['old_value'])
                    config_change['new_value'] = new_version
                config_change['applying'] = (choice.lower() == 'y' or choice.lower() == 'o')
                user_changes.append(config_change)

        #Give a summary and save the user changes
        logging.info("-"*30)
        logging.info("SUMMARY:")
        for user_change in user_changes:
            if user_change['applying']:
                logging.info("%30s: Changing from %15s to %15s (key: %s)",
                             user_change['name'], user_change['old_value'], user_change['new_value'], user_change['config_key'])
        if (user_changes) > 0:
            with open(user_change_plan_file_path, 'w') as user_config_change_file:
                mwriter = csv.writer(user_config_change_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                headers = user_changes[0].keys()
                mwriter.writerow(headers)
                for user_change in user_changes:
                    row = [user_change[key] for key in headers]
                    mwriter.writerow(row)
        #Write the changes
        for config_path in config_paths:
            writeConfigChange(config_path, user_changes)

        repo.git.add(u=True)

        if interaction.confirm("Commit and push changes in branch {}?".format(branch), key="push_commit"):
            commit_msg = interaction.ask_value_with_confirm("Enter a commit message: ")
            try:
                repo.git.commit('-m',"\"{}\"".format(commit_msg))
                repo.git.push("origin",branch)
                logging.info(colored("Changes committed and pushed to {} in {}".format(branch, origin_branch), "green"))
                time.sleep(3)
            except:
                logging.error("Failed to push {} in {}".format(branch,origin_branch))
        else:
            logging.error("Changes not committed. You'll need to do it manually\n")

def run():
    parser = argparse.ArgumentParser(description='Set the service version for this release', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-l', '--level', help='Log Level',default='INFO', required=False, choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'])
    parser.add_argument('-e', '--env', help='Which env to do the change', required=True, choices=['staging','integration','production'])
    parser.add_argument('-p', '--puppet', help='Puppet directory', type=str, required=False, default='~/git/puppet')
    parser.add_argument('-b', '--branch', help='Git branch for the change', type=str, required=False, default='production')
    # parser.add_argument('-y', '--yes', help='Remove Interactions', action='store_true', required=False)

    args = parser.parse_args()

    logging.basicConfig(level=args.level, format='%(asctime)s %(levelname)s %(message)s')
    puppet_path = expanduser(args.puppet)
    env = args.env
    logging.info("Preparing environment %s in %s using branch %s", env, puppet_path, args.branch)
    doVersion(puppet_path,env, branch=args.branch)


if __name__ == "__main__":
    run()
