import logging
import sys
import os
import json
from termcolor import colored
from datetime import date, datetime, timedelta
import subprocess
import platform
import requests
from service_config import *
import interaction
import jenkins
import re
import time
import thread
from jira import JIRA
import threading
from select import select

home = os.path.expanduser('~')
CONFIG_FILE = os.path.join(home, ".wandera/woopas.json")

def addPrefix(prefix):
    if len(prefix) > 0:
        for h in logging.root.handlers:
            if not h.formatter._fmt.startswith('['):
                h.formatter._fmt = "[]" + h.formatter._fmt
            prefix_before = h.formatter._fmt.split(']')[0][1:]
            prefix_array = [p for p in prefix_before.split(':') if len(p) > 0]
            prefix_array.append(prefix)
            new_prefix = "["+":".join(prefix_array)+']'

            h.formatter._fmt = new_prefix+h.formatter._fmt[len(prefix_before)+2:]

def popPrefix():
    for h in logging.root.handlers:
        if h.formatter._fmt.startswith('['):
            prefix_before = h.formatter._fmt.split(']')[0][1:]
            prefix_array = [p for p in prefix_before.split(':') if len(p) > 0]
            prefix_array.pop()
            if len(prefix_array) == 0:
                new_prefix = ""
            else:
                new_prefix = "[" + ":".join(prefix_array) + ']'

            h.formatter._fmt = new_prefix + h.formatter._fmt[len(prefix_before) + 2:]

def print_log(log_file_name, msg, log_level="INFO", nodate= False):
    if nodate:
        date_str = ""
    else:
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        date_str = "{} : ".format(now)
    with open(log_file_name, "a+") as log_file:
        log_file.write("{}{}\n".format(date_str, msg))
    log_level_color = { "WARN" : "yellow" , "ERROR" : "red" }
    if log_level in log_level_color:
        print(colored("{}{} : {}".format(date_str, log_level,msg) , log_level_color[log_level]))
    else:
        try:
            print("{}{} : ".format(date_str, "INFO") + colored("{}".format(msg), log_level))
        except:
            print "{}{} : {}".format(date_str, "INFO", msg)

def clear_screen():
    clear = subprocess.Popen( "cls" if platform.system() == "Windows" else "clear", shell=True)
    clear.communicate()

def init_ssh_user():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        fab_user = config_json["woopas"]["ssh_username"]
    except Exception as e:
        print(colored("ERROR: Failed to load ssh user from {}\n{}".format(CONFIG_FILE,e) , "red"))
        sys.exit(1)

    return fab_user

def get_proxy_credentails():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        proxy_user = config_json["woopas"]["proxy_ping_username"]
        proxy_pwd = config_json["woopas"]["proxy_ping_password"]
    except Exception as e:
        print(colored("ERROR: Failed to load proxy credentials from {}\n{}".format(CONFIG_FILE,e) , "red"))
        sys.exit(1)

    return proxy_user, proxy_pwd

def get_jira_credentails():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        jira_user = config_json["woopas"]["jira_user"]
        jira_sword = config_json["woopas"]["jira_sword"]
    except Exception as e:
        print(colored("ERROR: Failed to load proxy credentials from {}\n{}".format(CONFIG_FILE,e) , "red"))
        sys.exit(1)

    return jira_user, jira_sword

def get_jenkins_credentails(site='ciops'):
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        ciops_user = config_json["woopas"]["{}_user".format(site)]
        ciops_token = config_json["woopas"]["{}_token".format(site)]
    except Exception as e:
        print(colored("ERROR: Failed to load jenkins credentials from {}\n{}".format(CONFIG_FILE,e) , "red"))
        sys.exit(1)

    return ciops_user, ciops_token

def slack_post(message):
    data = {'text' : message}
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    try:
        slack_response = requests.post(slack_url,data=json.dumps(data),headers=headers)
    except Exception as e:
        print(colored("ERROR: Failed to post to slack url {}\n{}".format(slack_url,e) , "red"))
        slack_response = "Failed to post to slack: {}".format(e.message)

    return slack_response

def run_healthcheck(environ,app=False,ip=None,show_errors=True):
    failed_urls = []
    if app:
        if ip is None:
            logging.error('MISSING HOST IP to run APP Healthcheck')
            return
        urls_list = app_healthchecks_urls
    else:
        urls_list = healthchecks_urls

    hc_status = {}
    if environ in urls_list:
        env_url_list = urls_list[environ]
    elif 'default' in urls_list:
        env_url_list = urls_list['default']
    else:
        return False

    for url in env_url_list:
        resp = None
        if app:
            url = "http://{}{}".format(ip,url)
        try:
            resp = requests.get(url)
            if resp.status_code != 200:
                logging.error("{} : {}".format(url,resp.status_code))
                fail = {'url' : url}
                try:
                    fail['resp'] = resp.json()
                    fail['json'] = True
                except:
                    fail['resp'] = resp.text
                    fail['json'] = False
                failed_urls.append(fail)
            hc_status[url] = resp.status_code
        except Exception as e:
            logging.error("Failed to get '{}' : {}".format(url, e))
            fail = {'url' : url, 'json' : False, 'resp' : e.message}
            failed_urls.append(fail)
            try:
                hc_status[url] = resp.status_code
            except:
                hc_status[url] = 'FAILED'

    if len(failed_urls) > 0:
        logging.info('')
        #if interaction.confirm("Would you like to see full response for failed healthchecks?"):
        if show_errors:
            for fail in failed_urls:
                logging.error("Full response from {}".format(fail['url']))
                if fail['json']:
                    logging.info(json.dumps(fail['resp'],indent=4))
                else:
                    logging.info(fail['resp'])
                logging.info("")

    logging.info('')
    logging.info('STATUS OF HEALTHCHECK (full response for failed HC above)')
    logging.info('')
    slack_text = "STATUS OF {} HEALTHCHECK".format(environ.upper())
    for url in hc_status:
        if hc_status[url] == 200:
            logging.info("[ OK ] {} : {}".format(url,hc_status[url]))
            slack_text += "\n[ OK ] {} : {}".format(url,hc_status[url])
        else:
            logging.error("{} : {}".format(url,hc_status[url]))
            slack_text += "\n[ FAILED ] {} : {}".format(url,hc_status[url])
    slack_post(slack_text)

    if len(failed_urls) > 0:
        return False
    else:
        return True

def next_weekday(d, weekday):
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0: # Target day already happened this week
        days_ahead += 7
    return d + timedelta(days_ahead)

def run_jenkins(url='http://ci.snappli.net/',job='acceptance-tests-stgn-radar',params={}):
    site = re.sub(r'.*//','',url).split('.')[0]
    username, token = get_jenkins_credentails(site=site)
    server = jenkins.Jenkins(url, username=username, password=token)

    logging.info("Running job '{}' in {}".format(job, url))

    build = server.build_job(job, params)
    time.sleep(5)
    job_num = server.get_job_info(job)['lastBuild']['number']
    while server.get_job_info(job)['lastCompletedBuild']['number'] != job_num:
        print ".",
        sys.stdout.flush()
        time.sleep(5)
    print
    if server.get_job_info(job)['lastSuccessfulBuild']['number'] == job_num:
        logging.info("[ OK ] {} JOB SUCCEEDED".format(server.get_job_info(job)['lastSuccessfulBuild']['url']))
        slack_post("{} SUCCESS".format(server.get_job_info(job)['lastSuccessfulBuild']['url']))
        return True

    elif server.get_job_info(job)['lastFailedBuild']['number'] == job_num:
        logging.error("[ ERROR ] {} JOB FAILED".format(server.get_job_info(job)['lastFailedBuild']['url']))
        slack_post("[ ERROR ] {} FAILED".format(server.get_job_info(job)['lastFailedBuild']['url']))

    elif server.get_job_info(job)['lastUnstableBuild']['number'] == job_num:
        logging.error("[ ERROR ] {} JOB UNSTABLE".format(server.get_job_info(job)['lastUnstableBuild']['url']))
        slack_post("[ ERROR ] {} UNSTABLE".format(server.get_job_info(job)['lastUnstableBuild']['url']))

    elif server.get_job_info(job)['lastUnsuccessfulBuild']['number'] == job_num:
        logging.error("[ ERROR ] {} JOB UNSUCCESSFUL".format(server.get_job_info(job)['lastUnsuccessfulBuild']['url']))
        slack_post("[ ERROR ] {} UNSUCCESSFUL".format(server.get_job_info(job)['lastUnsuccessfulBuild']['url']))

    else:
        logging.error("[ ERROR ] Could not find status of job {}".format(job_num))
        slack_post("[ ERROR ] Could not find status of {}/job/{}/{} SUCCESS".format(url,job,job_num))
    return False

def get_release_ops_ticket(rel_num, jira=None, jira_creds=None):
    if jira is None:
        if jira_creds is None:
            jira_creds = get_jira_credentails()
        #Inputs:
        # - JIRA ticket
        jira = JIRA("https://snappli.atlassian.net", basic_auth=jira_creds)

    jql_search = "project=OPS AND labels=Release"
    rl_tickets = jira.search_issues(jql_search,maxResults=200)
    for ticket in rl_tickets:
        if re.search(r'{}'.format(rel_num),ticket.fields.summary):
            return ticket
    return None

def get_release_tasks(rel_num, jira=None,jira_creds=None):
    if jira is None:
        if jira_creds is None:
            jira_creds = get_jira_credentails()
        #Inputs:
        # - JIRA ticket
        jira = JIRA("https://snappli.atlassian.net", basic_auth=jira_creds)

    jql_search = "issuetype=Release and fixVersion=R{}".format(rel_num)
    rl_tickets = jira.search_issues(jql_search,maxResults=100)
    before_tickets = []
    after_tickets = []
    ff_tickets = []
    for jticket in rl_tickets:
        #print "{},{},{}".format(jticket.id,jticket.fields.fixVersions,jticket.fields.customfield_13918)
        try:
            task_type = jticket.fields.customfield_13918.value
        except:
            #print "{} has not customfield_13918".format(jticket.key)
            continue
        if task_type.lower() == 'after':
            after_tickets.append(jticket)
        elif task_type.lower() == 'before':
            before_tickets.append(jticket)
        elif task_type.lower() == 'feature flag':
            ff_tickets.append(jticket)

    return {'before' : before_tickets,
            'after' : after_tickets,
            'ff' : ff_tickets
            }

def get_ticket_status(ticket, jira=None, jira_creds=None):
    if jira is None:
        if jira_creds is None:
            jira_creds = get_jira_credentails()
        jira = JIRA("https://snappli.atlassian.net", basic_auth=jira_creds)

    jira_ticket = jira.issue(ticket)

    return jira_ticket.fields.status.name

def check_tickets_valid_status(tickets,environ, jira=None, jira_creds=None):
    logging.info("Checking status of release tasks")
    if jira is None:
        if jira_creds is None:
            jira_creds = get_jira_credentails()
        jira = JIRA("https://snappli.atlassian.net", basic_auth=jira_creds)

    valid_status = "production"
    if environ in ['stgn','staging']:
        valid_status += "|staging"
    valid_status_regex = re.compile(valid_status, re.IGNORECASE)

    tickets_ready = True
    for ticket in tickets:
        t_status = get_ticket_status(ticket.key, jira=jira)
        if not valid_status_regex.search(t_status):
            tickets_ready = False
            logging.error("{} is '{}' ==> not a valid status".format(ticket.key, t_status))
        else:
            logging.info("[ OK ] {} is {}".format(ticket.key, t_status))

    return tickets_ready

def input_thread(L):
    derp = raw_input()
    L.append(derp)

def wait_tasks_done(tickets,environ,type,jira):
    L = []
    th = WaitInput(L)
    th.start()
    while not check_tickets_valid_status(tickets,environ,jira=jira):
        timer = 1
        while True:
            if timer == 300:
                timer = 1
                print
                break
            if timer % 5 == 0:
                print '.',
                sys.stdout.flush()
            if L:
                print
                logging.info(colored("User confimed {} tasks done. Checking status".format(type),"cyan"))
                if check_tickets_valid_status(tickets,environ,jira=jira):
                    break
                else:
                    logging.warning("{} tasks not completed yet. Will keep waiting. Alternatively, press enter when finished".format(type.capitalize()))
                    L = []
                    th = WaitInput(L)
                    th.start()
                    timer = 1
            time.sleep(1)
            timer += 1
    if not L:
        th.join()

class WaitInput(threading.Thread):
    def __init__(self,L):
        threading.Thread.__init__(self)
        self.shutdown_flag = threading.Event()
        self.L = L

    def run(self):
        while not self.shutdown_flag.is_set():
            timeout = 2
            rlist, _, _ = select([sys.stdin], [], [], timeout)
            if rlist:
                s = sys.stdin.readline()
                self.L.append(s)
                return

    def join(self, timeout=None):
        self.shutdown_flag.set()
        super(WaitInput, self).join(timeout)
