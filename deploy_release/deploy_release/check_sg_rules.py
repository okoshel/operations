#!/usr/local/bin/python

import libcloud_api
from libcloud_api import *
import os
import sys
from datetime import date
from pprint import pprint
import argparse
import subprocess
import platform
from interaction import *

current_dir = os.getcwd()
today = date.today()
DATE = today.isoformat()
sg_info_dir = os.path.join(current_dir, "security_groups_info")
known_filename = os.path.join(sg_info_dir, "known_ips_{}".format(DATE))
unknown_filename = os.path.join(sg_info_dir, "unknown_ips_{}".format(DATE))
open_filename = os.path.join(sg_info_dir, "open_ports_{}".format(DATE))

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('--regions', dest = "regions_TODO", nargs = '+', help = "List of regions. For all regions: all. Default all")
    parser.add_argument('--group-name', dest = "group_name", nargs = '+', help = "List of group names to check")
    parser.add_argument('--group-id', dest = "group_id", nargs = '+', help = "List of group IDs to check")
    return parser.parse_args()

if __name__ == "__main__":
    clear_screen()
    if not os.path.isdir(sg_info_dir):
        os.makedirs(sg_info_dir)
    #(options, args) = parse_options(sys.argv[1:])
    options = parse_options(sys.argv[1:])

    if options.regions_TODO:
        regions_TODO = options.regions_TODO
    else:
        regions_TODO = get_regions("EC2")

    drivers = init_ec2_driver(regions_TODO)

    groups_list = []
    if options.group_name is not None:
        groups_list.extend(options.group_name)
        known_filename = "{}_{}.csv".format(known_filename,'-'.join(options.group_name))
        unknown_filename = "{}_{}.csv".format(unknown_filename,'-'.join(options.group_name))
        open_filename = "{}_{}.csv".format(open_filename,'-'.join(options.group_name))

    if options.group_id is not None:
        groups_list.extend(options.group_id)
        if options.group_name is None:
            known_filename = "{}_{}.csv".format(known_filename,'-'.join(options.group_id))
            unknown_filename = "{}_{}.csv".format(unknown_filename,'-'.join(options.group_id))
            open_filename = "{}_{}.csv".format(open_filename,'-'.join(options.group_id))

    if options.group_id is None and options.group_name is None:
        for filename in [known_filename,unknown_filename,open_filename]:
            filename = "{}.csv".format(filename)

    print "\nGetting all instances"
    all_instances = get_all_instances()
    print "\nGetting security groups"
    all_SG = get_security_groups(drivers)
    if len(groups_list) > 0:
        sg_list = {}
        for sg_driver_name in all_SG:
            sg_list[sg_driver_name] = []
            for sg in all_SG[sg_driver_name]:
                if sg.name in groups_list or sg.id in groups_list:
                    sg_list[sg_driver_name].append(sg)
                    continue
    else:
        sg_list = all_SG

    unknown_ips = []
    known_ips = []
    open_ports = []
    for sg_driver_name in sorted(sg_list.keys()):
        for sg in sg_list[sg_driver_name]:
            #print json.dumps(vars(sg), indent=4)
            for rule in sg.ingress_rules:
                for ip in rule["cidr_ips"]:
                    if re.search('0.0.0.0', ip):
                        try:
                            sg_info = {'group_name' : sg.name, 'group_id' : sg.id, 'region' : sg_driver_name, 'ip':ip, 'protocol':rule["protocol"], 'from_port':rule["from_port"], 'to_port':rule["to_port"], 'description' : rule.description}
                        except:
                            sg_info = {'group_name' : sg.name, 'group_id' : sg.id, 'region' : sg_driver_name, 'ip':ip, 'protocol':rule["protocol"], 'from_port':rule["from_port"], 'to_port':rule["to_port"], 'description' : '' }
                        open_ports.append(sg_info)
                        continue
                    ip_found = False
                    for prov_driver in all_instances:
                        for instance in all_instances[prov_driver]:
                            try:
                                if ip.split('/')[0] == instance.public_ip:
                                    ip_found = True
                                    known_ips.append({'group_name' : sg.name, 'group_id' : sg.id, 'region' : sg_driver_name, 'ip' : ip, 'protocol':rule["protocol"], 'from_port':rule["from_port"], 'to_port':rule["to_port"], 'instance' : instance})
                            except:
                                pass
                            try:
                                if ip.split('/')[0] == instance.private_ip:
                                    ip_found = True
                                    known_ips.append({'group_name' : sg.name, 'group_id' : sg.id, 'region' : sg_driver_name, 'ip' : ip, 'protocol':rule["protocol"], 'from_port':rule["from_port"], 'to_port':rule["to_port"], 'instance' : instance})
                            except:
                                pass
                    if not ip_found:
                        try:
                            unknown_ips.append({'group_name' : sg.name, 'group_id' : sg.id, 'region' : sg_driver_name, 'ip':ip, 'protocol':rule["protocol"], 'from_port':rule["from_port"], 'to_port':rule["to_port"], 'description' : rule.description})
                        except:
                            unknown_ips.append({'group_name' : sg.name, 'group_id' : sg.id, 'region' : sg_driver_name, 'ip':ip, 'protocol':rule["protocol"], 'from_port':rule["from_port"], 'to_port':rule["to_port"], 'description' : '' })

    print(colored("Known ips","yellow"))
    with open(known_filename, "w") as known_file:
        known_file.write("group_name,group_id,ip,instance,region\n")
        for ip in known_ips:
            print "{},{},{},{},{},{},{},{}".format(ip['group_name'], ip['group_id'], ip['region'], ip['ip'], ip['protocol'], ip['from_port'], ip['to_port'], ip['instance'].name)
            known_file.write("{},{},{},{},{},{},{},{}\n".format(ip['group_name'], ip['group_id'], ip['region'], ip['ip'], ip['protocol'], ip['from_port'], ip['to_port'], ip['instance'].name))

    print(colored("Open Ports","magenta"))
    with open(open_filename, "w") as open_file:
        open_file.write("group_name,group_id,ip,port,region,description\n")
        for ip in open_ports:
            print "{group_name},{group_id},{region},{ip},{protocol},{from_port},{to_port},{description}".format(**ip)
            open_file.write("{group_name},{group_id},{region},{protocol},{ip},{from_port},{to_port},{description}\n".format(**ip))

    print(colored("\n\nUNKNOWN IPs","red"))
    with open(unknown_filename, "w") as unknown_file:
        unknown_file.write("group_name,group_id,region,ip,port,description\n")
        for ip in unknown_ips:
            print "{group_name},{group_id},{region},{ip},{protocol},{from_port},{to_port},{description}".format(**ip)
            unknown_file.write("{group_name},{group_id},{region},{ip},{protocol},{from_port},{to_port},{description}\n".format(**ip))

    print "\nData Available in:"
    print "- Known IPs: {}".format(known_filename)
    print "- Open Ports: {}".format(open_filename)
    print "- Unknown IPs: {}\n".format(unknown_filename)
