#!/usr/local/bin/python

#####################################################################
# deploy_release.py
# Author: Arturo Noha
# Version: 3.2
#------------------------------------------------------------------
# USAGE: ./deploy_release.3.2.py --rel <release_num> -u <ssh_username> --env <environment> --relservices <release_services_file> --dryrun <True/False> [options]
#	* Available environments:
#	- stgn: staging
#	- intg: integration
#	- prod-proxy: production proxies (via woopas). Requires proxy deployment plan file
#	- prod-core: app and portal in prodution ==> Set stack weight to 0/1. Option to only ser APP or PORTAL + Detaches instances from Load Balancers
#	- prod-services: non app backend services (siem, connector, delta, filterservice...). Requires release services file
#	- core-full: all production services (app, portal, other services). Requires release services file
#	* Options:
#	--allservices: file with the inforamtion of all available services. Default: ./release_services/all_services.info
#	--nodes: File with the list of nodes to deploy to if not all default nodes required
#	--proxy-plan: File with the daily proxy deployment plan per region. Default: ./release_Proxy_nodes/proxy_deployment_plan.csv
#####################################################################

import os
import sys

from pprint import pprint
import argparse
import re
import subprocess
from subprocess import Popen, PIPE

import fabric
from fabric.context_managers import settings
from fabric.api import env
import imp
from datetime import date, datetime
from time import sleep
import platform
from termcolor import colored
import logging
import prepare_version_file

import service_config
import libcloud_api
from util import CONFIG_FILE
import woopas_amigo_demo
from service_config import *
from fabfile import *
import interaction

env.use_ssh_config = True

today = date.today()
DATE = today.isoformat()
today_regex = re.compile("{},(.*),TODO".format(DATE))
nodes_todo_regex = re.compile("[^,]*,TODO")

current_dir = os.getcwd()
log_file = os.path.join(current_dir,"logs")
WOOPAS_PLAN = os.path.join(current_dir, "release_Proxy_nodes", "THREAD_1_PLAN.csv")
update_versions_script = os.path.join(current_dir, "prepare_version_file.sh")
home = os.path.expanduser('~')

def clear_screen():
    clear = Popen( "cls" if platform.system() == "Windows" else "clear", shell=True)
    clear.communicate()

def update_node_status(node,TODOLIST):
    temp_file_name = "{}.tmp".format(TODOLIST)
    with open(TODOLIST, "r")  as todo_file, open(temp_file_name,"w") as temp_file:
        for line in todo_file:
            update_regex_start = re.compile("^{},TODO".format(node))
            replace_str_start = "{},DONE".format(node)
            line = update_regex_start.sub(replace_str_start, line)

            update_regex_middle = re.compile(",{},TODO".format(node))
            replace_str_middle = ",{},DONE".format(node)
            line = update_regex_middle.sub(replace_str_middle, line)

            temp_file.write(line)
    os.remove(TODOLIST)
    os.rename(temp_file_name, TODOLIST)

def get_input(message = None):
    if message is None:
        display_message = "Would you wish to continue? [Y/n]: "
    else:
        display_message = message + " [Y/n]: "

    option = raw_input(display_message)
    while option not in valid_options:
        option = raw_input("Please enter a valid option [Y/n]: ")

    return option

def stopped_instance_options(name = ""):
    options = ['1','2','3']
    option = raw_input("WARNING: {} is stopped. What would you wish todo?\n".format(name) +
                    "\t1. Start and deploy\n" +
                    "\t2. Deploy without starting\n" +
                    "\t3. Skip\n" +
                    "\nYour choice: ")

    while option not in options:
        print "$ Please select a valid option: {}".format(options)

    return option

def deploy(myhost, user, name, dryrun=True, tags=None):
    if not dryrun:
        try:
            with settings(host_string = myhost, user = user):
                return deploy_server(name,tags)
        except Exception as e:
            print_log(log_file, "Could not deploy on {}".format(myhost), "ERROR")
            print_log(log_file, "Exception: {}".format(e.message))
            print_log(log_file, "{}".format(e))
            return -1
    else:
        return 0

def deploy_canary(myhost, user, name, dryrun=True, tags=None):
    for server in service_config.canary_servers:
        deploy(myhost, user, name, dryrun, tags)

def deploy_core(user,stack_type,dryrun=True):
    """
    user: ssh username
    stack_type: CORE / RADAR
    dryrun : True/False
    """
    stack_type = stack_type.upper()
    stack_type_regex = re.compile(stack_type)

    if not dryrun:
        print(colored("\nHave you sent the release notification email to ops-notify@wandera.com and releases@wandera.com?\n", "yellow"))
        if interaction.ask_value("Please enter 'continue' to start deployment ", choices=['continue','exit']) == 'e':
            sys.exit(0)

    lb_provider = "EC2"
    lb_region = "eu-west-1"
    lb_driver = libcloud_api.get_LB_driver(lb_region)


    ec2_drivers = libcloud_api.init_ec2_driver([lb_region])
    ec2_instances = libcloud_api.get_all_instances(ec2_drivers)

    stack_out_weight = 0
    stack_in_weight = 255
    stack_startup_weights = [10,50,150,stack_in_weight]
    stack_removal_weights = [stack_out_weight,50,150]

    if not dryrun:
        print "\n"
        if get_input("Would you like to set Stacks to working weight {}".format(stack_in_weight)) == "Y":
            for stack in sorted(stacks_info.keys()):
                if get_input("Will set weight of Stack {} to {}. Proceed?".format(stack, stack_in_weight)) != "Y":
                    if get_input("Do you wish to continue with the other stacks?") != "Y":
                        sys.exit(0)
                    else:
                        continue
                else:
                    stack_update = libcloud_api.update_route53_weight(stack, stack_in_weight, stack_type=stack_type, dryrun=dryrun)
                    if stack_update == -1:
                        print_log(log_file, "-- User skipped update of all record sets in stack {} to weight {}. No wait performed".format(stack, stack_in_weight),"WARN")
                    elif stack_update:
                        print_log(log_file, "-- Waiting for Stack {} to settle at weight {}".format(stack, stack_in_weight))
                        sleep(stack_phase_sleep)
                    else:
                        if get_input("ERROR: Failed to set weight to {}. Continue?".format(stack_in_weight)) != "Y":
                            sys.exit("\n** Deployment aborted by user **\n")
    else:
        print "\n"
        print_log(log_file, "Will set all Stacks to working weight: {}".format(stack_in_weight))

    for stack in sorted(stacks_info.keys()):
        print "\n"
        print_log(log_file, "* Starting deployment to Stack {}".format(stack))

        for my_stack_type in stacks_info[stack].keys():
            if not stack_type_regex.search(my_stack_type.upper()):
                continue

            if not dryrun:
                if get_input("Will phase out {} on Stack {}. Proceed?".format(stack_type, stack)) != "Y":
                    if get_input("Do you wish to continue with the other stacks?") != "Y":
                        sys.exit(0)
                    else:
                        continue
            else:
                print_log(log_file, "Will phase out {} on Stack {}.".format(stack_type, stack))

            for weight in sorted(stack_removal_weights, key=int, reverse=True):
                if weight == stack_in_weight:
                    continue
                stack_update = libcloud_api.update_route53_weight(stack, weight, stack_type=stack_type, dryrun=dryrun)
                if stack_update == -1:
                    print_log(log_file, "-- User skipped update of all record sets in stack {} to weight {}. No wait performed".format(stack, weight),"WARN")
                elif stack_update:
                    if not dryrun:
                        print_log(log_file, "-- Waiting for {} in Stack {} to settle at weight {}".format(stack_type, stack, weight))
                        sleep(stack_phase_sleep)
                    else:
                        print_log(log_file, "-- Will wait for {} in Stack {} to settle at weight {}\n".format(stack_type, stack, weight))
                        sleep(2)
                else:
                    if get_input("ERROR: Failed to set weight to {}. Continue?".format(weight)) != "Y":
                        sys.exit("\n** Deployment aborted by user **\n")

            for lb_instance in sorted(stacks_info[stack][my_stack_type].keys()):
                if not dryrun:
                    print "\n-- Starting deployment to instances behind {}".format(lb_instance)
                    for node in stacks_info[stack][my_stack_type][lb_instance]["instances"]:
                        print "\t- {} ({})".format(node, stacks_info[stack][my_stack_type][lb_instance]["instances"][node])

                    if get_input("\n-- Would you like to deploy to those instances?".format(lb_instance)) != "Y":
                        print_log(log_file, "Skipping deployment of {} insntances as per user request".format(lb_instance))
                        continue

                for lb in sorted(stacks_info[stack][my_stack_type][lb_instance]["lbs"]):
                    if not dryrun:
                        if get_input("\n. Continue? ({} will be detached from {})".format(lb_instance, lb)) == "n":
                            sys.exit("\n ** Deployment aborted by user **")

                        print "\n"
                        print_log(log_file, "* Detaching {} from {}".format(lb_instance, lb))
                        if not libcloud_api.detach_LB_member(lb_instance, lb, lb_driver):
                            print "\n"
                            print_log(log_file, "Failed to detach {} from {}. Please check manually.".format(lb_instance, lb), "ERROR")
                            if get_input("Would you wish to continue with deployment?") != "Y":
                                sys.exit(1)
                        else:
                            print_log(log_file, "* {} successfully detached from {}\nWaiting for it to fully take effect".format(lb_instance, lb))
                            sleep(15)
                    else:
                        print_log(log_file, "Will detach {} from {}".format(lb_instance, lb))

                for node in sorted(stacks_info[stack][my_stack_type][lb_instance]["instances"].keys()):
                    for driver_name in ec2_instances:
                        for ec2_instance in ec2_instances[driver_name]:
                            if ec2_instance.name == node:
                                node_ip = ec2_instance.public_ip
                    inst_type = stacks_info[stack][my_stack_type][lb_instance]["instances"][node]

                    if not dryrun:
                        if get_input("\n-- Would you like to deploy to {} {} ({})".format(inst_type,node, node_ip)) != "Y":
                            print_log(log_file, "Skipping deployment of {} as per user request".format(node))
                            continue
                        print "\n"
                        print_log(log_file, "* Starting deployment to {} ({})".format(node, node_ip))
                        deploy_return = deploy(node_ip, user, node, dryrun)
                        if deploy_return != 0:
                            print_log(log_file, "Deployment to {} ({}) failed".format(node, node_ip), "ERROR")
                            if get_input("\nWould you wish to continue deploying the rest of nodes?".format(node)) != "Y":
                                print_log(log_file, "Deployment aborted by user")
                                sys.exit(1)
                        if get_input("\n-- Continue with the rest of nodes?") != "Y":
                            print
                            print_log(log_file, "** Deployment aborted by user **")
                            sys.exit(1)
                    else:
                        print_log(log_file, "Will deploy to {} {} ({})".format(stacks_info[stack][my_stack_type][lb_instance]["instances"][node], node, node_ip))
                        '''deploy_return = deploy(node_ip, user, node, dryrun)
                        if deploy_return != 0:
                            if get_input("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes?".format(node)) != "Y":
                                sys.exit(1)'''

                for lb in sorted(stacks_info[stack][my_stack_type][lb_instance]["lbs"]):
                    if not dryrun:
                        print "\n"
                        print_log(log_file, "* Re-attaching {} to {}".format(lb_instance, lb))
                        if libcloud_api.attach_LB_member(lb_instance, lb, lb_driver) == False:
                            print_log(log_file, "There was a problem reattching {}".format(lb_instance), "ERROR")
                            if get_input("\nContinue deployment?") != "Y":
                                print_log(log_file, "Deployment aborted by user")
                                sys.exit(1)
                        else:
                            print_log(log_file, "* {} successfully re-attached to {}\nWaiting for it to fully take effect".format(lb_instance, lb))
                            sleep(15)
                    else:
                        print_log(log_file, "Will re-attach {} to {}".format(lb_instance, lb))

            print "\n"
            print_log(log_file, "- Reactivating Stack {}".format(stack))
            for weight in sorted(stack_startup_weights, key=int):
                stack_update = libcloud_api.update_route53_weight(stack, weight, stack_type=stack_type, dryrun=dryrun)
                if stack_update == -1:
                    print_log(log_file, "-- User skipped update of all record sets in stack {} to weight {}. No wait performed".format(stack, weight),"WARN")
                elif stack_update:
                    if not dryrun:
                        print_log(log_file, "-- Waiting for {} in Stack {} to settle at weight {}".format(stack_type, stack, weight))
                        sleep(stack_phase_sleep)
                    else:
                        print_log(log_file, "-- Will wait for {} in Stack {} to settle at weight {}\n".format(stack_type, stack, weight))
                        sleep(2)
                else:
                    print_log(log_file, "Failed to set weight to 1", "ERROR")
                    if get_input("\nContinue?") != "Y":
                        print_log(log_file, "Deployment aborted by user")
                        sys.exit(1)

            print "\n"
            print_log(log_file, "** Deployment to Stack {} completed".format(stack))
            if not dryrun:
                if get_input("Continue?") != "Y":
                    sys.exit(1)

def deploy_endpoint(endpoint, user,dryrun=True):
    lb_provider = "EC2"
    lb_region = "eu-west-1"
    lb_driver = libcloud_api.get_LB_driver(lb_region)

    ep_service = endpoint_mapping[endpoint]["service_name"]
    ep_service_regex = re.compile(ep_service)
    server_type = None

    for service in service_config.ALL_SERVICES:
        if ep_service_regex.search(service['name']) or (service.get('alias') and ep_service_regex.search(service['alias'])):
            server_type = service['node']

    if server_type is None:
        print(colored("server type not found for endpoint {}".format(endpoint), "red"))
        return False
    server_type_regex = re.compile(server_type)

    ec2_drivers = libcloud_api.init_ec2_driver([lb_region])
    ec2_instances = libcloud_api.get_all_instances(ec2_drivers)

    stack_out_weight = 0
    stack_in_weight = 255
    stack_startup_weights = [10,50,150,stack_in_weight]
    stack_removal_weights = [stack_out_weight,50,150]

    if not dryrun:
        print "\n"
        if get_input("Would you like to set endpoint {} to working weight {} for all Stacks".format(endpoint, stack_in_weight)) == "Y":
            for stack in sorted(stacks_info.keys()):
                if get_input("Will set weight of {} on Stack {} to {}. Proceed?".format(endpoint, stack, stack_in_weight)) != "Y":
                    if get_input("Do you wish to continue with the other stacks?") != "Y":
                        sys.exit(0)
                    else:
                        continue
                else:
                    if libcloud_api.update_endpoint_weight(endpoint, stack, stack_in_weight):
                        print_log(log_file, "-- Waiting for {} on Stack {} to settle at weight {}".format(endpoint, stack, stack_in_weight))
                        sleep(15)
                    else:
                        if get_input("ERROR: Failed to set weight to {} for {} on Stack {}. Continue?".format(stack_in_weight, endpoint, stack)) != "Y":
                            sys.exit("\n** Deployment aborted by user **\n")
    else:
        print "\n"
        print_log(log_file, "Will set all weigth of {} to working weight {} on all stacks".format(endpoint, stack_in_weight))

    for stack in sorted(stacks_info.keys()):
        if not dryrun:
            print "\n"
            print_log(log_file, "* Starting deployment to {} on Stack {}".format(endpoint, stack))

            if get_input("Will phase out {} on Stack {}. Proceed?".format(endpoint, stack)) != "Y":
                if get_input("Do you wish to continue with the other stacks?") != "Y":
                    sys.exit(0)
                else:
                    continue

            for weight in sorted(stack_removal_weights, key=int, reverse=True):
                if weight == stack_in_weight:
                    continue
                stack_update = libcloud_api.update_endpoint_weight(endpoint, stack, weight)
                if stack_update == -1:
                    print_log(log_file, "-- User skipped update of {} in stack {} to weight {}. No wait performed".format(endpoint, stack, weight),"WARN")
                elif stack_update:
                    print_log(log_file, "-- Waiting for {} in Stack {} to settle at weight {}".format(endpoint, stack, weight))
                    sleep(stack_phase_sleep)
                else:
                    if get_input("ERROR: Failed to set weight of {} to {} on {}. Continue?".format(endpoint, weight, stack)) != "Y":
                        sys.exit("\n** Deployment aborted by user **\n")

            for my_stack_type in sorted(stacks_info[stack].keys()):
                for lb_instance in sorted(stacks_info[stack][my_stack_type].keys()):
                    if get_input("\n-- Starting deployment to instances behind {}".format(lb_instance)) != "Y":
                        print_log(log_file, "Skipping deployment of {} insntances as per user request".format(lb_instance))
                        continue

                    for node in stacks_info[stack][my_stack_type][lb_instance]["instances"]:
                        if server_type_regex.match(node) and node != lb_instance:
                            for driver_name in ec2_instances:
                                for ec2_instance in ec2_instances[driver_name]:
                                    if ec2_instance.name == node:
                                        node_ip = ec2_instance.public_ip
                            if get_input("\n-- Would you like to deploy to {} ({})".format(node, node_ip)) != "Y":
                                print_log(log_file, "Skipping deployment of {} as per user request".format(node))
                                continue
                            print "\n"
                            print_log(log_file, "* Starting deployment to {} ({})".format(node, node_ip))
                            process_name = endpoint_mapping[endpoint]["puppet_tag"]
                            deploy_return = deploy(node_ip, user, node, dryrun, tags=process_name)
                            if deploy_return != 0:
                                if get_input("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes?".format(node)) != "Y":
                                    sys.exit(1)
                            if get_input("\n-- Continue with the rest of nodes?") != "Y":
                                sys.exit("\n** Deployment aborted by user **\n")
                print "\n"
                print_log(log_file, "- Reactivating {} on Stack {}".format(endpoint, stack))
                for weight in sorted(stack_startup_weights, key=int):
                    stack_update = libcloud_api.update_endpoint_weight(endpoint, stack, weight)
                    if stack_update == -1:
                        print_log(log_file, "-- User skipped update of {} in stack {} to weight {}. No wait performed".format(endpoint, stack, weight),"WARN")
                    elif stack_update:
                        print_log(log_file, "-- Waiting for {} in Stack {} to settle at weight {}".format(endpoint, stack, weight))
                        sleep(stack_phase_sleep)
                    else:
                        if get_input("ERROR: Failed to set weight of {} to {} on {}. Continue?".format(endpoint, weight, stack)) != "Y":
                            sys.exit("\n** Deployment aborted by user **\n")

                print "\n"
                print_log(log_file, "** Deployment of {} to Stack {} completed".format(endpoint, stack))
                if get_input("Continue?") != "Y":
                    sys.exit(1)
        # DRYRUN
        else:
            print "\n"
            print_log(log_file, "* Starting deployment to Stack {}".format(stack, dryrun))
            for weight in sorted(stack_removal_weights, key=int, reverse=True):
                if weight == stack_in_weight:
                    continue
                print "\n"
                print_log(log_file, "Will set {} on stack {} to {} weight".format(endpoint, stack,weight))
                print_log(log_file, "- will wait {} min for {} on Stack {} to settle at weight {}".format(stack_phase_sleep/60, endpoint, stack, weight))
                sleep(2)
            for my_stack_type in sorted(stacks_info[stack].keys()):
                for lb_instance in sorted(stacks_info[stack][my_stack_type].keys()):
                    print "\n"
                    print_log(log_file, "* Starting deployment to {}".format(lb_instance))
                    print "\n"

                    for node in sorted(stacks_info[stack][my_stack_type][lb_instance]["instances"]):
                        if server_type_regex.match(node):
                            for driver_name in ec2_instances:
                                for ec2_instance in ec2_instances[driver_name]:
                                    if ec2_instance.name == node:
                                        node_ip = ec2_instance.public_ip
                            print_log(log_file, "Will deploy to {} ({})".format(node, node_ip))
                            process_name = endpoint_mapping[endpoint]["puppet_tag"]
                            deploy_return = deploy(node_ip, user, node, dryrun, tags=process_name)
                            if deploy_return != 0:
                                if get_input("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes?".format(node)) != "Y":
                                    sys.exit(1)

                for weight in sorted(stack_startup_weights, key=int):
                    print "\n"
                    print_log(log_file, "Will set weight of {} to {} for Stack {}".format(endpoint, weight, stack))
                    print_log(log_file, "Will sleep {} min".format(stack_phase_sleep/60))
                    sleep(2)

def display_options(user, env, proxy_plan, dryrun, nodes_todo=None, relservices=None, allservices=None, endpoint=None):
    clear_screen()
    #if clear != '':
    nodes = []
    if nodes_todo is not None:
        with open(nodes_todo, "r") as nodes_list:
            for line in nodes_list:
                if nodes_todo_regex.match(line):
                    nodes.append(line.rstrip().split(',')[0])
        if len(nodes) == 0:
            sys.exit("ERROR: no nodes TODO found for today ({})".format(DATE))


    print "-------------------------------------------------"
    print "Running deployment with following options:"
    print "\t- user: {}".format(user)
    print "\t- environment: {}".format(env)

    if endpoint is not None:
        print "\t- endpoint: {} ({})".format(endpoint, endpoint_mapping[endpoint]["service_name"])

    if relservices is not None:
        release_services_list = []
        with open(relservices, "r") as rel_services_file:
            for service in rel_services_file:
                release_services_list.append(service.rstrip())
        print "\t- Relsease services file: {}".format(relservices)
        for serv in release_services_list:
            print "\t\t. {}".format(serv)

    if proxy_plan is not None:
        print "\t- Proxy deployment plan file: {}".format(proxy_plan)

    if allservices is not None:
        print "\t- All services info: {}".format(allservices)

    if len(nodes) > 0:
        print "\t- List of target nodes: {}".format(nodes_todo, nodes)
        for node in nodes:
            print "\t\t. {}".format(node)

    print "\t- dryrun: {}".format(dryrun)

    return get_input()

def print_log(log_file_name, msg, log_level="INFO"):
    now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    with open(log_file_name, "a+") as log_file:
        log_file.write("{} : {}\n".format(now, msg))
    log_level_color = { "WARN" : "yellow" , "ERROR" : "red" }
    if log_level in log_level_color:
        print(colored("{}: {} : {}".format(now,log_level,msg) , log_level_color[log_level]))
    else:
        print "{}: {} : {}".format(now, log_level, msg)

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('--versions', dest = "versions_file")

    return parser.parse_args()

def menu(option=None):
    env_opt_mapping = {
        1 : "versions", 2 : "stgn", 3 : "core", 4 : "prod-services", 5 : "radar", 6 : "prod-proxy", 7 : "tools", 8 : "prod-endpoint"
    }
    versions_env_mapping = {
        1 : "staging", 2 : "production"
    }
    update_versions = False
    options = {}
    proxy_plan = None

    if option is None:
        clear_screen()
        print("\nSelect an option:\n" +
                "\n\t1. Prepare versions" +
                "\n\t2. Deploy to staging" +
                "\n\t3. Deploy Core to production" +
                "\n\t4. Deploy Services to production" +
                "\n\t5. Deploy Portal to production" +
                "\n\t6. Deploy Proxy to production" +
                "\n\t7. Deploy Internal Tools" +
                "\n\t8. Deploy To a single Core endpoint" +
                "\n\t0. Cancel")

        option = int(raw_input("\nChoice: "))
    while option not in range(0,9):
        option = int(raw_input("Please enter a valid option (0-8): "))
    if option == 0:
        sys.exit(0)
    environ = env_opt_mapping[option]

    endpoint = None
    puppetdir = None
    if option == 8:
        endpoint = raw_input("\n- Endpoint (i.e. eu.wandera.com)\nReference: {}: ".format(endpoints_url))
        while endpoint not in endpoint_mapping:
            endpoint = raw_input("\nPlease enter a valid endpoint (i.e. eu.wandera.com): ")

    if option in [1,5,6,8]:
        puppetdir = raw_input("\n- Puppet directory: (default: ~/git/puppet)")
        if not puppetdir:
            puppetdir = os.path.expanduser("~/git/puppet")
        while not os.path.isdir(puppetdir):
            puppetdir = raw_input("{} is not a directory. Please enter a valid path for your puppet directory: ".format(puppetdir))

    options["puppetdir"] = puppetdir
    options["endpoint"] = endpoint

    release_num = raw_input("\n- Release version number: ")
    while release_num == "":
        release_num = raw_input("\nPlease enter a valid version. Release version number: ")
    options["release_num"] = release_num

    if option == 1:
        update_versions = True
        print("\nSelect an environment:\n" +
            "\n\t1. Staging" +
            "\n\t2. Production")
        env_opt = int(raw_input("\nChoice: "))
        while env_opt not in range(1,3):
            env_opt = int(raw_input("Please enter a valid option (1-3): "))
        environ = versions_env_mapping[env_opt]
        puppet_branch = raw_input("Puppet branch for version changes? (default: {}_{})".format(release_num,environ))
        if not puppet_branch:
            puppet_branch = "{}_{}".format(release_num,environ)
        options["branch"] = puppet_branch
    else:
        dryrun = raw_input("\n- Dryrun (true/false): ")
        while dryrun.lower() not in [ "true", "false" ]:
            dryrun = raw_input("\n- Please enter a valid option. Dryrun (true/false): ")
        if dryrun.lower() == "true":
            dryrun = True
        else:
            dryrun = False
        options["dryrun"] = dryrun

        if option == 6:
            proxy_plan = raw_input("\n- Proxy plan (default: {}): ".format(WOOPAS_PLAN))
            if proxy_plan == "":
                proxy_plan = WOOPAS_PLAN
            while not os.path.isfile(proxy_plan):
                proxy_plan = raw_input("\n{} is not a directory.  Proxy plan (default: {}): ".format(proxy_plan, WOOPAS_PLAN))
                if proxy_plan == "":
                    proxy_plan = WOOPAS_PLAN

    options["proxy_plan"] = proxy_plan
    options["update_versions"] = update_versions
    options["environ"] = environ

    return options

def init_ssh_user():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        fab_user = config_json["woopas"]["ssh_username"]
    except Exception as e:
        print(colored("ERROR: Failed to load ssh user from {}\n".format(CONFIG_FILE) , "red"))
        sys.exit(1)

    return fab_user

def health_check(environ,release_num,before):
    if before:
        check_stage = "before"
    else:
        check_stage = "after"

    if environ in health_check_url or environ in acceptance_tests_url:
        print(colored("Makes sure to run required tests and checks:",attrs=['reverse']))
        try:
            print(colored("\n\t- Health check {} deployment: ".format(check_stage), "magenta") + colored("{}".format(health_check_url[environ]),"cyan",attrs=['underline']))
        except:
            pass
        try:
            print(colored("\n\t- Accpeptance Tests: ","magenta") + colored("{}".format(acceptance_tests_url[environ]),"cyan",attrs=['underline']))
            print "\t\t. Click on \"Build with parameters\""
            print "\t\t. Enter the release name. i.e. release-{} and click build.".format(release_num)
            print "\t\t. Check the result of build history, if green proceed to next step. If the build fails investigate why and escalate to the QA team on the release channel if needed"
            if not before:
                try:
                    print(colored("\n\t- Health check after tests: ", "magenta") + colored("{}".format(health_check_url[environ]),"cyan",attrs=['underline']))
                except:
                    pass
        except:
            pass

        if not before and environ == 'stgn':
            now = datetime.now()
            time_now = now.time()
            if time_now >= datetime.strptime(stgn_deploy_cutoff_time, "%I:%M%p").time():
                print(colored("\n\tIt is after {}. Please run a device enrolment test\n".format(stgn_deploy_cutoff_time), "yellow"))

    if before:
        print
        return interaction.ask_value("Please enter 'continue' to start deployment ", choices=['continue','exit'])

def main(option=None):
    if not os.path.isfile(CONFIG_FILE):
        print(colored("\nERROR: Configuration file {} not found.\n".format(CONFIG_FILE)))
        sys.exit(1)

    fab_user = init_ssh_user()
    options = menu(option)
    environ = options["environ"]

    if options["update_versions"]:
        prepare_version_file.doVersion(options["puppetdir"], environment=options["environ"], branch=options["branch"])
        #print "Changes committed and pushed"
    else:
        release_num = options["release_num"]
        dryrun = options["dryrun"]
        endpoint = options["endpoint"]

        global log_file
        if not dryrun:
            log_file = os.path.join(current_dir, "logs", "RL_{}_{}.log".format(release_num,environ))
        else:
            log_file = os.path.join(current_dir, "logs", "RL_{}_{}_dryrun.log".format(release_num,environ))

        #if display_options(fab_user, environ, options.nodes_TODO_file, options.proxy_plan, dryrun) != "Y":
        if display_options(fab_user, environ, options["proxy_plan"], dryrun, endpoint = endpoint) != "Y":
            sys.exit(1)

        if environ == "stgn":
            if health_check(environ, release_num, before=True) == 'e':
                sys.exit(1)

        providers, provider_aliases = libcloud_api.get_providers()

        if "nodes_TODO_file" in options:
            TODOLIST = options["nodes_TODO_file"]
        else:
            TODOLIST = None

        if environ not in [ "prod-core", "core" , "radar" , "prod-proxy", "prod-endpoint" ]:
            # Getting the instances
            print_log(log_file, "Getting list of instances")
            all_instances = libcloud_api.get_all_instances()
            #print "[DEBUG] number of instances: {}".format(len(all_instances))

        # Git Pull on puppet

        if not dryrun:
            print_log(log_file, "Running git pull in puppet master ({}:{})".format(puppet_master, puppet_master_dir))
            with settings(host_string = puppet_master, user = fab_user):
                git_pull(puppet_master_dir)
        else:
            print_log(log_file, "Would run git pull in puppet master ({}:{})".format(puppet_master, puppet_master_dir))

        app_done = False

        # read through the file of services to deploy and deploy to required instances
        if environ == "prod-proxy":
            if "proxy_plan" in options:
                PROXY_PLAN = options["proxy_plan"]
            else:
                PROXY_PLAN = WOOPAS_PLAN
                if get_input("Proxy default deployment plan: {}. Continue?".format(PROXY_PLAN)) != "Y":
                    PROXY_PLAN = raw_input("Please enter the file with the woopas plan: ")
                while not os.path.isfile(PROXY_PLAN):
                    PROXY_PLAN = raw_input("\"{}\" not found. Please enter a valid file: ".format(PROXY_PLAN))

            woopas_amigo_demo.main(release_num,PROXY_PLAN,puppetdir=options["puppetdir"],dryrun=dryrun)
        elif environ in ["core","radar"]:
            deploy_core(fab_user,environ,dryrun)
            print "\nLOG file: {}\n".format(log_file)
        elif environ == "prod-endpoint":
            deploy_endpoint(endpoint,fab_user,dryrun)
        else:
            if environ == "core-full":
                deploy_core(fab_user,"core",dryrun)

            if environ in env_service_types:
                my_service_types = env_service_types[environ]
            else:
                all_service_types = []
                for service in service_config.ALL_SERVICES:
                    if not service.get('deprecated') and not service.get('tools') and service['node'] not in all_service_types:
                        all_service_types.append(service['node'])
                my_service_types = all_service_types

            for service_type in sorted(my_service_types):
                if ( service_type == "app" or service_type == "web" ) and ( environ == "core-full" or environ == "prod-services" ):
                    print "\n"
                    print_log(log_file, "Skipping deployment of Core \"{}\" services".format(service_type),"WARN")
                    continue

                if service_type in woopas_service_types and environ in live_environments and environ != "proxy":
                    print "\n"
                    print_log(log_file, "Skipping {} deployment. Incompatible type: woopas service not run as --env proxy **".format(service_type),"WARN")
                    print "\n"
                    sleep(3)
                    continue

                print "\nDeploying to {} servers".format(service_type)
                print "- services running:"
                for service in service_config.ALL_SERVICES:
                    if not service.get('deprecated') and service['node'] == service_type:
                        print "\t- {}".format(service['name'])
                if get_input("\nWould you like to deploy to {} servers?".format(service_type)) != "Y":
                    continue

                instances_TODO = {}
                if service_type == "bea" and environ == "stgn":
                    print_log(log_file, "Switching server type bea to app in staging", "WARN")
                    service_type = "app"

                if service_type == "app" and app_done:
                    print_log(log_file, "deployment already done on app servers. Skipping")
                    continue

                if (environ in live_environments and service_type in [ "fis", "fid" ]) or (environ == 'tools'):
                    service_regex = re.compile("^{}-[0-9]*-cops".format(service_type), re.VERBOSE)
                elif service_type == "vid":
                    service_regex = re.compile("^{}-[0-9]*-prxy.*wandera\.{}".format(service_type, env_extensions[environ.split('-')[0]]), re.VERBOSE)
                elif environ in live_environments:
                    service_regex = re.compile("^{}-[0-9]*-core".format(service_type), re.VERBOSE)
                elif service_type == "prx" and environ == "stgn":
                    service_regex = re.compile("(^{}-[0-9]*-{}|\.node\..*wandera\.{})".format(service_type, environ, env_extensions['stgn']), re.VERBOSE)
                else:
                    service_regex = re.compile("^{}-[0-9]*-{}".format(service_type, environ), re.VERBOSE)
                if TODOLIST is None:
                    for driver in all_instances:
                        for instance in all_instances[driver]:
                            if service_regex.search(instance.name):
                                name = instance.name
                                instances_TODO[name] = { "instance" : instance , "provider" : driver }
                else:
                    with open(TODOLIST, "r") as todo_nodes:
                        for line in todo_nodes:
                            line = line.rstrip()
                            node = line.rsplit(',')[0]
                            node_regex = re.compile(node)
                            if nodes_todo_regex.search(line) and service_regex.search(node):
                                print_log(log_file, "Looking for {} instance for {}".format(service_type, node))
                                found = False
                                for driver in all_instances:
                                    for instance in all_instances[driver]:
                                        #print_log(log_file, "{}".format(instance.name))
                                        if node_regex.search(instance.name):
                                            instances_TODO[node] = { "instance" : instance , "provider" : driver }
                                            found = True
                                            break
                                    if found:
                                        break
                                if not found:
                                    print_log(log_file, "{} not found".format(node), "ERROR")

                if len(instances_TODO) == 0:
                    print "\n"
                    print_log(log_file, "*** No nodes found for service type \"{}\". Skipping... ***".format(service_type))
                    print "\n"
                    sleep(3)
                else:
                    print "\n"
                    print_log(log_file, "** Will deploy on:")
                    for name in instances_TODO:
                        #print_log(log_file, "\t- {} ({})".format(name, vars(instances_TODO[name])))
                        print_log(log_file, "\t- {} on {} ({})".format(name, instances_TODO[name]["provider"], instances_TODO[name]["instance"].state))

                    #cont = get_input()

                    if get_input() == "Y":
                        for name in instances_TODO:
                            instance = instances_TODO[name]["instance"]
                            state = instance.state
                            host = instance.public_ip

                            print "\n"
                            print_log(log_file, "* Deploying to {} - {} (currently {})".format(name, host, state))
                            print "\n"
                            if not dryrun:
                                if get_input("Proceed with deployment on {} ({})".format(name, host)) != "Y":
                                    continue
                                if state != "running":
                                    option = stopped_instance_options(name)
                                    if option == '3':
                                        print_log(log_file, "Skipping deployment of {}".format(name))
                                        continue
                                    else:
                                        if option == '1':
                                            #driver = instances_TODO[name]["driver"]
                                            print_log(log_file, "Starting {}".format(name))
                                            #sleep(2)
                                            if not dryrun:
                                                libcloud_api.start_instance(name)
                                                print_log(log_file, "Waiting for {} to be fully up".format(name))
                                                sleep(120)
                                        deploy_return = deploy(host,fab_user,name,dryrun)
                                        if deploy_return == 3:
                                            continue
                                        elif deploy_return != 0:
                                            if get_input("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes?".format(name)) == "Y":
                                                continue
                                            else:
                                                sys.exit(1)
                                        elif service_type == "app":
                                                app_done = True
                                        if not dryrun:
                                            stop_option = interaction.ask_value("Would you like to re-stop the instance? ", choices=['Yes','No','exit'])
                                            if stop_option == 'y':
                                                print_log(log_file, "Stopping {}".format(name))
                                                libcloud_api.stop_instance(name)
                                                sleep(5)
                                            elif stop_option == 'e':
                                                sys.exit(0)
                                        print_log(log_file, "Deployment to {} completed".format(name))

                                else:
                                    if deploy(host,fab_user,name,dryrun) != 0:
                                        if get_input("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes?".format(name)) == "Y":
                                            continue
                                        else:
                                            sys.exit(1)
                                    else:
                                        if service_type == "app":
                                            app_done = True

                                        if TODOLIST is not None:
                                            update_node_status(name, TODOLIST)

                                        print_log(log_file, "Deployment to {} completed".format(name))
                                        if get_input("\nDeployment of {} completed. Continue with rest of nodes?".format(name)) != "Y":
                                            print_log(log_file, "Deployment aborted by user")
                                            sys.exit("BYE")
                if environ == "core-full":
                    deploy_core(fab_user,"radar",dryrun)
            print(colored("\nLOG file: {}\n".format(log_file),"green"))

            health_check(environ, release_num, before=False)

    fabric.state.output.status = False
    fabric.network.disconnect_all()


#############  MAIN  #############

if __name__ == "__main__":
    logging.basicConfig(level='INFO', format='%(levelname)s: %(message)s')
    #(options, args) = parse_options(sys.argv[1:])
    main()
