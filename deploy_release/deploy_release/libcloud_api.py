#####################################################################
# libcloud_api.py
# Author: Arturo Noha
# Version: 1.0
#-----------------------------------------------------------------
# Functions for the access and management of cloud instances.
# Available funtions:
# * get_providers(): return the list of providers and thier aliases
# * get_regions(provider): return an array with all the regions for a given provider. Retunrs an empty array if failed or not applicable
#
# * get_rackspace_credentials(): Return username and access key for each Rackspace region.
#   - credentials must be stored in environment variables named: RACKSPACE_<region>_username and RACKSPACE_<region>_accesskey.
# * get_aws_credentials(): Return AWS Access Key Id and Access Secret Key.
#   - Credentials must be stores in environment variables named: AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
#
# * init_softlayer_driver(): returns the driver to SoftLayer cloud
#   - returns a dictionary with a single keypair value: key = SL / value = the driver
# * init_openstack_driver(): returns the driver to the Deutsche Telekom cloud
#   - returns a dictionary with a single keypair value: key = DT / value = the driver
# * init_rackspace_driver(regions): returns the drivers for the required Rackspace regions
#   - takes an array with the regions for which to get the drivers
#   - returns a dictionary where each entry is the driver for a region, the key being a string with the format RACKSPACE_<region>. i.e. RACKSPACE_lon
# * init_ec2_driver(regions)
#   - takes an array with the regions for which to get the drivers
#   - returns a dictionary where each entry is the driver for a region, the key being a string with the format AWS_<region>. i.e. EC2_eu-west-1
# * init_all_drivers(): get the drivers for all available providers based on initialisers above
# -->> All driver initialisers return an empty dictionary if fail to retrieve any drivers
#
# * get_all_instances(): get all instances from all providers.
#   - Returns a dictionary whith key = driver_name and value = array of all instances for the given driver
# * get_instance(name,mydrivers=None): get the raw details of a given instance.
#   - Takes the instance name as first argument
#   - Takes a specific driver to search. If none given, it will search through all providers for the instance
# * display_instance(drivers=None,instance_to_show=None): display key info (name, state, id, Public IP) for required instnaces
#   - Arguments (optional):
#       1. dictionary of drivers (key: driver name, i.e. RACKSPACE_lon, AWS_eu-west-1, SL, DT). In none provided, will browse through all providers
#       2. name of the instance to show. If none provided, will display all insances for the relevant dirvers
#
# * stop_instance(name,mydrivers={}): stop the instance(s) passed as first argument (mandatory)
# * start_instance(name,mydrivers={}): start the instance(s) passed as first argument (mandatory)
#   - MANDATORY Argument for stop/start_instance:
#       - an instance or and aray of instance to stop
#   - Optional Argument for stop/start_instance:
#       - dictionary of drivers in which to search for the instance. Key = driver_name, Value = driver_name
#       - if no specific driver(s) provided, will look for the instance in all providers
#####################################################################

import os
import sys
import datetime
import json
import re
import boto3
import logging
import fabric
from fabric.context_managers import settings
from fabric.operations import sudo, local
from fabric.context_managers import hide, show

import libcloud

from libcloud.compute.types import Provider as computeProvider
from libcloud.compute.providers import get_driver as get_compute_driver
from libcloud.compute.drivers import ec2, rackspace
from libcloud.compute.drivers.openstack import *
from libcloud.common.types import InvalidCredsError

from libcloud.loadbalancer.types import Provider as lbProvider
from libcloud.loadbalancer.providers import get_driver as get_lb_driver

from libcloud.dns.providers import get_driver as get_dns_driver
from libcloud.dns.types import Provider as dnsProvider

from wandera_woopas.database import FileDatabase
from termcolor import colored

import statuscake_api
from time import sleep
import interaction

import util

#### GLOBAL VARIABLES ####
route53_hosted_zone = "wandera.com"

#providers = [ 'EC2', 'RACKSPACE', 'SOFTLAYER', 'OPENSTACK' ]
providers = [ 'EC2', 'RACKSPACE', 'SOFTLAYER', 'OPENSTACK' ]
provider_aliases = { 'EC2' : ['aws','AWS','ec2'],
                    'RACKSPACE' : ['rackspace','rs','RS'],
                    'SOFTLAYER' : ['softlayer','sl','SL'],
                    'OPENSTACK' : ['dt','DT']
            }
aws_regions = ec2.EC2NodeDriver.list_regions()
rs_regions = rackspace.ENDPOINT_ARGS_MAP.keys()
#rackspace_cred = { "lon": { "username" : "arturonohauk", "accesskey" : "51e74c3edf2a486997a181cc278fd3a5" } }

providers_credentials = {}
rackspace_credentials = {}
#softlayer_cred = { "username" : "arturonoha" , "accesskey" : "69b0bf01eaff947bdea96a3c5e02966ade1d7dd5d0c1c17988d5241297cde4f5" }
#DT_cred = { "username" : "14651301 OTC00000000001000000675" , "accesskey" : "w3g4pyLokueXdgd0ROPLIlehyQPSBL7vy5cQp99V" }

proxy_regex = re.compile(".*\.node\..*\.wandera\.com")
ip_format = "^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
ip_regex = re.compile("{}$".format(ip_format))
ip_range_format = "{}/[0-9]{{1,2}}$".format(ip_format)
ip_range_regex = re.compile(ip_range_format)

#### FUNCTIONS ####

# Utils
def info(message):
    timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print "{} [INFO] {}".format(timestamp, message)

def get_config_data():
    with open(util.CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()
    try:
        config_json = json.loads(config)
    except Exception as e:
        sys.exit("ERROR: Failed to load config from {}\n".format(util.CONFIG_FILE))

    return config_json

class AttributeDict(dict):
    __getattribute__ = dict.__getitem__
    __setattr__ = dict.__setitem__

# Initialise accesses
def get_providers():
    return providers, provider_aliases

def get_credentials():
    if not os.path.isfile(util.CONFIG_FILE):
        sys.exit("\nERROR: Configuration file {} not found\n".format(util.CONFIG_FILE))

    with open(util.CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        providers_info = config_json["woopas"]
    except:
        sys.exit("ERROR: Invalid configuration file format ({})".format(util.CONFIG_FILE))

    #print "[DEBUG] providers_info from config file:\n{}".format(providers_info)
    if "ec2_user_id" in providers_info and "ec2_secret" in providers_info:
        AWSKEY = providers_info["ec2_user_id"]
        AWSSECKEY = providers_info["ec2_secret"]
        providers_credentials["EC2"] = { "username" : AWSKEY, "accesskey" : AWSSECKEY }
    else:
        sys.exit("\nERROR: Credentials not found for AWS: ec2_user_id / ec2_secret\n")

    for region in rs_regions:
        if region not in rackspace_credentials:
            username_var = "rackspace_user_id"
            accesskey_var = "rackspace_secret"

            if username_var in providers_info and accesskey_var in providers_info:
                username = providers_info[username_var]
                accesskey = providers_info[accesskey_var]
                rackspace_credentials.update( { region : { "username" : username, "accesskey" : accesskey } } )
            '''else:
                print "WARN: Credentials not found for RACKSPACE {}: {} / {}".format(region, username_var, accesskey_var)
                continue'''
    providers_credentials["RACKSPACE"] = rackspace_credentials

    if "softlayer_user_id" in providers_info and "softlayer_secret" in providers_info:
        SOFTLAYER_username = providers_info["softlayer_user_id"]
        SOFTLAYER_accesskey = providers_info["softlayer_secret"]
        softlayer_cred = { "username" : SOFTLAYER_username, "accesskey" : SOFTLAYER_accesskey }
        providers_credentials["SOFTLAYER"] = softlayer_cred
    else:
        sys.exit("\nERROR: Credentials not found for SOFLAYER: softlayer_user_id / softlayer_secret\n")

    DT_username = providers_info["dt_username"]
    DT_accesskey = providers_info["dt_accesskey"]
    DT_project = providers_info["dt_project_name"]
    DT_domain = providers_info["dt_user_domain_name"]
    DT_cred = { "username" : DT_username, "accesskey" : DT_accesskey, "project" : DT_project, "domain" : DT_domain }
    providers_credentials["OPENSTACK"] = DT_cred

    return providers_credentials

def init_all_drivers():
    drivers = {}

    rs_drivers = init_rackspace_driver()
    if len(rs_drivers) > 0:
        drivers.update(rs_drivers)

    aws_drivers = init_ec2_driver()
    if len(aws_drivers) > 0:
        drivers.update(aws_drivers)

    sl_drivers = init_softlayer_driver()
    if len(sl_drivers) > 0:
        drivers.update(sl_drivers)

    dt_drivers = init_openstack_driver()
    if len(dt_drivers) > 0:
        drivers.update(dt_drivers)

    return drivers

def init_softlayer_driver(regions=None):
    logging.info("Initialising SoftLayer drivers")
    cls = get_compute_driver(computeProvider.SOFTLAYER)
    username = get_credentials()["SOFTLAYER"]["username"]
    accesskey = get_credentials()["SOFTLAYER"]["accesskey"]
    driver = cls(username, accesskey)

    if driver is None:
        print "ERROR: Could not get driver from Softlayer"
        return {}
    else:
        return { "SOFTLAYER" : driver }
    #pprint(driver.list_locations())
    #print "------------------------------------"
    #pprint(driver.list_nodes())

def init_openstack_driver(regions=None):
    username = get_credentials()["OPENSTACK"]["username"]
    accesskey = get_credentials()["OPENSTACK"]["accesskey"]
    dt_project = get_credentials()["OPENSTACK"]["project"]
    dt_domain = get_credentials()["OPENSTACK"]["domain"]

    logging.info("Initialising Open Telekom Cloud drivers")
    cls = get_compute_driver(computeProvider.OPENSTACK)
    #ex_force_auth_url = 'http://192.168.1.101:5000/v3/auth/tokens',
    driver = cls(
                    username,
                    accesskey,
                    ex_domain_name=dt_domain,
                    ex_force_service_region=dt_project,
                    ex_force_auth_url = 'https://iam.eu-de.otc.t-systems.com/v3/auth/tokens',
                    ex_force_auth_version = '3.x_password',
                    ex_tenant_name = 'eu-de')

    if driver is None:
        print "ERROR: Could not get driver from DT Cloud"
        return {}
    else:
        return { "OPENSTACK" : driver }

def get_dt_floating_ips(driver=None):
    if driver is None:
        driver = init_openstack_driver()["OPENSTACK"]

    return driver.ex_list_floating_ips()

def get_dt_instance_ip(node_id,driver=None,floating_ips=None):
    if floating_ips is None:
        floating_ips = get_dt_floating_ips(driver)

    ip = next(fip.ip_address for fip in floating_ips if fip.node_id == node_id)
    return ip

def init_rackspace_driver(regions=None):
    logging.info("Initialising Rackspace drivers")
    rs_drivers = {}

    get_credentials()
    if regions is None:
        credentials_TODO = rackspace_credentials
    else:
        credentials_TODO = {}
        for reg in regions:
            if reg in rackspace_credentials:
                credentials_TODO[reg] = rackspace_credentials[reg]

    for reg in credentials_TODO:
        #print "[DEBUG] reg: {} ({})".format(reg,regions[reg])
        username = rackspace_credentials[reg]['username']
        accesskey = rackspace_credentials[reg]['accesskey']
        #print "[DEBUG] Creating rackspace dirver for {} with key {} in Rackspace {}".format(username, accesskey, reg)
        cls = get_compute_driver(computeProvider.RACKSPACE)
        driver = cls(username, accesskey, region = reg)
        if driver is None:
            print "ERROR: Could not get driver for {} from Rackspace {}".format(username, reg)
            #continue
        else:
            driver_name = "RACKSPACE_{}".format(reg)
            #print "[DEBUG] addin driver {}".format(driver_name)
            rs_drivers[driver_name] = driver
    return rs_drivers

def init_ec2_driver(regions=None):
    aws_drivers = {}
    #aws_regions = get_regions('EC2')
    logging.info("Initialising AWS drivers")
    #print "[DEBUG] init_ec2_drivers: aws_regions:\n{}".format(aws_regions)
    if regions is None:
        regions = aws_regions

    for region in regions:
        #print "[DEBUG] region = {}".format(region)
        '''if region not in aws_regions:
            sys.exit("ERROR: region {} is not available".format(region))'''

        #print "[DEBUG] adding driver for region {}".format(region)
        #access_key, secret_key = get_aws_credentials()
        access_key = get_credentials()['EC2']['username']
        secret_key = get_credentials()['EC2']['accesskey']
        cls = get_compute_driver(computeProvider.EC2)
        driver = cls(access_key, secret_key, region=region)
        driver_name = "EC2_{}".format(region)
        aws_drivers[driver_name] = driver

    return aws_drivers

# Get instances information

def _get_instances(driver,csv=False):
    try:
        nodes = driver.list_nodes()
    except Exception as e:
        if not csv:
            logging.debug("Failed to get instances from %s: %s", driver, e)
        raise

    if type(driver) == libcloud.compute.drivers.openstack.OpenStack_1_1_NodeDriver:
        floating_ips = get_dt_floating_ips(driver)
        for node in nodes:
            ip = next(fip.ip_address for fip in floating_ips if fip.node_id == node.id)
            node.__setattr__('public_ip',ip)
            try:
                node.__setattr__('private_ip',node.private_ips[0])
            except:
                node.__setattr__('private_ip','N/A')
            try:
                node.__delattr__('private_ips')
            except:
                pass
            instance_name = re.sub(r'(-eu)?-de', '.de', node.name)
            instance_name = re.sub(r'-wandera-com', '.wandera.com', instance_name)
            instance_name = re.sub(r'-node', '.node', instance_name)
            node.name = instance_name
    else:
        for node in nodes:
            for pub_ip in node.public_ips:
                if ip_regex.search(pub_ip):
                    node.__setattr__('public_ip',pub_ip)
            try:
                node.__getattribute__('public_ip')
            except:
                node.__setattr__('public_ip','N/A')
            try:
                node.__setattr__('private_ip',node.private_ips[0])
            except:
                node.__setattr__('private_ip','N/A')
            try:
                node.__delattr__('private_ips')
                node.__delattr__('public_ips')
            except:
                pass
    return nodes

def check_cloud_access():
    logging.info("Checking access to cloud providers")
    failed_access = {}

    drivers = init_all_drivers()
    for driver_name in drivers:
        try:
            drivers[driver_name].list_nodes()
        except InvalidCredsError:
            print "{} --> Invalid credentials".format(driver_name)
            if 'InvalidCredsError' not in failed_access:
                failed_access['InvalidCredsError'] = []
            failed_access['InvalidCredsError'].append(driver_name)
        except Exception as e:
            if e.message not in failed_access:
                failed_access[e.message] = []
            failed_access[e.message].append(driver_name)
            print "{} : {}".format(driver_name, e)

    return failed_access

def get_all_instances(drivers=None):
    all_instances = {}
    if drivers == None:
        drivers = init_all_drivers()

    # Get instances from providers implemented via libcloud
    for driver_name in drivers:
        try:
            #print "INFO: Getting all instances from: {}".format(driver_name)
            instances = _get_instances(drivers[driver_name])
            all_instances[driver_name] = (instances)
            #print "INFO: GOT Instances from: {}".format(driver_name)
        except InvalidCredsError as ea:
            logging.error("Failed to Authenticate to {}".format(driver_name))
        except Exception as e:
            logging.warning("Could not get instances list from %s. Exception: %s", driver_name, e.message)

    return all_instances

def get_instance(name,mydrivers=None, silent=False):
    if mydrivers is None:
        mydrivers = init_all_drivers()

    #print "silent = {}".format(silent)
    for driver_name in mydrivers:
        instances = []
        if not silent:
            print "Looking for {} in {}".format(name,driver_name)
        driver = mydrivers[driver_name]
        #print driver
        try:
            instances = _get_instances(driver)
        except InvalidCredsError as ea:
            logging.error("Failed to Authenticate to {}".format(driver_name))
        except Exception as e:
            print "ERROR: Could not get instances list from {}\n{}".format(driver_name,e)

        for instance in instances:
            if instance.name == name:
                return instance, driver

    return None,None


def get_regions(provider):
    if provider not in providers:
        sys.exit("ERROR: Provider {} not available".format(provider))

    #print ">> Getting regions for {}\n".format(provider)
    if provider == "EC2":
        #return ec2.EC2NodeDriver.list_regions()
        return aws_regions
    elif provider == "RACKSPACE":
        return rs_regions
    else:
        return []

def display_instance(drivers=None,instance_to_show=None,instances=None,csv=False):
    if instances is not None:
        for driver_name in instances:
            for instance in instances[driver_name]:
                name = instance.name
                state = instance.state
                ips = instance.public_ip
                int_ips = instance.private_ip

                if csv:
                    print_string = "{},{},{},{},{},{}".format(driver_name,name,state,instance.id,ips,int_ips)
                else:
                    print_string = "[{}] Name:{} , State: {} ,Id: {} ,Public IP: {}, Private IP: {}".format(driver_name,name,state,instance.id,ips,int_ips)

                if instance_to_show is None:
                    print "{}".format(print_string)
                elif instance_to_show == name:
                    print "\n{}\n".format(print_string)
                    return
    else:
        if drivers is None:
            drivers = init_all_drivers()

        for driver_name in drivers:
            try:
                driver = drivers[driver_name]
                try:
                    instances = _get_instances(driver,csv)
                except InvalidCredsError as ea:
                    logging.error("Failed to Authenticate to {}".format(driver_name))
                    continue
                except Exception as e:
                    logging.error("{} : Exception: {}".format(driver_name,e))
                    continue
                if len(instances) > 0:
                    if instance_to_show is None and not csv:
                        print "Instances from {}".format(driver_name)
                    #print "[DEBUG] Number of instances : {}".format(len(instances))
                    for instance in instances:
                        name = instance.name
                        state = instance.state
                        ips = instance.public_ip
                        int_ips = instance.private_ip

                        if csv:
                            print_string = "{},{},{},{},{},{}".format(driver_name,name,state,instance.id,ips,int_ips)
                        else:
                            print_string = "[{}] Name:{} , State: {} ,Id: {} ,Public IP: {}, Private IP: {}".format(driver_name,name,state,instance.id,ips,int_ips)

                        if instance_to_show is None:
                            print "{}".format(print_string)
                        elif instance_to_show == name:
                            print "\n{}\n".format(print_string)
                            return
                elif not csv:
                    print "No instances found from {}".format(driver_name)
            except Exception as e:
                if not csv:
                    print "ERROR: Could not get instances from {}".format(driver_name)
                    print "(Exception) {}".format(e)
                else:
                    pass

# Actions on the instances

def _stop_one_instance(name, mydrivers=None, provider=None, region=None, silent=False, user=None, processed=None, statuscake=True, sc_details=None, force_clean=False):
    fdb = FileDatabase(get_config_data())
    if processed is None:
        processed = []

    if mydrivers is None:
        if provider is not None:
            mydrivers = eval('init_' + provider.lower() + '_driver([\'' + region + '\'])')
        else:
            mydrivers = init_all_drivers()

    for driver_name in mydrivers:
        if name in processed:
            return True
        if not silent:
            print "Looking for {} in {}".format(name,driver_name)
        driver = mydrivers[driver_name]
        try:
            instances = _get_instances(driver)
        except InvalidCredsError as ea:
            logging.error("Failed to Authenticate to {}".format(driver_name))
            continue
        except Exception as e:
            logging.error("{} : Exception: {}".format(driver_name,e))
            continue
        for instance in instances:
            if instance.name == name and name not in processed:
                while instance.state not in ['running','stopped']:
                    instance, driver = get_instance(name,{driver_name : driver},silent=True)
                if instance.state != "stopped":
                    print "* Trying to Stop {} on {}".format(name,driver_name)
                    if proxy_regex.search(name):
                        if user is None:
                            user = interaction.ask_value_with_confirm("ssh user: ")
                        print "Checking /opt/user-config"
                        with settings(host_string=name , user=user):
                            with hide('running', 'stderr', 'stdout'):
                                try:
                                    user_config_count = sudo("ls /opt/user-config | grep -v proxyIdentity.json | wc -l")
                                except:
                                    print(colored("WARN : Could not sudo to instance","yellow"))
                                    processed.append(name)
                                    return False
                                if user_config_count != "0":
                                    user_config = sudo("ls -l /opt/user-config/customers")
                                    print "\n/opt/user-config/customers:\n{}".format(user_config)
                                    user_config = sudo("ls -l /opt/user-config")
                                    print "\n/opt/user-config:\n{}".format(user_config)
                                    print(colored("ERROR: /opt/user-config is not empty (contents above).".format(name), "red"))
                                    #print "Resuming Status Cake test"
                                    #update_proxy_statuscake(name,0)
                                    processed.append(name)
                                    if force_clean or interaction.confirm("Would you like to clean user-config ('AssumeIdentity?name=cleancache'): "):
                                        proxy_user, proxy_pwd = util.get_proxy_credentails()
                                        clean_assume = sudo("curl http://{}:{}@{}:1666/AssumeIdentity?name=cleancache".format(proxy_user, proxy_pwd, name))
                                        print "AssumeIdentity status: {}".format(clean_assume)
                                        if clean_assume != 'OK':
                                            return False
                                        else:
                                            print "Waiting for AssumeID to complete."
                                            tries = 0
                                            while user_config_count != "0" and tries < 4:
                                                sleep(30)
                                                user_config_count = sudo("ls /opt/user-config | grep -v proxyIdentity.json | wc -l")
                                                tries += 1
                                            if user_config_count != "0":
                                                print(colored("ERROR: /opt/user-config did not clear. Will NOT STOP {}".format(name), "red"))
                                                return False
                                    else:
                                        return False
                                else:
                                    print(colored("/opt/user-config empty","green"))

                        if statuscake:
                            update_proxy_statuscake(str(name),1,sc_details=sc_details)

                        print "Stopping proxy process".format(name)
                        with settings(host_string=name , user=user):
                            stop_proxy = sudo('sed -i -e "s:\(.*/usr/local/bin/scout\):#\\1:g" /var/spool/cron/crontabs/root; sleep 5; service proxy stop')
                            if stop_proxy.succeeded:
                                sleep(10)
                            else:
                                print(colored("ERROR: Failed to stop proxy service" , "red"))
                                return stop_proxy
                    #return True
                    stop_status = driver.ex_stop_node(instance)
                    if statuscake and not stop_status and proxy_regex.search(name):
                        print(colored("ERROR: Failed to stop instance. Resuming status cake test" , "red"))
                        update_proxy_statuscake(str(name),0,sc_details=sc_details)
                    processed.append(name)
                    return stop_status
                else:
                    print "{} already stopped".format(name)
                    return True

    print "ERROR: Could not find {} in {}".format(name, mydrivers.keys())
    return False

def stop_instance(to_stop, mydrivers=None, provider=None, regions=None, silent=False, user=None, statuscake=True, sc_details=None, force_clean=False):
    processed = []
    if mydrivers is None:
        if provider is not None:
            #init_driver_function = "init_{}_driver(['{}'])".format(provider.lower(),region)
            #print "DEBUG: init_driver_function: \"{}\"".format(init_driver_function)
            #mydrivers = eval(init_driver_function)
            if regions is None:
                mydrivers = eval('init_' + provider.lower() + '_driver()')
            else:
                if type(regions).__name__ == "str":
                    mydrivers = eval('init_' + provider.lower() + '_driver([\'' + regions + '\'])')
                else:
                    mydrivers = {}
                    for region in regions:
                        mydrivers = eval('init_' + provider.lower() + '_driver([\'' + region + '\'])')

        else:
            mydrivers = init_all_drivers()

    if type(to_stop).__name__ == "str":
        return _stop_one_instance(to_stop, mydrivers, provider, regions, silent=silent, user=user, statuscake=statuscake, sc_details=sc_details, force_clean=force_clean)
    else:
        for instance_to_stop in sorted(to_stop):
            if instance_to_stop not in processed:
                if regions is None:
                    _stop_one_instance(instance_to_stop, mydrivers, provider, silent=silent, user=user, processed=processed, statuscake=statuscake, sc_details=sc_details, force_clean=force_clean)
                    processed.append(instance_to_stop)
                else:
                    for region in regions:
                        _stop_one_instance(instance_to_stop, mydrivers, provider, region, silent=silent, user=user,processed=processed, statuscake=statuscake, sc_details=sc_details, force_clean=force_clean)
                        processed.append(instance_to_stop)

def _start_one_instance(name, mydrivers=None, silent=False, statuscake=True, sc_details=None, user=None):
    fdb = FileDatabase(get_config_data())

    if mydrivers is None:
        mydrivers = init_all_drivers()

    #print "[DEBUG] Number of drivers: {}".format(len(mydrivers))
    for driver_name in mydrivers:
        if not silent:
            print "Looking for {} in {}".format(name,driver_name)
        driver = mydrivers[driver_name]
        try:
            instances = _get_instances(driver)
        except InvalidCredsError as ea:
            logging.error("Failed to Authenticate to {}".format(driver_name))
            continue
        except Exception as e:
            logging.error("{} : Exception: {}".format(driver_name,e))
            continue
        for instance in instances:
            if instance.name == name:
                if instance.state != "running":
                    print "Starting {} on {}".format(name,driver_name)
                    start_status = driver.ex_start_node(instance)
                    if proxy_regex.search(name) and statuscake:
                        print "Waiting for instance to be up to resume statuscake"
                        sleep(120)
                        try:
                            with settings(host_string=name , user=user):
                                sudo('sed -i -e "s:^#\(.*/usr/local/bin/scout\):\\1:g" /var/spool/cron/crontabs/root')
                        except Exception as e:
                            logging.warning("Failed to activate scout crontab. Exception: {}".format(e))
                        update_proxy_statuscake(str(name),0,sc_details=sc_details)
                    return start_status
                else:
                    print "{} already running".format(name)
                    return True
    print "ERROR: Could not find {} in {}".format(name, mydrivers.keys())
    return False

def start_instance(to_start, mydrivers=None, silent=False, statuscake=True, sc_details=None, user=None):
    if mydrivers is None:
        mydrivers = init_all_drivers()

    if type(to_start).__name__ == "str":
        return _start_one_instance(to_start, mydrivers,silent=silent, statuscake=statuscake, sc_details=sc_details, user=user)
    else:
        for instance_to_start in sorted(to_start):
            _start_one_instance(instance_to_start, mydrivers, silent=silent, statuscake=statuscake, sc_details=sc_details, user=user)

def get_all_proxies(drivers=None,all_instances=None):
    proxy_list = {}
    if all_instances is None:
        all_instances = get_all_instances(drivers)
    for driver in all_instances:
        for instance in all_instances[driver]:
            if proxy_regex.search(instance.name):
                proxy_list.update({instance.name : { 'instance' : instance, 'driver_name' : driver}})
    return proxy_list

def get_elastic_ips(driver=None):
    all_elastic_ips = driver.ex_describe_all_addresses(only_associated=False)
    associated_elastic_ips = driver.ex_describe_all_addresses(only_associated=True)
    free_elastic_ips = []
    for ip in all_elastic_ips:
        if ip.instance_id is None:
            free_elastic_ips.append(ip)
    print "Associated IPs"
    for ip in associated_elastic_ips:
        print ip.ip
    print "\nFree IPs"
    for ip in free_elastic_ips:
        print ip.ip

def get_security_groups(drivers=None):
    if drivers is None:
        drivers = init_ec2_driver()
    all_SG = {}
    for driver_name in drivers:
        logging.info("Getting Security Groups in {}".format(driver_name))
        driver = drivers[driver_name]
        try:
            all_SG[driver_name] = driver.ex_get_security_groups()
        except:
            logging.warn("Failed to get SGs from {}".format(driver_name))

    return all_SG

def create_security_group(name,description,**kwargs):
    params = {}
    for key, value in kwargs.items():
        params[key] = value

    if 'drivers' not in params:
        if 'regions' not in params:
            raise AttributeError("Must specify a region where to create the SG")
        drivers = init_ec2_driver(params['regions'])
    else:
        drivers = params['drivers']

    try:
        vpc_id = params['vpc_id']
    except:
        vpc_id = None

    for driver_name in drivers:
        driver = drivers[driver_name]
        try:
            sg = driver.ex_create_security_group(name, description, vpc_id=vpc_id)
            print(colored("{} created successfully".format(name), "green"))
            print "{}".format(sg)
            return sg
        except Exception as e:
            sys.exit(colored("Failed to add {}\n{}".format(name,e),"red"))

def delete_security_group(**kwargs):
    params = {}
    for key, value in kwargs.items():
        params[key] = value

    if 'group_id' not in params and 'group_name' not in params:
        raise AttributeError("'group_id' or 'group_name' required")

    if 'drivers' not in params:
        try:
            regions = params['regions']
        except:
            regions = None
        drivers = init_ec2_driver(regions)
    else:
        drivers = params['drivers']

    for driver_name in drivers:
        driver = drivers[driver_name]
        if 'group_name' in params:
            if 'group_id' in params:
                found = False
                all_sg = get_security_groups({driver_name : driver})
                for sg_driver in all_sg:
                    for sg in all_sg[sg_driver]:
                        if sg.name == params['group_name']:
                            if sg.id != params['group_id']:
                                logging.error("SG name {}({}) and SG id found {} do not match".format(params['group_name'],sg.id,params['group_id']))
                                continue
                            else:
                                found = True
                                break
                    if found:
                        break
                if not found:
                    logging.error("Could not verify SG name {} against SG ID {} in {}".format(params['group_name'], params['group_id'], driver_name))
                    continue
            try:
                driver.ex_delete_security_group(params['group_name'])
                print(colored("{} successfully deleted from {}".format(params['group_name'], driver_name), "green"))
            except Exception as e:
                logging.error("Failed to delete {} in {}\n{}".format(params['group_name'], driver_name, e))
        else:
            try:
                driver.ex_delete_security_group_by_id(params['group_id'])
                print(colored("{} successfully deleted from {}".format(params['group_id'], driver_name), "green"))
            except Exception as e:
                logging.error("Failed to delete {} in {}\n{}".format(params['group_id'], driver_name, e))


def update_security_group_ip(**kwargs):
    '''
        params:
            action (required): add / delete
            group_id: security group ID where to insert the rule (i.e. sg-2013f74f)
            group_name: security group name where to insert the rule (i.e. Stgn-ProxyServiceSG)
                -- either group_id or group_name required
            instance_id (required): instance name, IP or IP range to insert. (i.e. mdb-101-stgn.eu-west-1a.ie.wandera.biz or 54.62.121.203 or 10.0.4.0/24)
            port (optional): single port to be addded
            to_port + from_port (optional): if port range required
            regions (optional): region(s) where to look for the security group
            protocol (optional): default 'tcp'
            drivers (optional): ec2 libcloud drivers to use to find the security group
            all_instances (optional): dict with instances where to look for the instance_id to get IP
                    Format: { 'region_driver_name' : [list of instances] }
                    see init_ec2_driver for details
            decription (optional): description to add to all rules affected by the insertion
    '''
    required_params = ['group_id' , 'instances' ]
    params = {}
    for key, value in kwargs.items():
        params[key] = value

    if 'action' not in params:
        raise AttributeError("'action' is required")
    action = params['action']

    regions = None
    if 'regions' in params:
        regions = params['regions']

    if 'drivers' not in params:
        drivers = init_ec2_driver(regions)
    else:
        drivers = params['drivers']

    found = False
    if 'group_name' in params:
        print "Looking for SG id for {}".format(params['group_name'])
        all_sg = get_security_groups(drivers)
        for sg_driver in all_sg:
            for sg in all_sg[sg_driver]:
                if sg.name == params['group_name']:
                    if 'group_id' in params and sg.id != params['group_id']:
                        logging.error("SG name {}({}) and SG id {} do not match".format(params['group_name'],sg.id,params['group_id']))
                    else:
                        params['group_id'] = sg.id
                        found = True
                        break
            if found:
                break
        if not found:
            raise LookupError("Could not find SG name {}".format(params['group_name']))

    for req_param in required_params:
        if req_param not in params:
            raise AttributeError("{} is required".format(req_param))

    group_id = params['group_id']

    if ( 'from_port' in params and 'to_port' not in params ) or ( 'to_port' in params and 'from_port' not in params ):
        logging.error("Both or none of from_port and to_port must be provided\n" +
                    "Port options:\n" +
                    "\t1. port: single port to open\n" +
                    "\t2. from_port and to_port: ports range required\n" +
                    "\t3. none specified: all port allowed")

    if 'protocol' in params:
        if params['protocol'].lower() == 'all':
            protocol = -1
        else:
            protocol = params['protocol']
    else:
        protocol = "tcp"

    if 'from_port' in params:
        from_port = params['from_port']
        to_port = params['to_port']
    else:
        if 'port' not in params:
            if protocol == "tcp":
                from_port = 0
                to_port = 65535
            else:
                from_port = 0
                to_port = 0
        else:
            from_port = params['port']
            to_port = params['port']

    try:
        description = params['description']
    except:
        description = ''

    instances = params['instances']
    if type(instances).__name__ == "str":
        instances = [instances]

    all_instances = None
    for instance_id in instances:
        if ip_regex.search(instance_id):
            ips = ["{}/32".format(instance_id)]
        elif ip_range_regex.search(instance_id):
            ips = [instance_id]
        else:
            print "\nLooking for IP of {}".format(instance_id)
            description = instance_id
            my_instance = None
            if all_instances is None:
                if 'all_instances' not in params:
                    all_instances = get_all_instances()
                else:
                    all_instances = params['all_instances']

            for driver_name in all_instances:
                for instance in all_instances[driver_name]:
                    if instance.name == instance_id:
                        my_instance = instance
            if my_instance is None:
                logging.warning("Could not find details for {}".instance_id)
                return False
            ips = ["{}/32".format(my_instance.public_ip)]

        print "IP = {}".format(ips)

        print "Looking for SG {}".format(group_id)
        for driver_name in drivers:
            #print "Getting Security Groups in {}".format(driver_name)
            driver = drivers[driver_name]
            try:
                sg = driver.ex_get_security_groups([group_id])
                if len(sg) > 0:
                    try:
                        if action == 'add':
                            driver.ex_authorize_security_group_ingress(group_id, from_port, to_port, cidr_ips=ips, protocol=protocol, description=description)
                            print(colored("Rule {} from port {} to port {} on {} in {} successfully {}ed.".format(ips, from_port, to_port, group_id, driver_name, action),"green"))
                        elif action == 'delete':
                            for ip_to_delete in ips:
                                rule_found = False
                                for sec_group in sg:
                                    for rule in sec_group.ingress_rules:
                                        #print "rule['cidr_ips'] = {} ; ips = {}".format(rule['cidr_ips'], ips)
                                        if ip_to_delete in rule['cidr_ips']:
                                            rule_found = True
                                            if rule["from_port"] is None:
                                                from_port = 0
                                            else:
                                                from_port = rule["from_port"]
                                            if rule["to_port"] is None:
                                                to_port = 0
                                            else:
                                                to_port = rule["to_port"]
                                            print "driver.ex_revoke_security_group_ingress({},{},{},cidr_ips={},protocol={})".format(group_id, from_port, to_port, [ip_to_delete], rule["protocol"])
                                            driver.ex_revoke_security_group_ingress(group_id, from_port, to_port, cidr_ips=ips, protocol=rule["protocol"])
                                if not rule_found:
                                    logging.warning(colored("Could not find rule for '{}' in SG '{}'".format(ips, group_id), "yellow"))
                                    #raise LookupError("Could not find rule for '{}' in SG '{}'".format(ips, group_id))
                                else:
                                    print(colored("Rule {} from port {} to port {} on {} in {} successfully {}ed.".format(ip_to_delete, from_port, to_port, group_id, driver_name, action),"green"))
                    except Exception as ue:
                        logging.error(colored("Failed to {} {} on {} : {}".format(action, group_id, driver_name, ue),"red"))
            except Exception as e:
                pass
                #logging.warning("Could not find {} from {}: {}".format(group_id, driver_name, e))

def print_sg(sg, group_name=None, get_rules=False):
    if get_rules:
        if group_name is None:
            for rule in sg.ingress_rules:
                try:
                    port = rule["from_port"]
                    ips = rule["cidr_ips"]
                    for ip in ips:
                        print "{} : {}".format(ip, port)
                except:
                    print vars(rule)
        elif sg.name == group_name:
            for rule in sg.ingress_rules:
                try:
                    port = rule["from_port"]
                    ips = rule["cidr_ips"]
                    for ip in ips:
                        print "{} : {}".format(ip, port)
                except Exception as e:
                    print e
                    print vars(rule)
    else:
        if group_name is None:
            try:
                print "\n{}\n".format(json.dumps(sg, indent=4, sort_keys=True))
            except:
                print "\n{}\n".format(sg)
        elif sg.name == group_name:
            try:
                print "\n{}\n".format(json.dumps(vars(sg), indent=4, sort_keys=True))
            except:
                print "\n{}\n".format(vars(sg))


############ LOAD BALANCER AWS ############

lb_region = "eu-west-1"

def test_LB():
    region = lb_region

    AWS_ACCESS_KEY_ID = get_credentials()['EC2']['username']
    AWS_SECRET_ACCESS_KEY = get_credentials()['EC2']['accesskey']

    cls = get_lb_driver(lbProvider.ELB)
    driver = cls(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, region=region)

    balancers = driver.list_balancers()

    for lb in balancers:
        print "{}".format(lb.name)

def get_LB_driver(region=None):
    if region is None:
        region = lb_region

    AWS_ACCESS_KEY_ID = get_credentials()['EC2']['username']
    AWS_SECRET_ACCESS_KEY = get_credentials()['EC2']['accesskey']

    cls = get_lb_driver(lbProvider.ELB)
    driver = cls(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, region=region)
    return driver

def get_LB_list(driver=None, region=None):
    if driver is None:
        driver = get_LB_driver(region)

    return driver.list_balancers()

def get_LB_members(name, lbs=None, driver=None):
    if driver is None:
        driver = get_LB_driver(lb_region)

    if lbs is None:
        lbs = get_LB_list(driver, lb_region)

    for lb in lbs:
        if lb.name == name:
            balancer = driver.get_balancer(balancer_id = lb.id)
            return balancer, balancer.list_members()

def detach_LB_member(instance_name, lb, driver=None, provider=None, region=None):
    #print "[DEBUG] Trying to detach {} from {}".format(instance_name, lb)

    if driver is None:
        driver = get_LB_driver()

    balancer, lb_members = get_LB_members(lb,None,driver)
    ec2_drivers = init_ec2_driver([lb_region])
    ec2_instances = get_all_instances(ec2_drivers)

    for ec2_driver in ec2_instances:
        for ec2_instance in ec2_instances[ec2_driver]:
            if ec2_instance.name == instance_name:
                for member in lb_members:
                    if ec2_instance.id == member.id:
                        print "Detaching {} ({}) from {}".format(ec2_instance.name, member.id, balancer.id)
                        try:
                            detach_status = driver.balancer_detach_member(balancer, member)
                            return detach_status
                        except:
                            return False

def attach_LB_member(instance_name, lb, driver=None, provider=None, region=None):
    #print "[DEBUG] Trying to attach {} to {}".format(instance_name, lb)

    if driver is None:
        driver = get_LB_driver()

    balancer, lb_members = get_LB_members(lb,None,driver)
    ec2_drivers = init_ec2_driver([lb_region])
    ec2_instances = get_all_instances(ec2_drivers)

    for ec2_driver in ec2_instances:
        for ec2_instance in ec2_instances[ec2_driver]:
            if ec2_instance.name == instance_name:
                print "Attaching {} to {}".format(ec2_instance.name, balancer.id)
                try:
                    attach_status = driver.balancer_attach_compute_node(balancer, ec2_instance)
                    return attach_status
                except:
                    return None

################  ROUTE 53  ###############
#boto3
def get_route53_record_sets(hz_id,next_name=None,next_type=None):
    client = boto3.client('route53')

    #print "{}".format(client.get_hosted_zone(Id=hz_id))
    params = {'HostedZoneId': hz_id}

    if next_name is not None:
        params['StartRecordName'] = next_name
    if next_type is not None:
        params['StartRecordType'] = next_type

    record_sets = client.list_resource_record_sets(**params)
    all_records_info = record_sets['ResourceRecordSets']
    IsTruncated = record_sets['IsTruncated']

    while IsTruncated == True:
        next_name = record_sets['NextRecordName']
        next_type = record_sets['NextRecordType']
        #print "Getting next batch: Starting @ {}".format(next_name)

        params = {
            'HostedZoneId': hz_id,
            'StartRecordName': next_name,
            'StartRecordType': next_type
        }
        record_sets = client.list_resource_record_sets(**params)
        all_records_info = all_records_info + record_sets['ResourceRecordSets']
        IsTruncated = record_sets['IsTruncated']

    return all_records_info

def get_stack_record_sets(stack):
    all_records = get_route53_record_sets(get_hosted_zone(route53_hosted_zone).id)
    stack_records = []
    stack_regex = re.compile("Stack {}".format(stack))
    for rec in all_records:
        if 'SetIdentifier' in rec:
            if stack_regex.search(rec['SetIdentifier']):
                stack_records.append(rec)
    return stack_records

def get_hosted_zone(hosted_zone_name):
    driver = get_DNS_driver()
    zones = driver.list_zones()
    hz_name_regex = re.compile(hosted_zone_name)
    for zone in zones:
        if hz_name_regex.search(zone.domain):
            return zone

def update_route53_weight(stack, weight, stack_type=None, dryrun=True, force_weights=False):
    hz = get_hosted_zone(route53_hosted_zone)

    records = get_route53_record_sets(hz.id)

    client = boto3.client('route53')
    stack_regex = re.compile("Stack {}".format(stack))
    if not dryrun:
        print "INFO: Setting weight to {} for Stack {}".format(weight, stack)

    stack_record_sets = []
    for rec in get_stack_record_sets(stack):
        if rec['SetIdentifier'] not in stack_record_sets:
            stack_record_sets.append(rec['SetIdentifier'])

    rec_updated = False

    for stack_setIdentifier in stack_record_sets:
        if not dryrun:
            if stack_type is not None:
                stack_type_regex = re.compile(stack_type.lower())
                if stack_type_regex.search(stack_setIdentifier.lower()):
                    if not force_weights and _get_input("\nWould you like to set weight of \"{}\" to {}".format(stack_setIdentifier, weight),default='y').lower() != "y":
                        continue
                else:
                    continue
            else:
                if not force_weights and _get_input("\nWould you like to set weight of \"{}\" to {}".format(stack_setIdentifier, weight),default='y').lower() != "y":
                    continue

            for rec in get_stack_record_sets(stack):
                if rec['SetIdentifier'] == stack_setIdentifier:
                    print "- Updating {}".format(rec['Name'])
                    resourceRecordSet = {}
                    for key in rec:
                        if key == 'Weight':
                            resourceRecordSet[key] = weight
                        elif key == 'TTL':
                            resourceRecordSet[key] = 60
                        else:
                            resourceRecordSet[key] = rec[key]
                    rec_updated = True
                    #print "'Changes': [{{'Action': 'UPSERT','ResourceRecordSet': {{'Name': {},'Type': {},'SetIdentifier': {},'Weight': {},'TTL': 60}}}},]".format(rec['Name'],rec['Type'],rec['SetIdentifier'],weight)
                    try:
                        response = client.change_resource_record_sets(
                            HostedZoneId=hz.id,
                            ChangeBatch={
                                'Comment': 'Release deployment weight update',
                                'Changes': [
                                    {
                                        'Action': 'UPSERT',
                                        'ResourceRecordSet': resourceRecordSet
                                    },
                                ]
                            }
                        )
                        print response
                    except Exception as e:
                        print "\nERROR: Failed to set weight to {} for Stack {}".format(weight, stack)
                        print "{}".format(e)
                        return False

        else:
            if stack_type is not None:
                stack_type_regex = re.compile(stack_type.lower())
                if stack_type_regex.search(stack_setIdentifier.lower()):
                    print "\nWill ONLY set weight of \"{}\" to {}\n".format(stack_setIdentifier, weight)
            else:
                print "\nWill set weight of \"{}\" to {}\n".format(stack_setIdentifier, weight)

    if (not dryrun and rec_updated) or dryrun:
        return True
    else:
        return -1

def update_endpoint_weight(endpoint, stack, weight, force_weights=False):
    rec_updated = False
    hz = get_hosted_zone(route53_hosted_zone)

    records = get_route53_record_sets(hz.id)

    client = boto3.client('route53')
    stack_regex = re.compile("Stack {}".format(stack))
    print "INFO: Setting weight of {} to {} on Stack {}".format(endpoint, weight, stack)

    stack_record_sets = []
    endpoint_regex = re.compile(endpoint)
    for rec in get_stack_record_sets(stack):
        #print "{}".format(rec['Name'])
        if endpoint_regex.match(rec['Name']):
            print "Record set current status:\n{}".format(rec)
            if not force_weights and _get_input("\nWould you like to set weight of \"{}\" on Stack {} to {}".format(rec['Name'], rec['SetIdentifier'], weight),default='y').lower() != "y":
                continue
            resourceRecordSet = {}
            for key in rec:
                if key == 'Weight':
                    resourceRecordSet[key] = weight
                elif key == 'TTL':
                    resourceRecordSet[key] = 60
                else:
                    resourceRecordSet[key] = rec[key]
            try:
                response = client.change_resource_record_sets(
                    HostedZoneId=hz.id,
                    ChangeBatch={
                        'Comment': 'Release deployment weight update',
                        'Changes': [
                            {
                                'Action': 'UPSERT',
                                'ResourceRecordSet': resourceRecordSet
                            },
                        ]
                    }
                )
                rec_updated = True
                print response
            except Exception as e:
                print "\nERROR: Failed to set weight to {} for {} on Stack {}".format(weight, endpoint, stack)
                print "{}".format(e)
                return False
    if rec_updated:
        return True
    else:
        return -1
#end boto3

def get_DNS_driver():
    AWS_ACCESS_KEY_ID = get_credentials()['EC2']['username']
    AWS_SECRET_ACCESS_KEY = get_credentials()['EC2']['accesskey']

    cls = get_dns_driver(dnsProvider.ROUTE53)
    driver = cls(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
    return driver

def get_DNS_records(record_name=None, myzone=None, driver=None):
    if driver is None:
        driver = get_DNS_driver()
    r53_zones = driver.list_zones()
    zones = []
    if myzone is None:
        zones = r53_zones
    else:
        zone_regex = re.compile(myzone)
        for zone in r53_zones:
            if zone_regex.match(zone.domain):
                zones = [zone]
                break

    records = []
    for zone in zones:
        print "Getting records from {}".format(zone)
        zone_records = driver.list_records(zone)
        if len(zone_records) > 0:
            if record_name is None:
                records.extend(zone_records)
            else:
                for record in zone_records:
                    if record.name == record_name:
                        records.append(record)

    return records

########## STATUS CAKE ##########
def get_status_cake_driver():
    sc_user, sc_key = statuscake_api.init_statuscake_credentials()
    return statuscake_api.StatusCake(sc_key,sc_user)

def get_status_cake_details():
    print "Getting status cake tests"
    sc = get_status_cake_driver()
    all_statuscake_tests = sc.get_all_tests()
    statuscake_tests_details = []
    for test in all_statuscake_tests:
        statuscake_tests_details.append(sc.get_details_test(test["TestID"]))
    return sc, statuscake_tests_details

def update_statuscake_test(sc,test,paused):
    test["Paused"] = paused
    try:
        test["Confirmation"] = int(test["Confirmation"])
        test["ContactGroup"] = int(test["ContactGroups"][0]["ID"])
        test["TriggerRate"] = int(test["TriggerRate"])
    except:
        pass
    #print "\n{}\n".format(json.dumps(test))
    sc.update_test(test)
    sleep(5)
    new_test = sc.get_details_test(test["TestID"])
    print(colored("Updated Status for {} - Paused: {}".format(new_test["WebsiteName"], new_test["Paused"]) , "magenta"))
    return new_test["Paused"]

def update_proxy_statuscake(proxy_list,paused,sc_details=None):
    logging.info("Trying to set StatusCake to Pause = {} for {}".format(paused,proxy_list))
    try:
        if sc_details is None:
            sc, statuscake_tests_details = get_status_cake_details()
        else:
            sc = sc_details['sc']
            statuscake_tests_details = sc_details['statuscake_tests_details']
        if isinstance("proxy_list", str):
            proxy_list = [ proxy_list ]

        for proxy_name in proxy_list:
            logging.info("proxy_name to update: {}".format(proxy_name))
            proxy_regex = re.compile(proxy_name)
            for test in statuscake_tests_details:
                #if proxy_regex.search(test["WebsiteName"]):
                if test["WebsiteName"] == proxy_name:
                    print "Current status: TestID: {} - WebsiteName: {} - Paused: {}".format(test["TestID"], test["WebsiteName"], test["Paused"])
                    if ( test["Paused"] and paused == 1 ) or ( not test["Paused"] and paused == 0):
                        print(colored("{} is already paused {}".format(proxy_name, test["Paused"])))
                    else:
                        try:
                            update_statuscake_test(sc, test, paused)
                        except Exception as e:
                            logging.error("Failed to update test {}.\n{}".format(proxy_name, e))
    except Exception as e:
        logging.error("Failed to get status cakes tests\n{}\n".format(e))

########## UTILS ################

'''def _get_input(message = None):
    if message is None:
        display_message = "Would you wish to continue? [Y/n]: "
    else:
        display_message = message + " [Y/n]: "

    option = raw_input(display_message)
    while option not in [ "Y" , "n" ]:
        option = raw_input("Please enter a valid option [Y/n]: ")

    return option'''

def _get_input(message = None, default = None):
    if message is None:
        display_message = "Would you wish to continue? [y/n]"
    else:
        display_message = message + " [y/n]"

    if default is not None:
        display_message += " (default '{}'): ".format(default)
    else:
        display_message += ": "

    option = raw_input(display_message)
    if option == '' and default is not None:
        option = default
    while option.lower() not in ['y','n']:
        option = raw_input("Please enter a valid option [y/n] ")
        if option == '' and default is not None:
            option = default

    return option
