from setuptools import setup, find_packages

__version__ = None
execfile('deploy_release/__init__.py') # Get __version__

if not __version__:
    raise RuntimeError('__version__ not found')

setup(
    name='deploy_release',
    version=__version__,
    description='New release doployment',
    url='https://bitbucket.org/snappli-ondemand/operations/deploy_release',
    author='Wandera Ops',
    author_email='arturo.noha@wandera.com',
    packages=['deploy_release'],
    package_dir={'deploy_release': 'deploy_release'},
    package_data={'deploy_release': ['release_Proxy_nodes/*.csv','*.sh','release_services/*.info']},
    install_requires=[
        'apache-libcloud==1.1.0',
        'paramiko',
        'requests==2.11.1',
        'natsort',
        'boto',
        'boto3',
        'python-otcclient',
        'appscript',
        'termcolor',
        'wandera_woopas',
        'wandera_client'
    ],
    dependency_links = [
        'https://bitbucket.org/snappli-ondemand/wandera_woopas.git#egg=wandera_woopas',
        'https://bitbucket.org/snappli-ondemand/wandera_client.git#egg=wandera_client'
    ],
    entry_points={
        'console_scripts': [
            'deploy_release = deploy_release.deploy_release:main'
        ],
    },
)
