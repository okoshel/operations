#!/bin/bash
# Version: 0.0.1
# Release: 1
# Author: Gerric Chaplin
# Purpose: This script allows the user to easily diff two releases of specific packages from our repo. 
#

DIALOG=${DIALOG=dialog}
FILE_LOCATION="/tmp"
DIFF_LOCATION="/tmp"

get-package-version(){
$DIALOG --infobox "Retrieving $1 version: $2 " 20 50
sleep 5
wget --quiet https://s3-eu-west-1.amazonaws.com/snappli-software-repo/releases/$1_$2_all.deb -O $FILE_LOCATION/$1_$2_all.deb
		if [ $? -eq 0 ]; then
                        $DIALOG --infobox "$1 version: $2 has been downloaded to $FILE_LOCATION/$1_$2_all.deb" 20 50
                        sleep 2
                else
                        $DIALOG --infobox "There was an error downloading the file."  20 50
                        sleep 2
			rm -rf $FILE_LOCATION/$1_$2_all.deb
			exit 1
                fi

}

diff-package-version(){
$DIALOG --infobox "Performing diff of $1 versions: $2 $3 " 20 50
sleep 5
mkdir -p $DIFF_LOCATION/$1_$2
mkdir -p $DIFF_LOCATION/$1_$3

cd $DIFF_LOCATION/$1_$2 
ar p $FILE_LOCATION/$1_$2_all.deb data.tar.gz | tar zx
                if [ $? -eq 0 ]; then
                        $DIALOG --infobox "$1 version: $2 has been extracted to $DIFF_LOCATION/$1_$2" 20 50
                        sleep 2
                else
                        $DIALOG --infobox "There was an error extracting the file."  20 50
                        sleep 2
                        rm -rf $FILE_LOCATION/$1_$2_all.deb
                        rm -rf $DIFF_LOCATION/$1_$2
                        exit 1
                fi


cd $DIFF_LOCATION/$1_$3 
ar p $FILE_LOCATION/$1_$3_all.deb data.tar.gz | tar zx

                if [ $? -eq 0 ]; then
                        $DIALOG --infobox "$1 version: $3 has been extracted to $DIFF_LOCATION/$1_$3" 20 50
                        sleep 2
                else
                        $DIALOG --infobox "There was an error extracting the file."  20 50
                        sleep 2
                        rm -rf $FILE_LOCATION/$1_$3_all.deb
                        rm -rf $DIFF_LOCATION/$1_$3
                        exit 1
                fi

diff -urw $DIFF_LOCATION/$1_$2/opt/*/etc/*.yaml $DIFF_LOCATION/$1_$3/opt/*/etc/*.yaml > $FILE_LOCATION/DIFF_$1_$2_$3.txt
cat $FILE_LOCATION/DIFF_$1_$2_$3.txt
                        rm -rf $FILE_LOCATION/$1_$2_all.deb
                        rm -rf $DIFF_LOCATION/$1_$2
                        rm -rf $FILE_LOCATION/$1_$3_all.deb
                        rm -rf $DIFF_LOCATION/$1_$3
			rm -rf $FILE_LOCATION/DIFF_$1_$2_$3.txt
}

perform-diff-packages() {
dialog --inputbox \
"Please provide the old version:" 0 0 2> /tmp/inputbox.tmp.$$
	retval=$?
	v1=`cat /tmp/inputbox.tmp.$$`
	rm -f /tmp/inputbox.tmp.$$	
dialog --inputbox \
"Please provide the new version:" 0 0 2> /tmp/inputbox.tmp.$$
        retval=$?
        v2=`cat /tmp/inputbox.tmp.$$`
        rm -f /tmp/inputbox.tmp.$$

	get-package-version $1 $v1
	get-package-version $1 $v2

	diff-package-version $1 $v1 $v2
}


tempfile=`tempfile 2>/dev/null` || tempfile=/tmp/test$$
trap "rm -f $tempfile" 0 1 2 5 15
$DIALOG --clear --title "Diff packages" \
        --menu "Please select a package to diff." 20 51 10 \
        "1" "aggregationservice-enterprise" \
        "2" "apnservice-consumer" \
        "3" "apnservice-enterprise" \
        "4" "elementservice-consumer" \
        "5"  "portal-enterprise" \
        "6"  "profileservice-consumer" \
        "7"  "profileservice2-enterprise" \
        "8"  "proxyservice-consumer" \
        "9"  "proxyservice-enterprise" \
        "10"  "rabbit-consumer" \
        "11"  "rabbit-enterprise" \
        "12"  "reportservice-consumer" \
        "13"  "reportservice3-enterprise" \
        "14"  "userservice-consumer" \
        "15"  "userservice2-enterprise" \
        "16"  "videoservice" 2> $tempfile

retval=$?

choice=`cat $tempfile`
case $retval in
  0)
		case $choice in
			1)
				perform-diff-packages aggregationservice-enterprise
			;;
			2)
				perform-diff-packages apnservice-consumer
			;;
			3)
				perform-diff-packages apnservice-enterprise
                        ;;
                        4)
				perform-diff-packages elementservice-consumer
                        ;;
                        5)
				perform-diff-packages portal-enterprise
                        ;;
                        6)
				perform-diff-packages profileservice-consumer
                        ;;
                        7)
				perform-diff-packages profileservice2-enterprise
                        ;;
			8)
				perform-diff-packages proxyservice-consumer
			;;
                        9)
				perform-diff-packages proxyservice-enterprise
			;;
                        10)
				perform-diff-packages rabbit-consumer
                        ;;
			11)
				perform-diff-packages rabbit-enterprise
			;;
                        12)
				perform-diff-packages reportservice-consumer
                        ;;
			13)
				perform-diff-packages reportservice3-enterprise
			;;
			14)
				perform-diff-packages userservice-consumer
			;;
			15)
				perform-diff-packages userservice2-enterprise
			;;
			16)
				perform-diff-packages videoservice
			;;
		esac
	;;
  1)
    echo "Cancel pressed.";;
  255)
    echo "ESC pressed.";;
esac