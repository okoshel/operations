#!/bin/bash

#list of all environments
envList="production staging development qa operations"
#list of production environments
envProdList="production staging operations"

puppetMasterFQDN=puppet.snappli.net
puppetDevMasterFQDN=puppet-dev.wandera.net

# Needed so that the aptitude/apt-get operations will not be interactive
export DEBIAN_FRONTEND=noninteractive

disable_ipv6 () {
    echo "#disable ipv6 due to App engine IPv6 support
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1" > /etc/sysctl.conf; /sbin/sysctl -p
}

setup_puppet_repo () {
    wget http://apt.puppetlabs.com/puppetlabs-release-precise.deb -O /tmp/puppetlabs-release-precise.deb
    dpkg -i /tmp/puppetlabs-release-precise.deb
    apt-get update
    
    apt-get update && apt-get -y install puppet=3.7.3-1puppetlabs1 puppet-common=3.7.3-1puppetlabs1 facter=2.3.0-1puppetlabs1 hiera=1.3.4-1puppetlabs1
}

set_puppet_master () {
    # Find the current IP of the puppet master and make "puppet" point to it
    puppetMasterIp=$(host $puppetMasterFQDN | grep "has address" | head -1 | awk '{print $NF}')
    echo "$puppetMasterIp puppet puppet.snappli.net" >> /etc/hosts
}

set_dev_puppet_master () {
    # Find the current IP of the puppet master and make "puppet" point to it
    puppetMasterIp=$(host $puppetDevMasterFQDN | grep "has address" | head -1 | awk '{print $NF}')
    echo "$puppetMasterIp puppet puppet-dev.wandera.net" >> /etc/hosts
}

make_puppet_conf () {
    echo "[agent]
pluginsync = true
listen = true
lastrunreport = /var/lib/puppet/state/last_run_report.yaml
report_server = $puppetFQDN
report_port = 8140
reportserver = $puppetFQDN
reports = store,log
reportdir = /var/lib/puppet/reports
rundir = /var/run/puppet
server = $puppetFQDN
runninterval=60
rundir = /var/run/puppet
stringify_facts = false
environment = ${environment}" > /etc/puppet/puppet.conf

    echo "path /
auth any
method find,search,save
allow $puppetFQDN" > /etc/puppet/auth.conf
}

puppet_post_conf () {
    touch /etc/puppet/namespaceauth.conf 
    
    # Enable the puppet client
    sed -i /etc/default/puppet -e 's/START=no/START=yes/'
    
    # Make the puppet run directory so that the init script works. Puppet bug.
    mkdir -p /var/run/puppet
    
    update-rc.d puppet disable
    service puppet stop
    puppet agent -t
}

set_hostname () {
    hostname $FQDN
    echo $FQDN > /etc/hostname
    
    # get hostname
    domain=`echo $FQDN|cut -d "." -f 2-20`
    hostname=`echo $FQDN|cut -d "." -f 1`
    ip=`/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'`
    echo "$ip      $FQDN $hostname" > /tmp/hosts
    cat /etc/hosts >> /tmp/hosts
    cp /tmp/hosts /etc/hosts
    chmod 664 /etc/hosts
}

########
# MAIN #
########

# get input variables #
environment=${1}
FQDN=${2}

if [ "$#" -ne 2 ] ; then
    echo "Please provide an environment and FQDN as an argument, valid environments are: ${envList}"
    echo "Example: ./install_puppet.sh development web-101-dev.wandera.co.uk"
    exit 1
fi
 
if [[ $envList =~ $environment ]] ; then
    # env is good
    echo "Installing and configuring puppet for ${environment}"
else
    echo "Unknown environment ${environment}. Must be one of ${envList}"
    exit 1
fi

if [[ $envProdList =~ $environment ]] ; then
    # env is prod
    echo "$environment is PRODUCTION environment."
    prod=1
else
    echo "$environment is NON production environment."
    prod=0
fi

# end of get input variables #

disable_ipv6
set_hostname
setup_puppet_repo

if [[ $prod == 1 ]]; then
    puppetFQDN=$puppetMasterFQDN
    set_puppet_master
    echo '---
extension_requests:
  pp_preshared_key: "AAAAB3NzaC1yc2EAAAADAQABAAABAQCnO23ExeiZyVieFxzI07wjtVSljyhfO/DLeXj+/ztN9tlEygLoE4ZRlNwkZ5WgEHzbd7+i1W03oy3n0AUUuaAs7DphYo2PsseB1sNlqRnP1qcsULMkK7Wm+h8fZX+aCNwmJ0jdT+UBJ1lyoXDExY6XN2lhUMAuTVzYL0W8O7g4Ok/y2+s4RG32nUCXcFX9VqXp9zTJsxOJjesFMusBWOAtydm8BhLOUaITKaJeXfUrJnm+2zI989JuJWZ3plpq0zAtqj89vp/w6D/kU/JbIZfHv2sNGW869Ff7q8SOYD6imAtMsY6Y3VoZIuHqLLsL5+xOKuhjoB82exFhybXqP8w/"' > /etc/puppet/csr_attributes.yaml

else
    puppetFQDN=$puppetDevMasterFQDN
    set_dev_puppet_master
    # TODO keys they are now same, but they should differ
    echo '---
extension_requests:
  pp_preshared_key: "AAAAB3NzaC1yc2EAAAADAQABAAABAQCnO23ExeiZyVieFxzI07wjtVSljyhfO/DLeXj+/ztN9tlEygLoE4ZRlNwkZ5WgEHzbd7+i1W03oy3n0AUUuaAs7DphYo2PsseB1sNlqRnP1qcsULMkK7Wm+h8fZX+aCNwmJ0jdT+UBJ1lyoXDExY6XN2lhUMAuTVzYL0W8O7g4Ok/y2+s4RG32nUCXcFX9VqXp9zTJsxOJjesFMusBWOAtydm8BhLOUaITKaJeXfUrJnm+2zI989JuJWZ3plpq0zAtqj89vp/w6D/kU/JbIZfHv2sNGW869Ff7q8SOYD6imAtMsY6Y3VoZIuHqLLsL5+xOKuhjoB82exFhybXqP8w/"' > /etc/puppet/csr_attributes.yaml

fi

make_puppet_conf
puppet_post_conf
