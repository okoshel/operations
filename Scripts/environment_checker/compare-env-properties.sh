#!/bin/bash
version="1.0"
# VERSION 1.0 - by Radovan Makovsky 2017.07
# This script takes service properties varibles from server and compares them to environemnt variables used by puppet. Better description in help.

# local variables
puppetMaster=176.34.150.183
download=1

# variables for change
subject="Compare env properties script"
tmpDir="/tmp/compare-env-properties"
log="$tmpDir/compare-env-properties.log"

# support variables
scriptName="$0"
ts="date +%Y-%m-%d-%H:%M:%S"
# gnu-getopt on OS X installed from brew. Standart version is too old to use new features
gnuGetopt=/usr/local/opt/gnu-getopt/bin/getopt

### Functions ###
ech () { echo "--- `$ts` - INFO  - $1" |tee -a $log; }
echs () { echo -ne "--- `$ts` - INFO  - $1" |tee -a $log; }
echr () { echo "--- `$ts` - ERROR - $1" |tee -a $log; }
line () { echo "---------------------------------------" |tee -a $log; }
lineh () { echo "#######################################" |tee -a $log; }

f_help () {
    ech "Printing help"
    cat << EOF
This script compares data in environment and service files present on servers. It downloads and parses everything from servers and process it localy. This script is not ment to run on server, but on users computer.

Usage: $scriptName  [--server <ip> | -env <environment>][--help][--version]
Parameters:
  -s|--server - Some app-* server ip from environment we are interested.
  -e|--env - Environment name, like: production or qa
  -d|--skipdownload - skip downloading variables from puppets hiera which is slow. It is enough to download this data only once and then only verify against data downloaded from server.
  -v|--version
  -h|--help

Example:
# compares data from puppet environment and present in files on staging server
$scriptName -s 54.246.241.216 -e staging
EOF
}

# check if the server is reachable via ssh
f_check_ssh_connection () {
    # check if the server is reachable by ssh
    ech "ssh -q -oStrictHostKeyChecking=no $1"
    ssh -q -oStrictHostKeyChecking=no $1 "echo 0" |grep -q 0
    if [ $? -eq 0 ]; then
        ech "Server: $1 is reachable by ssh"
    else
        echr "Server: $1 is unreachable by ssh"
        exit 1
    fi
    return 0
}

# downloads files through scp
f_download_files () {
    ech "Downloading properties files from server: $1, location: $2"
    scp -q $1:"$2" $tmpDir/
    if [ $? -eq 0 ]; then
        ech "Files downloaded sucesfully"
    else
        echr "Failed to download files"
        exit 1
    fi
    return 0
}

# check if the correct gnu-getopt is installed on osx
f_set_getopt () {
    # If im on osx i need to have propper gnu-getopt installed
    ech "Checking OSX"
    which sw_vers > /dev/null
    if [ $? -eq 0 ]; then
        ech "OS X detected"
        if [ -f $gnuGetopt ]; then
            ech "Setting usage of gnu-getopt"
            useGetopt=$gnuGetopt
        else
            echr "gnu-getopt not found. Please install with: brew install gnu-getopt"
            exit 0
        fi
    else
        ech "OS X not detected. We are probably running on Linux."
    fi
    return 0
}

# it takes environment properties template and substitute variables with values from hiera
f_fill_data_from_hiera () {
    rm -f $envResult
    ech "Stripping all comments and empty lines"
    grep -v "^#" $tmpDir/environment.properties.erb |grep '[:blank:]' > $tmpDir/environment.properties.erb.tmp
    ech "rm $tmpDir/environment.properties.erb"
    rm $tmpDir/environment.properties.erb
    ech "Reading file into variables"
    while read -r line; do filecontent+=( "$line" ); done<$tmpDir/environment.properties.erb.tmp
    ech "rm $tmpDir/environment.properties.erb.tmp"
    rm $tmpDir/environment.properties.erb.tmp

    ech "Reading variables from hiera"
    line
    for line in "${filecontent[@]}"; do
        if [[ "$line" =~ "scope.function_hiera" ]]; then 
            firstPart=`echo "$line" |cut -d "=" -f 1`
            # we will get only variable name from line to get secondPart
            line=${line##*[\'}
            line=${line%%\']*}
            secondPart=`ssh -q -oStrictHostKeyChecking=no -oPreferredAuthentications=publickey $1 "sudo hiera -c /etc/puppet/hiera.yaml $line ::environment=$env"`
            # final string with filled variables from hiera
            exchanged="$firstPart=$secondPart"
            echo $exchanged >> $envResult
        else
            echo "$line" >> $envResult
        fi
        echo -n .
    done
    return 0
}

# removes empty lines and comments from files
f_strip_unneeded () {
    rm $tmpDir/environment.properties
    ech "Stripping empty spaces and comments from files"
    for file in `ls $tmpDir/*.properties`; do
        grep -v "^#" $file |grep '[:blank:]' > $file.stripped
        rm "$file"
    done
}

# finds matching and nod maching environment variables and stores them in variables for later display
f_find_env_variables () {
    line
    ech "Parsing variables"
    while read -r line; do fileResult+=( "$line" ); done<$envResult
    for line in "${fileResult[@]}"; do
        echo -n .
        firstPart=`echo "$line" |cut -d "=" -f 1`
        secondPart=`echo "$line" |cut -d "=" -f 2`
        missing=1
        allCorrect=1
        firstLine=""
        listFiles=""
        listVariables=""
        localVarDiff=""
        for file in `ls $tmpDir/*.properties.stripped`; do
            # we do not want any extra characters in name of the file
            shortFile=${file##*$tmpDir/}
            shortFile=${shortFile%%.properties.stripped}
            firstLine="$firstPart = $secondPart "
            grep -q "dw.$firstPart=" $file
            if [ $? -eq 0 ]; then # we found a match, that variable from environment is also present in service file
                listFiles="$listFiles | $shortFile"
                valueInFile=`grep "dw.$firstPart=" $file |cut -d "=" -f 2`
                listVariables="$listVariables | $valueInFile "
                # we want to know if variable in environment differs from that in service file
                if [ "$secondPart" != "$valueInFile" ]; then
                    allCorrect=0
                fi
                missing=0
            fi
        done

        # we want to store results nicely in arrays so we can display them easily
        if [ $missing -eq 1 ]; then
            varMissing+=("$firstLine")
        else
            if [ $allCorrect -eq 1 ]; then
                varCorrect+=("$firstLine")
                varCorrect+=("$listFiles")
            else
                varDiff+=("$firstLine")
                varDiff+=("$listFiles")
                varDiff+=("$listVariables")
            fi
        fi
    done

    # we do not need the files anymore
    ech "rm $tmpDir/*.properties.stripped"
    rm $tmpDir/*.properties.stripped

    return 0
}

# prints sorted results to stdout and to logfile
f_print_results () {
    echo
    lineh
    ech "OK variables. The value is same in environment and in all services."
    lineh
    for i in "${varCorrect[@]}"; do
        echo "$i"
    done
    line
    lineh
    ech "Missing variables. Variables are present in environment and not in services."
    lineh
    for i in "${varMissing[@]}"; do
        echo "$i"
    done
    line
    lineh
    ech "!!! DIFF variables !!! Varibles differs in environment and in services."
    lineh
    for i in "${varDiff[@]}"; do
        echo "$i"
    done
    lineh
    ech "All done."

    return 0
}

############
### MAIN ###
############

# we nee to create tmp dir first
if [ ! -d "$tmpDir" ]; then
    mkdir -p "$tmpDir"
fi

f_set_getopt

# commandline parameters
OPTIONS=`$useGetopt -o s:e:dhv --long server:,env:,skipdownload,help,version  -n "$0" -- "$@"`
if [ $? -ne 0 ]; then
    echr "Incorrect script variables"
    f_help
    exit 1
fi
eval set -- "$OPTIONS"

while true ; do
    case "$1" in
        -s|--server)server=$2; shift 2 ;;
        -e|--env)env=$2; shift 2 ;;
        -d|--skipdownload)download=0; shift 1 ;;
        -v|--version)echo "Version: $version"; exit 0 ;;
        -h|--help)f_help; exit 0 ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

if [[ -z $server ]] || [[ -z $env ]]; then
    echr "You have to specify server and environment"
    f_help
    exit 3
fi

# varibles setup from input parameters
envResult=$tmpDir/environment.properties.erb.result.$env

f_check_ssh_connection $server
f_download_files $server "/opt/service-properties/*.properties"
f_strip_unneeded
# download env properties from puppetmaster
if [ $download -eq 1 ]; then
    f_check_ssh_connection $puppetMaster
    f_download_files $puppetMaster "/etc/puppet/modules/environment_properties/templates/opt/service-properties/environment.properties.erb"
    f_fill_data_from_hiera $puppetMaster
else
    ech "Downloading data from puppets hiera skipped"
fi
f_find_env_variables
f_print_results

exit 0
