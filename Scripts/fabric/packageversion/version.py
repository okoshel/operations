from fabric.api import get, run, sudo, task, settings, local, abort
from fabric.contrib.console import confirm
from fabric.utils import abort

@task
def get_version():
    run("dpkg -l | grep enterprise")

@task    
def get_oomkiller():
    run("sudo grep -i oom /var/log/syslog")


# You can achive the same with 'fab - - command. 
@task    
def run_command(command): 
    #  Ignore exit code 1
    with settings(warn_only=True):
        result = run("{0}".format(command))
    if result.failed and not confirm("Tests failed. Continue anyway?"):
        abort("Aborting at user request.")  
# Testing, testing....    
@task    
def test_task(command, file):
    print("Arg one: %s, Arg two: %s" % (command, file))