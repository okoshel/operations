# Usage:
# Update from s3
# fab -P update_access_file:roles=staging_proxy,filename=access_file_OPS-2520 | grep done
# Restore backup version 
# fab -P restore_access_file:roles=staging_proxy 

from fabric.api import run, sudo, task
from fabric.utils import abort

@task
def update_access_file(filename=''):
        
    if filename:
        # Back up current file 
        sudo('cp /opt/proxy/conf/access /opt/proxy/conf/access_backup')
        # Update
        sudo('s3cmd  --config=/root/.s3cfg get s3://blacklist-wandera-net/{0} /opt/proxy/conf/access --force'.format(filename))
    else:
        abort("Missing access file name")

@task
def restore_access_file():
    sudo('cp /opt/proxy/conf/access_backup /opt/proxy/conf/access')
