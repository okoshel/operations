# Usage:
# Update from s3
# fab -P update_access_file:roles=staging_proxy,filename=access_file_OPS-2520 | grep done
# Restore backup version
# fab -P restore_access_file:roles=staging_proxy

from fabric.api import run, sudo, task
from fabric.utils import abort

@task
def check_access_file(filename=''):
    sudo('hostname; ls -l /opt/proxy/conf/access')

    '''if filename:
        # Back up current file
        sudo('ls -l /opt/proxy/conf/access')
    else:
        abort("Missing access file name")'''
