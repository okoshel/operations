from fabric.api import get, run, sudo, task
from fabric.utils import abort

@task
def get_access_log_count():
    run("sudo zcat /opt/proxy/logs/access.log.`date --date='yesterday' '+%Y-%m-%d'`.gz| wc -l")
