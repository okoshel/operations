# Usage:
# Update from s3
# fab -P update_access_file:roles=staging_proxy,filename=access_file_OPS-2520 | grep done
# Restore backup version
# fab -P restore_access_file:roles=staging_proxy

from fabric.api import run, sudo, task
from fabric.utils import abort

@task
def update_logback_file(filename=''):

    if filename:
        # Back up current file
        sudo('cp /opt/proxy/conf/logback.xml /opt/proxy/conf/logback.xml.backup')
        # Update
        sudo('s3cmd  --config=/root/.s3cfg get s3://wandera-static-files/proxy-logback/{0} /opt/proxy/conf/logback.xml --force'.format(filename))
    else:
        abort("Missing logback file name")

@task
def restore_logback_file():
    sudo('cp /opt/proxy/conf/logback.xml.backup /opt/proxy/conf/logback.xml')
