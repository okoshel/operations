from fabric.api import run, sudo, task, env
from fabric.utils import abort
from fabric.context_managers import hide, show
from termcolor import colored
import sys

canary = 'f69de1f33ef87cc9.node.eu-west-2b.gb.wandera.com'

def transfer_file(filename=''):
    if filename:
        # Back up current file
        sudo('cp /opt/proxy/conf/dynamicFirewallRules.json /opt/proxy/conf/dynamicFirewallRules.json_backup')
        # Update
        sudo('s3cmd --config=/root/.s3cfg get s3://blacklist-wandera-net/{0} /opt/proxy/conf/dynamicFirewallRules.json --force'.format(filename))

    if env.host_string == canary:
        print "Checking 'com.wandera.proxy.util.DynamicFileReloader' in rabbit.log"
        with hide('running','stdout','stderr'):
            out = sudo('if [[ `grep "com.wandera.proxy.util.DynamicFileReloader" /opt/proxy/logs/rabbit.log` ]]; then grep "com.wandera.proxy.util.DynamicFileReloader" /opt/proxy/logs/rabbit.log; else echo "OK"; fi')
            if out == 'OK':
                print(colored('DFW Load successful','green'))
            else:
                print out
                '''print "Reverting File"
                restore_dynamicfirewall_file()
                sys.exit(1)'''

@task
def update_dynamicfirewall(filename=''):
    transfer_file(filename)

@task
def restore_dynamicfirewall_file():
    sudo('cp /opt/proxy/conf/dynamicFirewallRules.json_backup /opt/proxy/conf/dynamicFirewallRules.json')
