# Usage:
# Update from s3
# fab -P update_access_file:roles=staging_proxy,filename=access_file_OPS-2520 | grep done
# Restore backup version
# fab -P restore_access_file:roles=staging_proxy

from fabric.api import run, sudo, task
from fabric.utils import abort

@task
def update_proxy_chain_file(filename=''):

    if filename:
        # Back up current file
        sudo('cp /opt/proxy/conf/proxyChain.yaml /opt/proxy/conf/proxyChainBackup.yaml')
        sudo('cp /opt/linkreplacementservice/conf/proxyChain.yaml /opt/linkreplacementservice/conf/proxyChainBackup.yaml')
        # Update
        sudo('s3cmd get s3://blacklist-wandera-net/{0} /opt/proxy/conf/proxyChain.yaml --force'.format(filename))
        sudo('s3cmd get s3://blacklist-wandera-net/{0} /opt/linkreplacementservice/conf/proxyChain.yaml --force'.format(filename))
    else:
        abort("Missing access file name")

@task
def restore_proxy_chain_file():
    sudo('cp /opt/proxy/conf/proxyChainBackup.yaml /opt/proxy/conf/proxyChain.yaml')
    sudo('cp /opt/linkreplacementservice/conf/proxyChainBackup.yaml /opt/linkreplacementservice/conf/proxyChain.yaml')
