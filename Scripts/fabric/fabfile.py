from fabric.api import env

import instances
from filterservice.blacklist import blacklist
from filterservice.whitelist import whitelist_ips
from filterservice.whitelist import whitelist_domain
from filterservice.whitelist import whitelist
from packageversion.version import get_version
from packageversion.version import get_oomkiller
from packageversion.version import run_command
from packageversion.version import test_task
from proxy.dynamicfirewallupdate import update_dynamicfirewall
from proxy.countproxyrows import get_access_log_count
from proxy.accessfileupdate import update_access_file
from proxy.accessfileupdate import restore_access_file
from proxy.logbackfileupdate import update_logback_file
from proxy.logbackfileupdate import restore_logback_file
from proxy.accessfilecheck import check_access_file
from proxy.proxychainfileupdate import update_proxy_chain_file
from proxy.proxychainfileupdate import restore_proxy_chain_file
from general.mcollective import restart_mcollective
from general.mcollective import stop_mcollective
from general.mcollective import start_mcollective
from general.puppet import clear_puppet_ssl
from general.puppet import cleanweakssh
from general.logstash import *

env['sudo_prefix'] += '-H '
env['abort_on_prompts'] = False
#env['skip_bad_hosts'] = True
