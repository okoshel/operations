import argparse
import logging
from os.path import expanduser
from shutil import copyfile
from fabric.context_managers import settings
from fabric.api import cd, env
from fabric.operations import sudo
import json
import os
import sys
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import ssl
import instances
import proxy.accessfileupdate
from jira import JIRA

env.use_ssh_config = True
_old_match_hostname = ssl.match_hostname
pr_reviewer = "matthew.tyas"

def _new_match_hostname(cert, hostname):
    # https: // github.com / boto / boto / issues / 2836
   if hostname.endswith('.s3.amazonaws.com'):
      pos = hostname.find('.s3.amazonaws.com')
      hostname = hostname[:pos].replace('.', '') + hostname[pos:]
   return _old_match_hostname(cert, hostname)

ssl.match_hostname = _new_match_hostname

def do_blacklist_prep(ticket, jira_creds, pr_link, ssh_user=None, ssh_key=None, job_num="1"):
    # Fabric to dev/qa/staging
    if ssh_key is not None:
        env.key_filename = ssh_key
    if ssh_user is not None:
        env.user = ssh_user
    failed_hosts = []
    for env_name in ['qa', 'development', 'staging']:
        logging.info("Getting proxies for %s", env_name)
        hosts = instances.get_proxies(env_name, getip=True)
        logging.info("Deploying to %s", hosts)
        for host in hosts:
            try:
                with settings(host_string=host):
                    proxy.accessfileupdate.update_access_file("access_file_{}".format(ticket))
            except Exception as e:
                logging.error("Impossible to update access for %s, skipping: %s", host, e)
                failed_hosts.append(host)

    if len(failed_hosts) > 0:
        logging.error("Failed to update blacklist in {}".format(json.dumps(failed_hosts)))
        logging.error("Halting execution, CR won't be created. Fix and re-run")
        sys.exit(1)
    #Inputs:
    # - JIRA ticket
    jira = JIRA("https://snappli.atlassian.net", basic_auth=jira_creds)
    OPS_ticket = jira.issue(ticket)

    # Create CR linked
    logging.info("Prepare CR change")
    CR_content = """Created by [Jenkins Pipeline|https://ciops.wandera.net/job/blacklist-automation]
*Preparation:*
* [PR|%(PR_LINK)s]
* [File uploaded to S3|https://console.aws.amazon.com/s3/object/blacklist-wandera-net/access_file_%(TICKET)s?region=eu-west-1&tab=overview]
* [Deployed to proxies in dev/qa/staging |https://ciops.wandera.net/job/blacklist-automation/%(JOB)s/console]

*Deployment:*
* [Deploy to ONE proxy in production|https://ciops.wandera.net/job/blacklist-automation/parambuild/?TICKET=%(TICKET)s&STEP=CANARY]
* [Deploy to ALL proxies in production|https://ciops.wandera.net/job/blacklist-automation/parambuild/?TICKET=%(TICKET)s&STEP=PROD]""" % {
        'TICKET': ticket,
        'JOB' : job_num,
        'PR_LINK' : pr_link
    }
    jira_fields = {
        'project': 'CR',
        'summary': 'Blacklist update %s' % ticket,
        'description': CR_content,
        'issuetype': {'name': 'Change'},
        "customfield_13401": {
            "id": "11403",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11403",
            "value": "Minor / Localized"
        },
        "customfield_13402": {
            "id": "12030",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11405",
            "value": "Normal"
        },
        "customfield_13403": {
            "id": "11410",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11410",
            "value": "Low"
        },
        "customfield_13404": {
            "id": "11415",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11415",
            "value": "Other"
        },
        "customfield_13407": {
            "id": "11419",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11419",
            "value": "Low"
        },
    }
    new_issue = jira.create_issue(fields=jira_fields)
    jira.create_issue_link("Relates", new_issue, OPS_ticket)

    jira.add_comment(new_issue, '[~michael.grafton] Blacklist deployed On Non production envs')

    jira.assign_issue(new_issue,pr_reviewer)

    jira.transition_issue(new_issue,"Submit for Review")

    print "https://snappli.atlassian.net/browse/%s" % new_issue.key
    print "CR_KEY: {}".format(new_issue.key)

def run():
    parser = argparse.ArgumentParser(description='Does lot of blacklist shit', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-l', '--level', help='Log Level',default='INFO', required=False, choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'])
    parser.add_argument('-t', '--ticket', help='Jira Ticket', type=str, required=True)
    parser.add_argument('-c', '--creds', help='Jira Credentials: user password', nargs = '+', required=True)
    parser.add_argument('-k', '--key', help='SSH key', required=False, default=None)
    parser.add_argument('-u', '--user', help='SSH user', required=False, default=None)
    parser.add_argument('-j', '--job', help='Jenkins Job number', required=False, default="1")
    parser.add_argument('-p', '--pr', help='Link to access file PR', required=True)

    args = parser.parse_args()

    logging.basicConfig(level=args.level, format='%(asctime)s %(levelname)s %(message)s')

    do_blacklist_prep(args.ticket,tuple(args.creds),pr_link=args.pr,ssh_user=args.user,ssh_key=args.key,job_num=args.job)

if __name__ == "__main__":
    run()
