import tempfile

import ujson
from fabric.api import get, run, sudo, task
from fabric.utils import abort


def ensure_dirs_empty(dirs):
    for d in dirs:
        if sudo('ls -A {}'.format(d)):
            sudo('rm {}/*'.format(d))

def transfer_file(filename):
    sudo('s3cmd get s3://blacklist-wandera-net/{0} /opt/filterservice/etc/security/Phishtank/{0} --force'.format(filename))

@task
def blacklist(filename):

    dirs = ['/opt/filterservice/etc/security/Phishtank/',
            '/opt/filterservice/etc/security/sophos/illegal/',
            '/opt/filterservice/etc/security/sophos/malware/',
            '/opt/filterservice/etc/security/sophos/spam/']

    ensure_dirs_empty(dirs)
    transfer_file(filename)   
    output = run('curl -X POST "http://127.0.0.1:1598/tasks/LoadDataTask"')
    if 'Importing from: /opt/filterservice/etc/security/Phishtank/{}'.format(filename) not in output:
        abort('Error running LoadDataTask:\n{}'.format(output))

    with tempfile.TemporaryFile() as fd:
        get('/opt/filterservice/etc/security/Phishtank/{0}'.format(filename), fd)
        fd.seek(0)
        for entry in fd.readlines():
            output = run('curl -X GET "http://127.0.0.1:1599/filter/v1/analyser/?sourceIp=1.0.0.0&destinationIp=2.0.1.11&uri={}"'.format(entry.split(",",2)[1]))
            result = ujson.loads(output)['analyserResponse']['result']
            if result != 'DIRTY':
                abort('Failed to block all entries')

    sudo('rm /opt/filterservice/etc/security/Phishtank/{}'.format(filename))
