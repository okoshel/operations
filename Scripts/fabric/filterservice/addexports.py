import tempfile
import time
from fabric.api import sudo, task
from fabric.utils import abort


def extract_archive(filename):
    sudo('tar -zxvf /opt/filterservice/etc/deltaBerkeleyDb/{0} --strip-components=1'.format(filename))


def transfer_file(filename):
    sudo('s3cmd get s3://blacklist-wandera-net/{0} /opt/filterservice/etc/deltaBerkeleyDb/{0} --force'.format(filename))


def ensure_fs_stopped(servicename):
    sudo('service {0} stop'.format(servicename))


def ensure_fs_started(servicename):
    sudo('service {0} start'.format(servicename))


@task
def add_exports(filename, servicename):
    ensure_fs_stopped(servicename)
    time.sleep(15)
    transfer_file(filename)
    extract_archive(filename)
    ensure_fs_started(servicename)


