import ujson
from fabric.api import run, sudo, task
from fabric.utils import abort

def transfer_file(filename):
    sudo('s3cmd get s3://blacklist-wandera-net/{0} /opt/filterservice/etc/security/exceptions/exceptionRules.json --force'.format(filename))

@task
def whitelist_ips(filename):
    transfer_file(filename)
    output = run('curl -X POST "http://127.0.0.1:1598/tasks/ExceptionRulesLoadTask"')

    if "error" in output.lower():
        abort("Error running ExceptionRulesLoadTask")

    with open(iptest, "r") as f:
        for ip in f:
            msg = '\'{{ "sourceIp": "10.0.0.1", "destinationIp": "{}", "uri": "http://s3.amazonaws.com", ' \
             '"osType": "IOS", "requestHeader": {{ "requestLine": "GET http://test.malware.wandera.com HTTP/1.1", ' \
             '"Host": "test.malware.wandera.com", "User-Agent": "Chess/2.12.33 CFNetwork/609.1.4 Darwin/13.0.0", ' \
             '"Cookie": "lat=0.00; lon=0.00" }}, "responseHeader": {{ "responseLine": "HTTP/1.0 200 OK", ' \
             '"Content-Type": "application/json" }}}}\''.format(ip.strip())

            output = run('curl -X PUT -d {} -H "Accept:application/json" -H "Content-Type:application/json" "http://127.0.0.1:1599/filter/v1/analyser/"'.format(msg))
            result = ujson.loads(output)['analyserResponse']['result']
            if result != 'CLEAN':
                abort('Failed to whitelist all entries')

@task
def whitelist_domain(filename):
    transfer_file(filename)
    output = run('curl -X POST "http://127.0.0.1:1598/tasks/ExceptionRulesLoadTask"')

    if "error" in output.lower():
        abort("Error running ExceptionRulesLoadTask")

    msg = 'curl -s -X GET "http://127.0.0.1:1599/filter/v1/analyser/?sourceIp=1.0.0.0&destinationIp=62.241.13.37&uri=http://www.poste.it/test.html"'

    output = run(msg)
    result = ujson.loads(output)['analyserResponse']['result']
    if result != 'CLEAN':
        abort('Failed to whitelist all entries')


# Usage:
# fab -P whitelist:destinationIP='203.205.129.101',reloadRules=True,filename=exceptionRules-OPS-2309.json,roles=staging_filterservice
@task
def whitelist(uri='malware.threatops.co.uk', sourceIP='1.0.0.0', destinationIP='2.0.0.0', filename='exceptionRules.json', reloadRules=False):
    
    if reloadRules: 

        transfer_file(filename)
        output = run('curl -X POST "http://127.0.0.1:1598/tasks/ExceptionRulesLoadTask"')

        if "error" in output.lower():
            abort("Error running ExceptionRulesLoadTask")

    msg = 'curl -s -X GET "http://127.0.0.1:1599/filter/v1/analyser/?sourceIp=%s&destinationIp=%s&uri=%s"' % (sourceIP, destinationIP, uri)

    output = run(msg)
    result = ujson.loads(output)['analyserResponse']['result']
    if result != 'CLEAN':
        abort('Failed to whitelist all entries')
