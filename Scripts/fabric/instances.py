from functools import partial

from fabric.api import env
from pymongo import MongoClient

client = MongoClient('mongodb://54.246.142.127:27017/')
collection = client.cmdb.instances


def get_hostnames(environment, role=None, getip=False):
    filter = {"environment": environment, "exists":True}
    if role is not None:
        filter["role"] = role
    instance_info = 'hostname'
    if getip:
        instance_info = 'external_ip'
    return [instance[instance_info] for instance in collection.find(filter)]

def get_proxies(environment, getip=False):
    # Currently "role" is not working due to ongoing role fact update...
    # return [instance['hostname'] for instance in collection.find({"environment": environment, "role": role})]
    # Using regex instead. Following is for proxy only.
    instance_info = 'hostname'
    if getip:
        instance_info = 'external_ip'
    return_instances = []
    for instance in collection.find({"environment": environment, "exists":True, 'hostname':{'$regex':'node'}}):
        if instance_info not in instance:
            print "\n{} MISSING in {}:\nKeys: {}\n".format(instance_info,instance['hostname'],instance.keys())
        else:
            return_instances.append(instance[instance_info])
    return return_instances
    #return [instance[instance_info] for instance in collection.find({"environment": environment, "exists":True, 'hostname':{'$regex':'node'}})]

def get_environment(environment, getip=False):
    instance_info = 'hostname'
    if getip:
        instance_info = 'external_ip'
    instances = [instance[instance_info] for instance in collection.find({"environment": environment, "exists":True,  "external_ip" :{"$exists":True} })]
    print instances
    return instances

def get_all(getip=False):
    instance_info = 'hostname'
    if getip:
        instance_info = 'external_ip'
    return [instance[instance_info] for instance in collection.find({"exists":True})]

def get_filterservice_slaves(enviroment, getip=True):
    instance_info = 'hostname'
    if getip:
        instance_info = 'external_ip'
    return [instance[instance_info] for instance in collection.find({ "role": ["mysql_slave"], "environment": enviroment, "exists":True})]
    
def get_portals(enviroment, getip=True):
    instance_info = 'hostname'
    if getip:
        instance_info = 'external_ip'
    return [instance[instance_info] for instance in collection.find({ 'hostname':{'$regex':'web-'}, "environment": enviroment, "role": ["portal"], "exists":True })]

def get_file_hostnames():
    hostnames = []
    f = open('targets.txt', 'r')
    for line in f.readlines():
        line = line.partition('#')[0]
        line = line.strip()
        if len(line) > 0:
            hostnames.append(line)
    f.close()
    return hostnames
# Refactor get_hosts mess - work in progress
# def get_hosts(roles, names, enviroments, locations):
#     client = MongoClient('mongodb://54.246.142.127:27017/')
#     collection = client.cmdb.instances
#     return [instance['hostname'] for instance in collection.find({ 'hostname':{'$regex':hostname}, "environment": enviroment, "role": [role], "exists":True })]

env.roledefs = {
    'all_instances': partial(get_all, getip=True),
    'from_file': partial(get_file_hostnames),
}

for env_name in ['integration', 'qa', 'development', 'staging', 'operations', 'production']:
    #Specifics
    env.roledefs[env_name] = partial(get_environment, env_name, getip=True)
    env.roledefs['%s_proxy' % env_name] = partial(get_proxies, env_name, getip=True)
    env.roledefs['%s_portals' % env_name] = partial(get_portals, env_name, getip=True)
    env.roledefs['%s_filterservice' % env_name] = partial(get_filterservice_slaves, env_name, getip=True)

    #get_hostnames with filter (using ips)
    env.roledefs['%s_app' % env_name] = partial(get_hostnames, env_name, role='app', getip=True)
    env.roledefs['%s_service_apache' % env_name] = partial(get_hostnames, env_name, role='service_apache', getip=True)
    env.roledefs['%s_connector' % env_name] = partial(get_hostnames, env_name, role='connector', getip=True)
    env.roledefs['%s_video' % env_name] = partial(get_hostnames, env_name, role='video', getip=True)
    env.roledefs['%s_graphite' % env_name] = partial(get_hostnames, env_name, role='graphite', getip=True)
