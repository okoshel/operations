import argparse
import logging
from os.path import expanduser
from shutil import copyfile
from git import Repo
from fabric.context_managers import settings
from fabric.api import cd
from fabric.operations import sudo
import json
import os
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import ssl
from fabric.api import env
import instances
import proxy.accessfileupdate
from jira import JIRA

env.use_ssh_config = True
_old_match_hostname = ssl.match_hostname

def _new_match_hostname(cert, hostname):
    # https: // github.com / boto / boto / issues / 2836
   if hostname.endswith('.s3.amazonaws.com'):
      pos = hostname.find('.s3.amazonaws.com')
      hostname = hostname[:pos].replace('.', '') + hostname[pos:]
   return _old_match_hostname(cert, hostname)

ssl.match_hostname = _new_match_hostname

def prepare_puppet_git(puppet_path):
    repo = Repo(puppet_path)
    if repo.is_dirty():
        logging.critical("Puppet repository is dirty please clean your changes")
        exit(1)

    # repo.heads.master.checkout()
    repo.heads.master.checkout()
    repo.head.reset(index=True, working_tree=True)
    logging.debug("Pulling remote %s", repo.remote())
    repo.remote().pull()

    return repo

def do_blacklist_prep(ticket, puppet_path):
    #Inputs:
    # - JIRA ticket
    # - path of access file
    # - path of puppet repo
    repo = prepare_puppet_git(puppet_path)

    with open(os.path.expanduser('~/.wandera/woopas.json'), "r") as woopas_file:
        woopas_config = json.load(woopas_file)
    aws_key = woopas_config["woopas"]["ec2_user_id"]
    aws_secret = woopas_config["woopas"]["ec2_secret"]

    jira = JIRA("https://snappli.atlassian.net", basic_auth=(woopas_config["woopas"]["jira_user"], woopas_config["woopas"]["jira_sword"]))
    OPS_ticket = jira.issue(ticket)
    logging.info("Doing prep for %s", ticket)

    # Copy to right places
    non_prod_access_path = "%s/modules/service/files/config/proxy/access" % puppet_path
    prod_access_path = "%s/environments/production/modules/proxy_enterprise/templates/opt/proxy/conf/access" % puppet_path
    raw_ip_path = "%s/modules/service/files/config/proxy/rawips" % puppet_path

    logging.info("Downloading access file to non prod: %s", non_prod_access_path)
    if len(OPS_ticket.fields.attachment) == 0:
        logging.critical("Access file not attached to ticket")
        exit(1)
    #access list
    attachment = OPS_ticket.fields.attachment[0]
    #raw ip
    attachment1 = OPS_ticket.fields.attachment[1]
    access_file = attachment.get()
    raw_ip = attachment1.get()

    # Commit for prod on it's own branch
    logging.info("Creating branch for prod change")
    try:
        repo.heads[ticket]
    except IndexError:
        logging.info("Creating new branch %s from master", ticket)
        repo.create_head(ticket)
    repo.heads[ticket].checkout()

    with open(non_prod_access_path, 'w') as non_prod_access_file_w:
        non_prod_access_file_w.write(access_file)#Non Prod
    #Write rawIP into file under GIT
    with open(raw_ip_path, 'w') as raw_ip_path_file_w:
        raw_ip_path_file_w.write(raw_ip)# Writing RAW IP

    # Commit for staging Push
    logging.info("Commiting and pushing non prod change to master")
    repo.index.add([non_prod_access_path])
    staging_commit = repo.index.commit("%s: Non prod blacklist update" % ticket)

    logging.info("Copying access file to prod: %s", prod_access_path)
    copyfile(non_prod_access_path, prod_access_path) #Prod

    logging.info("Commiting and pushing prod change")
    repo.index.add([prod_access_path])
    repo.index.commit("%s: Prod blacklist update" % ticket)
    repo.remote().push(refspec='%(TICKET)s:%(TICKET)s' %{'TICKET':ticket})


    # Commit for rawIP
    logging.info("Commiting and pushing rawIP into master")
    repo.index.add([raw_ip_path])
    repo.index.commit("%s: RawIP blacklist update" % ticket)
    repo.remote().push(refspec='%(TICKET)s:%(TICKET)s' %{'TICKET':ticket})

    logging.info("Pulling puppet master")
    with settings(host_string='puppet.snappli.net', user=woopas_config["woopas"]["ssh_username"]):
        with cd("/opt/puppet"):
            sudo("git pull")

    # Push access file to S3
    logging.info("Pushing file to S3")
    conn = S3Connection(aws_key, aws_secret, host='s3-eu-west-1.amazonaws.com')
    bucket = conn.get_bucket("blacklist-wandera-net")
    access_file_key = Key(bucket, "access_file_%s" % ticket)
    with open(non_prod_access_path, "r") as access_file:
        access_file_key.set_contents_from_file(access_file)

    latest_access_file_key = Key(bucket, "access_file_latest")
    with open(non_prod_access_path, "r") as access_file:
        latest_access_file_key.set_contents_from_file(access_file)

    # Create CR linked
    logging.info("Prepare CR change")
    CR_content = """Commit: https://bitbucket.org/snappli-ondemand/puppet/commits/%(COMMIT)s
Pull request: https://bitbucket.org/snappli-ondemand/puppet/pull-requests/new?source=%(TICKET)s&t=1

Deployment:
* Put the file in S3: https://console.aws.amazon.com/s3/buckets/blacklist-wandera-net/?region=eu-west-1&tab=overview
* Deploy to one proxy in dev/qa/staging (/):
{code}
fab -P update_access_file:roles=development_proxy,filename=access_file_%(TICKET)s --colorize-errors --skip-bad-hosts
fab update_access_file:roles=qa_proxy,filename=access_file_%(TICKET)s --colorize-errors --skip-bad-hosts
fab update_access_file:roles=staging_proxy,filename=access_file_%(TICKET)s --colorize-errors --skip-bad-hosts
{code}
* Deploy to one proxy in production:
{code}
fab update_access_file:hosts='f69de1f33ef87cc9.node.eu-west-2b.gb.wandera.com',filename=access_file_%(TICKET)s --colorize-errors --skip-bad-hosts
{code}
* Deploy to all proxies in production:
{code}
fab -P -z 5 update_access_file:roles=production_proxy,filename=access_file_%(TICKET)s --colorize-errors --skip-bad-hosts
{code}""" % {
        'TICKET': ticket,
        'COMMIT': staging_commit.hexsha,
    }
    jira_fields = {
        'project': 'CR',
        'summary': 'Blacklist update %s' % ticket,
        'description': CR_content,
        'issuetype': {'name': 'Change'},
        "customfield_13401": {
            "id": "11403",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11403",
            "value": "Minor / Localized"
        },
        "customfield_13402": {
            "id": "12030",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11405",
            "value": "Normal"
        },
        "customfield_13403": {
            "id": "11410",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11410",
            "value": "Low"
        },
        "customfield_13404": {
            "id": "11415",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11415",
            "value": "Other"
        },
        "customfield_13407": {
            "id": "11419",
            "self": "https://snappli.atlassian.net/rest/api/2/customFieldOption/11419",
            "value": "Low"
        },
    }
    new_issue = jira.create_issue(fields=jira_fields)
    jira.create_issue_link("Relates", new_issue, OPS_ticket)

    # Fabric to dev/qa/staging
    for env_name in ['qa', 'development', 'staging']:
        logging.info("Getting proxies for %s", env_name)
        hosts = instances.get_proxies(env_name, getip=True)
        logging.info("Deploying to %s", hosts)
        for host in hosts:
            try:
                with settings(host_string=host):
                    proxy.accessfileupdate.update_access_file("access_file_%s" % ticket)
            except Exception as e:
                logging.error("Impossible to update access for %s, skipping: %s", host, e)

    jira.add_comment(new_issue, '[~michael.grafton] Blacklist deployed On Non production envs')

    print "https://snappli.atlassian.net/browse/%s" % new_issue.key

def run():
    parser = argparse.ArgumentParser(description='Does lot of blacklist shit', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-l', '--level', help='Log Level',default='INFO', required=False, choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'])
    parser.add_argument('-p', '--puppet', help='Puppet directory', type=str, required=False, default='~/git/puppet')
    parser.add_argument('-t', '--ticket', help='Jira Ticket', type=str, required=True)

    args = parser.parse_args()

    logging.basicConfig(level=args.level, format='%(asctime)s %(levelname)s %(message)s')
    puppet_path = expanduser(args.puppet)

    do_blacklist_prep(args.ticket, puppet_path)

if __name__ == "__main__":
    run()
