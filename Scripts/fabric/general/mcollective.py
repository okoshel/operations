from fabric.api import get, run, sudo, task
from fabric.utils import abort

@task
def restart_mcollective():
    run("sudo service mcollective restart")

@task
def start_mcollective():
    run("sudo service mcollective start")

@task
def stop_mcollective():
    run("sudo service mcollective stop")
