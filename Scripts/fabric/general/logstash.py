from fabric.api import sudo, task, settings, run, warn_only
from fabric.contrib.files import exists

@task
def reload_logstash():
    with warn_only():
        run('cat /etc/hostname')
        if exists('/etc/init.d/logstash-shipper'):
            run("mkdir ~/logstashconf")
            sudo("mv /opt/logstash-shipper/conf.d/* ~/logstashconf", shell=False)
            sudo("pkill -f logstash-shipper", shell=False)
            with settings(ok_ret_codes=[0, 2]):
                sudo("puppet agent -t --tags logstash_shipper", shell=False)
