from fabric.api import sudo, task, settings, warn_only

@task
def clear_puppet_ssl():
    with settings(ok_ret_codes=[0, 2]):
        sudo("rm -rf $(puppet agent --configprint ssldir)")

@task
def cleanweakssh():
    with warn_only():
        sudo('sed -i.bak -e "s#,arcfour256,arcfour128##" /etc/ssh/sshd_config')
        sudo('service ssh reload')