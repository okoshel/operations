#!/usr/bin/ruby
# Ruby script to create events using the API.
#
# Copyright (c) 2014, PagerDuty, Inc. <info@pagerduty.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of PagerDuty Inc nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL PAGERDUTY INC BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

require 'json'

subdomain="wandera"
api_key="<REMOVED>"

time1 = Time.new

endpoint="https://events.pagerduty.com/generic/2010-04-15/create_event.json"
service_access_key="e429f997e7b24ae5ac999172292bf06a"
incident_key="pagerdutyCheckScript"
subject="Pagerduty Test Check"
details_hash={"Sent" => "#{time1}"} 

def curl_command_post_schedules(token_string,schedule_json,endpoint)
    curl_command='curl -H "Content-type: application/json" -X POST -d '+schedule_json+' "'+endpoint+'"'
end

def hash_to_json(hash)
  string=""
  hash.each do |key,value|
    string+="\"#{key}\": \"#{value}\", "
  end
  string
end
    
pager_details = details_hash.to_json

#puts pager_details.inspect

sched_json= '\'{ 
              "service_key": "'+service_access_key+'", 
              "event_type": "trigger", 
              "incident_key": "'+incident_key+'",
              "description": "'+subject+'",
              "details": '+pager_details+'
              }\''
#             "incident_key": "'+incident_key+'", 
#              "details": "'+pager_details+'"
#this can be included if you'd like to define the incident key; just remove the # to uncomment, and paste the line
#below the "service_key" line

sched_fix_json= '\'{ 
              "service_key": "'+service_access_key+'", 
              "incident_key": "'+incident_key+'",
              "event_type": "resolve", 
              "description": "'+subject+'",
              "details": '+pager_details+'
              }\''

curl_string = curl_command_post_schedules(api_key,sched_json,endpoint)
curl_fix_string = curl_command_post_schedules(api_key,sched_fix_json,endpoint)
puts curl_string
system(curl_string)

sleep(120)
puts curl_fix_string
system(curl_fix_string)

#def pipe_to_script(curl_string)
#  IO.popen(curl_string).each do |line|
#    parsed=JSON.parse(line)
#    puts parsed
#  end
#end
