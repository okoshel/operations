#!/bin/bash
# This script attempts to determine what version of software is running where.
# For now it will just check proxy.
proxyConListDev="176.34.100.174"
proxyConListStgn="54.228.243.118"
proxyConListLive="31.222.189.165 31.222.137.92 50.18.197.197 50.18.205.17"

proxyEntListDev="54.247.112.66"
proxyEntListStgn="31.222.165.249 31.222.163.131"
proxyEntListLive="95.138.171.192 31.222.165.53 54.241.154.78 54.241.135.241 54.251.152.131 54.251.151.104"

# List of apps and their locations
apnConListDev="54.228.242.149"
apnConListStgn="176.34.247.30"
apnConListLive="46.51.207.87 176.34.136.21"

apnEntListDev="54.247.100.33"
apnEntListStgn="176.34.254.79"
apnEntListLive="54.246.159.194 54.246.155.121"

aggEntListDev="54.247.100.33"
aggEntListStgn="176.34.254.79"
aggEntListLive="54.246.159.194 54.246.155.121"

elementConListDev="54.228.242.149"
elementConListStgn="176.34.247.30"
elementConListLive="46.51.207.87 176.34.136.21"

profileConListDev="54.228.242.149"
profileConListStgn="176.34.247.30"
profileConListLive="46.51.207.87 176.34.136.21"

profileEntListDev="54.247.100.33"
profileEntListStgn="176.34.254.79"
profileEntListLive="54.246.159.194 54.246.155.121"

proxyServiceConListDev="54.228.242.149"
proxyServiceConListStgn="176.34.247.30"
proxyServiceConListLive="46.51.207.87"

proxyServiceEntListDev="54.247.100.33"
proxyServiceEntListStgn="176.34.254.79"
proxyServiceEntListLive="54.246.159.194 54.246.155.121"


report2ConListDev="54.228.242.149"
report2ConListStgn="176.34.247.30"
report2ConListLive="46.51.207.87 176.34.136.21"

report3EntListDev="54.247.100.33"
report3EntListStgn="176.34.254.79"
report3EntListLive="54.246.159.194 54.246.155.121"

userConListDev="54.228.242.149"
userConListStgn="176.34.247.30"
userConListLive="46.51.207.87 176.34.136.21"

user2EntListDev="54.247.100.33"
user2EntListStgn="176.34.254.79"
user2EntListLive="54.246.159.194 54.246.155.121"

function proxy_consumer() {
    (echo >/dev/tcp/$1/22) &>/dev/null
    if [ $? -eq 0 ]; then
	version=`ssh -o StrictHostKeyChecking=no $1 "ls -l /opt/proxy | awk '{ print \\$11 }'"`;echo "<tr><td>$1:</td><td> $version</td></tr>"
    else
	echo "<tr><td>$1:</td><td> Not Available</td></tr>"
    fi
}
function proxy_enterprise() {
    (echo >/dev/tcp/$1/22) &>/dev/null
    if [ $? -eq 0 ]; then
	version=`ssh -o StrictHostKeyChecking=no $1 "dpkg -l rabbit-enterprise|grep enterprise | awk '{ print \\$3 }'"`;echo "<tr><td>$1:</td><td> $version</td></tr>"
    else
	echo "<tr><td>$1:</td><td> Not Available</td></tr>"
    fi
}

function software_consumer() {
    (echo >/dev/tcp/$1/22) &>/dev/null
    if [ $? -eq 0 ]; then
        version=`ssh -o StrictHostKeyChecking=no $1 "ls -l /opt/${2} | awk '{ print \\$11 }'"`;echo "<tr><td>$1:</td><td> $version</td></tr>"
    else
        echo "<tr><td>$1</td><td> Not Available</td></tr>"
    fi
}
function software_enterprise() {
    (echo >/dev/tcp/$1/22) &>/dev/null
    if [ $? -eq 0 ]; then
        version=`ssh -o StrictHostKeyChecking=no $1 "dpkg -l ${2}|grep ${2} | awk '{ print \\$3 }'"`;echo "<tr><td>$1:</td><td> $version</td></tr>"
    else
        echo "<tr><td>$1:</td><td> Not Available</td></tr>"
    fi
}

# Check the DEV proxy version
echo "<html>"
echo "<body>"
echo "<table>"
echo "<tr><td colspan=2><b>Dev Con Proxy Versions</b></td></tr>"
for host in $proxyConListDev; do proxy_enterprise $host; done
echo "<tr><td colspan=2><b>Stgn Con Proxy Versions</b></td></tr>"
for host in $proxyConListStgn; do proxy_enterprise $host; done
echo "<tr><td colspan=2><b>Live Con Proxy Versions</b></td></tr>"
for host in $proxyConListLive; do proxy_consumer $host; done
echo "<tr><td colspan=2><b>Dev Ent Proxy Versions</b></td></tr>"
for host in $proxyEntListDev; do proxy_enterprise $host; done
echo "<tr><td colspan=2><b>Stgn Ent Proxy Versions</b></td></tr>"
for host in $proxyEntListStgn; do proxy_enterprise $host; done
echo "<tr><td colspan=2><b>Live Ent Proxy Versions</b></td></tr>"
for host in $proxyEntListLive; do proxy_enterprise $host; done
echo "</table>"
echo "<br/>"
echo "<table>"
echo "<tr><td colspan=2><b>Dev Con APN Versions</b></td></tr>"
for host in $apnConListDev; do software_consumer $host apnservice; done
echo "<tr><td colspan=2><b>Stgn Con APN Versions</b></td></tr>"
for host in $apnConListStgn; do software_enterprise $host apnservice-consumer; done
echo "<tr><td colspan=2><b>Live Con APN Versions</b></td></tr>"
for host in $apnConListLive; do software_consumer $host apnservice; done
echo "<tr><td colspan=2><b>Dev Ent APN Versions</b></td></tr>"
for host in $apnEntListDev; do software_enterprise $host apnservice-enterprise; done
echo "<tr><td colspan=2><b>Stgn Ent APN Versions</b></td></tr>"
for host in $apnEntListStgn; do software_enterprise $host apnservice-enterprise; done
echo "<tr><td colspan=2><b>Live Ent APN Versions</b></td></tr>"
for host in $apnEntListLive; do software_enterprise $host apnservice-enterprise; done
echo "</table>"
echo "<br/>"
echo "<table>"
echo "<tr><td colspan=2><b>Dev Ent Agg Versions</b></td></tr>"
for host in $aggEntListDev; do software_enterprise $host aggregationservice-enterprise; done
echo "<tr><td colspan=2><b>Stgn ENt Agg Versions</b></td></tr>"
for host in $aggEntListStgn; do software_enterprise $host aggregationservice-enterprise; done
echo "<tr><td colspan=2><b>Live Ent Agg Versions</b></td></tr>"
for host in $aggEntListLive; do software_enterprise $host aggregationservice-enterprise; done
echo "</table>"
echo "<br/>"
echo "<table>"
echo "<tr><td colspan=2><b>Dev Con ELEMENT Versions</b></td></tr>"
for host in $elementConListDev; do software_consumer $host elementservice; done
echo "<tr><td colspan=2><b>Stgn Con ELEMENT Versions</b></td></tr>"
for host in $elementConListStgn; do software_enterprise $host elementservice-consumer; done
echo "<tr><td colspan=2><b>Live Con ELEMENT Versions</b></td></tr>"
for host in $elementConListLive; do software_consumer $host elementservice; done
echo "</table>"
echo "<br/>"
echo "<table>"
echo "<tr><td colspan=2><b>Dev Con Profile Versions</b></td></tr>"
for host in $profileConListDev; do software_consumer $host profileservice; done
echo "<tr><td colspan=2><b>Stgn Con Profile Versions</b></td></tr>"
for host in $profileConListStgn; do software_enterprise $host profileservice-consumer; done
echo "<tr><td colspan=2><b>Live Con Profile Versions</b></td></tr>"
for host in $profileConListLive; do software_consumer $host profileservice; done
echo "<tr><td colspan=2><b>Dev Ent Profile Versions</b></td></tr>"
for host in $profileEntListDev; do software_enterprise $host profileservice2-enterprise; done
echo "<tr><td colspan=2><b>Stgn Ent Profile Versions</b></td></tr>"
for host in $profileEntListStgn; do software_enterprise $host profileservice2-enterprise; done
echo "<tr><td colspan=2><b>Live Ent Profile Versions</b></td></tr>"
for host in $profileEntListLive; do software_enterprise $host profileservice2-enterprise; done
echo "</table>"
echo "<br/>"
echo "<table>"
echo "<tr><td colspan=2><b>Dev Con Proxy Service Versions</b></td></tr>"
for host in $proxyServiceConListDev; do software_consumer $host proxyservice; done
echo "<tr><td colspan=2><b>Stgn Con Proxy Service Versions</b></td></tr>"
for host in $proxyServiceConListStgn; do software_enterprise $host proxyservice-consumer; done
echo "<tr><td colspan=2><b>Live Con Proxy Service Versions</b></td></tr>"
for host in $proxyServiceConListLive; do software_consumer $host proxyservice; done
echo "<tr><td colspan=2><b>Dev Ent Proxy Service Versions</b></td></tr>"
for host in $proxyServiceEntListDev; do software_enterprise $host proxyservice-enterprise; done
echo "<tr><td colspan=2><b>Stgn Ent Proxy Service Versions</b></td></tr>"
for host in $proxyServiceEntListStgn; do software_enterprise $host proxyservice-enterprise; done
echo "<tr><td colspan=2><b>Live Ent Proxy Service Versions</b></td></tr>"
for host in $proxyServiceEntListLive; do software_enterprise $host proxyservice-enterprise; done
echo "</table>"
echo "<br/>"
echo "<table>"
echo "<tr><td colspan=2><b>Stgn Con REPORT2 Versions</b></td></tr>"
for host in $report2ConListStgn; do software_enterprise $host reportservice-consumer; done
echo "<tr><td colspan=2><b>Live Con REPORT2 Versions</b></td></tr>"
for host in $report2ConListLive; do software_consumer $host reportservice2; done
echo "</table>"
echo "<br/>"
echo "<table>"
echo "<tr><td colspan=2><b>Dev Ent REPORT3 Versions</b></td></tr>"
for host in $report3EntListDev; do software_enterprise $host reportservice3-enterprise; done
echo "<tr><td colspan=2><b>Stgn Ent REPORT3 Versions</b></td></tr>"
for host in $report3EntListStgn; do software_enterprise $host reportservice3-enterprise; done
echo "<tr><td colspan=2><b>Live Ent REPORT3 Versions</b></td></tr>"
for host in $report3EntListLive; do software_enterprise $host reportservice3-enterprise; done
echo "</table>"
echo "<br/>"
echo "<table>"
echo "<tr><td colspan=2><b>Dev Con USER Versions</b></td></tr>"
for host in $userConListDev; do software_consumer $host userservice; done
echo "<tr><td colspan=2><b>Stgn Con USER Versions</b></td></tr>"
for host in $userConListStgn; do software_enterprise $host userservice-consumer; done
echo "<tr><td colspan=2><b>Live Con USER Versions</b></td></tr>"
for host in $userConListLive; do software_consumer $host userservice; done
echo "</table>"
echo "<br/>"
echo "<table>"
echo "<tr><td colspan=2><b>Dev Ent USER2 Versions</b></td></tr>"
for host in $user2EntListDev; do software_enterprise $host userservice2-enterprise; done
echo "<tr><td colspan=2><b>Stgn Ent USER2 Versions</b></td></tr>"
for host in $user2EntListStgn; do software_enterprise $host userservice2-enterprise; done
echo "<tr><td colspan=2><b>Live Ent USER2 Versions</b></td></tr>"
for host in $user2EntListLive; do software_enterprise $host userservice2-enterprise; done
echo "</table>"
echo "<br/>"
echo "</body>"
echo "</html>"