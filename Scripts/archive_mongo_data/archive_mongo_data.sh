#!/bin/bash

MAILTO=ops@snappli.com

if [ "$#" -ne "2" ]; then
	date1=`date +%Y%m%d -d "yesterday"` 
	date2=`date +%Y%m%d -d "today"`
else
	date1=$1
	date2=$2
fi

	if [ "$date1" -ge "$date2" ]; then
	 echo "The end time can not be before the start time."
	 exit 1
	fi


mongo_host="ds037517-a1.mongolab.com:37517"
mongo_database="snappli_live_eu_logs"
mongo_user="mongo_backup"
mongo_password="Mong05avem3"
filename=traffic_logs_export


#verify dates
if ! date -d "$date1" 2>&1 > /dev/null ; 
    then echo "first date is invalid" ; exit 1
fi
if ! date -d "$date2" 2>&1 > /dev/null ; 
    then echo "second date is invalid" ; exit 1
fi

#set current and end date
current=$(date +%Y%m%d -d "$date1")
end=$(date +%Y%m%d -d "$date2")
partition=$(date +%Y-%m-%d -d "$date1")

#loop over all dates
until [ "$end" == "$current" ]
do
	echo $current
	current_time=`date +%Y-%m-%d -d "${current}"`
	current_start_time_ns=$(($(date +%s%N -d "${current_time}T00:00:00Z")/1000000))
	current_end_time_ns=$(($(date +%s%N -d "${current_time}T23:59:59Z")/1000000))
	echo "`date '+%Y-%m-%dT%H:%M:%S'`: Export Started"
	
	echo "Start: $current_start_time_ns End: $current_end_time_ns"
	start_s=`date +%s`
mongoexport -h ds037517-a1.mongolab.com:37517 -d snappli_live_eu_logs -c proxy_logs -u ${mongo_user} -p ${mongo_password} --query "{startUtcInMs: {\$gte : $current_start_time_ns , \$lte: $current_end_time_ns}}" > ${filename}-${current}.json

	if [ $? -ne 0 ]; then
		echo "`date '+%Y-%m-%dT%H:%M:%S'`: Export Failed"
		exit 1
	else
		echo "`date '+%Y-%m-%dT%H:%M:%S'`: Export complete"
	fi
	end_s=`date +%s`
	export_time_m=$(((${end_s}-${start_s})/60))
	export_time_s=$((${end_s}-${start_s}))
#	lzop -o ${filename}-${current}.json.lzop ${filename}-${current}.json
	gzip ${filename}-${current}.json
	if [ $? -ne 0 ]; then
		echo "`date '+%Y-%m-%dT%H:%M:%S'`: Compression failed"
		exit 1
	else
		echo "`date '+%Y-%m-%dT%H:%M:%S'`: Compression complete"
	fi

# Get a MD5 so we can check the upload later on.
#fileMD5=`md5sum ${filename}-${current}.json.lzop |awk '{ print $1 }'`
fileMD5=`md5sum ${filename}-${current}.json.gz |awk '{ print $1 }'`

partition=$(date +%Y-%m-%d -d "${current}")

# Lets perform the S3 put for the service.
s3cmd --check-md5 --no-progress put --guess-mime-type ${filename}-${current}.json.gz s3://snappli-traffic-data-archived/backup/dt\=${partition}/${filename}-${current}.json.gz

# Get the MD5 of the file we uploaded so we can check if it matches the original.
s3cmd --check-md5 --no-progress get --guess-mime-type s3://snappli-traffic-data-archived/backup/dt\=${partition}/${filename}-${current}.json.gz ${filename}-${current}.json.gz.compare
s3FileMD5=`md5sum ${filename}-${current}.json.gz.compare | awk '{print $1}'`

if [ "$fileMD5" != "$s3FileMD5" ]; then
	echo -e "Upload did not work for s3://snappli-traffic-data-archived/backup/dt\=${partition}/${filename}-${current}.json.gz\nExport time: $export_time_m minutes or $export_time_s seconds" | mail $MAILTO -s "Snappli Traffic Data Archiving - Failed"
	echo "`date '+%Y-%m-%dT%H:%M:%S'`: Upload Failed"
	rm -rf ${filename}-${current}.json.gz
	rm -rf ${filename}-${current}.json.gz.compare
else
	echo -e "Upload worked for s3://snappli-traffic-data-archived/backup/dt\=${partition}/${filename}-${current}.json.gz\nExport time: $export_time_m minutes or $export_time_s seconds"| mail $MAILTO -s "Snappli Traffic Data Archiving - Completed"
	echo "`date '+%Y-%m-%dT%H:%M:%S'`: Upload Complete"
	rm -rf ${filename}-${current}.json.gz
	rm -rf ${filename}-${current}.json.gz.compare
fi
    current=$(date +%Y%m%d -d "$current +1 days")
done