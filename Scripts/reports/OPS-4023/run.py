import csv
import itertools
from datetime import datetime, timedelta
from time import gmtime, strftime

from pymongo import MongoClient

uri = 'mongodb://username:password' \
      '@mdb-101-core.eu-west-1a.ie.wandera.com:22000,' \
      'mdb-201-core.eu-west-1b.ie.wandera.com:22000' \
      '/?authSource=admin&replicaSet=rs22000'
db = MongoClient(uri, readPreference='secondaryPreferred').wandera_live_ent_eu

cutoff = ((datetime.now() - timedelta(days=3)) - datetime.utcfromtimestamp(0)).total_seconds() * 1000.0
pipeline = [
    {
        '$match': {
            'proxy': {'$exists': True},
        }
    },
    {
        '$project': {
            'proxyName': '$proxy.hostWithoutPortPrefix',
            'deleted': {'$cond': [{'$and': [{'$lte': ['$traffic.lastTrafficRun', cutoff]}, {'$eq': ['$deleted', True]}]}, 1, 0]},
            'deletedActive': {'$cond': [{'$and': [{'$gt': ['$traffic.lastTrafficRun', cutoff]}, {'$eq': ['$deleted', True]}]}, 1, 0]},
            'active': {'$cond': [{'$and': [{'$gt': ['$traffic.lastTrafficRun', cutoff]}, {'$eq': ['$deleted', False]}]}, 1, 0]},
            'freed': {'$cond': [{'$eq': ['$proxy.freed', True]}, 1, 0]},
        }
    },
    {
        '$lookup':
            {
                'from': 'nodes',
                'localField': 'proxyName',
                'foreignField': 'proxyName',
                'as': 'node'
            }
    },
    {
        '$project': {
            'proxyName': 1,
            'deleted': 1,
            'deletedActive': 1,
            'active': 1,
            'freed': 1,
            'nodeName': '$node.nodeName',
            'datacenter': '$node.dataCenter',
            'type': {
                '$cond': {
                    'if': {'$eq': ['$node.supportedProfileTypes', [['apn', 'cellular_app_plus']]]},
                    'then': 'Cellular',
                    'else': {
                        '$cond': {
                            'if': {'$eq': ['$node.supportedProfileTypes', [['ghp', 'vpn', 'cellular_wifi_app_plus', 'wifi_app_plus']]]},
                            'then': 'Wifi',
                            'else': 'Unknown'
                        }
                    }
                }
            },
            'capacity': '$node.capacity'
        }
    },
    {
        '$group': {
            '_id': {'proxyName': '$proxyName', 'nodeName': '$nodeName',
                    'dataCenter': '$datacenter', 'capacity': '$capacity',
                    'type': '$type'},
            'deleted': {'$sum': '$deleted'},
            'deletedActive': {'$sum': '$deletedActive'},
            'active': {'$sum': '$active'},
            'freed': {'$sum': '$freed'},
            'allocated': {'$sum': 1}
        }
    },
    {'$unwind': "$_id.nodeName"},
    {'$unwind': "$_id.dataCenter"},
    {'$unwind': "$_id.capacity"},
    {'$unwind': "$_id.type"},
    {
        '$project': {
            '_id': 0,
            'Proxy Name': '$_id.proxyName',
            'Node Name': '$_id.nodeName',
            'Datacenter': '$_id.dataCenter',
            'Deleted': '$deleted',
            'Deleted Active': '$deletedActive',
            'Active': '$active',
            'Freed': '$freed',
            'Allocated': '$allocated',
            'Capacity': '$_id.capacity',
            'Unallocated': {'$subtract': ['$_id.capacity', '$allocated']},
            'Type': '$_id.type',
        }
    },
]

results = db.userDevice.aggregate(pipeline)

with open('{}.csv'.format(strftime('%Y-%m-%d-%H%M', gmtime())), 'w') as csvfile:
    peek = next(results)
    fieldnames = peek.keys()
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    for result in itertools.chain([peek], results):
        writer.writerow(result)

