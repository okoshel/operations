from flask import Flask, request, jsonify
import json, requests
app = Flask(__name__)


slackpayload = {"username": "Graylog-Bot", "icon_emoji": ":crystal_ball:"}

@app.route('/')
def hello_world():
    return 'Graylog 2 Slack Reporter'

@app.route('/error-alert', methods=['POST'])
def erroralert():
    streamurlvars = 'messages?q=%2A&rangetype=relative&relative=300&width=1440'
    alertresponse = request.get_json(force=True, silent=False)
    streamurl = 'https://logs.wandera.net/streams/' + alertresponse['stream']['id'] + '/' + streamurlvars
    slacktext = 'ERROR: ' + alertresponse['stream']['title'] + ' visit the stream' + ' <' + streamurl + '|here> for more details.'
    slackdata = {"text": slacktext, "username": slackpayload['username'], "icon_emoji": slackpayload['icon_emoji'] }
    slackreq = requests.post('https://hooks.slack.com/services/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', json=slackdata)
    return jsonify(slackdata)
 

@app.route('/warning-alert', methods=['POST'])
def warningalert():
    streamurlvars = 'messages?q=%2A&rangetype=relative&relative=300&width=1440'
    alertresponse = request.get_json(force=True, silent=False)
    streamurl = 'https://logs.wandera.net/streams/' + alertresponse['stream']['id'] + '/' + streamurlvars
    slacktext = 'WARNING: ' + alertresponse['stream']['title'] + ' visit the stream' + ' <' + streamurl + '|here> for more details.'
    slackdata = {"text": slacktext, "username": slackpayload['username'], "icon_emoji": slackpayload['icon_emoji'] }
    slackreq = requests.post('https://hooks.slack.com/services/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', json=slackdata)
    return jsonify(slackdata)


@app.route('/critical-alert', methods=['POST'])
def criticalalert():
    streamurlvars = 'messages?q=%2A&rangetype=relative&relative=300&width=1440'
    alertresponse = request.get_json(force=True, silent=False)
    streamurl = 'https://logs.wandera.net/streams/' + alertresponse['stream']['id'] + '/' + streamurlvars
    slacktext = 'CRITICAL: ' + alertresponse['stream']['title'] + ' visit the stream' + ' <' + streamurl + '|here> for more details.'
    slackdata = {"text": slacktext, "username": slackpayload['username'], "icon_emoji": slackpayload['icon_emoji'] }
    slackreq = requests.post('https://hooks.slack.com/services/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', json=slackdata)
    return jsonify(slackdata)


if __name__ == '__main__':
    app.debug = True
    app.run()

