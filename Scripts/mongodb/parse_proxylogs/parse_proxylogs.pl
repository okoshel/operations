#!/usr/bin/perl
use strict;
use warnings;

# export the proxy_logs collection and use this script to remove all of the oids so entries can be reimported multiple times.

my $filename = "proxy_logs.json";
open( my $fh, '<', $filename ) or die "Can't open $filename: $!";
    while ( my $line = <$fh> ) {
	$line =~ s/("_id" : { "\$oid" : ".*" }, )//;
            print $line;
    }
close $fh;