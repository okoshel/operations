#!/usr/bin/ruby

require "rubygems"
require 'json'
require 'fastercsv'

IO.foreach('traffic_logs_export-20120702.json') do |line|

input = JSON.parse(line)

transform = [input['_id']['$oid'],input['status'],input['dataTransferred'],input['sizeReduction'],input['requestHeaderSize'],input['responseHeaderSize'],input['bytesReadUpstream'],input['interrupted'],input['connectionType'],input['request'],input['method'],input['httpVersion'],input['address'],input['user'],input['port'],input['guid'],input['date'],input['startUtcInMs'],input['endUtcInMs'],input['contentLength'],input['originalContentLength'],input['type'],input['extra'],input['chunk'],input['cache'],input['userAgent'],input['app'],input['duration'],input['site'],input['tld'],input['referer'],input['refererSite']]

csv_str = FasterCSV.generate(:col_sep => "\t") do |csv|
  csv << transform
end
puts csv_str

end
