#!/bin/bash

DATE=$(date +"%Y-%m-%d")
LOGFILE="/var/log/$DATE-ops-filter-update.log"

if [ $1 = "node" ]; then
    ADMINPORT="6998"
    PORT="6999"
else
    ADMINPORT="3998"
    PORT="3999"
fi

if [ ! -f $LOGFILE ]; then
    echo "Log File Not Found!";
    touch $LOGFILE;
    chmod ugo+w $LOGFILE;
fi

# This is the filterservice dirs to clean and use.
declare -a DIRS=(/opt/filterservice/etc/security/Phishtank/
                 /opt/filterservice/etc/security/sophos/illegal/
                 /opt/filterservice/etc/security/sophos/malware/
                 /opt/filterservice/etc/security/sophos/spam/)

for i in "${DIRS[@]}"
do
    if [ "$(sudo ls -A $i)" ]; then
        echo "$(date +"%Y-%m-%d-%H-%M-%S") WARN: $i is not Empty. Clearing Dir." >> $LOGFILE; 
        rm $i/*;
    else
        echo "$(date +"%Y-%m-%d-%H-%M-%S") INFO: $i is Empty." >> $LOGFILE;
    fi
done

s3cmd get s3://static.secure.wandera.com/tmp/semijb.domain-ops-1009 /opt/filterservice/etc/security/Wandera/semijb.domain;
echo "$(date +"%Y-%m-%d-%H-%M-%S") INFO: Copied semijb file." >> $LOGFILE;
chown fsvc:fsvc /opt/filterservice/etc/security/Wandera/semijb.domain;

curl -vvv -X POST -w %{time_connect}:%{time_starttransfer}:%{time_total} "http://127.0.0.1:$ADMINPORT/tasks/LoadDataTask" >> $LOGFILE;
curl -vvv -X GET -w %{time_connect}:%{time_starttransfer}:%{time_total} "http://127.0.0.1:$PORT/filter/v1/analyser/?sourceIp=1.0.0.0&destinationIp=2.0.1.11&uri=m.pangu8.com" >> $LOGFILE;
curl -vvv -X GET -w %{time_connect}:%{time_starttransfer}:%{time_total} "http://127.0.0.1:$PORT/filter/v1/analyser/?sourceIp=1.0.0.0&destinationIp=2.0.1.11&uri=http://m.pangu8.com/app/83/" >> $LOGFILE;
curl -vvv -X GET -w %{time_connect}:%{time_starttransfer}:%{time_total} "http://127.0.0.1:$PORT/filter/v1/analyser/?sourceIp=1.0.0.0&destinationIp=2.0.1.11&uri=http://m.pangu8.com/app/82/" >> $LOGFILE;

rm /opt/filterservice/etc/security/Wandera/semijb.domain;
echo "$(date +"%Y-%m-%d-%H-%M-%S") INFO: Removed semijb file." >> $LOGFILE