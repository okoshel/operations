class AWSRedShiftLinesLoaded < Scout::Plugin
  # need the ruby-pg gem
  needs 'pg'

  OPTIONS=<<-EOS
    user:
      name: PostgreSQL username
      notes: Specify the username to connect with
      default: "logging_data"
    password:
      name: PostgreSQL password
      notes: Specify the password to connect with
      attributes: password
      default: "LogDat4!f0rPr0FiT"
    host:
      name: PostgreSQL host
      notes: Specify the host name of the PostgreSQL server. If the value begins with
              a slash it is used as the directory for the Unix-domain socket. An empty
              string uses the default Unix-domain socket.
      default: "live-eu-west-1-a-proxylogs-1.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com"
    dbname:
      name: Database
      notes: The database name to monitor
      default: "loggingdata"
    port:
      name: PostgreSQL port
      notes: Specify the port to connect to PostgreSQL with
      default: "5439"
  EOS

  def build_report
    report = {}

    begin

# Here comes the time conversion from epoch to redshift epoch (1/1/2000) in microsecond
$time_now = (Time.now - 60).strftime("%Y-%m-%d %H:%M:%S")
$time_past = (Time.now - 120).strftime("%Y-%m-%d %H:%M:%S")

    pgconn = PGconn.connect(:host=>option(:host), :user=>option(:user), :password=>option(:password), :port=>option(:port).to_i, :dbname=>option(:dbname))
	startTime = Time.now.to_f
        result = pgconn.exec("select SUM(lines), SUM(bytes), COUNT(QUERY) from stl_file_scan where curtime > '#$time_past' and curtime < '#$time_now';")
	endTime = Time.now.to_f
	queryTime = endTime - startTime
		report(:lines_loaded=>result.getvalue(0,0), :bytes_loaded=>result.getvalue(0,1), :files_loaded=>result.getvalue(0,2) )

    rescue PGError => e
      return errors << {:subject => "Unable to connect to PostgreSQL.",
                       :body => "Scout was unable to connect to the PostgreSQL server: \n\n#{e}\n\n#{e.backtrace}"}
    end
    report(report) if report.values.compact.any?
  end
end
