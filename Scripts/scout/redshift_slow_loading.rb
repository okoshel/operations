class AWSRedShiftSlowCheck < Scout::Plugin
  # need the ruby-pg gem
  needs 'pg'

  OPTIONS=<<-EOS
    user:
      name: PostgreSQL username
      notes: Specify the username to connect with
      default: "logging_data"
    password:
      name: PostgreSQL password
      notes: Specify the password to connect with
      attributes: password
      default: "LogDat4!f0rPr0FiT"
    host:
      name: PostgreSQL host
      notes: Specify the host name of the PostgreSQL server. If the value begins with
              a slash it is used as the directory for the Unix-domain socket. An empty
              string uses the default Unix-domain socket.
      default: "live-eu-west-1-a-proxylogs-1.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com"
    dbname:
      name: Database
      notes: The database name to monitor
      default: "loggingdata"
    port:
      name: PostgreSQL port
      notes: Specify the port to connect to PostgreSQL with
      default: "5439"
  EOS

  def build_report
    report = {}

    begin

# Here comes the time conversion from epoch to redshift epoch (1/1/2000) in microsecond
$redshift_epoch = (Time.utc("2000,0,0,0,0,0")).strftime("%s")
$time_now = (Time.now).strftime("%s")
$time_past = (Time.now - 600).strftime("%s")

$redshift_now = (($time_now.to_i - $redshift_epoch.to_i)*1000000)
$redshift_past = (($time_past.to_i - $redshift_epoch.to_i)*1000000)

# Slow loading cutoff in seconds
$loading_cutoff = 2

# Convert the cutoff time to microseconds.
$loading_cutoff_mic = ($loading_cutoff.to_i * 1000000)

    pgconn = PGconn.connect(:host=>option(:host), :user=>option(:user), :password=>option(:password), :port=>option(:port).to_i, :dbname=>option(:dbname))
	startTime = Time.now.to_f
        result = pgconn.exec("select count(*) from  stl_s3client where http_method='GET' and start_time >= '#$redshift_past' and end_time <= '#$redshift_now' and transfer_time >= '#$loading_cutoff_mic';")
	endTime = Time.now.to_f
	queryTime = endTime - startTime
		report(:slow_loads=>result.getvalue(0,0))

    rescue PGError => e
      return errors << {:subject => "Unable to connect to PostgreSQL.",
                       :body => "Scout was unable to connect to the PostgreSQL server: \n\n#{e}\n\n#{e.backtrace}"}
    end
    report(report) if report.values.compact.any?
  end
end
