class ConnectorCheck < Scout::Plugin
    needs 'openssl'
    needs 'rest-client'
    needs 'base64'
    needs 'pp'

  OPTIONS=<<-EOS
    customerid:
      name: Customerid to check
      notes: Specify the customerid to check for last sync
      default: "d6add3a5-68ae-4a91-8af4-39786895ca0e"
    endpoint:
      name: Uri of connector
      notes: Specify the URI of the connector to be checked
      default: "gufaSt6chaspaCHU.emc.eu-west-1b.ie.wandera.com"
  EOS

  def build_report
    report = {}

    begin


#    USER_ENDPOINT = option(:endpoint)
#    CUSTOMERID = option(:customerid)

    @api_key = "645895b7-1449-45d4-be39-b17391843b1a"
    @signing_key = "REMOVED"

  def get_connector_status
    path    = "/v2/customers/#{option(:customerid)}/status"
    url     = "#{option(:endpoint)}#{path}"
    headers = auth_headers(path)
    body    = ""
    response = RestClient.get url, headers

  begin
    response = RestClient.get url, headers
  rescue => e
    e.response
  end
    return JSON.parse(response)
  end


  def auth_headers path
    x_ts  = (Time.now.to_f * 1000.0).to_i
    data      = "ts=#{x_ts};path=#{path};"
    digest    = OpenSSL::Digest::Digest.new( 'sha1' )
    signature = OpenSSL::HMAC.digest( digest, @signing_key, data )
    x_sig     = Base64.encode64(signature).chomp


    {
      'X-Key' => @api_key,
      'X-Sig' => x_sig,
      'X-TS'  => x_ts,
      'Content-Type' => 'application/json',
      'Accept' => 'application/json'

    }

  end

  def convert_time(updateTime)
    #puts updateTime
    current_time = Time.now.to_i * 1000
    wanderaLast = ((current_time - updateTime)/1000)/60
  return wanderaLast
  end

  customer_response = get_connector_status.to_hash
  wanderaUpdatedAtUtcMs = convert_time(customer_response["wanderaUpdatedAtUtcMs"])
  #puts "wanderaUpdatedAtUtcMs: #{wanderaUpdatedAtUtcMs}"
  cacheUpdatedAtUtcMs = convert_time(customer_response["cacheUpdatedAtUtcMs"])
  #puts "cacheUpdatedAtUtcMs: #{cacheUpdatedAtUtcMs}"

    report(:cacheUpdatedAtUtcMs=>cacheUpdatedAtUtcMs, :wanderaUpdatedAtUtcMs=>wanderaUpdatedAtUtcMs)


    rescue => e
      return errors << {:subject => "Unable to check connector.",
                       :body => "Scout was check the connector server server: \n\n#{e}\n\n#{e.backtrace}"}
    end
    report(report) if report.values.compact.any?
  end
end
