$VERBOSE=false
class ProxyPortAvailabilityCheck < Scout::Plugin
  OPTIONS=<<-EOS
    host:
      name: Mongo Server
      notes: Where mongodb is running. 
      default: localhost
    username:
      notes: Leave blank unless you have authentication enabled. 
      attributes: advanced
    password:
      notes: Leave blank unless you have authentication enabled. 
      attributes: advanced,password
    port:
      name: Port
      default: 27017
      notes: MongoDB standard port is 27017.
    connect_timeout:
      name: Connect Timeout
      notes: The number of seconds to wait before timing out a connection attempt.
      default: 30
      attributes: advanced
    threshold:
      name: Threshold to alert on
      default: 75
      notes: The threshold of used ports to alert on.
  EOS

  needs 'mongo'

  def option_to_f(op_name)
    opt = option(op_name)
    opt.nil? ? opt : opt.to_f
  end

  def build_report 

    # check if options provided
    @host     = option('host') 
    @port     = option('port')
    @ssl      = option("ssl").to_s.strip == 'true'
    if [@host,@port].compact.size < 2
      return error("Connection settings not provided.", "The host and port must be provided in the settings.")
    end
    @username = option('username')
    @password = option('password')
    @connect_timeout = option_to_f('connect_timeout')
    @threshold = option('threshold').to_i

    begin
      connection = Mongo::Connection.new(@host,@port,:ssl=>@ssl,:slave_ok=>true,:connect_timeout=>@connect_timeout)
    rescue Mongo::ConnectionFailure
      return error("Unable to connect to the MongoDB Daemon.","Please ensure it is running on #{@host}:#{@port}\n\nException Message: #{$!.message}, also confirm if SSL should be enabled or disabled.")
    end
    
    # Connect to the database
    @nodes_db = connection.db('wandera_live_ent_eu')
    @nodes_db.authenticate(@username,@password) unless @username.nil?
    get_proxy_status
  end
 
  def count_available_ports(nodeName,capacity)
    proxies_collection = @nodes_db.collection("proxies")
#    db.proxies.find({'host' : {'$regex' :'a91b1bc4e52d4138.proxy.wandera.com'},'available' : true })
    proxies_count = proxies_collection.find({'host' => {'$regex' => "#{nodeName}"},'available' => false }).count() 
    
    available = ((proxies_count.to_f / capacity.to_f)*100).round(0)
    return available
  end

 
  def get_proxy_status
    # db.nodes.find({'proxyName':{'$exists':true}})
    node_collection = @nodes_db.collection("nodes")
    node_list = node_collection.find({'proxyName' => {'$exists' => true}})
    
    unless node_list.count >= 1
      return error("Error Message:\n\n#{node_list['errmsg']}")
    end

    node_list.each do |node|
      proxyName = node["proxyName"].to_s
      capacity = node["capacity"].to_i
      availability = count_available_ports(proxyName,capacity)
        
      #puts availability
      if availability >= @threshold
   #   report(:name => proxyName,:usage => availability)
       alert("Proxy Availability Check Alert", "The proxy #{proxyName} has breached the port usage threshold of #{@threshold} and is now at #{availability} of capacity.")
      end
    end 
    
  end  
end

