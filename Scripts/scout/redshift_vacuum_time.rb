class AWSRedShiftVacuumTime < Scout::Plugin
  # need the ruby-pg gem
  needs 'pg'

  OPTIONS=<<-EOS
    user:
      name: PostgreSQL username
      notes: Specify the username to connect with
      default: "logging_data"
    password:
      name: PostgreSQL password
      notes: Specify the password to connect with
      attributes: password
      default: "LogDat4!f0rPr0FiT"
    host:
      name: PostgreSQL host
      notes: Specify the host name of the PostgreSQL server. If the value begins with
              a slash it is used as the directory for the Unix-domain socket. An empty
              string uses the default Unix-domain socket.
      default: "live-eu-west-1-a-proxylogs-1.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com"
    dbname:
      name: Database
      notes: The database name to monitor
      default: "loggingdata"
    port:
      name: PostgreSQL port
      notes: Specify the port to connect to PostgreSQL with
      default: "5439"
  EOS

  def build_report
    report = {}

    begin

# Here comes the time conversion from epoch to redshift epoch (1/1/2000) in microsecond
t = Time.now
$time_before = Time.at(t.to_i/(5*60)*(5*60)-60).strftime("%Y-%m-%d %H:%M:%S")
$time_after = Time.at(t.to_i/(5*60)*(5*60)+60).strftime("%Y-%m-%d %H:%M:%S")

    pgconn = PGconn.connect(:host=>option(:host), :user=>option(:user), :password=>option(:password), :port=>option(:port).to_i, :dbname=>option(:dbname))
	startTime = Time.now.to_f
        result = pgconn.exec("select xid from stl_query where querytxt like 'Vacuum:%' and endtime > '#$time_before' and endtime < '#$time_after' order by endtime asc limit 1;")
	$xid_lookup = result.getvalue(0,0).to_i
        time = pgconn.exec("SELECT EXTRACT('epoch' FROM (o.endtime - f.starttime)) AS diff_in_sek FROM (SELECT * FROM stl_query WHERE xid = '#$xid_lookup' order by starttime desc limit 1) o JOIN  (SELECT * FROM stl_query WHERE xid = '#$xid_lookup' order by starttime asc limit 1) f USING (xid,pid) order by 1,1;")
	endTime = Time.now.to_f
	queryTime = endTime - startTime
		report(:vacuum_time=>time.getvalue(0,0))

    rescue PGError => e
      return errors << {:subject => "Unable to connect to PostgreSQL.",
                       :body => "Scout was unable to connect to the PostgreSQL server: \n\n#{e}\n\n#{e.backtrace}"}
    end
    report(report) if report.values.compact.any?
  end
end
