import requests
import sys
import argparse
import json

sign_url = 'https://api.eu1.echosign.com/api/rest/v5/widgets'
wid_url = 'https://wandera.eu1.echosign.com/public/esignWidget?wid'
access_token = '3AAABLblqZhCl750oqxTuwjSPGB3oO9ZQ-j120MM3vP8U8pZ_DAns-v4MjvtX7_GYDMKgZUAgVPJm90GmLSgoOWpNqgICAeBF'

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('--wid', dest = "wid", help='wid in the URL obtained via the UI')
    parser.add_argument('--name', dest = "name", help='Name of the widget')

    return parser.parse_args()

if __name__ == '__main__':
    options = parse_options(sys.argv[1:])

    if options.wid is None and options.name is None:
        sys.exit("ERROR: Missing wid or widget name")

    wid = None
    wname = None
    if options.wid is not None:
        wid = options.wid
        wid_url = "{}={}".format(wid_url,wid)
    else:
        wname = options.name

    widgets = requests.get(sign_url,headers={ 'access-token' : access_token })

    for widget in widgets.json()['userWidgetList']:
        if (wid is not None and widget['url'] == wid_url) or widget['name'] == wname:
            widgetId = widget['widgetId']
            widget_info = requests.get("{}/{}".format(sign_url,widgetId),headers={ 'access-token' : access_token })
            print "Name: {}\nWidgetId: {}\nDocumentId: {}".format(widget['name'],widgetId, widget_info.json()['latestVersionId'])
            sys.exit(0)
    print "Widget {} not found".format(sys.argv[1:])
