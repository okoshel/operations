#
# Usage:
# dns_helper.py --env=staging --setLiveStack=ELB-A
#
import yaml
import time
import boto3
# import string
import argparse
from termcolor import colored

parser = argparse.ArgumentParser()
parser.add_argument('--env', help='development, staing, qa, production', required=True)
parser.add_argument('--liveStack', help='ELB-A, ELB-B')
parser.add_argument('--testStack', help='ELB-A, ELB-B')

args = parser.parse_args()

ENVIROMENT = args.env
LIVE_STACK = args.liveStack
TEST_STACK = args.testStack

with open("config/updater_config.yml", 'r') as ymlfile: # load config file
    cfg = yaml.load(ymlfile)

client = boto3.client('route53')

def assign_cname_to_elb(dnsname, elbname): # Assign a DNS name to a ELB
    # print "\nChecking current CNAME record for %s..." %(dnsname)
    response = client.list_resource_record_sets(
        HostedZoneId=cfg[ENVIROMENT]['HOSTED-ZONE-ID'],
        StartRecordName=dnsname,
        StartRecordType='CNAME',
        MaxItems='1'
    )

    for i in response['ResourceRecordSets']:
        for x in i['ResourceRecords']:
            print "Current CNAME record for %s Key: %s Value: %s " %(dnsname, i['Name'], x['Value'])

            if elbname == x['Value']:
                # print "ELB Key name: %s is same as Value: %s." %(elbname, x['Value'])
                print colored('Record already set. Nothing to do here.', 'green')
                return
            else:
                print colored("Updating CNAME for %s to %s" %(dnsname, elbname), 'red')
                response = client.change_resource_record_sets(
                    HostedZoneId=cfg[ENVIROMENT]['HOSTED-ZONE-ID'],
                    ChangeBatch={
                        'Changes': [
                            {
                                'Action': 'UPSERT',
                                'ResourceRecordSet': {
                                    'Name': dnsname,
                                    'Type': 'CNAME',
                                    'TTL': 60,
                                    'ResourceRecords': [
                                        {
                                            'Value': elbname
                                        },
                                    ]
                                }
                            },
                        ]
                    }
                )
    return response

print colored('######################################\nUpdating DNS for LIVE STACK!\n######################################', 'red')
if LIVE_STACK:   # update live stack DNS setttings

    for key, value in cfg[ENVIROMENT]['live-domains'].iteritems(): # iterate over domains from yaml file

        print "DRY RUN: assign_cname_to_elb(%s, %s) " %(value, cfg[ENVIROMENT][LIVE_STACK])

        # aws_response = assign_cname_to_elb(value, cfg[ENVIROMENT][LIVE_STACK]) # LIVE_STACK = elb name
        # print "AWS response: ", colored(aws_response, 'blue')

    print colored('CNAME records has been set/confirmed', 'red')
    time.sleep(1)

elif TEST_STACK:
    for key, value in cfg[ENVIROMENT]['test-domains'].iteritems(): # iterate over domains from yaml file

        print "DRY RUN: assign_cname_to_elb(%s, %s) " %(value, cfg[ENVIROMENT][TEST_STACK])

        # aws_response = assign_cname_to_elb(value, cfg[ENVIROMENT][TEST_STACK])  # TEST_STACK = elb name
        # print "AWS response: ", colored(aws_response, 'blue')

    print colored('CNAME records has been set/confirmed', 'red')
    time.sleep(1)
