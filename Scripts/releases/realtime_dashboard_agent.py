#
# Usage:
#
import os
import yaml
import time
# import boto3
import psutil
import argparse
import subprocess
# from random import randint
from termcolor import colored
from time import gmtime, strftime

parser = argparse.ArgumentParser()
parser.add_argument('--env', help='development, staing, qa, production', required=True)
# parser.add_argument('--setTestStack', help='ELB-A, ELB-B', required=True)

args = parser.parse_args()

ENVIROMENT = args.env

with open("config/realtime_dashboard_config.yml", 'r') as ymlfile: # load config file
    cfg = yaml.load(ymlfile)

def get_service_stats(servicename):
    stats = []
    userservice_pid = subprocess.check_output("ps ax | grep '%s-enterprise' | grep 'java' | awk '{print $1}'" %(servicename), shell=True)
    return userservice_pid

try:
    while True:
        # print psutil.cpu_times()
        # for i in psutil.pids():
        #     p = psutil.Process(i)
        #     print p.name()
        print get_service_stats(userservice2)

        time.sleep(1)
except KeyboardInterrupt:
    pass
