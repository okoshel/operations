####################################################################################
#### release_version_loader.py - Dedicated release loader system for each IT    ####
####################################################################################

import re
import datetime
import os
import sys
from boto.s3.connection import S3Connection
from natsort import natsorted


def GetS3Auth(awsacckey, awsseckey):
    """ Peform AWS auth
        :param: awsacckey string
        :param: awsseckey string
        :returns: conn boto s3 session
    """
    conn = S3Connection(awsacckey, awsseckey)
    return conn


class ReleaseVersion(object):

    """ Release Version Object For Each Release Software
        :param: dirlist s3 bucketlist object
        :returns: major.minor.devstage-hotfixversion string
    """

    def __init__(self, name, s3conn, blist, date=datetime.datetime.now()):

        self.swname = name
        self.date = date
        self.auth = s3conn
        softname = self.swname
        if softname == 'proxy_enterprise':
            softname = 'rabbit-enterprise'
        if softname == 'enterprise_connectorservice':
            softname = 'emm-connect'
        swregex = re.match('([a-z]*)_*([a-z]*)', softname)
        for sm in swregex.groups():
            if sm == 'enterprise' or sm == '':
                continue
            for dir in blist:
                if dir.name == 'releases/':
                    continue
                dirregex = re.match('releases\W([a-z]*)', dir.name)
                dirmatch = re.match('{}'.format(dirregex.groups()[0]), sm)
                if dirmatch:
                    self.releasedir = dir.name

    def get_latest_version(self, buc):
        """ lookup latest version of sofware from AWS s3bucket
            :returns: major.minor.devstage-hotfixversion string
        """

        software_v_list = []
        vder = buc.list(self.releasedir)
        for key in vder:
            vregex = re.match(
                '.*_([0-9]*\.[0-9]*\.[0-9]*\.?[0-9]*)-([0-9]*).*', key.name)
            if vregex is not None:
                software_v_list.append(key.name)
        # TODO: Find standard lib way of doing this more easily.
        slist = natsorted(software_v_list)
        lversionnum = len(slist) - 1
        lvregex = re.search(
            r"_(?P<version>\w+\.\w+\.\w+(\.\w+){,1}(\-\w+){,2})_",
            slist[lversionnum])
        self.latestversion = lvregex.group('version')
        return self.latestversion

    def set_latest_version(self):
        """ set latest sw version to hieradata cfmgt defined file
            :returns: success bool
        """


def main():
    # The ENV vars for AWS
    AWSKEY = os.environ.get('AWS_ACCESS_KEY_ID')
    AWSSECKEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
    if AWSKEY is None or AWSSECKEY is None:
        sys.exit("ERROR: You need to set AWS_ACCESS_KEY_ID"
                 " AND AWS_SECRET_ACCESS_KEY env var values")
    # S3 bucket const for checking our software versions
    S3_SW_BUCKET = "snappli-software-repo"
    # Run The S3 auth
    s3auth = GetS3Auth(AWSKEY, AWSSECKEY)
    wandera_software_bucket = s3auth.get_bucket(S3_SW_BUCKET)
    wandera_release_dirs = wandera_software_bucket.list("releases/", "/")
    # These should be the puppet classes we use on our repo.
    # TODO: Improve below eventually
    sw_services = ["apnservice_enterprise",
                   "enterprise_connectorservice",
                   "enterprise_filterservice",
                   "app-db-manager-service",
                   "enterprise_notificationservice",
                   "enterprise_portal_cms",
                   "enterprise_redirectionservice",
                   "enterprise_ticketservice",
                   "filterservice",
                   "logservice_enterprise",
                   "portal_enterprise",
                   "profileservice2_enterprise",
                   "proxy_enterprise",
                   "proxyservice_enterprise",
                   "reportservice3_enterprise",
                   "userservice2_enterprise",
		   "stateanalyserservice_enterprise",
		   "publisherservice_enterprise",
                   "videoservice"]
    for service in sw_services:
        sw_service = ReleaseVersion(service, s3auth, wandera_release_dirs)
        print('Software {0} ==> {1}'.format(sw_service.swname,
                       sw_service.get_latest_version(wandera_software_bucket)))


if __name__ == "__main__":
    main()
