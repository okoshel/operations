#
# Usage:
# dns_helper.py --env=staging --setLiveStack=ELB-A
#
import json
import yaml
import time
import boto3
# import string
import argparse
from termcolor import colored

parser = argparse.ArgumentParser()
parser.add_argument('--env', help='development, staing, qa, production', required=True)
# parser.add_argument('--liveStack', help='ELB-A, ELB-B')
# parser.add_argument('--testStack', help='ELB-A, ELB-B')
parser.add_argument('--domain', help='domain name to check')


args = parser.parse_args()

ENVIROMENT = args.env
# LIVE_STACK = args.liveStack
# TEST_STACK = args.testStack
DOMAIN = args.domain

with open("config/updater_config.yml", 'r') as ymlfile: # load config file
    cfg = yaml.load(ymlfile)

client = boto3.client('route53')

def list_record_set(dnsname):
    response = client.list_resource_record_sets(
        HostedZoneId=cfg[ENVIROMENT]['HOSTED-ZONE-ID'],
        StartRecordName=dnsname,
        StartRecordType='A',
        MaxItems='100'
    )

    for i in response['ResourceRecordSets']:
        for x in i['ResourceRecords']:
                return response


aws_records = list_record_set(DOMAIN)

print json.dumps(aws_records, ensure_ascii=False)
# print aws_records
# print type(aws_records)
