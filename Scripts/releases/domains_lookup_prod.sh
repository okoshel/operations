#!/bin/bash

ping_service () # $1 = host
{
    START_TIME=`date +"%T"`
    START_TIME_MS=`date +%s`
    # PING=`ping -t 1 $1 | grep 'PING elb' | awk '{print $2 $3}'`
    PING=`nslookup $1 | grep canonical | awk '{print $5}'`
    END_TIME_MS=`date +%s`
    END_TIME=`date +"%T"`
    END_TIME_MS=`date +%s`
    RESPONSE_TIME=$((END_TIME_MS-START_TIME_MS))
    TIME_ELAPSED=$((END_TIME_MS-SCRIPT_START_TIME))
    echo "$START_TIME $1: $PING, Time elapsed: $TIME_ELAPSED sec"
}

SCRIPT_START_TIME=`date +%s`
while [ "x$keypress" = "x" ]; do
    clear
    echo "Wandera domains lookup: "
    ping_service eu.wandera.com
    ping_service ef.wandera.com
    ping_service er.wandera.com
    ping_service ea.wandera.com
    ping_service en.wandera.com
    ping_service ep.wandera.com
    ping_service radar.wandera.com
    ping_service radar-test.wandera.com
    sleep 2
done
