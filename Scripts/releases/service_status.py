#
# This is a mockup of what real dashbaord would look like
# Usage:
# service_status.py --env=staging

import os
import yaml
import time
import boto3
import string
import argparse
from random import randint
from termcolor import colored
from time import gmtime, strftime

parser = argparse.ArgumentParser()
parser.add_argument('--env', help='development, staing, qa, production', required=False)
# parser.add_argument('--setTestStack', help='ELB-A, ELB-B', required=True)

args = parser.parse_args()

ENVIROMENT = args.env

with open("config/updater_config.yml", 'r') as ymlfile: # load config file
    cfg = yaml.load(ymlfile)

client = boto3.client('route53')

try:
    while True:
        os.system('clear')
        random_value = 'some value'

        print "TIME:",strftime("%Y-%m-%d %H:%M:%S", gmtime())
        print colored('SERVICE STATUS - STACK A - app-101-stgn', 'blue')
        print colored('US:','blue'), colored('UP ','green'), colored("CPU: %s %%, RAM: %sMB" %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('RS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue'), colored('[ MASTER ]','blue')
        print colored('TS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue'), colored('[ MASTER ]','blue')
        print colored('NS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('FS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('AS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('TOTAL MEM: %sMB '%(randint(2000,4000)), 'blue')
        print "   "
        print colored('SERVICE STATUS - STACK B - app-201-stgn', 'blue')
        print colored('US:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('RS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue'), colored('[ SLAVE ]','blue')
        print colored('TS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue'), colored('[ SLAVE ]','blue')
        print colored('NS:','blue'), colored('SERVICE IS DOWN!!! ','red', attrs=['reverse', 'blink'])
        print colored('FS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('AS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('TOTAL MEM: %sMB '%(randint(2000,4000)), 'blue')
        print "   "
        print colored('SERVICE STATUS - STACK C - app-301-stgn', 'blue')
        print colored('US:','blue'), colored('UP ','green'), colored("CPU: %s %%, RAM: %sMB" %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('RS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue'), colored('[ SLAVE ]','blue')
        print colored('TS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue'), colored('[ SLAVE ]','blue')
        print colored('NS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('FS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('AS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('TOTAL MEM: %sMB '%(randint(2000,4000)), 'blue')
        print "   "
        print colored('SERVICE STATUS - STACK D - app-401-stgn', 'blue')
        print colored('US:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('RS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue'), colored('[ SLAVE ]','blue')
        print colored('TS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue'), colored('[ SLAVE ]','blue')
        print colored('NS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('FS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('AS:','blue'), colored('UP ','green'), colored('CPU: %s %%, RAM: %sMB' %(randint(40,99),randint(500,750)), 'red'), colored('TCP IN: %s ' %(randint(60,99)), 'blue')
        print colored('TOTAL MEM: %sMB ' %(randint(2000,4000)), 'blue')
        time.sleep(1)
except KeyboardInterrupt:
    pass
