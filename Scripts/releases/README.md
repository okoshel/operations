# Releases Ops Scripts

### release_version_loader.py -- Gets latest service release version from S3 and display to stdout

Requires python 3.3.x +

```
pip install -r requirements.txt
```

Please set the following env var values to use the script:-

```
AWS_ACCESS_KEY_ID=xxxxxxxx
AWS_SECRET_ACCESS_KEY=xxxxxx
```


