from setuptools import setup, find_packages

__version__ = None
execfile('__init__.py') # Get __version__

if not __version__:
    raise RuntimeError('__version__ not found')

setup(
    name='deploye_release',
    version=__version__,
    description='Build proxies',
    url='https://bitbucket.org/snappli-ondemand/operations/Scripts/Arturo',
    author='Wandera Ops',
    author_email='arturo.noha@wandera.com',
    packages=find_packages(),
    package_dir={'new_release_deployment': 'Arturo'},
    package_data={'new_release_deployment': ['./*']},
    install_requires=[
        'six',
        'apache-libcloud>=1.1.0',
        'lazy>=1.2',
        'anyconfig>=0.6',
        'click==6.6',
        'paramiko',
        'pycrypto',
        'chopsticks>=0.5',
        'enum34>=1.1.6',
        'PingdomLib',
        'requests==2.11.1',
        'ProxyTypes',
    ],
    tests_require=[
        'pytest',
        'mock',
    ],
    entry_points={
        'console_scripts': [
            'deploy_release = deploy_release:main',
        ],
    },
)
