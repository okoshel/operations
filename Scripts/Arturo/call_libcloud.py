#!/usr/local/bin/python

import libcloud_api
from libcloud_api import *
import os
import sys
from pprint import pprint
import argparse
import subprocess
import platform

def clear_screen():
    clear = subprocess.Popen( "cls" if platform.system() == "Windows" else "clear", shell=True)
    clear.communicate()

def print_json(mydict,tabs=""):
    for key in mydict:
        if type(mydict[key]).__name__ == "dict":
            print "{}{} : {{".format(tabs, key)
            print_json(mydict[key],"{}\t".format(tabs))
            print "{}}}".format(tabs)
        else:
            print "{}{} : {}".format(tabs,key,mydict[key])

def get_aws_instances():
    options = parse_options(sys.argv[1:])
    regions = get_regions('EC2')
    print "{}".format(regions)

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('--providers', dest = "providers_TODO", nargs = '+', help = "For all providers: all. Default: all")
    parser.add_argument('--get-raw-instances', action = "store_true", dest = "instances_to_get", default = False, help = "print raw data for all instances")
    parser.add_argument('--show-instances', action = "store_true", dest = "instances_to_show", default = False, help = "print name, provider and state info for all instances")
    parser.add_argument('--get-instance', nargs = '+', dest = "instance_to_get", help = "print raw data for a given (list of) instance")
    parser.add_argument('--show-instance', nargs = '+', dest = "instance_to_show", help = "print name, provider and state for a given (list of) instance")
    parser.add_argument('--get-regions', action = "store_true", dest = "get_regions", default = False)
    parser.add_argument('--regions', dest = "regions_TODO", nargs = '+', help = "List of regions. For all regions: all. Default all")
    parser.add_argument('--stop-instances', dest = "instances_to_stop", nargs = '+')
    parser.add_argument('--start-instances', dest = "instances_to_start", nargs = '+')
    parser.add_argument('--get-proxies', action = "store_true", dest = "get_proxy_list", default = False)
    parser.add_argument('--get-lb-list', action = "store_true", dest = "get_lb_list", default = False)
    parser.add_argument('--get-lbs-members', action = "store_true", dest = "get_lbs_members", default = False)
    return parser.parse_args()

#############  MAIN  #############

if __name__ == "__main__":
    clear_screen()
    #(options, args) = parse_options(sys.argv[1:])
    options = parse_options(sys.argv[1:])
    if options.instances_to_show and options.instances_to_get:
        sys.exit("ERROR: --get-raw-instances and --show-instances are mutually exclusive")
    #print "\nArguments: {}\n".format(options)
    if options.regions_TODO and not options.providers_TODO:
        sys.exit("ERROR: You must specify the provider to which the regions belong")

    providers, provider_aliases = get_providers()

    providers_TODO = []
    drivers = {}
    if not options.providers_TODO:
        providers_TODO = providers
    else:
        found = False
        for alias in options.providers_TODO:
            for provider_name in provider_aliases:
                if alias == provider_name or alias in provider_aliases[provider_name]:
                    found = True
                    if provider_name not in providers_TODO:
                        providers_TODO.append(provider_name)
            if not found:
                print "ERROR: Provider {} is not suported. Skipping".format(alias)
        #providers_TODO = options.providers_TODO

    for provider in providers_TODO:
        if provider == "RACKSPACE":
            if options.regions_TODO:
                rs_drivers = init_rackspace_driver(options.regions_TODO)
            else:
                rs_drivers = init_rackspace_driver()

            if len(rs_drivers) > 0:
                #print "[DEBUG] adding RS drivers to all drivers"
                drivers.update(rs_drivers)
        elif provider == "EC2":
            #regions_TODO = aws_regions
            if options.regions_TODO:
                regions_TODO = options.regions_TODO
            else:
                regions_TODO = get_regions("EC2")

            aws_drivers = init_ec2_driver(regions_TODO)
            #aws_drivers = init_ec2_driver()
            if len(aws_drivers) > 0:
                #print "[DEBUG] adding AWS drivers to all drivers"
                drivers.update(aws_drivers)
        elif provider == "SOFTLAYER":
            sl_drivers = init_softlayer_driver()
            if len(sl_drivers):
                #print "[DEBUG] adding SL drivers to all drivers"
                drivers.update(sl_drivers)
        elif provider == "OPENSTACK":
            #dt_drivers = { "OPENSTACK" : "N/A" }
            dt_drivers = init_openstack_driver()
            if len(dt_drivers) > 0:
                #print "[DEBUG] adding DT drivers to all drivers"
                drivers.update(dt_drivers)

    if options.instances_to_show:
        display_instance(drivers)

    if options.instances_to_get:
        print "INFO: Getting all instances from relevant providers"
        all_instances = get_all_instances(drivers)
        for driver_name in all_instances:
            dr_instances = all_instances[driver_name]
            if len(dr_instances) > 0:
                print "==> {} instances <<==".format(driver_name)
                for instance in dr_instances:
                    print "{}".format(instance)

    if options.instance_to_show:
        for instance in options.instance_to_show:
            try:
                #print "[DEBUG] Instance to show: {} ({} drivers)".format(instance, len(drivers))
                display_instance(drivers, instance)
            except Exception as e:
                print "[ERROR] Could not get information to display for {}".format(options.instance_to_show)
                print "{}".format(e.message)

    if options.instance_to_get:
        for instance in options.instance_to_get:
            try:
                return_instance, driver = get_instance(instance,drivers)
                print "{}".format(return_instance)
                print_json(vars(return_instance))
            except Exception as e:
                print "[ERROR] Could not get information to display for {}".format(options.instance_to_show)
                print "{}".format(e.message)

    if options.instances_to_stop:
        for instance in options.instances_to_stop:
            if stop_instance(instance,drivers):
                print "{} successfully stopped".format(instance)
            else:
                print "ERROR: could not stop {}".format(instance)

    if options.instances_to_start:
        for instance in options.instances_to_start:
            if start_instance(instance,drivers):
                print "{} successfully started".format(instance)
            else:
                print "ERROR: could not start {}".format(instance)

    if options.get_proxy_list:
        proxy_list = []
        proxy_regex = re.compile(".*\.node\..*\.wandera\.com")
        all_instances = get_all_instances(drivers)
        for driver in all_instances:
            for instance in all_instances[driver]:
                if proxy_regex.search(instance.name):
                    proxy_list.append(instance.name)
                    print "{}".format(instance.name)

    if options.get_lb_list:
        lb_drivers = get_LB_list()
        for driver in lb_drivers:
            if re.search('-core', driver.name):
                print "{}".format(vars(driver))

    if options.get_lbs_members:
        lb_drivers = get_LB_list()
        for driver in lb_drivers:
            if re.search('-core', driver.name):
                balancer, members = get_LB_members(driver.name)
                print "** Members of {}".format(driver.name)
                for member in members:
                    print "{}".format(vars(member))
