#####################################################################
# fabfile.py
# Author: Arturo Noha
# Version: 1.0
#-----------------------------------------------------------------
# Available tasks
# * check_app: check the status of the services running in app servers
# * check_connector: check connector service
# * check_delta: check delta service
# * check_fis: check filter service
# * check_portal: check radar
# * check_proxy: check proxy service
# * check_siem: check siem service
#   - check_<service> will
#       - check the package version for the relevant services running on that server
#       - check the relevant processes running and verify if they are running on the installed package version
#       - tail the log for the relevant services
# * check_process: check the package for a given service and verify the status of the service's process
#   - Argument: string to grep when running dpkg in the host
# * service_start:
# * service_stop:
# * service_restart:
# * service_status:
#   - the 4 service management tasks above take 1 argument: the service name, as recognised by the "service" command
# * create_ssh_access: run mco command on puppet master for given host to grant access to "staff"
# * git_pull: run git pull on the given host
# * puppet_agent: run puppet agent on the host.
#   - syntax: puppet_agent(noop=<True/False>, tags=<tags>, force=<True/False>)
#       - noop: True: run puppet with --noop / False: run puppet without --noop. It will ask for confirmation by default
#       - tags: required tags for the puppet run
#       - force: only applicable for noop=False. If set to "force" will run puppet without asking for confirmation
# * deploy_server: delpoy new release on given host
#       - runs puppet noop
#       - runs puppet without noop if noop succeeds
#       - run the required check_<service> function based on the server type
# * tail_rabbit_log: tail rabbit.log on given proxy node
#----------------------------------------------------------------
# PRIVATE FUNCTIONS
# * _get_option: print a given message and question and parse input from user.
#   - valid user options: Y / n (case sensitive)
#   - question must be a yes or no question
# * _run_extra_commands: run a series of commands on the host.
#   - used to run server specific commands on the check_<service> tasks
#   - takes an array of strings (each string = 1 command to run)
#####################################################################

import os
import sys
from sys import stdin
import subprocess
from subprocess import PIPE, Popen
#import paramiko
#from paramiko import ssh_exception
import time
from fabric.api import env, run
from fabric.operations import sudo, local
from fabric.context_managers import hide, show
import re
from service_types import *

env.warn_only = True # Allow mangement of errors and exceptions without stopping execution by default
env['sudo_prefix'] += '-H '
valid_options = [ 'Y' , 'n' ]

def check_process(process):
    return_dict = {}
    dpkg_cmd = "dpkg -l | grep {}".format(process)

    with hide('running', 'stderr', 'stdout'):
        dpkg_response = sudo(dpkg_cmd)

    format_dpkg_response = re.sub(r'[ \t]+', ':', dpkg_response)

    for service_line in format_dpkg_response.splitlines():
        service = service_line.split(':')[1]
        version = service_line.split(':')[2].split('-')[0]

        grep_service_cmd = "ps -ef | grep {} | grep -v grep".format(service)
        print "\n* Checking Service: {}. Dpkg Version: {}\n".format(service, version)
        try:
            with hide('running', 'stderr', 'stdout'):
                grep_service_response = sudo(grep_service_cmd)
        except:
            print "Failed to run the {}.".format(grep_service_cmd)
            print "Return Code: {}, Stderr: {}".format(grep_service_response.return_code, grep_service_response.stderr)

        if grep_service_response.failed:
            print "\"{}\" Failed. -- Return Code: {}, Stderr: {}".format(grep_service_cmd, grep_service_response.return_code, grep_service_response.stderr)
            return_dict.update({service : 'failed'})
        else:
            PID = re.sub(r'[ \t]+',':',grep_service_response).split(':')[1]
            JAR = re.sub(r'.* -jar ','',grep_service_response).split(' ')[0]
            serv_vers_pattern = re.compile("{}.*{}".format(service, version))

            if not serv_vers_pattern.search(grep_service_response):
                print "!!! ERROR: {} is not running in the dpkg version.".format(service)
                print "PID: {} -- JAR: {} -- DPKG: {}".format(PID, JAR, version)
                return_dict.update({service : 'wrongversion'})
            else:
                print "Process running correct version:\n\t- PID: {}\n\t- JAR: {}\n".format(PID, JAR)
                return_dict.update({service : PID})

        print "------------------------------------"
    return return_dict

def _run_extra_commands(commands):
    for command in commands:
        with hide('stdout'):
            cmd_response = sudo(command)
            if cmd_response.failed:
                print "\nERROR: {} failed. Status code: {} ({})".format(command, cmd_response.return_code, cmd_response.stderr)
            else:
                print "{}".format(cmd_response)
            print "------------------------------------"

def check_proxy():
    proxy_log="/opt/proxy/logs/rabbit.log"
    tail_lines = 25
    tail_log_cmd = "tail -n {} {}".format(tail_lines, proxy_log)

    check_all_status_cmd = "service --status-all | grep proxy"

    proxy_extra_commnads = [ check_all_status_cmd, tail_log_cmd ]

    check_response = check_process("rabbit | grep enter")
    for service in check_response.keys():
        PID = check_response.get(service)
        if PID != "failed":
            _run_extra_commands(proxy_extra_commnads)

def tail_rabbit_log():
    rabbit_log = "/opt/proxy/logs/rabbit.log"
    command = "tail -f {}".format(rabbit_log)
    with hide('running'):
        sudo(command)

def check_fis():
    mysql_cmd = "mysql -e 'SHOW SLAVE STATUS\G' | egrep 'Slave_IO_Running|Slave_SQL_Running'"

    tail_lines = 25
    fis_log = "/opt/filterservice/logs/filter-service-access.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, fis_log)

    fis_extra_commnads = [ mysql_cmd , tail_log_cmd ]

    check_response = check_process("enter")
    for service in check_response.keys():
        PID = check_response.get(service)
        if PID != "failed":
            _run_extra_commands(fis_extra_commnads)

def check_delta():
    tail_lines = 25
    delta_log = "/opt/deltaloadservice/logs/deltaload-service.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, delta_log)

    delta_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("delta")
    for service in check_response.keys():
        PID = check_response.get(service)
        if PID != "failed":
            _run_extra_commands(delta_extra_commnads)

def check_beacon():
    tail_lines = 25
    beacon_log = "/opt/beaconservice/logs/beacon-service.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, beacon_log)

    beacon_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("beacon")
    for service in check_response.keys():
        PID = check_response.get(service)
        if PID != "failed":
            _run_extra_commands(beacon_extra_commnads)

def check_osc():
    tail_lines = 25
    delta_log = "/opt/openscoringservice/logs/openscoring-service-enterprise.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, delta_log)

    osc_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("openscoring")
    for service in check_response.keys():
        PID = check_response.get(service)
        if PID != "failed":
            _run_extra_commands(osc_extra_commnads)

def check_siem():
    tail_lines = 25
    siem_log = "/opt/siemservice/logs/siem-service.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, siem_log)

    siem_extra_commnads = [ tail_log_cmd ]

    check_response = check_process("siem")
    for service in check_response.keys():
        PID = check_response.get(service)
        if PID != "failed":
            _run_extra_commands(siem_extra_commnads)

def check_connector():
    tail_lines = 25
    siem_log = "/opt/connectorservice/logs/connector-service.log"
    tail_log_cmd = "tail -n {} {}".format(tail_lines, siem_log)

    dpkg_cmd = "dpkg -l | grep emm-connect"

    with hide('running', 'stderr', 'stdout'):
        dpkg_response = sudo(dpkg_cmd)

    format_dpkg_response = re.sub(r'[ \t]+', ':', dpkg_response)

    for service_line in format_dpkg_response.splitlines():
        service = service_line.split(':')[1]
        version = service_line.split(':')[2].split('-')[0]

        grep_service_cmd = "ps -ef | grep connectorservice | grep -v grep".format(service)
        print "\n* Checking Service: {}. Dpkg Version: {}\n".format(service, version)
        try:
            with hide('running', 'stderr', 'stdout'):
                grep_service_response = sudo(grep_service_cmd)
        except:
            print "Failed to run the {}.".format(grep_service_cmd)
            print "Return Code: {}, Stderr: {}".format(grep_service_response.return_code, grep_service_response.stderr)

        if grep_service_response.return_code != 0:
            print "\"{}\" Failed. -- Return Code: {}, Stderr: {}".format(grep_service_cmd, grep_service_response.return_code, grep_service_response.stderr)
        else:
            PID = re.sub(r'[ \t]+',':',grep_service_response).split(':')[1]
            JAR = re.sub(r'.* -jar ','',grep_service_response).split(' ')[0]
            serv_vers_pattern = re.compile("EMM-Connector.*{}".format(version))

            if not serv_vers_pattern.search(grep_service_response):
                print "!!! ERROR: {} is not running in the dpkg version.".format(service)
                print "PID: {} -- JAR: {} -- DPKG: {}".format(PID, JAR, version)
            else:
                print "Process running correct version:\n\t- PID: {}\n\t- JAR: {}\n".format(PID, JAR)
        print "------------------------------------"

    _run_extra_commands([tail_log_cmd])

def check_portal():
    tail_lines = 25
    portal_log_dir = "/var/www-admin/app/logs/"
    portal_conf_dir = "/var/www-admin/app/config"

    with hide('running', 'stdout', 'stderr'):
        hostname = sudo("hostname")
    if re.search('-stgn', hostname):
        environment = "stgn"
    elif re.search('-(prod|core)', hostname):
        environment = "production"
    elif re.search('-intg', hostname):
        environment = "integration"
    else:
        print "ERROR: Unknwn environment"
        sys.exit(1)

    log_file = "{}.log".format(environment)
    conf_file = "config_{}.yml".format(environment)

    portal_log = os.path.join(portal_log_dir, log_file)
    portal_conf = os.path.join(portal_conf_dir, conf_file)

    tail_log_cmd = "tail -n {} {}".format(tail_lines, portal_log)
    check_config_cmd = "cat {}".format(portal_conf)
    check_process_cmd = "ps -ef | grep apache | grep -v grep"

    dpkg_cmd = "dpkg -l | grep portal"

    with hide('running', 'stderr', 'stdout'):
        dpkg_response = sudo(dpkg_cmd)

    format_dpkg_response = re.sub(r'[ \t]+', ':', dpkg_response)

    for service_line in format_dpkg_response.splitlines():
        service = service_line.split(':')[1]
        version = service_line.split(':')[2]

    print "\n* Service: {}. Dpkg Version: {}\n".format(service, version)

    _run_extra_commands([ check_process_cmd , check_config_cmd , tail_log_cmd ])

def check_app():
    tail_lines = 25

    dpkg_cmd = "dpkg -l | grep enter"

    with hide('running', 'stderr', 'stdout'):
        dpkg_response = sudo(dpkg_cmd)

    format_dpkg_response = re.sub(r'[ \t]+', ':', dpkg_response)

    failed_services = []
    wrong_version_services = {}

    for service_line in format_dpkg_response.splitlines():
        service = service_line.split(':')[1]
        version = service_line.split(':')[2].split('-')[0]

        grep_service_cmd = "ps -ef | grep {} | grep -v grep".format(service)
        print "\n* Checking Service: {}. Dpkg Version: {}\n".format(service, version)
        time.sleep(1)
        try:
            with hide('running', 'stderr', 'stdout'):
                grep_service_response = sudo(grep_service_cmd)
        except:
            print "Failed to run the {}.".format(grep_service_cmd)
            print "Return Code: {}, Stderr: {}".format(grep_service_response.return_code, grep_service_response.stderr)

        if grep_service_response.return_code != 0:
            print "\"{}\" Failed. -- Return Code: {}, Stderr: {}".format(grep_service_cmd, grep_service_response.return_code, grep_service_response.stderr)
            failed_services.append(service)
        else:
            PID = re.sub(r'[ \t]+',':',grep_service_response).split(':')[1]
            JAR = re.sub(r'.* -jar ','',grep_service_response).split(' ')[0]
            serv_vers_pattern = re.compile("{}.*{}".format(service, version))

            if not serv_vers_pattern.search(grep_service_response):
                print "!!! ERROR: {} is not running in the dpkg version.".format(service)
                print "PID: {} -- JAR: {} -- DPKG: {}".format(PID, JAR, version)
                wrong_version_services[service] = [version, JAR]
            else:
                print "Process running correct version:\n\t- PID: {}\n\t- JAR: {}\n".format(PID, JAR)

            service_alias = re.sub(r'-.*', '', service)
            get_log_cmd = "find /opt/{0}/etc/ -maxdepth 1 -type f -name '*-service.yaml' | xargs grep -A25 '^logging' | grep currentLogFilename | cut -d':' -f2 | tr -d ' ' | sed -e 's:\./::g' | xargs -I logfile sh -c 'echo \"==> /opt/{0}/logfile <==\";tail -n {1} /opt/{0}/logfile'".format(service_alias, tail_lines)
            with hide('running', 'stdout', 'stderr'):
                log = sudo(get_log_cmd)

            if log.failed:
                print "ERROR: Failed to get log file for \"{}\"".format(service)
            else:
                print "{}".format(log)

        print "------------------------------------"
    app_extra_cmd = ". ~/service-status.sh"
    _run_extra_commands([app_extra_cmd])

    if len(failed_services) > 0:
        print "------------------------------------"
        print "ERROR: The following services are stopped:"
        for serv in failed_services:
            print "\t- {}".format(serv)
        print "\nAvailable tasks:"
        print "\tfab -H <host> -u <ssh_username> service_start/service_restart/service_stop/service_status:<service_name>"

    if len(wrong_version_services) > 0:
        print "------------------------------------"
        print "WARN: The following services are not running on the same version as the package"
        for serv in wrong_version_services:
            print "\t- {}: version: {} --- Running jar: {}".format(serv, wrong_version_services[serv][0], wrong_version_services[serv][1])
    return failed_services, wrong_version_services

#########################################################################

def service_restart(service):
    restart_cmd = "service {} restart".format(service)
    return sudo(restart_cmd)

def service_stop(service):
    stop_cmd = "service {} stop".format(service)
    return sudo(stop_cmd)

def service_start(service):
    start_cmd = "service {} start".format(service)
    return sudo(start_cmd)

def service_status(service):
    status_cmd = "service {} status".format(service)
    return sudo(status_cmd)

def failover_proxy(src, dest, reverse=None):
    failover_cmd = "woopas failover {} {}".format(src, dest)
    if reverse is not None:
        failover_cmd = "{} -r".format(failover_cmd)
    local(failover_cmd)

#########################################################################

def git_pull(puppet_folder):
    git_pull_command = "cd {}; git pull".format(puppet_folder)
    sudo(git_pull_command)

def puppet_agent(noop = True, tags = None, force = "check"):
    if noop == "False" or not noop:
        puppet_agent_cmd = "puppet agent -t"
        noop = False
    else:
        puppet_agent_cmd = "puppet agent -t --noop"

    if tags is not None:
        puppet_agent_cmd = "{} --tags {}".format(puppet_agent_cmd, tags)

    if not noop and force != "force":
        confirm = _get_option('WARNING : Running puppet agent WITHOUT --noop option ("fab -H <host> -u <user> puppet_agent:noop=True" for -noop option).\nWould you wish to proceed?')
    else:
        confirm = 'Y'

    if confirm == 'Y':
        return sudo(puppet_agent_cmd)
    else:
        print "Skipping puppet agent."
        return -1

def puppet_error_options(hostname, noop):
    valid_option = ['1','2','3','4']
    print "WARNING: Puppet returned an error on {} ({}).\nWhat would you like to do?".format(hostname, env.host_string)

    if noop:
        next_step = "puppet live run"
    else:
        next_step = "check node status"

    print """\t1. Re-run"
    \t2. Continue deployment on this node ({})
    \t3. Skip this node and continue on the rest of nodes
    \t4. Abort deployment""".format(next_step)
    option = raw_input("Select an option: ")
    while option not in valid_option:
        option = raw_input("Please, select a valid option: ")
    return option

def run_puppet_deploy(hostname, noop):
    result = puppet_agent(noop = noop)
    while True:
        if result == -1:
            return result
        elif result.failed and result.return_code != 2:
            next_action = puppet_error_options(hostname, noop)
            if next_action == '1':
                run_puppet_deploy(hostname, noop)
            elif next_action == '4':
                sys.exit("** Deployment aborted by user after puppet noop = {} failed (Error Code: {}) **".format(noop, result.return_code))
            else:
                return next_action
        else:
            return 0


def deploy_server(name):
    print "Starting release deployment on {} ({})".format(name, env.host_string)

    noop = run_puppet_deploy(name, noop=True)
    if noop == 4:
        sys.exit("** Deployment aborted by user **")
    elif noop == 3:
        return noop

    live = run_puppet_deploy(name, noop=False)
    #live = 2
    if live == -1:
        print "WARN: Puppet agent not run. Abort deployment on {} ({})".format(name, env.host_string)
        return -1
    elif live == 4:
        sys.exit("** Deployment aborted by user **")
    elif live == 3:
        return live

    '''elif live.failed and live.return_code != 2:
        if _get_option("\WARNIG: Puppet returned an error on {} ({}).\nWould you wish to continue?".format(name, env.host_string)) != "Y":
            sys.exit(1)'''
    #else:
    '''if _get_option("Puppet finished {}.\nWould you wish to continue?".format(env.host)) != "Y":
        sys.exit(1)'''
    proxy_regex = re.compile("\.node\.")
    if proxy_regex.search(name):
        server_type = "prx"
    else:
        server_type = re.sub(r'-.*', '', name)
    print "Server type: {}".format(server_type)
    if server_type in server_check_function:
        check_function = server_check_function[server_type]
        print "running {}".format(check_function)
        time.sleep(5)
        eval(check_function+'()')

    print "Release deployment completed for {} ({})".format(name, env.host_string)
    return 0

def start_service(service):
    start_cmd = "service {} start".format(service)
    start = sudo(start_cmd)
    if start.succeeded:
        return check_process(service)
    else:
        print "Failed to start {} on {}".format(service, env.host_string)


def create_ssh_access(server):
    #server = server.replace("\..*",'')
    server = re.sub(r'\..*', '', server)
    create_cmd = 'mco puppet runall 5 --tags="staff" -S "hostname={}"'.format(server)
    sudo(create_cmd)

def _get_option(message = None):
    if message is None:
        display_message = "Would you wish to continue? [Y/n]: "
    else:
        display_message = message + " [Y/n]: "

    option = raw_input(display_message)
    while option not in valid_options:
        option = raw_input("Please enter a valid option [Y/n]: ")

    return option


def _set_user(user):
    env.user = user

def _set_hosts(hosts):
    print "setting hosts to {}".format(hosts)
    env.disable_known_hosts = True
    env.hosts.extend(hosts)
    print env.hosts
