#############################
import subprocess
from subprocess import Popen
#from subprocess import CREATE_NEW_CONSOLE
#import appscript
import sys
import tempfile
import os

def main(active_svr, user):
    #fab_dir = "/Users/arturonoha/Documents/scripts/releases"
    fab_dir = "~/"
    active_cmd = "cd {}; fab -H {} -u {} tail_rabbit_log".format(fab_dir, active_svr, user)
    os.system("gnome-terminal {}".format(active_cmd))
    #os.system("start cmd /K {}".format(active_cmd))
    #Popen('xterm -hold -e "{}}"'.format(active_cmd))
    #appscript.app('Terminal').do_script(active_cmd)

    if len(sys.argv) == 3:
        failover_svr = sys.argv[2]
        failover_cmd = "cd {}; fab -H {} -u {} tail_rabbit_log".format(fab_dir, failover_svr, user)
        appscript.app('Terminal').do_script(failover_cmd)

if __name__ == "__main__":
    main(sys.argv[1], "anoha")
