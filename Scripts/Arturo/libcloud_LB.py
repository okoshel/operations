import libcloud_api
from libcloud_api import *
import sys

def print_json(mydict,tabs=""):
    for key in mydict:
        if type(mydict[key]).__name__ == "dict":
            print "{}{} : {{".format(tabs, key)
            print_json(mydict[key],"{}\t".format(tabs))
            print "{}}}".format(tabs)
        else:
            print "{}{} : {}".format(tabs,key,mydict[key])

if __name__ == "__main__":
    #lbs = get_LB_members(lb_region)
    '''for lb in lbs:
        print "{}".format(lb)'''
    '''aws_drivers = init_ec2_driver([lb_region])
    aws_instances = get_all_instances(aws_drivers)'''

    '''print "\nGetting members in elb-002-core\n"
    balancer, members = get_LB_members("elb-002-core")
    for member in members:
        for driver_name in aws_instances:
            for instance in aws_instances[driver_name]:
                if instance.id == member.id:
                    print "{}, {}".format(member.id, instance.name)

    print "------------------"
    detach_LB_member("web-201-core.eu-west-1b.ie.wandera.com","elb-002-core")
    print "------------------"
    attach_LB_member("web-201-core.eu-west-1b.ie.wandera.com","elb-002-core")
    print "------------------"'''

    '''dns_driver = get_DNS_driver()
    print "Route 53 zones:"
    for zone in dns_driver.list_zones():
        print "{} - {}".format(zone.domain, zone.id)'''

    '''record_name = "ap"
    print "Details for {}".format(record_name)
    records = get_DNS_records(record_name=record_name, myzone="wandera.com")
    if not records:
        print "ERROR: Failed to get details for {}".format(record_name)
    else:
        for record in records:
            print "{}".format(record.__dict__)'''

    '''dt_drivers = init_openstack_driver()
    for driver in dt_drivers:
        print "{}: {}\n{}".format(driver, dt_drivers[driver], dir(driver))
        print "\n{}".format(dt_drivers[driver].list_nodes())'''

    '''print "\nList of Load Balancers in AWS"
    libcloud_api.test_LB()'''

    print "\nBOTO3 test"

    hz = libcloud_api.get_hosted_zone('wandera.com')
    all_info = libcloud_api.get_route53_record_sets(hz.id)
    print "wandera.com id: {}".format(hz.id)
    '''print "{}".format(record_sets)
    sys.exit(0)'''

    '''for record in all_info:
        #print "{}".format(record)
        if 'Weight' in record:
            print "{}".format(record)
            #print "{}, {}, {}, {}".format(record['Name'], record['Type'], record['SetIdentifier'], record['Weight'])

    sys.exit(0)'''
    stacks = ['A']
    for stack in stacks:
        print "Checking Stack {}".format(stack)
        stack_regex = re.compile("Stack {}".format(stack))
        for record in all_info:
            if 'SetIdentifier' in record:
                if stack_regex.search(record['SetIdentifier']):
                    print "{}, {}, {}, {}, {}".format(record['Name'], record['Type'], record['SetIdentifier'], record['ResourceRecords'][0]['Value'], record['Weight'])
        print "\nUpdating weight for Stack {}".format(stack)
        libcloud_api.update_route53_weight(stack, 1)
        print "\n--------------------\n"
    sys.exit(0)

    if len(sys.argv) != 4:
        sys.exit("ERROR: usage {} <attach/detach> <instance_name> <load_balance_name>".format(sys.argv[0]))

    action = sys.argv[1]
    instance = sys.argv[2]
    lb = sys.argv[3]

    if action == "attach":
        if attach_LB_member(instance, lb) is not None:
            print "{} attached successfully to {}".format(instance, lb)
        else:
            print "ERROR: Failed to attach {} to {}".format(instance, lb)
    elif action == "detach":
        if detach_LB_member(instance, lb):
            print "{} detached successfully from {}".format(instance, lb)
        else:
            print "ERROR: Failed to detach {} from {}".format(instance, lb)
