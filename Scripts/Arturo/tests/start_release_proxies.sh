#!/bin/bash

# Argument: proxy release woopas plan file

if [[ $# -lt 2 ]]; then
  echo "USAGE: $0 <woopas_plan_file> <idle_instances_dest_file>"
  exit 1
fi
PLAN=$1 # Format: <DATE>,<proxy_name>,TODO
STARTED_LIST=$2

echo "Getting Stopped instances from deployment plan"
grep TODO $PLAN | cut -d',' -f2 | grep "\.node\.[a-z]\{1,\}-[a-z]\{1,\}-[0-9]" | while read instance; do region=`echo $instance | sed -e "s/^[^\.]*\.node\.\([a-z]*-[a-z]*-[0-9]\).*/\1/g"`; python ./call_libcloud.py --providers aws --regions $region --show-instance $instance; done | grep stopped | awk -F"Name:" '{print $2}' | cut -d' ' -f1 | while read inst
do
  region=$(echo $inst | sed -e "s/^[^\.]*\.node\.\([a-z]*-[a-z]*-[0-9]\).*/\1/g")
  echo "./call_libcloud.py --start-instance $inst --providers aws --regions $region"
  python ./call_libcloud.py --start-instance $inst --providers aws --regions $region
  echo "* Waiting for $inst to start"
  sleep 5
  #while [[ ! `python ./call_libcloud.py --providers aws --regions $region --show-instance $inst | grep stopped` ]]; do
  while [[ ! `python ./call_libcloud.py --providers aws --regions $region --show-instance $inst | grep running` ]]; do
      echo "...waiting for $inst to start"
    sleep 5
  done
  echo "$inst" >> $STARTED_LIST
  python ./call_libcloud.py --regions $region --show-instance $inst | grep $inst
  #echo ""
  echo "* Creating ssh access (fab -H puppet.snappli.net -u anoha create_ssh_access:$inst)"
  fab -H puppet.snappli.net -u anoha create_ssh_access:$inst
  echo "------"
done
