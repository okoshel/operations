#!/usr/local/bin/python

############################################################
# woopas_amigo.py
# Author: Arturo Noha
# April 2017
############################################################
# Conversion to python of pre-existing woopas_amigo.sh + additional features
#
# Arguments:
# 1. (MANDATORY) an input file with the list of proxies to deploy and the date:
#   . format: YYYY-MM-DD,<proxy_name>,TODO
# 2. a dictionary with the providers libcloud drivers:
#   . format: { 'driver_name1' : 'driver1'[, 'driver_name2' : '{driver2}'...]}
#   . if not provided, it will contact the available providers via libcloud to get them
# 3. a dictionary with the list of instances from the providers to use as reference
#   . format: { 'driver_name1' : [array_of_instances_from_driver1][, 'driver_name2' : [array_of_instances_from_driver2]...]}
#   . if not provided, it will get them from the providers via libcloud
#
# Logic:
# 1. if not provided as arguments, get porivders drivers and instances (via libcloud_api)d
# 2. iterate through the list of proxies TODO for today's date
# 3. check the state of the proxy from the instances information gather initially
#   3.a. if the instance is stopped, try to start it (via libcloud_api)
# 4. open a new terminal window and tail the rabbit.log on the proxy to upgrade
# 5. run woopas
# 6. if woopas successfull, update the file with the list of proxies and set the proxy to DONE
# 7. check the status of the proxy (via fabfile)
#   - check the proxy_enterprise package verion and verify that the service is running on the same version
#   - check all services status
#   - tail rabbit.log
# 8. if any instance was started, stopped them (via fabfile)


import os
import sys
from datetime import date
import re
from time import sleep
import subprocess
from subprocess import Popen, PIPE
import fabfile
import libcloud_api
from fabric.context_managers import settings
import tempfile
import appscript

today = date.today()
DATE = today.isoformat()
FAB_USERNAME="anoha"
current_dir=os.getcwd()
operations_dir = "/Users/arturonoha/Documents/scripts/releases"

def update_proxy_status(node,TODOLIST):
    temp_file_name = "{}.tmp".format(TODOLIST)
    with open(TODOLIST, "r")  as todo_file, open(temp_file_name,"w") as temp_file:
        for line in todo_file:
            update_regex = re.compile(",{},TODO".format(node))
            replace_str = ",{},DONE".format(node)
            line = update_regex.sub(replace_str, line)
            temp_file.write(line)
    os.remove(TODOLIST)
    os.rename(temp_file_name, TODOLIST)

def new_window(active_svr, user):
    active_cmd = "cd {}; fab -H {} -u {} tail_rabbit_log".format(operations_dir, active_svr, user)
    appscript.app('Terminal').do_script(active_cmd)
    #os.system("start cmd /K {}".format(active_cmd))
    #Popen('xterm -hold -e "{}}"'.format(active_cmd))
    #appscript.app('Terminal').do_script(active_cmd)

    if len(sys.argv) == 3:
        failover_svr = sys.argv[2]
        failover_cmd = "cd {}; fab -H {} -u {} tail_rabbit_log".format(fab_dir, failover_svr, user)
        appscript.app('Terminal').do_script(failover_cmd)

def main(todo_file,drivers=None,all_instances=None):
    TODOLIST = todo_file

    if drivers is None:
        print "Getting Providers drivers"
        drivers = libcloud_api.init_all_drivers()

    if all_instances is None:
        print "Getting all instances"
        all_instances = libcloud_api.get_all_instances(drivers)

    started_proxies_list = []
    dt_proxy_regex = re.compile("\.(it|hk|fr)\.")
    with open(TODOLIST, "r") as todolist_file:
        for line in todolist_file:
            today_regex = re.compile("{},(.*),TODO".format(DATE))
            line = line.rstrip()
            print "{}".format(line)
            if today_regex.search(line):
                PROXY = line.split(',')[1]
                if dt_proxy_regex.search(PROXY):
                    print "{} is a DT proxy. Currently cannot verify status. Running woopas."
                    sleep(2)
                else:
                    print "Checking Running State of {}".format(PROXY)

                    '''aws_reg_regex = re.compile("^[^\.]*\.node\([a-z]*-[a-z]*-[0-9]\).*")
                    region = aws_reg_regex.sub("\1", PROXY)'''

                    proxy_info = None
                    proxy_regex = re.compile(PROXY)
                    for driver_name in all_instances:
                        for instance in all_instances[driver_name]:
                            if proxy_regex.search(instance.name):
                                proxy_info = instance
                                print "[DEBUG] Found instance: {}".format(instance)
                                break
                        if proxy_info is not None:
                            break

                    if proxy_info is None:
                        print "ERROR: Could not find {} in the list of instances. Contacting providers".format(PROXY)
                        proxy_info, driver = libcloud_api.get_instance(PROXY, drivers)
                        if proxy_info is None:
                            print "ERROR: Could not verify the status of {}.".format(PROXY)
                            if fabfile._get_option(">> Would you like to continue deployment on this node?") != "Y":
                                print "Skipping. Please check manually."
                                sleep(2)
                                continue
                    mydriver = {}
                    mydriver[driver_name] = drivers[driver_name]
                    #proxy_info, driver = libcloud_api.get_instance(PROXY, mydriver)
                    print ""
                    print "{} ({}) is {}.".format(PROXY, proxy_info.name, proxy_info.state)
                    if proxy_info.state == "stopped":
                        print "Starting..."
                        if not libcloud_api.start_instance(PROXY, mydriver):
                            print "ERROR: Failed to start {}".format(PROXY)
                            if fabfile._get_option("Continue with deployment on {}".format(PROXY)) == "n":
                                continue
                        else:
                            started_proxies_list.append(PROXY)

                print "Opening servers log tail windows"
                new_window(PROXY, FAB_USERNAME)

                woopas_cmd = "woopas upgrade_proxy {}".format(PROXY)
                print "{}".format(woopas_cmd)
                woopas_open = Popen(woopas_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
                out, err = woopas_open.communicate()

                #if err == '':
                print "{}".format(out)
                print "*****************************************************************************"
                print "Updated {}. Updating todo list at {}...".format(PROXY, TODOLIST)
                print "*****************************************************************************"
                update_proxy_status(PROXY,TODOLIST)

                print "*****************************************************************************"
                print "Checking status of {}".format(PROXY)
                print "*****************************************************************************"

                try:
                    with settings(host_string = PROXY, user = FAB_USERNAME):
                        fabfile.check_proxy()
                except Exception as e:
                    print "Failed to run check on {}\nError: {}\nPlease check manually".format(PROXY, e.message)
                    if fabfile._get_option() == "n":
                        sys.exit("Deployment aborted")
                print "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"
                print "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"

                sleep(1)
                '''else:
                    print "Failed to run woopas."
                    print "{}\n-----\n-----\n{}".format(out,err)'''

    print "** Deployment completed."
    if len(started_proxies_list) > 0:
        print "Stopping started instances"
        libcloud_api.stop_instance(started_proxies_list)

if __name__ == "__main__":

    if len(sys.argv) == 2:
        TODOLIST = sys.argv[1]
    else:
        sys.exit("Missing Argument! Usage: woopas_amigo.py <file>")

    main(TODOLIST)
