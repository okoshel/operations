#!/bin/bash

########################################################################
# prepare_version_file.sh
# Author: Arturo Noha
# Version 3.0
# Date: 13 April 2017
#---------------------------------------------------------------
# This script generates the new release .sh file
# 1. Gets the lis of services to be updated from a file provided by the user
# 2. gets the current versions from versions.yaml (or a specific file provided)
# 3. Gets the new versions to be installed from the Amazon S3 versions repository
# 4. Renames the module package folder to the new version number
# 5. If in the release, will update delta and filterservice separate configuration
# --> Uses STAGING as default environment if no environment is specified
#---------------------------------------------------------------
# INPUT FILES (can be passed as arguments to override defaults)
#
# * RELEASE_SERVICES_FILE: file with the list of services to update
#   - format: <service_name/alias>
#             proxy_enterprise
#             filterservice
#   - default: No default, MUST BE PROVIDED with the relevant services
#
# * SERVICES_FILE: file containing the list of Wandera services
#   - format: service:<service_name>:[service_alias]:[server]
#             service:deltaloadservice-enterprise:deltaload:fid
#             service:emm-connect:enterprise_connectorservice:con
#             service:userservice2-enterprise:userservice2_enterprise:app
#   - default: release_services/all_services.info
#
# * VERSIONS_FILE: yaml file containing the current version deployed for the services
#   - format: [options:]<service_name/alias>:[options] "<version>"
#             enterprise_connectorservice::install::connector_version: "2.1.1-7"
#             service::sae::version: "1.10.0-201703171328-1"
#   - default: puppet/hieradata/environments/staging/versions.yaml
#########################################################################

## DEFAULT VALUES ##

PUPPET_PATH=./puppet # Users's puppet folder
PUPPET_ENV="staging"
ENV_PATH=$PUPPET_PATH/environments/$PUPPET_ENV

#RELEASE_SERVICES_FILE=./release_services.list
SERVICES_FILE=./release_services/all_services.info # Need to defined real default file

VERSIONS_PATH=$PUPPET_PATH/hieradata/environments/$PUPPET_ENV
VERSIONS_FILE=$VERSIONS_PATH/versions.yaml
DELTA_CONFIG=$PUPPET_PATH/hieradata/environments/operations/common.yaml
FILTER_CONFIG=PUPPET_PATH/hieradata/environments/operations/role/mysql_slave.yaml
version_format="^[0-9]\{1,\}\.[0-9]\{1,\}\.[0-9]\{1,\}[-]*"

LOADER_SCRIPT=./release_version_loader.2.1.py
LOADER_CMD="python $LOADER_SCRIPT"

# END OF DEFAULT VALUES

## FUNCTIONS ##

usage() {
	echo "USAGE: $0 --relservices <release_services_file> [options]" 1>&2;
	echo "		--relservices : file with the services upgraded in the new release."
	echo "		--allservices : file with the list of services available. Default: $SERVICES_FILE"
	echo "		--puppet : puppet repository folder. Default, current folder"
	echo "		--env : environemt to deploy the release. Default: staging"
	echo "		--versions : yaml file containing the current versions deployed. Default: $VERSIONS_FILE"
	echo "		--help"
	exit 1;
}

display_options() {
	if [[ $# -lt 1 ]]; then
		choice="null"
	else
		choice=$1
	fi

	clear
	echo "Creating the new release script:"
	echo "- Puppet root: $PUPPET_PATH"
	echo "- Puppet environment: $ENV_PATH"
	echo "- Wandera Services file: $SERVICES_FILE"
	echo "- Release services file: $RELEASE_SERVICES_FILE"
	echo "- Current versions file: $VERSIONS_FILE"
	echo
	if [[ $choice == "null" ]]; then
    read -p "Do you wish to proceed? [Y/n]: " choice
  else
    read -p "Please enter a valid option. Do you wish to proceed? [Y/n]: " choice
  fi

	while [[ "$choice" != "Y" && "$choice" != "n" ]]; do
		display_options
	done

	if [[ "$choice" == "n" ]]; then
		echo "EXIT. BYE"
		exit 0
	fi
}

get_current_version() {
	local service=$1
	regex=":$service[,:]"
	aliases=$(egrep "$regex" $SERVICES_FILE | cut -d':' -f2,3 | sed -e "s/[,:]/|/g")
	regex="(^($aliases):|:($aliases)[:$])"
	current=$(egrep "$regex" $VERSIONS_FILE | cut -d'"' -f2 | grep "$version_format")
	eval "$2=$current"
}

function update_module {
  local module_name=$1
  local old_version=$2
  local new_version=$3

  # We don't copy folders for generic service module services
  if  [ $module_name == "portal" ] || [ $module_name == "service_filterservice" ] || [ $module_name == "service_publisher" ] || [ $module_name == "service_dataplan" ] || [ $module_name == "service_siem" ]; then
    # check the passed module name is goodgit
    # change version number of package to be installed
    perl -pi -e "s/${old_version}/${new_version}/g" "${VERSIONS_FILE}"
		echo "INFO: $module_name does not have a version folder. Skipping folder renaming"
    return 0

  else

	  # change version number of package to be installed
	  perl -pi -e "s/${old_version}/${new_version}/g" "${VERSIONS_FILE}"

    folders="files templates"

    for folder in $folders ; do
      # check if old versions exists
      if [ -d "${PUPPET_PATH}/environments/${PUPPET_ENV}/modules/${module_name}/${folder}/${old_version}" ] ; then
        # check the new version doesnt exist
          if [ ! -d "${PUPPET_PATH}/environments/${PUPPET_ENV}/modules/${module_name}/${folder}/${new_version}" ] ; then
						echo "mv \"${PUPPET_PATH}/environments/${PUPPET_ENV}/modules/${module_name}/${folder}/${old_version}" "${PUPPET_PATH}/environments/${PUPPET_ENV}/modules/${module_name}/${folder}/${new_version}\""
            mv "${PUPPET_PATH}/environments/${PUPPET_ENV}/modules/${module_name}/${folder}/${old_version}" "${PUPPET_PATH}/environments/${PUPPET_ENV}/modules/${module_name}/${folder}/${new_version}"
          fi
      else
        echo "WARN: ${module_name}/${folder}/${old_version} does not exist in ${PUPPET_ENV} environment. Skip renaming new version folder"
        return 1
      fi
    done
  fi
    return 0
}

parse_options() {
	while getopts ":-:" opt; do
		case "$opt" in
			-)
				case "$OPTARG" in
					"new")
						RELEASE=${!OPTIND}
						OPTIND=$((OPTIND + 1))
						;;
					"allservices")
						SERVICES_FILE=${!OPTIND}
						OPTIND=$((OPTIND + 1))
						;;
					"relservices")
						RELEASE_SERVICES_FILE=${!OPTIND}
						OPTIND=$((OPTIND + 1))
						;;
					"puppet")
						PUPPET_PATH=${!OPTIND}
						OPTIND=$((OPTIND + 1))
						;;
					"env")
						PUPPET_ENV=${!OPTIND}
						OPTIND=$((OPTIND + 1))
						;;
					"versions")
						versions_arg=${!OPTIND}
						OPTIND=$((OPTIND + 1))
						;;
					"help")
						usage
						;;
				esac
				;;
			*)
				usage
				;;
		esac
	done

	#if [[ $RELEASE == "" || $RELEASE_SERVICES_FILE == "" ]]; then
	if [[ $RELEASE_SERVICES_FILE == "" ]]; then
					echo "** ERROR: Missing the release verions file"
					echo ""
	        usage
	fi

	# Check the validity of the paths and files provided
	ENV_PATH=$PUPPET_PATH/environments/$PUPPET_ENV
	if [[ ! -d $ENV_PATH ]]; then
		echo "ERROR : Puppet environment path $ENV_PATH is not a folder"
		exit 1
	fi
	DELTA_CONFIG=$PUPPET_PATH/hieradata/environments/operations/common.yaml
	FILTER_CONFIG=$PUPPET_PATH/hieradata/environments/operations/role/mysql_slave.yaml

	VERSIONS_PATH=$PUPPET_PATH/hieradata/environments/$PUPPET_ENV
	VERSIONS_FILE=$VERSIONS_PATH/versions.yaml
	if [[ $versions_arg ]]; then
		if [[ -f $versions_arg ]]; then
			# if the versions file exists as provided use it as the full path for VERSIONS_FILE
			VERSIONS_FILE=$versions_arg
		elif [[ $versions_arg && -f $VERSIONS_PATH/$versions_arg ]]; then
			# if the versios file does not exist, check it it exists in the puppet versions path and set it so
			VERSIONS_FILE=$VERSIONS_PATH/$versions_arg
		else
			# if the file does not exist as a ful path or in the puppet versions path, then use the default
			echo "WARNING : Versions file $versions_arg not found. Using default $VERSIONS_FILE"
		fi
	fi

	if [[ -f $VERSIONS_FILE ]]; then
		echo "WARNING : No current versions file provided. Using default $VERSIONS_FILE"
	else
		echo "ERROR : Versions file $VERSIONS_FILE not found."
		exit 1
	fi

	if [[ ! -f $RELEASE_SERVICES_FILE ]]; then
		echo "ERROR : Source file $RELEASE_SERVICES_FILE not found"
		exit 1
	fi

	if [[ ! -f $SERVICES_FILE ]]; then
		echo "ERROR : Services file $SERVICES_FILE not found"
		exit 1
	fi
}

################### MAIN ##################

parse_options $@

display_options

current_dir=$(pwd)
cd $PUPPET_PATH
echo "Available branches"
git branch
active_branch=$(git branch | grep "^\*" | awk '{print $2}')
read -p "- Please select a branch (active: $active_branch): " deploy_branch
if [[ $deploy_branch == "" ]]; then
	deploy_branch=$active_branch
fi

while [[ ! `git branch | grep "[ \t]$deploy_branch$"` ]]; do
	read -p "Please select a valid branch from athe above list: " deploy_branch
done
git checkout $deploy_branch
if [[ $? != 0 ]]; then
	echo "ERROR: Could not checkout to selected branch. Please fix errors and re-run"
	exit 1
else
	#git branch
	#sleep 5
	git pull
fi

cd $current_dir

# Make a copy of the original versions file to diff at the end
echo "Creating temporary copy of $VERSIONS_FILE"
if [[ -f $VERSIONS_FILE.tmp ]]; then
	rm -f $VERSIONS_FILE.tmp
fi
cp -p $VERSIONS_FILE $VERSIONS_FILE.tmp

# get the latest service releases from AWS S3 releases reporitory
# and iterate through the list of services deployed in Wandera platform
echo "Getting the latest version for all services from AWS S3 repository"

#$LOADER_CMD -f $SERVICES_FILE | grep "^Software" | sed -e "s/^Software \([^( ]*\) (*\([^= )]*\))* *==\> \([0-9.-]\{1,\}\)/\1:\2:\3/g" | while read line; do
$LOADER_CMD -f $SERVICES_FILE | while read line; do
	if [[ ! `echo $line | grep "^Software"` ]]; then
		echo $line
	else
		line=$(echo $line | sed -e "s/^Software \([^( ]*\) (*\([^= )]*\))* *==\> \([0-9.-]\{1,\}\)/\1:\2:\3/g")
		#echo "[DEBUG] Processing $line"
		service=$(echo $line | cut -d':' -f1)
		alias=$(echo $line | cut -d':' -f2)
		newversion=$(echo $line | cut -d':' -f3)

		if [[ $alias == "" ]]; then
			regex="^$service($|:)"
		else
			regex="^($service|$alias)($|:)"
		fi

		# check if the servie is affected by the release
		if [[ `egrep "$regex" $RELEASE_SERVICES_FILE` ]]; then
			if [[ `echo $service | grep "filterservice"` ]]; then
				echo "INFO: $service is not in the global configuration."
				echo "Updating $FILTER_CONFIG"
				if [[ -f $FILTER_CONFIG.tmp ]]; then
					rm -f $FILTER_CONFIG.tmp
				fi
				cp -p $FILTER_CONFIG $FILTER_CONFIG.tmp
				#sed -i -e "s/\(.*\):filterservice:\(.*\)\".*\"$/\1:deltaload:\2\"$newversion\"/g" $FILTER_CONFIG
				sed -i -e "s/\(.*\):filterservice:\([^\"]*\)\"[^\"]*\"$/\1:filterservice:\2\"$newversion\"/g" $FILTER_CONFIG
			elif [[ `echo $service | grep "delta"` ]]; then
				echo "INFO: $service is not in the global configuration."
				echo "Updating $DELTA_CONFIG"
				if [[ -f $DELTA_CONFIG.tmp ]]; then
					rm -f $DELTA_CONFIG.tmp
				fi
				cp -p $DELTA_CONFIG $DELTA_CONFIG.tmp
				#sed -i -e "s/\(.*\):deltaload:\(.*\)\".*\"$/\1:deltaload:\2\"$newversion\"/g" $DELTA_CONFIG
				sed -i -e "s/\(.*\):deltaload:\([^\"]*\)\"[^\"]*\"$/\1:deltaload:\2\"$newversion\"/g" $DELTA_CONFIG
			else
				echo "* Processing service $service (alias: $alias)"
				# find the current verion deployed and update the release script with service name - current versions - new version
				get_current_version $service current_version
				# Get the right name for the service to use in the files and paths based on versions.yaml configuration
				if [[ `egrep "(^|:)$service:" $VERSIONS_FILE` ]]; then
					service_name=$service
				else
					service_name=$alias
				fi
				echo "update_module \"$service_name\" \"$current_version\" \"$newversion\""
				update_module "$service_name" "$current_version" "$newversion"
				echo "---"
			fi
		else
			echo "INFO: $service ($alias) is not in this release. Skipping it."
		fi
	fi
done

echo ""
echo "---------------------------------------------------"
echo "Update completed. Please check following files:"
echo ""
echo "- $VERSIONS_FILE"
echo ""
echo "diff to previous version (\"diff $VERSIONS_FILE $VERSIONS_FILE.tmp\"):"
diff $VERSIONS_FILE ${VERSIONS_FILE}.tmp
echo ""
echo "====================="
echo ""
echo "- $DELTA_CONFIG"
echo ""
diff $DELTA_CONFIG ${DELTA_CONFIG}.tmp
echo ""
echo "====================="
echo ""
echo "- $FILTER_CONFIG"
echo ""
diff $FILTER_CONFIG ${FILTER_CONFIG}.tmp

read -p "Would you like to commit changes to bitbucket? [Y/n] " option
while [[ $option != "Y" && $option != "n" ]]; do
	read -p "Please enter a valid choice: [Y/n] " option
done

if [[ $option == "Y" ]]; then
	cd $PUPPET_PATH
	git checkout $deploy_branch
	if [[ $? != 0 ]]; then
		echo "ERROR: Could not checkout to branch $deploy_branch. Please fix errors and re-run"
		exit 1
	else
		git branch
		git pull
		git add -Ap
		echo ""
		echo "Would you wish to commit this changes? [Y/n]"
		while [[ $option != "Y" && $option != "n" ]]; do
			read -p "Please enter a valid choice: [Y/n]" option
		done
		if [[ $option == "Y" ]]; then
			echo "Please enter a message for the commit: "
			read commit_msg
			echo "git commit -m $commit_msg"
			git commit -m "$commit_msg"
			git push origin $deploy_branch
		fi
	fi
fi
