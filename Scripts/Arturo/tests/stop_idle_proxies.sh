#!/bin/bash

# Argument: file with list of started proxies. List of proxy names

if [[ $# -lt 1 ]]; then
  echo "ERROR: Missing file with list of proxies to stop"
  exit 1
fi

for proxy in `cat $1`; do
  echo "*** Stopping $proxy"
  echo "--------------"
  region=$(echo $proxy | sed -e "s/^[^\.]*\.node\.\([a-z]*-[a-z]*-[0-9]\).*/\1/g")
  python ./call_libcloud.py --stop-instance $proxy --regions $region --providers aws
  echo "Waiting for instance to stop..."
  while [[ ! `python ./call_libcloud.py --show-instance $proxy --regions $region --providers aws | grep stopped` ]]; do
    sleep 5
  done
  python ./call_libcloud.py --show-instance $proxy --regions $region --providers aws | grep $proxy
  echo "================================="
done
