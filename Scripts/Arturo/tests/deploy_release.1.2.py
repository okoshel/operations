#!/usr/local/bin/python

#####################################################################
# deploy_release.py
# Author: Arturo Noha
# Version: 1.0
#-----------------------------------------------------------------
# Script to deploy the relevant services for a new release
#   1. get the cloud providers (from libcloud_api.py)
#   2. get all the instances from the providers (from libcloud_api.py)
#   3. iterate through the list of services provided
#   4. based on the server type on which the service is running (from allservices file) get the nodes on which it needs to be deployed
#   5. deploy and check on each relevant node (from fabfile.py)
#------------------------------------------------------------------
# Syntax: python deploy_release.py --u <ssh_username> --env <environment> --relservices <services_file> [--allservices <all_services_file>]
#   -u: user name to use for the ssh connections to the servers. REQUIRED
#   --env: environment to deploy: stgn / prod / dev / intg. REQUIRED
#   --relservices: file with the list of services that need to be deployed for the release. REQUIRED
#   --allservices: file with the info about the services.
#       - format: <service_name>:<service_alias>:<server_type>
#           . service_name: as found in AWS S3 repository
#           . service_alias: name used for configuration (if different from AWS)
#           . server_type: server on which the service runs: app / fid / fis / con / sie / web / prx
#####################################################################

import libcloud_api
import os
import sys
from pprint import pprint
import argparse
import re
from fabric.context_managers import settings
from fabric.api import env
from fabfile import *
from time import sleep
import imp
import woopas_amigo
#woopas = imp.load_source("woopas_amigo", "/Users/arturonoha/Documents/bitbucket/wandera_woopas/Woopas_PY/woopas_amigo.py")

service_types = ['app','web','fid','fis','con','sie','prx']
server_check_function = {
    'app' : 'check_app',
    'fid' : 'check_delta',
    'fis' : 'check_filterservice',
    'con' : 'check_connector',
    'sie' : 'check_siem',
    'prx' : 'check_proxy',
    'web' : 'check_portal'
}
puppet_master = "puppet.snappli.net"
puppet_master_dir = "/opt/puppet"
environments = ['prod','stgn','dev','intg','core']
live_environments = ['prod','core','cops']
woopas_services = ['proxy-enterprise','rabbit-enterprise','proxy_enterprise','proxy']

def get_aws_instances():
    options = parse_options(sys.argv[1:])
    regions = libcloud_api.get_regions('EC2')
    print "{}".format(regions)

def get_input(message = None):
    if message is None:
        display_message = "Would you wish to continue? [Y/n]: "
    else:
        display_message = message + " [Y/n]: "

    option = raw_input(display_message)
    while option not in valid_options:
        option = raw_input("Please enter a valid option [Y/n]: ")

    return option

def stopped_instance_options(name = ""):
    options = ['1','2','3']
    option = raw_input("WARNING: {} is stopped. What would you wish todo?\n".format(name) +
                    "\t1. Start and deploy\n" +
                    "\t2. Deploy without starting\n" +
                    "\t3. Skip\n" +
                    "Your choice: ")

    while option not in options:
        print "$ Please select a valid option: {}".format(options)

    return option

def deploy(myhost, user, name):
    try:
        with settings(host_string = myhost, user = user):
            return deploy_server(name)
    except Exception as e:
        print "ERROR: Could not deploy on {}".format(myhost)
        print "Exception: {}".format(e.message)
        print "{}".format(e)
        return -1
    sleep(5)

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('--relservices', dest = "services_TODO", help = "File containing the list of services to deploy")
    parser.add_argument('--allservices', dest = "all_services_file", default = "./all_services.info", help = "File containing all services and their info")
    parser.add_argument('-u', dest = "fab_user")
    parser.add_argument('--env', dest = "environ")
    parser.add_argument('--nodes', dest = "nodes_TODO_file", help = "Optional: file with the list of nodes to deploy to")

    return parser.parse_args()

def display_options(user, env, relservices, allservices, nodes_todo):
    print "-------------------------------------------------"
    print "Running deployment with following options:"
    print "\t- user: {}".format(user)
    print "\t- environment: {}".format(env)

    release_services_list = []
    with open(relservices, "r") as rel_services_file:
        for service in rel_services_file:
            release_services_list.append(service.rstrip())
    print "\t- Relsease services file: {} ({})".format(relservices, release_services_list)

    print "\t- All services info: {}".format(allservices)
    if nodes_todo is not None:
        nodes = []
        with open(nodes_todo, "r") as nodes_list:
            for node in nodes_list:
                nodes.append(node.rstrip())
        print "\t- List of target nodes: {} ({})".format(nodes_todo, nodes)

    return get_input()

def usage():
    sys.exit("USAGE: {} -u <ssh_username> --env <prod/stgn/dev/intg> --relservices <release_services_file> [options]\n".format(sys.argv[0]) +
            "\t* Options:\n\t--allservices: file with the inforamtion of all available services. Default: ./all_services.info" +
            "\n\t--nodes: File with the list of nodes to deploy to if not all nodes required")

#############  MAIN  #############

if __name__ == "__main__":
    #(options, args) = parse_options(sys.argv[1:])
    options = parse_options(sys.argv[1:])
    #print "\nArguments: {}\n".format(options)
    if not options.fab_user:
        print "\nERROR: Missing ssh user (-u <username>)\n"
        usage()
    fab_user = options.fab_user

    if not options.services_TODO:
        print "\nERROR: Missing file with list of services to deploy (--relservices <file>)\n"
        usage()
    if not os.path.isfile(options.services_TODO):
        sys.exit("ERROR: {} not found".format(options.services_TODO))

    if not options.environ or options.environ not in environments:
        print "\nERROR: Missing environment (--env <prod / stgn / dev / intg>)\n"
        usage()
    environ = options.environ

    if display_options(fab_user, environ, options.services_TODO, options.all_services_file, options.nodes_TODO_file) != "Y":
        sys.exit(1)

    providers, provider_aliases = libcloud_api.get_providers()

    if options.nodes_TODO_file:
        TODOLIST = options.nodes_TODO_file
    else:
        TODOLIST = None

    # Getting the instances
    print "INFO: Getting list of instances"
    all_instances = libcloud_api.get_all_instances()
    #print "[DEBUG] number of instances: {}".format(len(all_instances))

    # Git Pull on puppet
    with settings(host_string = puppet_master, user = fab_user):
        git_pull(puppet_master_dir)

    app_done = False

    # read through the file of services to deploy and deploy to required instances
    with open(options.services_TODO,"r") as services_list:
        for service in services_list:
            service = service.rstrip()
            if (environ == "prod" or environ == "core") and service in woopas_services:
                print "WARNING: Woopas run not implemented yet. Please run Woopas to deploy {}".format(service)
                if TODOLIST is None:
                    TODOLIST = raw_input("Please enter the file with the woopas plan: ")
                elif get_input("List of nodes will be taken from {}. Continue? ".format(TODOLIST)) == "n":
                    TODOLIST = raw_input("Please enter the file with the woopas plan: ")
                while not os.path.isfile(TODOLIST):
                    TODOLIST = raw_input("\"{}\" not found. Please enter a valid file: ".format(TODOLIST))

                woopas_amigo.main(TODOLIST,None,all_instances)
            else:
                print "[DEBUG] Checking {}".format(service)
                ## get type of server (app, web, fid...) from all_services_file file
                service_type = None
                service_regex = re.compile(".*{}.*".format(service), re.VERBOSE)
                with open(options.all_services_file,"r") as all_services:
                    for line in all_services:
                        #print "{}".format(line.rstrip())
                        if service_regex.search(line.rstrip()):
                            service_type = line.rstrip().split(':')[3]
                print "service_type = {}".format(service_type)
                if service_type not in service_types:
                    print "ERROR: Unknown service type for {}".format(service)
                elif service_type == "app" and app_done:
                    print "INFO deployment already done on app servers. Skipping"
                else:
                    # go through instnces and deploy on matching ones
                    instances_TODO = {}
                    if environ in live_environments and service_type == "fis":
                        service_regex = re.compile("^{}-[0-9]*-cops".format(service_type), re.VERBOSE)
                    elif environ in live_environments:
                        service_regex = re.compile("^{}-[0-9]*-core".format(service_type), re.VERBOSE)
                    else:
                        service_regex = re.compile("^{}-[0-9]*-{}".format(service_type, environ), re.VERBOSE)
                    if TODOLIST is None:
                        for driver in all_instances:
                            for instance in all_instances[driver]:
                                if service_regex.search(instance.name):
                                    name = instance.name
                                    instances_TODO[name] = instance
                    else:
                        with open(TODOLIST, "r") as todo_nodes:
                            for node in todo_nodes:
                                node = node.rstrip()
                                #print "looking for node: {}".format(node)
                                node_regex = re.compile(node)
                                if service_regex.search(node):
                                    print "Looking for {} instance for {}".format(service_type, node)
                                    found = False
                                    for driver in all_instances:
                                        for instance in all_instances[driver]:
                                            #print "{}".format(instance.name)
                                            if node_regex.search(instance.name):
                                                instances_TODO[node] = instance
                                                found = True
                                                break
                                        if found:
                                            break
                                    if not found:
                                        print "ERROR: {} not found".format(node)

                    if len(instances_TODO) == 0:
                        print "No nodes found for service \"{}\". Skipping...".format(service)
                    else:
                        print "{} will be deployed on:".format(service)
                        for name in instances_TODO:
                            print "{} ({})".format(name, instances_TODO[name].state)

                        #cont = get_input()
                        if get_input() == "Y":
                            for name in instances_TODO:
                                instance = instances_TODO[name]
                                state = instance.state
                                host = instance.public_ips[0]
                                print "* Deploying to {} (currently {})".format(name, state)
                                if get_input("Proceed with deployment on {}".format(name)) != "Y":
                                    continue
                                if state != "running":
                                    option = stopped_instance_options(name)
                                    if option == '3':
                                        print "INFO: Skipping deployment of {}".format(name)
                                        continue
                                    else:
                                        if option == '1':
                                            #driver = instances_TODO[name]["driver"]
                                            print "INFO: Starting {}".format(name)
                                            #sleep(2)
                                            libcloud_api.start_instance(name)
                                        deploy_return = deploy(host,fab_user,name)
                                        if deploy_return == 3:
                                            continue
                                        elif deploy_return != 0:
                                            if get_input("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes?".format(name)) == "Y":
                                                continue
                                            else:
                                                sys.exit(1)
                                        elif service_type == "app":
                                                app_done = True
                                else:
                                    if deploy(host,fab_user,name) != 0:
                                        if get_input("ERROR: Deployment failed on {}.\nWould you wish to continue deploying the rest of nodes?".format(name)) == "Y":
                                            continue
                                        else:
                                            sys.exit(1)
                                    else:
                                        if service_type == "app":
                                            app_done = True
                                        if get_input("\nDeployment of {} completed. Continue with rest of nodes?".format(name)) != "Y":
                                            sys.exit("BYE")
