#!/usr/bin/env python

import sys
import os
import json

from otcclient.core.userconfigaction import userconfigaction
from otcclient.core.configloader import configloader
from otcclient.core.OtcConfig import OtcConfig
from otcclient.plugins.ecs.ecs import ecs

from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

import libcloud.security

def print_json(mydict,tabs=""):
    for key in mydict:
        if type(mydict[key]).__name__ == "dict":
            print "{}{} : {{".format(tabs, key)
            print_json(mydict[key],"{}\t".format(tabs))
            print "{}}}".format(tabs)
        else:
            print "{}{} : {}".format(tabs,key,mydict[key])

if __name__ == '__main__':
    configloader.readUserValues()
    configloader.readProxyValues()
    configloader.validateConfig()

    OtcConfig.OUTPUT_FORMAT = "Json"

    token = json.loads(ecs.getIamToken())
    print "Token:\n"
    print_json(token)

    '''print "\n\nCatalog:"
    for inst in token["token"]["catalog"]:
        print "\n{}".format(inst)'''

    '''OpenStack = get_driver(Provider.OPENSTACK)
    driver = OpenStack('otc-api-test', 'WanderaOTC123*',
                   ex_force_auth_url='http://192.168.1.101:5000',
                   ex_force_auth_version='2.0_password',
                   ex_force_base_url='https://iam.eu-de.otc.t-systems.com/v3',
                   ex_force_auth_token=token)

    driver.list_nodes()'''

    instances = ecs.describe_instances()
    print "\nInstances:\n{}".format(instances)

    addresses = ecs.describe_addresses()
    print "\nAddresses:\n{}".format(addresses)

    print "\nVolumes:\n{}".format(ecs.list_volumes())

    #ecs.describe_vpcs()
