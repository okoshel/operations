#####################################################################
# libcloud_api.py
# Author: Arturo Noha
# Version: 1.0
#-----------------------------------------------------------------
# Functions for the access and management of cloud instances.
# Available funtions:
# * get_providers(): return the list of providers and thier aliases
# * get_regions(provider): return an array with all the regions for a given provider. Retunrs an empty array if failed or not applicable
#
# * get_rackspace_credentials(): Return username and access key for each Rackspace region.
#   - credentials must be stored in environment variables named: RACKSPACE_<region>_username and RACKSPACE_<region>_accesskey.
# * get_aws_credentials(): Return AWS Access Key Id and Access Secret Key.
#   - Credentials must be stores in environment variables named: AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
#
# * init_softlayer_driver(): returns the driver to SoftLayer cloud
#   - returns a dictionary with a single keypair value: key = SL / value = the driver
# * init_openstack_driver(): returns the driver to the Deutsche Telekom cloud
#   - returns a dictionary with a single keypair value: key = DT / value = the driver
# * init_rackspace_driver(regions): returns the drivers for the required Rackspace regions
#   - takes an array with the regions for which to get the drivers
#   - returns a dictionary where each entry is the driver for a region, the key being a string with the format RACKSPACE_<region>. i.e. RACKSPACE_lon
# * init_ec2_driver(regions)
#   - takes an array with the regions for which to get the drivers
#   - returns a dictionary where each entry is the driver for a region, the key being a string with the format AWS_<region>. i.e. EC2_eu-west-1
# * init_all_drivers(): get the drivers for all available providers based on initialisers above
# -->> All driver initialisers return an empty dictionary if fail to retrieve any drivers
#
# * get_all_instances(): get all instances from all providers.
#   - Returns a dictionary whith key = driver_name and value = array of all instances for the given driver
# * get_instance(name,mydrivers=None): get the raw details of a given instance.
#   - Takes the instance name as first argument
#   - Takes a specific driver to search. If none given, it will search through all providers for the instance
# * display_instance(drivers=None,instance_to_show=None): display key info (name, state, id, public ips) for required instnaces
#   - Arguments (optional):
#       1. dictionary of drivers (key: driver name, i.e. RACKSPACE_lon, AWS_eu-west-1, SL, DT). In none provided, will browse through all providers
#       2. name of the instance to show. If none provided, will display all insances for the relevant dirvers
#
# * stop_instance(name,mydrivers={}): stop the instance(s) passed as first argument (mandatory)
# * start_instance(name,mydrivers={}): start the instance(s) passed as first argument (mandatory)
#   - MANDATORY Argument for stop/start_instance:
#       - an instance or and aray of instance to stop
#   - Optional Argument for stop/start_instance:
#       - dictionary of drivers in which to search for the instance. Key = driver_name, Value = driver_name
#       - if no specific driver(s) provided, will look for the instance in all providers
#####################################################################

import os
import sys
import argparse
import optparse
from pprint import pprint
import datetime
import json
import re
import boto3

#import libcloud
from libcloud.compute.types import Provider as computeProvider
from libcloud.compute.providers import get_driver as get_compute_driver
from libcloud.compute.drivers import ec2, rackspace

from libcloud.loadbalancer.base import Member, Algorithm
from libcloud.loadbalancer.types import State
from libcloud.loadbalancer.types import Provider as lbProvider
from libcloud.loadbalancer.providers import get_driver as get_lb_driver

from libcloud.dns.providers import get_driver as get_dns_driver
from libcloud.dns.types import Provider as dnsProvider
from libcloud.dns.types import RecordType

from wandera_woopas.database import FileDatabase

#### GLOBAL VARIABLES ####
home = os.path.expanduser('~')
CONFIG_FILE = os.path.join(home, ".woopas.json")
#CONFIG_FILE = ".creds.json"
route53_hosted_zone = "wandera.com"

#providers = [ 'EC2', 'RACKSPACE', 'SOFTLAYER', 'OPENSTACK' ]
providers = [ 'EC2', 'RACKSPACE', 'SOFTLAYER', 'OPENSTACK' ]
provider_aliases = { 'EC2' : ['aws','AWS','ec2'],
                    'RACKSPACE' : ['rackspace','rs','RS'],
                    'SOFTLAYER' : ['softlayer','sl','SL'],
                    'OPENSTACK' : ['dt','DT']
            }
aws_regions = ec2.EC2NodeDriver.list_regions()
rs_regions = rackspace.ENDPOINT_ARGS_MAP.keys()
#rackspace_cred = { "lon": { "username" : "arturonohauk", "accesskey" : "51e74c3edf2a486997a181cc278fd3a5" } }

providers_credentials = {}
rackspace_credentials = {}
#softlayer_cred = { "username" : "arturonoha" , "accesskey" : "69b0bf01eaff947bdea96a3c5e02966ade1d7dd5d0c1c17988d5241297cde4f5" }
#DT_cred = { "username" : "14651301 OTC00000000001000000675" , "accesskey" : "w3g4pyLokueXdgd0ROPLIlehyQPSBL7vy5cQp99V" }

#### FUNCTIONS ####

# Utils
def info(message):
    timestamp = datetime.datetime.now()
    print "{} [INFO] {}".format(timestamp, message)

def get_config_data():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()
    try:
        config_json = json.loads(config)
    except Exception as e:
        sys.exit("ERROR: Failed to load config from {}\n".format(CONFIG_FILE))

    return config_json

# Initialise accesses
def get_providers():
    return providers, provider_aliases

def get_credentials():
    if not os.path.isfile(CONFIG_FILE):
        sys.exit("\nERROR: Configuration file {} not found\n".format(CONFIG_FILE))

    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        providers_info = config_json["providers"]
    except:
        sys.exit("ERROR: Invalid configuration file format ({})".format(CONFIG_FILE))

    #print "[DEBUG] providers_info from config file:\n{}".format(providers_info)
    if "AWS_ACCESS_KEY_ID" in providers_info and "AWS_SECRET_ACCESS_KEY" in providers_info:
        AWSKEY = providers_info["AWS_ACCESS_KEY_ID"]
        AWSSECKEY = providers_info["AWS_SECRET_ACCESS_KEY"]
        providers_credentials["EC2"] = { "username" : AWSKEY, "accesskey" : AWSSECKEY }
    else:
        sys.exit("\nERROR: Credentials not found for AWS: AWS_ACCESS_KEY_ID / AWS_SECRET_ACCESS_KEY\n")

    for region in rs_regions:
        if region not in rackspace_credentials:
            username_var = "RACKSPACE_{}_username".format(region)
            accesskey_var = "RACKSPACE_{}_accesskey".format(region)

            if username_var in providers_info and accesskey_var in providers_info:
                username = providers_info[username_var]
                accesskey = providers_info[accesskey_var]
                rackspace_credentials.update( { region : { "username" : username, "accesskey" : accesskey } } )
            '''else:
                print "WARN: Credentials not found for RACKSPACE {}: {} / {}".format(region, username_var, accesskey_var)
                continue'''
    providers_credentials["RACKSPACE"] = rackspace_credentials

    if "SOFTLAYER_username" in providers_info and "SOFTLAYER_username" in providers_info:
        SOFTLAYER_username = providers_info["SOFTLAYER_username"]
        SOFTLAYER_accesskey = providers_info["SOFTLAYER_accesskey"]
        softlayer_cred = { "username" : SOFTLAYER_username, "accesskey" : SOFTLAYER_accesskey }
        providers_credentials["SOFTLAYER"] = softlayer_cred
    else:
        sys.exit("\nERROR: Credentials not found for SOFLAYER: SOFTLAYER_username / SOFTLAYER_username\n")

    DT_username = providers_info["DT_username"]
    DT_accesskey = providers_info["DT_accesskey"]
    DT_cred = { "username" : DT_username, "accesskey" : DT_accesskey }
    providers_credentials["OPENSTACK"] = DT_cred

    return providers_credentials

def init_all_drivers():
    drivers = {}

    rs_drivers = init_rackspace_driver()
    if len(rs_drivers) > 0:
        drivers.update(rs_drivers)

    aws_drivers = init_ec2_driver()
    if len(aws_drivers) > 0:
        drivers.update(aws_drivers)

    sl_drivers = init_softlayer_driver()
    if len(sl_drivers) > 0:
        drivers.update(sl_drivers)

    '''dt_drivers = init_openstack_driver()
    if len(dt_drivers) > 0:
        drivers.update(dt_drivers)'''

    return drivers

def init_softlayer_driver(regions=None):
    info("Initialising SoftLayer drivers")
    cls = get_compute_driver(computeProvider.SOFTLAYER)
    username = get_credentials()["SOFTLAYER"]["username"]
    accesskey = get_credentials()["SOFTLAYER"]["accesskey"]
    driver = cls(username, accesskey)

    if driver is None:
        print "ERROR: Could not get driver from Softlayer"
        return {}
    else:
        return { "SOFTLAYER" : driver }
    #pprint(driver.list_locations())
    #print "------------------------------------"
    #pprint(driver.list_nodes())

def init_openstack_driver(regions=None):
    info("Initialising Open Telekom Cloud drivers")
    cls = get_compute_driver(computeProvider.OPENSTACK)
    username = get_credentials()["OPENSTACK"]["username"]
    accesskey = get_credentials()["OPENSTACK"]["accesskey"]
    #ex_force_auth_url = 'http://192.168.1.101:5000/v3/auth/tokens',
    driver = cls(username, accesskey,
                    #ex_force_auth_url = 'http://192.168.1.101:5000/v3/auth/tokens',
                    #ex_force_auth_url = 'http://192.168.1.101:5000',
                    ex_force_auth_url = 'https://iam.eu-de.otc.t-systems.com/v3/auth/tokens',
                    ex_force_auth_version = '2.0_password',
                    ex_tenant_name = 'eu-de')

    if driver is None:
        print "ERROR: Could not get driver from DT Cloud"
        return {}
    else:
        return { "OPENSTACK" : driver }
    #pprint(driver.list_locations())
    #print "------------------------------------"
    #pprint(driver.list_nodes())

def init_rackspace_driver(regions=None):
    info("Initialising Rackspace drivers")
    rs_drivers = {}

    get_credentials()
    if regions is None:
        credentials_TODO = rackspace_credentials
    else:
        credentials_TODO = {}
        for reg in regions:
            if reg in rackspace_credentials:
                credentials_TODO[reg] = rackspace_credentials[reg]

    for reg in credentials_TODO:
        #print "[DEBUG] reg: {} ({})".format(reg,regions[reg])
        username = rackspace_credentials[reg]['username']
        accesskey = rackspace_credentials[reg]['accesskey']
        #print "[DEBUG] Creating rackspace dirver for {} with key {} in Rackspace {}".format(username, accesskey, reg)
        cls = get_compute_driver(computeProvider.RACKSPACE)
        driver = cls(username, accesskey, region = reg)
        if driver is None:
            print "ERROR: Could not get driver for {} from Rackspace {}".format(username, reg)
            #continue
        else:
            driver_name = "RACKSPACE_{}".format(reg)
            #print "[DEBUG] addin driver {}".format(driver_name)
            rs_drivers[driver_name] = driver
    return rs_drivers

def init_ec2_driver(regions=None):
    aws_drivers = {}
    #aws_regions = get_regions('EC2')
    info("Initialising AWS drivers")
    #print "[DEBUG] init_ec2_drivers: aws_regions:\n{}".format(aws_regions)
    if regions is None:
        regions = aws_regions

    for region in regions:
        #print "[DEBUG] region = {}".format(region)
        '''if region not in aws_regions:
            sys.exit("ERROR: region {} is not available".format(region))'''

        #print "[DEBUG] adding driver for region {}".format(region)
        #access_key, secret_key = get_aws_credentials()
        access_key = get_credentials()['EC2']['username']
        secret_key = get_credentials()['EC2']['accesskey']
        cls = get_compute_driver(computeProvider.EC2)
        driver = cls(access_key, secret_key, region=region)
        driver_name = "EC2_{}".format(region)
        aws_drivers[driver_name] = driver

    return aws_drivers

# Get instances information

def _get_instances(driver):
    try:
        return driver.list_nodes()
    except Exception as e:
        print "ERROR: Failed to get instances from {}".format(driver)
        print "{}".format(e)
        return []

def get_all_instances(drivers=None):
    all_instances = {}
    if drivers == None:
        drivers = init_all_drivers()

    for driver_name in drivers:
        try:
            print "[INFO] Getting all instances from: {}".format(driver_name)
            instances = _get_instances(drivers[driver_name])
            #print "\n{} instances:\n{}\n---------------".format(driver,instances)
            all_instances[driver_name] = (instances)
        except Exception as e:
            print "ERROR: Could not get instances list from {}".format(driver_name)
            print "Exception: {}".format(e.message)

    return all_instances

def get_instance(name,mydrivers=None):
    if mydrivers is None:
        mydrivers = init_all_drivers()

    for driver_name in mydrivers:
        info("Looking for {} in {}".format(name,driver_name))
        driver = mydrivers[driver_name]
        try:
            instances = _get_instances(driver)
            for instance in instances:
                if instance.name == name:
                    return instance, driver
        except:
            print "ERROR: Could not get instances list from {}".format(driver_name)
            return None, None

def get_regions(provider):
    if provider not in providers:
        sys.exit("ERROR: Provider {} not available".format(provider))

    print ">> Getting regions for {}\n".format(provider)
    if provider == "EC2":
        #return ec2.EC2NodeDriver.list_regions()
        return aws_regions
    elif provider == "RACKSPACE":
        return rs_regions
    else:
        return []

def display_instance(drivers=None,instance_to_show=None):
    if drivers is None:
        drivers = init_all_drivers()

    for driver_name in drivers:
        #print "[DEBUG] Driver Name: {}".format(driver_name)
        driver = drivers[driver_name]
        try:
            instances = _get_instances(driver)
            if len(instances) > 0:
                if instance_to_show is None:
                    print "Instances from {}".format(driver_name)
                #print "[DEBUG] Number of instances : {}".format(len(instances))
                for instance in instances:
                    name = instance.name
                    state = instance.state
                    ips = instance.public_ips
                    #print "[DEBUG] {}".format(name)

                    if instance_to_show is None:
                        print "[{}] Name:{} , State: {} ,Id: {} ,Public IPs: {}".format(driver_name,name,state,instance.id,ips)
                    elif instance_to_show == name:
                        print "\n[{}] Name:{} , State: {} ,Id: {} ,Public IPs: {}\n".format(driver_name,name,state,instance.id,ips)
                        return
            else:
                print "No instances found from {}".format(driver_name)
        except Exception as e:
            print "ERROR: Could not get instances from {}".format(driver)
            print "(Exception) {}".format(e)

# Actions on the instances

def _stop_one_instance(name, mydrivers=None, provider=None, region=None):
    fdb = FileDatabase(get_config_data())
    pingdom = fdb.load_pingdom()
    pingdomchecks = pingdom.getChecks()

    if mydrivers is None:
        if provider is not None:
            mydrivers = eval('init_' + provider.lower() + '_driver([\'' + region + '\'])')
        else:
            mydrivers = init_all_drivers()

    for driver_name in mydrivers:
        '''if provider is not None:
            ** **'''
        print "Looking for {} in {}".format(name,driver_name)
        driver = mydrivers[driver_name]
        instances = _get_instances(driver)
        for instance in instances:
            if instance.name == name:
                if instance.state != "stopped":
                    print "* Stopping {} on {}".format(name,driver_name)
                    #return True
                    stop_status = driver.ex_stop_node(instance)
                    if stop_status:
                        name_regex = re.compile(name)
                        for check in pingdomchecks:
                            if name_regex.search(check.name) :
                                try:
                                    print "- Pausing pingdom for {}".format(name)
                                    check.paused = True
                                except:
                                    print "\nERROR: Failed to pause pingdom for {}\n".format(name)
                    return stop_status
                else:
                    print "{} already stopped".format(name)
                    return True

    print "ERROR: Could not find {} in {}".format(name, mydrivers.keys())
    return False

def stop_instance(to_stop, mydrivers=None, provider=None, regions=None):
    if mydrivers is None:
        if provider is not None:
            #init_driver_function = "init_{}_driver(['{}'])".format(provider.lower(),region)
            #print "DEBUG: init_driver_function: \"{}\"".format(init_driver_function)
            #mydrivers = eval(init_driver_function)
            if regions is None:
                mydrivers = eval('init_' + provider.lower() + '_driver()')
            else:
                if type(regions).__name__ == "str":
                    mydrivers = eval('init_' + provider.lower() + '_driver([\'' + regions + '\'])')
                else:
                    mydrivers = {}
                    for region in regions:
                        mydrivers = eval('init_' + provider.lower() + '_driver([\'' + region + '\'])')

        else:
            mydrivers = init_all_drivers()

    if type(to_stop).__name__ == "str":
        return _stop_one_instance(to_stop, mydrivers, provider, regions)
    else:
        for instance_to_stop in sorted(to_stop):
            if regions is None:
                _stop_one_instance(instance_to_stop, mydrivers, provider)
            else:
                for region in regions:
                    _stop_one_instance(instance_to_stop, mydrivers, provider, region)

def _start_one_instance(name, mydrivers=None):
    fdb = FileDatabase(get_config_data())
    pingdom = fdb.load_pingdom()
    pingdomchecks = pingdom.getChecks()

    if mydrivers is None:
        mydrivers = init_all_drivers()

    #print "[DEBUG] Number of drivers: {}".format(len(mydrivers))
    for driver_name in mydrivers:
        print "Looking for {} in {}".format(name,driver_name)
        driver = mydrivers[driver_name]
        instances = _get_instances(driver)
        for instance in instances:
            if instance.name == name:
                if instance.state != "running":
                    print "Starting {} on {}".format(name,driver_name)
                    start_status = driver.ex_start_node(instance)
                    if start_status:
                        name_regex = re.compile(name)
                        for check in pingdomchecks:
                            if name_regex.search(check.name) :
                                try:
                                    print "- Starting pingdom for {}".format(name)
                                    check.paused = False
                                except:
                                    print "\nERROR: Failed to start pingdom for {}\n".format(name)
                    return start_status
                else:
                    print "{} already running".format(name)
                    return True
    print "ERROR: Could not find {} in {}".format(name, mydrivers.keys())
    return False

def start_instance(to_start, mydrivers=None):
    if mydrivers is None:
        mydrivers = init_all_drivers()

    if type(to_start).__name__ == "str":
        return _start_one_instance(to_start, mydrivers)
    else:
        for instance_to_start in sorted(to_start):
            _start_one_instance(instance, mydrivers)

############ LOAD BALANCER AWS ############

lb_region = "eu-west-1"

def test_LB():
    region = lb_region

    AWS_ACCESS_KEY_ID = get_credentials()['EC2']['username']
    AWS_SECRET_ACCESS_KEY = get_credentials()['EC2']['accesskey']

    cls = get_lb_driver(lbProvider.ELB)
    driver = cls(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, region=region)

    balancers = driver.list_balancers()

    for lb in balancers:
        print "{}".format(lb.name)

def get_LB_driver(region=None):
    if region is None:
        region = lb_region

    AWS_ACCESS_KEY_ID = get_credentials()['EC2']['username']
    AWS_SECRET_ACCESS_KEY = get_credentials()['EC2']['accesskey']

    cls = get_lb_driver(lbProvider.ELB)
    driver = cls(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, region=region)
    return driver

def get_LB_list(driver=None, region=None):
    if driver is None:
        driver = get_LB_driver(region)

    return driver.list_balancers()

def get_LB_members(name, lbs=None, driver=None):
    if driver is None:
        driver = get_LB_driver(lb_region)

    if lbs is None:
        lbs = get_LB_list(driver, lb_region)

    for lb in lbs:
        if lb.name == name:
            balancer = driver.get_balancer(balancer_id = lb.id)
            return balancer, balancer.list_members()

def detach_LB_member(instance_name, lb, driver=None, provider=None, region=None):
    print "[DEBUG] Trying to detach {} from {}".format(instance_name, lb)

    if driver is None:
        driver = get_LB_driver()

    balancer, lb_members = get_LB_members(lb,None,driver)
    ec2_drivers = init_ec2_driver([lb_region])
    ec2_instances = get_all_instances(ec2_drivers)

    for ec2_driver in ec2_instances:
        for ec2_instance in ec2_instances[ec2_driver]:
            if ec2_instance.name == instance_name:
                for member in lb_members:
                    if ec2_instance.id == member.id:
                        print "Detaching {} ({}) from {}".format(ec2_instance.name, member.id, balancer.id)
                        try:
                            detach_status = driver.balancer_detach_member(balancer, member)
                            return detach_status
                        except:
                            return False

def attach_LB_member(instance_name, lb, driver=None, provider=None, region=None):
    print "[DEBUG] Trying to attach {} to {}".format(instance_name, lb)

    if driver is None:
        driver = get_LB_driver()

    balancer, lb_members = get_LB_members(lb,None,driver)
    ec2_drivers = init_ec2_driver([lb_region])
    ec2_instances = get_all_instances(ec2_drivers)

    for ec2_driver in ec2_instances:
        for ec2_instance in ec2_instances[ec2_driver]:
            if ec2_instance.name == instance_name:
                print "Attaching {} to {}".format(ec2_instance.name, balancer.id)
                try:
                    attach_status = driver.balancer_attach_compute_node(balancer, ec2_instance)
                    return attach_status
                except:
                    return None

################  ROUTE 53  ###############
#boto3
def get_route53_record_sets(hz_id,next_name=None,next_type=None):
    client = boto3.client('route53')

    #print "{}".format(client.get_hosted_zone(Id=hz_id))
    params = {'HostedZoneId': hz_id}

    if next_name is not None:
        params['StartRecordName'] = next_name
    if next_type is not None:
        params['StartRecordType'] = next_type

    record_sets = client.list_resource_record_sets(**params)
    all_records_info = record_sets['ResourceRecordSets']
    IsTruncated = record_sets['IsTruncated']

    while IsTruncated == True:
        next_name = record_sets['NextRecordName']
        next_type = record_sets['NextRecordType']
        #print "Getting next batch: Starting @ {}".format(next_name)

        params = {
            'HostedZoneId': hz_id,
            'StartRecordName': next_name,
            'StartRecordType': next_type
        }
        record_sets = client.list_resource_record_sets(**params)
        all_records_info = all_records_info + record_sets['ResourceRecordSets']
        IsTruncated = record_sets['IsTruncated']

    return all_records_info

def get_stack_record_sets(stack):
    all_records = get_route53_record_sets(get_hosted_zone(route53_hosted_zone).id)
    stack_records = []
    stack_regex = re.compile("Stack {}".format(stack))
    for rec in all_records:
        if 'SetIdentifier' in rec:
            if stack_regex.search(rec['SetIdentifier']):
                stack_records.append(rec)
    return stack_records

def get_hosted_zone(hosted_zone_name):
    driver = get_DNS_driver()
    zones = driver.list_zones()
    hz_name_regex = re.compile(hosted_zone_name)
    for zone in zones:
        if hz_name_regex.search(zone.domain):
            return zone

def update_route53_weight(stack, weight):
    hz = get_hosted_zone(route53_hosted_zone)

    records = get_route53_record_sets(hz.id)

    client = boto3.client('route53')
    stack_regex = re.compile("Stack {}".format(stack))
    print "INFO: Setting weight to {} for Stack {}".format(weight, stack)

    stack_record_sets = []
    for rec in get_stack_record_sets(stack):
        if rec['SetIdentifier'] not in stack_record_sets:
            stack_record_sets.append(rec['SetIdentifier'])

    for stack_setIdentifier in stack_record_sets:
        if _get_input("\nWould you like to set weight of \"{}\" to {}".format(stack_setIdentifier, weight)) != "Y":
            continue
        for rec in get_stack_record_sets(stack):
            '''if 'SetIdentifier' in rec:
                if stack_regex.search(rec['SetIdentifier']):'''
            if rec['SetIdentifier'] == stack_setIdentifier:
                print "- Updating {}".format(rec['Name'])
                #print "'Changes': [{{'Action': 'UPSERT','ResourceRecordSet': {{'Name': {},'Type': {},'SetIdentifier': {},'Weight': {},'TTL': 60}}}},]".format(rec['Name'],rec['Type'],rec['SetIdentifier'],weight)
                '''try:
                    response = client.change_resource_record_sets(
                        HostedZoneId=hz.id,
                        ChangeBatch={
                            'Comment': 'Release deployment weight update',
                            'Changes': [
                                {
                                    'Action': 'UPSERT',
                                    'ResourceRecordSet': {
                                        'Name': rec['Name'],
                                        'Type': rec['Type'],
                                        'SetIdentifier': rec['SetIdentifier'],
                                        'Weight': weight,
                                        'TTL': 60,
                                        'ResourceRecords': rec['ResourceRecords']
                                    }
                                },
                            ]
                        }
                    )
                    print response
                except Exception as e:
                    print "\nERROR: Failed to set weight to {} for Stack {}".format(weight, stack)
                    print "{}".format(e)
                    return False'''
    return True
#end boto3

def get_DNS_driver():
    AWS_ACCESS_KEY_ID = get_credentials()['EC2']['username']
    AWS_SECRET_ACCESS_KEY = get_credentials()['EC2']['accesskey']

    cls = get_dns_driver(dnsProvider.ROUTE53)
    driver = cls(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
    return driver

def get_DNS_records(record_name=None, myzone=None, driver=None):
    if driver is None:
        driver = get_DNS_driver()
    r53_zones = driver.list_zones()
    zones = []
    if myzone is None:
        zones = r53_zones
    else:
        zone_regex = re.compile(myzone)
        for zone in r53_zones:
            if zone_regex.match(zone.domain):
                zones = [zone]
                break

    records = []
    for zone in zones:
        print "Getting records from {}".format(zone)
        zone_records = driver.list_records(zone)
        if len(zone_records) > 0:
            if record_name is None:
                records.extend(zone_records)
            else:
                for record in zone_records:
                    if record.name == record_name:
                        records.append(record)

    return records

########## UTILS ################

def _get_input(message = None):
    if message is None:
        display_message = "Would you wish to continue? [Y/n]: "
    else:
        display_message = message + " [Y/n]: "

    option = raw_input(display_message)
    while option not in [ "Y" , "n" ]:
        option = raw_input("Please enter a valid option [Y/n]: ")

    return option
