#!/usr/local/bin/python

import os
import sys
current_dir = os.getcwd()
import argparse
import json
from datetime import date
import re
from time import sleep
import subprocess
from subprocess import Popen, PIPE
import fabfile
import libcloud_api
from fabric.context_managers import settings
from fabric.operations import sudo, local
import tempfile
import appscript
import platform

wandera_client_dir = os.path.join("/Users", "arturonoha", "Documents", "bitbucket", "wandera_client", "wandera_client")
wandera_client_client_dir = os.path.join(wandera_client_dir, "client")
wandera_client_service_dir = os.path.join(wandera_client_client_dir, "service")

import wandera_client
from wandera_client.client.service.proxy_manager import ProxyManagerClient
#from wandera_client.client.service import ProxyManagerClient as PMC
from wandera_client.resource import Server
from wandera_client.client.auth import ServiceAuthentication

import wandera_woopas
from wandera_woopas.pm import ProxyController
from wandera_woopas.database import FileDatabase

today = date.today()
DATE = today.isoformat()
#operations_dir = "/Users/arturonoha/Documents/bitbucket/operations/Scripts/Arturo"
operations_dir = current_dir
proxies_release_dir = os.path.join(current_dir,"release_Proxy_nodes")
CANARY_PROXY = "aa2ef5898d7f40d0.node.lon3.uk.wandera.com"

TODOLIST = {}
#proxy_name_regex = re.compile("[0-9a-zA-Z]*\.node\..*\.wandera.com")
proxy_name_regex = re.compile("[0-9a-zA-Z]*\.node\.")

home = os.path.expanduser('~')
CONFIG_FILE = os.path.join(home, ".woopas.json")

current_dir = os.getcwd()
WOOPAS_PLAN = os.path.join(current_dir, "release_Proxy_nodes", "proxy_deployment_plan.csv")

ticket_dir = os.path.join("/Users","arturonoha","Documents","Projects","OPS-2742")
update_script_name = "remove_td_agent_mongo_ireland.sh"
update_script = os.path.join(ticket_dir, update_script_name)
proxy_update_script = "/tmp/{}".format(update_script_name)

puppet_master = "puppet.snappli.net"

def get_config_data():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()
    try:
        config_json = json.loads(config)
    except Exception as e:
        sys.exit("ERROR: Failed to load config from {}\n".format(CONFIG_FILE))

    return config_json

def init_ssh_user():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        FAB_USERNAME = config_json["woopas"]["ssh_username"]
    except Exception as e:
        sys.exit("ERROR: Failed to load ssh user from {}\n".format(CONFIG_FILE))

    return FAB_USERNAME

def init_proxy_manager():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        pm_url = config_json["woopas"]["proxy_manager_url"]
        pm_api_key = config_json["woopas"]["service_api_key"]
        pm_api_secret = config_json["woopas"]["service_api_secret"]
        pm_auth = ServiceAuthentication(pm_api_key, pm_api_secret)
        pmc = ProxyManagerClient(pm_url,auth=pm_auth)
    except Exception as e:
        sys.exit("ERROR: Failed to load ProxyManagerClient\n({})".format(e.message))

    return pmc

def clear_screen():
    clear = subprocess.Popen( "cls" if platform.system() == "Windows" else "clear", shell=True)
    clear.communicate()

def update_done_status(done_line,FILE_TO_UPDATE):
    temp_file_name = "{}.tmp".format(FILE_TO_UPDATE)
    done_line = done_line.replace(",TODO","")
    #print "DEBUG: updating line: \"{}\"".format(done_line)
    with open(FILE_TO_UPDATE, "r")  as proxy_plan, open(temp_file_name,"w") as temp_file:
        for line in proxy_plan:
            update_regex = re.compile("{},TODO".format(done_line))
            replace_str = "{},DONE".format(done_line)
            line = update_regex.sub(replace_str, line)
            #print "DEBUG: Updated line: \"{}\"".format(line)
            temp_file.write(line)
    os.remove(FILE_TO_UPDATE)
    os.rename(temp_file_name, FILE_TO_UPDATE)

def new_window(active_svr, user):
    active_cmd = "cd {}; fab -H {} -u {} tail_rabbit_log".format(operations_dir, active_svr, user)
    appscript.app('Terminal').do_script(active_cmd)
    #os.system("start cmd /K {}".format(active_cmd))
    #Popen('xterm -hold -e "{}}"'.format(active_cmd))
    #appscript.app('Terminal').do_script(active_cmd)

def main(PROXY_LIST, dryrun=True):
    clear_screen()

    if not os.path.isfile(CONFIG_FILE):
        sys.exit("\nERROR: Configuration file {} not found.\n".format(CONFIG_FILE))

    FAB_USERNAME = init_ssh_user()

    # initialise the proxy manager and required woopas instances for later use
    pmc = init_proxy_manager()
    fdb = FileDatabase(get_config_data())
    woopas_controler = ProxyController(FileDatabase.load_service_client(fdb,ProxyManagerClient))
    # get cloud providers drivers
    drivers = libcloud_api.init_all_drivers()

    # get the list of proxies already updated, in case the execution was stopped
    DONELIST = os.path.join(ticket_dir,"OPS-2742_done_proxies.txt")
    done_proxies_list = []
    if os.path.isfile(DONELIST):
        done_regex = re.compile(".*,DONE")
        with open(DONELIST, "r") as done_proxies_file:
            for line in done_proxies_file:
                line = line.rstrip()
                if done_regex.search(line):
                    done_proxies_list.append(line.split(',')[1])
    # get all instances from our providers to extract the proxies info
    print "Getting all instances"
    all_instances = libcloud_api.get_all_instances(drivers)

    #get list of proxies to update from the input file
    proxy_list = []
    with open(PROXY_LIST, "r") as proxy_list_file:
        for line in proxy_list_file:
            proxy_name = line.rstrip()
            #print "{}".format(proxy_name)
            proxy_list.append(proxy_name)

    # based on proxies to do create the action list per provider and status [idle/active]
    proxie_list_info = {}
    print "Getting the list of proxies"
    for driver_name in all_instances: # iterate through all the instances
        for instance in all_instances[driver_name]:
            if instance.name in proxy_list: # if the name of the instance is the proxis list to do process
                proxy_name = instance.name
                provider = driver_name.split('_')[0]

                # get the proxy manager info for the proxy
                proxy_running_state = instance.state
                pm_proxy = ProxyManagerClient.node_by_name(pmc, instance.name)
                proxy_pm_state = pm_proxy.state

                if provider in proxie_list_info: # add the instance to the list of instances associated to the provider in the corresponding state
                    if proxy_pm_state in proxie_list_info[provider]:
                        proxie_list_info[provider][proxy_pm_state].append(instance)
                    else:
                        proxie_list_info[provider][proxy_pm_state] = [instance]
                else:
                    proxie_list_info[provider] = {}
                    proxie_list_info[provider][proxy_pm_state] = [instance]
                # Add the proxy to the tracking file with status TODO
                with open(DONELIST, "a+") as done_proxies_file:
                    done_proxies_file.write("{},{},TODO\n".format(DATE, proxy_name))

    # iterate throug the dictinary created above: provider - ProxyManager state - proxies list
    for provider in proxie_list_info:
        started_proxies_list = []
        print "** Starting update of {} proxies".format(provider)
        # Print list of proxies to perform for the provider and ask for confirmation to continue
        print "List of proxies:"
        for proxy_pm_state in sorted(proxie_list_info[provider].keys(), reverse=True):
            print "-- {} proxies".format(proxy_pm_state)
            for proxy_instance in sorted(proxie_list_info[provider][proxy_pm_state]):
                print "{}".format(proxy_instance.name)
        if fabfile._get_option() != "Y":
            sys.exit("\nAborted by user\n")
        print "\n------------------------------\n"
        # iterate each proxyManager state available for the provider at hand
        for proxy_pm_state in sorted(proxie_list_info[provider].keys(), reverse=True):
            print "- {} proxies in {}".format(proxy_pm_state, provider)
            for proxy_instance in sorted(proxie_list_info[provider][proxy_pm_state]):
                proxy_name = proxy_instance.name
                # check if the proxy was already updated in a previous run and if so try to get status of the service (should return unknown) and skip
                if proxy_name in done_proxies_list:
                    print "\n{} has already been updated. Will skip\n".format(proxy_name)
                    try:
                        with settings(host_string = proxy_name, user = FAB_USERNAME):
                            fabfile.service_status("td-agent-proxy_events_mongo_ireland")
                    except Exception as e:
                        print "++++++++++++++++++++++++++++++++++++++++++++++++"
                        continue
                    print "++++++++++++++++++++++++++++++++++++++++++++++++"
                    continue
                print "- {}".format(proxy_name)
                proxy_running_state = proxy_instance.state

                if proxy_running_state != "running":
                    if proxy_running_state == "stopped":
                        print "INFO: {} is STOPPED. Will start it".format(proxy_name)
                        if dryrun == "False" or not dryrun:
                            libcloud_api.start_instance(proxy.name, drivers)
                        started_proxies_list.append(proxy_name)
                    else:
                        print "ERROR: Unknown running state for {} : ({})".format(proxy_name, proxy_running_state)
                        if fabfile._get_option("Would you like to try to start it?") == "Y":
                            print "INFO: {} is STOPPED. Will start it".format(proxy_name)
                            if dryrun == "False" or not dryrun:
                                if libcloud_api.start_instance(proxy.name, drivers):
                                    started_proxies_list.append(proxy_name)
                                else:
                                    print "\nERROR: Failed to start {}. Skipping update".format(proxy_name)
                                    continue
                            else:
                                started_proxies_list.append(proxy_name)
                        else:
                            print "Skipping update of {}".format(proxy_name)
                            continue
                # open a new terminal window and tail rabbit log
                print "- Opening new window with tail logs"
                if dryrun == "False" or not dryrun:
                    new_window(proxy_name, FAB_USERNAME)

                # Failover active proxies
                if proxy_pm_state == "active":
                    pm_proxy = ProxyManagerClient.node_by_name(pmc, proxy_name)
                    failovers = ProxyController.find_failovers(woopas_controler, pm_proxy)
                    failover = failovers[0]["nodeName"]
                    print "\nWARN: Failing active proxy {} to {}".format(proxy_name, failover)
                    failover_cmd = "woopas failover {} {}".format(proxy_name, failover)
                    print "{}".format(failover_cmd)
                    if dryrun == "False" or not dryrun:
                        failover_open = Popen(failover_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
                        failover_open.communicate()
                # Running puppet agent
                with settings(host_string=puppet_master, user=FAB_USERNAME):
                    # Run puppet noop
                    noop = fabfile.run_puppet_deploy(proxy_name, noop=True)
                    if noop == 4:
                        sys.exit("** Deployment aborted by user **")
                    elif noop == 3:
                        return noop
                    # If not dryrun, then run live puppet execution
                    if dryrun == "False" or not dryrun:
                        live = fabfile.run_puppet_deploy(proxy_name, noop=False)
                        #live = 2
                        if live == -1: # user did not run puppet live. check if he wants to continue with update. If no skip to next server
                            if fabfile._get_option("\nContinue with rest of server update?") != "Y":
                                continue
                            else:
                                sys.exit("** Update aborted by user **")
                        elif live == 4: # User aborted execution after puppet error
                            sys.exit("** Update aborted by user **")
                        elif live == 3: # User chose to skip this node's update after puppet error
                            continue
                # Transfer update script
                scp_cmd = "scp {} {}:{}".format(update_script, proxy_name, proxy_update_script)
                print "- {}".format(scp_cmd)
                scp_open = Popen(scp_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
                scp_open.communicate()
                print "- Change mode to executable for {}:{}".format(proxy_name, proxy_update_script)
                chmod_cmp = "chmod +x {}".format(proxy_update_script)
                with settings(host_string = proxy_name, user = FAB_USERNAME):
                    sudo(chmod_cmp)
                # Running the update script
                print "RUNNING update: {}".format(update_script)
                if dryrun == "False" or not dryrun:
                    run_script_cmp = "{0} --dryrun False".format(proxy_update_script)
                    print "{}".format(run_script_cmp)
                    with settings(host_string = proxy_name, user = FAB_USERNAME):
                        sudo(run_script_cmp)
                else:
                    run_script_cmp = "{0} --dryrun True".format(proxy_update_script)
                    print "{}".format(run_script_cmp)
                    with settings(host_string = proxy_name, user = FAB_USERNAME):
                        sudo(run_script_cmp)
                # Fail back over active proxies
                if proxy_pm_state == "active":
                    failover_cmd = "woopas failover {} {}".format(failover, proxy_name)
                    print "\nWARN: Failing back proxy {} to {}".format(failover, proxy_name)
                    print "{}".format(failover_cmd)
                    if dryrun == "False" or not dryrun:
                        failover_open = Popen(failover_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
                        failover_open.communicate()
                # update tracking file
                print "*****************************************************************************"
                print "Updated {}. Updating todo list at {}...".format(proxy_name, DONELIST)
                print "*****************************************************************************"
                update_done_status(proxy_name,DONELIST)

                #Run status check on the proxy: proxy service, services running, log tail, status of td-agent-proxy_events_mongo_ireland
                print "*****************************************************************************"
                print "Checking status of {}".format(proxy_name)
                print "*****************************************************************************"

                try:
                    with settings(host_string = proxy_name, user = FAB_USERNAME):
                        fabfile.check_proxy()
                        print "\nChecking status of td-agent-proxy_events_mongo_ireland"
                        fabfile.service_status("td-agent-proxy_events_mongo_ireland")
                except Exception as e:
                    print "Failed to run check on {}\nError: {}\nPlease check manually".format(proxy_name, e.message)
                    if fabfile._get_option() == "n":
                        sys.exit("Deployment aborted")
                print "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"

                sleep(1)

                if dryrun == "False" or not dryrun:
                    if fabfile._get_option("\nUpdate of {} completed.\nContinue with next?".format(proxy_name)) != "Y":
                        sys.exit("\Aborted by user\n")
        # Stop started proxies
        if len(started_proxies_list) > 0:
            print "\nINFO: Stopping started instances."
            for instance in sorted(started_proxies_list):
                print "{}".format(instance)
            if dryrun == "False" or not dryrun:
                libcloud_api.stop_instance(started_proxies_list, provider=provider)
            print "\n*** CHECK PINGDOM STATUS ***\n"
            time.sleep(2)

        if fabfile._get_option("\nFinished update of {} proxies. Proceed with next provider?".format(provider)) != "Y":
            sys.exit("\nAborted by user\n")

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('--proxy-list', dest = "proxy_list_input", help = "File with list of proxies to update")
    parser.add_argument('--dryrun', dest = "dryrun", default = True)

    return parser.parse_args()

def usage():
    clear_screen()
    print "\nUSAGE: {} --proxy-list <prixy_list_file> --dryrun <True/False>\n".format(sys.argv[0])
    sys.exit(1)

if __name__ == "__main__":
    options = parse_options(sys.argv[1:])

    if options.proxy_list_input is None:
        print "\nERROR: Missing file with list of proxies to process\n"
        usage()
    proxies_file = options.proxy_list_input
    # Requires a file with the list of proxies that require update (1 line per proxy instance name)
    if not os.path.isfile(proxies_file):
        clear_screen()
        sys.exit("\nERROR: file {} not found\n".format(proxies_file))

    dryrun = options.dryrun
    clear_screen()
    if fabfile._get_option("dryrun: {}\nfile: {}\nProceed:".format(dryrun, proxies_file)) != "Y":
        sys.exit("\n** Aborted by user **\n")
    main(proxies_file, dryrun)
