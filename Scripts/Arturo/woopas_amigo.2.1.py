#!/usr/local/bin/python

############################################################
# woopas_amigo.py
# Author: Arturo Noha
# version: 2.1
# April 2017
############################################################
# v 1.0: conversion to python of originam woopas_amigo.sh + extra features
# v 2.0: Automatic retrieval of daily proxies to do based on regions to do
# v 2.1: canary proxy management
#
# Arguments:
# 1. (MANDATORY) an input file with the deployment plan: regions to do per day:
#   . format: deplpoyment_day,region,TODO
# 2. a dictionary with the providers libcloud drivers:
#   . format: { 'driver_name1' : 'driver1'[, 'driver_name2' : '{driver2}'...]}
#   . if not provided, it will contact the available providers via libcloud to get them
# 3. a dictionary with the list of instances from the providers to use as reference
#   . format: { 'driver_name1' : [array_of_instances_from_driver1][, 'driver_name2' : [array_of_instances_from_driver2]...]}
#   . if not provided, it will get them from the providers via libcloud
#
# Logic:
# 1. if not provided as arguments, get porivders drivers and instances (via libcloud_api)d
# 2. get the proxies from the regions to deplpoy
# 3. check the state of the proxy from the instances information gather initially
#   3.a. if the instance is stopped, try to start it (via libcloud_api)
# 4. open a new terminal window and tail the rabbit.log on the proxy to upgrade
# 5. run woopas
# 6. if woopas successfull, update the file with the list of proxies and set the proxy to DONE
# 7. check the status of the proxy (via fabfile)
#   - check the proxy_enterprise package verion and verify that the service is running on the same version
#   - check all services status
#   - tail rabbit.log
# 8. if any instance was started, stopped them (via fabfile)


import os
import sys
current_dir = os.getcwd()

from datetime import date
import re
from time import sleep
import subprocess
from subprocess import Popen, PIPE
import fabfile
import libcloud_api
from fabric.context_managers import settings
import tempfile
import appscript
import platform

today = date.today()
DATE = today.isoformat()
FAB_USERNAME = "anoha"
#operations_dir = "/Users/arturonoha/Documents/bitbucket/operations/Scripts/Arturo"
operations_dir = current_dir
proxies_release_dir = os.path.join(current_dir,"release_Proxy_nodes")
CANARY_PROXY = "aa2ef5898d7f40d0.node.lon3.uk.wandera.com"

TODOLIST = {}
proxy_name_regex = re.compile("[0-9a-zA-Z]*\.node\.")

def clear_screen():
    clear = subprocess.Popen( "cls" if platform.system() == "Windows" else "clear", shell=True)
    clear.communicate()

def update_done_status(done_line,FILE_TO_UPDATE):
    temp_file_name = "{}.tmp".format(FILE_TO_UPDATE)
    done_line = done_line.replace(",TODO","")
    #print "DEBUG: updating line: \"{}\"".format(done_line)
    with open(FILE_TO_UPDATE, "r")  as proxy_plan, open(temp_file_name,"w") as temp_file:
        for line in proxy_plan:
            update_regex = re.compile("{},TODO".format(done_line))
            replace_str = "{},DONE".format(done_line)
            line = update_regex.sub(replace_str, line)
            #print "DEBUG: Updated line: \"{}\"".format(line)
            temp_file.write(line)
    os.remove(FILE_TO_UPDATE)
    os.rename(temp_file_name, FILE_TO_UPDATE)

def new_window(active_svr, user):
    active_cmd = "cd {}; fab -H {} -u {} tail_rabbit_log".format(operations_dir, active_svr, user)
    appscript.app('Terminal').do_script(active_cmd)
    #os.system("start cmd /K {}".format(active_cmd))
    #Popen('xterm -hold -e "{}}"'.format(active_cmd))
    #appscript.app('Terminal').do_script(active_cmd)

def main(rel_num,proxy_plan,drivers=None,all_instances=None):
    clear_screen()

    dt_proxies_file_name = os.path.join(proxies_release_dir,"DT-proxies.plan")
    PROXY_DEPLOY_PLAN = proxy_plan
    if not os.path.isfile(PROXY_DEPLOY_PLAN):
        sys.exit("ERROR: {} not found".format(PROXY_DEPLOY_PLAN))

    DONELIST = os.path.join(proxies_release_dir,"RL_{}_done_proxies.txt".format(rel_num))
    done_proxies_list = []
    if os.path.isfile(DONELIST):
        with open(DONELIST, "r") as done_proxies_file:
            for line in done_proxies_file:
                line = line.rstrip()
                done_proxies_list.append(line.split(',')[1])

    today_regex = re.compile("^[0-9],.*,TODO")
    day_TODO = -1
    regions_TODO = []
    with open(PROXY_DEPLOY_PLAN, "r") as proxy_plan_file:
        for line in proxy_plan_file:
            # get day to do: number of first line TODO
            line = line.rstrip()
            if today_regex.search(line):
                day_TODO = line.split(',')[0]
                break

    if day_TODO < 0:
        sys.exit("ERROR: could not find a day to deploy. Exit.")
    else:
        if day_TODO == "0":
            print "INFO: CANARY PROXY DEPLOYMENT ({})".format(CANARY_PROXY)
        else:
            print "INFO: Executing plan for day {} of proxy deployment.".format(day_TODO)
            print "day,Provider,Region,day_weight,status"
            day_todo_regex = re.compile("^{},.*,TODO".format(day_TODO))
            with open(PROXY_DEPLOY_PLAN, "r") as proxy_plan_file:
                for line in proxy_plan_file:
                    line = line.rstrip()
                    if day_todo_regex.search(line):
                        print "{}".format(line)

        if fabfile._get_option("Continue?") != "Y":
            sys.exit("\nDeployment aborted by user\n")
        today_regex = re.compile("^{},.*,TODO".format(day_TODO))

    if day_TODO != "0":
        if drivers is None:
            print "Getting Providers drivers"
            drivers = libcloud_api.init_all_drivers()

        if all_instances is None:
            print "Getting all instances"
            all_instances = libcloud_api.get_all_instances(drivers)

        all_proxies = {}
        print "Getting the list of proxies"
        for driver_name in all_instances: # iterate through all the instances
            for instance in all_instances[driver_name]:
                if proxy_name_regex.match(instance.name): # if the name of the instance matches the proxy name convention process
                    provider = driver_name.split('_')[0]
                    '''for provider in provider_aliases: # extract the provider name from the driver name and the list of aliases in libcloud_api
                        if provider in provider_aliases[provider]:
                            break'''
                    if provider in all_proxies: # add the instance to the list of instances associated to the provider
                        all_proxies[provider].append(instance)
                    else:
                        all_proxies[provider] = [instance]

    started_proxies_list = []

    with open(PROXY_DEPLOY_PLAN, "r") as proxy_plan_file:
        for plan_line in proxy_plan_file:
            plan_line = plan_line.rstrip()
            # print "DEBUG: {}".format(plan_line)
            if today_regex.search(plan_line):
                print "DEBUG: Processing {}".format(plan_line)
                provider = plan_line.split(',')[1]
                region = plan_line.split(',')[2]
                weight = plan_line.split(',')[3]
                region_proxies_TODO = {} # will hold the list of proxies from the regions that need to be deployed. All if weight = 1, half it weight = 0.5
                started_proxies_list = []

                if provider == "OPENSTACK":
                    print "WARN: DT is not supported for Cloud automation. Will use file {} to get proxies list.".format(dt_proxies_file_name)

                    dt_choice = raw_input("\nWhat do you want to do?\n\t1. continue\n\t2. use another file\n\t3. skip DT deplpoyment\nChoice: ")
                    while dt_choice not in ['1','2','3']:
                        dt_choice = raw_input("Please enter a valid option: ")

                    if dt_choice == '2':
                        dt_proxies_file_name = raw_input("Please enter full file path (File format: deployment_day,proxy_name): ")
                        while not os.path.isfile(dt_proxies_file_name):
                            if fabfile._get_option("File not find. Would you like to provide a new file? ") == "Y":
                                dt_proxies_file_name = raw_input("Please enter full file path: ")
                            else:
                                break
                        if not os.path.isfile(dt_proxies_file_name):
                            print "INFO: Skipping DT proxies deployment.\n"
                            sleep(2)
                    elif dt_choice == '3':
                        print "INFO: Skipping DT proxies deployment.\n"
                        sleep(2)
                        continue

                    date_regex = re.compile("{},.*".format(day_TODO))
                    with open(dt_proxies_file_name, "r") as dt_proxies_file:
                        for line in dt_proxies_file:
                            line = line.rstrip()
                            if date_regex.match(line):
                                proxy_name = line.split(',')[1]
                                region_proxies_TODO[proxy_name] = proxy_name

                else:
                    region_regex = re.compile(".*\.node[\.-]{}[a-z0-9]*[\.-].*wandera[\.-]com".format(region)) # node name parts split by '-' for DT, '.' for AWS, RS, SL
                    all_region_proxies = {}
                    if day_TODO == "0":
                        drivers = eval('libcloud_api.init_' + provider.lower() + '_driver([\'' + region + '\'])')
                        canary_instance, canary_driver = libcloud_api.get_instance(CANARY_PROXY, drivers)
                        print "DEBUG: Found canary instance: {}".format(canary_instance)
                        #sys.exit(0)
                        all_region_proxies[CANARY_PROXY] = canary_instance
                    else:
                        print "> Deploying to proxies in {}".format(region)
                        print "DEBUG: {}".format(plan_line)

                        #print "DEBUG: Provider: {}".format(provider)
                        provider_proxies = all_proxies[provider]
                        #region_proxies_TODO = {} # will hold the list of proxies from the regions that need to be deployed. All if weight = 1, half it weight = 0.5

                        for proxy in provider_proxies:
                            #print "DEBUG: checking if {} needs deployment".format(proxy.name)
                            if proxy.name == CANARY_PROXY:
                                print "INFO: CANARY_PROXY proxy ({}) found. Skipping...".format(CANARY_PROXY)
                                continue
                            if region_regex.search(proxy.name): # add all proxies from the region to do to the list of proxies to deploy
                                #print "DEBUG: Adding {} to all_region_proxies".format(proxy.name)
                                all_region_proxies[proxy.name] = proxy
                                if proxy.state != "running": # Start stopped instances from regions to deploy
                                    #print "DEBUG: {}".format(proxy)
                                    print "INFO: {} is stopped. Starting it...".format(proxy.name)
                                    libcloud_api.start_instance(proxy.name)
                                    started_proxies_list.append(proxy.name)
                    if weight == "1": # if the daily region weight is 1, we'll do all proxies
                        region_proxies_TODO = all_region_proxies
                    else: # if the weight of region is not 1, select only half of the region's proxies to deploy
                        print "DEBUG: weight = {}. Getting required instances. Total number of proxies = {}".format(weight, len(all_region_proxies))
                        total_region_proxies = len(all_region_proxies)

                        if total_region_proxies % 2 == 0: # if number of prixies is multiple of 2, we'll do half
                            count_TODO = total_region_proxies / 2
                        else:
                            count_TODO = ( total_region_proxies // 2 ) + 1 # if number of proxies is odd number, we'll do floor division by 2 + 1

                        print "DEBUG: number of proxies to deploy = {}".format(count_TODO)
                        added_count = 1
                        for proxy_name in all_region_proxies:
                            #print "DEBUG: next proxy = {}".format(proxy_name)
                            if added_count > count_TODO:
                                break

                            if proxy_name not in done_proxies_list:
                                #print "DEBUG: Adding {} to the list of proxies to do.".format(proxy_name)
                                region_proxies_TODO[proxy_name] = all_region_proxies[proxy_name]
                                added_count = added_count + 1

                print "INFO: adding proxies to do to for {} {} status tracking file ({})".format(provider, region, DONELIST)
                with open(DONELIST, "a+") as done_proxies_file:
                    for proxy_name in region_proxies_TODO:
                        done_proxies_file.write("{},{},TODO\n".format(DATE, proxy_name))

                for PROXY in region_proxies_TODO:
                    print "Opening servers log tail windows"
                    new_window(PROXY, FAB_USERNAME)

                    woopas_cmd = "woopas upgrade_proxy {}".format(PROXY)
                    print "{}".format(woopas_cmd)
                    woopas_open = Popen(woopas_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
                    out, err = woopas_open.communicate()

                    #if err == '':
                    #print "{}".format(out) *** REQUIRED???
                    print "*****************************************************************************"
                    print "Updated {}. Updating todo list at {}...".format(PROXY, PROXY_DEPLOY_PLAN)
                    print "*****************************************************************************"
                    update_done_status(PROXY,DONELIST)

                    print "*****************************************************************************"
                    print "Checking status of {}".format(PROXY)
                    print "*****************************************************************************"

                    try:
                        with settings(host_string = PROXY, user = FAB_USERNAME):
                            fabfile.check_proxy()
                    except Exception as e:
                        print "Failed to run check on {}\nError: {}\nPlease check manually".format(PROXY, e.message)
                        if fabfile._get_option() == "n":
                            sys.exit("Deployment aborted")
                    print "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"
                    print "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"

                    sleep(1)

                print "DEBUG: Completed deplpoyment to {} region {} for day {}".format(provider, region, day_TODO)
                update_done_status(plan_line,PROXY_DEPLOY_PLAN)
                if len(started_proxies_list) > 0:
                    print "\nINFO: Stopping started instances"
                    libcloud_api.stop_instance(started_proxies_list, provider=provider, region=region)

    print "\n** Deployment of day {} completed **".format(day_TODO)
    print "\nList of updated proxies to date for release {} can be found in {}".format(rel_num, DONELIST)

if __name__ == "__main__":

    if len(sys.argv) == 3:
        rel_num = sys.argv[1]
        PROXY_DEPLOY_PLAN = sys.argv[2]
    else:
        sys.exit("\nERROR: USAGE: woopas_amigo.py <release_number> <proxy_deployment_plan>\n")

    main(rel_num, PROXY_DEPLOY_PLAN)
