import sys
import deploy_release
import re
import os

def update_node_status(node,TODOLIST):
    temp_file_name = "{}.tmp".format(TODOLIST)
    with open(TODOLIST, "r")  as todo_file, open(temp_file_name,"w") as temp_file:
        for line in todo_file:
            update_regex_start = re.compile("^{},TODO".format(node))
            replace_str_start = "{},DONE".format(node)
            line = update_regex_start.sub(replace_str_start, line)

            update_regex_middle = re.compile(",{},TODO".format(node))
            replace_str_middle = ",{},DONE".format(node)
            line = update_regex_middle.sub(replace_str_middle, line)
            
            temp_file.write(line)
    os.remove(TODOLIST)
    os.rename(temp_file_name, TODOLIST)

home = os.path.expanduser('~')
filename = "{}/Documents/bitbucket/operations/deploy_release/deploy_release/release_Proxy_nodes/RL_17-08_done_proxies.txt".format(home)
with open(filename, 'r') as file:
    print "{}".format(file.read())
#update_node_status("app-101-core.eu-west-1a.ie.wandera.com","release_Core_nodes/release_nodes_Core_StackA_web101.list")
