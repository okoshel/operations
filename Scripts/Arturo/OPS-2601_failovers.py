#!/usr/local/bin/python

import os
import sys
current_dir = os.getcwd()
import argparse
import json
from datetime import date
import re
from time import sleep
import subprocess
from subprocess import Popen, PIPE
import fabfile
import libcloud_api
from fabric.context_managers import settings
import tempfile
import appscript
import platform

wandera_client_dir = os.path.join("/Users", "arturonoha", "Documents", "bitbucket", "wandera_client", "wandera_client")
wandera_client_client_dir = os.path.join(wandera_client_dir, "client")
wandera_client_service_dir = os.path.join(wandera_client_client_dir, "service")

import wandera_client
from wandera_client.client.service.proxy_manager import ProxyManagerClient
from wandera_client.resource import Server
from wandera_client.client.auth import ServiceAuthentication

today = date.today()
DATE = today.isoformat()
#operations_dir = "/Users/arturonoha/Documents/bitbucket/operations/Scripts/Arturo"
operations_dir = current_dir
proxies_release_dir = os.path.join(current_dir,"release_Proxy_nodes")
CANARY_PROXY = "aa2ef5898d7f40d0.node.lon3.uk.wandera.com"

TODOLIST = {}
proxy_name_regex = re.compile("[0-9a-zA-Z]*\.node\..*\.wandera.com")

home = os.path.expanduser('~')
CONFIG_FILE = os.path.join(home, ".woopas.json")

current_dir = os.getcwd()
WOOPAS_PLAN = os.path.join(current_dir, "release_Proxy_nodes", "proxy_deployment_plan.csv")

ticket_dir = os.path.join("/Users","arturonoha","Documents","Projects","OPS-2601")
update_script_name = "remove_filterservice_from_proxy-app.sh"
update_script = os.path.join(ticket_dir, update_script_name)
proxy_update_script = "/tmp/{}".format(update_script_name)

def init_ssh_user():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        FAB_USERNAME = config_json["woopas"]["ssh_username"]
    except Exception as e:
        sys.exit("ERROR: Failed to load ssh user from {}\n".format(CONFIG_FILE))

    return FAB_USERNAME

def init_proxy_manager():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        pm_url = config_json["woopas"]["proxy_manager_url"]
        pm_api_key = config_json["woopas"]["service_api_key"]
        pm_api_secret = config_json["woopas"]["service_api_secret"]
        pm_auth = ServiceAuthentication(pm_api_key, pm_api_secret)
        pmc = ProxyManagerClient(pm_url,auth=pm_auth)
    except Exception as e:
        sys.exit("ERROR: Failed to load ProxyManagerClient\n({})".format(e.message))

    return pmc

def clear_screen():
    clear = subprocess.Popen( "cls" if platform.system() == "Windows" else "clear", shell=True)
    clear.communicate()

def update_done_status(done_line,FILE_TO_UPDATE):
    temp_file_name = "{}.tmp".format(FILE_TO_UPDATE)
    done_line = done_line.replace(",TODO","")
    #print "DEBUG: updating line: \"{}\"".format(done_line)
    with open(FILE_TO_UPDATE, "r")  as proxy_plan, open(temp_file_name,"w") as temp_file:
        for line in proxy_plan:
            update_regex = re.compile("{},TODO".format(done_line))
            replace_str = "{},DONE".format(done_line)
            line = update_regex.sub(replace_str, line)
            #print "DEBUG: Updated line: \"{}\"".format(line)
            temp_file.write(line)
    os.remove(FILE_TO_UPDATE)
    os.rename(temp_file_name, FILE_TO_UPDATE)

def new_window(active_svr, user):
    active_cmd = "cd {}; fab -H {} -u {} tail_rabbit_log".format(operations_dir, active_svr, user)
    appscript.app('Terminal').do_script(active_cmd)
    #os.system("start cmd /K {}".format(active_cmd))
    #Popen('xterm -hold -e "{}}"'.format(active_cmd))
    #appscript.app('Terminal').do_script(active_cmd)

def main(dryrun):
    clear_screen()

    if not os.path.isfile(CONFIG_FILE):
        sys.exit("\nERROR: Configuration file {} not found.\n".format(CONFIG_FILE))

    FAB_USERNAME = init_ssh_user()

    pmc = init_proxy_manager()

    drivers = libcloud_api.init_all_drivers()

    DONELIST = os.path.join(proxies_release_dir,"OPS_2601_done_proxies.txt")
    done_proxies_list = []
    if os.path.isfile(DONELIST):
        with open(DONELIST, "r") as done_proxies_file:
            for line in done_proxies_file:
                line = line.rstrip()
                done_proxies_list.append(line.split(',')[1])

    print "Getting all instances"
    all_instances = libcloud_api.get_all_instances(drivers)

    all_proxies = {}
    print "Getting the list of proxies"
    for driver_name in all_instances: # iterate through all the instances
        for instance in all_instances[driver_name]:
            if proxy_name_regex.match(instance.name): # if the name of the instance matches the proxy name convention process
                #print "proxy: {}".format(instance.name)
                proxy_name = instance.name
                provider = driver_name.split('_')[0]

                proxy_running_state = instance.state
                pm_proxy = ProxyManagerClient.node_by_name(pmc, instance.name)
                proxy_pm_state = pm_proxy.state

                if provider in all_proxies: # add the instance to the list of instances associated to the provider
                    if proxy_pm_state in all_proxies[provider]:
                        all_proxies[provider][proxy_pm_state].append(instance)
                    else:
                        all_proxies[provider][proxy_pm_state] = [instance]
                else:
                    all_proxies[provider] = {}
                    all_proxies[provider][proxy_pm_state] = [instance]

                with open(DONELIST, "a+") as done_proxies_file:
                    done_proxies_file.write("{},{},TODO\n".format(DATE, proxy_name))

    for provider in all_proxies:
        started_proxies_list = []
        print "** Starting update of {} proxies".format(provider)
        # Print list of proxies to perform for the provider and ask for confirmation to continue
        print "List of proxies:"
        for proxy_pm_state in sorted(all_proxies[provider].keys(), reverse=True):
            print "-- {} proxies".format(proxy_pm_state)
            for proxy_instance in sorted(all_proxies[provider][proxy_pm_state]):
                print "{}".format(proxy_instance.name)
        if fabfile._get_option() != "Y":
            sys.exit("\nAborted by user\n")
        print "\n------------------------------\n"
        # iterate each proxyManager state available for the provider at hand
        for proxy_pm_state in sorted(all_proxies[provider].keys(), reverse=True):
            print "- {} proxies in {}".format(proxy_pm_state, provider)
            for proxy_instance in sorted(all_proxies[provider][proxy_pm_state]):
                proxy_name = proxy_instance.name
                # check if the proxy was already updated in a previous run and if so try to get status of the service (should return unknown) and skip
                if proxy_name in done_proxies_list:
                    print "\n{} has already been updated. Will skip\n".format(proxy_name)
                    try:
                        print "\n* checking status of filter service:"
                        fabfile.service_status("filterservice")
                        print "\n* checking filterservice files and folders:"
                        print "- dpkg -l | grep filterservice"
                        print "- find /opt /etc /var/lib -name \"*filterservice*\""
                        with settings(host_string = proxy_name, user = FAB_USERNAME):
                            sudo("dpkg -l | grep filterservice; find /opt /etc /var/lib -name \"*filterservice*\"")
                    except Exception as e:
                        print "++++++++++++++++++++++++++++++++++++++++++++++++"
                        continue
                    print "++++++++++++++++++++++++++++++++++++++++++++++++"
                    continue
                print "- {}".format(proxy_name)
                proxy_running_state = proxy_instance.state

                if proxy_running_state != "running":
                    if proxy_running_state == "stopped":
                        print "INFO: {} is STOPPED. Will start it".format(proxy_name)
                        if dryrun == "False" or not dryrun:
                            libcloud_api.start_instance(proxy.name, drivers)
                        started_proxies_list.append(proxy_name)
                    else:
                        print "ERROR: Unknown running state for {} : ({})".format(proxy_name, proxy_running_state)
                        if fabfile._get_option("Would you like to try to start it?") == "Y":
                            print "INFO: {} is STOPPED. Will start it".format(proxy_name)
                            if dryrun == "False" or not dryrun:
                                if libcloud_api.start_instance(proxy.name, drivers):
                                    started_proxies_list.append(proxy_name)
                                else:
                                    print "\nERROR: Failed to start {}. Skipping update".format(proxy_name)
                                    continue
                            else:
                                started_proxies_list.append(proxy_name)
                        else:
                            print "Skipping update of {}".format(proxy_name)
                            continue
                # open a new terminal window and tail rabbit log
                print "- Opening new window with tail logs"
                if dryrun == "False" or not dryrun:
                    new_window(proxy_name, FAB_USERNAME)

                # Failover active proxies
                if proxy_pm_state == "active":
                    pm_proxy = ProxyManagerClient.node_by_name(pmc, proxy_name)
                    failovers = ProxyController.find_failovers(woopas_controler, pm_proxy)
                    failover = failovers[0]["nodeName"]
                    print "\nWARN: Failing active proxy {} to {}".format(proxy_name, failover)
                    failover_cmd = "woopas failover {} {}".format(proxy_name, failover)
                    print "{}".format(failover_cmd)
                    if dryrun == "False" or not dryrun:
                        failover_open = Popen(failover_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
                        failover_open.communicate()
                # Running puppet agent
                with settings(host_string=puppet_master, user=FAB_USERNAME):
                    # Run puppet noop
                    noop = fabfile.run_puppet_deploy(proxy_name, noop=True)
                    if noop == 4:
                        sys.exit("** Update aborted by user **")
                    elif noop == 3:
                        return noop
                    # If not dryrun, then run live puppet execution
                    if dryrun == "False" or not dryrun:
                        live = fabfile.run_puppet_deploy(proxy_name, noop=False)
                        #live = 2
                        if live == -1: # user did not run puppet live. check if he wants to continue with update. If no skip to next server
                            if fabfile._get_option("\nContinue with rest of server update?") != "Y":
                                continue
                            else:
                                sys.exit("** Update aborted by user **")
                        elif live == 4: # User aborted execution after puppet error
                            sys.exit("** Update aborted by user **")
                        elif live == 3: # User chose to skip this node's update after puppet error
                            continue
                # Transfer update script
                scp_cmd = "scp {} {}:{}".format(update_script, proxy_name, proxy_update_script)
                print "- {}".format(scp_cmd)
                scp_open = Popen(scp_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
                scp_open.communicate()
                print "- Change mode to executable for {}:{}".format(proxy_name, proxy_update_script)
                chmod_cmp = "chmod +x {}".format(proxy_update_script)
                with settings(host_string = proxy_name, user = FAB_USERNAME):
                    sudo(chmod_cmp)
                # Running the update script
                print "RUNNING update: {}".format(update_script)
                if dryrun == "False" or not dryrun:
                    run_script_cmp = "{0} --dryrun False".format(proxy_update_script)
                    print "{}".format(run_script_cmp)
                    with settings(host_string = proxy_name, user = FAB_USERNAME):
                        sudo(run_script_cmp)
                else:
                    run_script_cmp = "{0} --dryrun True".format(proxy_update_script)
                    print "{}".format(run_script_cmp)
                    with settings(host_string = proxy_name, user = FAB_USERNAME):
                        sudo(run_script_cmp)
                # Fail back over active proxies
                if proxy_pm_state == "active":
                    failover_cmd = "woopas failover {} {}".format(failover, proxy_name)
                    print "\nWARN: Failing back proxy {} to {}".format(failover, proxy_name)
                    print "{}".format(failover_cmd)
                    if dryrun == "False" or not dryrun:
                        failover_open = Popen(failover_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
                        failover_open.communicate()
                # update tracking file
                print "*****************************************************************************"
                print "Updated {}. Updating todo list at {}...".format(proxy_name, DONELIST)
                print "*****************************************************************************"
                update_done_status(proxy_name,DONELIST)

                #Run status check on the proxy: proxy service, services running, log tail, status of td-agent-proxy_events_mongo_ireland
                print "*****************************************************************************"
                print "Checking status of {}".format(proxy_name)
                print "*****************************************************************************"

                try:
                    with settings(host_string = proxy_name, user = FAB_USERNAME):
                        fabfile.check_proxy()
                        print "\n* checking status of filter service:"
                        fabfile.service_status("filterservice")
                        print "\n* checking filterservice files and folders:"
                        print "- dpkg -l | grep filterservice"
                        print "- find /opt /etc /var/lib -name \"*filterservice*\""
                        with settings(host_string = proxy_name, user = FAB_USERNAME):
                            sudo("dpkg -l | grep filterservice; find /opt /etc /var/lib -name \"*filterservice*\"")
                except Exception as e:
                    print "Failed to run check on {}\nError: {}\nPlease check manually".format(proxy_name, e.message)
                    if fabfile._get_option() == "n":
                        sys.exit("Deployment aborted")
                print "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"

                sleep(1)

                if dryrun == "False" or not dryrun:
                    if fabfile._get_option("\nUpdate of {} completed.\nContinue with next?".format(proxy_name)) != "Y":
                        sys.exit("\Aborted by user\n")
        # Stop started proxies
        if len(started_proxies_list) > 0:
            print "\nINFO: Stopping started instances."
            for instance in sorted(started_proxies_list):
                print "{}".format(instance)
            if dryrun == "False" or not dryrun:
                libcloud_api.stop_instance(started_proxies_list, provider=provider)
            print "\n*** CHECK PINGDOM STATUS ***\n"
            time.sleep(2)

        if fabfile._get_option("\nFinished update of {} proxies. Proceed with next provider?".format(provider)) != "Y":
            sys.exit("\nAborted by user\n")

def parse_options(options):
    parser = argparse.ArgumentParser()
    #parser.add_argument('--proxy-list', dest = "proxy_list_input", help = "File with list of proxies to update")
    parser.add_argument('--dryrun', dest = "dryrun", default = True)

    return parser.parse_args()

def usage():
    clear_screen()
    print "\nUSAGE: {} --dryrun <True/False>\n".format(sys.argv[0])
    sys.exit(1)

if __name__ == "__main__":
    options = parse_options(sys.argv[1:])

    dryrun = options.dryrun
    clear_screen()
    if fabfile._get_option("- dryrun: {}\nContinue?".format(dryrun)) != "Y":
        sys.exit("\n** Aborted by user **\n")
    main(dryrun)
