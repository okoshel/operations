#!/bin/bash

########################################################################
# flatten_services_verions.sh
# Author: Arturo Noha
# Version 1.0
# Date: 5 April 2017
#---------------------------------------------------------------
# This script
# - removes the old versions folders from puppet/evironments/<env>/modules
# - leaves the latest folder unchanged
#---------------------------------------------------------------
# OPTIONS (ALL MANDATORY)
# --puppet: the puppet repository folder
# --env: the environment (staging, prodduction,...)
# --dryrun: true / false
#########################################################################

usage() {
  echo "$0 --puppet <puppet_path> --env <environments> --dryrun <true/false>"
  exit 1
}

execute() {
  #for serv in `ls $SERVICES_ROOT`
  #for serv in "userservice2_enterprise"
  find $SERVICES_ROOT -type d -maxdepth 1 | sed -e "s:.*/::g" | while read serv; do
    echo ">> Checking $serv"
    folders=('files' 'templates')
    for folder in ${folders[@]}; do
        myfolder=$SERVICES_ROOT/$serv/$folder
        if [[ -d $myfolder ]]; then
            versions_count=`ls $myfolder | egrep "^[0-9]+\.[0-9]+\.[0-9]+" | wc -l | tr -d ' '`
            if [[ $versions_count -ne 0 || -d $myfolder/2015 ]]; then
            #if [[ `ls $myfolder | egrep "^[0-9]+\.[0-9]+\.[0-9]+"` ]]; then
              echo "> $myfolder"

              if [[ -d $myfolder/2015 ]]; then
                if [[ $dryrun == true ]]; then
                  echo "rm -r $myfolder/2015"
                else
                  #echo "EXECUTE rm -rv $myfolder/$version"
                  rm -rv $myfolder/2015
                fi
              fi
              if [[ -d $myfolder/latest ]]; then
                if [[ $dryrun == true ]]; then
                  echo "rm -r $myfolder/latest"
                else
                  #1echo "EXECUTE mv -v $myfolder/$version $myfolder/latest"
                  rm -r $myfolder/latest
                fi
              fi

              versions=`ls $myfolder | egrep "^[0-9]+\.[0-9]+\.[0-9]+" | sort -t . -k 2 -g`
              #echo "`echo $versions | wc -l` versions"
              count=0
              #for version in $versions; do
              find $myfolder -type d -maxdepth 1 -name [[:digit:]+.[:digit:]+.[:digit:]+].* | sed -e "s:.*/::g" | sort -t . -k 2 -g | while read version; do
              #while [[ $count -lt $(($versions_count - 1)) ]]; do
                if [[ $count -lt $(($versions_count - 1)) ]]; then
                  if [[ $dryrun == true ]]; then
                    echo "rm -r $myfolder/$version"
                  else
                    #echo "EXECUTE rm -rv $myfolder/$version"
                    rm -rv $myfolder/$version
                  fi
                  count=$(($count + 1))
                else
                  echo "latest version. Leaving $myfolder/$version untouched"
                fi
              done
              echo "----"
            else
              echo "[INFO] No versions folders under $myfolder"
            fi
        else
            echo "[INFO] $myfolder does not exist. No action taken"
        fi
    done
    echo "================================="
  done
}

display_new_structure(){
  find $SERVICES_ROOT -type d -maxdepth 1 | sed -e "s:.*/::g" | while read serv; do
    echo ">> Checking $serv"
    folders=('files' 'templates')
    for folder in ${folders[@]}; do
        myfolder=$SERVICES_ROOT/$serv/$folder
        if [[ -d $myfolder ]]; then
          echo "> $myfolder"
          ls $myfolder
        else
            echo "[INFO] $myfolder does not exist."
        fi
    done
    echo "================================="
  done
}

parse_options() {
  while getopts ":-:" opt; do
  	case "$opt" in
  		-)
  			case "$OPTARG" in
  				"puppet")
  					PUPPET_PATH=${!OPTIND}
  					OPTIND=$((OPTIND + 1))
  					;;
  				"env")
  					PUPPET_ENV=${!OPTIND}
  					OPTIND=$((OPTIND + 1))
  					;;
  				"dryrun")
  					dryrun=${!OPTIND}
  					OPTIND=$((OPTIND + 1))
  					;;
  				"help")
  					usage
  					;;
  			esac
  			;;
  		*)
  			usage
  			;;
  	esac
  done

  if [[ ${PUPPET_PATH} == "" || $PUPPET_ENV == "" || $dryrun == "" ]]; then
    usage
  fi

  if [[ ! -d ${PUPPET_PATH} ]]; then
    echo "ERROR: ${PUPPET_PATH} does not exist"
    exit 2
  fi

  SERVICES_ROOT="${PUPPET_PATH}/environments/${PUPPET_ENV}/modules"
  if [[ ! -d ${SERVICES_ROOT} ]]; then
    echo "ERROR: ${SERVICES_ROOT} does not exist"
    exit 2
  fi

  if [[ "$dryrun" != "true" && "$dryrun" != "false" ]]; then
    usage
  fi
}

check_options() {
  if [[ $# -lt 1 ]]; then
    choice="null"
  else
    choice=$1
  fi

  clear
  echo ""
  echo "Flattening the versions structure with the following options:"
  echo "- Puppet path: $PUPPET_PATH"
  echo "- Environment: $PUPPET_ENV"
  echo "- Modules: $SERVICES_ROOT"
  if [[ $dryrun == false ]]; then
    echo "- Dryrun: $dryrun ** WARNING ** All actions (removal and renaming of versions folders will be executed)"
  else
    echo "- Dryrun: $dryrun -> No actions will be performed"
  fi
  echo ""

  if [[ $choice == "null" ]]; then
    read -p "Do you wish to proceed? [Y/n]: " choice
  else
    read -p "Please enter a valid option. Do you wish to proceed? [Y/n]: " choice
  fi

  while [[ $choice != "Y" && $choice != "n" ]]; do
    check_options "$choice"
  done

  if [[ $choice == "n" ]]; then
    echo "BYE..."
    exit 0
  fi
}

############## MAIN ################
parse_options $@

check_options

#execute

if [[ $dryrun == true ]]; then
  echo ""
  echo "** Dryrun - no actions performed. Please double check the output before running"
else
  echo ""
  echo "************************************************"
  echo ""
  echo "Flattening completed. New structure:"
  echo "------------------------------------"
  echo ""
  sleep 2

  display_new_structure
fi
