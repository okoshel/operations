import os
import sys
current_dir = os.getcwd()
import json
from datetime import date
import re
from time import sleep
import subprocess
from subprocess import Popen, PIPE
import fabfile
import libcloud_api
from fabric.context_managers import settings
import tempfile
import appscript
import platform

wandera_client_dir = os.path.join("/Users", "arturonoha", "Documents", "bitbucket", "wandera_client", "wandera_client")
wandera_client_client_dir = os.path.join(wandera_client_dir, "client")
wandera_client_service_dir = os.path.join(wandera_client_client_dir, "service")

import wandera_client
from wandera_client.client.service.proxy_manager import ProxyManagerClient
from wandera_client.resource import Server
from wandera_client.client.auth import ServiceAuthentication


#sys.path.append(current_dir + '/../../../wandera_woopas/wandera_woopas/')
#from pm import ProxyController as pm
'''sys.path.append(wandera_client_dir)
sys.path.append(wandera_client_client_dir)
sys.path.append(wandera_client_service_dir)
import proxy_manager as pm'''
#ProxyManagerClient

today = date.today()
DATE = today.isoformat()
FAB_USERNAME = "anoha"
#operations_dir = "/Users/arturonoha/Documents/bitbucket/operations/Scripts/Arturo"
operations_dir = current_dir
proxies_release_dir = os.path.join(current_dir,"release_Proxy_nodes")
CANARY_PROXY = "aa2ef5898d7f40d0.node.lon3.uk.wandera.com"

TODOLIST = {}
proxy_name_regex = re.compile("[0-9a-zA-Z]*\.node\.")

CONFIG_FILE = "/Users/arturonoha/.woopas.json"

def init_proxy_manager():
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        pm_url = config_json["woopas"]["proxy_manager_url"]
        pm_api_key = config_json["woopas"]["service_api_key"]
        pm_api_secret = config_json["woopas"]["service_api_secret"]
        pm_auth = ServiceAuthentication(pm_api_key, pm_api_secret)
        pmc = ProxyManagerClient(pm_url,auth=pm_auth)
    except Exception as e:
        sys.exit("ERROR: Failed to load ProxyManagerClient\n({})".format(e.message))

    return pmc

def run_woopas(PROXY):
    woopas_cmd = "woopas upgrade_proxy {}".format(PROXY)
    print "{}".format(woopas_cmd)
    woopas_open = Popen(woopas_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
    out, err = woopas_open.communicate()

    print "*****************************************************************************"
    print "Checking status of {}".format(PROXY)
    print "*****************************************************************************"

    try:
        with settings(host_string = PROXY, user = FAB_USERNAME):
            fabfile.check_proxy()
    except Exception as e:
        print "Failed to run check on {}\nError: {}\nPlease check manually".format(PROXY, e.message)
        if fabfile._get_option() == "n":
            sys.exit("Deployment aborted")
    print "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"
    print "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"

    sleep(1)

def clear_screen():
    clear = subprocess.Popen( "cls" if platform.system() == "Windows" else "clear", shell=True)
    clear.communicate()

if __name__ == "__main__":

    if len(sys.argv) < 2:
        sys.exit("\nERROR: Missing proxy to check")

    for proxy in sys.argv[1:]:
        pmc = init_proxy_manager()
        pm_proxy = ProxyManagerClient.node_by_name(pmc, proxy)
        print "{} is {}".format(proxy, pm_proxy.state)
        run_woopas(proxy)
