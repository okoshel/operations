server_check_function = {
    'app' : 'check_app',
    'fid' : 'check_delta',
    'fis' : 'check_fis',
    'con' : 'check_connector',
    'sie' : 'check_siem',
    'prx' : 'check_proxy',
    'web' : 'check_portal',
    'osc' : 'check_osc',
    'bea' : 'check_beacon'
}

service_types = ['app','web','fid','fis','con','sie','prx','osc','bea']
env_extensions = {
    'stgn' : 'biz',
    'prod' : 'com',
    'intg' : 'biz'
}

puppet_master = "puppet.snappli.net"
puppet_master_dir = "/opt/puppet"
environments = ['stgn','dev','intg','prod-proxy','prod-core','prod-services','core-full']
live_environments = ['prod','core','cops','prod-proxy','prod-core','prod-services','core-full']
woopas_services = ['proxy-enterprise','rabbit-enterprise','proxy_enterprise','proxy']
