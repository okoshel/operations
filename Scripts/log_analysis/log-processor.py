#!/usr/bin/python
import sys
import pprint
import re

log_levels = {
              "DEBUG": {"total":0},
              "INFO": {"total":0},
              "ERROR": {"total":0},
              "WARN": {"total":0},
              "TRACE": {"total":0},
              "EXCEPTION": {"total":0},
              "NOLEVEL": {"total":0}
              }

def process_log_level(line):
  if re.search("^201",line):
    log_level = line.split(' ')[2]
    if not log_level in log_levels:
        if log_level == '!':
            log_level = "EXCEPTION"
        else:
            log_level = "NOLEVEL"
    return log_level

def remove_guids_and_json(line):
    caller = line.split(' ')[3].strip().split(' ')[0]
    new_text = line.replace(caller,"CALLER")
    json_start = None
    try:
        json_start = new_text.index('{')
    except ValueError, e:
        pass
    if json_start:
        new_text = new_text[:json_start].strip().strip("'")
    else:
        new_text = new_text
    # remove email addresses
    email_regex = re.compile('([\w\-\.]+@(\w[\w\-]+\.)+[\w\-]+)')
    for match in email_regex.findall(new_text):
        new_text = new_text.replace(match[0],"EMAIL")
    # remove any @ABCDEFG strings
    at_regex = re.compile('@[0-9a-fA-F]+')
    for match in at_regex.findall(new_text):
        new_text = new_text.replace(match,"")

    numbers_removed = re.sub("\S*\d\S*", "", new_text).strip()
    return numbers_removed.replace("CALLER", caller)

def process_line(line):
  if re.search("^201",line):
    log_level = process_log_level(line)
    log_levels[log_level]["total"] += 1
    if not log_level == "NOLEVEL":
        if log_level == "EXCEPTION":
            exception_type = line.split(' ')[1]
            if not exception_type == "at": # ignore lines starting ! at ...
                if exception_type in log_levels[log_level]:
                    log_levels[log_level][exception_type] += 1
                else:
                    log_levels[log_level][exception_type] = 1
        else:
            pp.pprint(line)
            caller = line.split(' ')[4]
            #caller = line.split(' ')[3].strip().split(' ')[2]
            #if not caller.startswith("rabbit") and not caller.startswith("org"):
            #    print caller
            #    sys.exit(1)
            if not caller in log_levels[log_level]:
                log_levels[log_level][caller] = {}
            stripped_line = line
            stripped_line = remove_guids_and_json(line)
            log_message = stripped_line.split(caller)[1].strip()
            if not log_message in log_levels[log_level][caller]:
                log_levels[log_level][caller][log_message] = 1
            else:
                log_levels[log_level][caller][log_message] += 1

def format_csv_files():
    print "LOG LINES CATAGORIES"
    print
    for key in log_levels:
        print key + ",",
    print
    for key in log_levels:
        print str(log_levels[key]["total"]) + ",",
    print
    print
    for key in log_levels:
        print key
        for caller in log_levels[key]:
            if not caller == "total":
                print caller
                print
                for log in log_levels[key][caller]:
                    print log + ",",
                print
                for log in log_levels[key][caller]:
                    print str(log_levels[key][caller][log]) + ",",
                print
                print
                print

def convert_dict_to_csv(dictionary):
    csv = ""
    for key in dictionary:
        if isinstance(dictionary[key], dict):
            csv = csv + convert_dict_to_csv(dictionary[key])
        else:
            for key in dictionary:
                csv = csv + key + ","
            csv = csv + '\n'
            for key in dictionary:
                csv = csv + str(dictionary[key]) + ","
            csv = csv + '\n\n'
            break
    return csv

pp = pprint.PrettyPrinter(indent=2)

# PRE PROCESSED
#log_levels = preprocessed_log
#format_csv_files()

with open("/Users/gchaplin/tmp/full_rabbit_2014-02-18_2014.log") as log_file:
    for line in log_file:
        process_line(line)
        pp.pprint(line)
#        pp.pprint(process_log_level(line))
print("Finished processing log")
print("Report:")
#pp.pprint(log_levels)
#for key in log_levels:
 #   print key
 #   print convert_dict_to_csv(log_levels[key])

format_csv_files()
