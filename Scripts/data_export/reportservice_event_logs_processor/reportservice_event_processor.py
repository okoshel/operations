import pymongo
from pymongo import MongoClient, MongoReplicaSetClient
import json

# PROD mongo Mongo connecting to mongo
client = MongoClient('localhost:27017')
# client.wandera_live_ent_eu.authenticate('', '')
db = client.reportServiceEventLogs

#connecting to collections
logs_collection = db.eventLogs

counter = 0
with open('file.log') as f:
    for line in f:
        counter=counter+1
        # convert JSON to Python
        json_object = json.loads(line)
        # print "JSON :", json_object['eventType'], json_object['timestamp']

        if json_object['eventType'] == "REPORT_ACCESSED":
            try:
                json_output = json.dumps(
                {"timestamp":json_object['timestamp'],
                "customerId":json_object['customerId'],
                "eventType":json_object['eventType'],
                "parameters": { "reportType": json_object['parameters']['reportType'],
                "guid": json_object['parameters']['guid'],
                "id": json_object['parameters']['id']
                }},default=lambda x:x.__dict__)
                json_output = json.loads(json_output)
                result = logs_collection.insert(json_output)
                print "Object added: ", result, "type : "
            except Exception as e:
                print "Ooops, something went wrong here (1)! Error: ", e
                print "JSON OBJECT :", json_output

        if json_object['eventType'] == "QUERY_FINISHED":
            try:
                json_output = json.dumps(
                {"timestamp":json_object['timestamp'],
                "customerId":json_object['customerId'],
                "eventType":json_object['eventType'],
                "parameters": { "reportType": json_object['parameters']['reportType'],
                "guid": json_object['parameters']['guid'],
                "id": json_object['parameters']['id']
                }},default=lambda x:x.__dict__)
                json_output = json.loads(json_output)
                result = logs_collection.insert(json_output)
                print "Object added: ", result, "type : "
            except Exception as e:
                print "Ooops, something went wrong here (1)! Error: ", e
                print "JSON OBJECT :", json_output
