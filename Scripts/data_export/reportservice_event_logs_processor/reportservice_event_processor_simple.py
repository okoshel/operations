import pymongo
from pymongo import MongoClient, MongoReplicaSetClient
import json

# PROD mongo Mongo connecting to mongo
client = MongoClient('localhost:27017')
# client.wandera_live_ent_eu.authenticate('', '')
db = client.reportServiceEventLogs

#connecting to collections
logs_collection = db.eventLogs

counter = 0
with open('file.log') as f:
    for line in f:
        counter=counter+1
        # convert JSON to Python
        json_object = json.loads(line)
        result = logs_collection.insert(json_object)
        print "JSON :", json_object['eventType'], json_object['timestamp'], "added with _id:" ,result
print "Summary: ", counter, "has been loaded"
