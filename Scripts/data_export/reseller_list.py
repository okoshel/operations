import pymongo
import csv
import sys, getopt
import psycopg2
import datetime
import dns.resolver
from pymongo import MongoClient, MongoReplicaSetClient


# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu


# Open csv file
csv_out = open('resellers.csv', 'wb')
writer = csv.writer(csv_out)

reseller_collection = db.reseller
reseller_list = reseller_collection.find({})


for entries in reseller_list:

  print "Exporting for:", entries['name']
  csv_to_write = entries['name'],''
  writer.writerow(csv_to_write)

#csv_out.close()