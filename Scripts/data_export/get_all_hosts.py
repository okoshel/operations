# Export all hosts from cmdb. Usefull if you need to sync up /etc/hosts with all Wandera hosts
import re
import pymongo
# import collections
from pymongo import MongoClient

# Connect to mongo
client = MongoClient('54.246.142.127:27017')
db = client.cmdb
instances_collection = db.instances

# db.instances.find({"exists":true},{"hostname":1,"external_ip":1});
instances = instances_collection.find({"exists":True},{"hostname":1,"external_ip":1})

for instance in instances:
# Catch None type of fileds 
    try:
        print instance['external_ip'],"   ",instance['hostname']
    except KeyError:
        print("")

