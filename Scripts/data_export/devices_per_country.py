import pymongo
import csv
import sys, getopt
#import psycopg2
import datetime
from pymongo import MongoClient, MongoReplicaSetClient

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

#connecting to collections
device_collection = db.userDevice

current_date = datetime.date.today()
# Write to text file
# f = open('device_list.txt', 'w')

# Write to CSV
csv_file_name = 'devices_per_country_%s.csv' %(current_date)
csv_out = open(csv_file_name, 'wb')
writer = csv.writer(csv_out)
header = 'Country code', 'Device number'
#writer.writerow(header)
emptyline = '','',''

timezone = [{"$group" : {"_id": "$timeZone"} },{"$sort": {"timezone": 1}} ]
timezones_all = device_collection.aggregate(timezone)["result"]
timezones_all.sort()

# 1. Per timezone deleted=false
timezone = [{"$match": {"deleted": False}},{"$group" : {"_id": "$timeZone", "total": { "$sum": 1 }} },{"$sort": {"timezone": -1}} ]
deleted_false = device_collection.aggregate(timezone)["result"]

# 2. Per timezone deleted=false and port assigned
timezone = [{"$match": {"deleted": False,"proxy.port":{"$exists":True}}},{"$group" : {"_id": "$timeZone", "total": { "$sum": 1 }} },{"$sort": {"timezone": -1}} ]
deleted_false_port_assigned = device_collection.aggregate(timezone)["result"]

# 1. Per timezone deleted=false
timezone = [{"$match": {"deleted": False}},{"$group" : {"_id": "$timeZone", "total": { "$sum": 1 }} },{"$sort": {"timezone": -1}} ]
result = device_collection.aggregate(timezone)["result"]
writer.writerow(header)
query_desciption='Per timezone deleted=false',''
writer.writerow(query_desciption)

for i in result:
  print i['_id'], i['total']
  csv_to_write = i['_id'], i['total']
  writer.writerow(csv_to_write)


# 2. Per timezone deleted=false and port assigned
timezone = [{"$match": {"deleted": False,"proxy.port":{"$exists":True}}},{"$group" : {"_id": "$timeZone", "total": { "$sum": 1 }} },{"$sort": {"timezone": -1}} ]
result = device_collection.aggregate(timezone)["result"]
query_desciption='Per timezone deleted=false and port assigned',''
writer.writerow(query_desciption)

for i in result:
  print i['_id'], i['total']
  csv_to_write = i['_id'], i['total']
  writer.writerow(csv_to_write)


# 3. Per timezone deleted=false and port not assigned
timezone = [{"$match": {"deleted": False,"proxy.port":{"$exists":False}}},{"$group" : {"_id": "$timeZone", "total": { "$sum": 1 }} },{"$sort": {"timezone": -1}} ]
result = device_collection.aggregate(timezone)["result"]
query_desciption='Per timezone deleted=false and port not assigned',''
writer.writerow(query_desciption)

for i in result:
  print i['_id'], i['total']
  csv_to_write = i['_id'], i['total']
  writer.writerow(csv_to_write)

# 4. Per timezone deleted=true
timezone = [{"$match": {"deleted": True,}},{"$group" : {"_id": "$timeZone", "total": { "$sum": 1 }} },{"$sort": {"timezone": -1}} ]
result = device_collection.aggregate(timezone)["result"]
query_desciption='Per timezone deleted=true',''
writer.writerow(query_desciption)

for i in result:
  print i['_id'], i['total']
  csv_to_write = i['_id'], i['total']
  writer.writerow(csv_to_write)

# 5. Per timezone deleted=trye and port assigned
timezone = [{"$match": {"deleted": True,}},{"$group" : {"_id": "$timeZone", "total": { "$sum": 1 }} },{"$sort": {"timezone": -1}} ]
result = device_collection.aggregate(timezone)["result"]
query_desciption='Per timezone deleted=trye and port assigned',''
writer.writerow(query_desciption)

for i in result:
  print i['_id'], i['total']
  csv_to_write = i['_id'], i['total']
  writer.writerow(csv_to_write)


# 1. Per country deleted=false
country = [{"$match": {"deleted": False}},{"$group" : {"_id": "$info.carrier.isoCountryCode", "total": { "$sum": 1 }} },{"$sort": {"info.carrier.isoCountryCode": -1}} ]
result = device_collection.aggregate(country)["result"]
writer.writerow(header)
query_desciption='Per country deleted=false',''
writer.writerow(query_desciption)

for i in result:
  print i['_id'], i['total']
  csv_to_write = i['_id'], i['total']
  writer.writerow(csv_to_write)


# 2. Per country deleted=false and port assigned
country = [{"$match": {"deleted": False,"proxy.port":{"$exists":True}}},{"$group" : {"_id": "$info.carrier.isoCountryCode", "total": { "$sum": 1 }} },{"$sort": {"info.carrier.isoCountryCode": -1}} ]
result = device_collection.aggregate(country)["result"]
query_desciption='Per country deleted=false and port assigned',''
writer.writerow(query_desciption)

for i in result:
  print i['_id'], i['total']
  csv_to_write = i['_id'], i['total']
  writer.writerow(csv_to_write)


# 3. Per country deleted=false and port not assigned
country = [{"$match": {"deleted": False,"proxy.port":{"$exists":False}}},{"$group" : {"_id": "$info.carrier.isoCountryCode", "total": { "$sum": 1 }} },{"$sort": {"info.carrier.isoCountryCode": -1}} ]
result = device_collection.aggregate(country)["result"]
query_desciption='Per country deleted=false and port not assigned',''
writer.writerow(query_desciption)

for i in result:
  print i['_id'], i['total']
  csv_to_write = i['_id'], i['total']
  writer.writerow(csv_to_write)

# 4. Per country deleted=true
country = [{"$match": {"deleted": True,}},{"$group" : {"_id": "$info.carrier.isoCountryCode", "total": { "$sum": 1 }} },{"$sort": {"info.carrier.isoCountryCode": -1}} ]
result = device_collection.aggregate(country)["result"]
query_desciption='Per country deleted=true',''
writer.writerow(query_desciption)

for i in result:
  print i['_id'], i['total']
  csv_to_write = i['_id'], i['total']
  writer.writerow(csv_to_write)

# 5. Per country deleted=trye and port assigned
country = [{"$match": {"deleted": True,}},{"$group" : {"_id": "$info.carrier.isoCountryCode", "total": { "$sum": 1 }} },{"$sort": {"info.carrier.isoCountryCode": -1}} ]
result = device_collection.aggregate(country)["result"]
query_desciption='Per country deleted=trye and port assigned',''
writer.writerow(query_desciption)

for i in result:
  print i['_id'], i['total']
  csv_to_write = i['_id'], i['total']
  writer.writerow(csv_to_write)



csv_out.close()
