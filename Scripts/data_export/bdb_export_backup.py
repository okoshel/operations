#####################################################################################################################################################
# bdb backup. Add to crontab
#####################################################################################################################################################

import datetime, json, os, subprocess, time
import boto
from boto.s3.connection import S3Connection
from datetime import timedelta, datetime, tzinfo
from time import gmtime, strftime
import smtplib
from email.mime.text import MIMEText


# Global variables:
# TODO: Change s3_bucket folder
# s3_bucket = "s3://backup.wandera.net/tmp/delta_bdb_backup/"
s3_bucket = "s3://backup.wandera.net/tmp/"
current_time = strftime("%Y-%m-%d-%H-%M-%S", gmtime())
today = strftime("%Y-%m-%d", gmtime())
filename = "bdb_backup.%s.tar.gz" %(current_time)
conn = S3Connection('AKIAIIUKM5WIPNOMINQQ', 'YNZJL0wq+E0zudClNlhb+PvZuqb1x7+FHm0gIRyl')
mybucket = conn.get_bucket('stgn.filterservicedb.wandera.net')

command_stop_filterservice = "service filterservice stop"
command_start_filterservice = "service filterservice start"
command_stop_delta_machine_security_defs = 'curl -vvv -X POST "http://127.0.0.1:3998/tasks/StopStateMachineTask?dbType=SECURITY_DEFINITION"'
command_stop_delta_machine_source_ip = 'curl -vvv -X POST "http://127.0.0.1:3998/tasks/StopStateMachineTask?dbType=SOURCE_IP"'
command_stop_delta_machine_effective_app_db = 'curl -vvv -X POST "http://127.0.0.1:3998/tasks/StopStateMachineTask?dbType=EFFECTIVE_APP_DB"'
command_stop_delta_machine_full_app_db = 'curl -vvv -X POST "http://127.0.0.1:3998/tasks/StopStateMachineTask?dbType=FULL_APP_DB"'
command_start_delta_machine_security_defs = 'curl -vvv -X POST "http://127.0.0.1:3998/tasks/StartStateMachineTask?dbType=SECURITY_DEFINITION"'
command_start_delta_machine_source_ip = 'curl -vvv -X POST "http://127.0.0.1:3998/tasks/StartStateMachineTask?dbType=SOURCE_IP"'
command_start_delta_machine_effective_app_db = 'curl -vvv -X POST "http://127.0.0.1:3998/tasks/StartStateMachineTask?dbType=EFFECTIVE_APP_DB"'
command_start_delta_machine_full_app_db = 'curl -vvv -X POST "http://127.0.0.1:3998/tasks/StartStateMachineTask?dbType=FULL_APP_DB"'

# TODO: Add /opt/filterservice/etc/berkeleyDb to finalrelease
command_compress_current_export = 'cd /opt/filterservice/etc/; tar -cvzf %s deltaBerkeleyDb berkeleyDb' % (filename)
command_sync_to_s3_bucket = 's3cmd put %s %s/%s' % (filename, s3_bucket, filename)

def remove_old_backups():

    for key in mybucket.list():
        mtime = time.mktime(time.strptime(key.last_modified.split(".")[0], "%Y-%m-%dT%H:%M:%S"))
        now = time.time()
        if (now - 2592000) > mtime:
            print "Would delete %s as its older than 1 month (date: %s)" % (key.name,key.last_modified)

remove_old_backups()

command_output = subprocess.check_output('service filterservice status', shell=True)
if 'not' in command_output:
    print "filterservice is not running"

else:
    print "filterservice is running"

    command_output = subprocess.check_output(command_stop_delta_machine_security_defs, shell=True)
    print "Stopping SECURITY_DEFINITION...\n", command_output
    time.sleep(2)

    command_output = subprocess.check_output(command_stop_delta_machine_source_ip, shell=True)
    print "Stopping SOURCE_IP...\n", command_output
    time.sleep(2)

    command_output = subprocess.check_output(command_stop_delta_machine_effective_app_db, shell=True)
    print "Stopping EFFECTIVE_APP_DB...\n", command_output
    time.sleep(2)

    command_output = subprocess.check_output(command_stop_delta_machine_full_app_db, shell=True)
    print "Stopping EFFECTIVE_APP_DB...\n", command_output
    time.sleep(10)

    command_output = subprocess.check_output(command_stop_filterservice, shell=True)
    print "Stopping filterservice instance...\n", command_output
    time.sleep(10)

    try:
        print "Archiving...\n",
        command_output = subprocess.check_output(command_compress_current_export, shell=True)
        time.sleep(5)
    except Exception as e:
        print "Failed at Archiving with ", e

    try:
        print "Syncing to s3 bucket...\n", command_sync_to_s3_bucket
        command_output = subprocess.check_output(command_sync_to_s3_bucket, shell=True)
        # print "s3cmd put...\n", command_output
        # time.sleep(1)
        # message = "Latest export has been backed up to: %s/%s" % (s3_bucket, filename)
        # msg = MIMEText(message)
        # msg['Subject'] = 'BDB backup has been created: %s' % (filename)
        # msg['From'] = 'root@bak-001-cops.eu-west-1b.ie.snappli.net'
        # msg['To'] = 'ops-notify@wandera.com'
        # s = smtplib.SMTP('localhost')
        # s.sendmail('root@bak-001-cops.eu-west-1b.ie.snappli.net', 'ops-notify@wandera.com', msg.as_string())
        # s.quit()
    except Exception as e:
        print "Failed at syncing to s3bucket", e

    try:
        command_output = subprocess.check_output(command_start_filterservice, shell=True)
        print "Starting filterservice instance...\n", command_output
        time.sleep(1)
    except Exception as e:
        print "Failed at starting filterservice", e
