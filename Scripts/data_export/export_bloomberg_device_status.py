import csv
# import sys, getopt
# import psycopg2
import datetime
from pymongo import MongoClient, MongoReplicaSetClient

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

#connecting to collections
device_collection = db.userDevice
#device_list = device_collection.find({"customerId":"71761e13-53f0-4f73-a6f6-34050db68e93","deleted":False})

customer_collection = db.customer
customer_list = customer_collection.find({"name":{"$regex":"McKinsey"}})

# Write to CSV
csv_out = open('export_bloomberg_device_status.csv', 'wb')
writer = csv.writer(csv_out)

for customer in customer_list:

	print "Exporting for:", customer['name']
	device_list = device_collection.find({"customerId":customer['_id'],"deleted":False})

	for device in device_list:

		csv_to_write = customer['name'],device['user']['email'],device['user']['name'],device['status']
		print "Adding row: ",csv_to_write
		writer.writerow(csv_to_write)

# Close communication with the Redshift
#cur.close()
#conn.close()

csv_out.close()
