import pymongo
import csv
import collections
# import os, sys, getopt
#import psycopg2
import datetime
from datetime import date, timedelta
from pymongo import MongoClient, MongoReplicaSetClient


# PROD mongo Mongo connecting to mongo
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

# db.userDevice.find({'_id':'customerId', 'proxy':'proxy_name'})

customer_collection = db.customer
device_collection = db.userDevice
node_collection = db.nodes
customer_list_sorted = []
current_date = datetime.date.today()

csv_input = open('ey.csv' , 'rb')
csv_file_name = 'output_list_csv_%s.csv' %(current_date)
csv_out = open(csv_file_name, 'wb')
writer = csv.writer(csv_out)
header = 'guid','Email'
writer.writerow(header)

for i in csv_input:

    print i

    device = device_collection.find({"_id":"%s"} %(str(i))

    line_to_write = device['_id']
    print line_to_write
    writer.writerow(line_to_write)
