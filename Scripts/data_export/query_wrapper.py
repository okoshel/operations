######################################################################################################################################################
#
# Find multiple devices hiding under one guid
#
######################################################################################################################################################

import csv
import pymongo
from pymongo import MongoClient, MongoReplicaSetClient
import json
import re
import time

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
live = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
live.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu', 'm31ngr03rD4t3n00')
db_main = live.wandera_live_ent_eu

status_db = MongoReplicaSetClient('mdb-101-cops.eu-west-1b.ie.wandera.net:37000,mdb-201-cops.eu-west-1c.ie.wandera.net:37000', replicaSet='ops')
status_db.admin.authenticate('tools', 'f29t0GlaMvoa92g0jKcl')
db_status = status_db.main

#connecting to collections
status_update_logs = db_status.service_events
device_list = db_main.userDevice
customer_list = db_main.customer

# global vars
duplicates = 0
duplicates_list =""
processed_customers = 1
total_customers = customer_list.find({}).count()
current_time_in_ms = int(time.time())*1000
current_time_in_ms_minus_24h = current_time_in_ms - 86400000
current_time_in_ms_minus_48h = current_time_in_ms - 172800000


query = '{ "name": {"$regex" : ^[ARUP UK 2|Vitol]} }'
customers = customer_list.find(query)

customers_id = ['']
for i in customers:
    # customers_id.append(i['_id'])
    print i['_id']
# print customers_id

# for i in customer_list:
#
# query =' {
#     "$and" : [
#         {"activated": false},
#         {"status": "INACTIVE"},
#         {"connectorState": "MANAGED"},
#         {"joinDate": { "$gte" : 1450346400000 }},
#         {"joinDate": { "$lte" : 1450371600000 }},
#         {"customerId": { "$in" : [ ] }}
#     ]
# }'

# total_devices = device_list.find(query)

# print "Found %s duplicates" %(duplicates)
print "Affectes devices count: %s. Total customers processed: %s" % (duplicates_list, processed_customers)


csv_out.close()
