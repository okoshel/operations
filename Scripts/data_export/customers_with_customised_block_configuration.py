import pymongo
import csv
import collections

import datetime
from datetime import date, timedelta
from pymongo import MongoClient, MongoReplicaSetClient


# PROD mongo Mongo connecting to mongo
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu


customer_collection = db.customer
customerBlockConfiguration_collection = db.customerBlockConfiguration

csv_file_name = 'customers_with_customised_block_configuration.csv'
csv_out = open(csv_file_name, 'wb')
writer = csv.writer(csv_out)
# header = 'Id','apn','carrier','Country','mcc','mnc'
# writer.writerow(header)

customer_list =  customerBlockConfiguration_collection.find()

# _id, apn, carrier, isoCountry, mcc, mnc

for customer in customer_list:

    # print "Customer: %s" %(customer['_id'])

    customerName = customer_collection.find({"_id":customer['_id']})

    for i in customerName:

        print " %s - %s" % (customer['_id'], i['name'])
    
    line_to_write = customer['_id'], i['name'].encode('utf8')
    writer.writerow(line_to_write)
