import pymongo
import csv
import collections
import datetime
from datetime import date, timedelta
from pymongo import MongoClient, MongoReplicaSetClient


# PROD mongo Mongo connecting to mongo
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

db.userDevice.find({'_id':'customerId', 'proxy':'proxy_name'})

customer_collection = db.customer
device_collection = db.userDevice
node_collection = db.nodes
customer_list_sorted = []
current_date = datetime.date.today()

csv_file_name = 'devices_with_v3_%s.csv' %(current_date)
csv_out = open(csv_file_name, 'wb')
writer = csv.writer(csv_out)
header = 'Customer','IOS','ANDROID','EMMC ENABLED','RESELLER'
writer.writerow(header)

customer_list = customer_collection.find({})
# devices = device_collection.find({{ "info.app.version":{"$regex":"^3.*.*"}, "deleted":false }})

for i in device_collection.find({ "info.app.version":{"$regex":"^3.*.*"}, "activated":True, "deleted": False }).distinct("customerId"):

    customer = customer_collection.find({"_id":i})
    for edit in customer:
        customer_name = edit['name']
        customer_reseller = edit['resellerName']

        # print i
        if edit['contract']['purchasedComponents']['connector'] == True:
            emmc_enabled = "True"
        else:
            emmc_enabled = "False"

    ios_device_count = device_collection.find({ "info.app.version":{"$regex":"^3.*.*"},"hardwareSpec.osType":"IOS", "customerId":i, "deleted": False, "activated":True }).count()
    android_device_count = device_collection.find({ "info.app.version":{"$regex":"^3.*.*"},"hardwareSpec.osType":"ANDROID", "customerId":i, "deleted": False, "activated":True }).count()

    print "Customer name: %s \t iOS devices: %s \t Android Devices: %s \t EMMC enabled: %s Reseller: %s " % (customer_name, ios_device_count, android_device_count, emmc_enabled, customer_reseller)
    line_to_write = customer_name.encode("utf-8"), ios_device_count, android_device_count, emmc_enabled, customer_reseller
    writer.writerow(line_to_write)
