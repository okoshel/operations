import csv
# import sys, getopt
# import psycopg2
import datetime
from pymongo import MongoClient, MongoReplicaSetClient

# Export all Brunwick devices since 1 Dec 2016


# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

device_collection = db.userDevice

customer_collection = db.customer
customer_list = customer_collection.find({"name":{"$regex":"Brunswick"}})

# Write to CSV
csv_out = open('export_brunswick_devices.csv', 'wb')
writer = csv.writer(csv_out)
header = 'Portal','Email','Name','Join date'
writer.writerow(header)

for customer in customer_list:

    print "Exporting for:", customer['name']
    device_list = device_collection.find({"customerId":customer['_id'], "joinDate":{"$gte":long(1448928000000)} })

    for device in device_list:

		device_joindate = datetime.datetime.fromtimestamp(device['joinDate']/1000).strftime('%Y-%m-%d %H:%m')

		csv_to_write = customer['name'], device['user']['email'], device['user']['name'].encode('utf-8'), device_joindate
		print "Adding row: ",csv_to_write
		writer.writerow(csv_to_write)

csv_out.close()
