import re
import json
import pymongo
from pymongo import MongoClient, MongoReplicaSetClient

# PROD mongo Mongo connecting to mongo
# client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
# client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu', 'm31ngr03rD4t3n00')
# db = client.wandera_live_ent_eu

client = MongoClient('localhost')
db = client.customer_live_restore_2017_02_02

# connecting to collections
customer_collection = db.customer
customer_list = customer_collection.find({}).sort([("name", pymongo.ASCENDING )]).limit(10)

# d0cbd778-40c2-4219-8ab4-982fa1b85bc2 Irish Prison Service IE [{u'category': u'SECURITY', u'recipients': []}, {u'category': u'MOBILE_DATA', u'recipients': []}, {u'category': u'SERVICE_MANAGEMENT', u'recipients': []}]


for customer in customer_list:

    customer_name = customer['name'].encode('utf-8')
    customer_id = customer['_id'].encode('utf-8')

    for i in customer['settings']['notificationSettings']['categories']:
        
        category_security =  json.dumps(i['category']) +  json.dumps(i['recipients'])
        print "Would update following query:"
        print "%s; %s; %s" %(customer_name, customer_id , category_security) 

        print 'db.customer.update({ "name":"%s" }, $set: { "settings.notificationSettings.categories": {} })' %(customer_name)

