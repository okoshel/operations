# Installed on rpt-001
import slackweb
from pymongo import MongoClient, MongoReplicaSetClient


# STGN:
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
# client.snappli_stgn_eu_2.authenticate('db_snappli_stgn_eu_2','Sn4pD4t42')
# db = client.snappli_stgn_eu_2

# PROD
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

# https://hooks.slack.com/services/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
slack = slackweb.Slack(url="https://hooks.slack.com/services/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")

hardware_collection = db.hardwareDetails

aggregate_query = [{"$match": {"confirmed": False }},{"$group" : {"_id":{ "id":"$_id","version":"$version","platform":"$platform","version":"$version","osType":"$osType","supported":"$supported"},"total" :{ "$sum" : 1 }} }]

# Actually query: 
result = hardware_collection.aggregate(aggregate_query)["result"]

for i in result:
    message = "Supported, but unconfirmed device has been found. Details: %s (%s) - %s. You can add it here: https://opscenter.wandera.net/hardware" %(i['_id']['id'],i['_id']['platform'], i['_id']['osType'], )
    print message
    slack.notify(text=message, channel="#alerts-warning", username="Unconfirmed hardware bot", icon_emoji=":sushi:")   