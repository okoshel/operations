import pymongo
import csv
import collections
import os, sys, getopt
import psycopg2
import time
import datetime
from datetime import date, timedelta
from pymongo import MongoClient, MongoReplicaSetClient

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

# Connect to an PROD Redshift
conn = psycopg2.connect(host="redshift-secondary-live-eu-west-1c.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com", database="loggingdata", user="logging_data", password="5hefAduchadrEzu", port="5439")

# Open a cursor to perform database operations
cur = conn.cursor()

# Dates definitions used by script
current_date = datetime.date.today()
report_start_date = date.today() - timedelta(days=7)
report_end_date = current_date

customer_collection = db.customer
# customer_list = customer_collection.find({})
device_collection = db.userDevice

occurence = 0
cached_guid = ''

# CSV processing
csv_out = open('tethering_per_device.csv', 'wb')
writer = csv.writer(csv_out)
csv_header = 'Email address','Number of occurrences', 'Date of occurrence ',
writer.writerow(csv_header)

customer_list = customer_collection.find({"name":"IMG Worldwide"})
for customer in customer_list:

    query = "select deviceguid,createdutcms from notifications_log where customerid = '%s' and createdutcms >= 1445098614078 group by deviceguid,createdutcms order by deviceguid asc limit 5000" % (customer['_id'])
    print "DEBUG: Query: ", query
    cur.execute(query)
    data_query = cur.fetchall()

    for row in data_query:
        # print "Searching for guid: ", row[0]
        if cached_guid == row[0]:
            occurence += 1
        else:
            cached_guid = row[0]
            occurence = 1
        print "cached_guid", cached_guid
        userdevice_object = device_collection.find({"_id":row[0]})
        for i in userdevice_object:
            user_email = i['user']['email']
        event_time = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(row[1]/1000.0))

        print "Email: %s, Date: %s, Occurences: %s" % (user_email, event_time, occurence)

        line_to_write = user_email,occurence,event_time
        writer.writerow(line_to_write)
# Close communication with db
cur.close()
conn.close()

csv_out.close()
