import pymongo
import sys, getopt
import datetime
from pymongo import MongoClient, MongoReplicaSetClient

# PROD mongo Mongo connecting to mongo
client = MongoClient('localhost')
# client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
#client.wandera_live_ent_eu.authenticate('','')
db = client.cmdb
properties_collection = db.properties

enviroment = 'default'
service = ''
input_filename = 'file.properties'
versions = ''

options, remainder = getopt.getopt(sys.argv[1:], 'i:s:e:v:', ['input=','service=','enviroment=','versions'])

for opt, arg in options:
   if opt in ('-i', '--input'):
      input_filename = arg
   elif opt in ('-s', '--service'):
      service = arg
   elif opt in ('-e', '--enviroment'):
      enviroment = arg
   elif opt in ('-v', '--versions'):
      versions = arg

print 'Enviroment :', enviroment
print 'Service :', service
print 'Input file :', input_filename
print 'Service version: ', versions

myprops = {}
with open(input_filename, 'r') as f:
    for line in f:
        line = line.rstrip() #removes trailing whitespace and '\n' chars

        if "=" not in line: continue #skips blanks and comments w/o =
        # if line.startswith("#"): continue #skips comments which contain =

        k, v = line.split("=", 1)
        myprops[k] = v

        post = {
          "enviroment":enviroment,
          "service": service,
          "key": k,
          "value": v,
          "serviceVersion": versions,
          "comment":"",
          "deleted": False,
          "destinationFile":""
          }

        result = properties_collection.save(post)
        print "adding: ", post
        print "entry added",result
