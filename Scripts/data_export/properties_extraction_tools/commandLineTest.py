import getopt
import sys

enviroment = 'default'
service = ''
input_filename = 'file.properties'


options, remainder = getopt.getopt(sys.argv[1:], 'i:s:e:', ['input=','service=','enviroment=',])


for opt, arg in options:
   if opt in ('-i', '--input'):
      input_filename = arg
   elif opt in ('-s', '--service'):
      service = arg
   elif opt in ('-e', '--enviroment'):
      enviroment = arg

print 'Enviroment   :', enviroment
print 'Service   :', service
print 'Input file    :', input_filename
#print 'REMAINING :', remainder