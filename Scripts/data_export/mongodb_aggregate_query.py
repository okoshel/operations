#
# Test your crazy aggregations here! 
#

from datetime import datetime, timedelta
from pymongo import MongoClient, MongoReplicaSetClient

# STGN:
client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client.snappli_stgn_eu_2.authenticate('db_snappli_stgn_eu_2','Sn4pD4t42')
db = client.snappli_stgn_eu_2

# PROD
# client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
# client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
# db = client.wandera_live_ent_eu


hardware_collection = db.hardwareDetails
userdevice_collection = db.userDevice
customer_collection = db.customer


cutoff_3days = datetime.now() - timedelta(days=3)
cutoff_30days = datetime.now() - timedelta(days=30)


# aggregate_query = [
#     {"$project": 
#         {
#             "customerId":1
# 		    # "enrolled": {"$cond": [{"$eq": ["$deleted", False]}, 1, 0]},
# 		    # "activated": {"$cond": [{"$and": [{"$eq": ["$activated", True]}, {"$eq": ["$deleted", False]}]}, 1, 0]},
#             # "last_traffic": {
#             #             "$cond": [{
#             #                 "$and": [
#             #                     {"$gte": [{"$ifNull": ["$traffic.lastTrafficRun", 0]}, cutoff_3days]},
#             #                     {"$eq": ["$activated", True]},
#             #                     {"$eq": ["$deleted", False]}
#             #                 ]
#             #             }, 1, 0]},
#             # "last_traffic_30": {
#             #             "$cond": [{
#             #                 "$and": [
#             #                     {"$gte": [{"$ifNull": ["$traffic.lastTrafficRun", 0]}, cutoff_30days]},
#             #                     {"$eq": ["$activated", True]}
#             #                 ]
#             #             }, 1, 0]}            
#         }
#     },
#     {"$group" : 
#         # {"_id":
#             {   
#             "_id": "$customerId"
#             # "Enrolled_Devices__c": {"$sum": '$enrolled'},
#             # "Activated_devices__c": {"$sum": '$activated'},
#             # "Activated_devices__c": {"$sum": '$activated'},
#             # "Active_devices__c": {"$sum": '$last_traffic'},
#             # "RADAR_Active_Devices_Past_30_Days__c": {"$sum": '$last_traffic_30'}
#             }
#         # }
#     }
# ]

aggregate_query = [
    {"$project": 
        {
            "customerId":  {"$cond": [{"$eq": ["$customerId", '81a7bca0-212f-440a-9c8d-ad7711a0c014']}, 1, 0]},
            "proxyname.hostWithoutPortPrefix": 1
		    # "enrolled": {"$cond": [{"$eq": ["$deleted", False]}, 1, 0]},
		    # "activated": {"$cond": [{"$and": [{"$eq": ["$activated", True]}, {"$eq": ["$deleted", False]}]}, 1, 0]},
            # "last_traffic": {
            #             "$cond": [{
            #                 "$and": [
            #                     {"$gte": [{"$ifNull": ["$traffic.lastTrafficRun", 0]}, cutoff_3days]},
            #                     {"$eq": ["$activated", True]},
            #                     {"$eq": ["$deleted", False]}
            #                 ]
            #             }, 1, 0]},
            # "last_traffic_30": {
            #             "$cond": [{
            #                 "$and": [
            #                     {"$gte": [{"$ifNull": ["$traffic.lastTrafficRun", 0]}, cutoff_30days]},
            #                     {"$eq": ["$activated", True]}
            #                 ]
            #             }, 1, 0]}            
        }
    },
    {"$group" : 
        # {"_id":
            {   
            "_id": "$customerId",
            "proxyname.hostWithoutPortPrefix": "$proxyname.hostWithoutPortPrefix"
            # "Enrolled_Devices__c": {"$sum": '$enrolled'},
            # "Activated_devices__c": {"$sum": '$activated'},
            # "Activated_devices__c": {"$sum": '$activated'},
            # "Active_devices__c": {"$sum": '$last_traffic'},
            # "RADAR_Active_Devices_Past_30_Days__c": {"$sum": '$last_traffic_30'}
            }
        # }
    }
]



# Actually query: 
result = userdevice_collection.aggregate(aggregate_query)["result"]

for i in result:
   print i['_id']
   print customer_collection.find_one({"_id":i['_id']},{"_id":1})
