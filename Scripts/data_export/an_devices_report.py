import pymongo
import csv
import collections
import datetime
from datetime import date, timedelta
from pymongo import MongoClient, MongoReplicaSetClient


# PROD mongo Mongo connecting to mongo
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

# db.userDevice.find({'_id':'customerId', 'proxy':'proxy_name'})

customer_collection = db.customer
device_collection = db.userDevice
node_collection = db.nodes
customer_list_sorted = []
current_date = datetime.date.today()

csv_file_name = 'an_devices_report_%s.csv' %(current_date)
csv_out = open(csv_file_name, 'wb')
writer = csv.writer(csv_out)
header = 'Customer','Email','App version','Platform'
writer.writerow(header)

# customer_name = "Rackspace"
reseller_name = "Alternative"

customer_list = customer_collection.find({"resellerId":"42dd8c3b-737e-474c-85b9-41a05bb89d68" })

for customer in customer_list:
    customerId = customer['_id']
    # print "Customer Id: %s Customer Name %s" % (customerId, customer['name'])

    for i in device_collection.find({"deleted": False, "customerId":customerId }):

        try:
            app_version = i['info']['app']['version']
        except Exception as e:
            app_version = 'Unknown'


        print "Customer: %s \t email: %s \t App version: %s, platform: %s " % (customer['name'], i['user']['email'], app_version, i['hardwareSpec']['osType'])
        line_to_write = customer['name'], i['user']['email'], app_version, i['hardwareSpec']['osType']
        writer.writerow(line_to_write)
