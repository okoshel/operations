import csv
# import datetime
import pymongo
from pymongo import MongoClient, MongoReplicaSetClient
import json

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu', 'm31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

#connecting to collections
device_collection = db.userDevice
customer_collection = db.customer
connector_configs = db.connectorConfigs
customer_list = customer_collection.find({"contract.purchasedComponents.connector":True})

# global vars
connector_settings = []
groupsCount = 0
mdmConnectorName = ''
lastMdmCheckInUtcMs = 0
devicesPerGroupString = ''
# csv_out = open('customers_with_emmc.csv', 'wb')
# writer = csv.writer(csv_out)

for customer in customer_list:

    # find at least one device with lastStatusUpdateUtcMs within last 24h
    if device_collection.find_one({"customerId":customer['_id'],"statusUpdate.connector.lastStatusUpdateUtcMs":{"$gte":1449571630025}}):
        short_id = customer['_id']
        short_id = short_id[:4]

        # print "Found customer with lastStatusUpdateUtcMs >= than 24h, Customer ID: ", customer['_id'], "Customer name: ", customer['name']

        # count how many groups per customer from connectorConfigs collection
        try:
            connector_settings = connector_configs.find_one({"customerId":customer['_id']})
            mdmConnectorName = connector_settings['mdmConnector']
            for i in connector_settings['groups']:
                print "Group number:", i
                # Count how many devices in this group
                devicesPerGroup = 0
                devicesPerGroup = device_collection.find({"customerId":customer['_id'],"statusUpdate.connector.payload.groups":i}).count()
                if devicesPerGroup >= 0:
                    groupsCount=groupsCount+1
                    # stringToappend = "%s(%s)" % (i, devicesPerGroup)
                    # devicesPerGroupString = devicesPerGroupString.join(stringToappend)
                    print "Group %s has %s devices: " % (i, devicesPerGroup)
                else:
                    # devicesPerGroupString = devicesPerGroupString.append(i,'0')
                    stringToappend = "%s(0)" % (i)
                    devicesPerGroupString = devicesPerGroupString.join(stringToappend)
                    print "Group %s has 0 devices: " % (i)

        except Exception as e:
            print "Exception in groups count: ", e
            groupsCount = 0

        # Found latest lastMdmCheckInUtcMs per group from statusUpdate.connector.payload.lastMdmCheckInUtcMs
        # lastSuccessfulEmmSync = device_collection.find({"customerId":customer['_id'], "statusUpdate.connector.lastStatusUpdateUtcMs":{"$gte":long(1449571630025)}}).sort([("statusUpdate.connector.lastMdmCheckInUtcMs", pymongo.ASCENDING )]).limit( 1 )
        lastSuccessfulEmmSync = device_collection.find({"customerId":customer['_id'], "statusUpdate.connector.lastStatusUpdateUtcMs":{"$gte":1449571630025}}).limit( 1 )

        # print "lastSuccessfulEmmSync:", lastSuccessfulEmmSync
        for groupObject in lastSuccessfulEmmSync:
            try:
                # print "ID: ", i['_id'], "Email: ", i['user']['email']
                # print "lastMdmCheckInUtcMs: ", groupObject['statusUpdate']['connector']['lastStatusUpdateUtcMs']
                lastMdmCheckInUtcMs = groupObject['statusUpdate']['connector']['lastStatusUpdateUtcMs']

            except Exception as e:
                print "Exception: ", e

        print "Name:", customer['name'],"ID:",customer['_id'], "Country: ", customer['settings']['isoCountry'], "Groups: ", groupsCount, "MDM Name: ", mdmConnectorName, "lastMdmCheckInUtcMs: ", lastMdmCheckInUtcMs
        groupsCount = 0

        # csv_to_write = customer['_id'],
        # print "Adding row: ",csv_to_write
        # writer.writerow(csv_to_write)

# csv_out.close()
