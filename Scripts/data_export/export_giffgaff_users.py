import pymongo
import csv
import sys, getopt
import psycopg2
import datetime
from pymongo import MongoClient, MongoReplicaSetClient

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

# Connect to an PROD Redshift
# conn = psycopg2.connect(host="redshift-secondary-live-eu-west-1c.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com", database="loggingdata", user="logging_data", password="5hefAduchadrEzu", port="5439")

# Open a cursor to perform database operations
# cur = conn.cursor()

#connecting to collections
device_collection = db.userDevice
device_list = device_collection.find({ "deleted":False, "info.carrier.carrierName" : "giffgaff" })

# Write to CSV
csv_out = open('export_giffgaff_users.csv', 'wb')
writer = csv.writer(csv_out)


for device in device_list:

	# lastModifiedTime = ((device['profile']['lastModifiedTime'])/1000).strftime('%Y-%m-%d %H:%m')
	# 
	try:
		lastprofileaccesstime = datetime.datetime.fromtimestamp(device['profile']['lastModifiedTime']/1000).strftime('%Y-%m-%d %H:%m')
	except:
		lastprofileaccesstime = datetime.datetime.fromtimestamp(device['lastModifiedTime']/1000).strftime('%Y-%m-%d %H:%m')
		
	csv_to_write = device['user']['email'],device['status'],lastprofileaccesstime
	print "Adding row: ",csv_to_write	
	writer.writerow(csv_to_write)

# Close communication with the Redshift
#cur.close()
#conn.close()

csv_out.close()