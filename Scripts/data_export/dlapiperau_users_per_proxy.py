import csv
from pymongo import MongoClient, MongoReplicaSetClient

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

#connecting to collections
device_collection = db.userDevice

customer_collection = db.customer
customer_list = customer_collection.find({"name":{"$regex":"DLA Piper LLP AU"}})

# Write to CSV
csv_out = open('dlapiperau_users_per_proxy.csv', 'wb')
writer = csv.writer(csv_out)

header = 'Email','Name'

for customer in customer_list:

	print "Exporting for:", customer['name']
	device_list = device_collection.find({"customerId":customer['_id'],"deleted":False, "proxy.hostWithoutPortPrefix":"a91b1bc4e52d4138.proxy.wandera.com", "traffic.lastTrafficRun":{"$gte":long(1461687257630)}})

	for device in device_list:

		csv_to_write = device['user']['email'],device['user']['name']
		print "Adding row: ",csv_to_write
		writer.writerow(csv_to_write)

csv_out.close()
