import pymongo
import csv
import sys, getopt
import psycopg2
import datetime
from pymongo import MongoClient, MongoReplicaSetClient

# client = MongoReplicaSetClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004', replicaSet='rs20004')
# client.snappli_stgn_eu_2.authenticate('db_snappli_stgn_eu_2','Sn4pD4t42')
# db = client.snappli_stgn_eu_2

client = MongoReplicaSetClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20002,mdb-201-dev.eu-west-1b.ie.wandera.biz:20002', replicaSet='rs20002')
client.snappli_dev_eu_2.authenticate('wandera_dev','t#cG#TqYo0NLoSZ')
db = client.snappli_dev_eu_2


#connecting to collections
device_collection = db.userDevice

customer_collection = db.customer
customer_list = customer_collection.find()

# Write to CSV
csv_out = open('customers_and_zendeskid.csv', 'wb')
writer = csv.writer(csv_out)

for customer in customer_list:

  print "Exporting for:", customer['name']


  try:
    customer_name = customer['name'].encode('utf-8')

  except:
    customer_name = ''


  try:
    cutomerzendeskid = customer['zenDeskOrgId']
  except:
    cutomerzendeskid = ''

  csv_to_write = customer_name,cutomerzendeskid
  print "Adding row: ",csv_to_write
  writer.writerow(csv_to_write)

# Close communication with the Redshift
#cur.close()
#conn.close()

csv_out.close()