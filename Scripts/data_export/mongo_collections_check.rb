class AWSIgnoredRoamingUpdates < Scout::Plugin
  # need the ruby mongo gem
	needs 'rubygems'
	needs 'mongo'

  def build_report
    report = {}
    begin
      mongo_uri = 'mongodb://db_snappli_live_ent_eu:m31ngr03rD4t3n00@mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000/wandera_live_ent_eu'

  	  connection = Mongo::Connection.from_uri(mongo_uri)
  	  db = connection.db('wandera_live_ent_eu')
  	  collection = db['connectorConfigs']
      count = collection.find({ "customerId" => {"$in" =>["fb4567b6-4ee2-4c4c-abb9-6c78ec463b25","b651f2a9-9e5f-4b54-a87a-830ee1290296"]}, "groups" => {"$size" => 0} } ).count
  	  report(:customers_with_no_groups => count)

      rescue Mongo::OperationFailure => e
        return errors << {:subject => "Unable to connect to MongoDB.",
                         :body => "Scout was unable to connect to the MongoDB server: \n\n#{e}\n\n#{e.backtrace}"}
    end
    report(report) if report.values.compact.any?
  end
end
