import pymongo
import csv
import collections
# import os, sys, getopt
#import psycopg2
import datetime
from datetime import date, timedelta
from pymongo import MongoClient, MongoReplicaSetClient


# PROD mongo Mongo connecting to mongo
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

# db.userDevice.find({'_id':'customerId', 'proxy':'proxy_name'})

customer_collection = db.customer
device_collection = db.userDevice
node_collection = db.nodes
customer_list_sorted = []
current_date = datetime.date.today()

csv_file_name = 'samsung_devices_v6.x.x_%s.csv' %(current_date)
csv_out = open(csv_file_name, 'wb')
writer = csv.writer(csv_out)
header = 'Email','App version'
writer.writerow(header)

customer_name = "Rackspace"

customer_list = customer_collection.find().sort([("name", pymongo.ASCENDING )])

for customer in customer_list:
    customerId = customer['_id']
    print "Customer Id: %s Customer Name %s" % (customerId, customer['name'])

    for i in device_collection.find({ "info.app.version":{"$regex":"^6.0.1"}, "activated":True, "deleted": False, "customerId":customer['_id'], "hardwareSpec.osType":"ANDROID" }):

        print "email: %s \t App version: %s " % (i['user']['email'], i['info']['app']['version'])
        line_to_write = i['user']['email'], i['info']['app']['version']
        writer.writerow(line_to_write)
