import csv
# import datetime
import pymongo
from pymongo import MongoClient, MongoReplicaSetClient
import json

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu', 'm31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

#connecting to collections
device_collection = db.userDevice
customer_collection = db.customer
connector_configs = db.connectorConfigs
customer_list = customer_collection.find({"contract.purchasedComponents.connector":True})

# global vars

mdmConnectorName = ''

csv_out = open('customers_with_emmc_and_no_name.csv', 'wb')
writer = csv.writer(csv_out)


for customer in customer_list:

    mdmConnectorName = ''
    connector_settings = connector_configs.find({"customerId":customer['_id']})
    for i in connector_settings:
        mdmConnectorName = i['mdmConnector']

    device_list = device_collection.find({"customerId":customer['_id'], "statusUpdate.connector.lastStatusUpdateUtcMs": {"$gte": 1450483200000}, "statusUpdate.connector.payload.firstName": {"$exists": False}}).limit(1)

    try:
        for device in device_list:
            print "Name:", customer['name'], "ID:",customer['_id'], "MDM Name:", mdmConnectorName , "External Id", device['externalId']
            csv_to_write = customer['name'], customer['_id'], mdmConnectorName, device['externalId']
            # print "Adding row: ",csv_to_write
            writer.writerow(csv_to_write)
    except Exception as e:
        print "Nothing found for customer: ", customer['_id']



csv_out.close()
