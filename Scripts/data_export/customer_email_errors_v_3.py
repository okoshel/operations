import pymongo
import csv
import collections
import os, sys, getopt
import psycopg2
import datetime
from datetime import date, timedelta
# import dns.resolver
from pymongo import MongoClient, MongoReplicaSetClient
from collections import OrderedDict
# Import smtplib for the actual sending function
import smtplib
# Import the email modules we'll need
from email.mime.text import MIMEText

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

# Connect to an PROD Redshift
conn = psycopg2.connect(host="redshift-secondary-live-eu-west-1c.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com", database="loggingdata", user="logging_data", password="5hefAduchadrEzu", port="5439")

# Open a cursor to perform database operations
cur = conn.cursor()

# Dates definitions used by script
current_date = datetime.date.today()
report_start_date = date.today() - timedelta(days=7)
report_end_date = current_date

print "DEBUG: Report start date: ", report_start_date, "Report end date:", report_end_date

# print "DEBUG: Current date", current_date
csv_file_name = 'customer_connections_report_%s.csv' %(current_date)
print "DEBUG: File name", csv_file_name
csv_out = open(csv_file_name, 'wb')
writer = csv.writer(csv_out)
emptyline = '','',''

customer_collection = db.customer
# customer_list = customer_collection.find({ "name":{"$regex":"Allen & Overy"}})
customer_list = customer_collection.find({})
device_collection = db.userDevice

customer_list_sorted = []

def get_top_customers():
    i = 0
    customer_device = {}
    for customer in customer_list:
        device_count = device_collection.find({"customerId":customer['_id'],"deleted":False}).count()
#    print customer['name'], customer['_id'], device_count
        customer_device[customer['_id']] = device_count
    sorted_by_value = OrderedDict(sorted(customer_device.items(),reverse=True, key=lambda x: x[1]))

    for k, v in sorted_by_value.items():
#    print "%s: %s: %s" % (i, k, v)
        i = i + 1
        customer_list_sorted.append(k)
        if i > 49:
            break
    return customer_list_sorted

customer_list_sorted_mongo = customer_collection.find({ "_id":{"$in":get_top_customers()}})

# sql query
excluded_sites = "'facebook','twitter','youtube','google','apple','spotify','dropbox','googleapis','akamaihd','icloud','yahoo','weather','itunes','doubleclick','linkedin','office365','fbcdn','gstatic','wandera'"

# Main loop
for customer in customer_list_sorted_mongo:

    print "DEBUG: Exporting for:", customer['name']
    writer.writerow(emptyline)
    writer.writerow(emptyline)
    csv_to_write = customer['name'],'destinationaddress','error type','connections count'
    writer.writerow(csv_to_write)

    query = "select distinct(request), destinationaddress, proxyerrors, rule, category, count(*) from proxy_logs where customerId='%s' AND site not in (%s) AND proxyerrors != 'null' AND dateadd(ms,startutcinms,'1970-1-1') >= '%s' AND dateadd(ms,startutcinms,'1970-1-1') < '%s' group by request,proxyerrors,rule,destinationaddress, category order by count(*) desc Limit 20;" % (customer['_id'], excluded_sites, report_start_date, report_end_date)
    print "DEBUG: Query: ", query

    cur.execute(query)
    first_passed_data_query = cur.fetchall()

    for row in first_passed_data_query:
        print row[0], row[1], row[2], row[5]
        csv_to_write = row[0], row[1],row[2], row[5]
        #print "Adding row: ",csv_to_write
        writer.writerow(csv_to_write)

# Close communication with db
cur.close()
conn.close()

csv_out.close()

# put to s3
print "DEBUG: Exporting to s3 bucket..."
s3cmd_command = 's3cmd put %s s3://backup.wandera.net/tmp/connections/' % csv_file_name
os.system(s3cmd_command)

csv_link = 'https://s3-eu-west-1.amazonaws.com/backup.wandera.net/tmp/connections/%s' % csv_file_name
print csv_link

email_data= 'Connections report for top 50 customer has been exported. Period: %s to %s. Download it from: %s' % (report_start_date,report_end_date,csv_link)


print email_data
msg = MIMEText(email_data)
msg['Subject'] = 'Connection stats for top 50 customers (%s - %s)' % (report_start_date, report_end_date)
msg['From'] = 'root@bak-001-cops.eu-west-1b.ie.snappli.net'
msg['To'] = 'artur.gut@wandera.com'

s = smtplib.SMTP('localhost')
s.sendmail('root@bak-001-cops.eu-west-1b.ie.snappli.net', 'artur.gut@wandera.com', msg.as_string())
s.quit()
