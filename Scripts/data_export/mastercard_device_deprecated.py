import pymongo
import csv
import datetime
from time import gmtime, strftime
from pymongo import MongoClient, MongoReplicaSetClient

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

#connecting to collections
device_collection = db.userDevice
proxies_collection = db.proxies

current_time = strftime("%Y-%m-%d-%H-%M-%S", gmtime())
filename = "mastercard_deprecated_devices_%s.csv" %(current_time)
csv_out = open(filename, 'wb')
writer = csv.writer(csv_out)
csv_header = 'Email','GUID','UDID','Provisioning Method','Depracated On','Last traffic run', 'Proxyname', 'Proxy Port'
writer.writerow(csv_header)

device_list = device_collection.find({ "customerId":"b651f2a9-9e5f-4b54-a87a-830ee1290296","connectorState":"MANAGED", "hardwareSpec.osType":"IOS", "proxy.deprecated":False}).limit(5000)

for device in device_list:
	
    proxies_list = proxies_collection.find({"guid":device['_id'], "deprecatedOn": {"$gt":1478710591241}})
    print "Checking device:", device['_id']
    
    for proxy in proxies_list:
        line = device['user']['email'], device['_id'], device['externalId'], device['info']['provisioningMethod'], datetime.datetime.fromtimestamp(proxy['deprecatedOn']/1000.0), datetime.datetime.fromtimestamp(device['traffic']['lastTrafficRun']/1000.0), device['proxy']['host'], device['proxy']['port']
        print "Entry", line 
        writer.writerow(line)

csv_out.close()

# info.provisioningMethod