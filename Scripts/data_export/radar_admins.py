######################################################################################################################################################
#
# Export all Radar admins
#
######################################################################################################################################################
import csv
import pymongo
from pymongo import MongoClient, MongoReplicaSetClient
import json
import re
import time

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
live = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
# live.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu', 'm31ngr03rD4t3n00')
live.wandera_live_ent_eu.authenticate('tools', 'dk92kFpca1iguG0a2kFFac0',source='admin')

db_main = live.wandera_live_ent_eu
customer_list = db_main.customer
admin_list = db_main.admin

db_auth = live.wandera_live_ent_eu_auth
admin_auth_list = db_auth.admin

# global vars
duplicates = 0
duplicates_list =""
admin_name = ''
admin_role = ''
admin_sfid = ''
sfid = ''
admin_real_name = ''
customer_name = ''

# CSV processing
csv_out = open('all_radar_admins.csv', 'wb')
writer = csv.writer(csv_out)
csv_header = 'Admin email','Admin name','Admin type','SFID','Customer name'
writer.writerow(csv_header)

for admin in admin_list.find():
    for i in admin_auth_list.find({"email":admin['securityDetails']['email']}):
        admin_real_name = i['name']
        # print "Admin name is:", admin_real_name

    for i in customer_list.find({"_id": admin['securityDetails']['customerId']}):
        customer_name = i['name']
        sfid = i['salesForceId']

    try:
        if admin['securityDetails']['keyType'] == 'PARENT_ADMIN':
            print "Customer: %s Admin name: %s Admin Email: %s, Admin role: %s,  SFID: %s " % (customer_name, admin_real_name, admin['securityDetails']['email'],admin['securityDetails']['keyType'], sfid)
            line_to_write = admin['securityDetails']['email'], admin_real_name, admin['securityDetails']['keyType'], sfid, customer_name
            writer.writerow(line_to_write)

        elif admin['securityDetails']['keyType'] == 'RESELLER_ADMIN':
            print "Customer: %s Admin name: %s Admin Email: %s, Admin role: %s,  SFID: %s " % (customer_name, admin_real_name, admin['securityDetails']['email'],admin['securityDetails']['keyType'], sfid)
            line_to_write = admin['securityDetails']['email'], admin_real_name, admin['securityDetails']['keyType'], sfid
            writer.writerow(line_to_write)

        else:
            print "Customer: %s Admin name: %s Admin Email: %s, Admin role: %s,  SFID: %s " % (customer_name, admin_real_name, admin['securityDetails']['email'],admin['securityDetails']['keyType'], sfid)
            line_to_write = admin['securityDetails']['email'], admin_real_name, admin['securityDetails']['keyType'], sfid, customer_name
            writer.writerow(line_to_write)

    except Exception as e:
        print "Exception!", e

csv_out.close()
