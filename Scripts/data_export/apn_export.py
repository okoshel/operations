import pymongo
import csv
import collections

import datetime
from datetime import date, timedelta
from pymongo import MongoClient, MongoReplicaSetClient


# PROD mongo Mongo connecting to mongo
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu


apns_collection = db.apns

csv_file_name = 'apns_list.csv'
csv_out = open(csv_file_name, 'wb')
writer = csv.writer(csv_out)
header = 'Id','apn','carrier','Country','mcc','mnc'
writer.writerow(header)

apns_list =  apns_collection.find()

# _id, apn, carrier, isoCountry, mcc, mnc

for apn in apns_list:

    print "Processing: %s" %(apn['_id'])

# Can't think of a better way of handling unexcepted fields so will stick to try/except and encode('utf8')...

    try:
        apnName = apn['apn'].encode('utf8')
    except:
        apnName = ' '

    try:
        isoCountry = apn['isoCountry'].encode('utf8')
    except:
        isoCountry = ' '

    try:
        apnCarrier = apn['carrier'].encode('utf8')
    except:
        apnCarrier = ' '

    try:
        apnMcc = apn['mcc'].encode('utf8')
    except:
        apnMcc = ' '

    try:
        apnMnc = apn['mnc'].encode('utf8')
    except:
        apnMnc = ' '
        

    print "%s %s %s %s %s %s " % (apn['_id'], apnName, apnCarrier, isoCountry, apnMcc, apnMnc)
    
    line_to_write = apn['_id'], apnName, apnCarrier, isoCountry, apnMcc, apnMnc
    writer.writerow(line_to_write)
