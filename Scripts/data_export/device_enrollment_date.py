import pymongo
import csv
import time
import collections
import datetime
from datetime import date, timedelta
from pymongo import MongoClient, MongoReplicaSetClient


# PROD mongo Mongo connecting to mongo
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

db.userDevice.find({'_id':'customerId', 'proxy':'proxy_name'})

customer_collection = db.customer
device_collection = db.userDevice
node_collection = db.nodes
customer_list_sorted = []
current_date = datetime.date.today()

csv_file_name = 'device_enrollment_date_%s.csv' %(current_date)
csv_out = open(csv_file_name, 'wb')
writer = csv.writer(csv_out)
header = 'Email','GUID','Join date UTC','Profile Last Modified time'
writer.writerow(header)

# current_time_in_ms = int(time.time())*1000

customer_list = customer_collection.find({})

for i in device_collection.find({ "customerId":"44b9bcf8-3426-4564-a2eb-99d74777a48d", "deleted":False , "joinDate": {"$lte":1469641172000} }):

    joinDateConverted = datetime.datetime.fromtimestamp(i["joinDate"]/1000)
    profileModifiedTime = datetime.datetime.fromtimestamp(i["profile"]["lastModifiedTime"]/1000)
    # print i["user"]["email"],joinDateConverted
    
    
    line_to_write = i["user"]["email"],i["_id"],joinDateConverted, profileModifiedTime
    print line_to_write
    writer.writerow(line_to_write)
