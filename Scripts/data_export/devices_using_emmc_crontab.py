######################################################################################################################################################
# Request: https://snappli.atlassian.net/browse/OPS-1300
# The Business needs to know the number of devices that use EMMC.
# Therefore I need a report that has the following info:
# SFID, Customer name, Number of EMMC Linked Device, Number of Pending devices, Number of Invited devices,
# I will likely need a similar report every month until we have this info fed into SF, therefore would it be possible to have the script saved for re-use?
######################################################################################################################################################

import csv
import json
import smtplib
# from datetime import timedelta, datetime, tzinfo
from time import gmtime, strftime
import pymongo
from pymongo import MongoClient, MongoReplicaSetClient
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu', 'm31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

#connecting to collections
device_collection = db.userDevice
customer_collection = db.customer
connector_configs = db.connectorConfigs
customer_list = customer_collection.find({"contract.purchasedComponents.connector":True}).sort("name",pymongo.ASCENDING)

# global vars
connector_settings = []
groupsCount = 0
mdmConnectorName = ''
lastMdmCheckInUtcMs = 0
devicesPerGroupString = ''
current_time = strftime("%Y-%m-%d-%H-%M-%S", gmtime())
current_date = strftime("%Y-%m-%d", gmtime())
filename = "devices_using_emmc_stats_%s.csv" %(current_time)
csv_out = open(filename, 'wb')
writer = csv.writer(csv_out)
csv_header = 'SFID','Name','Number of EMMC Linked Device','Linked and Activated','MDM'
message = "Please find attached the csv export."
writer.writerow(csv_header)
# SFID, Customer name, Number of EMMC Linked Device, Number of Pending devices, Number of Invited devices,

for customer in customer_list:

    sfid = customer['salesForceId']
    name = customer['name']
    name = name.encode("utf-8")

    number_of_emmc_devices = device_collection.find({"customerId":customer['_id'], "connectorState":"MANAGED", "deleted":False}).count()
    number_pending_devices = device_collection.find({"customerId":customer['_id'], "connectorState":"MANAGED","status":"ACTIVE", "deleted":False}).count()
    for i in connector_configs.find({"customerId":customer['_id']}):
        mdmConnectorName = i['mdmConnector']

    csv_to_write = sfid, name, number_of_emmc_devices, number_pending_devices, mdmConnectorName
    print "Adding row: ",csv_to_write
    writer.writerow(csv_to_write)

msg = MIMEMultipart('alternative')


f = file(filename)

attachment = MIMEText(f.read())
attachment.add_header('Content-Disposition', 'attachment', filename=filename)

part1 = MIMEText(message, 'plain')

msg.attach(part1)
msg.attach(attachment)

print msg

# msg['Subject'] = 'Devices using emmc - %s' %(current_date)
# msg['From'] = 'root@bak-001-cops.eu-west-1b.ie.snappli.net'
# msg['To'] = 'ruwan.ellewala@wandera.com'
#
# #recipients = ['ops-notify@wandera.com', 'ruwan.ellewala@wandera.com', 'mark.flanders@wandera.com','matthew.barnard@wandera.com']
# recipients = ['artur.gut@wandera.com']
#
# s = smtplib.SMTP('localhost')
# s.sendmail('root@bak-001-cops.eu-west-1b.ie.snappli.net', recipients, msg.as_string())
# s.quit()
# csv_out.close()
