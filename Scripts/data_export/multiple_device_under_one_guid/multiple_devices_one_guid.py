######################################################################################################################################################
#
# Find multiple devices hiding under one guid
#
######################################################################################################################################################

import csv
import pymongo
from pymongo import MongoClient, MongoReplicaSetClient
import json
import re
import time

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
live = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
live.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu', 'm31ngr03rD4t3n00')
db_main = live.wandera_live_ent_eu

status_db = MongoReplicaSetClient('mdb-101-cops.eu-west-1b.ie.wandera.net:37000,mdb-201-cops.eu-west-1c.ie.wandera.net:37000', replicaSet='ops')
status_db.admin.authenticate('tools', 'f29t0GlaMvoa92g0jKcl')
db_status = status_db.main

#connecting to collections
status_update_logs = db_status.service_events
device_list = db_main.userDevice
customer_list = db_main.customer

# global vars
duplicates = 0
duplicates_list =""
processed_customers = 1
total_customers = customer_list.find({}).count()
current_time_in_ms = int(time.time())*1000
current_time_in_ms_minus_24h = current_time_in_ms - 86400000
current_time_in_ms_minus_48h = current_time_in_ms - 172800000

# CSV processing
csv_out = open('multiple_devices_under_one_guid.csv', 'wb')
writer = csv.writer(csv_out)
csv_header = 'Customer Name','GUID','Email','Device Name(s)','Device system version','hardware platform'
writer.writerow(csv_header)

for customer in customer_list.find().batch_size(5):
# for customer in customer_list.find({"name":{"$regex":"McKinsey"}}):
    try:
        print "Processing customer %s of %s. Customer name: %s" %(processed_customers,total_customers,customer['name'])
        processed_customers += 1
        for device in device_list.find({"customerId":customer['_id'], "deleted":False, "statusUpdate.device.lastStatusUpdateUtcMs": {"$gte":current_time_in_ms_minus_24h}}).batch_size(50):
            # print "Searching for duplicates for guid: ", device['_id']
            devices_behind_guid = status_update_logs.find({"guid":device['_id'],"timestamp":{"$gte":current_time_in_ms_minus_24h},"eventType": "DEVICE_STATUS_UPDATE"}).distinct("parameters.payload.apnsToken")

            # devices_behind_guid = status_update_logs.find({"guid":device['_id'],"timestamp":{"$gte":1450114037361},"eventType": "DEVICE_STATUS_UPDATE"}).count()
            # print "Elements in a list: ", len(devices_behind_guid)
            if len(devices_behind_guid) >= 2:

                print devices_behind_guid
                device_names = status_update_logs.find({"guid":device['_id'],"timestamp":{"$gte":current_time_in_ms_minus_24h},"eventType": "DEVICE_STATUS_UPDATE"}).distinct("parameters.payload.device.deviceName")
                devices = ', '.join(device_names)
                device_versions = status_update_logs.find({"guid":device['_id'],"timestamp":{"$gte":current_time_in_ms_minus_24h},"eventType": "DEVICE_STATUS_UPDATE"}).distinct("parameters.payload.device.deviceSystemVersion")
                versions = ', '.join(device_versions)
                hardware_paltform = status_update_logs.find({"guid":device['_id'],"timestamp":{"$gte":current_time_in_ms_minus_24h},"eventType": "DEVICE_STATUS_UPDATE"}).distinct("parameters.payload.device.hwPlatform")
                platform = ', '.join(hardware_paltform)

                print "Device names", device_names
                print "Affected guid", device['_id']

                duplicates_list += "Name: %s GUID: %s Email: %s Device Name: %s Device System Version: %s Platform %s \n" %( customer['name'], device['_id'], device['user']['email'], devices, versions, platform)
                duplicates += 1
                print "Found %s duplicates" %(duplicates)
                line_to_write = customer['name'], device['_id'], device['user']['email'], devices, versions, platform
                writer.writerow(line_to_write)


    except Exception as e:
        print "Exception!", e


# print "Found %s duplicates" %(duplicates)
print "Affectes devices count: %s. Total customers processed: %s" % (duplicates_list, processed_customers)


csv_out.close()
