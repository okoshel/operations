import re
import pymongo
from pymongo import MongoClient, MongoReplicaSetClient

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu', 'm31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

# connecting to collections
customer_collection = db.customer
customer_list = customer_collection.find({'name': re.compile('Vitol|McKinsey|United Airlines')})


for customer in customer_list:

   print customer['_id'],  customer['name']
