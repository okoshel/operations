import csv
# import datetime
import pymongo
from pymongo import MongoClient, MongoReplicaSetClient
import json
import re
# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu', 'm31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

#connecting to collections
device_collection = db.userDevice
# customer_collection = db.customer
# connector_configs = db.connectorConfigs
# customer_list = customer_collection.find()

# global vars
connector_settings = []
groupsCount = 0
mdmConnectorName = ''
lastMdmCheckInUtcMs = 0
devicesPerGroupString = ''
device_processed = 0

csv_out = open('santander_emm_missing_devices.csv', 'wb')
writer = csv.writer(csv_out)


f = open('AirWatch_Devices-Table_1.csv')
csv_f = csv.reader(f)

for line in csv_f:
    in_wandera = line[0]
    last_seen = line[1]
    name = line[3]
    username = line[4]
    first_name = line[5]
    last_name = line[6]
    email = line[6]
    os = line[7]
    model = line[8]
    enrollment = line[9]
    udid = line[10]
    og = line[11]
    # print in_wandera, last_seen, name, username, first_name, last_name, email, os, model, enrollment, udid, og

    udid_lower = udid.lower()
    udid_upper = udid.upper()

    query = "%s" % (udid_upper)

    # print "Query:", query

    result = device_collection.find({"externalId": query}).count()

    if result == 0:
        print "Not found with uppercase: ", udid_upper, "looking for lowercase.."

        query = "%s" % (udid_lower)
        if device_collection.find({"externalId": query}).count() == 0:
            print "External ID not in db, writing to csv"
            line_to_write = in_wandera,last_seen,name,username,first_name,last_name,email,os,model,enrollment,udid,og
            print "Adding row: ",line_to_write
            writer.writerow(line_to_write)

        else:
            print "Device found in db. Nothing to do!"

csv_out.close()
