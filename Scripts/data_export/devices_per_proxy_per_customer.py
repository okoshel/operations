import pymongo
import csv
import collections
import os, sys, getopt
#import psycopg2
import datetime
from datetime import date, timedelta
from pymongo import MongoClient, MongoReplicaSetClient
from collections import OrderedDict
#import smtplib
#from email.mime.text import MIMEText
import graphitesend

# PROD mongo Mongo connecting to mongo
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

# Script explained
# 1. Find top 50 customers with get_top_customers()
# 2. For each customer, find proxies in proxies filed
# 3. Fore each field in proxies find customer name and proxy name in userDevice collection like: db.userDevice.find({'_id':'customerId', 'proxy':'proxy_name'})

customer_collection = db.customer
# customer_list = customer_collection.find({ "name":{"$regex":"Bloomberg"}})
customer_list = customer_collection.find({})
device_collection = db.userDevice
node_collection = db.nodes

customer_list_sorted = []

# Find top customers by number of devices
def get_top_customers():
  i = 0
  customer_device = {}
  for customer in customer_list:
    device_count = device_collection.find({"customerId":customer['_id'],"deleted":False}).count()
#    print customer['name'], customer['_id'], device_count
    customer_device[customer['_id']] = device_count
  sorted_by_value = OrderedDict(sorted(customer_device.items(),reverse=True, key=lambda x: x[1]))

  for k, v in sorted_by_value.items():
#    print "%s: %s: %s" % (i, k, v)
    i = i + 1
    customer_list_sorted.append(k)
    if i > 49:
      break
  return customer_list_sorted

# function to reverse '.' separeted string:
def reverse_dns(node_name):
  node_name = node_name.split('.')
  node_name.reverse()
  node_name = '.'.join(node_name)
  return node_name

customer_list_sorted_mongo = customer_collection.find({ "_id":{"$in":get_top_customers()}})

#graphite_host = "graphite.wandera.net"
graphite_host = "52.16.153.45"
graphite_port = 2003

# Main block
for customer in customer_list_sorted_mongo:

  proxies = customer['proxies']

  for entries in proxies:
    devices_per_proxy = device_collection.find({"proxy.hostWithoutPortPrefix":entries,"customerId":customer['_id'] }).count()
    node = node_collection.find_one({"proxyName":entries})
    node_name = node['nodeName']
    node_name_reversed = reverse_dns(node_name)
    customer_name = customer['name'].replace('.','')

    graphite_prefix = node_name_reversed+".proxy.stats.customer."+customer_name
    print graphite_prefix, devices_per_proxy

#    g = graphitesend.init(dryrun=True)
    g = graphitesend.init(prefix=graphite_prefix, graphite_server=graphite_host, graphite_port=graphite_port, system_name="")
    g.send_dict({ "devices" : int(devices_per_proxy) })
