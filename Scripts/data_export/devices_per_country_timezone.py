import pymongo
import csv
import sys, getopt
#import psycopg2
import datetime
from pymongo import MongoClient, MongoReplicaSetClient

# PROD mongo Mongo connecting to mongo
# client = MongoClient('mdb-101-dev.eu-west-1a.ie.wandera.biz:20004,mdb-201-dev.eu-west-1b.ie.wandera.biz:20004',replicaSet='rs20004')
client = MongoReplicaSetClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

# Connect to an PROD Redshift
# conn = psycopg2.connect(host="redshift-secondary-live-eu-west-1c.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com", database="loggingdata", user="logging_data", password="5hefAduchadrEzu", port="5439")

# Open a cursor to perform database operations
# cur = conn.cursor()

#connecting to collections
device_collection = db.userDevice
country_codes = device_collection.distinct({"info.carrier.isoCountryCode"})
#device_list = device_collection.find({"customerId":"71761e13-53f0-4f73-a6f6-34050db68e93","deleted":False})
print country_codes
#customer_collection = db.customer
#customer_list = customer_collection.find({"name":{"$regex":"McKinsey"}})

# Write to text file
# f = open('device_list.txt', 'w')

# Write to CSV
# csv_out = open('devices_per_country_timezone.csv', 'wb')
# writer = csv.writer(csv_out)
# header = 'Country code', 'Device number'
# #writer.writerow(header)
# emptyline = '','','','','',''


# for customer in customer_list:

#   print "Exporting for:", customer['name']
#   device_list = device_collection.find({"customerId":customer['_id'],"deleted":False, "activated":True, "activation.firstActivationTime":{"$gt":1420070400000}})

#   writer.writerow(emptyline)
#   writer.writerow(header)

#   # Username, Email Address, Join Date, Activation Date, Device Status, RADAR portal.

#   for device in device_list:

#     device_joindate = datetime.datetime.fromtimestamp(device['joinDate']/1000).strftime('%Y-%m-%d %H:%m')

#     try:
#       device_activation = datetime.datetime.fromtimestamp(device['activation']['firstActivationTime']/1000).strftime('%Y-%m-%d %H:%m')
#     except:
#       device_activation = 'Never'

#     try:
#       name = device['user']['name']
#       name = name.encode('ascii',errors='ignore')
#     except:
#       name = "wrong format"

#     csv_to_write = customer['name'],device['user']['email'], name, device_joindate, device_activation, device['status']
#     print "Adding row: ",csv_to_write
#     writer.writerow(csv_to_write)


# # Close communication with the Redshift
# #cur.close()
# #conn.close()

# csv_out.close()