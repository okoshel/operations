#!/usr/bin/php
<?PHP
// Version: 0.1
// This script munges the MongoDB RabbIT access logs into a squid style log.
// This is required in order to run them through Web Polygraph.
// The MongoDB export should be a standard export without and -f specifications for fields.

$filename = "f:";
$filename = getopt($filename);

if (isset($filename["f"])) {
    echo "Processing: $filename[f]\n";
} else {
	echo "You must specify a file to use this script.\n";
	echo "To do this use the -f option. json_to_squid.php -f logs.json\n";
	exit(1);
}

$lines = file("$filename[f]");
foreach ($lines as $line)
{
    $obj = json_decode($line);
	$request=preg_replace("/(HTTP\/1.1)/i" , "" , $obj->{'request'});
	print $obj->{'startUtcInMs'}." ".$obj->{'duration'}." ".$obj->{'address'}." TCP_MISS/".$obj->{'status'}." ".$obj->{'contentLength'}." ".$request."- NONE/- ".$obj->{'type'};

    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            //echo ' - No errors';
        break;
        case JSON_ERROR_DEPTH:
            echo ' - Maximum stack depth exceeded';
        break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Underflow or the modes mismatch';
        break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Unexpected control character found';
        break;
        case JSON_ERROR_SYNTAX:
            echo ' - Syntax error, malformed JSON';
        break;
        case JSON_ERROR_UTF8:
            echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
        break;
        default:
            echo ' - Unknown error';
        break;
    }

    echo PHP_EOL;
}

?>
