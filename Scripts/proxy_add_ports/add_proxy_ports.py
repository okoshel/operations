#!/usr/bin/env python
###################################################################################
#
# Add additional ports to a specified proxy node
#
###################################################################################

import sys
import uuid
import time
import pymongo
import pprint
from pymongo import MongoClient, MongoReplicaSetClient
from pymongo import ReturnDocument
from optparse import OptionParser

db_user = ''
db_pwd = ''

# Production Mongo connecting
mongo_cnx_url = "mongodb://{}:{}@mdb-101-core.eu-west-1a.ie.wandera.com,mdb-201-core.eu-west-1b.ie.wandera.com:22000/?replicaSet=rs22000".format(db_user,db_pwd)
production = MongoClient(mongo_cnx_url)
db_main = production.wandera_live_ent_eu

#connecting to collections
proxies = db_main.proxies
nodes = db_main.nodes

def UpdateNodeDetails(nodename,number):
    """ Update node document with the changes we want to make
        :param: nodename, number
        :returns: success or failure from modifications
    """
    try:
        result = nodes.find_one_and_update({ "nodeName": nodename},{'$inc': {'capacity': number}},return_document=ReturnDocument.AFTER)
    except Exception as e:
        sys.stderr.write("Not able to add document to MongoDB: %s" % e)
        sys.exit(1)
    return result

def GetNodeDetails(nodename):
    """ Perform Mongo query for nodename
        :param: nodename
        :returns: node document from Mongo
    """
    result = nodes.find_one({ "nodeName": nodename})
    return result

def GetProxyDetails(proxyname):
    """ Perform Mongo query for proxyname
        :param: proxyname
        :returns: proxy document from Mongo
    """
    result = proxies.find({ "host": {'$regex': proxyname}}).sort("_id",pymongo.DESCENDING).limit(1)
    return result[0]

def GetProxyCount(proxyname):
    """ Perform Mongo query for proxyname
        :param: proxyname
        :returns: proxy document from Mongo
    """
    result = proxies.find({ "host": {'$regex': proxyname}}).count()

    print "Count of proxies" + str(result)
    return result

def AddProxyToMongo(proxyDocument):
    """ Perform Mongo query for proxyname
        :param: proxyname
        :returns: proxy document from Mongo
    """
    try:
	result = proxies.insert_one(proxyDocument)
    except Exception as e:
        sys.stderr.write("Not able to update document in MongoDB: %s" % e)
        sys.exit(1)
    return result

def GenerateProxyNodes(proxyname,number,capacity,mccList,supportedProfileTypes,dataCenter,verbose,dryrun):
    """ Perform Mongo query for proxyname
        :param: proxyname
        :returns: proxy document from Mongo
    """
    capacity = int(capacity)
    result = {}
    for n in range(number):
      available = True
      millis = int(round(time.time() * 1000))
      randomUuid = str(uuid.uuid3(uuid.NAMESPACE_DNS, str(n + 1 + millis))).replace("-", "")[0:16]
      host = randomUuid +'.'+ proxyname
      roamingHost = randomUuid +'.r.'+ proxyname
      roamingHttpHost = 'h.' + roamingHost
      roamingVpnConcentrator = 'v.' + roamingHost
      # callbackUrl is no longer used disable
      #callbackUrl = "http://callback."+ proxyname +":1666/ProxyCallback?guid=%s"
      port = n + 1 + capacity
      mcc = mccList
      order = millis
      supportedProfileTypes = supportedProfileTypes
      deprecated = False
      dataCenter = dataCenter

      #proxyDocument = { "available": available, "callbackUrl": str(callbackUrl), "host": str(host), "mcc": mcc, "mccsToBeRemoved": [ ], "order": millis, "port": str(port), "supportedProfileTypes": supportedProfileTypes, "deprecated": deprecated, "dataCenter": str(dataCenter),"roamingHttpHost": roamingHttpHost,"roamingVpnConcentrator": roamingVpnConcentrator }
      proxyDocument = { "available": available, "host": str(host), "mcc": mcc, "mccsToBeRemoved": [ ], "order": millis, "port": str(port), "supportedProfileTypes": supportedProfileTypes, "deprecated": deprecated, "dataCenter": str(dataCenter),"roamingHttpHost": roamingHttpHost,"roamingVpnConcentrator": roamingVpnConcentrator }

      if verbose:
        print n
        print n + 1
        print host
        print roamingHost
        print roamingHttpHost
        print roamingVpnConcentrator
        #print callbackUrl
        print port
        print mcc
        print order
        print supportedProfileTypes
        print dataCenter
        pprint.pprint(proxyDocument)

      if dryrun != True:
        inserted = AddProxyToMongo(proxyDocument)

    result['capacity'] = int(port)

    return result


def main():
    usage = "usage: %prog [options]"
    parser = OptionParser(usage)
    parser.add_option("-p", "--proxy",
                      dest="proxyname",
                      type="string",
                      help="Specify a Proxy Name which we should add additional ports to")
    parser.add_option("-n", "--node",
                      dest="nodename",
                      type="string",
                      help="Specify a Node Name which we should add additional ports to")
    parser.add_option("-a", "--add",
                      dest="add",
                      type="int",
                      help="Specify the number of ports we should add to the Proxy specified in proxyname")
    parser.add_option("-g", "--generate",
                      dest="generate",
                      action="store_true",
                      help="Generate the required number of additional ports for the proxy node.")
    parser.add_option("-d", "--dry-run",
                      dest="dryrun",
                      action="store_true",
                      help="Ensures no actual changes are performed on the database. Simulates process.")
    parser.add_option("-v", "--verbose",
                      action="store_true",
                      dest="verbose")
    parser.add_option("-q", "--quiet",
                      action="store_false",
                      dest="verbose")
    (options, args) = parser.parse_args()

    # Print some details from the options if verbose is enabled
    if options.verbose:
      print options
      print args

    if options.add is not None and options.nodename is not None:
        node_details = GetNodeDetails(options.nodename)
        if node_details is None:
          print """This Node Name could not be found in the database.\nPlease check you have specified the correct node name."""
          sys.exit(1)
        else:
          if options.verbose:
            pprint.pprint(node_details)

          if options.dryrun != True:
	    print "This script will run UpdateNodeDetails on proxy %s and add %s ports to %s" % (options.nodename, options.add, node_details['capacity'])
	    confirmed = raw_input("Are you sure you want to perform this action (y/n): ")
            if confirmed == "y":
              update_result = UpdateNodeDetails(options.nodename,options.add)
              if options.verbose:
                pprint.pprint(update_result)
    	      if update_result['capacity'] - node_details['capacity'] == options.add:
                print "Node capacity increased by %s.\nNew node capacity is %s" % (options.add, update_result['capacity'])
              else:
                print "Something went wrong Node capacity not increased correctly"
            else:
              print "This action will not be performed."
          else:
            print "This script would run UpdateNodeDetails on node %s and add %s ports to %s" % (options.nodename, options.add, node_details['capacity'])

    if options.add is not None and options.proxyname is not None:
        proxy_details = GetProxyDetails(options.proxyname)
        if proxy_details is None:
          print """This Proxy Name could not be found in the database.\nPlease check you have specified the correct proxy name."""
          sys.exit(1)
        else:
          if options.verbose:
           pprint.pprint(proxy_details)

          if options.dryrun != True:
	    print "This script will run GenerateProxyNodes on proxy %s and generate %s proxies in additon to %s" % (options.proxyname, options.add, proxy_details['port'])
	    confirmed = raw_input("Are you sure you want to perform this action (y/n): ")
            if confirmed == "y":
              update_result = GenerateProxyNodes(options.proxyname,options.add,proxy_details['port'],proxy_details['mcc'],proxy_details['supportedProfileTypes'],proxy_details['dataCenter'],options.verbose,options.dryrun)
              if options.verbose:
                pprint.pprint(update_result)
    	      if int(update_result['capacity']) - int(proxy_details['port']) == options.add:
                print "Proxy capacity increased by %s.\nNew proxy capacity is %s" % (options.add, update_result['capacity'])
              else:
                print "Something went wrong Proxy capacity not increased correctly"
            else:
              print "This action will not be performed."
          else:
            print "This script would run GenerateProxyNodes on proxy %s and generate %s proxies in additon to %s" % (options.proxyname, options.add, proxy_details['port'])

if __name__ == "__main__":
  main()
