from wandera_client.shell import create_client
from wandera_client.client import ProxyManagerClient, ServiceAuthentication
import os
import sys
import json

try:
    supported_type = sys.argv[1]
    AFTER = int(sys.argv[2])
except:
    sys.exit("USAGE: {} <supported_type> <target_ports>".format(sys.argv[0]))

print "Preparing commands for {} proxies under {} ports".format(supported_type, AFTER)
if raw_input("Proceed? (y/n) ").lower() != 'y':
    sys.exit("aborted by user")

home = os.path.expanduser('~')
CONFIG_FILE = os.path.join(home, ".wandera/woopas.json")

with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

try:
    config_json = json.loads(config)
    pm_url = config_json["woopas"]["proxy_manager_url"]
    pm_api_key = config_json["woopas"]["service_api_key"]
    pm_api_secret = config_json["woopas"]["service_api_secret"]
    pm_auth = ServiceAuthentication(pm_api_key, pm_api_secret)
    pm = ProxyManagerClient(pm_url,auth=pm_auth)
except Exception as e:
    print "Failed to load ProxyManagerClient\n({})".format(e.message)
    sys.exit(1)

ghp_nodes = [n for n in pm.nodes() if supported_type in n.profile_types and n.capacity < AFTER]
idle = [n for n in ghp_nodes if n.state == 'idle']
active = [n for n in ghp_nodes if n.state == 'active']

assert set(ghp_nodes) == set(idle + active)

for node in idle:
    print './add_proxy_ports.py -n {0} -a {1} -v'.format(node.server.name,
                                                         AFTER - node.capacity)

for node in active:
    print './add_proxy_ports.py -n {0} -p {1} -a {2} -v'.format(node.server.name,
                                                                node.server.dns_names[0],
                                                                AFTER - node.capacity)

print "\nFOLLOWING PROXIES WILL NOT BE MODIFIED\n"

ghp_nodes = [n for n in pm.nodes() if supported_type in n.profile_types and n.capacity >= AFTER]
idle = [n for n in ghp_nodes if n.state == 'idle']
active = [n for n in ghp_nodes if n.state == 'active']

assert set(ghp_nodes) == set(idle + active)

for node in idle:
    print '{0} (idle) {1} ports'.format(node.server.name,node.capacity)

for node in active:
    print '{0} ({1}) {2} ports'.format(node.server.name,node.server.dns_names[0],node.capacity)
