#!/bin/bash

function print_help {
    echo "USAGE: `basename ${BASH_SOURCE}` REPO BUNDLE BUCKET REMOTE_FILE"
    echo ""
    echo "Where:"
    echo "  REPO is the path to the git repo you wish to bundle (backup)"
    echo "  BUNDLE is what you want to call the bundle"
    echo "  BUCKET is the name of the bucket on S3 into which you want to put the BUNDLE, if this doesnt exist it will be created for you"
    echo "  REMOTE_FILE is the name you want to use for the BUNDLE in the BUCKET. WARNING: If REMOTE_FILE already exists it will be overwritten"
}

function is_s3cmd_installed {
    which s3cmd > /dev/null
    return $?
}

function is_md5sum_installed {
    which md5sum > /dev/null
    return $?
}

function bundle_repo {
    local repo_name=${1}
    local bundle_name=${2}
    local return_code=1
    if [ -d ${repo_name} ] ; then
      local currentdir=$PWD
      cd ${repo_name}
        if git bundle create ${currentdir}/${bundle_name} --all > /dev/null 2>&1 ; then
            return_code=0
        fi
        cd ${currentdir}
    else
        echo "Could not find repo ${repo_name}"
    fi
    return $return_code
}

function verfiy_bundle {
    local bundle=${1}
    if git bundle verify ${bundle} > /dev/null 2>&1; then
        return 0
    else
        return 1
    fi
}

function does_bucket_exist {
    local bucket="${1}"
    local bucket_list=`s3cmd ls`
    if echo ${bucket_list} | grep -q "${bucket}" ; then
        return 0
    else
        return 1
    fi
}

function create_bucket {
    local bucket="${1}"
    if s3cmd mb s3://"${bucket}" ; then
        return 0
    else
        return 1
    fi
}

function retrieve_bundle_from_s3 {
    local bundle="${1}"
    local bucket="${2}"
    local local_file="${3}"
    local return_code=1
    if does_bucket_exist ${bucket} ; then
        local cmd="s3cmd get s3://${bucket}/${bundle} ${local_file}"
        if $cmd ; then
            return_code=0
        fi
    else
        echo "Bucket ${bucket} does not exist, cannot retrieve from it"
    fi
    return ${return_code}
}

function send_bundle_to_s3 {
    local bundle="${1}"
    local bucket="${2}"
    local remote_file="${3}"
    local return_code=1
    local cmd="s3cmd put ${bundle} s3://${bucket}/${remote_file}"
    if ${cmd} ; then
        return_code=0
    fi
    return $return_code
}

if [ $# -ne 4 ] ; then
    print_help
    exit 1
fi

repo_name="${1}"
bundle_name="${2}"
bucket_name="${3}"
remote_filename="${4}"
exit_code=1
if ! is_s3cmd_installed ; then
    echo "s3cmd is not on the path, please install it."
    exit 1
fi
if ! is_md5sum_installed ; then
    echo "md5sum is not on the path, please install it."
    exit 1
fi

if bundle_repo ${repo_name} ${bundle_name} ; then
    echo "Bundled ${repo_name} in to ${bundle_name}"
    if verfiy_bundle ${bundle_name} ; then
        echo "Verified bundle is ok"
        bucket_exists=1
        if does_bucket_exist "${bucket_name}" ; then
            bucket_exists=0
        else
            echo "Bucket ${bucket_name} does not exist. Creating it"
            if create_bucket "${bucket_name}" ; then
                bucket_exists=0
            else
                echo "Failed to create ${bucket_name}"
            fi
        fi
        if [ ${bucket_exists} -eq 0 ] ; then
            if send_bundle_to_s3 "${bundle_name}" "${bucket_name}"  "${remote_filename}" ; then
                echo "Bundle sent to S3 bucket ${bucket_name}/${remote_filename}"
                echo "Verifying we can retrieve the bundle unchanged..."
                original_md5=`md5sum ${bundle_name} | awk '{ print $1 }'`
                echo "Retrieving bundle from S3..."
                if retrieve_bundle_from_s3 ${remote_filename} ${bucket_name} ${bundle_name}.s3 ; then
                    retrieved_md5=`md5sum ${bundle_name}.s3 | awk '{ print $1 }'`
                    if [ "${original_md5}" == "${retrieved_md5}" ] ; then
                        echo "Retrieved bundle is correct"
                        exit_code=0
                    else
                        echo "Bundle md5sums do not match after being sent to S3"
                        echo "Bundle original md5sum: ${original_md5}"
                        echo "Retrieved bundle md5sum: ${retrieved_md5}"
                    fi
                else
                    echo "Failed to retrive bundle ${bucket_name} from bucket ${bucket_name}"
                fi
            else
                echo "Failed to send bundle to S3 bucket ${bucket_name}"
            fi
        fi
    else
        echo "Failed to verify bundle"
    fi
else
    echo "Failed to bundle ${repo_name}"
fi
# cleanup
if [ -f ${bundle_name} ] ; then
    rm ${bundle_name}
fi
if [ -f ${bundle_name}.s3 ] ; then
    rm ${bundle_name}.s3
fi

if [ $exit_code -eq 0 ] ; then
    echo "Backup of ${repo_name} successful"
else
    echo "Backup of ${repo_name} failed"
fi
exit $exit_code
