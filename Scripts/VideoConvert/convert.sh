#!/bin/bash
VIDEO_ID=$1
BITRATE=$2
FPS=$3
IP=$4
URL=$5
ID="$VIDEO_ID"

OUTPUT_DIR="/var/www/streaming/$ID"

SQUID_IP_STG=176.34.242.251 
SQUID_IP_LIVE=176.34.151.1

LOG_FILE=/tmp/$ID.out
OUTPUT_FILE_PREFIX="$OUTPUT_DIR/$ID"
INDEX_URL="http://54.247.114.169/streaming/$ID/$ID"
#HTTP_PROXY=http://54.247.101.193:8080/
#HTTP_PROXY=http://31.222.137.50:9666/
#HTTP_PROXY=http://82.28.141.250:9666/

SHOULD_PROXY=`echo $URL | grep $IP`
if [ -z $SHOULD_PROXY ] ; then 
HTTP_PROXY=
else
    if [ "$IP" = "$SQUID_IP_STG" ] || [ "$IP" = "$SQUID_IP_LIVE" ] ; then
    	HTTP_PROXY=http://$IP:8080/
    else
    	HTTP_PROXY=http://$IP:9666/
    fi
fi    
#HTTP_PROXY=

if [ -f "$OUTPUT_FILE_PREFIX.m3u8" ] ; then 
  echo "$INDEX_URL.m3u8"
  exit 0 
fi

if [ ! -d $OUTPUT_DIR ] ; then 
  mkdir $OUTPUT_DIR
fi
pid=`ps -ef | grep -v grep | grep vlc | grep "$URL" | grep "$ID" | awk '{print $2}'`

if [ "$pid" ] ; then
  while [ ! -f "$OUTPUT_FILE_PREFIX.m3u8" ]
  do
    sleep 1
  done
  echo "$INDEX_URL.m3u8"
  exit 0
else 
  #vlc -vvv -I dummy "$URL" vlc://quit --sout="#transcode{width=320,height=240,vcodec=h264,vb=$BITRATE,venc=x264{aud,profile=baseline,level=30,keyint=30,ref=1},acodec=mp4a,ab=96}:std{access=livehttp{seglen=10,delsegs=false,numsegs=0,index=$OUTPUT_FILE_PREFIX.m3u8,index-url=$INDEX_URL-########.ts},mux=ts{use-key-frames},dst=$OUTPUT_FILE_PREFIX-########.ts}" > /dev/null &
  #vlc -vvv -I dummy "$URL" vlc://quit --sout="#transcode{width=320,height=240,vcodec=h264,vb=$BITRATE,venc=x264{aud,profile=baseline,level=30,keyint=30,ref=1},acodec=mp4a,ab=96}:std{access=livehttp{seglen=10,delsegs=false,numsegs=0,index=$OUTPUT_FILE_PREFIX.m3u8,index-url=$INDEX_URL-########.ts},mux=ts{use-key-frames},dst=$OUTPUT_FILE_PREFIX-########.ts}" > /dev/null &
  vlc -I dummy "$URL" --http-proxy="$HTTP_PROXY" vlc://quit --sout="#transcode{width=320,height=240,vcodec=h264,vb=$BITRATE,fps=$FPS,venc=x264{aud,profile=baseline,level=30,keyint=30,ref=1}}:std{access=livehttp{seglen=10,delsegs=false,numsegs=0,index=$OUTPUT_FILE_PREFIX.m3u8,index-url=$INDEX_URL-########.ts},mux=ts{use-key-frames},dst=$OUTPUT_FILE_PREFIX-########.ts}" > /dev/null &
#vlc -vvv -I dummy "$URL" vlc://quit --sout="#transcode{vcodec=h264,venc=x264{aud,profile=baseline,level=30,keyint=30,ref=1}}:std{access=livehttp{seglen=10,delsegs=false,numsegs=0,index=$OUTPUT_FILE_PREFIX.m3u8,index-url=$INDEX_URL-########.ts},mux=ts{use-key-frames},dst=$OUTPUT_FILE_PREFIX-########.ts}"  &
  pid=`ps -ef | grep -v grep | grep vlc | grep "$URL" | grep "$ID" | awk '{print $2}'`
  if [ ! -n "$pid" ] ; then
    echo "ERROR: Transconding was not started"
    exit -1;
  else
    while [ ! -f "$OUTPUT_FILE_PREFIX.m3u8" ] 
    do
      sleep 1
    done 		
    echo "$INDEX_URL.m3u8"
    exit 0
  fi 
fi

