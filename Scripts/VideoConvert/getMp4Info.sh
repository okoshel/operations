#!/bin/bash
ID=$1
URL=$4
QUALITY=$2
IP=$3
OUTPUT_DIR=/tmp
MAX_SLEEP=8;
CAPACITY=2;
VLC=vlc
LOCAL_IP=31.222.137.171
OTHER_SERVER=31.222.137.172

SQUID_IP_STG=176.34.242.251 
SQUID_IP_LIVE=176.34.151.1

SHOULD_PROXY=`echo $URL | grep $IP`
if [ -z $SHOULD_PROXY ] ; then 
HTTP_PROXY=
else
    if [ "$IP" = "$SQUID_IP_STG" ] || [ "$IP" = "$SQUID_IP_LIVE" ] ; then
        HTTP_PROXY=http://$IP:8080/
    else
        HTTP_PROXY=http://$IP:9666/
    fi
fi

num_procs=`ps -ef | grep -v grep | grep vlc | grep -v "access http" | wc -l`
if [ $num_procs -le $CAPACITY ] ; then 
quality_bucket=`awk -v "quality=$QUALITY" 'BEGIN{i = int(4*quality)+1; printf("%d", i)}'`
content_type=`curl -I -L "$URL" | grep "^Content-Type:" | egrep -o "(3gpp|mp4)"`

FILENAME="$ID-$content_type-$quality_bucket"
JSON_FILE="/var/www/$FILENAME.json"
VLC_OUTPUT_FILE="$OUTPUT_DIR/$FILENAME.txt"

if [ -f "$JSON_FILE" ] ; then 
  cat "$JSON_FILE"
  exit 0;
fi 

REMOTE_JSON=`curl "http://$OTHER_SERVER/$FILENAME.json"`
REMOTE_JSON_EXISTS=`echo "$REMOTE_JSON" | grep "videoIp"`

if [ ! -z "$REMOTE_JSON_EXISTS" ]; then
  echo "$REMOTE_JSON"
  exit 0;
fi

if [ ! -d "$OUTPUT_DIR" ] ; then 
  mkdir "$OUTPUT_DIR"
fi
"$VLC" -vvv --access http -I dummy "$URL" vlc://quit --http-proxy="$HTTP_PROXY" --sout="#transcode{width=320,height=240,vcodec=h264,venc=x264{aud,profile=baseline,level=30,keyint=30,ref=1}}:std{access=livehttp{seglen=10,delsegs=false,numsegs=0,index=$OUTPUT_DIR/$FILENAME.m3u8,index-url=http://0.0.0.0/streaming/$FILENAME-########.ts},mux=ts{use-key-frames},dst=$OUTPUT_DIR/$FILENAME-########.ts}" 2> "$VLC_OUTPUT_FILE" &

result=`grep "answer code [23]" "$VLC_OUTPUT_FILE" | awk '{if ($9 > 399) {print $9} else { print "OK" }   }'|sort|uniq ` 
sleepCounter=0;
while [ -z  $result ] && [ $sleepCounter -lt $MAX_SLEEP ] ;  
do 
  let sleepCounter=$sleepCounter+1
  result=`grep "answer code [23]" "$VLC_OUTPUT_FILE" | awk '{if ($9 > 399) {print $9} else { print "OK" }   }'|sort|uniq ` 
  sleep 1
done


if [ $result = "OK" ] ; then 
  size=`grep "source .*destination.*" "$VLC_OUTPUT_FILE" | awk '{print $7}' | sed -e 's/,//g'`
  #streamSize=`grep "stream size.*pos=0\|frame size" "$VLC_OUTPUT_FILE" | awk -F'=' '{print $2}' | sed -e 's/,.*//g'`
  streamSize=`grep -m 1 "frame size" "$VLC_OUTPUT_FILE" | awk -F'=' '{print $2}' | sed -e 's/,.*//g'`
  streamDuration=`grep "\- duration" "$VLC_OUTPUT_FILE" | awk '{print $8/1000}'`
  sleepCounter=0;
  while   [ -z $streamDuration ] && [ $sleepCounter -lt $MAX_SLEEP ] ; 
  do
    let sleepCounter=$sleepCounter+1
    streamDuration=`grep "\- duration" "$VLC_OUTPUT_FILE" | awk '{print $8/1000}'`
    sleep 1
  done 
  if [ ! -z $streamDuration ] ; then 
    bitrate=`awk  "BEGIN { print $streamSize/$streamDuration*8}"`

    mapping_suffix="low"    
    if [ "mp4" = "$content_type" ]; then     
      mapping_suffix=`awk -v "bitrate=$bitrate" 'BEGIN{if ( bitrate > 1000 ) {print "high"} else if ( bitrate > 300 ) {print "medium"} else { print "low" }}'` 
    fi

    new_info=`awk -v "quality=$QUALITY" -v "streamSize=$streamSize" -v "duration=$streamDuration" -F ',' '{i = int(NF*quality)+1; split($i,data,":") ; bitrate=data[1]; newSize=data[2]; printf("\"newbitrate\":%d, \"factor\":%f", bitrate, streamSize/(newSize*duration/1000))}' /opt/video/qualityMapping-"$mapping_suffix".csv `
    json=`echo "{\"videoIp\": \"$LOCAL_IP\", \"size\": \"$size\", \"streamSize\":$streamSize, \"duration\":$streamDuration, \"bitrate\":$bitrate, $new_info}" `
    echo $json > "$JSON_FILE"
    echo $json
  else 
    echo "{\"error\": \"no duration found\"}"
  fi

else
  result=`grep "answer code" "$VLC_OUTPUT_FILE" | awk '{if ($9 > 399) {print $9} else { print "OK" }   }'|sort|uniq `
  echo "{\"error\": \"$result\"}"
fi 

pid=`ps -ef | grep -v grep | grep vlc | grep "$URL" | grep 'access http' | awk '{print $2}'`
if [ -n "$pid" ] ; then
  kill $pid
fi
else 
	echo "{\"error\": \"no_capacity\"}"
fi
