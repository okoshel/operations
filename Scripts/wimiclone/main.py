#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# 

import webapp2 as webapp
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import db

def render_page(proxied, ip, proxy_ip, via, headers):
    body = ""
    if proxied:
        if via == "HTTP/1.1 Wandera":
          body = """<div class="wrapper">
          <img src="http://reset.wandera.com/wandera_logo.png" width="400" height="144" alt="Wandera" />
          <div class=" line proxied">
            <p>Your traffic is being proxied through Wandera</p>
            <p>Your IP: """ + ip + """</p>
            <p>Proxy IP: """ + proxy_ip + """</p>
          </div>
          </div>"""
        else:
          body = """<div class="wrapper">
          <img src="http://reset.wandera.com/wandera_logo.png" width="400" height="144" alt="Wandera" />
          <div class="line not-proxied">
            <p>Your traffic is being proxied through a non Wandera proxy</p>
            <p>Your IP: """ + ip + """</p>
            <p>Proxy: """ + via + """</p>
            <p>Please check you have the Wandera profile installed and are not using WiFi</p>
            <p>Check the <a href="wandera://?action=display&pane=status">Wandera</a> app for your status</p>
          </div>
          </div>"""
    else:
      body = """<div class="wrapper">
      <img src="http://reset.wandera.com/wandera_logo.png" width="400" height="144" alt="Wandera" />
      <div class="not-proxied">
        <p>Traffic not being proxied</p>
        <p>Your IP: """ + ip + """</p>
        <p>Please check you have the Wandera profile installed and are not using WiFi</p>
        <p>Check the <a href="wandera://?action=display&pane=status">Wandera</a> app for your status</p>
      </div>
      </div>"""

    style = ("""
        <style>
         .wrapper {
            margin:0 auto;
            width:100%;
            text-align:center;
            padding:40px 0 30px 0;
         }
         .line {
             border-top:1px solid #EFEFEF;
             padding: 40px 0 0
         }
         .line {
             padding:16px 32px;
             border-radius:4px;
             box-shadow: 0 1px 1px rgba(2,2,2,.42);
             text-decoration: none;
             font-size:40px;
             font-family:"Helvetica Neue", Helvetica, Arial, sans-serif
             background: #95da38;
             color: #fff;
         }
         .not-proxied {
             background: #B61C2B;
             padding:16px 32px;
             color: #fff;
             border-radius:4px;
             text-decoration: none;
             font-size:30px;
             font-family:"Helvetica Neue", Helvetica, Arial, sans-serif
         }
         .proxied {
             background: #95da38;
             color: #fff;
             padding:16px 32px;
             border-radius:4px;
             text-decoration: none;
             font-size:30px;
             font-family:"Helvetica Neue", Helvetica, Arial, sans-serif
         }
         }
         </style>""")
    head = style + "<meta charset=\"UTF-8\"><title>Wandera</title>"
    page = ("""<!doctype html>
    <html>
      <head>""" + head + """
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-38286413-1', 'auto');
                  ga('send', 'pageview');

                  </script>
      </head>
      <body>""" + body + """ """ + str(headers) + """
      </body>
    </html>""")
    return page

class MainPage(webapp.RequestHandler):
    def get(self):

        # obtain ip address
        xff = self.request.headers.get("X-Forwarded-For", None)
        proxy_ip = self.request.remote_addr
        via = self.request.headers.get("Via", "Unknown")
        page = None
        headers = self.request.headers
        headers = ""
        if xff:
          if via != "Unknown":
            page = render_page(True, xff, proxy_ip, via, headers)
          else:
            page = render_page(True, proxy_ip, xff, via, headers)
        else:
          page = render_page(False, proxy_ip, None, via, headers)

        # output 
        self.response.headers['Content-Type'] = 'text/html'
        self.response.out.write(page)

app = webapp.WSGIApplication([
    ('/', MainPage),
], debug=True)

def main():
    run_wsgi_app(app)

if __name__ == '__main__':
    main()
