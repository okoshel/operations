#!/usr/local/bin/python

import sys
import os
import json
import argparse
import shutil
import re
import appscript
import subprocess
from subprocess import Popen, PIPE
from pymongo import MongoClient
from fabric.operations import sudo
from fabric.context_managers import settings
from time import sleep
import fabfile

from wandera_client.client.service.proxy_manager import ProxyManagerClient
from wandera_client.resource import Server
from wandera_client.client.auth import ServiceAuthentication, BaseAuthentication

current_dir = os.getcwd()
home = os.path.expanduser('~')
CONFIG_FILE = os.path.join(home, ".woopas.json")
bitbucket_dir = os.path.join("/Users","arturonoha","Documents","bitbucket")

puppet_master = "puppet.snappli.net"
ssh_user = "anoha"

mongo_intg = {
    'mongo_url1' : "mdb-101-dev.eu-west-1a.ie.wandera.biz",
    'mongo_url2' : "mdb-201-dev.eu-west-1b.ie.wandera.biz",
    'mongo_replica_set' : "rs20007",
    'mongo_port' : 20007,
    'mongo_user' : "admin",
    'mongo_pwd' : "napa6ruK",
    'mongo_db' : "wandera_intg_eu_2"
}

mongo_prod = {
    'mongo_url1' : "mdb-101-core.eu-west-1a.ie.wandera.com",
    'mongo_url2' : "mdb-201-core.eu-west-1b.ie.wandera.com",
    'mongo_replica_set' : "rs22000",
    'mongo_port' : 22000,
    'mongo_user' : "admin",
    'mongo_pwd' : "Pr4DrUya",
    'mongo_db' : "wandera_live_ent_eu"
}

mongo_stgn = {
    'mongo_url1' : "mdb-101-dev.eu-west-1a.ie.wandera.biz",
    'mongo_url2' : "mdb-201-dev.eu-west-1b.ie.wandera.biz",
    'mongo_replica_set' : "rs20004",
    'mongo_port' : 20004,
    'mongo_user' : "admin",
    'mongo_pwd' : "napa6ruK",
    'mongo_db' : "snappli_stgn_eu_2"
}

env_mapping = {
"stgn" : "staging",
"intg" : "integration",
"prod" : "production"
}

node_env_mapping = {
"stgn" : "staging",
"intg" : "integration",
"prod" : "live"
}

mco_roles = [ "proxy" , "app" , "portal" ]

def init_proxy_manager(env):
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        pm_url = config_json["proxy_manager"][env]["proxy_manager_url"]
        #pm_url = "http://54.246.241.216:1999/"
        pm_api_key = config_json["proxy_manager"][env]["service_api_key"]
        pm_api_secret = config_json["proxy_manager"][env]["service_api_secret"]
        pm_auth = ServiceAuthentication(pm_api_key, pm_api_secret)
        #pm_auth = BaseAuthentication(pm_api_key, pm_api_secret)
        pmc = ProxyManagerClient(pm_url,auth=pm_auth)
    except Exception as e:
        print "ERROR: Failed to load ProxyManagerClient\n({})".format(e.message)
        return None

    return pmc

def _get_input(message = None):
    if message is None:
        display_message = "Would you wish to continue? [Y/n]: "
    else:
        display_message = message + " [Y/n]: "

    option = raw_input(display_message)
    while option not in [ 'Y' , 'n' ]:
        option = raw_input("Please enter a valid option [Y/n]: ")

    return option

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', dest = "customers_file")
    parser.add_argument('--env', dest = "env")
    parser.add_argument('--branch', dest = "git_branch")
    parser.add_argument('--dryrun', dest = "dryrun", default = True)

    return parser.parse_args()

def usage():
    print "\nUSAGE: {} --file <customers_list> --env <stgn / intg / prod> --branch <ff_git_branch> --dryrun <True/False>\n".format(sys.argv[0])
    sys.exit(1)

def update_proxies(env, customers_list, dryrun):
    query = { '_id' : { '$in' : customers_list } }
    cursor = db.customer.find(query)
    assumeid_sleep = 30

    proxies = []
    print "\n* Migrated customers proxies:"
    for document in cursor:
        #print "{}".format(document["_id"])
        for proxy in document['proxies']:
            if proxy not in proxies:
                print "{}".format(proxy)
                proxies.append(proxy)

    if len(proxies) == 0:
        print "\nWARN: No proxies found for these customers:\n{}\n".format(customers_list)
    else:
        pmc = init_proxy_manager(env)
        #pmc = None
        if pmc is None:
            print "\n* ERROR: Failed to run Assume ID. Proxies to manually apply AssumeID:"
            for proxy in proxies:
                print "{}".format(proxy)
            if _get_input() != "Y":
                sys.exit("** Aborted by user **")
        else:
            customers_list_str = customers_list[0]
            for i in range(1,len(customers_list)):
                customers_list_str = "{},{}".format(customers_list_str, customers_list[i])
            for proxy in proxies:
                try:
                    pm_proxy = ProxyManagerClient.node_by_host(pmc, proxy)
                    print "INFO: Opening new window and tail logs for {}".format(proxy)
                    new_window(pm_proxy["ip"], ssh_user, customers_list_str)
                    if dryrun in [ True, "true", "True" ]:
                        print "Would apply AssumeId to {} (Name: {}, ID: {})\n".format(proxy, pm_proxy["nodeName"], pm_proxy["ip"])
                    else:
                        print "Applying AssumeId to {} (Name: {}, ID: {})\n".format(proxy, pm_proxy["nodeName"], pm_proxy["ip"])
                        ProxyManagerClient.assume_id(pmc, pm_proxy._id)
                        print "- Waiting {}s for assume ID to be completed".format(assumeid_sleep)
                        sleep(assumeid_sleep)
                except Exception as e:
                    print "\n* ERROR: Failed to run Assume ID on {}. *** Please run manually ***\n({})\n".format(proxy, e)

def git_check_branch(git_path, branch):
    cmd = "cd {}; git branch".format(git_path)
    runcmd = Popen(cmd, shell=True,stdout=PIPE,stderr=PIPE,stdin=sys.stdin)
    out,err = runcmd.communicate()

    if not re.search(branch,out):
    	print "\n** ERROR : Branch dow not exist.\nAvailable branches:\n{}".format(out)
        if _get_input("\nWould you like to create {}".format(branch)) == "Y":
            cmd = "cd {}; git branch {}".format(git_path, branch)
            run_cmd = Popen(cmd, shell=True,stdout=sys.stdout,stderr=sys.stderr,stdin=sys.stdin)
            run_cmd.communicate()
        else:
            branch = raw_input("Please enter a new branch to use: ")
            while not re.search(branch,out):
                branch = raw_input("Please enter a valid branch to use: ")
    return branch

def git_pull(git_path, branch):
    print "\n* Git pull on {}".format(git_path)
    git_cmd = "cd {}; git checkout {}; git pull".format(git_path, branch)
    git_run = Popen(git_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
    git_run.communicate()

def git_commit(git_path, branch, commit_msg):
    print "\n* Git commit on {}".format(git_path)
    git_cmd = "cd {}; git checkout {}; git add -Ap; git commit -m \"{}\";  git push origin {}".format(git_path, branch, commit_msg, branch)
    if _get_input("Would you like to commit changes?") == "Y":
        git_run = Popen(git_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
        git_run.communicate()

def remove_from_ff_file(env, customers_list, failed_customers, flag, dryrun, git_branch):
    ff_path = os.path.join(bitbucket_dir, "feature-flags-{}".format(env))
    ff_file = os.path.join(ff_path, "featureConfigurations.json")
    tmp_file = "{}.tmp".format(ff_file)
    bkp_file = "{}.ori".format(ff_file)

    print "\n- FF: {}\n- TMP: {}\n- ORI: {}\n- Flag: {}\n".format(ff_file, tmp_file, bkp_file, flag)
    #sys.exit(0)

    ff_found = False
    ff_ended = False
    #flag = "multipleDataPlansPerCarrierInitialLoad"
    flag_regex = re.compile(flag)
    end_flag_regex = re.compile("enabled")
    customer_regex = re.compile(".*\"([0-9a-z]{1,}-[0-9a-z]{1,}-[0-9a-z]{1,}-[0-9a-z]{1,}-[0-9a-z]{1,})\".*", re.VERBOSE)

    git_pull(ff_path, git_branch)

    print "\n* Backup {}\n".format(ff_file)
    if dryrun in [ False, "false", "False" ]:
        shutil.copyfile(ff_file, bkp_file)

    with open(ff_file, "r") as input_file, open(tmp_file, "w") as out_file:
        for line in input_file:
            if not ff_found:
                if dryrun in [ False, "false", "False" ]:
                    out_file.write(line)
                line = line.rstrip()
                if flag_regex.search(line):
                    print "Found {} flag ({})".format(flag, line)
                    ff_found = True
            else:
                if ff_ended:
                    if dryrun in [ False, "false", "False" ]:
                        out_file.write(line)
                else:
                    if end_flag_regex.search(line):
                        print "Found end of {} flag ({})".format(flag, line.rstrip())
                        if dryrun in [ False, "false", "False" ]:
                            out_file.write(line)
                        ff_ended = True
                    else:
                        if customer_regex.search(line):
                            #customerid = customer_regex.sub('\1', line)
                            customerid = re.sub(r'[ \t]*"[,]*', '', line.rstrip())
                            #customerid = re.sub(r'".*', '', customerid)
                            #print "DEBUG: Found customerid: {}".format(customerid)
                            if customerid not in customers_list:
                                if dryrun in [ False, "false", "False" ]:
                                    out_file.write(line)
                            else:
                                if customerid in failed_customers:
                                    print "\nWARN: {} failed migration. Skip removal from {}".format(customerid, flag)
                                    out_file.write(line)
                                else:
                                    if dryrun in [ False, "false", "False" ]:
                                        print "INFO: customer {} removed from {} in {}".format(customerid, flag, ff_file)
                                    else:
                                        print "INFO: customer {} would be removed from {} in {}".format(customerid, flag, ff_file)
                        else:
                            if dryrun in [ False, "false", "False" ]:
                                out_file.write(line)
    if dryrun in [ False, "false", "False" ]:
        #move content of tmp file to original file, accounting for last customer having been removed. In that case the customers list would have a comma before the closing ] ==> We remove it
        with open(tmp_file,"r") as myfile:
    	       content = myfile.read()
        replacecontent = re.sub(r',([\n \t]*)\]','\n        ]', content)
        os.remove(ff_file)
        with open(ff_file, "w") as myout:
        	myout.write(replacecontent)
        os.remove(tmp_file)
    else:
        if os.path.isfile(tmp_file):
            os.remove(tmp_file)

    if dryrun in [ False, "false", "False" ]:
        git_message = "OPS-2430 - Remove from {}".format(flag)
        git_commit(ff_path, git_branch, git_message)

def add_to_ff_file(env, customers_list, flag, dryrun, git_branch):
    ff_path = os.path.join(bitbucket_dir, "feature-flags-{}".format(env))
    ff_file = os.path.join(ff_path, "featureConfigurations.json")
    tmp_file = "{}.tmp".format(ff_file)
    bkp_file = "{}.ori".format(ff_file)

    print "\n- FF: {}\n- TMP: {}\n- ORI: {}\n- Flag: {}\n".format(ff_file, tmp_file, bkp_file, flag)
    #sys.exit(0)

    ff_found = False
    ff_ended = False
    tag_found = False
    #flag = "multipleDataPlansPerCarrierInitialLoad"
    flag_regex = re.compile(flag)
    customers_tag_regex = re.compile('"customers"')
    end_flag_regex = re.compile("enabled")
    customer_regex = re.compile(".*\"([0-9a-z]{1,}-[0-9a-z]{1,}-[0-9a-z]{1,}-[0-9a-z]{1,}-[0-9a-z]{1,})\".*", re.VERBOSE)

    git_pull(ff_path, git_branch)

    print "\n* Backup {}\n".format(ff_file)
    if dryrun in [ False, "false", "False" ]:
        shutil.copyfile(ff_file, bkp_file)

    with open(ff_file, "r") as input_file, open(tmp_file, "w") as out_file:
        prev_line = ""
        for line in input_file:
            if not ff_found:
                if dryrun in [ False, "false", "False" ]:
                    out_file.write(line)
                #line = line.rstrip()
                if flag_regex.search(line.rstrip()):
                    print "Found {} flag ({})".format(flag, line.rstrip())
                    ff_found = True
            else:
                if ff_ended:
                    if dryrun in [ False, "false", "False" ]:
                        out_file.write(line)
                else:
                    if not tag_found:
                        if customers_tag_regex.search(line):
                            print "\nFound tag: {}".format(line)
                            tag_found = True
                            blank = False
                            if re.search("\[\]", line):
                                blank = True
                                if dryrun in [ False, "false", "False" ]:
                                    out_file.write("        \"customers\": [\n")
                                print "        \"customers\": [\n"
                            else:
                                if dryrun in [ False, "false", "False" ]:
                                    out_file.write(line)
                            print "inserting customers to migrate"
                            count = 1
                            for customer in customers_list:
                                if count < len(customers_list):
                                    if dryrun in [ False, "false", "False" ]:
                                        out_file.write("            \"{}\",\n".format(customer))
                                    print "            \"{}\",".format(customer)
                                else:
                                    if blank:
                                        if dryrun in [ False, "false", "False" ]:
                                            out_file.write("            \"{}\"\n".format(customer))
                                            out_file.write("        ],\n")
                                        print "            \"{}\"".format(customer)
                                        print "        ],"
                                    else:
                                        if dryrun in [ False, "false", "False" ]:
                                            out_file.write("            \"{}\",\n".format(customer))
                                        print "            \"{}\",".format(customer)
                                count = count + 1
                            print "\nInsertion finished\n"

                    elif end_flag_regex.search(line):
                        print "Found end of {} flag ({})".format(flag, line.rstrip())
                        if dryrun in [ False, "false", "False" ]:
                            out_file.write(line)
                        else:
                            print "{}".format(line)
                        ff_ended = True
                    else:
                        if dryrun in [ False, "false", "False" ]:
                            out_file.write(line)
                        else:
                            print "{}".format(line.rstrip())
                prev_line = line

    if dryrun in [ False, "false", "False" ]:
        #shutil.move(tmp_file, ff_file)
        # remove possible , before end of customer list
        with open(tmp_file,"r") as myfile:
               content = myfile.read()
        replacecontent = re.sub(r',([\n \t]*)\]','\n        ]', content)
        os.remove(ff_file)
        with open(ff_file, "w") as myout:
            myout.write(replacecontent)
        os.remove(tmp_file)
    else:
        if os.path.isfile(tmp_file):
            os.remove(tmp_file)

<<<<<<< HEAD
    if dryrun in [ False, "false", "False" ]:
        git_message = "OPS-2430 - Insert into {}".format(flag)
        git_commit(ff_path, git_branch, git_message)
=======
    git_message = "OPS-2430 - Insert into {}".format(flag)
    git_commit(ff_path, git_branch, git_message)
>>>>>>> df525cb0f3cbb6447cc8ba639bf4db0b3e6e96f7

def check_apns(env, dryrun):
    if env in env_mapping:
        node_env = node_env_mapping[env]
    else:
        sys.exit("\nERROR: invalid environment {}".format(env))

    node_dir = os.path.join(bitbucket_dir, "opsscripts")
    check_apn_cmd = "cd {}; export NODE_ENV={}; node scripts/linkApnsToCarriers.js --action=list-unlinked --file=apns_{}.txt --dryrun={}".format(node_dir, node_env, env, dryrun)
    run_check = Popen(check_apn_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
    run_check.communicate()

    apn_file = os.path.join(node_dir, "apns_{}.txt".format(env))
    with open(apn_file, "r") as bad_apns:
        for apn in bad_apns:
            print "{}".format(apn.rstrip())

def migrate_customers(customers_file, env, dryrun):
    if env in node_env_mapping:
        myenv = node_env_mapping[env]
    else:
        sys.exit("\nERROR: invalid environment {}".format(env))

    migrate_script = os.path.join(current_dir,"migrate-all-customers.sh")
    migrate_cmd = "{} {} {} {}".format(migrate_script, customers_file, myenv, dryrun)
    run_migration = Popen(migrate_cmd, close_fds=True, shell=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
    run_migration.communicate()

def refresh_ff(env, role, dryrun):
    if env in env_mapping:
        myenv = env_mapping[env]
    else:
        sys.exit("\nERROR: invalid environment {}".format(env))

    if not role in mco_roles:
        sys.exit("\nERROR: invalid mco role {}".format(role))

    refresh_cmd = "mco puppet runall 5 --tags=\"refreshfeatureflags\" -S \"environment={} and role={}\"".format(myenv, role)
    if dryrun in [ False, "false", "False" ]:
        with settings(host_string = puppet_master, user = ssh_user):
            sudo(refresh_cmd)
    else:
        print "Would run: {} on {}".format(refresh_cmd, puppet_master)

def get_failed_customers(customers_list):
    failed_customers = []
    for customer in customers_list:
        #print "Checking if {} was successful".format(customer)
        failed_file = "{}.failed".format(customer)
        if os.path.isfile(failed_file):
            #print "** WARN: {} FAILED".format(customer)
            failed_customers.append(customer)
    #print "{} failed customers".format(len(failed_customers))
    return failed_customers

def new_window(active_svr, user, customers_list):
    active_cmd = "cd {}; fab -H {} -u {} tail_proxy_logs:{}".format(current_dir, active_svr, user, customers_list)
    appscript.app('Terminal').do_script(active_cmd)

def check_proxies(env, customers_list):
    query = { '_id' : { '$in' : customers_list } }
    cursor = db.customer.find(query)
    assumeid_sleep = 30

    proxies = []
    #print "\n* Migrated customers proxies:"
    for document in cursor:
        #print "{}".format(document["_id"])
        for proxy in document['proxies']:
            if proxy not in proxies:
                #print "{}".format(proxy)
                proxies.append(proxy)

    if len(proxies) == 0:
        print "\nWARN: No proxies found for these customers:\n{}\n".format(customers_list)
    else:
        pmc = init_proxy_manager(env)
        #pmc = None
        if pmc is None:
            print "\n* ERROR: Failed to get PM driver. Proxies to check"
            for proxy in proxies:
                print "{}".format(proxy)
            if _get_input() != "Y":
                sys.exit("** Aborted by user **")
        else:
            customers_list_str = customers_list[0]
            for i in range(1,len(customers_list)):
                customers_list_str = "{},{}".format(customers_list_str, customers_list[i])
            for proxy in proxies:
                try:
                    pm_proxy = ProxyManagerClient.node_by_host(pmc, proxy)
                    print "Checking FF in {} ({} - {})\n".format(pm_proxy["nodeName"], proxy, pm_proxy["ip"])
                    with settings(host_string=pm_proxy["ip"], user=ssh_user):
                        fabfile.check_proxy_ff(customers_list)

                    print "\nChecking logs in {} ({} - {})\n".format(pm_proxy["nodeName"], proxy, pm_proxy["ip"])
                    with settings(host_string=pm_proxy["ip"], user=ssh_user):
                        fabfile.check_customerid_proxy_logs(customers_list)
                    print "\n++++++++++++++++++++++++++++++++++++++++++++++++++\n"
                except Exception as e:
                    print "\n* ERROR: Failed to check {}. *** Please run manually ***\n({})\n".format(proxy, e)

def check_proxies_ff(env, customers_list):
    query = { '_id' : { '$in' : customers_list } }
    cursor = db.customer.find(query)
    assumeid_sleep = 30

    proxies = []
    for document in cursor:
        #print "{}".format(document["_id"])
        for proxy in document['proxies']:
            if proxy not in proxies:
                proxies.append(proxy)

    if len(proxies) == 0:
        print "\nWARN: No proxies found for these customers:\n{}\n".format(customers_list)
    else:
        pmc = init_proxy_manager(env)
        #pmc = None
        if pmc is None:
            print "\n* ERROR: Failed to get PM driver. Proxies to check"
            for proxy in proxies:
                print "{}".format(proxy)
            if _get_input() != "Y":
                sys.exit("** Aborted by user **")
        else:
            customers_list_str = customers_list[0]
            for i in range(1,len(customers_list)):
                customers_list_str = "{},{}".format(customers_list_str, customers_list[i])
            for proxy in proxies:
                try:
                    pm_proxy = ProxyManagerClient.node_by_host(pmc, proxy)
                    print "Checking FF in {} ({} - {})\n".format(pm_proxy["nodeName"], proxy, pm_proxy["ip"])
                    with settings(host_string=pm_proxy["ip"], user=ssh_user):
                        fabfile.check_proxy_ff(customers_list)
                except Exception as e:
                    print "\n* ERROR: Failed to check {}. *** Please check manually ***\n({})\n".format(proxy, e)

if __name__ == "__main__":
    ## PARSE AND VALIDATE INPUT ARGUMENTS
    options = parse_options(sys.argv[1:])

    # Verify that a file with the list of customers is provided and it exists
    if options.customers_file is None:
        usage()
    customers_file = os.path.abspath(options.customers_file)
    if not os.path.isfile(customers_file):
        sys.exit("\nERROR: {} not found\n".format(customers_file))

    # Validate the environment provided
    if options.env is None or options.env not in [ 'stgn','intg','prod']:
        usage()
    env = options.env

    # Verify that a branch is provided
    if options.git_branch is None:
        usage()
    git_branch = options.git_branch

    # Initialise the mongodb instance based on the environment
    mongo = eval("mongo_{}".format(env))
    mongo_cnx_url = "mongodb://{}:{}@{},{}:{}/?replicaSet={}".format(mongo["mongo_user"], mongo["mongo_pwd"], mongo["mongo_url1"], mongo["mongo_url2"], mongo["mongo_port"], mongo["mongo_replica_set"])
    client = MongoClient(mongo_cnx_url)
    db = client[mongo["mongo_db"]]

    # Display values that will be used for the migration and ask for comfirmation
    dryrun = options.dryrun
    if _get_input("\n- env: {}\n- customers file: {}\n- git branch: {}\n- dryrun: {}\nContinue?".format(env, customers_file, git_branch, dryrun)) != "Y":
        sys.exit("\n** Aborted by user **\n")

    # Get the list of customers to migrate from the input file
    customers_list = []
    with open(customers_file, "r") as myfile:
        for line in myfile:
            customers_list.append(line.rstrip())

    '''update_proxies(env, customers_list, dryrun)
    sys.exit(0)'''

    # check the existance of the branch and give option to create or select an existing one
    git_path = os.path.join(bitbucket_dir, "feature-flags-{}".format(env))
    git_branch = git_check_branch(git_path, git_branch)

    # run the script to check the apn - carrier mapping
    check_apns(env, dryrun)

    if dryrun in [ False, "false", "False" ]:
        if _get_input("\nContinue with next step (migrate_customers - dryrun)?") != "Y":
            sys.exit("\n*** Aborted by user ***\n")
    # Run dryrun of MongoDB update
    migrate_customers(customers_file, env, True)

    # Display the list of customers that would have failed
    failed_customers = get_failed_customers(customers_list)
    if len(failed_customers) > 0:
        print "\nWARNING: The following customers would fail to migrate:"
        for customer in failed_customers:
            print "{}".format(customer)

    if dryrun in [ False, "false", "False" ]:
        if _get_input("\nContinue with next step (add customer list to upgradeInProgress FF)?") != "Y":
            sys.exit("\n*** Aborted by user ***\n")
    # Add the customers to the upgradeInProgress FF and refresh FF for portal role
    add_to_ff_file(env, customers_list, "upgradeInProgress", dryrun, git_branch)
    refresh_ff(env, "portal", dryrun)

    # Run MongoDB update
    if dryrun in [ False, "false", "False" ]:
        if _get_input("\nContinue with next step (migrate_customers)?") != "Y":
            sys.exit("\n*** Aborted by user ***\n")
        migrate_customers(customers_file, env, "False")
        # Display the list of failed customers
        failed_customers = get_failed_customers(customers_list)
        if len(failed_customers) > 0:
            print "\nWARNING: The following customers FAILED to migrate:"
            for customer in failed_customers:
                print "{}".format(customer)

    if dryrun in [ False, "false", "False" ]:
        if _get_input("\nContinue with next step (remove customers from multipleDataPlansPerCarrierInitialLoad FF)?") != "Y":
            sys.exit("\n*** Aborted by user ***\n")
    # Remove migrated customers from multipleDataPlansPerCarrierInitialLoad
    remove_from_ff_file(env, customers_list, failed_customers, "multipleDataPlansPerCarrierInitialLoad", dryrun, git_branch)

    if dryrun in [ False, "false", "False" ]:
        if _get_input("\nContinue with next step (refresh FF proxy)?") != "Y":
            sys.exit("\n*** Aborted by user ***\n")
    # Refresh FF for proxy role
    refresh_ff(env, "proxy", dryrun)
<<<<<<< HEAD
    refresh_ff(env, "app", dryrun)
=======
>>>>>>> df525cb0f3cbb6447cc8ba639bf4db0b3e6e96f7

    if dryrun in [ False, "false", "False" ]:
        if _get_input("\nContinue with next step (AssumeID in proxies)?") != "Y":
            sys.exit("\n*** Aborted by user ***\n")
    # Attempt to run Assume ID. If it fails, list the proxies that need to be manually updated
    update_proxies(env, customers_list, dryrun)

<<<<<<< HEAD
    if dryrun in [ False, "false", "False" ]:
        print "\n* Checking FF status\n"
        check_proxies_ff(env, customers_list)
=======
    print "\n* Checking FF status\n"
    check_proxies_ff(env, customers_list)
>>>>>>> df525cb0f3cbb6447cc8ba639bf4db0b3e6e96f7

    if dryrun in [ False, "false", "False" ]:
        if _get_input("\nContinue with next step (remove customers from multipleDataPlansPerCarrier FF)?") != "Y":
            sys.exit("\n*** Aborted by user ***\n")
    # Remove migrated customers from multipleDataPlansPerCarrier FF
    remove_from_ff_file(env, customers_list, failed_customers, "multipleDataPlansPerCarrier", dryrun, git_branch)

    if dryrun in [ False, "false", "False" ]:
        if _get_input("\nContinue with next step (refresh FF proxy - app - portal)?") != "Y":
            sys.exit("\n*** Aborted by user ***\n")
    # Refresh FF for all 3 roles proxy, app, portal
    refresh_ff(env, "proxy", dryrun)
    refresh_ff(env, "app", dryrun)
    refresh_ff(env, "portal", dryrun)

    if dryrun in [ False, "false", "False" ]:
        if _get_input("\nContinue with next step (remove customers from upgradeInProgress FF)?") != "Y":
            sys.exit("\n*** Aborted by user ***\n")
    # Remove the migrated customers from the upgradeInProgress FF and refresh FF for portal
    remove_from_ff_file(env, customers_list, failed_customers, "upgradeInProgress", dryrun, git_branch)
    refresh_ff(env, "portal", dryrun)

<<<<<<< HEAD
    if dryrun in [ False, "false", "False" ]:
        print "\n\n*** Running check of FF and logs in updated proxies ***\n\n"
        check_proxies(env, customers_list)
=======
    #if dryrun in [ False, "false", "False" ]:
    print "\n\n*** Running check of FF and logs in updated proxies ***\n\n"
    check_proxies(env, customers_list)
>>>>>>> df525cb0f3cbb6447cc8ba639bf4db0b3e6e96f7
