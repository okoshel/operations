#!/bin/bash

opsdir="/Users/arturonoha/Documents/bitbucket/opsscripts"
current_dir=$(pwd)


clear

usage() {
	echo "$0 <input_file> <env> <dryrun>"
	exit 1
}

if [ -z $1 ]; then
	echo "Please specify the file with customer ids as a first argument"
	usage
fi

if [ -z $2 ]; then
	echo "Please specify environment: staging / integration / live"
	usage
fi
if [[ $2 != "staging" && $2 != "integration" && $2 != "live" ]]; then
	echo "Please specify environment: staging / integration / live"
	usage
fi

if [ -z $3 ]; then
	echo "Please specify dryrun <true/false> as a second argument"
	usage
fi
dryrun=$3
if [[ $dryrun == "False" || $dryrun == false ]]; then
	dryrun="false"
	LOG_FILE="${current_dir}/migration.`date +%Y-%m-%d_%H-%M-%S`.${2}.log"
else
	LOG_FILE="${current_dir}/migration.`date +%Y-%m-%d_%H-%M-%S`.${2}.DRYRUN.log"
fi

echo "Starting migration of customers from file $1 dryrun=$dryrun" | tee $LOG_FILE

current_env=$(echo $NODE_ENV)
cd ${opsdir}
export NODE_ENV="$2"
echo $NODE_ENV
index=0
while IFS='' read -r line || [[ -n "$line" ]]; do
	((index+=1))
	printf "\n\nMigrating $index. customer ($line):" | tee -a $LOG_FILE
	TMP_FILE="${current_dir}/$line.tmp"
	FAILED_FILE="${current_dir}/$line.failed"
	if [[ -f $FAILED_FILE ]]; then
		rm $FAILED_FILE
	fi
	node ./scripts/migrateOldDataplans.js --dryrun=$dryrun --customerId=$line | tee $TMP_FILE
	cat $TMP_FILE >> $LOG_FILE
	if [[ `grep "All data plans were migrated successfully" $TMP_FILE` ]]; then
		rm $TMP_FILE
	else
		echo "** WARNING: Failed $line. Creating $FAILED_FILE" | tee -a $LOG_FILE
		mv -v $TMP_FILE $FAILED_FILE
	fi
done < "$1"

echo "Migration finished" >> $LOG_FILE
cd ${current_dir}
export NODE_ENV=${current_env}
echo "Log available at: ${LOG_FILE}"
