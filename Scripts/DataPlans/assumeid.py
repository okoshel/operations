#!/usr/local/bin/python

import sys
import os
import json
import argparse
import shutil
import re
import appscript
import subprocess
from subprocess import Popen, PIPE
from pymongo import MongoClient
from fabric.operations import sudo
from fabric.context_managers import settings
from time import sleep

from wandera_client.client.service.proxy_manager import ProxyManagerClient
from wandera_client.resource import Server
from wandera_client.client.auth import ServiceAuthentication, BaseAuthentication

current_dir = os.getcwd()
home = os.path.expanduser('~')
CONFIG_FILE = os.path.join(home, ".woopas.json")
bitbucket_dir = os.path.join("/Users","arturonoha","Documents","bitbucket")

puppet_master = "puppet.snappli.net"
ssh_user = "anoha"

mongo_intg = {
    'mongo_url1' : "mdb-101-dev.eu-west-1a.ie.wandera.biz",
    'mongo_url2' : "mdb-201-dev.eu-west-1b.ie.wandera.biz",
    'mongo_replica_set' : "rs20007",
    'mongo_port' : 20007,
    'mongo_user' : "admin",
    'mongo_pwd' : "napa6ruK",
    'mongo_db' : "wandera_intg_eu_2"
}

mongo_prod = {
    'mongo_url1' : "mdb-101-core.eu-west-1a.ie.wandera.com",
    'mongo_url2' : "mdb-201-core.eu-west-1b.ie.wandera.com",
    'mongo_replica_set' : "rs22000",
    'mongo_port' : 22000,
    'mongo_user' : "admin",
    'mongo_pwd' : "Pr4DrUya",
    'mongo_db' : "wandera_live_ent_eu"
}

mongo_stgn = {
    'mongo_url1' : "mdb-101-dev.eu-west-1a.ie.wandera.biz",
    'mongo_url2' : "mdb-201-dev.eu-west-1b.ie.wandera.biz",
    'mongo_replica_set' : "rs20004",
    'mongo_port' : 20004,
    'mongo_user' : "admin",
    'mongo_pwd' : "napa6ruK",
    'mongo_db' : "snappli_stgn_eu_2"
}

env_mapping = {
"stgn" : "staging",
"intg" : "integration",
"prod" : "production"
}

node_env_mapping = {
"stgn" : "staging",
"intg" : "integration",
"prod" : "live"
}

mco_roles = [ "proxy" , "app" , "portal" ]

assumeid_sleep = 30

def init_proxy_manager(env):
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        pm_url = config_json["proxy_manager"][env]["proxy_manager_url"]
        #pm_url = "http://54.246.241.216:1999/"
        pm_api_key = config_json["proxy_manager"][env]["service_api_key"]
        pm_api_secret = config_json["proxy_manager"][env]["service_api_secret"]
        pm_auth = ServiceAuthentication(pm_api_key, pm_api_secret)
        #pm_auth = BaseAuthentication(pm_api_key, pm_api_secret)
        pmc = ProxyManagerClient(pm_url,auth=pm_auth)
    except Exception as e:
        print "ERROR: Failed to load ProxyManagerClient\n({})".format(e.message)
        return None

    return pmc

def _get_input(message = None):
    if message is None:
        display_message = "Would you wish to continue? [Y/n]: "
    else:
        display_message = message + " [Y/n]: "

    option = raw_input(display_message)
    while option not in [ 'Y' , 'n' ]:
        option = raw_input("Please enter a valid option [Y/n]: ")

    return option

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', dest = "customers_file")
    parser.add_argument('--proxy', dest = "proxy_to_assume")
    parser.add_argument('--env', dest = "env")
    parser.add_argument('--dryrun', dest = "dryrun", default = True)

    return parser.parse_args()

def usage():
    print "\nUSAGE: {} [--proxy <proxy_name> / --file <customers_list>] --env <stgn / intg / prod> --dryrun <True/False>\n".format(sys.argv[0])
    sys.exit(1)

def update_proxies(env, customers_list, dryrun):
    query = { '_id' : { '$in' : customers_list } }
    cursor = db.customer.find(query)


    proxies = []
    for document in cursor:
        print "{}".format(document["_id"])
        for proxy in document['proxies']:
            if proxy not in proxies:
                proxies.append(proxy)

    if len(proxies) == 0:
        print "\nWARN: No proxies found for these customers:\n{}\n".format(customers_list)
    else:
        pmc = init_proxy_manager(env)
        #pmc = None
        if pmc is None:
            print "\n* ERROR: Failed to run Assume ID. Proxies to manually apply AssumeID:"
            for proxy in proxies:
                print "{}".format(proxy)
            if _get_input() != "Y":
                sys.exit("** Aborted by user **")
        else:
            customers_list_str = customers_list[0]
            for i in range(1,len(customers_list)):
                customers_list_str = "{},{}".format(customers_list_str, customers_list[i])
            for proxy in proxies:
                try:
                    pm_proxy = ProxyManagerClient.node_by_host(pmc, proxy)
                except Exception as e:
                    print "\n* ERROR: Failed to get ProxyManager info for {}. *** Please run manually ***\n({})\n".format(proxy, e)
                try:
                    print "INFO: Opening new window and tail logs for {}".format(proxy)
                    new_window(pm_proxy["ip"], ssh_user, customers_list_str)
                    if dryrun in [ True, "true", "True" ]:
                        print "Would apply AssumeId to {} (Name: {} - IP: {})\n".format(proxy, pm_proxy["nodeName"], pm_proxy["ip"])
                    else:
                        print "Applying AssumeId to {} (Name: {} - IP: {})\n".format(proxy, pm_proxy["nodeName"], pm_proxy["ip"])
                        ProxyManagerClient.assume_id(pmc, pm_proxy._id)
                        print "- Waiting {}s for assume ID to be completed".format(assumeid_sleep)
                        sleep(assumeid_sleep)
                except Exception as e:
                    print "\n* ERROR: Failed to run Assume ID on {}. *** Please run manually ***\n({})\n".format(proxy, e)

def assume_proxy(proxy,env):
    try:
        pmc = init_proxy_manager(env)
        pm_proxy = ProxyManagerClient.node_by_host(pmc, proxy)
    except Exception as e:
        print "\n* ERROR: Failed to get ProxyManager info for {}. *** Please run manually ***\n({})\n".format(proxy, e)
    if pm_proxy is None:
        print "\n* ERROR: ProxyManager returned None for {}\n".format(proxy)
    else:
        try:
            print "{}".format(pm_proxy)
            print "INFO: Opening new window and tail logs for {}".format(proxy)
            new_window(pm_proxy.ip, ssh_user)
            if dryrun in [ True, "true", "True" ]:
                print "Would apply AssumeId to {} (Name: {} - IP: {})\n".format(proxy, pm_proxy["nodeName"], pm_proxy["ip"])
            else:
                print "Applying AssumeId to {} (Name: {} - IP: {})\n".format(proxy, pm_proxy["nodeName"], pm_proxy["ip"])
                ProxyManagerClient.assume_id(pmc, pm_proxy._id)
                print "- Waiting {}s for assume ID to be completed".format(assumeid_sleep)
                sleep(assumeid_sleep)
        except Exception as e:
            print "\n* ERROR: Failed to run Assume ID on {}. *** Please run manually ***\n({})\n".format(proxy, e)


def new_window(active_svr, user, customers_list=None):
    active_cmd = "cd {}; fab -H {} -u {} tail_proxy_logs:{}".format(current_dir, active_svr, user, customers_list)
    appscript.app('Terminal').do_script(active_cmd)

if __name__ == "__main__":
    ## PARSE AND VALIDATE INPUT ARGUMENTS
    options = parse_options(sys.argv[1:])


    # Validate the environment provided
    if options.env is None or options.env not in [ 'stgn','intg','prod']:
        usage()
    env = options.env

    # Initialise the mongodb instance based on the environment
    mongo = eval("mongo_{}".format(env))
    mongo_cnx_url = "mongodb://{}:{}@{},{}:{}/?replicaSet={}".format(mongo["mongo_user"], mongo["mongo_pwd"], mongo["mongo_url1"], mongo["mongo_url2"], mongo["mongo_port"], mongo["mongo_replica_set"])
    client = MongoClient(mongo_cnx_url)
    db = client[mongo["mongo_db"]]

    dryrun = options.dryrun

    # Verify that a file with the list of customers is provided and it exists
    if options.customers_file is None and options.proxy_to_assume is None:
        usage()

    if options.customers_file is not None:
        customers_file = os.path.abspath(options.customers_file)
        if not os.path.isfile(customers_file):
            sys.exit("\nERROR: {} not found\n".format(customers_file))
        # Display values that will be used for the migration and ask for comfirmation

        if _get_input("\n- env: {}\n- customers file: {}\n- dryrun: {}\nContinue?".format(env, customers_file, dryrun)) != "Y":
            sys.exit("\n** Aborted by user **\n")

        # Get the list of customers to migrate from the input file
        customers_list = []
        with open(customers_file, "r") as myfile:
            for line in myfile:
                customers_list.append(line.rstrip())

        update_proxies(env, customers_list, dryrun)
    else:
        proxy_to_assume = options.proxy_to_assume
        if _get_input("\n- env: {}\n- porxy: {}\n- dryrun: {}\nContinue?".format(env, proxy_to_assume, dryrun)) != "Y":
            sys.exit("\n** Aborted by user **\n")
        assume_proxy(proxy_to_assume,env)
