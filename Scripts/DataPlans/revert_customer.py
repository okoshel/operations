#!/usr/local/bin/python

import sys
import os
import json
import argparse
from pymongo import MongoClient

from wandera_client.client.service.proxy_manager import ProxyManagerClient
from wandera_client.resource import Server
from wandera_client.client.auth import ServiceAuthentication

home = os.path.expanduser('~')
CONFIG_FILE = os.path.join(home, ".woopas.json")

mongo_intg = {
    'mongo_url1' : "mdb-101-dev.eu-west-1a.ie.wandera.biz",
    'mongo_url2' : "mdb-201-dev.eu-west-1b.ie.wandera.biz",
    'mongo_replica_set' : "rs20007",
    'mongo_port' : 20007,
    'mongo_user' : "admin",
    'mongo_pwd' : "napa6ruK",
    'mongo_db' : "wandera_intg_eu_2"
}

mongo_prod = {
    'mongo_url1' : "mdb-101-core.eu-west-1a.ie.wandera.com",
    'mongo_url2' : "mdb-201-core.eu-west-1b.ie.wandera.com",
    'mongo_replica_set' : "rs22000",
    'mongo_port' : 22000,
    'mongo_user' : "admin",
    'mongo_pwd' : "Pr4DrUya",
    'mongo_db' : "wandera_live_ent_eu"
}

mongo_stgn = {
    'mongo_url1' : "mdb-101-dev.eu-west-1a.ie.wandera.biz",
    'mongo_url2' : "mdb-201-dev.eu-west-1b.ie.wandera.biz",
    'mongo_replica_set' : "rs20004",
    'mongo_port' : 20004,
    'mongo_user' : "admin",
    'mongo_pwd' : "napa6ruK",
    'mongo_db' : "snappli_stgn_eu_2"
}

def _get_input(message = None):
    if message is None:
        display_message = "Would you wish to continue? [Y/n]: "
    else:
        display_message = message + " [Y/n]: "

    option = raw_input(display_message)
    while option not in [ 'Y' , 'n' ]:
        option = raw_input("Please enter a valid option [Y/n]: ")

    return option

def parse_options():
    parser = argparse.ArgumentParser()
    parser.add_argument("customerid")
    parser.add_argument("ORIGINAL_CAP_PERIOD")
    parser.add_argument('--firstday', dest = "firstday")
    parser.add_argument('--env', dest = "env")
    parser.add_argument('--dryrun', dest = "dryrun", default = True)

    return parser.parse_args()

def usage():
    print "\nUSAGE: {} customerid ORIGINAL_CAP_PERIOD --firstday <firstday for MONTHLY PERIOD> --env <stgn / intg / prod> --dryrun <True/False>\n".format(sys.argv[0])
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        usage()

    #customerid = sys.argv[1]

    options = parse_options()

    if options.customerid is None:
        usage()
    customerid = options.customerid

    if options.ORIGINAL_CAP_PERIOD is None:
        usage()
    ORIGINAL_CAP_PERIOD = options.ORIGINAL_CAP_PERIOD

    if ORIGINAL_CAP_PERIOD == "MONTHLY":
        if options.firstday is None:
            usage()
        else:
            firstday = options.firstday
            update = { '$set' : {"globalPolicy.local.capPeriod.value" : ORIGINAL_CAP_PERIOD, "globalPolicy.local.capPeriod.firstDay" : firstday, "globalPolicy.local.allowancePeriod.value" : ORIGINAL_CAP_PERIOD, "globalPolicy.local.allowancePeriod.firstDay" : firstday} }
            dryrun_msg = "will be updated to \"{}\", firstDay: {}".format(ORIGINAL_CAP_PERIOD, firstday)
    else:
        update = { '$set' : {"globalPolicy.local.capPeriod.value" : ORIGINAL_CAP_PERIOD, "globalPolicy.local.allowancePeriod.value" : ORIGINAL_CAP_PERIOD} }
        dryrun_msg = "will be updated to \"{}\"".format(ORIGINAL_CAP_PERIOD)

    if options.env is None or options.env not in [ 'stgn','intg','prod']:
        usage()
    env = options.env

    mongo = eval("mongo_{}".format(env))
    mongo_cnx_url = "mongodb://{}:{}@{},{}:{}/?replicaSet={}".format(mongo["mongo_user"], mongo["mongo_pwd"], mongo["mongo_url1"], mongo["mongo_url2"], mongo["mongo_port"], mongo["mongo_replica_set"])
    client = MongoClient(mongo_cnx_url)
    db = client[mongo["mongo_db"]]

    dryrun = options.dryrun
    if dryrun in [False,"False","false"]:
        if _get_input("\n- customers id:{}\n-environment: {}\n- dryrun: {}\nContinue?".format(customerid, env, dryrun)) != "Y":
            sys.exit("\n** Aborted by user **\n")

    query = {"_id" : customerid}

    if dryrun in [False,"False","false"]:
        result = db.customer.update_one(query, update)
        print "Matched: {} - Updated: {}".format(result.matched_count, result.modified_count)
    else:
        fields = {"globalPolicy.local.capPeriod.value":1,"globalPolicy.local.allowancePeriod.value":1}
        print "Running {}".format(query)
        result = db.customer.find(query, fields)
        for doc in result:
            print "{} {}".format(doc, dryrun_msg)
            print "{}".format(update)
