#!/bin/bash
echo $#
if [[ $# -lt 4 ]]; then
  echo ""
  echo "USAGE: $0 <CUSTOMERS_LIST_FILE> <MIGRATION_LOG> <ENV> <dryrun>"
  echo ""
  exit 1
fi

customers_file=$1
migration_log=$2
env=$3
dryrun=$4

if [[ ! -f $customers_file || ! -f $migration_log ]]; then
  echo ""
  echo "Input files not found"
  echo ""
  exit 1
fi

for customer in `cat $customers_file`; do
  prev=`grep -A4 $customer $migration_log | grep "Resolved customer" | cut -d'{' -f3 | cut -d'}' -f1 | tr -d '\' | tr -d '"'`
  echo $prev
  if [[ `echo $prev | grep "MONTHLY"` ]]; then
    firstday=`echo $prev | cut -d',' -f2 | cut -d':' -f2`
    plan=`echo $prev | cut -d',' -f1 | cut -d':' -f2`
    echo "python revert_customer.py $customer $plan --firstday $firstday --env $env --dryrun $dryrun"
    python revert_customer.py $customer $plan --firstday $firstday --env $env --dryrun $dryrun
  else
    plan=`echo $prev | cut -d',' -f1 | cut -d':' -f2`
    echo "python revert_customer.py $customer $plan --env $env --dryrun $dryrun"
    python revert_customer.py $customer $plan --env $env --dryrun $dryrun
  fi
  echo
  echo
done
