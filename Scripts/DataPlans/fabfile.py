from fabric.operations import sudo, local
from fabric.context_managers import hide, show
from fabric.api import env

def tail_proxy_logs(*customers_list):
    grep_cmd = "Resetting.*counters|Traffic counter period changed for customer|Successfully persisted.*customers states to User Service|Processing policy changes for customer|Customer.*is in an invalid state"
    if customers_list is not None:
        for customerid in customers_list:
            grep_cmd = "{}|{}".format(grep_cmd, customerid)

    proxy_logs = "/opt/proxy/logs/*.log"
    command = "echo \"tail -f {0} | egrep '{1}'\"; tail -f {0} | egrep \"{1}\"".format(proxy_logs, grep_cmd)
    with hide('running'):
        sudo(command)

def check_customerid_proxy_logs(customers_list):
    grep_cmd = "{}".format(customers_list[0])
    for i in range(1,len(customers_list)):
        grep_cmd = "{}|{}".format(grep_cmd, customers_list[i])

    proxy_logs = [ "/opt/proxy/logs/rabbit.log" , "/opt/proxy/logs/access.log" ]
    for log_file in proxy_logs:
        print "* Checking {} on {}".format(log_file, env.host_string)
        command = "echo \"egrep '{1}' {0} | tail -n 50\"; egrep \"{1}\" {0} | tail -n 20".format(log_file, grep_cmd)
        with hide('running'):
            sudo(command)
        print "\n-------------------\n"

def check_proxy_ff(customers_list):
    grep_cmd = "multipleDataPlansPerCarrier|multipleDataPlansPerCarrierInitialLoad|notificationsTethering|dataplansBoltons".format(customers_list[0])
    for i in range(1,len(customers_list)):
        grep_cmd = "{}|{}".format(grep_cmd, customers_list[i])

    command = "egrep \"{}\" /opt/flags/featureConfigurations.json".format(grep_cmd)
    with hide('running'):
        sudo(command)
