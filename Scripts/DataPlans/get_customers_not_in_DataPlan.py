#!/usr/local/bin/python

import sys
import os
import json
import argparse
from pymongo import MongoClient

mongo_intg = {
    'mongo_url1' : "mdb-101-dev.eu-west-1a.ie.wandera.biz",
    'mongo_url2' : "mdb-201-dev.eu-west-1b.ie.wandera.biz",
    'mongo_replica_set' : "rs20007",
    'mongo_port' : 20007,
    'mongo_user' : "admin",
    'mongo_pwd' : "napa6ruK",
    'mongo_db' : "wandera_intg_eu_2"
}

mongo_prod = {
    'mongo_url1' : "mdb-101-core.eu-west-1a.ie.wandera.com",
    'mongo_url2' : "mdb-201-core.eu-west-1b.ie.wandera.com",
    'mongo_replica_set' : "rs22000",
    'mongo_port' : 22000,
    'mongo_user' : "admin",
    'mongo_pwd' : "Pr4DrUya",
    'mongo_db' : "wandera_live_ent_eu"
}

mongo_stgn = {
    'mongo_url1' : "mdb-101-dev.eu-west-1a.ie.wandera.biz",
    'mongo_url2' : "mdb-201-dev.eu-west-1b.ie.wandera.biz",
    'mongo_replica_set' : "rs20004",
    'mongo_port' : 20004,
    'mongo_user' : "admin",
    'mongo_pwd' : "napa6ruK",
    'mongo_db' : "snappli_stgn_eu_2"
}

def _get_input(message = None):
    if message is None:
        display_message = "Would you wish to continue? [Y/n]: "
    else:
        display_message = message + " [Y/n]: "

    option = raw_input(display_message)
    while option not in [ 'Y' , 'n' ]:
        option = raw_input("Please enter a valid option [Y/n]: ")

    return option

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('--env', dest = "env")

    return parser.parse_args()

def usage():
    print "\nUSAGE: {} --env <stgn / intg / prod>\n".format(sys.argv[0])
    sys.exit(1)

if __name__ == "__main__":
    options = parse_options(sys.argv[1:])

    if options.env is None or options.env not in [ 'stgn','intg','prod']:
        usage()
    env = options.env

    mongo = eval("mongo_{}".format(env))
    mongo_cnx_url = "mongodb://{}:{}@{},{}:{}/?replicaSet={}".format(mongo["mongo_user"], mongo["mongo_pwd"], mongo["mongo_url1"], mongo["mongo_url2"], mongo["mongo_port"], mongo["mongo_replica_set"])
    client = MongoClient(mongo_cnx_url)
    db = client[mongo["mongo_db"]]

    query = { '$nor' : [ { '$and' : [ { 'globalPolicy.activeDataThresholdStrategy.value' : 'ALLOWANCE' }, { 'globalPolicy.allowancePeriod.value' : 'AS_PER_PLAN' } ] }, { '$and' : [ { 'globalPolicy.activeDataThresholdStrategy.value' : { '$ne' : 'ALLOWANCE' } }, { 'globalPolicy.local.capPeriod.value' : 'AS_PER_PLAN' } ] } ] }

    cursor = db.customer.find(query)

    customers = []
    for document in cursor:
        customers.append(document["_id"])

    for cust in customers:
        print "{}".format(cust)
