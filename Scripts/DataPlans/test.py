#!/usr/local/bin/python

import sys
import os
import json
import argparse
import shutil
import re
import subprocess
from subprocess import Popen, PIPE
from pymongo import MongoClient
from fabric.operations import sudo
from fabric.context_managers import settings

from datetime import datetime

import wandera_client
from wandera_client.client.service.proxy_manager import ProxyManagerClient
from wandera_client.resource import Server
from wandera_client.client.auth import ServiceAuthentication, BaseAuthentication

from requests.auth import HTTPBasicAuth

home = os.path.expanduser('~')
CONFIG_FILE = os.path.join(home, ".woopas.json")
bitbucket_dir = os.path.join("/Users","arturonoha","Documents","bitbucket")

puppet_master = "puppet.snappli.net"
ssh_user = "anoha"

mongo_intg = {
    'mongo_url1' : "mdb-101-dev.eu-west-1a.ie.wandera.biz",
    'mongo_url2' : "mdb-201-dev.eu-west-1b.ie.wandera.biz",
    'mongo_replica_set' : "rs20007",
    'mongo_port' : 20007,
    'mongo_user' : "admin",
    'mongo_pwd' : "napa6ruK",
    'mongo_db' : "wandera_intg_eu_2"
}

mongo_prod = {
    'mongo_url1' : "mdb-101-core.eu-west-1a.ie.wandera.com",
    'mongo_url2' : "mdb-201-core.eu-west-1b.ie.wandera.com",
    'mongo_replica_set' : "rs22000",
    'mongo_port' : 22000,
    'mongo_user' : "admin",
    'mongo_pwd' : "Pr4DrUya",
    'mongo_db' : "wandera_live_ent_eu"
}

mongo_stgn = {
    'mongo_url1' : "mdb-101-dev.eu-west-1a.ie.wandera.biz",
    'mongo_url2' : "mdb-201-dev.eu-west-1b.ie.wandera.biz",
    'mongo_replica_set' : "rs20004",
    'mongo_port' : 20004,
    'mongo_user' : "admin",
    'mongo_pwd' : "napa6ruK",
    'mongo_db' : "snappli_stgn_eu_2"
}

env_mapping = {
"stgn" : "staging",
"intg" : "integration",
"prod" : "production"
}

node_env_mapping = {
"stgn" : "staging",
"intg" : "integration",
"prod" : "live"
}

assumeid_user = "RabbIT"
assumeid_pwd = "RabbIT5napProxy"

def init_proxy_manager(env):
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        pm_url = config_json["proxy_manager"][env]["proxy_manager_url"]
        #pm_url = "http://54.246.241.216:1999/"
        pm_api_key = config_json["proxy_manager"][env]["service_api_key"]
        pm_api_secret = config_json["proxy_manager"][env]["service_api_secret"]
        pm_auth = ServiceAuthentication(pm_api_key, pm_api_secret)
        #pm_auth = HTTPBasicAuth(pm_api_key, pm_api_secret)
        pmc = ProxyManagerClient(pm_url,auth=pm_auth)
    except Exception as e:
        print "ERROR: Failed to load ProxyManagerClient\n({})".format(e.message)
        return None

    return pmc, pm_api_key, pm_api_secret

def update_proxies(env, customers_list, dryrun):
    query = { '_id' : { '$in' : customers_list } }
    cursor = db.customer.find(query)

    proxies = []
    for document in cursor:
        #print "{}".format(document["_id"])
        for proxy in document['proxies']:
            if proxy not in proxies:
                proxies.append(proxy)
    pmc, pm_api_key, pm_api_secret = init_proxy_manager(env)
    #pmc = None
    if pmc is None:
        print "\n* ERROR: Failed to run Assume ID. Proxies to manually apply AssumeID:"
        for proxy in proxies:
            print "{}".format(proxy)
        if _get_input() != "Y":
            sys.exit("** Aborted by user **")
    else:
        for proxy in proxies:
            try:
                pm_proxy = ProxyManagerClient.node_by_host(pmc, proxy)
                if dryrun in [ True, "true", "True" ]:
                    print "Would apply AssumeId to {} (Name: {}, ID: {})".format(proxy, pm_proxy["nodeName"], pm_proxy._id)
                else:
                    print "Applying AssumeId to {} (Name: {}, ID: {})".format(proxy, pm_proxy["nodeName"], pm_proxy._id)
                    ProxyManagerClient.assume_id(pmc, pm_proxy._id)
            except Exception as e:
                print "\n* ERROR: Failed to run Assume ID on {}. Please run manually\n\n{}".format(proxy,e)


if __name__ == "__main__":
    env = "prod"
    #env = "stgn"
    mongo = eval("mongo_{}".format(env))
    mongo_cnx_url = "mongodb://{}:{}@{},{}:{}/?replicaSet={}".format(mongo["mongo_user"], mongo["mongo_pwd"], mongo["mongo_url1"], mongo["mongo_url2"], mongo["mongo_port"], mongo["mongo_replica_set"])
    client = MongoClient(mongo_cnx_url)
    db = client[mongo["mongo_db"]]

    '''query = {'contract.purchasedComponents.extend' : False }
    cursor = db.customer.find(query)
    for doc in cursor:
        print "ID: {} - Name: {} - CapPeriod: {} - allowancePeriod: {}".format(doc["_id"],doc["name"],doc["globalPolicy"]["local"]["capPeriod"]["value"],doc["globalPolicy"]["local"]["allowancePeriod"]["value"])'''

    customers = []
    with open(sys.argv[1], "r") as input_file:
        for line in input_file:
            customers.append(line.rstrip())
    query = {'_id' : { '$in' : customers}}
    cursor = db.customer.find(query)
    for doc in cursor:
        try:
            created = doc["creationTime"]
            created_date = datetime.fromtimestamp(created/1000).strftime('%Y-%m-%d')
            updated = doc["lastModifiedTime"]
            updated_date = datetime.fromtimestamp(updated/1000).strftime('%Y-%m-%d')
            print "ID: {}, Name: {}, Created: {}, Modified: {}, CapPeriod: {}, allowancePeriod: {}".format(doc["_id"],doc["name"],created_date,updated_date,doc["globalPolicy"]["local"]["capPeriod"]["value"],doc["globalPolicy"]["local"]["allowancePeriod"]["value"])
            #print "ID: {}, Name: {}, Created: {}, Modified: {}, CapPeriod: {}, allowancePeriod: {}".format(doc["_id"],doc["name"],created,updated,doc["globalPolicy"]["local"]["capPeriod"]["value"],doc["globalPolicy"]["local"]["allowancePeriod"]["value"])
        except:
            print "Failed to get fields for {}".format(doc["_id"])

    '''#customers_list = ['4c59fcb3-0790-4383-b7f8-f83dddf8e87f','e07741c6-eacf-4184-93ef-b95211be2bb2','4566e508-9fb9-4a10-bf0c-0c7e0569c315']
    customers_list = ['9e3135b3-57d5-4d95-b2da-4c8e8133cda5']
    pmc, pm_api_key, pm_api_secret = init_proxy_manager(env)
    update_proxies(env,customers_list,False)'''
