#!/usr/local/bin/python

import sys
import os
import json
import argparse
import shutil
import re
import appscript
import subprocess
from subprocess import Popen, PIPE
from pymongo import MongoClient
from fabric.operations import sudo
from fabric.context_managers import settings
from time import sleep
import fabfile

from wandera_client.client.service.proxy_manager import ProxyManagerClient
from wandera_client.resource import Server
from wandera_client.client.auth import ServiceAuthentication, BaseAuthentication

current_dir = os.getcwd()
home = os.path.expanduser('~')
CONFIG_FILE = os.path.join(home, ".woopas.json")
bitbucket_dir = os.path.join("/Users","arturonoha","Documents","bitbucket")

puppet_master = "puppet.snappli.net"
ssh_user = "anoha"

mongo_intg = {
    'mongo_url1' : "mdb-101-dev.eu-west-1a.ie.wandera.biz",
    'mongo_url2' : "mdb-201-dev.eu-west-1b.ie.wandera.biz",
    'mongo_replica_set' : "rs20007",
    'mongo_port' : 20007,
    'mongo_user' : "admin",
    'mongo_pwd' : "napa6ruK",
    'mongo_db' : "wandera_intg_eu_2"
}

mongo_prod = {
    'mongo_url1' : "mdb-101-core.eu-west-1a.ie.wandera.com",
    'mongo_url2' : "mdb-201-core.eu-west-1b.ie.wandera.com",
    'mongo_replica_set' : "rs22000",
    'mongo_port' : 22000,
    'mongo_user' : "admin",
    'mongo_pwd' : "Pr4DrUya",
    'mongo_db' : "wandera_live_ent_eu"
}

mongo_stgn = {
    'mongo_url1' : "mdb-101-dev.eu-west-1a.ie.wandera.biz",
    'mongo_url2' : "mdb-201-dev.eu-west-1b.ie.wandera.biz",
    'mongo_replica_set' : "rs20004",
    'mongo_port' : 20004,
    'mongo_user' : "admin",
    'mongo_pwd' : "napa6ruK",
    'mongo_db' : "snappli_stgn_eu_2"
}

env_mapping = {
"stgn" : "staging",
"intg" : "integration",
"prod" : "production"
}

node_env_mapping = {
"stgn" : "staging",
"intg" : "integration",
"prod" : "live"
}

mco_roles = [ "proxy" , "app" , "portal" ]

def init_proxy_manager(env):
    with open(CONFIG_FILE, "r") as config_file:
        config = config_file.read().rstrip()

    try:
        config_json = json.loads(config)
        pm_url = config_json["proxy_manager"][env]["proxy_manager_url"]
        #pm_url = "http://54.246.241.216:1999/"
        pm_api_key = config_json["proxy_manager"][env]["service_api_key"]
        pm_api_secret = config_json["proxy_manager"][env]["service_api_secret"]
        pm_auth = ServiceAuthentication(pm_api_key, pm_api_secret)
        #pm_auth = BaseAuthentication(pm_api_key, pm_api_secret)
        pmc = ProxyManagerClient(pm_url,auth=pm_auth)
    except Exception as e:
        print "ERROR: Failed to load ProxyManagerClient\n({})".format(e.message)
        return None

    return pmc

def _get_input(message = None):
    if message is None:
        display_message = "Would you wish to continue? [Y/n]: "
    else:
        display_message = message + " [Y/n]: "

    option = raw_input(display_message)
    while option not in [ 'Y' , 'n' ]:
        option = raw_input("Please enter a valid option [Y/n]: ")

    return option

def parse_options(options):
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', dest = "customers_file")
    parser.add_argument('--env', dest = "env")
    parser.add_argument('--branch', dest = "git_branch")
    parser.add_argument('--dryrun', dest = "dryrun", default = True)

    return parser.parse_args()

def usage():
    print "\nUSAGE: {} --file <customers_list> --env <stgn / intg / prod>\n".format(sys.argv[0])
    sys.exit(1)

def check_proxies(env, customers_list):
    query = { '_id' : { '$in' : customers_list } }
    cursor = db.customer.find(query)
    assumeid_sleep = 30

    proxies = []
    for document in cursor:
        print "{}".format(document["_id"])
        for proxy in document['proxies']:
            if proxy not in proxies:
                proxies.append(proxy)

    if len(proxies) == 0:
        print "\nWARN: No proxies found for these customers:\n{}\n".format(customers_list)
    else:
        pmc = init_proxy_manager(env)
        #pmc = None
        if pmc is None:
            print "\n* ERROR: Failed to rget PM driver. Proxies to check"
            for proxy in proxies:
                print "{}".format(proxy)
            if _get_input() != "Y":
                sys.exit("** Aborted by user **")
        else:
            customers_list_str = customers_list[0]
            for i in range(1,len(customers_list)):
                customers_list_str = "{},{}".format(customers_list_str, customers_list[i])
            for proxy in proxies:
                try:
                    pm_proxy = ProxyManagerClient.node_by_host(pmc, proxy)
                    print "Checking FF in {} ({} - {})\n".format(pm_proxy["nodeName"], proxy, pm_proxy["ip"])
                    with settings(host_string=pm_proxy["ip"], user=ssh_user):
                        fabfile.check_proxy_ff(customers_list)

                    print "\nChecking logs in {} ({} - {})\n".format(pm_proxy["nodeName"], proxy, pm_proxy["ip"])
                    with settings(host_string=pm_proxy["ip"], user=ssh_user):
                        fabfile.check_customerid_proxy_logs(customers_list)
                    print "\n++++++++++++++++++++++++++++++++++++++++++++++++++\n"
                except Exception as e:
                    print "\n* ERROR: Failed to check {}. *** Please run manually ***\n({})\n".format(proxy, e)


def new_window(active_svr, user, customers_list):
    active_cmd = "cd {}; fab -H {} -u {} tail_proxy_logs:{}".format(current_dir, active_svr, user, customers_list)
    appscript.app('Terminal').do_script(active_cmd)

if __name__ == "__main__":
    ## PARSE AND VALIDATE INPUT ARGUMENTS
    options = parse_options(sys.argv[1:])

    # Verify that a file with the list of customers is provided and it exists
    if options.customers_file is None:
        usage()
    customers_file = os.path.abspath(options.customers_file)
    if not os.path.isfile(customers_file):
        sys.exit("\nERROR: {} not found\n".format(customers_file))

    # Validate the environment provided
    if options.env is None or options.env not in [ 'stgn','intg','prod']:
        usage()
    env = options.env

    # Initialise the mongodb instance based on the environment
    mongo = eval("mongo_{}".format(env))
    mongo_cnx_url = "mongodb://{}:{}@{},{}:{}/?replicaSet={}".format(mongo["mongo_user"], mongo["mongo_pwd"], mongo["mongo_url1"], mongo["mongo_url2"], mongo["mongo_port"], mongo["mongo_replica_set"])
    client = MongoClient(mongo_cnx_url)
    db = client[mongo["mongo_db"]]

    # Get the list of customers to migrate from the input file
    customers_list = []
    with open(customers_file, "r") as myfile:
        for line in myfile:
            customers_list.append(line.rstrip())

    check_proxies(env, customers_list)
    sys.exit(0)
