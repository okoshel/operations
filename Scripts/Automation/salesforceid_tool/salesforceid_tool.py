import string
import sys
import csv
import os
import argparse
import logging
import logging.handlers
from mongoengine import Document, connect, BooleanField, StringField, DictField, DynamicField, IntField  # pylint: disable=import-error,line-too-long
from mongoengine.connection import _get_db, connect


################
# LOAD LOGGING #
################

F = logging.Formatter(fmt='%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
                      datefmt="%Y-%m-%d %H:%M:%S")

HANDLERS = [
    logging.handlers.RotatingFileHandler('SalesForceID_Tool.log',
                                         encoding='utf-8',
                                         maxBytes=100000, backupCount=1),
    logging.StreamHandler()
]

ROOT_LOGGER = logging.getLogger()
ROOT_LOGGER.setLevel(logging.DEBUG)
for h in HANDLERS:
    h.setFormatter(F)
    h.setLevel(logging.DEBUG)
    ROOT_LOGGER.addHandler(h)


####################
# SETUP ARG PARSER #
####################

PARSER = argparse.ArgumentParser(
    description='Script Command Line Arguments For salesforceid script')
PARSER.add_argument('-import_csv_file', help='This is the Customer or Account list that is exported from SalesForce.',
                    type=str, nargs='?')
PARSER.add_argument('-export_csv_file', help='This is an output in the format of Customer or Account list with any modifications made.',
                    type=str, nargs='?')
PARSER.add_argument('-fi', help='Read Csv File input', action='store_true')
PARSER.add_argument('-fo', help='CSV file output', action='store_true')
PARSER.add_argument('-step', help='Step SalesforceId forward from 15 Chars to 18 Chars.', action='store_true')
PARSER.add_argument('-back', help='Step SalesforceId back from 18 Chars to 15 Chars', action='store_true')
PARSER.add_argument('-updatedb', help='Update Mongo Database with new format of sfid.', action='store_true')
PARSER.add_argument('--dryrun', help='Dry Run Only - Display to stdout only', action='store_true')
ARGS = PARSER.parse_args()


# Bin lookup for correcting salesforeid from 15 to 18 chars
bin_lookup = {
    '00000':'A',
    '00001':'B',
    '00010':'C',
    '00011':'D',
    '00100':'E',
    '00101':'F',
    '00110':'G',
    '00111':'H',
    '01000':'I',
    '01001':'J',
    '01010':'K',
    '01011':'L',
    '01100':'M',
    '01101':'N',
    '01110':'O',
    '01111':'P',
    '10000':'Q',
    '10001':'R',
    '10010':'S',
    '10011':'T',
    '10100':'U',
    '10101':'V',
    '10110':'W',
    '10111':'X',
    '11000':'Y',
    '11001':'Z',
    '11010':'0',
    '11011':'1',
    '11100':'2',
    '11101':'3',
    '11110':'4',
    '11111':'5'
}

class AdditionalMethods(object):

    @classmethod
    def aggregate(cls, pipeline):
        """
        Perform an aggregation using the aggregation framework on this collection.

        :param cls:
        :param pipeline: a single command or list of aggregation commands
        :return: PyMongo stuff (dict with "ok" and "result"...)
        """
        collection = _get_db()[cls._get_collection_name()]  # pylint: disable=no-member
        return collection.aggregate(pipeline)

    @classmethod
    def group(cls, key, condition, initial, reduce_f, finalize_f=None):  # pylint: disable=too-many-arguments
        """
        Perform a query.

        :param key: fields to group by
        :param condition: specification of rows to be considered
                          (as a :meth:`find` query specification)
        :param initial: initial value of the aggregation counter object
        :param reduce_f: aggregation function as a JavaScript string
        :param finalize_f: function to be called on each object in output list
        :return: an array of grouped items
        """
        collection = _get_db()[cls._get_collection_name()]  # pylint: disable=no-member
        return collection.group(key, condition, initial, reduce_f, finalize_f)


class Customer(AdditionalMethods, Document):  # pylint: disable=too-few-public-methods
    meta = {"collection": "customer",
            "strict": False}
    id = StringField(primary_key=True, required=True)
    name = StringField()
    salesForceId = StringField()


def connect_to_db(envname, dbhost, dbuname, dbpasswd, reps):
    try:
        db_conn = connect(envname,
                          host='mongodb://' + dbhost,
                          username=dbuname,
                          password=dbpasswd,
                          replicaSet=reps)
    except Exception as err:
        print err
        logging.error('Error connecting to database {} due to {}'
                      .format(dbhost, err))
        return False
    return db_conn


def shorten_sf_id(ipsfid):

    if len(ipsfid) != 18:
        logging.error('SalesForceId entered is not 18 chars, cannot step back. {0}'.format(ipsfid))
    return ipsfid[:-3]


def sfid_file_import(file):
    data = []
    with open(file, 'rU') as csvfile:
        csvexportreader = csv.reader(csvfile, delimiter=',')
        csvexportreader.next()
        for row in csvexportreader:
            data.append(row)
    return data

# def sfid_file_export(file):


def sfid_file_io(file, iot, data):

    if iot == 'import':
        return sfid_file_import(file)
    elif iot == 'export':
        return sfid_file_export(file)
    else:
        logging.error('You have called sfid file io but not chosen to import/export.')
        sys.exit(0)

# TODO: Add in optional arg if want to change column number of where sfid is present
def process_sfid_bulk(sfcsv, exp):

    sfid_list = []
    for l in sfcsv:
        if exp:
            lapp = expand_sf_id(l[2])
            sfid_list.append(lapp)
        else:
            try:
                lapp = shorten_sf_id(l[2])
                sfid_list.append(lapp)
            except IndexError as e:
                logging.warning('The processed line is missing a sfid col.')
    return sfid_list


def sf_replace(incoming_id_char):

    if incoming_id_char in string.ascii_uppercase:
        return '1'
    else:
        return '0'


def expand_sf_id(incoming_sf_id):

    if len(incoming_sf_id) != 15:
        raise ValueError('id string must be exactly 15 characters long')

    # split into list of 3 5-character chunks
    id_chunks = map("".join, zip(*[iter(incoming_sf_id)]*5))

    suffix = ''

    for id_chunk in id_chunks:
        # replace all capital letters with 1, non cap letters with 0
        lookup_components = [sf_replace(id_char) for id_char in id_chunk]
        # make it string and reverse it
        lookup_chunk = "".join(lookup_components)[::-1]
        # get the letter from the lookup table based on the reversed string
        bin_replacement = bin_lookup[lookup_chunk]
        suffix += bin_replacement

    #add the suffix to the origional id
    expanded_sf_id = incoming_sf_id + suffix

    return expanded_sf_id


def update_cust_documents(sfidl, dr):

    if dr:
        for id in sfidl:
            customers = Customer.objects(salesForceId=id)
            newid = expand_sf_id(id)
            for customer in customers:
                # Python 2.7.x logging module unicode bug funs (fuck off)
                try:
                    logging.info('Dry Run - Would of Updated Customer {0} from sfid {1} to sfid {2}.'.format(customer['name'].encode('utf-8'), id, newid))
                except UnicodeDecodeError as e:
                    logging.info('Dry Run - Would of Updated Customer {0} from sfid {1} to sfid {2}.'.format(customer['name'].encode('utf-8'), id, newid))
    else:
        for id in sfidl:
            customers = Customer.objects(salesForceId=id)
            newid = expand_sf_id(id)
            for customer in customers:
                Customer.objects(id=customer['id']).update_one(set__salesForceId=newid)
                logging.info('Updating Cust Record with id {0} sfid to {1}'.format(customer['id'], newid))


def main():
    # DB Variables
    DB_ENV = os.environ['SFID_DB_ENV']
    DB_HOST = os.environ['SFID_DB_HOST']
    DB_UNAME = os.environ['SFID_DB_UNAME']
    DB_PWD = os.environ['SFID_DB_PWD']
    DB_RS = os.environ['SFID_DB_RS']

        # Check args before continuing
    if ARGS.fi and ARGS.import_csv_file is None:
        logging.error('You have passed the file input flag but -import_csv_file is empty.')
        sys.exit(0)

    if ARGS.fo and ARGS.export_csv_file is None:
        logging.error('You have passed the file export flag but -export_csv_file is empty.')
        sys.exit(0)

    logging.info('Connecting to Database.....')
    DB_CLIENT = connect_to_db(DB_ENV, DB_HOST, DB_UNAME, DB_PWD, DB_RS)
    if DB_CLIENT is None:
        logging.error('Cannot Continue With DB Work - Error in Logs')
        sys.exit(0)
    DB_CLIENT.admin.authenticate(DB_UNAME, DB_PWD)
    # Check if this is an import task first and get the data if it is.
    if ARGS.fi:
        sf_list = sfid_file_io(file=ARGS.import_csv_file, iot='import', data=None)

    # Get imported salesforceID's and shorten if they are passed as 18chars
    if ARGS.back:
        processsfidlist = process_sfid_bulk(sfcsv=sf_list, exp=False)
    # Update Mongo Backend Documents if selected as arg
    if ARGS.updatedb:
        update_cust_documents(sfidl=processsfidlist, dr=ARGS.dryrun)


if __name__ == '__main__':
    main()
