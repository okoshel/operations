# SalesForceId Python Tool

A SalesForceID tool for stepping forward and back sfid depending on what actions need to be taken. Option to update Mongodb with changes as well. WIP.


Cmd Line Options:

```
usage: salesforceid_tool.py [-h] [-import_csv_file [IMPORT_CSV_FILE]]
                            [-export_csv_file [EXPORT_CSV_FILE]] [-fi] [-fo]
                            [-step] [-back] [-updatedb] [--dryrun]

Script Command Line Arguments For salesforceid script

optional arguments:
  -h, --help            show this help message and exit
  -import_csv_file [IMPORT_CSV_FILE]
                        This is the Customer or Account list that is exported
                        from SalesForce.
  -export_csv_file [EXPORT_CSV_FILE]
                        This is an output in the format of Customer or Account
                        list with any modifications made.
  -fi                   Read Csv File input
  -fo                   CSV file output
  -step                 Step SalesforceId forward from 15 Chars to 18 Chars.
  -back                 Step SalesforceId back from 18 Chars to 15 Chars
  -updatedb             Update Mongo Database with new format of sfid.
  --dryrun              Dry Run Only - Display to stdout only
```

Example Usage:

``python salesforceid_tool.py -fi -back --dryrun -updatedb -import_csv_file "one_record_update.csv"``
