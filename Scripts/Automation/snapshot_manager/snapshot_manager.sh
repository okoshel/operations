#!/bin/bash -x
# EBS Snapshot volume management script
# Created by Gerric Chaplin.
# Copyright (c) 2012 Gerric Chaplin. All rights reserved.
# Features to come: 
# Multiple volume bakups
# email reporting of snapshots with retention period and interval 
# Once above is done then report if there are missing snapshots.

[ -f /etc/profile ] && . /etc/profile

[ -f ~/.bashrc ] && . ~/.bashrc

#========================================#
#== AWS EBS Volume Snapshot Automation ==#
#========================================#

#-------------------#
#-- Configuration --#
#-------------------#

Retention_Period="7"
ec2_private_key=${EC2_PRIVATE_KEY}
ec2_cert=${EC2_CERT}
verbose_output="1"
aws_region="eu-west-1"

#=======================#
#== Script Code Below ==#
#=======================#

if [ $verbose_output -eq "1" ]; then    
	echo "The retention period is $Retention_Period"
	echo "EC2 Private Key location: $ec2_private_key"
	echo "EC2 Certificate location: $ec2_cert"
fi

#---- FUNCTION --------------------------------------------------------------------#
# NAME: define_dates
# DESCRIPTION: Redefines the retention date in Unix time/POSIX time
# PARAMATER 1: Retention_Period
#----------------------------------------------------------------------------------#

function calculate_dates() {
	if [ -z $Retention_Period ]; then
		echo "The retention period has not been set."
		exit 1
	else
		date_seven_days_ago=`date +%Y-%m-%d --date '7 days ago'`				
		unixtime_seven_days_ago=`date --date "$date_seven_days_ago" +%s`
		unixtime_now=`date +%s`

		if [ $verbose_output -eq "1" ]; then	
			echo "The date seven days ago was: $date_seven_days_ago"
			echo "The unixtime timestamp seven days ago was: $unixtime_seven_days_ago"
		fi
	fi
}

#---- FUNCTION --------------------------------------------------------------------#
# NAME: get_detailed_volume_list
# DESCRIPTION: Get a list of instances from EC2 region (including names) and check 
# if they should have their volumes snapshot using instance tags.
# PARAMATER 1: aws_region
#----------------------------------------------------------------------------------#

function get_detailed_snapshot_list() {
        if [ -z $aws_region ]; then
                echo "The AWS region has not been set."
        else

	calculate_dates
	
	aws_region="$1"

	INSTANCES=""
	INSTANCES_COUNT="0"
	for instance in `ec2-describe-instances --region $aws_region --filter 'tag:snapshot=yes' | grep INSTANCE | awk '{ print $2 }'`; do 
		instance_id[$INSTANCES_COUNT]=$instance
		instance_name[$INSTANCES_COUNT]=`ec2-describe-instances --region $aws_region $instance | grep Name | awk '{ print $5 }'`
		snapshot_interval[$INSTANCES_COUNT]=`ec2-describe-instances --region $aws_region $instance | grep snapshot_interval | awk '{ print $5 }'`
		snapshot_retention[$INSTANCES_COUNT]=`ec2-describe-instances --region $aws_region $instance | grep snapshot_retention | awk '{ print $5 }'`
		volume_name[$INSTANCES_COUNT]=`ec2-describe-volumes --region $aws_region --filter "attachment.instance-id=${instance_id[${INSTANCES_COUNT}]}"|grep VOLUME| awk '{ print $2}'`
                if [ $verbose_output -eq "1" ]; then
                        echo "The region you have chosen is: $aws_region"
                        echo "You have the following instance tagged to be snapshot in $aws_region: $INSTANCES"
			echo "This instance is instance number: $INSTANCES_COUNT"
			echo "This is instance ${instance_id[${INSTANCES_COUNT}]} which is called ${instance_name[${INSTANCES_COUNT}]}, it uses the volume ${volume_name[${INSTANCES_COUNT}]} and is backed up ${snapshot_interval[${INSTANCES_COUNT}]}. You want to keep ${snapshot_retention[${INSTANCES_COUNT}]} snapshots of this volume."
			echo
                fi
		let "INSTANCES_COUNT++"
	done
        fi
}

#---- FUNCTION --------------------------------------------------------------------#
# NAME: create_snapshot
# DESCRIPTION: create a snapshot for each of the Instances in the instance list 
#----------------------------------------------------------------------------------#

function create_snapshot() {
	get_detailed_snapshot_list $1

        if [ ${#instance_id[@]} -eq 0 ]; then
                echo "There are no Instances to snapshot in this region."
        else
		
	
	VOLUME_COUNT=${#instance_id[@]}
	for (( v=0; v<${VOLUME_COUNT}; v++ )); do
		snapshot_description[$v]="backup_${instance_name[${v}]}_`date +%Y-%m-%d_%H`_${unixtime_now}"
		snapshot_id[$v]=`ec2-create-snapshot --region $aws_region -d ${snapshot_description[${v}]} ${volume_name[${v}]}|grep SNAPSHOT | awk '{ print $2 }'`
                if [ $verbose_output -eq "1" ]; then
			echo "The snapshot is called $snapshot_description"
                        echo "The script will generate a snapshot of instance ${instance_id[${v}]} which is called ${instance_name[${v}]} and uses the volume ${volume_name[${v}]}."
                        echo "The snapshot ID is ${snapshot_id[${v}]} and has a description of ${snapshot_description[${v}]}."
			echo
                fi
        done
	unset instance_id
        fi
}

#---- FUNCTION --------------------------------------------------------------------#
# NAME: snapshot_cleanup
# DESCRIPTION: Remove any snapshot older than its retention period.
#----------------------------------------------------------------------------------#

function snapshot_cleanup() {
	aws_region="$1"
	if [ -z $aws_region ]; then
		echo "The AWS region has not been set."
        else
        for instance in `ec2-describe-instances --region $aws_region --filter 'tag:snapshot=yes' | grep INSTANCE | awk '{ print $2 }'`; do
		instance_id=$instance
                instance_name=`ec2-describe-instances --region $aws_region $instance | grep Name | awk '{ print $5 }'`
                snapshot_interval=`ec2-describe-instances --region $aws_region $instance | grep snapshot_interval | awk '{ print $5 }'`
                snapshot_retention=`ec2-describe-instances --region $aws_region $instance | grep snapshot_retention | awk '{ print $5 }'`
                volume_name=`ec2-describe-volumes --region $aws_region --filter "attachment.instance-id=${instance_id}"|grep VOLUME| awk '{ print $2}'`
			for snap in `ec2-describe-snapshots --region eu-west-1 --filter "description=*${instance_name}*" | grep SNAPSHOT | awk '{ print $9 }'`;do 
				snapshot_unixtime=`echo ${snap} | cut -d "_" -f 5`
				current_unixtime=`date +%s`
				if [ $snapshot_interval == "hourly" ]; then
					interval="3600"		
					snapshot_age=$((current_unixtime - snapshot_unixtime))
						if [ $snapshot_age -gt $interval ]; then
							snapshot_retention_period=$((interval * snapshot_retention))
								if [ $snapshot_age -gt $snapshot_retention_period ]; then
									echo "At this stage I would delete this $snap due to it being older than the $snapshot_retention_period. Age: $snapshot_age Snap: $snapshot_unixtime"
									snapshot_id=`ec2-describe-snapshots --region $aws_region --filter "description=${snap}" | grep SNAPSHOT | awk '{ print $2 }'`
									ec2-delete-snapshot --region $aws_region $snapshot_id
								fi
						fi
					
				elif [ $snapshot_interval == "daily" ]; then
					interval="86400"		
					snapshot_age=$((current_unixtime - snapshot_unixtime))
						if [ $snapshot_age -gt $interval ]; then
							snapshot_retention_period=$((interval * snapshot_retention))
								if [ $snapshot_age -gt $snapshot_retention_period ]; then
									echo "At this stage I would delete this $snap due to it being older than the $snapshot_retention_period. Age: $snapshot_age Snap: $snapshot_unixtime"
									snapshot_id=`ec2-describe-snapshots --region $aws_region --filter "description=${snap}" | grep SNAPSHOT | awk '{ print $2 }'`
									ec2-delete-snapshot --region $aws_region $snapshot_id
								fi
						fi
					
				elif [ $snapshot_interval == "monthly" ]; then
			                date_month_ago=`date +%Y-%m-%d --date '1 Month ago'`
					interval=`date --date "$date_month_ago" +%s`
					snapshot_age=$((current_unixtime - snapshot_unixtime))
						if [ $snapshot_age -gt $interval ]; then
							snapshot_retention_period=$((interval * snapshot_retention))
								if [ $snapshot_age -gt $snapshot_retention_period ]; then
									echo "At this stage I would delete this $snap due to it being older than the $snapshot_retention_period. Age: $snapshot_age Snap: $snapshot_unixtime"
									snapshot_id=`ec2-describe-snapshots --region $aws_region --filter "description=${snap}" | grep SNAPSHOT | awk '{ print $2 }'`
									ec2-delete-snapshot --region $aws_region $snapshot_id
								fi
						fi

				elif [ $snapshot_interval == "yearly" ]; then
			                date_year_ago=`date +%Y-%m-%d --date '1 Year ago'`
					interval=`date --date "$date_year_ago" +%s`
					snapshot_age=$((current_unixtime - snapshot_unixtime))
						if [ $snapshot_age -gt $interval ]; then
							snapshot_retention_period=$((interval * snapshot_retention))
								if [ $snapshot_age -gt $snapshot_retention_period ]; then
									echo "At this stage I would delete this $snap due to it being older than the $snapshot_retention_period. Age: $snapshot_age Snap: $snapshot_unixtime"
									snapshot_id=`ec2-describe-snapshots --region $aws_region --filter "description=${snap}" | grep SNAPSHOT | awk '{ print $2 }'`
									ec2-delete-snapshot --region $aws_region $snapshot_id
								fi
						fi
				fi
	
                		if [ $verbose_output -eq "1" ]; then
                       	 		echo "The region you have chosen is: $aws_region"
                       	 		echo "The unixtime on the snapshot is: $snapshot_unixtime"
                        		echo
                		fi
        		done
        done
        fi
}

case $1 in
	cleanup)
		if [ $# -eq 1 ]; then
                        echo "You did not provide a region"
                        exit 1
                else
			snapshot_cleanup $2	
                fi
	;;
	snapshot)
		if [ $# -eq 1 ]; then
			echo "You did not provide a region"
			exit 1
		else
			if [ "$2" == "all" ]; then
				for region in eu-west-1 sa-east-1 us-east-1 ap-northeast-1 us-west-2 us-west-1 ap-southeast-1; do
					create_snapshot $region
				done
			else
				create_snapshot $2
			fi
		fi
	;;
	*)
		echo "Usage: command: {cleanup|snapshot} region: {all|eu-west-1}"
		exit 1
	;;
esac

