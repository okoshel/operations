#!/bin/bash

# Enable debug if you want to see some output to the screen when testing.
DEBUG=0

# Set the hostname of the server you are testing from. This determines the location in Graphite. Should be the inversed hostname.
HOST="test.host"

# The proxy you want to test through.
PROXY=$1

PORT=$2

# The user you want to use. This is the Guid of the user you will be using.
USER=$3

# The full URL of the request you want to test.
URL=$4

# The label you want to give this test (Google,Facebook etc). 
LABEL=$5

ProxyUser="RabbDev"
ProxyPassword="RabbDevTest"
DIRECT=`(time phantomjs testProxy.js "$URL") 2>&1 | grep real | sed -e 's/.*m\(.*\)s/\1/'`
CACHE_CLEAR_RESULT=$(curl -s http://${ProxyUser}:${ProxyPassword}@${PROXY}:9666/ProxyCallback?guid=$USER)

if [ $DEBUG -eq 1 ]; then
echo $CACHE_CLEAR_RESULT
fi

PROXY=`(time phantomjs --proxy "$PROXY:$PORT" testProxy.js "$URL") 2>&1 | grep real | sed -e 's/.*m\(.*\)s/\1/'`

if [ $DEBUG -eq 1 ]; then
echo "$5 direct: $DIRECT"
echo "$5 proxied: $PROXY"
fi

DATE=`date +%s`
echo "${HOST}.latency.${LABEL}.direct $DIRECT $DATE" | nc graphite.snappli.net 2003 
echo "${HOST}.latency.${LABEL}.proxy $PROXY $DATE" | nc graphite.snappli.net 2003