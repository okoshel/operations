#!/bin/bash

USER=$1
SERVICE=$2

if [ -z "$USER" ]; then
	echo "deploy_snapshot.sh :: Invalid user specified '$USER'";
	exit 1;
fi

if [ -z "$SERVICE" ]; then
	echo "deploy_snapshot.sh :: Invalid service specified '$SERVICE'";
	exit 1;
fi

sudo tar xfvz *.tar.gz

DIR_NAME=`ls -la "$PWD" | grep '^d' | grep -v "\.$" | awk '{print $9}'`

sudo chown -R "$USER:$USER" "$DIR_NAME"
sudo rm "/opt/$SERVICE"
sudo ln -s "$PWD/$DIR_NAME" "/opt/$SERVICE"
sudo service $SERVICE restart &

