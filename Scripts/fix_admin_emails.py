import json
from collections import defaultdict
from functools import partial

from pymongo import MongoClient

client = MongoClient(
    'mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000',
    replicaset='rs22000',
    readPreference='secondaryPreferred'
)
client.admin.authenticate('', '')
db = client.wandera_live_ent_eu


# Arturs special format
def read_csv():
    with open('customer_collection_backup.csv') as f:
        for line in f:
            data = line.split(';')
            yield data[1].strip(), json.loads('{' + data[2].strip() + '}')


def update_category(categories, recipients, category_name):
    category = next((c for c in categories if c['category'] == category_name), None)
    category['recipients'] = list(set(category['recipients'] + recipients[category_name]))


recipient_map = defaultdict(dict)
for line in read_csv():
    recipient_map[line[0]].update(line[1])


for customer in db.customer.find({}, {'settings.notificationSettings.categories': 1}):
    print("EXISTING DOCUMENT:", customer)
    if customer['_id'] not in recipient_map:
        print("NOT UPDATING, CUSTOMER IS PROBABLY NEW")
        continue
    update = partial(update_category, customer['settings']['notificationSettings']['categories'], recipient_map[customer['_id']])
    update('SECURITY')
    update('MOBILE_DATA')
    update('SERVICE_MANAGEMENT')
    print("UPDATED DOCUMENT:", customer)
    db.customer.update({'_id': customer['_id']}, {'$set': {"settings.notificationSettings.categories": customer['settings']['notificationSettings']['categories']}})
