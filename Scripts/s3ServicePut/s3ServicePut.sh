#!/bin/bash
# requires: mailutils and s3cmd: http://s3tools.org/debian-ubuntu-repository-for-s3cmd

# Email to send errors
mail=ops-notify@snappli.com

# Get the service we want to put logs for
service=$1

# Get yesterdays date as we need this to reference the file
date_yesterday=`date +%Y-%m-%d -d "yesterday"`

# Point to the file we should back up for easy refences later
file="/opt/${service}service/logs/${service}-service.log.${date_yesterday}.gz"

# Get a MD5 so we can check the upload later on.
fileMD5=`md5sum ${file}|awk '{ print $1 }'`
echo $fileMD5

# Get the short version of the hostname to allow for naming files
hostname=`hostname --short`

# Lets perform the S3 put for the service.
s3cmd --no-progress put --guess-mime-type $file s3://snappli.logs.${service}.service/${hostname}-${file}

# Get the MD5 of the file we uploaded so we can check if it matches the original.
s3FileMD5=`s3cmd --list-md5 ls s3://snappli.logs.user.service/${hostname}-${file} | awk '{print $4}'`

if [ "$fileMD5" != "$s3FileMD5" ]; then
	echo "Upload did not work for s3://snappli.logs.${service}.service/${hostname}-${file}"| mail $mail -s "S3 log upload did not work on $hostname"
fi
