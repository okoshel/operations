#!/usr/bin/ruby1.8
# This script is used to load data from a S3 bucket into RedShift cluster.

require 'rubygems'
require 'getoptlong'
require 'pg'
require 'socket'

# Configure the required RedShift cluster details.
$host = "live-eu-west-1-a-proxylogs-1.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com"	# The IP or Cname of the Cluster. Cname Preferred.
$port = "5439"			# The port we want to connect to (5439)
$db = "loggingdata"	# The DB we need where I log table resides.
$user = "logging_data"		# The user which has access to the DB.
$password = ""	# The password for that user.
$log_table = "proxy_logs"	# The log table we want to perform queries on.
$S3bucket = "wandera-redshift-data-live-eu-west-1"	# The S3 bucket you want to load data from. 
$delimiter = "\t"

# Resolve the IP of the host so it can go into the pg connection string.
#$host = Socket::getaddrinfo("#$host", $port, nil, Socket::SOCK_STREAM)

# The pg connection string to use for the maintenance SQL commands.
$conn=PGconn.connect( :host=>$host, :port=>$port, :dbname=>$db, :user=>$user, :password=>$password)

def  load_data
    begin
	res = $conn.exec("copy #$log_table from 's3://#$S3bucket/redshift_data/' CREDENTIALS 'aws_access_key_id=AKIAIK6JMNSPU4AYDDWA;aws_secret_access_key=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' delimiter '#$delimiter' REMOVEQUOTES GZIP ESCAPE MAXERROR 100000 NULL as 'null' TRUNCATECOLUMNS;")
    rescue PG::Error => err
       $stderr.puts "%p performing data loading: %s" % [ err.class, err.message ]
        conn.reset
    end
end

opts = GetoptLong.new(
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
  [ '--load-data', '-l', GetoptLong::OPTIONAL_ARGUMENT ]
)

if ARGV.length != 1
  puts "Missing dir argument (try --help)"
  exit 1
end

opts.each do |opt, arg|
  case opt
    when '--help'
      puts "redshift_maintenance [OPTION]

-h, --help:
   show help

--load-data, -l:
  Loads in data from a S3 bucket to a DB using the same copy command as live.
"
    when '--load-data'
	load_data
  end
end
