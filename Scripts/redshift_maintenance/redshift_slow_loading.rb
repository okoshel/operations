#!/usr/bin/ruby1.8
# This script is used to load data from a S3 bucket into RedShift cluster.

require 'rubygems'
require 'getoptlong'
require 'pg'
require 'socket'

# Configure the required RedShift cluster details.
$host = "live-eu-west-1-a-proxylogs-1.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com"	# The IP or Cname of the Cluster. Cname Preferred.
$port = "5439"			# The port we want to connect to (5439)
$db = "loggingdata"	# The DB we need where I log table resides.
$user = "logging_data"		# The user which has access to the DB.
$password = ""	# The password for that user.
$log_table = "proxy_logs"	# The log table we want to perform queries on.
$S3bucket = "wandera-redshift-data-live-eu-west-1"	# The S3 bucket you want to load data from. 
$delimiter = "\t"

# Here comes the time conversion from epoch to redshift epoch (1/1/2000) in microsecond
$redshift_epoch = (Time.utc("2000,0,0,0,0,0")).strftime("%s")
$time_now = (Time.now).strftime("%s")
$time_past = (Time.now - 600).strftime("%s")

$redshift_now = (($time_now.to_i - $redshift_epoch.to_i)*1000000)
$redshift_past = (($time_past.to_i - $redshift_epoch.to_i)*1000000)

# Slow loading cutoff in seconds
$loading_cutoff = 2

# Convert the cutoff time to microseconds.
$loading_cutoff_mic = ($loading_cutoff.to_i * 1000000)

# The pg connection string to use for the maintenance SQL commands.
$conn=PGconn.connect( :host=>$host, :port=>$port, :dbname=>$db, :user=>$user, :password=>$password)

def  load_data
    begin
	res = $conn.exec("select count(*) from  stl_s3client where http_method='GET' and start_time >= '#$redshift_past' and end_time <= '#$redshift_now' and transfer_time >= '#$loading_cutoff_mic';")
	puts res.getvalue(0,0)
    rescue PG::Error => err
       $stderr.puts "%p performing stl_s3client query: %s" % [ err.class, err.message ]
        conn.reset
    end
end

opts = GetoptLong.new(
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
  [ '--check-slow-loading', '-s', GetoptLong::OPTIONAL_ARGUMENT ]
)

if ARGV.length != 1
  puts "Missing dir argument (try --help)"
  exit 1
end

opts.each do |opt, arg|
  case opt
    when '--help'
      puts "redshift_maintenance [OPTION]

-h, --help:
   show help

--load-data, -l:
  Loads in data from a S3 bucket to a DB using the same copy command as live.
"
    when '--check-slow-loading'
	load_data
  end
end
