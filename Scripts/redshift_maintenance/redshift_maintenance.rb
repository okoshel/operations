#!/usr/bin/ruby1.8
# This script is used to perform maintenance tasks on the RedShift cluster.

require 'rubygems'
require 'getoptlong'
require 'pg'
require 'net/smtp'
require 'socket'

# Email configuration
$from_addres = "ops-notify@wandera.com" # The email will come from this address
$to_address = "ops-notify@wandera.com" # The email should go to this address

# Configure the required RedShift cluster details.
$host = "live-eu-west-1-a-proxylogs-1.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com"	# The IP or Cname of the Cluster. Cname Preferred.
$port = "5439"			# The port we want to connect to (5439)
$db = "loggingdata"	# The DB we need where I log table resides.
$user = "logging_data"		# The user which has access to the DB.
$password = ''	# The password for that user.
$log_table = "proxy_logs"	# The log table we want to perform queries on.
$delete_field = "startutcinms"	# The field you want to use for row removal.

# Configure the retention period for records.
# The delete will remove all rows up to the time of the script running X days ago.
#So if you run this in the middle of the day you will end up losing half a days logs. Best to add an additional day incase you actually want that data from the last day.
retention_period_days = 366

# Perform calculations to provide millisecond time X days ago.
retention_period_s = (retention_period_days * 24 * 60 * 60)
delete_date = (Time.now - retention_period_s)
$delete_time_ms = delete_date.to_i * 1000

# Resolve the IP of the host so it can go into the pg connection string.
#$host = Socket::getaddrinfo("#$host", $port, nil, Socket::SOCK_STREAM)

# The pg connection string to use for the maintenance SQL commands.
#$conn=PGconn.connect( :hostaddr=>$host[0][3], :port=>$port, :dbname=>$db, :user=>$user, :password=>$password)
$conn=PGconn.connect( :host=>$host, :port=>$port, :dbname=>$db, :user=>$user, :password=>$password)

def send_mail(eclass,emessage,eaction)
message = <<MESSAGE_END
From: OPS Notify <#$from_address>
To: OPS Notify <#$to_address>
Subject: Error Running RedShift Maintenance Task
Action #{eaction}
Class #{eclass} 
Message #{emessage}
MESSAGE_END

	Net::SMTP.start('localhost') do |smtp|
  		smtp.send_message message, "#$from_address", "#$to_address"
	end
end

def  vacuum_sort
    begin
	startTime = Time.now.to_f
	res = $conn.exec("VACUUM SORT ONLY #$log_table;")
        endTime = Time.now.to_f
        queryTime = endTime - startTime
	currentTime = Time.now
        puts "Time: #{currentTime} Query Time: #{queryTime}"
    rescue PG::Error => err
#       $stderr.puts "%p performing vacuum sort: %s" % [ err.class, err.message ]
	send_mail(err.class, err.message,"vacuum sort")
        conn.reset
    end
end

def  vacuum_clean
    begin
	startTime = Time.now.to_f
        res = $conn.exec("VACUUM DELETE ONLY #$log_table;")
        endTime = Time.now.to_f
        queryTime = endTime - startTime
	currentTime = Time.now
        puts "Time: #{currentTime} Query Time: #{queryTime}"
    rescue PG::Error => err
#       $stderr.puts "%p performing vacuum cleanup: %s" % [ err.class, err.message ]
	send_mail(err.class, err.message,"vacuum delete")
        conn.reset
    end
end

def  vacuum
    begin
	startTime = Time.now.to_f
        res = $conn.exec("set wlm_query_slot_count to 3; VACUUM #$log_table;")
        endTime = Time.now.to_f
        queryTime = endTime - startTime
	currentTime = Time.now
        puts "Time: #{currentTime} Query Time: #{queryTime}"
    rescue PG::Error => err
#       $stderr.puts "%p performing vacuum: %s" % [ err.class, err.message ]
	send_mail(err.class, err.message,"vacuum")
        conn.reset
    end
end

def analyze
    begin
	startTime = Time.now.to_f
        res = $conn.exec("ANALYZE #$log_table;")
        endTime = Time.now.to_f
        queryTime = endTime - startTime
	currentTime = Time.now
        puts "Time: #{currentTime} Query Time: #{queryTime}"
    rescue PG::Error => err
#       $stderr.puts "%p performing analyze: %s" % [ err.class, err.message ]
        send_mail(err.class, err.message,"analyze")
        conn.reset
    end
end

def delete_rows
    begin
	startTime = Time.now.to_f
        res = $conn.exec("DELETE FROM #$log_table where #$delete_field < #$delete_time_ms;")
        endTime = Time.now.to_f
        queryTime = endTime - startTime
	currentTime = Time.now
        puts "Time: #{currentTime} Query Time: #{queryTime}"
    rescue PG::Error => err
#       $stderr.puts "%p delete of rows older than #$delete_time_ms: %s" % [ err.class, err.message ]
        send_mail(err.class, err.message,"delete rows")
        conn.reset
    end
end

opts = GetoptLong.new(
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
  [ '--vacuum-sort', '-s', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--vacuum-clean', '-c', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--vacuum', '-v', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--analyze', '-a', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--delete', '-d', GetoptLong::OPTIONAL_ARGUMENT ]
)

if ARGV.length != 1
  puts "Missing dir argument (try --help)"
  exit 1
end

opts.each do |opt, arg|
  case opt
    when '--help'
      puts "redshift_maintenance [OPTION]

-h, --help:
   show help

--vacuum-sort, -s:
  Performs a vacuum with sort only, to resort rows. 

--vacuum-clean, -c:
  Performs a vacuum with delete only to clear up space. 

--vacuum, -v:
  Performs a FULL vacuum does a sort and space cleanup. 

--analyze, -a:
  Performs an ANALYZE of the configured table. 

--delete, -d:
  Performs a delete of old date. 
"
    when '--vacuum-sort'
	vacuum_sort
    when '--vacuum-clean'
	vacuum_clean
    when '--vacuum'
	vacuum
    when '--analyze'
	analyze
    when '--delete'
	delete_rows
  end
end
