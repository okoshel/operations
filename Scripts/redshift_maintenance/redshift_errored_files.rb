#!/usr/bin/ruby1.8
# This script is used to load data from a S3 bucket into RedShift cluster.

require 'rubygems'
require 'getoptlong'
require 'pg'
require 'socket'

# Configure the required RedShift cluster details.
$host = "live-eu-west-1-a-proxylogs-1.ckhvkdjbgwco.eu-west-1.redshift.amazonaws.com"	# The IP or Cname of the Cluster. Cname Preferred.
$port = "5439"			# The port we want to connect to (5439)
$db = "loggingdata"	# The DB we need where I log table resides.
$user = "logging_data"		# The user which has access to the DB.
$password = ""	# The password for that user.
$log_table = "proxy_logs"	# The log table we want to perform queries on.
$S3bucket = "wandera-redshift-data-live-eu-west-1"	# The S3 bucket you want to load data from. 
$delimiter = "\t"

# Here comes the time conversion from epoch to redshift epoch (1/1/2000) in microsecond
$time_now = (Time.now).strftime("%Y-%m-%d %H:%M:%S")
$time_past = (Time.now - 60).strftime("%Y-%m-%d %H:%M:%S")

# The pg connection string to use for the maintenance SQL commands.
$conn=PGconn.connect( :host=>$host, :port=>$port, :dbname=>$db, :user=>$user, :password=>$password)

def  load_data
    begin
	res = $conn.exec("select count(*) from loadview where table_name ='#$log_table' and starttime > '#$time_past' and starttime < '#$time_now';")
	puts res.getvalue(0,0)
    rescue PG::Error => err
       $stderr.puts "%p performing errored lines query: %s" % [ err.class, err.message ]
        conn.reset
    end
end

opts = GetoptLong.new(
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
  [ '--check-loading-errors', '-e', GetoptLong::OPTIONAL_ARGUMENT ]
)

if ARGV.length != 1
  puts "Missing dir argument (try --help)"
  exit 1
end

opts.each do |opt, arg|
  case opt
    when '--help'
      puts "redshift_maintenance [OPTION]

-h, --help:
   show help

--check-loading-errors, -e:
  Count the number of errored lines in the past 1 minutes. 
"
    when '--check-loading-errors'
	load_data
  end
end
