from pymongo import MongoClient
import json

# PROD
client = MongoClient('mdb-101-core.eu-west-1a.ie.wandera.com:22000,mdb-201-core.eu-west-1b.ie.wandera.com:22000', replicaSet='rs22000')
client.wandera_live_ent_eu.authenticate('db_snappli_live_ent_eu','m31ngr03rD4t3n00')
db = client.wandera_live_ent_eu

customers = db.customer
groups = db.groups

customer_find_query = {'globalPolicy.local.blockList.valueRange':{ "$elemMatch": { "$in": ['akamaized.net','apple.com','googlevideo.com','apple.com','uplynk.com','comcast.net','mediafly.com','akamaihd.net','googleapis.com','icloud-content.com','hbogo.com','nflxvideo.net','hbonow.com','apple.com','cloudfront.net','akamaihd.net','tableau.com','amazonaws.com','apple.com'
] }}}
groups_find_query = { "$or": [
{'groupPolicy.local.blocks.value.additional':{ "$elemMatch": { "$in": ['akamaized.net','apple.com','googlevideo.com','apple.com','uplynk.com','comcast.net','mediafly.com','akamaihd.net','googleapis.com','icloud-content.com','hbogo.com','nflxvideo.net','hbonow.com','apple.com','cloudfront.net','akamaihd.net','tableau.com','amazonaws.com','apple.com'
]}}},
{'groupPolicy.local.blocks.value.exclude':{ "$elemMatch": { "$in": ['akamaized.net','apple.com','googlevideo.com','apple.com','uplynk.com','comcast.net','mediafly.com','akamaihd.net','googleapis.com','icloud-content.com','hbogo.com','nflxvideo.net','hbonow.com','apple.com','cloudfront.net','akamaihd.net','tableau.com','amazonaws.com','apple.com'
]}}},
{'groupPolicy.raoming.blocks.value.additional':{ "$elemMatch": { "$in": ['akamaized.net','apple.com','googlevideo.com','apple.com','uplynk.com','comcast.net','mediafly.com','akamaihd.net','googleapis.com','icloud-content.com','hbogo.com','nflxvideo.net','hbonow.com','apple.com','cloudfront.net','akamaihd.net','tableau.com','amazonaws.com','apple.com'
]}}},
{'groupPolicy.raoming.blocks.value.exclude':{ "$elemMatch": { "$in": ['akamaized.net','apple.com','googlevideo.com','apple.com','uplynk.com','comcast.net','mediafly.com','akamaihd.net','googleapis.com','icloud-content.com','hbogo.com','nflxvideo.net','hbonow.com','apple.com','cloudfront.net','akamaihd.net','tableau.com','amazonaws.com','apple.com'
]}}},
{'groupPolicy.wifi.blocks.value.additional':{ "$elemMatch": { "$in": ['akamaized.net','apple.com','googlevideo.com','apple.com','uplynk.com','comcast.net','mediafly.com','akamaihd.net','googleapis.com','icloud-content.com','hbogo.com','nflxvideo.net','hbonow.com','apple.com','cloudfront.net','akamaihd.net','tableau.com','amazonaws.com','apple.com'
]}}},
{'groupPolicy.wifi.blocks.value.exclude':{ "$elemMatch": { "$in": ['akamaized.net','apple.com','googlevideo.com','apple.com','uplynk.com','comcast.net','mediafly.com','akamaihd.net','googleapis.com','icloud-content.com','hbogo.com','nflxvideo.net','hbonow.com','apple.com','cloudfront.net','akamaihd.net','tableau.com','amazonaws.com','apple.com'
]}}}
]}

# Actually query:
customer_result = customers.find(customer_find_query, {"_id":False, 'globalPolicy.local.blockList.valueRange':True})
groups_result = groups.find(groups_find_query)

count_domains = {
	'akamaized.net': {'cust':0, 'groups':0},
	'apple.com': {'cust':0, 'groups':0},
	'googlevideo.com': {'cust':0, 'groups':0},
	'uplynk.com': {'cust':0, 'groups':0},
	'comcast.net': {'cust':0, 'groups':0},
	'mediafly.com': {'cust':0, 'groups':0},
	'akamaihd.net': {'cust':0, 'groups':0},
	'googleapis.com': {'cust':0, 'groups':0},
	'icloud-content.com': {'cust':0, 'groups':0},
	'hbogo.com': {'cust':0, 'groups':0},
	'nflxvideo.net': {'cust':0, 'groups':0},
	'hbonow.com': {'cust':0, 'groups':0},
	'cloudfront.net': {'cust':0, 'groups':0},
	'tableau.com': {'cust':0, 'groups':0},
	'amazonaws.com': {'cust':0, 'groups':0},
}

for r in customer_result:
	for domain in r['globalPolicy']['local']['blockList']['valueRange']:
		try:
			count_domains[domain]['cust'] +=1
		except Exception as e:
			pass

for r in groups_result:
	domains = []
	try:
		domains += r['groupPolicy']['local']['blocks']['value']['additional']
	except Exception as e:
		pass
	try:
		domains += r['groupPolicy']['local']['blocks']['value']['exclude']
	except Exception as e:
		pass
	try:
		domains += r['groupPolicy']['raoming']['blocks']['value']['additional']
	except Exception as e:
		pass
	try:
		domains += r['groupPolicy']['raoming']['blocks']['value']['exclude']
	except Exception as e:
		pass
	try:
		domains += r['groupPolicy']['local']['blocks']['value']['additional']
	except Exception as e:
		pass
	try:
		domains += r['groupPolicy']['wifi']['blocks']['value']['exclude']
	except Exception as e:
		pass
	try:
		domains += r['groupPolicy']['wifi']['blocks']['value']['additional']
	except Exception as e:
		pass

	domains = set(domains)
	for domain in domains:
		try:
			count_domains[domain]['groups'] +=1
		except Exception as e:
			pass

print json.dumps(count_domains, indent=4, sort_keys=True)
