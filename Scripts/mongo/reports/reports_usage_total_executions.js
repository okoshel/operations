db.dev_service_event_log.group({
    "key": {"@fields.eventType":true},
    "cond": {"@fields.guid":{"$exists":true}, "@fields.eventType": {"$in":["REPORT_ELEMENT_SUCCESS","REPORT_OVERVIEW_SUCCESS","REPORT_STATS_SUCCESS"]}},
    "reduce": "function(doc, out) {out.total++;}",
    "initial": {"total":0}
}).forEach(printjson);
