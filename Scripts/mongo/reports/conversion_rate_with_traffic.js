db.temp_aggregatedEvents.group({
	"key": {"value.mcc":true, "value.mnc":true, "value.carrier":true, "value.country":true},
	"cond": {},
	"reduce": "function(doc, out) { if (doc.value.registration==true) {out.total++;} if (doc.value.profileGenerated>0) {out.success++;} if (doc.value.trafficLast24h==true) {out.trafficrun24h++;}}",
	"initial": {"total":0, "success":0, "trafficrun24h":0}
}).forEach(printjson);