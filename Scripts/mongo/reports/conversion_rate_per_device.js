var key = {
	"value.mcc" : true,
	"value.mnc" : true,
	"value.carrier" : true,
	"value.country" : true,
	"value.hwAtRegistration" : true,
	"value.iOSAtRegistration" : true,
	"value.hwLatest" : true,
	"value.iOSLatest" : true
};

var reduce = function(doc, out) {
	out.total++;
};

var cond = {
	"value.registration" : true,
	"value.hwAtRegistration" : {
		"$ne" : null
	},
	"value.iOSAtRegistration" : {
		"$ne" : null
	}
};

var init = {
	"total" : 0
};

db.temp_aggregatedEvents.group({
	"key" : key,
	"cond" : cond,
	"reduce" : reduce,
	"initial" : init
}).forEach(printjson);