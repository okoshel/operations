db.dev_service_event_log.group({
    "key": {"@fields.parameters.registrationType":true},
    "cond": {"@fields.eventType":"UPDATE_SUCCESS_APN_CHANGE"},
    "reduce": "function(doc, out) {out.total++;}",
    "initial": {"total":0}
}).forEach(printjson);