db.dev_service_event_log.group({
    "key": {"@fields.parameters.mcc":true, "@fields.parameters.mnc":true, "@fields.parameters.carrierName":true, "@fields.parameters.isoCountryCode":true},
    "cond": {"@fields.eventType":"UPDATE_SUCCESS"},
    "reduce": "function(doc, out) {var params = doc['@fields'].parameters; if (params['registrationType']!=null) {out.totalRegistrationTypeChanges++;} if (params['videoQuality']!=null) {out.totalVideoQualityChanges++;} if (params['imageQuality']!=null) {out.totalImageQualityChanges++;} if (params['tariffCost']!=null || params['billingDate']!=null || params['inclusiveMb']!=null) {out.totalPlanChanges++;} if (params['enableSecurityMode']!=null) {out.totalSecuritySettingsChanges++;}}",
    "initial": {"totalImageQualityChanges":0, "totalVideoQualityChanges":0, "totalSecuritySettingsChanges":0, "totalRegistrationTypeChanges":0, "totalPlanChanges":0}
}).forEach(printjson);
