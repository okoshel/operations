db.dev_service_event_log.group({
    "key": {"@fields.parameters.mcc":true, "@fields.parameters.mnc":true, "@fields.parameters.carrierName":true, "@fields.parameters.isoCountryCode":true},
    "cond": {"@fields.guid":{"$exists":true}},
    "reduce": "function(doc, out) {var e = doc['@fields'].eventType; if (e=='REGISTRATION_SUCCESS') {out.total++;} if (e=='PROFILE_GENERATION_SUCCESS') {out.success++;}}",
    "initial": {"total":0, "success":0}
}).forEach(printjson);