function map() {
	var lastRunVal = 0;
	var e = this['@fields'].eventType;
	
	var dayStart = new Date(); 
	dayStart.setHours(0); 
	dayStart.setMinutes(0); 
	dayStart.setSeconds(0); 
	var startTime = dayStart.getTime(); 
                                   
	if (e == 'RUNTRAFFIC_LAST24H' || e == 'REGISTRATION_SUCCESS' 
		|| e == 'PROFILE_GENERATION_SUCCESS' || e == 'REPORT_OVERVIEW_SUCCESS'
		|| e == 'REPORT_STATS_SUCCESS' || e == 'REPORT_ELEMENT_SUCCESS' || e == 'UPDATE_SUCCESS'
		|| e == 'UPDATE_SUCCESS_APN_CHANGE') {
		if (e == 'RUNTRAFFIC_LAST24H') lastRunVal = this['@fields'].timestamp;
                                   
		var value = {
        	mcc: this['@fields'].parameters.mcc,
        	mnc: this['@fields'].parameters.mnc,
        	carrier: this['@fields'].parameters.carrierName,
        	country: this['@fields'].parameters.isoCountryCode,
        	apn: ((e == 'UPDATE_SUCCESS_APN_CHANGE')? this['@fields'].parameters.apn:null),
        	registration: ((e == 'REGISTRATION_SUCCESS')? true:false),
        	registrationTime: ((e == 'REGISTRATION_SUCCESS')? this['@fields'].timestamp:0),
        	
        	hwAtRegistration: ((e == 'REGISTRATION_SUCCESS')? this['@fields'].parameters.hwPlatform:null),
        	iOSAtRegistration: ((e == 'REGISTRATION_SUCCESS')? this['@fields'].parameters.deviceSystemVersion:null),
        	appVersionAtRegistration: ((e == 'REGISTRATION_SUCCESS')? this['@fields'].parameters.appVersion:null),
        	localeAtRegistration: ((e == 'REGISTRATION_SUCCESS')? this['@fields'].parameters.appleLocale:null),
        	
        	hwLatest: ((e == 'REGISTRATION_SUCCESS' || e == 'UPDATE_SUCCESS' )? this['@fields'].parameters.hwPlatform:null),
        	iOSLatest: ((e == 'REGISTRATION_SUCCESS' || e == 'UPDATE_SUCCESS' )? this['@fields'].parameters.deviceSystemVersion:null),
        	appVersionLatest: ((e == 'REGISTRATION_SUCCESS' || e == 'UPDATE_SUCCESS' )? this['@fields'].parameters.appVersion:null),
        	localeLatest: ((e == 'REGISTRATION_SUCCESS' || e == 'UPDATE_SUCCESS' )? this['@fields'].parameters.appleLocale:null),
        	
        	deviceUpdateTime: ((e == 'REGISTRATION_SUCCESS' || e == 'UPDATE_SUCCESS' )? this['@fields'].timestamp:0),
        	profileGenerated: ((e == 'PROFILE_GENERATION_SUCCESS')? 1:0),
        	trafficLast24h: ((e == 'RUNTRAFFIC_LAST24H' && lastRunVal > startTime)? true:false),
        	reportOverviews: ((e == 'REPORT_OVERVIEW_SUCCESS')? 1:0),
        	reportStats: {
        		countTotal: ((e == 'REPORT_STATS_SUCCESS')? 1:0),
        		countLast30Days: ((e == 'REPORT_STATS_SUCCESS' && this['@fields'].parameters.key == 'last_thirty_days')? 1:0),
        		countToday: ((e == 'REPORT_STATS_SUCCESS' && this['@fields'].parameters.key == 'today')? 1:0)
        	},
         	reportElements: {
        		countTotal: ((e == 'REPORT_ELEMENT_SUCCESS')? 1:0),
        		countLast30Days: ((e == 'REPORT_ELEMENT_SUCCESS' && this['@fields'].parameters.key == 'last_thirty_days')? 1:0),
        		countToday: ((e == 'REPORT_ELEMENT_SUCCESS' && this['@fields'].parameters.key == 'today')? 1:0)
        	},       	
        	firstTrafficRun: lastRunVal,
        	lastTrafficRun: lastRunVal
        };
        emit(this['@fields'].guid, value);
	}
}

function reduce(key, values) {
	var ret = {
		mcc: '',
    	mnc: '',
    	carrier: '',
    	country: '',
    	apn: '',
    	registration: false,
    	registrationTime: 0,
    	hwAtRegistration: null,
    	iOSAtRegistration: null,
    	appVersionAtRegistration: null,
    	localeAtRegistration: null,
    	hwLatest: null,
    	iOSLatest: null,
    	hwLatest: null,
    	localeLatest: null,
    	deviceUpdateTime: 0,
        profileGenerated: 0,
        trafficLast24h: false,
        reportOverviews: 0,
        reportStats: {
        	countTotal: 0,
        	countLast30Days: 0,
        	countToday: 0
        },        
        reportElements: {
        	countTotal: 0,
        	countLast30Days: 0,
        	countToday: 0
        },
        firstTrafficRun: Number.MAX_VALUE,
        lastTrafficRun: 0
	};
                                   
	values.forEach( function(val) {
		if (val.mcc != '' && val.mcc != null) ret.mcc = val.mcc;
        if (val.mnc != '' && val.mnc != null) ret.mnc = val.mnc;
        if (val.carrier != '' && val.carrier != null) ret.carrier = val.carrier;
        if (val.country != '' && val.country != null) ret.country = val.country;
        if (val.apn != '' && val.apn != null) ret.apn = val.apn;
                
        if (val.registration === true) ret.registration = true;
        if (val.registrationTime > 0) ret.registrationTime = Math.max(ret.registrationTime, val.registrationTime);
        
        if (val.hwAtRegistration != null) ret.hwAtRegistration = val.hwAtRegistration;
        if (val.iOSAtRegistration != null) ret.iOSAtRegistration = val.iOSAtRegistration;
        if (val.appVersionAtRegistration != null) ret.appVersionAtRegistration = val.appVersionAtRegistration;
        if (val.localeAtRegistration != null) ret.localeAtRegistration = val.localeAtRegistration;
        
        if (val.deviceUpdateTime > 0 && val.deviceUpdateTime >= ret.deviceUpdateTime) {
        	if (val.hwLatest != null) ret.hwLatest = val.hwLatest;
            if (val.iOSLatest != null) ret.iOSLatest = val.iOSLatest;
        	if (val.appVersionLatest != null) ret.appVersionLatest = val.appVersionLatest;
            if (val.localeLatest != null) ret.localeLatest = val.localeLatest;
        }
        if (val.deviceUpdateTime > 0) ret.deviceUpdateTime = Math.max(ret.deviceUpdateTime, val.deviceUpdateTime);
        
        if (val.profileGenerated > 0) ret.profileGenerated+=val.profileGenerated;
        if (val.firstTrafficRun > 0) ret.firstTrafficRun = Math.min(ret.firstTrafficRun, val.firstTrafficRun);
        if (val.lastTrafficRun > 0) ret.lastTrafficRun = Math.max(ret.lastTrafficRun, val.lastTrafficRun);
        if (val.trafficLast24h === true) ret.trafficLast24h = true;
        
        if (val.reportOverviews > 0) ret.reportOverviews += val.reportOverviews;

        if (val.reportStats.countTotal > 0) ret.reportStats.countTotal+=val.reportStats.countTotal;
        if (val.reportStats.countLast30Days > 0) ret.reportStats.countLast30Days+=val.reportStats.countLast30Days;
        if (val.reportStats.countToday > 0) ret.reportStats.countToday+=val.reportStats.countToday;

        if (val.reportElements.countTotal > 0) ret.reportElements.countTotal+=val.reportElements.countTotal;
        if (val.reportElements.countLast30Days > 0) ret.reportElements.countLast30Days+=val.reportElements.countLast30Days;
        if (val.reportElements.countToday > 0) ret.reportElements.countToday+=val.reportElements.countToday;       
	});
    if (ret.firstTrafficRun == Number.MAX_VALUE) ret.firstTrafficRun = 0;
    return ret;
} 

db.dev_service_event_log.mapReduce(map, reduce, {out: {replace : "temp_aggregatedEvents"}});