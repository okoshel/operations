#!/bin/bash
ENV=MONGO_"$1"
HOST="$ENV"_HOST
USER="$ENV"_USER
PASS="$ENV"_PASSWORD
HOST=${!HOST}
USER=${!USER}
PASS=${!PASS}
mongo $HOST -u $USER -p $PASS --quiet < ./conversion_aggregation.js
