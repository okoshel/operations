db.temp_aggregatedEvents.group({
	"key": {"value.mcc":true, "value.mnc":true, "value.carrier":true, "value.country":true},
	"cond": {},
	"reduce": "function(doc, out) { if (doc.value.reportOverviews>0) {out.totalOverviews++;} if (doc.value.reportStats.countTotal>0) {out.totalStats++;} if (doc.value.reportElements.countTotal>0) {out.totalElements++;}}",
	"initial": {"totalOverviews":0, "totalStats":0, "totalElements":0}
}).forEach(printjson);