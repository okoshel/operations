db.temp_aggregatedEvents.group({
	"key": {"value.mcc":true, "value.mnc":true, "value.carrier":true, "value.country":true},
	"cond": {},
	"reduce": "function(doc, out) { if (doc.value.reportStats.countLast30Days>0) {out.reportStats.countLast30Days++;} if (doc.value.reportStats.countToday>0) {out.reportStats.countToday++;} if (doc.value.reportElements.countLast30Days>0) {out.reportElements.countLast30Days++;} if (doc.value.reportElements.countToday>0) {out.reportElements.countToday++;}}",
	"initial": {"reportStats":{"countLast30Days":0, "countToday":0}, "reportElements":{"countLast30Days":0, "countToday":0}}
}).forEach(printjson);