#!/bin/bash
# Created by Gerric Chaplin.
# Copyright (c) 2012 Snappli, Inc. All rights reserved.

#=========================#
#== Snappli Users Setup ==#
#=========================#

#----------------#
#-- Add Groups --#
#----------------#

# Add the Operations group
if [ `getent group ops|wc -l` -eq 0 ]; then
	groupadd ops
else
	/bin/echo "Group ops already exists."
fi

# Add the Development group
if [ `getent group dev|wc -l` -eq 0 ]; then
	groupadd dev
else
	/bin/echo "Group dev already exists."
fi

#---------------#
#-- Add Users --#
#---------------#

# Add Snappli Users & SSH public keys to the Instance.

# Add Gerric
if [ `getent passwd gchaplin|wc -l` -eq 0 ]; then
	useradd -m -s /bin/bash -G ops gchaplin; mkdir ~gchaplin/.ssh/; /bin/echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+OsQoLc9fdixvnWU4BI8fdsJDm9rp0yUN7Yf8rMP/Kmg8rTF20GBzj/e2XaHJrLzQllXBDgSDbPus8T1BMONGJmAEGpdvW+5kFPAg0In4H5dXW3UlTOW1QRe3QxeIkzc+5jjAD3AIlXgYH1tHUy+Ba+BDbCrku9AbRwgp+qgXyDKzbsd/JrHYGTOuZbKFV0XHhoWf+GhW0rVGaIZKLsMEn1rIYIQ0O/c1URdImzW/YpthoqlGhaplRSDDZ+Zj1cr3qVhqviRvgEPblvWQ0UK2rrVW/IpIqGzygnpt+pgdLfdvkrgOlHobeKGMsQHbiOGwvE8HxrwLjr9iDnGKVFtF gerric@snappli.com' >  ~gchaplin/.ssh/authorized_keys; chown -R gchaplin: ~gchaplin/; chmod 600 ~gchaplin/.ssh/authorized_keys;chmod 700 ~gchaplin/.ssh/
else
	/bin/echo "User gchaplin already exists."
fi

# Add Jim
if [ `getent passwd jwalker|wc -l` -eq 0 ]; then
	useradd -m -s /bin/bash -G ops jwalker; mkdir ~jwalker/.ssh/; /bin/echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCRZvC/wD8HuJSAah7vOTkl9TqQrhSR+nDbZ6bi10UWwO6LwjQ6mF4P41q7HKMbx9xew5Q376d2jerYnOQIo3VhWwL4J/hQRz/45TURpoexpVoWJSKQOdyLm46BJ9jNjqpe6scsulpTgx3MfAsMF825Js6mA/52hPA+HM8AW5ULuirv74MVbeVxoBEdDWqW+gBGM7DCtENsLFkS51SLofJvq97agse8rma88Xl3lFyco3qIOrNpMMQLEfWiNM2zq1rUtoIVBybWM2xSMSv56tqG7fhaFOIE2i6tipsLTjv6Qn4UUky5QlBHdtS9OQr02mPEcD+XT+AsmFvD3v8vzFyJ Snappli-JW' >  ~jwalker/.ssh/authorized_keys; chown -R jwalker: ~jwalker/; chmod 600 ~jwalker/.ssh/authorized_keys;chmod 700 ~jwalker/.ssh/
else
	/bin/echo "User jwalker already exists."
fi

# Add John
if [ `getent passwd jedwards|wc -l` -eq 0 ]; then
	useradd -m -s /bin/bash -G dev jedwards; mkdir ~jedwards/.ssh/; /bin/echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCmLZXFLpZTBI1p6not9S/eaXHdMTGmmgqBdaGa9XY57lZg4vohdjDnXfWxlyKiNCbLccheMCVHnK7PfYNJ7mfIKNJEZLNQrSVOu7cZ768GH5XUjPSElrpoaGy4Q2XJgWaRuvbUlu4DKF+qvMqK0vVAUq3Fzp73ywLsTsnXYnKgCootWn80SnjtFLWWtghN+MEApgBmi4nG62bqpMTEfN9ZS2l9udG0P51yxwJkR8fDN8kR3hud7BiQp9UHfCisZMOy+WvG8G0KdYVZeRuJypW4Y/mnUbHMdDmDPjHeO5h82VSwe/MMDlykToRiyEbOS/W4MknoKBPKTFSmPfY6X2jt Snappli-JDE' >  ~jedwards/.ssh/authorized_keys; chown -R jedwards: ~jedwards/; chmod 600 ~jedwards/.ssh/authorized_keys;chmod 700 ~jedwards/.ssh/
else
	/bin/echo "User jedwards already exists."
fi

# Add Thomas
if [ `getent passwd tbecker|wc -l` -eq 0 ]; then
	useradd -m -s /bin/bash -G dev tbecker; mkdir ~tbecker/.ssh/; /bin/echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDSzd5l3pYLQ1a7XM8mJWqFz8prxLAC7/SbTc201Hdh5Wq/3DEaOLUpC3BAkQ/zYn0FEUOsokoN7ZFaiwA+HQ0qgyR+LluuvI95et4v9nUyXgjpcvTnzoiYLqVJQ/ck95t0plFSFqsozTl/3O+j/fRXIvL5ixuQgxqAxALo9zE+3OO7uC2ruZWlRSLlHjxZsikBh9uStxG3QyDCFmNZGOi2KW/53pZhDD1jEFZ67I4+Q0tTBr1EHHBa1zSXqs4927T/Q0bDhkWYgGyh3i/R57X3EPlaHn1+Qr8XNWaUWNDC7IH5N/QY3qmiErzJn1cSkCvg1E+dudDoM3l3NMP6tPuJ thomas@snappli.com' >  ~tbecker/.ssh/authorized_keys; chown -R tbecker: ~tbecker/; chmod 600 ~tbecker/.ssh/authorized_keys;chmod 700 ~tbecker/.ssh/
else
	/bin/echo "User tbecker already exists."
fi

# Add Shaun
if [ `getent passwd sscaling|wc -l` -eq 0 ]; then
	useradd -m -s /bin/bash -G dev sscaling; mkdir ~sscaling/.ssh/; /bin/echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDSqR5v5ViQfuc36+GrTYqph6vM7N47DnyzvzzxVRQZOeJKvXI7y4vrkHp+7dnoEbwPtAgPohRhsE3DkuBmQWVvKTkxT5p4U0b7V+Qh+OwwiCM/QFzNbBPK0c20GAbpVfaXmHKb7SogHdTjkyPNsSdeoyP+t9Ct2Rvr5kpDC9hiGTLZECTjKXDH4ActwNsf43jA58zAgzjS9onQiIZVMYgBbxE6dTCyW7oiopFiETODdlJDTZVyUi//Hk4C1YR2mMtFDgt1BxaTjw37AIXuEHtF5w9EzO2ZKzBa6O9ifYvjty0IqpIin91HH8kadpmFSKEQZOhILDzDzL3Tz0wNmpcv shaun@snappli.com' >  ~sscaling/.ssh/authorized_keys; chown -R sscaling: ~sscaling/; chmod 600 ~sscaling/.ssh/authorized_keys;chmod 700 ~sscaling/.ssh/
else
	/bin/echo "User sscaling already exists."
fi

# Add Ruslan
if [ `getent passwd rciurca|wc -l` -eq 0 ]; then
	useradd -m -s /bin/bash -G dev rciurca; mkdir ~rciurca/.ssh/; /bin/echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDOiDLq13foA/DfKVS8RpTtAFi4JoATXRRaPWmvumnRl+RVV6yilaqh+9+VecLPKZbGDH0RKXRhFM3XynAixFwnRZrC/Mu+8XDwSzeF1Z7LhstTVJ3K+f2aVrriD4leUO+RUBCqsmkClmY7uIKjxfDCgIbqlpLgU3HVRSFhLDSB+vHtNJxJIyHJOyeCmnYQh8Xh0GsNCYcMtl6ckrW//f6NeTIGrK5gizz5sZzHYewzpHcYfcfE08piv/7NJ+qDZRuTY6csyE3X+Dmmy13V93/d2mkhZLi3gpxLue7QwYC4YNFPzztg86Vl/17PpoBJOrEr5tTMZhAsU+brXCb7j6yD ruslanciurca@Ruslans-MacBook-Air.local' >  ~rciurca/.ssh/authorized_keys; chown -R rciurca: ~rciurca/; chmod 600 ~rciurca/.ssh/authorized_keys;chmod 700 ~rciurca/.ssh/
else
	/bin/echo "User rciurca already exists."
fi

# Add Ondrej
if [ `getent passwd onevelik|wc -l` -eq 0 ]; then
        useradd -m -s /bin/bash -G dev onevelik; mkdir ~onevelik/.ssh/; /bin/echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDWybx+tQxXlbAqkpUcw40uNb25CN0RRcicInJxpvjdSSM3sRLH7Ac0NjkS/Cerup8/lIg+hcuNbBGw28LWo/GWeIbCJV8yA13PPIIoa1oFfarccGaoTs2ZfKS6mcMZ5QCleoB1CsL/B1mqXGqlV2TC4KqU/Ch+D5r/mN5GCPE9CTglpqy+7gRVTUG+aKHstBl2qO9bP9GmmwSvMH/oFYzDlstj9n6eIF5ld6W05JHpwx2IZNPXUvlGp9bxBfzvRnf+82FcDuEsycPs0ry+SFpVnKmR5mBuksVKk6FA6xCZYBxA9gR6oK5pLL2XQsHPecvxb6frN/LEJvcdcapYpZFV andreas@Ondrejs' >  ~onevelik/.ssh/authorized_keys; chown -R onevelik: ~onevelik/; chmod 600 ~onevelik/.ssh/authorized_keys;chmod 700 ~onevelik/.ssh/
else
        /bin/echo "User onevelik already exists."
fi

# Add Ralph
if [ `getent passwd rmasilamani|wc -l` -eq 0 ]; then
        useradd -m -s /bin/bash -G dev rmasilamani; mkdir ~rmasilamani/.ssh/; /bin/echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBw+KPzeHCMT8WDiZiA/8WsijgBlYJbkcBIt3ZSAlgB8brwmaI2/79g3h06liZE1BCoxtqf9rfYyXC7ZXb8y5r3L4RC4evXoV4sE0BU1xl42AFenHcY99fLy0zt/zatGLZFofJ80HOMKvZ7fkIemiZ/J5zGu0lO5FDRnsbs2U5oFL+/iix53lDNyJhDkx5XwzEkoYjBTWhtZjZvJZDyi9aiORp7el0zPM8wCNZ8QEtBqtnn7sUjbQ0dRjYly4jhg+N1bLItqVWh4VlWujhfrKz0L9kXABrV6PZMKI786TFoPKv+sZFC8DaOkr8/COROiIS6+PJG2WC+WD3LDxJsCL/ ralph@snappli.com' >  ~rmasilamani/.ssh/authorized_keys; chown -R rmasilamani: ~rmasilamani/; chmod 600 ~rmasilamani/.ssh/authorized_keys;chmod 700 ~rmasilamani/.ssh/
else
        /bin/echo "User onevelik already exists."
fi

#---------------------------#
#-- Add Groups to sudoers --#
#---------------------------#

if [ `grep ops /etc/sudoers|wc -l` -eq 0 ]; then
	/bin/echo "%ops  ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
else
	/bin/echo "User ops group already in sudoers."
fi

if [ `grep dev /etc/sudoers|wc -l` -eq 0 ]; then
	/bin/echo "%dev  ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
else
	/bin/echo "User dev group already in sudoers."
fi

#===================#
#== System Checks ==#
#===================#

# Add the machines hostname to /etc/hosts
if [ -e /etc/hosts ]; then
	/bin/echo "Checking to see if hostname is correct"
	host=`hostname`
	hostip=`echo $host | cut -d "-" -f2,3,4,5 |sed "s/\-/\./g"`
	hosts_entry="$hostip $host"
	hostcount=`grep "$hosts_entry" /etc/hosts | wc -l`
	if [ "$hostcount" -eq "0" ]; then
		echo $hosts_entry >> /etc/hosts
		/bin/echo "The hostname has been updated"
	else
		/bin/echo "The hostname is already in the hosts file."
	fi
fi

#---------------------------------#
#-- Check we are running Ubuntu --#
#---------------------------------#

if [ -e /etc/lsb-release ]; then
	/bin/echo "This is an Ubuntu server continuing with setup."
else
	/bin/echo "We are not running on Ubuntu. This script is for Ubuntu only."
	exit 1
fi

#===========================#
#== System Software Setup ==#
#===========================#

#------------------------------#
# Update apt packages list --#
#------------------------------#
apt-get -y update

#-----------------------#
#-- Install Denyhosts --#
#-----------------------#

# Install denyhosts using apt-get
apt-get -y install denyhosts

# Put a rule in hosts.allow for the new office.
if [ `grep 80.71.4.36 /etc/hosts.allow|wc -l` -eq 0 ]; then
	/bin/echo "sshd: 87.224.90.0/255.255.254.0 : allow" >> /etc/hosts.allow
	/bin/echo "sshd: 80.71.4.36/255.255.255.252 : allow" >> /etc/hosts.allow
else
	/bin/echo "Office IP already in hosts.allow."
fi

# Reconfgure the email details in the denyhosts file
host=`hostname`
if [ `grep $host /etc/denyhosts.conf|wc -l` -eq 0 ]; then
	sed -i "s/localhost/$host/g" /etc/denyhosts.conf
	service denyhosts restart
	/bin/echo "The denyhosts config file has been updated with the correct hostname"
else
	/bin/echo "The correct hostname is already in the denyhosts config file."
fi

#------------------#
#-- Install ntpd --#
#------------------#

# Install ntp service.
apt-get -y install ntp

if [ `grep iburst /etc/ntp.conf|wc -l` -eq 0 ]; then
	/bin/echo -e "restrict default nomodify notrap\nrestrict 127.0.0.1 \nserver 0.amazon.pool.ntp.org burst iburst\nserver 1.amazon.pool.ntp.org burst iburst\nserver 2.amazon.pool.ntp.org burst iburst\nserver 3.amazon.pool.ntp.org burst iburst\nserver 127.127.1.0 # local clock\nfudge 127.127.1.0 stratum 10	\ndriftfile /var/lib/ntp/drift \nkeys /etc/ntp/keys" > /etc/ntp.conf
	service ntp restart
else
	/bin/echo "ntp is already set up on this server"
fi

#---------------------#
#-- Install Postfix --#
#---------------------#

# Install Postfix in none interactive so it does not prompt you for anything.
DEBIAN_FRONTEND=noninteractive apt-get install postfix

# Put an alias for root in postfix.
if [ `grep snappli.com /etc/aliases|wc -l` -eq 0 ]; then
	/bin/echo "root: ops@snappli.com" >> /etc/aliases
	newaliases; service postfix restart
else
	/bin/echo "root email alias already in aliases."
fi

# Change the hostname to the correct server in postfix
if [ -e /etc/postfix/main.cf ]; then
	/bin/echo "Postfix is installed checking hostname."
	host=`hostname`
	hostcount=`egrep "^myhostname" /etc/postfix/main.cf | grep $host | wc -l`
	if [ "$hostcount" -eq "0" ]; then
		sed -i "/^myhostname/d" /etc/postfix/main.cf && /bin/echo "myhostname = $host" >> /etc/postfix/main.cf && service postfix restart
		/bin/echo "Postfix has been updated to include the correct hostname"
	fi
else
	/bin/echo "The hostname is already in the postfix configuration."
fi

#--------------------#
#-- System Cleanup --#
#--------------------#

# Add the machines hostname to collectd
if [ -e /etc/collectd/collectd.conf ]; then
	/bin/echo "Checking to see if hostname is correct in collectd"
	host=`hostname`
	hostcount=`grep "$host" /etc/collectd/collectd.conf | wc -l`
	if [ "$hostcount" -eq "0" ]; then
		sed -i "s/localhost/$host/g" /etc/collectd/collectd.conf
		service collectd restart
		/bin/echo "The hostname has been updated"
	else
		/bin/echo "The hostname is already in the hosts file."
	fi
fi

#--------------------------#
#-- Sysctl configuration --#
#--------------------------#

# Add sysctl configuration for the system
if [ `grep "Snappli changes" /etc/sysctl.conf |wc -l` -eq 0 ]; then
echo "# Snappli changes
#http://wiki.docdroppers.org/index.php?title=Sysctl_Modifications
# Disable packet forwarding.
net.ipv4.ip_forward=0
# Increase the maximum number of open files 
fs.file-max = 131072
# This option enables fast recycling of sockets in the TCP_WAIT state.
net.ipv4.tcp_tw_recycle = 0
# This option defines the range of ports that we allow clients to connect on.
net.ipv4.ip_local_port_range = 14001 65535
# This option tells the kernel how long to keep connections in the FIN-WAIT-2 state.
net.ipv4.tcp_fin_timeout = 10
# This option instructs the kernel to disable TCP timestamps.
net.ipv4.tcp_timestamps = 0
# We don't really want to proxy ARP for anyone, do we?
net.ipv4.conf.all.proxy_arp = 0" >> /etc/sysctl.conf
/sbin/sysctl -p
else 
	echo "Sysct config already applied"
fi

#--------------------------#
#-- limits configuration --#
#--------------------------#

# Add the security limits for users
if [ `grep "131072" /etc/sysctl.conf |wc -l` -eq 0 ]; then
echo "
*        soft    nofile	131072
*        hard    nofile	131072
*        soft    nproc	131072
*        hard    nproc	131072
rabbit        soft    nproc           131072
rabbit        hard    nproc           131072
asvc        soft    nproc           131072
asvc        hard    nproc           131072
rsvc        soft    nproc           131072
rsvc        hard    nproc           131072
psvc        soft    nproc           131072
psvc        hard    nproc           131072
usvc        soft    nproc           131072
usvc        hard    nproc           131072" >> /etc/security/limits.conf
else
	echo "limits already applied"
fi

#-----------------------------#
#-- su limits configuration --#
#-----------------------------#

# Add pam limits to sudo commands
if [ `grep pam_limits.so /etc/pam.d/su | grep '#'| wc -l` -eq 1 ]; then
sed -i "s/# session    required   pam_limits.so/session    required   pam_limits.so/g" /etc/pam.d/su
else
	echo "Limits will apply to su commands"
fi

#-----------------------------#
#-- sudo limits configuration --#
#-----------------------------#

# Add pam limits to sudo commands
if [ `grep pam_limits.so /etc/pam.d/sudo | wc -l` -eq 0 ]; then
echo "session    required   pam_limits.so" >> /etc/pam.d/sudo
else
	echo "Limits will apply to sudo commands"
fi

#------------------#
#-- Misc Cleanup --#
#------------------#

# Remove the popularity-contest cron entry. We don't want our deb lists going to somewhere public.
if [ -e /etc/cron.daily/popularity-contest ]; then
	/bin/echo "Popularity contest cron entry exists"
	rm -rf /etc/cron.daily/popularity-contest
else
	/bin/echo "Popularity contest cron entry does not exist"
fi

# Create upstart directory due to https://bugs.launchpad.net/ubuntu/+source/upstart/+bug/990102
if [ -d "/var/log/upstart" ]; then
	/bin/echo "Upstart log directory exists"
else
	/bin/echo "Upstart log directory does not exist. Creating."
	mkdir -p /var/log/upstart
fi
